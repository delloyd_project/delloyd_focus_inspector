﻿//*****************************************************************************
// Filename	: 	Def_Test_Cm.h
// Created	:	2016/06/30
// Modified	:	2016/08/08
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Test_Cm_h__
#define Def_Test_Cm_h__

#include "afxwin.h"
/*영상 WIDTH */
#define		CAM_IMAGE_WIDTH 720
/*영상 HEIGHT */
#define		CAM_IMAGE_HEIGHT 480
/*영상 WIDTH /2 */
#define		CAM_IMAGE_WIDTH_HALF CAM_IMAGE_WIDTH / 2
/*영상 HEIGHT /2 */
#define		CAM_IMAGE_HEIGHT_HALF CAM_IMAGE_HEIGHT / 2



/*영상 총 버퍼 사이즈 720*480*4  */
#define		RGB_BUF_SIZE CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT * 4

/*영상 버퍼 사이즈 720*480  */
#define		IMAGE_BUF_SIZE 921600 

/*영상 720*4  */
#define		IMAGE_WX4 5120 

/*카메라의 영상상태*/
enum enCamState
{
	/*좌우반전*/
	CAM_STATE_MIRROR =0,

	/*상하반전*/
	CAM_STATE_FLIP,

	/*오리지널*/
	CAM_STATE_ORIGINAL,

	/*로테이트*/
	CAM_STATE_ROTATE,

	CAM_STATE_MAXNUM,
};

static LPCTSTR	g_szCamState[] =
{
	_T("Original"),
	_T("Mirror & Flip"),
	_T("Horizontal Mirror"),
	_T("Vertical Flip"),
	NULL
};

/*카메라 영상의 네모 위치*/
enum enModel_Quad_Rect_Pos
{
	QRP_NOTUSE = 0,
	QRP_LT,
	QRP_LB,
	QRP_RT,
	QRP_RB,
};

// Device Fail Code
enum nResult
{
	result_Fail,
	result_Pass,
	result_MotorFail,
	result_CamDisconnect,
	result_HT_Fail,			
};

// 결과 코드 전체 검사
enum enResultCode_All
{
	RCA_UnknownError = 0,
	RCA_OK,
	RCA_Invalid_Handle,
	RCA_Exception,
	RCA_Parameter,
	RCA_TestRunning,
	RCA_Model_Err,
	RCA_EMO,
	RCA_MaxPogoCount,
	RCA_AreaSensor,
	RCA_PogoDown_Err,
	RCA_NoTest,
	RCA_RotateTable_Err,
	RAC_MotorMove_Err,
	RCA_Model_Empty_Err,
	RCA_LotID_Empty_Err,
	RCA_Operator_Empty_Err,
	RCA_Barcode_Empty_Err,
	RCA_Safty_Err,
	RCA_SocketEmpty_Err,
	RCA_ChartSensorEmpty_Err,
	RCA_CameraCurrent_Err,
	RCA_CommSignal_Err,
	RCA_CommTimeOutErr,
	RCA_ForcedStop,
	RAC_InitSetting_Err,
	RAC_DarkRoomUp_Err,
	RAC_UserStop_Err,
	RCA_Max,
};

static LPCTSTR g_szResultCode_All[] =
{
	_T("[Result] 알 수 없는 오류 발생"),					// RCA_UnknownError
	_T("[Result] OK"),									// RCA_OK
	_T("[Result] 무효한 포인터 오류"),					// RCA_Invalid_Handle
	_T("[Result] 예외상황 발생"),							// RCA_Exception
	_T("[Result] 잘못된 파라미터 처리"),					// RCA_Parameter
	_T("[Result] 검사 진행 중"),							// RCA_TestRunning
	_T("[Result] 모델 설정 오류"),						// RCA_Model_Err
	_T("[Result] EMO 상황"),								// RCA_EMO
	_T("[Result] The Pogo Pin Count reached the upper limit."),			// RCA_MaxPogoCount
	_T("[Result] Area 센서 감지 됨"),						// RCA_AreaSensor
	_T("[Result] 포고 지그 하강 오류"),					// RCA_PogoDown_Err
	_T("[Result] 검사 가능한 채널이 없음"),				// RCA_NoTest
	_T("[Result] 턴테이블 이동 오류"),					// RCA_RotateTable_Err
	_T("[Result] 모터 동작 오류 "),						// RCA_RotateTable_Err
	_T("[Result] 모델이 설정 되지 않았습니다."),			// RCA_Model_Empty_Err
	_T("[Result] LOT ID가 설정 되지 않았습니다."),		// RCA_LotID_Empty_Err
	_T("[Result] 작업자가 설정 되지 않았습니다."),		// RCA_Operator_Empty_Err
	_T("[Result] No barcode input.\r\nPlease enter a barcode."),	// RCA_Barcode_Empty_Err
	_T("[Result] 장비를 확인해 주세요."),					// RCA_Safty_Err
	_T("[Result] 소켓이 센서가 인식 되지 않았습니다."),	// RCA_SocketEmpty_Err
	_T("[Result] 차트 센서가 인식 되지 않았습니다.."),	// RCA_ChartSensorEmpty_Err
	_T("[Result] 전류 측정 데이터 오류"),					// RCA_CameraCurrent_Err
	_T("[Result] 테스터 PC와 통신 상태 불량"),			// RCA_CommSignal_Err
	_T("[Result] 테스터 PC와 통신 타임 아웃"),			// RCA_CommTimeOutErr
	_T("[Result] 작업자가 검사 작업을 중지합니다. "),		// RCA_ForcedStop
	_T("[Result] No initial instrument settings."),		// RAC_InitSetting_Err
	_T("[Result] DarkRoom Up Fail."),		// RAC_DarkRoomUp_Err
	_T("[Result] User Stop."),		// RAC_DarkRoomUp_Err
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

// 결과 코드 채널 개별 검사
enum enResultCode_Unit
{
	RCC_UnknownError = 0,
	RCC_OK,
	RCC_TestSkip,
	RCC_Invalid_Handle,
	RCC_Invalid_Point,
	RCC_Exception,
	RCC_Safty_Err,
	RCC_DeviceComm_Err,

	RCC_NoBarcode,	// 바코드 없음
	RCC_NoLotInfo,
	RCC_SiteA_Motor_T_Foward_Err,
	RCC_WaitAll_Timeout,
	RCC_WaitAll_Fail,
	RCC_TestAll_Start_Fail,

	RCC_LimitPogoCnt,
	RCC_Max,
};

// 결과 코드 Write Rom
enum enResultCode_TestItem
{
	RCTI_Error = 0,
	RCTI_OK = 1,
	RCTI_TestSkip,
	RCTI_Invalid_Handle,
	RCTI_Exception,
	
};

static LPCTSTR g_szResultCode_TestItem[] =
{
	_T("Error"),				// RCWR_UnknownError = 0,
	_T("OK"),					// RCWR_OK = 1,
	_T("Test Skip"),			// RCWR_TestSkip,
	_T("Invalid Handle"),		// RCWR_Invalid_Handle,
	_T("Exception Err"),		// RCWR_Exception,

	_T("Rom File Err"),			// RCWR_RomFile_Err,
	_T("Sync Fail"),			// RCWR_SyncFail,
	_T("Erase Fail"),			// RCWR_EraseFail,
	_T("Write Fail"),			// RCWR_WriteFail,
	_T("Read Fail"),			// RCWR_ReadFail,
	_T("Verify Fail"),			// RCWR_VerifyFail,
	_T("Comm Err"),				// RCWR_PCB_Comm_Err,
	NULL
};

// 기본 결과 코드
enum enResultCode
{
	RC_Error = 0,
	RC_OK = 1,
	RC_TestSkip,
	RC_Invalid_Handle,
	RC_Exception,

	RC_PowerOn_Err,
	RC_PowerOff_Err,
	RC_EEPROM_Erase_Err,
	RC_EEPROM_Write_Err,
	RC_EEPROM_Read_Err,
	RC_EEPROM_Verify_Err,
	RC_Comm_Err,
	RC_Comm_TimeOut_Err,

	RCC_EMO,
	RCC_ForcedStop,

	// Depth Call(H)
	RC_SA_Check_SafetySensor,
	RC_SA_Check_SocketEmptySensor,
	RC_SA_Check_Chart500EmptySensor,
	RC_SA_Check_Chart200EmptySensor,
	RC_SA_Check_ChartHallCodeEmptySensor,
	RC_SA_Check_CameraCurrent_Err,
	RC_SB_Check_SocketEmptySensor,
	RC_SB_Check_ChartTest_Err,
	RC_SC_Check_SocketEmptySensor,
	RC_SC_Check_ChartTest_Err,
	RC_SD_Check_SocketEmptySensor,
	RC_SD_Check_ChartTest_Err,

	// Depth Test(H)
	RC_SA_Check_Chart1000EmptySensor,
	RC_SA_Check_Chart300EmptySensor,

};

static LPCTSTR g_szResultCode[] =
{
	_T("Error"),								// RC_UnknownError = 0,
	_T("OK"),									// RC_OK = 1,
	_T("Test Skip"),							// RC_TestSkip,
	_T("Invalid Handle"),						// RC_Invalid_Handle,
	_T("Exception Error"),						// RC_Exception,

	_T("Power On Error"),						// RC_PowerOn_Err,
	_T("Power Off Error"),						// RC_PowerOff_Err,
	_T("Erase Error"),							// RC_Erase_Err,
	_T("Write Error"),							// RC_Write_Err,
	_T("Read Error"),							// RC_Read_Err,
	_T("Verify Error"),							// RC_Verify_Err,
	_T("SafetySensor Check Error"),				// RC_ZA_Check_SafetySensor,
	_T("SocketEmpty Sensor Check Error"),		// RC_ZA_Check_SocketEmptySensor,

	_T("Chart 200mm Sensor Check Error"),		// RC_ZA_Check_Chart200EmptySensor,
	_T("Chart 500mm Sensor Check Error"),		// RC_ZA_Check_Chart500EmptySensor,
	_T("Chart HellCode Sensor Check Error"),	// RC_ZA_Check_ChartHallCodeEmptySensor,
	NULL
};

enum enFailureCause
{
	Fail_NoError			= 0x00000000,
	Fail_SiteA				= 0x00000001,
	Fail_SiteB				= 0x00000002,
	Fail_SiteC				= 0x00000004,
	Fail_SiteD				= 0x00000008,

	Fail_Barcode			= 0x00000010,
	Fail_EmptyCam			= 0x00000020,
	Fail_LightBrd_Comm		= 0x00000040,
	Fail_CAM_Disconnet		= 0x00000080,

	Fail_CommSignal			= 0x00000100,
	Fail_EmptySocket		= 0x00000200,
	Fail_EmptyChart500		= 0x00000400,
	Fail_EmptyChart200		= 0x00000800,
	Fail_EmptyChartTest		= 0x00001000,
	Fail_EmptyChartHallCode = 0x00002000,
	Fail_Current			= 0x00004000,
	Fail_ChartSetting		= 0x00008000,
	Fail_Safty				= 0x00010000,
	Fail_EmptyChart1000		= 0x00020000,
	Fail_EmptyChart300		= 0x00040000,
	Fail_Depthtest			= 0x00080000,
	Fail_CommTimeout		= 0x00100000,
	Fail_PDEFTest			= 0x00200000,
	Fail_AFTest				= 0x00400000,
	Fail_HHCTest			= 0x00800000,

};

typedef struct _tag_StaticInf
{
	LPCTSTR		szText;
	COLORREF	TextColor;
	COLORREF	BackColor;
	COLORREF	BorderColor;
}ST_StaticInf, *PST_StaticInf;

typedef enum enTestProcess
{
	TP_Idle,
	TP_Run,
	TP_Stop,
	TP_Completed,
	TP_Error,
	TP_ForcedStop, //EMO, 사용자 정지, Stop
	TP_TableRotation,
	TP_Hold,
	TP_DryRun,
};

static ST_StaticInf g_TestProcess[] =
{	// Text					Text Color			Back Color			Border Color
	{ _T("STAND BY"),		RGB(  0,   0,   0), RGB(255, 255, 255), RGB(  0,   0,   0)	},
	{ _T("PROCESSING"),		RGB(255, 255, 255), RGB(255, 192,   0), RGB(188, 140,   0)	},
	{ _T("STOP"),			RGB(255, 255, 255), RGB(  0, 176,  80), RGB( 80, 126,  50)	},
	{ _T("COMPLETION"),		RGB(255, 255, 255), RGB(  0, 176,  80), RGB( 80, 126,  50)	},
	{ _T("ERROR"),			RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)	},
	{ _T("FORCED STOP"),	RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)	},
	{ _T("ROTATE"),			RGB(255, 255, 255), RGB(255, 192, 0),	RGB(188, 140, 0)	},
	{ _T("HOLD"),			RGB(0, 0, 0),		RGB(255, 255, 255), RGB(255, 255, 255)	},
	{ _T("DRY RUN"),		RGB(0, 0, 0),		RGB(255, 255, 255), RGB(255, 255, 255)	},

	NULL
};

typedef enum enTestResult
{
 	TR_Fail,
 	TR_Pass,
 	TR_Empty,
 	TR_Testing,
 	TR_Skip,
	TR_Init,		// 제품 투입전 초기 상태
	TR_Rework,		// EMO, 사용자 정지로 인한 재검해야 하는 경우
	TR_UserStop,	// 작업자가 검사 중지 시킨 경우
	TR_MachineCheck,// 기구, 보드, 통신 이상으로 검사가 정상적으로 이루어지지 않은 경우
	TR_Timeout,		// 검사 중이 아닐때, Tester로 부터 Timeout 신호 받은 경우
	TR_NoImage,
	TR_CamStateFail,// Camera 위치가 제대로 안들어간 경우
};

static ST_StaticInf g_TestResult[] =
{	// Text					Text Color			Back Color			Border Color
	{ _T("FAIL"),			RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)	},
	{ _T("PASS"),			RGB(255, 255, 255), RGB(  0, 176,  80), RGB( 80, 126,  50)	},
	{ _T("EMPTY"),			RGB(  0,   0,   0),	RGB(255, 255, 255), RGB(255, 255, 255)	},
	{ _T("TESTING"),		RGB(255, 255, 255), RGB(112,  48, 160), RGB( 62,  20, 110)	},
	{ _T("SKIP"),			RGB(255, 255, 255), RGB(255, 255, 255),	RGB( 47,  82, 143)	},
	{ _T("STAND BY"),		RGB(255, 255, 255), RGB(  0, 176,  80), RGB( 80, 126,  50)	},
	{ _T("REWORK"),			RGB(  0,   0,   0),	RGB(237, 125,  49), RGB(  0,   0,   0)	},
	{ _T("USER STOP"),		RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)	},
	{ _T("Machine Check"),	RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)	},
	{ _T("TIMEOUT"),		RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)  },
	{ _T("No Image"),		RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)  },
	{ _T("Cam State Fail"),	RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33)  },
	NULL
};

typedef enum enStepStatus
{
	TSS_Fail,
	TSS_Pass,
	TSS_Run,
	TSS_Error,
	TSS_Idle,
};

static ST_StaticInf g_StepStatus[] =
{	// Text				Text Color			Back Color			Border Color
	{ _T("Fail"),		RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33) },
	{ _T("Pass"),		RGB(255, 255, 255), RGB(  0, 176,  80), RGB( 80, 126,  50) },
	{ _T("Run"),		RGB(255, 255, 255), RGB(255, 192,   0), RGB( 62,  20, 110) },
	{ _T("Error"),		RGB(255, 255, 255), RGB(192,   0,   0), RGB(174,  90,  33) },
	{ _T(""),			RGB(  0,   0,   0), RGB(255, 255, 255), RGB(255, 255, 255) },
	NULL
};




#endif // Def_Test_Cm_h__
