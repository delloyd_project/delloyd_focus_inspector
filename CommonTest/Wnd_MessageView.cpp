﻿//*****************************************************************************
// Filename	: 	Wnd_MessageView.cpp
// Created	:	2016/3/11 - 14:59
// Modified	:	2016/3/11 - 14:59
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
// Wnd_AcessMode.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_MessageView.h"
#include "resource.h"
#include "Def_WindowMessage_Cm.h"

#include "Dlg_ChkPassword.h"
//=============================================================================
// CWnd_MessageView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_MessageView, CWnd)

CWnd_MessageView::CWnd_MessageView()
{
	VERIFY(m_Font.CreateFont(
		25,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_bResult	= FALSE;
	m_bMode		= FALSE;

	m_hTimer_FixCheck		= NULL;
	m_hTimerQueue_FixCheck	= NULL;

	m_pbFixStatus			= NULL;
	CreateTimerQueue_Mon();
}

CWnd_MessageView::~CWnd_MessageView()
{
	m_Font.DeleteObject();
	DeleteTimerQueue_Mon();
}

BEGIN_MESSAGE_MAP(CWnd_MessageView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDOK,					OnBnClickedOK		)
	ON_BN_CLICKED(IDCANCEL,				OnBnClickedCancel	)
END_MESSAGE_MAP()

//=============================================================================
// CWnd_MessageView message handlers
//=============================================================================
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/7/12 - 12:12
// Desc.		:
//=============================================================================
int CWnd_MessageView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	SetBackgroundColor(RGB(0, 0, 0));

	//m_st_Message.SetBackColor(RGB(255, 216, 216));
	m_st_Message.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_st_Message.SetColorStyle(CVGStatic::ColorStyle_Red);
	m_st_Message.SetFont_Gdip(L"맑은 고딕", 25.0F);
	m_st_Message.Create(_T("Warring Message"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_bn_OK.Create(_T("OK"), dwStyle | WS_TABSTOP, rectDummy, this, IDOK);
	m_bn_OK.SetFont(&m_Font);
	
	m_bn_Cancel.Create(_T("Cancel"), dwStyle | WS_TABSTOP, rectDummy, this, IDCANCEL);
	m_bn_Cancel.SetFont(&m_Font);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/11 - 15:00
// Desc.		:
//=============================================================================
void CWnd_MessageView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 5;
	int iSpacing = 5;
	int iCateSpacing = 5;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;
	int iCtrlWidth = (iWidth - iSpacing) / 2;
	int iCtrlHeight = (iHeight - iSpacing) / 5;
	int iStaticWidth = 100;
	int iTempWidth = iWidth - iStaticWidth;
	int iSubLeft = iLeft + iStaticWidth;

	m_st_Message.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight * 4);

	iTop += iCtrlHeight * 4 + iSpacing;
	iCtrlWidth = (iWidth - (iSpacing * 1)) / 2;
	m_bn_OK.MoveWindow(iLeft + 1, iTop, iCtrlWidth, iCtrlHeight + iSpacing);
	
	iLeft += iCtrlWidth + iSpacing;
	m_bn_Cancel.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight + iSpacing);
}

//=============================================================================
// Method		: OnGetMinMaxInfo
// Access		: protected  
// Returns		: void
// Parameter	: MINMAXINFO * lpMMI
// Qualifier	:
// Last Update	: 2017/6/22 - 10:17
// Desc.		:
//=============================================================================
void CWnd_MessageView::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 850;
	lpMMI->ptMaxTrackSize.y = 200;

	lpMMI->ptMinTrackSize.x = 850;
	lpMMI->ptMinTrackSize.y = 200;

	CWnd::OnGetMinMaxInfo(lpMMI);
}

//=============================================================================
// Method		: OnBnClickedOK
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 17:25
// Desc.		:
//=============================================================================
void CWnd_MessageView::OnBnClickedOK()
{
	m_bResult = TRUE;

	if (m_bMode == TRUE)
	{
		CDlg_ChkPassword	dlgPassword(this);
		
		if (dlgPassword.DoModal() == IDOK)
		{
			EndModalLoop(TRUE);
			ShowWindow(SW_HIDE);

			CWnd::OnClose();
		}
		
	}
	else
	{
		EndModalLoop(TRUE);
		ShowWindow(SW_HIDE);

		CWnd::OnClose();
	}
}

//=============================================================================
// Method		: OnBnClickedCancel
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 17:26
// Desc.		:
//=============================================================================
void CWnd_MessageView::OnBnClickedCancel()
{
	m_bResult = FALSE;
	
	if (m_bMode == TRUE)
	{
		CDlg_ChkPassword	dlgPassword(this);

		if (dlgPassword.DoModal() == IDOK)
		{
			EndModalLoop(TRUE);
			ShowWindow(SW_HIDE);

			CWnd::OnClose();
		}

	}
	else
	{
		EndModalLoop(TRUE);
		ShowWindow(SW_HIDE);

		CWnd::OnClose();
	}
}

//=============================================================================
// Method		: DoModel
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/12 - 12:31
// Desc.		:
//=============================================================================
BOOL CWnd_MessageView::DoModal(BOOL bMode, BOOL bFixCheckMode /*= FALSE*/)
{
	m_bMode = bMode;

	if (bFixCheckMode)
	{
		//  [1/25/2019 ysJang]
		//timer 생성. m_pbFixStatus 가 True 이면 DLG 넘어가고 정상루틴
		if (NULL == m_hTimer_FixCheck)
		{
			if (!CreateTimerQueueTimer(&m_hTimer_FixCheck, m_hTimerQueue_FixCheck, (WAITORTIMERCALLBACK)TimerRoutine_FixCheck, (PVOID)this, 100, 400, WT_EXECUTEDEFAULT))
			{
				TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
			}
		}
	}

	if (GetParent())
	{
		RunModalLoop(MLF_SHOWONIDLE);
		DestroyWindow();
	}

	if (bFixCheckMode)
	{
		if (NULL != m_hTimer_FixCheck)
		{
			if (DeleteTimerQueueTimer(m_hTimerQueue_FixCheck, m_hTimer_FixCheck, NULL))
			{
				m_hTimer_FixCheck = NULL;
			}
			else
			{
				TRACE(_T("DeleteTimerQueueTimer : m_hTimer_FixCheck failed (%d)\n"), GetLastError());
			}
		}
	}
	
	return m_bResult;
}

VOID CALLBACK CWnd_MessageView::TimerRoutine_FixCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	CWnd_MessageView* pThis = (CWnd_MessageView*)lpParam;

	if (*pThis->m_pbFixStatus == TRUE)
		pThis->OnBnClickedOK();
}

void CWnd_MessageView::CreateTimerQueue_Mon()
{
	__try
	{
		// Create the timer queue.
		m_hTimerQueue_FixCheck = CreateTimerQueue();
		if (NULL == m_hTimerQueue_FixCheck)
		{
			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
			return;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CreateTimerQueue_Mon ()"));
	}
}

void CWnd_MessageView::DeleteTimerQueue_Mon()
{
	__try
	{
		if (!DeleteTimerQueue(m_hTimerQueue_FixCheck))
			TRACE(_T("DeleteTimerQueue failed (%d)\n"), GetLastError());
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CTestManager_Base::DeleteTimerQueue_Mon()\n"));
	}

	TRACE(_T("타이머 종료 : CTestManager_Base::DeleteTimerQueue_Mon()\n"));

}

//=============================================================================
// Method		: SetWarringMessage
// Access		: public  
// Returns		: void
// Parameter	: CString szText
// Qualifier	:
// Last Update	: 2017/7/12 - 10:57
// Desc.		:
//=============================================================================
void CWnd_MessageView::SetWarringMessage(CString szText)
{
	m_st_Message.SetWindowText(szText);
}

//=============================================================================
// Method		: CWnd_MessageView::SetBackgroundColor
// Access		: protected 
// Returns		: void
// Parameter	: COLORREF color
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2010/11/23 - 10:02
// Desc.		:
//=============================================================================
void CWnd_MessageView::SetBackgroundColor(COLORREF color, BOOL bRepaint /*= TRUE*/)
{
	if (m_brBkgr.GetSafeHandle() != NULL)
	{
		m_brBkgr.DeleteObject();
	}

	if (color != (COLORREF)-1)
	{
		m_brBkgr.CreateSolidBrush(color);
	}

	if (bRepaint && GetSafeHwnd() != NULL)
	{
		Invalidate();
		UpdateWindow();
	}
}