﻿//*****************************************************************************
// Filename	: 	Def_TestItem.h
// Created	:	2016/12/30 - 12:31
// Modified	:	2016/12/30 - 12:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_TestItem_h__
#define Def_TestItem_h__

#pragma once

#include <afxwin.h>
#include "Def_Current.h"
#include "Def_CenterPoint.h"
#include "Def_PatternNoise.h"
#include "Def_Rotate.h"
#include "Def_EIAJ.h"
#include "Def_Particle.h"
#include "Def_SFR.h"

typedef enum enLT_TestItem_ResultCode
{
	TIRC_Fail	= 0,
	TIRC_Pass	= 1,
	
};

// 검사 항목 ID 
typedef enum enLT_TestItem_ID
{
	TIID_Base	= 0,
	TIID_TestInitialize,	// loading
	TIID_TestFinalize,		// unloading
	TIID_Vision,			// 비전
	TIID_Displace,			// 변위
	TIID_AFPosition,		// 전류 측정
	TIID_Current,			// 전류 측정
	TIID_CenterPointAdj,	// 광축 조정
	TIID_PatternNoise,		// 저조도 검사
	TIDI_SteeringLinkage,	// 조향연동 측정
	TIID_WarningPhrase,		// 경고문구 검사	
	TIID_Particle,			// 이물 검사
	TIID_IR_Filter,			// IR Filter
	TIID_VideoSignal,		// 영상 신호 검사
	TIID_Rotation,			// Rotation
	TIID_EIAJ,				// EIAJ
	TIID_SFR,				// SFR
	TIID_FOV,				// 화각
	TIID_ImageReversal,		// 좌우/상하 반전
	TIID_BrightnessRatio,	// 주변 광량비
	TIID_Color,				// 컬러
	TIID_ActiveAlgin,		// 조정
	TIID_MaxEnum,
};

static LPCTSTR g_szLT_TestItem_Name[] =
{
	_T("Base"),
	_T("Test Initialize"),
	_T("Test Finalize"),
	_T("Vision"),				// 비전
	_T("Displace"),				// 변위
	_T("Focusing Position"),	// 포커싱 위치
	_T("Current"),				// 전류 측정
	_T("Center Point"),			// 광축 조정
	_T("Pattern Noise"),		// 저조도 검사
	_T("Steering Linkage"),		// 조향연동 측정
	_T("Warning Phrase"),		// 경고문구 검사	
	_T("Particle"),				// 이물 검사
	_T("IR Filter"),			// IR Filter
	_T("Video Signal"),			// 영상 신호 검사
	_T("Rotation"),				// Rotation
	_T("Focus EIAJ"),			// EIAJ
	_T("Focus SFR"),			// SFR
	_T("FOV"),					// 화각
	_T("Image Reversal"),		// 좌우/상하 반전
	_T("Brightness Ratio"),		// 주변 광량비
	_T("Color"),				// 컬러
	_T("Focus & CenterPoint"),	// 조정
	NULL
};

typedef struct _tag_LT_TestItem
{
	enLT_TestItem_ID	m_nTestItem_ID;		// 테스트 항목 고유 ID
	CString				m_szTestItemName;	// 테스트 항목 명칭
	CString				m_szTestItemDesc;	// 테스트 항목 설명
	LONG				m_lResult;			// 테스트 결과 코드

	_tag_LT_TestItem()
	{
		m_nTestItem_ID	= TIID_Base;
		m_lResult		= -1;
	};

	_tag_LT_TestItem& operator= (_tag_LT_TestItem& ref)
	{
		m_nTestItem_ID		= ref.m_nTestItem_ID;
		m_szTestItemName	= ref.m_szTestItemName;
		m_szTestItemDesc	= ref.m_szTestItemDesc;
		m_lResult			= ref.m_lResult;

		return *this;
	}
}ST_LT_TestItem, *PST_LT_TestItem;

typedef struct _tag_LT_TI_Current : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_Current_Op stCurrentOp[CuOp_ItemNum];
	// 측정 데이터
	ST_Current_Data stCurrentData;


	_tag_LT_TI_Current()
	{
		m_szTestItemName = _T("Current");
	};

	void reset()
	{
		stCurrentData.reset();
	};

	_tag_LT_TI_Current& operator= (_tag_LT_TI_Current& ref)
	{
		__super::operator=(ref);

		stCurrentData = ref.stCurrentData;

		for (UINT nCh = 0; nCh < CuOp_ItemNum; nCh++)
		{
			stCurrentOp[nCh]	= ref.stCurrentOp[nCh];
		}

		return *this;
	};
}ST_LT_TI_Current, *PST_LT_TI_Current;

typedef struct _tag_LT_TI_CenterPoint : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_CenterPoint_Op stCenterPointOp;
	// 측정 데이터
	ST_CenterPoint_Data stCenterPointData;


	_tag_LT_TI_CenterPoint()
	{
		m_szTestItemName = _T("CenterPoint");
	};

	void reset(){
		stCenterPointData.reset();
	};

	_tag_LT_TI_CenterPoint& operator= (_tag_LT_TI_CenterPoint& ref)
	{
		__super::operator=(ref);
		stCenterPointOp = ref.stCenterPointOp;
		stCenterPointData = ref.stCenterPointData;
		return *this;
	};
}ST_LT_TI_CenterPoint, *PST_LT_TI_CenterPoint;

typedef struct _tag_LT_TI_CenterPoint_4CH : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_CenterPoint_Op stCenterPointOp;
	ST_CPRect_Op	  stCenterRectOp;
	ST_CPChart_Op	  stCPChartOp[4];
	// 측정 데이터
	ST_CenterPoint_Data stCenterPointData[4];


	_tag_LT_TI_CenterPoint_4CH()
	{
		m_szTestItemName = _T("CenterPoint");
	};

	void reset(){
		for (int t = 0; t < 4; t++)
		{
			stCenterPointData[t].reset();
		}
	};

	_tag_LT_TI_CenterPoint_4CH& operator= (_tag_LT_TI_CenterPoint_4CH& ref)
	{
		__super::operator=(ref);
		stCenterPointOp = ref.stCenterPointOp;
		for (int t = 0; t < 4; t++)
		{
			stCPChartOp[t] = ref.stCPChartOp[t];
			stCenterPointData[t] = ref.stCenterPointData[t];
		}
		stCenterRectOp = ref.stCenterRectOp;
		return *this;
	};
}ST_LT_TI_CenterPoint_4CH, *PST_LT_TI_CenterPoint_4CH;

typedef struct _tag_LT_TI_PatternNoise_4CH : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_PatternNoise_Op stPatternNoiseOp;
	// 측정 데이터
	ST_PatternNoise_Data stPatternNoiseData[4];


	_tag_LT_TI_PatternNoise_4CH()
	{
		m_szTestItemName = _T("PatternNoise");
	};

	void reset(){
		for (int t = 0; t < 4; t++)
		{
			stPatternNoiseData[t].reset();
		}
	};

	_tag_LT_TI_PatternNoise_4CH& operator= (_tag_LT_TI_PatternNoise_4CH& ref)
	{
		__super::operator=(ref);
		stPatternNoiseOp = ref.stPatternNoiseOp;
		for (int t = 0; t < 4; t++)
		{
			stPatternNoiseData[t] = ref.stPatternNoiseData[t];
		}
		return *this;
	};
}ST_LT_TI_PatternNoise_4CH, *PST_LT_TI_PatternNoise_4CH;

typedef struct _tag_LT_TI_Rotate : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_Rotate_Op stRotateOp;
	// 측정 데이터
	ST_Rotate_Data stRotateData;


	_tag_LT_TI_Rotate()
	{
		m_szTestItemName = _T("Rotate");
	};

	void reset()
	{
		stRotateData.reset();
	};

	_tag_LT_TI_Rotate& operator= (_tag_LT_TI_Rotate& ref)
	{
		__super::operator=(ref);
		stRotateOp	 = ref.stRotateOp;
		stRotateData = ref.stRotateData;
		return *this;
	};

}ST_LT_TI_Rotate, *PST_LT_TI_Rotate;

typedef struct _tag_LT_TI_EIAJ : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_EIAJ_Op		stEIAJOp;
	// 측정 데이터
	ST_EIAJ_Data	stEIAJData;

	_tag_LT_TI_EIAJ()
	{
		m_szTestItemName = _T("EIAJ");
	};

	void reset()
	{
		stEIAJData.reset();
	};

	_tag_LT_TI_EIAJ& operator= (_tag_LT_TI_EIAJ& ref)
	{
		__super::operator=(ref);
		stEIAJOp		= ref.stEIAJOp;
		stEIAJData		= ref.stEIAJData;
		return *this;
	};

}ST_LT_TI_EIAJ, *PST_LT_TI_EIAJ;

typedef struct _tag_LT_TI_SFR : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_SFR_Op		stSFROp;
	// 측정 데이터
	ST_SFR_Data		stSFRData;

	
	_tag_LT_TI_SFR()
	{
		m_szTestItemName = _T("SFR");
	};

	void reset()
	{
		stSFRData.reset();
		
	};

	_tag_LT_TI_SFR& operator= (_tag_LT_TI_SFR& ref)
	{
		__super::operator=(ref);
		stSFROp = ref.stSFROp;
		stSFRData = ref.stSFRData;
		
		return *this;
	};

}ST_LT_TI_SFR, *PST_LT_TI_SFR;


typedef struct _tag_LT_TI_Particle : public ST_LT_TestItem
{
	// 검사 기준 데이터
	ST_Particle_Op stParticleOp;
	// 측정 데이터
	ST_Particle_Data stParticleData;

	_tag_LT_TI_Particle()
	{
		m_szTestItemName = _T("Particle");
	};

	void reset()
	{
		stParticleData.reset();
	};

	_tag_LT_TI_Particle& operator= (_tag_LT_TI_Particle& ref)
	{
		__super::operator=(ref);
		stParticleOp = ref.stParticleOp;
		stParticleData = ref.stParticleData;
		return *this;
	};

}ST_LT_TI_Particle, *PST_LT_TI_Particle;

#endif // Def_TestItem_h__
