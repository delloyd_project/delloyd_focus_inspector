﻿#ifndef Def_Particle_h__
#define Def_Particle_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

typedef enum enPaticleRegion
{
	Particle_Region_Center = 0,
	Particle_Region_Middle,
	Particle_Region_Side,
	Particle_Region_MaxEnum,
};

static LPCTSTR g_szPaticleRegion[] =
{
	_T("Center"),
	_T("Middle"),
	_T("Side"),
	NULL
};


typedef struct _tag_RegionParticle
{
	// 영역이 사각형인지, 원형인지
	BOOL	bEllipse;
	
	// 영역
	CRect	RegionList;

	// 이물 검사 Threshold
	double  dbThreshold;

	// 멍 이물 농도
	double	dbBruiseConc;

	// 멍 이물 크기
	int		iBruiseSize;

	_tag_RegionParticle()
	{
		Reset();
	};

	void Reset()
	{
		bEllipse			= FALSE;
		RegionList.left		= 100;
		RegionList.top		= 100;
		RegionList.right	= CAM_IMAGE_WIDTH - 100;
		RegionList.bottom	= CAM_IMAGE_HEIGHT - 100;
		dbThreshold			= 10;
		dbBruiseConc		= 2.0;
		iBruiseSize			= 15;
	};

	void _Rect_Position_Sum(int X, int Y, int W, int H)
	{
		RegionList.top		= Y - (int)(H / 2);
		RegionList.bottom	= RegionList.top + H;
		RegionList.left		= X - (int)(W / 2);
		RegionList.right	= RegionList.left + W;
	};

	void _Rect_Position_Sum2(int W, int H)
	{
		CRect data		= RegionList;
		int posX		= data.CenterPoint().x;
		int posY		= data.CenterPoint().y;
		int halfX		= W / 2;
		int halfY		= H / 2;

		if (W % 2 == 0)
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX;
		}
		else
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX + 1;
		}

		if (H % 2 == 0)
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY;
		}
		else
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY + 1;
		}
	};

	_tag_RegionParticle& operator= (_tag_RegionParticle& ref)
	{
		RegionList		= ref.RegionList;
		bEllipse		= ref.bEllipse;
		dbThreshold		= ref.dbThreshold;
		dbBruiseConc	= ref.dbBruiseConc;
		iBruiseSize		= ref.iBruiseSize;

		return *this;
	};

}ST_RegionParticle, *PST_ZoneParticle;

typedef struct _tag_ResultRegion
{
	CRect	RegionList;
	double	dbresult;
	double	dbStainBlemish;

	_tag_ResultRegion()
	{
		dbresult		= 0.0;
		dbStainBlemish	= 0.0;

		Reset();
	};

	void Reset()
	{
		RegionList.SetRectEmpty();
	};

	_tag_ResultRegion& operator= (_tag_ResultRegion& ref)
	{
		dbresult		= ref.dbresult;
		dbStainBlemish	= ref.dbStainBlemish;
		RegionList		= ref.RegionList;

		return *this;
	};

	void _Rect_Position_Sum(int X, int Y, int W, int H)
	{
		RegionList.top		= Y - (int)(H / 2);
		RegionList.bottom	= RegionList.top + H - 1;
		RegionList.left		= X - (int)(W / 2);
		RegionList.right	= RegionList.left + W - 1;
	};

	void _Rect_Position_Sum2(int W, int H)
	{
		CRect data	 = RegionList;
		int posX	 = data.CenterPoint().x;
		int posY	 = data.CenterPoint().y;

		int halfX	 = W / 2;
		int halfY	 = H / 2;

		if (W % 2 == 0)
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX;
		}
		else
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX + 1;
		}

		if (H % 2 == 0)
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY;
		}
		else
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY + 1;
		}
	};

}ST_ResultRegion, *PST_ResultRegion;

typedef struct _tag_Particlee_Op
{
	// 불량 영역 체크
	BOOL	bFailCheck;

	// 노이즈 민감도
	int		iDustDis;

	// 이물 검사 영역 구조체
	ST_RegionParticle	rectData[Particle_Region_MaxEnum];

	_tag_Particlee_Op()
	{
		bFailCheck			= FALSE;
		iDustDis			= 2;
	};

	void Reset()
	{
		bFailCheck	 = FALSE;
		iDustDis	 = 2;
	};

	/*변수 교환 함수*/
	_tag_Particlee_Op& operator= (_tag_Particlee_Op& ref)
	{
		bFailCheck			= ref.bFailCheck;
		iDustDis			= ref.iDustDis;

		return *this;
	};

}ST_Particle_Op, *PST_Particle_Op;

typedef struct _tag_Particle_Data
{
	UINT nResult;

	// 검출된 이물 갯수
	int   iResultcnt;
	
	//검출된 이물 영역 구조체
	ST_ResultRegion ErrRegionList[UCHAR_MAX];

	/*변수 초기화 함수*/
	_tag_Particle_Data()
	{
		nResult		= TR_Init;
		iResultcnt = 0;

		for (UINT nIdx = 0; nIdx < UCHAR_MAX; nIdx++)
			ErrRegionList[nIdx].Reset();
	};

	void reset()
	{
		nResult		= TR_Init;
		iResultcnt = 0;

		for (UINT nIdx = 0; nIdx < UCHAR_MAX; nIdx++)
			ErrRegionList[nIdx].Reset();
	};

	/*변수 교환 함수*/
	_tag_Particle_Data& operator= (_tag_Particle_Data& ref)
	{
		nResult		= ref.nResult;
		iResultcnt = ref.iResultcnt;

		for (UINT nIdx = 0; nIdx < UCHAR_MAX; nIdx++)
			ErrRegionList[nIdx] = ref.ErrRegionList[nIdx];

		return *this;
	};

}ST_Particle_Data, *PST_Particle_Data;

#endif // Def_Particle_h__

