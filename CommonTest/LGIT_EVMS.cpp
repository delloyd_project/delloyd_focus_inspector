//*****************************************************************************
// Filename	: 	LGIT_EVMS.cpp
// Created	:	2016/11/8 - 16:29
// Modified	:	2016/11/8 - 16:29
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "LGIT_EVMS.h"
#include "CommonFunction.h"

CLGIT_EVMS::CLGIT_EVMS()
{
}


CLGIT_EVMS::~CLGIT_EVMS()
{
}

//=============================================================================
// Method		: Conv_SYSTEMTIME2String
// Access		: protected  
// Returns		: CString
// Parameter	: __in SYSTEMTIME * pTime
// Qualifier	:
// Last Update	: 2016/11/11 - 3:35
// Desc.		:
//=============================================================================
CString CLGIT_EVMS::Conv_SYSTEMTIME2String(__in SYSTEMTIME* pTime)
{
	CString szTime;
	// 20130218043404

	szTime.Format(_T("%04d%02d%02d%02d%02d%02d"), pTime->wYear, pTime->wMonth, pTime->wDay, pTime->wHour, pTime->wMinute, pTime->wSecond);

	return szTime;
}

//=============================================================================
// Method		: Conv_FileVer2EVMSVer
// Access		: public  
// Returns		: CString
// Qualifier	:
// Last Update	: 2016/11/11 - 3:27
// Desc.		:
//=============================================================================
CString CLGIT_EVMS::Conv_FileVer2EVMSVer()
{
	CString szEVMSVersion;
	CString szText;
	CString szTemp;
	szText = GetVersionInfo(_T("FileVersion"));

	//2016.9.18.0 -> 16090900
	CString resToken;
	int curPos = 0;
	int iValue = 0;

	// 연도 
	resToken = szText.Tokenize(_T("."), curPos);
	if (resToken != _T(""))
	{
		szEVMSVersion = resToken.Right(2);
		resToken = szText.Tokenize(_T("."), curPos);

		// 달 
		if (resToken != _T(""))
		{
			iValue = _ttoi(resToken.GetBuffer());
			szTemp.Format(_T("%02d"), iValue);
			szEVMSVersion += szTemp;
			resToken = szText.Tokenize(_T("."), curPos);

			// 일 
			if (resToken != _T(""))
			{
				iValue = _ttoi(resToken.GetBuffer());
				szTemp.Format(_T("%02d"), iValue);
				szEVMSVersion += szTemp;
				resToken = szText.Tokenize(_T("."), curPos);

				// 일련번호 
				if (resToken != _T(""))
				{
					iValue = _ttoi(resToken.GetBuffer());
					iValue = (99 < iValue) ? 99 : iValue;
					szTemp.Format(_T("%02d"), iValue);
					szEVMSVersion += szTemp;
					resToken = szText.Tokenize(_T("."), curPos);

					return szEVMSVersion;
				}
			}
		}
	}

	szEVMSVersion.Empty();
	return szEVMSVersion;
}

//=============================================================================
// Method		: VerifyDirectory
// Access		: public  
// Returns		: BOOL
// Parameter	: __in enEVMS_PGM_Type nPgmType
// Qualifier	:
// Last Update	: 2016/11/11 - 3:48
// Desc.		:
//=============================================================================
BOOL CLGIT_EVMS::VerifyDirectory(__in enEVMS_PGM_Type nPgmType)
{
	TCHAR szExePath[MAX_PATH] = { 0, };
	GetModuleFileName(NULL, szExePath, MAX_PATH);

 	TCHAR drive[_MAX_DRIVE];
 	TCHAR dir[_MAX_DIR];
 	TCHAR file[_MAX_FNAME];
 	TCHAR ext[_MAX_EXT];
 	_tsplitpath_s(szExePath, drive, _MAX_DRIVE, dir, _MAX_DIR, file, _MAX_FNAME, ext, _MAX_EXT);
 
 	CString szProgram;
 	szProgram.Format(_T("%s%s"), drive, dir);

	return VerifyDirectory(nPgmType, szProgram);
}

//=============================================================================
// Method		: VerifyDirectory
// Access		: public  
// Returns		: BOOL
// Parameter	: __in enEVMS_PGM_Type nPgmType
// Parameter	: __in LPCTSTR szDirectory
// Qualifier	:
// Last Update	: 2017/9/18 - 17:30
// Desc.		:
//=============================================================================
BOOL CLGIT_EVMS::VerifyDirectory(__in enEVMS_PGM_Type nPgmType, __in LPCTSTR szDirectory)
{
	CString szPgmType;
	CString	szVerifyDir;
	CString szCheckDir = szDirectory;

	if (EVMS_PGM_TP == nPgmType)
	{
		// D:\EVMS\TP(OP)\EXE 에서만 실행 File 동작 할 것 ( Zip File 형태로 제공 )
		// D:\EVMS\TP(OP)\ENV 환경 File은 모델 별/설비 종류별 Link 될 것

		szPgmType = EVMS_DIR_TP;
		//szVerifyDir.Format(_T("D:\\EVMS\\TP\\EXE"));
		szVerifyDir = EVMS_PATH_EXE_TP;
	}
	else if (EVMS_PGM_OP == nPgmType)
	{
		// D:\EVMS\TP(OP)\EXE 에서만 실행 File 동작 할 것 ( Zip File 형태로 제공 )
		// D:\EVMS\TP(OP)\ENV 환경 File은 모델 별/설비 종류별 Link 될 것

		szPgmType = EVMS_DIR_OP;
		//szVerifyDir.Format(_T("D:\\EVMS\\OP\\EXE"));
		szVerifyDir = EVMS_PATH_EXE_OP;
	}
	else
	{
		return FALSE;
	}

	if (0 <= szCheckDir.Find(szVerifyDir))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}
