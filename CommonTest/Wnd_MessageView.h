﻿//*****************************************************************************
// Filename	: 	Wnd_MessageView.h
// Created	:	2016/3/11 - 14:52
// Modified	:	2016/3/11 - 14:52
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_MessageView_h__
#define Wnd_MessageView_h__

#pragma once

#include "VGStatic.h"
#include "resource.h"

//-----------------------------------------------------------------------------
// CWnd_MessageView
//-----------------------------------------------------------------------------
class CWnd_MessageView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MessageView)

public:
	CWnd_MessageView();
	virtual ~CWnd_MessageView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnGetMinMaxInfo			(MINMAXINFO* lpMMI);
	afx_msg void	OnBnClickedOK			();
	afx_msg void	OnBnClickedCancel		();

	void			CreateTimerQueue_Mon();
	void            DeleteTimerQueue_Mon();

	static VOID CALLBACK TimerRoutine_FixCheck	(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);

	CBrush				m_brBkgr;

	CFont				m_Font;

	CVGStatic			m_st_Message;

	CButton				m_bn_OK;
	CButton				m_bn_Cancel;

	BOOL				m_bResult;
	BOOL				m_bMode;

	HANDLE				m_hTimer_FixCheck;
	HANDLE				m_hTimerQueue_FixCheck;

public:	

	BOOL				*m_pbFixStatus;

	BOOL	DoModal							(BOOL bMode, BOOL bFixCheckMode = FALSE);
	void	SetBackgroundColor				(COLORREF color, BOOL bRepaint = TRUE);
	void	SetWarringMessage				(CString szText);

};

#endif // Wnd_MessageView_h__


