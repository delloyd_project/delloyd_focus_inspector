﻿//*****************************************************************************
// Filename	: 	IF_Illumination.h
// Created	:	2016/03/09
// Modified	:	2016/05/09
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef IF_Illumination_h__
#define IF_Illumination_h__

#pragma once
#include "SerialCom_Base.h"
#include "Define_IF_Illumination.h"

using namespace IF_IlluminationBrd;

//=============================================================================
//
//=============================================================================
class CIF_Illumination : public CSerialCom_Base
{
public:
	CIF_Illumination();
	virtual ~CIF_Illumination();

	//---------------------------------------------------------
	// 재정의
	//---------------------------------------------------------
protected:
	//virtual void	OnEvent					(EEvent eEvent, EError eError);
	virtual LRESULT	OnFilterRecvData		(const char* szACK, DWORD dwAckSize);
	virtual void	OnRecvProtocol			(const char* szACK, DWORD dwAckSize);

	//-----------------------------------------------------
	// 통신 모듈
	//-----------------------------------------------------
 	ST_IF_IlluminationBrdProtocol		m_stProtocol;
 	ST_IF_IlluminationBrdRecvProtocol	m_stRecvProtocol;	

	void			ResetProtocol();

public:

	ST_IF_IlluminationBrdRecvProtocol	GetAckProtocol()
	{
		return m_stRecvProtocol;
	};

	BOOL Send_SetBrightness(__in UINT nCH, __in UINT nBrightness);


	BOOL Send_SetOnOff(__in UINT nCH, __in BOOL bMode);
	BOOL Send_GetBrightness(__in UINT nCH, __out UINT& nOutBrightness);
	BOOL Send_GetOnOff(__in UINT nCH, __out BOOL& bOutOnOff);
};

#endif // IF_Illumination_h__
