﻿//*****************************************************************************
// Filename	: 	Define_CamBrd.h
// Created	:	2016/3/8 - 13:31
// Modified	:	2016/3/8 - 13:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Define_CamBrd_h__
#define Define_CamBrd_h__

//=============================================================================
// LGIT - Camera Board 통신 프로토콜
//=============================================================================
/*
Transmit from host
<STX><CMD><DATA><ETX>

Response from host
<STX><CMD><DATA><ETX>

Where:
<STX>		= 1 ASCII '!'
<CMD>		= 1 ASCII character
<DATA>		= Command Argument, up to 9 ASCII characters.
<ETX>		= 1 ASCII '@'
*/
namespace LGIT_CamBrd
{

#define	CamBrd_STX				0x21	//'!'
#define	CamBrd_ETX				0x40	//'@'
#define CamBrd_DummyChar		0x30	//'0'
#define CamBrd_ProtoLength		11
#define CamBrd_DataLength		8
#define CamBrd_CH_DataLength	4
#define CamBrd_Channel			1

	// 고정된 통신 프로토콜 선언
	static const char*	g_szCamBrd_BoardCheck		= "!000000000@";
	static const char*	g_szCamBrd_GetInCurrent		= "!400000000@";

	// 프로토콜 커맨드 
	typedef enum enCamBrdCmd
	{
		CMD_BoardCheck		= '0',
		CMD_SetVoltage		= 'V',
		CMD_GetInCurrent	= '4',
		CMD_OverCurrent		= 'D',
	};

	typedef struct _tag_CamBrdCurrent
	{
		long lOutCurrent;

		_tag_CamBrdCurrent()
		{
			lOutCurrent = FALSE;
		}

	}ST_CamBrdCurrent, *PST_CamBrdCurrent;

	typedef struct _tag_CamBrdProtocol
	{
		char		STX;
		char		CMD;
		char		Data[CamBrd_DataLength];
		char		ETX;

		_tag_CamBrdProtocol()
		{
			STX = CamBrd_STX;
			CMD = CamBrd_DummyChar;
			ETX = CamBrd_ETX;
			memset(Data, CamBrd_DummyChar, CamBrd_DataLength);
		};

		void MakeProtocol_SetVolt(__in float fVolt)
		{
			CMD = CMD_SetVoltage;

			memset(Data, CamBrd_DummyChar, CamBrd_DataLength);

			CStringA szBuf;
			UINT nOffset = 0;
			szBuf.Format("%03.0f", fVolt * 10.0f);
			memcpy(&Data[nOffset], szBuf.GetBuffer(0), 3);


			//kdy
			/*if (fVolt == 0)
			{
				memset(Data, CamBrd_DummyChar, CamBrd_DataLength);
			} 
			else
			{
				memset(Data, CamBrd_DummyChar, CamBrd_DataLength);

				sprintf_s(Data, 4, "%03.0f", fVolt * 10.0f);
				Data[3] = CamBrd_DummyChar;
			}*/
			
		};

	}ST_CamBrdProtocol, *PST_CamBrdProtocol;

	typedef struct _tag_CamBrdRecvProtocol
	{
		char		STX;
		char		CMD;
		CStringA	Data;
		char		ETX;

		CStringA	Protocol;

		int			iCurrent[2];
		BOOL		nOverCurrent;
		BOOL		bResult;

		_tag_CamBrdRecvProtocol()
		{
			for (int i = 0; i < CamBrd_Channel; i++)
				iCurrent[i] = 0;
			nOverCurrent	= FALSE;
		};

		_tag_CamBrdRecvProtocol& operator= (_tag_CamBrdRecvProtocol& ref)
		{
			STX = ref.STX;
			CMD = ref.CMD;
			Data = ref.Data;
			ETX = ref.ETX;

			Protocol = ref.Protocol;

			return *this;
		};

		BOOL SetRecvProtocol(__in const char* pszProtocol, __in UINT_PTR nLength)
		{
			if (NULL == pszProtocol)
				return FALSE;

			if (nLength != CamBrd_ProtoLength)
				return FALSE;

			CStringA strProtocol = pszProtocol;

			INT_PTR nDataLength = nLength - 3; // STX, ETX, CMD
			INT_PTR nOffset = 0;

			STX  = pszProtocol[nOffset++];
			CMD  = pszProtocol[nOffset++];
			Data = strProtocol.Mid((int)nOffset++, (int)nDataLength);
			ETX  = pszProtocol[nOffset];

			switch (CMD)
			{
			case CMD_BoardCheck:
				AnalyzeData_BoardCheck();
				break;

			case CMD_SetVoltage:
				AnalyzeData_SetVolt();
				break;

			case CMD_GetInCurrent:
				AnalyzeData_Current();
				break;

			case CMD_OverCurrent:
				AnalyzeData_OverCurrent();
				break;
	
			default:
				break;
			}	

			// Check STX
			if (CamBrd_STX != STX)
				return FALSE;

			// Check ETX
			if (CamBrd_ETX != ETX)
				return FALSE;

			return TRUE;
		};

		void AnalyzeData_BoardCheck()
		{
			Data.ReleaseBuffer();
		};

		void AnalyzeData_SetVolt()
		{
			Data.ReleaseBuffer();
		};

		void AnalyzeData_Current()
		{
			/*CStringA szBuff;

			szBuff = Data.Mid(0, 4);
			iCurrent = atoi(szBuff);*/

			CStringA szBuff;

			for (int i = 0; i < CamBrd_Channel; i++)
			{
				szBuff = Data.Mid(i * CamBrd_CH_DataLength, 4);
				iCurrent[i] = atoi(szBuff);
			}
		};

		void AnalyzeData_OverCurrent()
		{
			CStringA szBuff;

			szBuff = Data.Left(1);

			if (1 == atoi(szBuff))
				nOverCurrent = TRUE;
			else
				nOverCurrent = FALSE;
			Data.ReleaseBuffer();
		};

	}ST_CamBrdRecvProtocol, *PST_CamBrdRecvProtocol;

};

#endif // Define_CamBrd_h__
