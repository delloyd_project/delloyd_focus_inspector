﻿//*****************************************************************************
// Filename	: 	PCBCamBrd.h
// Created	:	2016/03/09
// Modified	:	2016/05/09
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef PCBCamBrd_h__
#define PCBCamBrd_h__

#pragma once
#include "SerialCom_Base.h"
#include "Define_PCBCamBrd.h"

using namespace LGIT_CamBrd;

//=============================================================================
//
//=============================================================================
class CPCBCamBrd : public CSerialCom_Base
{
public:
	CPCBCamBrd();
	virtual ~CPCBCamBrd();

	//---------------------------------------------------------
	// 재정의
	//---------------------------------------------------------
protected:
	//virtual void	OnEvent					(EEvent eEvent, EError eError);
	virtual LRESULT	OnFilterRecvData		(const char* szACK, DWORD dwAckSize);
	virtual void	OnRecvProtocol			(const char* szACK, DWORD dwAckSize);

	//-----------------------------------------------------
	// 통신 모듈
	//-----------------------------------------------------
	ST_CamBrdProtocol		m_stProtocol;
	ST_CamBrdRecvProtocol	m_stRecvProtocol;	

	void			ResetProtocol();

public:

	ST_CamBrdRecvProtocol	GetAckProtocol()
	{
		return m_stRecvProtocol;
	};

	// 공용
	BOOL			Send_BoardCheck			(__out BYTE& byBrdNo);
	BOOL			Send_SetVolt			(__in float fVolt);
	BOOL			Send_GetCurrent			(__out double* iOutCurrent);
};

#endif // PCBCamBrd_h__
