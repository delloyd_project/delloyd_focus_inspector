﻿//*****************************************************************************
// Filename	: 	File_Motor
// Created	:	2016/9/23 - 13:04
// Modified	:	2016/9/23 - 13:04
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef File_Moter_h__
#define File_Moter_h__

#pragma once
#include "Def_Motor.h"

class CFile_Motor
{
public:
	CFile_Motor();
	~CFile_Motor();

	BOOL	LoadMotionParamFiles	(__in LPCTSTR szPath, __out ST_AllMotorData& stMotionInfo);
	BOOL	SaveMotionParamFiles	(__in LPCTSTR szPath, __in const ST_AllMotorData* pstMotionInfo);

	BOOL	LoadMotionSequenceFiles	(__in LPCTSTR szPath, __out ST_AllMotorData& stMotionInfo);
	BOOL	SaveMotionSequenceFiles	(__in LPCTSTR szPath, __in const ST_AllMotorData* pstMotionInfo);

private:

	UINT	m_nMaxAxis;
};

#endif // File_Motor_h__
