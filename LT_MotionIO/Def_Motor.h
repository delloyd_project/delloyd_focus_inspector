﻿//*****************************************************************************
// Filename	: 	Define_Motor
// Created	:	2016/9/23 - 13:01
// Modified	:	2016/9/23 - 13:01
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Motor_h__
#define Def_Motor_h__

#define UP						TRUE
#define DN						FALSE

#define ON						TRUE
#define OFF						FALSE

#define FIX						TRUE
#define UNFIX					FALSE

#define Initial					TRUE
#define Finalize				FALSE

#define Foward					TRUE
#define Reverse					FALSE

#define AXL_DEFAULT_IRQNO		7
#define DEF_MOTION_SEQ_MAX		10
#define DEF_SEQUENCE_STEP_MAX	10
#define DEF_MULTI_AXIS_CTRL		2


//=============================================================================
// Motor Axis Name
//=============================================================================
typedef enum enAXIS_NUM
{
	MotorAxisZ = 0,
	MotorAxisX,
	MotorAxisY,
	MotorAxisR,
	MotorAxisNum							// Total Motor Number
};


//=============================================================================
// Motor Param 구조체
//=============================================================================
typedef struct _tag_MotionParam
{
	double dbMaxVel;
	double dbVel;
	double dbAcc;
	double dbOriginAccFirst;
	double dbOriginAccSecond;
	double dbDec;
	double dbCoarseVel;
	double dbFineVel;
	double dbOriginVelFirst;
	double dbOriginVelSecond;
	double dbOriginVelThird;
	double dbOriginVelLast;
	double dbNegLimit;
	double dbPosLimit;
	double dbBufNegLimit;
	double dbBufPosLimit;
	double dbNumerator;
	double dbDenominator;
	double dbnit;
	double dbPulse;
	double dbGearRatio;
	double dbOriginOffset;
	double dbGantryOffset;
	double dbOffsetRange;
	double dbEncoderMethod;
	double dbCoordDirection;
	double dbMotorType;
	double dbAxisStatus;
	double dbEncoderType;
	double dbStepMotorSmoothing;
	double dbVelPulse;
	double dbMotorAmpEnableLevel;
	double dbMotorAmpResetLevel;
	double dbMotorAmpFaultLevel;
	double dbMotorNegaLimitLevel;
	double dbMotorPosiLimitLevel;
	double dbGantrySelect;
	double dbStartSpeed;
	double dbStopLevel;

	int iAxisUse;
	int	iAxisNum;
	int iAxisServoLevel;
	int iAxisInpositionLevel;
	int iAxisAlarmLevel;
	int iAxisHomeSenLevel;
	int iAxisHomeSig;
	int iAxisOutputMethod;
	int iAxisNegLimitSenLevel;
	int iAxisPosLimitSenLevel;
	int iAxisHomeDir;
	int iAxisEncInputMethod;
	int iAxisEmergencyLevel;

	CString szAxisName;

	_tag_MotionParam()
	{
		dbMaxVel				= 0.0;
		dbVel					= 0.0;
		dbAcc					= 0.0;
		dbOriginAccFirst		= 0.0;
		dbOriginAccSecond		= 0.0;
		dbDec					= 0.0;
		dbCoarseVel				= 0.0;
		dbFineVel				= 0.0;
		dbOriginVelFirst		= 0.0;
		dbOriginVelSecond		= 0.0;
		dbOriginVelThird		= 0.0;
		dbOriginVelLast			= 0.0;
		dbNegLimit				= 0.0;
		dbPosLimit				= 0.0;
		dbBufNegLimit			= 0.0;
		dbBufPosLimit			= 0.0;
		dbNumerator				= 0.0;
		dbDenominator			= 0.0;
		dbnit					= 0.0;
		dbPulse					= 0.0;
		dbGearRatio				= 0.0;
		dbOriginOffset			= 0.0;
		dbGantryOffset			= 0.0;
		dbOffsetRange			= 0.0;
		dbEncoderMethod			= 0.0;
		dbCoordDirection		= 0.0;
		dbMotorType				= 0.0;
		dbAxisStatus			= 0.0;
		dbEncoderType			= 0.0;
		dbStepMotorSmoothing	= 0.0;
		dbVelPulse				= 0.0;
		dbMotorAmpEnableLevel	= 0.0;
		dbMotorAmpResetLevel	= 0.0;
		dbMotorAmpFaultLevel	= 0.0;
		dbMotorNegaLimitLevel	= 0.0;
		dbMotorPosiLimitLevel	= 0.0;
		dbGantrySelect			= 0.0;
		dbStartSpeed			= 0.0;
		dbStopLevel				= 0.0;

		iAxisServoLevel			= 0;
		iAxisInpositionLevel	= 0;
		iAxisNum				= 0;
		iAxisAlarmLevel			= 0;
		iAxisHomeSenLevel		= 0;
		iAxisHomeSig			= 0;
		iAxisOutputMethod		= 0;
		iAxisPosLimitSenLevel	= 0;
		iAxisNegLimitSenLevel	= 0;
		iAxisHomeDir			= 0;
		iAxisEncInputMethod		= 0;
		iAxisEmergencyLevel		= 0;
		iAxisUse				= 0;

		szAxisName.Empty();
	}

	_tag_MotionParam& operator= (_tag_MotionParam& ref)
	{
		dbMaxVel				= ref.dbMaxVel;
		dbVel					= ref.dbVel;
		dbAcc					= ref.dbAcc;
		dbOriginAccFirst		= ref.dbOriginAccFirst;
		dbOriginAccSecond		= ref.dbOriginAccSecond;
		dbDec					= ref.dbDec;
		dbCoarseVel				= ref.dbCoarseVel;
		dbFineVel				= ref.dbFineVel;
		dbOriginVelFirst		= ref.dbOriginVelFirst;
		dbOriginVelSecond		= ref.dbOriginVelSecond;
		dbOriginVelThird		= ref.dbOriginVelThird;
		dbOriginVelLast			= ref.dbOriginVelLast;
		dbNegLimit				= ref.dbNegLimit;
		dbPosLimit				= ref.dbPosLimit;
		dbBufNegLimit			= ref.dbBufNegLimit;
		dbBufPosLimit			= ref.dbBufPosLimit;
		dbNumerator				= ref.dbNumerator;
		dbDenominator			= ref.dbDenominator;
		dbnit					= ref.dbnit;
		dbPulse					= ref.dbPulse;
		dbGearRatio				= ref.dbGearRatio;
		dbOriginOffset			= ref.dbOriginOffset;
		dbGantryOffset			= ref.dbGantryOffset;
		dbOffsetRange			= ref.dbOffsetRange;
		dbEncoderMethod			= ref.dbEncoderMethod;
		dbCoordDirection		= ref.dbCoordDirection;
		dbMotorType				= ref.dbMotorType;
		dbAxisStatus			= ref.dbAxisStatus;
		dbEncoderType			= ref.dbEncoderType;
		dbStepMotorSmoothing	= ref.dbStepMotorSmoothing;
		dbVelPulse				= ref.dbVelPulse;
		iAxisInpositionLevel	= ref.iAxisInpositionLevel;
		dbMotorAmpEnableLevel	= ref.dbMotorAmpEnableLevel;
		dbMotorAmpResetLevel	= ref.dbMotorAmpResetLevel;
		dbMotorAmpFaultLevel	= ref.dbMotorAmpFaultLevel;
		dbMotorNegaLimitLevel	= ref.dbMotorNegaLimitLevel;
		dbMotorPosiLimitLevel	= ref.dbMotorPosiLimitLevel;
		dbGantrySelect			= ref.dbGantrySelect;
		dbStartSpeed			= ref.dbStartSpeed;
		dbStopLevel				= ref.dbStopLevel;

		iAxisNum				= ref.iAxisNum;
		iAxisServoLevel			= ref.iAxisServoLevel;
		iAxisAlarmLevel			= ref.iAxisAlarmLevel;
		iAxisHomeSenLevel		= ref.iAxisHomeSenLevel;
		iAxisHomeSig			= ref.iAxisHomeSig;
		iAxisOutputMethod		= ref.iAxisOutputMethod;
		iAxisPosLimitSenLevel	= ref.iAxisPosLimitSenLevel;
		iAxisNegLimitSenLevel	= ref.iAxisNegLimitSenLevel;
		iAxisHomeDir			= ref.iAxisHomeDir;
		iAxisEncInputMethod		= ref.iAxisEncInputMethod;
		iAxisEmergencyLevel		= ref.iAxisEmergencyLevel;
		iAxisUse				= ref.iAxisUse;

		szAxisName				= ref.szAxisName;

		return *this;
	};
}ST_MotionParam, *PST_MotionParam;

//=============================================================================
// Motor Param temp Info
//=============================================================================
typedef struct _tagAxisMoveParam
{
	BOOL		bAxisUse;
	long		lAxisNum;
	double		dbPos;
	double		dbVel;
	double		dbAcc;
	double		dbDec;

	_tagAxisMoveParam()
	{
		bAxisUse	= FALSE;
		lAxisNum	= 0;
		dbPos		= 0.0;
		dbVel		= 0.0;
		dbAcc		= 0.0;
		dbDec		= 0.0;
	}

	_tagAxisMoveParam& operator= (_tagAxisMoveParam& ref)
	{
		bAxisUse	= ref.bAxisUse;
		lAxisNum	= ref.lAxisNum;
		dbPos		= ref.dbPos;
		dbVel		= ref.dbVel;
		dbAcc		= ref.dbAcc;
		dbDec		= ref.dbDec;

		return *this;
	};

}ST_AxisMoveParam, *PST_AxisMoveParam;

//=============================================================================
// Motor Status Info
//=============================================================================
typedef struct _tag_MotorInfo
{
	BOOL	bMotionOpened;
	DWORD	dwModulStatus;
	long	lMaxAxisCnt;

	_tag_MotorInfo()
	{
		bMotionOpened	= FALSE;
		dwModulStatus	= 0;
		lMaxAxisCnt		= 0;
	}

}ST_MotorInfo, *PST_MotorInfo;


//=============================================================================
// Sequence Step 구조체
//=============================================================================
typedef struct _tag_SeqStepTeach
{
	ST_AxisMoveParam stAxisParam[MotorAxisNum];

	void Reset()
	{
		for (int iAxis = MotorAxisX; iAxis < MotorAxisNum; iAxis++)
		{
			stAxisParam[iAxis].bAxisUse = FALSE;
			stAxisParam[iAxis].dbAcc = 0.0;
			stAxisParam[iAxis].dbDec = 0.0;
			stAxisParam[iAxis].dbVel = 0.0;
			stAxisParam[iAxis].dbPos = 0.0;
		}
	}

	_tag_SeqStepTeach()
	{
		for (int iAxis = MotorAxisX; iAxis < MotorAxisNum; iAxis++)
		{
			stAxisParam[iAxis].bAxisUse = FALSE;
			stAxisParam[iAxis].dbAcc = 0.0;
			stAxisParam[iAxis].dbDec = 0.0;
			stAxisParam[iAxis].dbVel = 0.0;
			stAxisParam[iAxis].dbPos = 0.0;
		}
	}

	_tag_SeqStepTeach& operator= (_tag_SeqStepTeach& ref)
	{
		for (int iAxis = MotorAxisX; iAxis < MotorAxisNum; iAxis++)
		{
			stAxisParam[iAxis] = ref.stAxisParam[iAxis];
		}

		return *this;
	};

}ST_SeqStepTeach, *PST_SeqStepTeach;


//=============================================================================
// Motion Sequence 구조체
//=============================================================================
typedef struct _tag_MotionSeqTeach
{
	CString szSequenceName;
	UINT	nStepCount;

	ST_SeqStepTeach stSeqStep[DEF_SEQUENCE_STEP_MAX];

	_tag_MotionSeqTeach()
	{
		nStepCount = 0;
		szSequenceName.Empty();

		for (int iSeq = 0; iSeq < DEF_SEQUENCE_STEP_MAX; iSeq++)
		{
			stSeqStep[iSeq].Reset();
		}
	}

	_tag_MotionSeqTeach& operator= (_tag_MotionSeqTeach& ref)
	{
		nStepCount = ref.nStepCount;
		szSequenceName = ref.szSequenceName;

		for (int iSeq = 0; iSeq < DEF_SEQUENCE_STEP_MAX; iSeq++)
		{
			stSeqStep[iSeq] = ref.stSeqStep[iSeq];
		}

		return *this;
	};

}ST_MotionSeqTeach, *PST_MotionSeqTeach;


//=============================================================================
// Motor Total Info
//=============================================================================
typedef struct _tag_AllMotorData
{
	ST_MotionParam*		pMotionParam;
	ST_MotorInfo		MotorInfo;

	ST_MotionSeqTeach	MotionSeq[DEF_MOTION_SEQ_MAX];

	_tag_AllMotorData()
	{
		pMotionParam	= NULL;
	}

}ST_AllMotorData, *PST_AllMotorData;

#endif // Def_Motor_h__