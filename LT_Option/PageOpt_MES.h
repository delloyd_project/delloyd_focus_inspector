﻿//*****************************************************************************
// Filename	: PageOpt_MES.h
// Created	: 2010/9/16
// Modified	: 2010/9/16 - 18:05
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef PageOpt_MES_h__
#define PageOpt_MES_h__

#pragma once
#include "PageOption.h"
#include "Define_Option.h"

using namespace Luritech_Option;

//=============================================================================
//
//=============================================================================
class CPageOpt_MES : public CPageOption
{
	DECLARE_DYNAMIC(CPageOpt_MES)

public:
	CPageOpt_MES						(void);
	CPageOpt_MES						(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CPageOpt_MES			(void);

protected:

	DECLARE_MESSAGE_MAP()

	virtual void			AdjustLayout		();
	virtual void			SetPropListFont		();
	virtual void			InitPropList		();

	stOpt_MES				m_stOption;

	stOpt_MES				GetOption			();
	void					SetOption			(stOpt_MES stOption);

public:

	virtual void			SaveOption			();
	virtual void			LoadOption			();
};

#endif // PageOpt_MES_h__
