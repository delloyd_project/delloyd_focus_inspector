
/* 	filename: 	hwif
 * **************************************************************************************
 *
 * 	created  :	2009/06/10
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *  version  :  1   [2009/06/10] : START CODE -> CLASS MADE / CODE WRITING..
 *				2	[2012/06/04] : new code apdn 
 *
 * **************************************************************************************
 */

#include "StdAfx.h"
#include "..\boxinc\box_type_win.h"
#include "..\boxinc\box_log_win.h"

#include "hwif.h"

#include <windows.h>
#include <stdio.H>
#include <strsafe.h>
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#pragma optimize("", off) 


/*#ifdef W32_W64_COMPATIBLE_LIB_USE*/

	#if defined(_WIN64)
	#pragma	comment(lib,"..\\ComartLib\\hwif\\cat3di_64b.lib")
	#else
	#pragma	comment(lib,"..\\ComartLib\\hwif\\cat3di_32b.lib")
	#endif

// #else 
// 	#pragma	comment(lib,"..\\ComartLib\\hwif\\CAT3DI.lib")
// 	
// #endif


















//* *************************************************************************************
//*
//*	HWIF			    MACRO and TYPE and DECLARE
//*
//* *************************************************************************************
extern "C" CAT3DIAPI	DWORD	WINAPI	CAT3DI_IO_STATUS
(stuint IN_st3DInx, stuint IN_stIO, stuint IN_stDev, stuint IN_stAddr, stuint IN_stData);


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	LogPrintf(LPCSTR lpcszString,...)	{	}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * frameTBL.cpp
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	GetG_VCAPT_SliceFPSTBL	(stuint IN_stNTSC, stuint IN_stUseCH,     stuint *IO_stCamFPSTBL);
void	Get_frameTBL			(stuint *IN_stCamFPSInTBL, stuint IN_stInTblCnt,
								 stuint *IO_stCamInxTBL,   stuint IN_stIoTblCnt);















/* 
 * **************************************************************************************
 * 
 * CLASS		: HWIF
 *
 *
 *
 *	ENVIRONMENT	: 
 *	COMPILER	: Visual Studio 2K8   				  
 *
 * **************************************************************************************
 */
cHWIF			g_hw;


//del>#define MAX_THRD_CTRL					8
DWORD			g_nvid_thrd_tbl[] = { 2, 4, 6, 0, 3, 5, 7, 1, };
DWORD			g_netc_thrd_tbl[] = { 3, 5, 7, 1, 2, 4, 6, 0, };






// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * <2016.06.08:model >
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
static	TCHAR	*g_szBDname[] = 
{	_T("LIV04" ), _T("LIV08"),  _T("LIV16"),				// 0, 1, 2	: XED 100,200, 400
	_T("CAP02" ), _T("CAP04"),  _T("CAP08"),				// 3, 4, 5	: XEC 50B,100B,200B

	_T("HCAP1" ), _T("HCAP2"),  _T("HCAP3"), _T("HCAP4"),	// 6, 7, 8, 9
	_T("HCAP8"),											// 10		: HDC 240S
	_T("F60S"),												// 11		: HDC 60S	
	_T("XXX1"),												// 12

	_T("CAP16e"), _T("CAP4Pe"), _T("CAP8Pe"), _T("ES_32"),	//13,14,15,16

	_T("HCAP9" ),											//17		: HDC 270S

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	_T("CAP32E"), _T("CAP16LP"),_T("HYC164"),				//18,19,20  : XEC 800,400,164

 	_T("HCAPSA"),											//21        : HDC 270SA,240SA 

	_T("HDC264" ), _T("XEC264"),							//22,23     : HDC 240SH, XEC 400EH

	_T("USCAP" ),											//24		: USC 

	_T("ADH264"), 											//25		: AHDC240H		

	_T("4KEXDI"),_T("H2704K"),								//26,27		: 4KEXSDI, HDC 2704K
	_T("ALLCAP"),											//28        : ALC 

	_T("UNKNOWN"),											//29
	_T("UNKNOWN"),											//30
	_T("UNKNOWN"),											//31
	_T("UNKNOWN"),											//32
	_T("UNKNOWN"),											//33
};



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FEATURE PER MODEL
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
static	int		g_nEnVLIVE[] = 
{	1,1,1,		0,0,0,				0,0,0,0,		0,0,0,		0,0,0,0,		0,		
	0,0,0,		0,		0,0,		0,		0,		0,0,		0,		0
};																						
static	int		g_nEnVCAPT[] =															
{	1,1,1,		1,1,1,				0,0,0,0,		0,0,0,		1,1,1,0,		0,		
	1,1,1,		0,		0,1,		0,		0,		0,0,		0,		0
};																						
static	int		g_nEnHCAPT[] =															
{	0,0,0,		0,0,0,				1,1,1,1,		1,1,1,		0,0,0,0,		1,		
	1,1,1,		1,		1,1,		1,		1,		1,1,		1,		0
};					
static	int		g_nEnSD_VS[] =															
{	0,0,0,		0,0,0,				0,0,0,0,		0,0,0,		0,0,0,0,		0,		
	0,0,0,		0,		0,1,		0,		0,		0,0,		0,		0
};					
static	int		g_nEnHD_VS[] =															
{	0,0,0,		0,0,0,				0,0,0,0,		0,0,0,		0,0,0,0,		0,		
	0,0,0,		0,		1,0,		0,		1,		0,0,		0,		0
};					

static	int		g_nEnACAPT[] =															
{	1,1,1,		1,1,1,				0,0,0,0,		0,0,0,		1,1,1,0,		0,		
	1,1,1,		1,		1,1,		0,		1,		0,1,		1,		0
};																						
static	int		g_nEnSQUAD[] =															
{	0,0,0,		1,0,0,				0,0,0,0,		0,0,0,		0,0,0,0,		0,		
	0,0,0,		0,		0,0,		0,		0,		0,0,		0,		0
};																						
static	int		g_nEnAPLAY[] =															
{	0,0,0,		1,1,1,				0,0,0,0,		0,0,0,		0,0,0,0,		0,		
	0,0,0,		0,		0,0,		0,		0,		0,0,		0,		0
};

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * Video feature - must video select model
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
static	int		g_nMuxstVCAPT[] = 
{	0,0,0,		0,0,0,				0,0,0,0,		0,0,0,		0,0,0,0,		0,		
	1,1,1,		1,		1, 1,		1,		1,		1,1,		1,		0
};

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * video channel per model
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
static	int		g_nVCAPT_MAX_SDCH[] = 
{  16,16,16,	16,32,32,			0,0,0,0,		0,0,0,		32,32,32,0,		0,		
	32,16,16,	0,		 0,16,		0,		0,		0,0,		0,		0
};
static	int		g_nVCAPT_MAX_HDCH[] = 
{	0, 0, 0,	0, 0, 0,			1,2,3,4,		8,3,0,		0, 0, 0, 0,		9,		
	8, 0, 8,	9,	    11, 2,		2,		11,		9,9,		16,		0
};
static	int		g_nVCAPT_MAX_VICH[] = 
{  16,16,16,	16,32,32,			1,2,3,4,		5,6,0,		32,32,32,0,		9,		
	40,16,40,	9,		11,38,		2,		11,		9,9,		16,		0
};
static	int		g_nVCAPT_STA_HDCH[] = 
{  16,16,16,	16,32,32,			0,0,0,0,		0,0,0,		32,32,32,0,		0,		
	32,32,32,	0,	     0,32,		0,		0,		0, 0,		0,		0
};
static	int		g_nVCAPT_MAX_VIGRP[] = 
{  16,16,16,	 2, 4, 8,			1,2,3,4,		8,3,0,		16, 4, 8,0,		9,		
	40,16,40,	9,		11,38,		2,		11,		9, 9,		16,		0 	
};


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * VCAPT
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
static	int		g_ndefault411[] = 
{	0, 0, 0,	0, 0, 0,			1,1,1,1,		1,1,1,		1, 1, 1, 0,		1,		
	1, 1, 1 ,	1,		1, 1,		1,		1,		1,0,		1,		1
};


//  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  
static	int		g_ndefaultVET[] = 
//0: normal
//1: 960h, exsdi
//2: casdi
{	0, 0, 0,	0, 0, 0,			0,0,0,0,		0,0,0,		0, 0, 0, 0,		0,
	0, 0, 0 ,	0,		0, 0,		0,		0,		0,0,		0,		0
};


//  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  ---  
static	int		g_ndefaultResol_SD[] =		//i:Refer CAPTURE RESOLUTION CONST : ComArT3DI.H
{	8, 8, 8,	 8, 8, 8,			0,0,0,0,		0,0,0,		8, 5, 5, 0,		0,		
	2, 2, 2 ,	0,		0, 2,		0,		0,		0,0,		0,		1
};

static	int		g_ndefaultResol_HD[] =		//i:Refer CAPTURE RESOLUTION CONST : ComArT3DI.H
{	0, 0, 0,	 0, 0, 0,			15,15,15,15,	15,15,15,	0, 0, 0, 0,		15,		
	15, 0,15 ,	13,		13,13,		13,		13,		19,13,		13,		13	
};


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * audio channel per model
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
static	int		g_nACAPT_MAX_CH[] = 
{ 16,16,16,		4,8,16,				0,0,0,0,		0,0,0,		16,8,16, 0,		0,		
  16,16,16,		8,		8,  8,		0,		0,		0,8,		16,		0,
};

static	int		g_nACAPT_MAX_VOL[] = 
{ 15,15,15,		31,31,31,			0,0,0,0,		0,0,0,		0, 0, 0, 0,		0,		
  0, 0,0 ,		0,		0, 15,		0,		0,		0,0,	   31,		0,	
};

static	int		g_nACAPT_DEF_VOL[] = 
{ 13,13,13,		30,30,30,			0,0,0,0,		0,0,0,		0, 0, 0, 0,		0,		
  0, 0,0 ,		0,		0, 12,		0,		0,		0,0,	   16,		0,	
};






// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * DQUAD MODEL : CAMERA SCREEN AREA
// * 14.11.18    : XEC400EH , HDC240SHONLY
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
static	int		g_dquad_area_tbl[4][20][4] = 
{
	//i: PAL - - - - - - - - - - -
	{	{ 0x0, 0x0, 704, 576,  },								// 1 div	
	    { 0x0, 0x0,1920,1080,  },	{ 0x0, 0x0,1920,1080, },	// 1, 2 div
																
		{ 0x0, 0x0,1408,1080,  },	{ 0x0, 0x0,1920,1080, },	// 4, 6 div
		{ 0x0, 0x0,1056, 864,  },	{ 0x0, 0x0, 944, 768, },	// 6, 8 div
																
		{ 0x0, 0x0,1920,1080,  },	{ 0x0, 0x0,1408,1080, },	// 9, 10div
		{ 0x0, 0x0,1408,1080,  },	{ 0x0, 0x0,1920,1080, },	//13, 16div
	},

	//i: NTSC - - - - - - - - - - 
	{	{ 0x0, 0x0, 704, 480,  },								// 1 div			
		{ 0x0, 0x0,1920,1080,  },	{ 0x0, 0x0,1920,1080, },	// 1, 2 div
																
		{ 0x0, 0x0,1408, 960,  },	{ 0x0, 0x0,1920, 960, },	// 4, 6 div	
		{ 0x0, 0x0,1056, 720,  },	{ 0x0, 0x0, 944, 640, },	// 6, 8 div
																
		{ 0x0, 0x0,1920,1080,  },	{ 0x0, 0x0,1408,1080, },	// 9, 10div
		{ 0x0, 0x0,1408, 960,  },	{ 0x0, 0x0,1920,1080, },	//13, 16div
	},

	//i: 1280x720 - - - - - - - - - - 
	{	{ 0x0, 0x0,1280, 720,  },								// 1 div	
		{ 0x0, 0x0,1920,1080,  },	{ 0x0, 0x0,1920,1080, },	// 1, 2 div
	  
		{ 0x0, 0x0,1920,1056,  },	{ 0x0, 0x0,1920,1056, },	// 4, 6 div
		{ 0x0, 0x0,1920,1056,  },	{ 0x0, 0x0,1664, 960, },	// 6, 8 div
	
		{ 0x0, 0x0,1920,1056,  },	{ 0x0, 0x0,1920,1080, },	// 9, 10div
		{ 0x0, 0x0,1920,1080,  },	{ 0x0, 0x0,1920,1072, },	//13, 16div
	},

	//i: 1920x1080 - - - - - - - - - - 
	{	{ 0x0, 0x0,1920,1080,  },								// 1 div	
		{ 0x0, 0x0,1920,1080,  },	{ 0x0, 0x0,1920,1080, },	// 1, 2 div
																
		{ 0x0, 0x0,1920,1056,  },	{ 0x0, 0x0,1920,1056, },	// 4, 6 div
		{ 0x0, 0x0,1920,1056,  },	{ 0x0, 0x0,1664, 960, },	// 6, 8 div
																
		{ 0x0, 0x0,1920,1056,  },	{ 0x0, 0x0,1920,1080, },	// 9, 10div
		{ 0x0, 0x0,1920,1080,  },	{ 0x0, 0x0,1920,1072, },	//13, 16div
	},

};
























// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//	VCAPT  SIZE
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
static DWORD	g_dtResolCONST_WidthTBL[2][MAX_RESOL_VCAPT_TRON]	  = 
{	{	0,		720, 704, 640,	720, 704, 640,	360, 352, 320,	180, 176, 160,	1920,1280,1280, 720, 1920, 1920, 3840, 4096, },	
	{	0,		960, 944, 800,	960, 944, 800,	480, 464, 400,	180, 176, 160,	1920,1280,1280, 720, 1920, 1920, 3840, 4096, },	
};

static DWORD	g_dtResolCONST_HeightTBL[2][MAX_RESOL_VCAPT_TRON] = 
{
	{	0,		576, 576, 576,	288, 288, 288,	288, 288, 288,	144, 144, 144,	1080, 960, 720, 576, 1080, 540, 2160, 2160, },
	{	0,		480, 480, 480,  240, 240, 240,	240, 240, 240,	120, 120, 120,	1080, 960, 720, 480, 1080, 540, 2160, 2160, },
};






// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//	VCAPT  VIDEO
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	GetG_VCAPT_XZ(DWORD IN_dw960H, DWORD IN_dwResConst) 
{
		return (g_dtResolCONST_WidthTBL [IN_dw960H][IN_dwResConst]);
}

DWORD	GetG_VCAPT_YZ(DWORD IN_dwNTSC, DWORD IN_dwResConst) 
{
		return (g_dtResolCONST_HeightTBL[IN_dwNTSC][IN_dwResConst]);
}

DWORD	GetG_VCAPT_IP(DWORD IN_dwResConst)
{
	switch(IN_dwResConst)
	{	case VCAPT_TRON_720X576 : 	
		case VCAPT_TRON_704X576 :
		case VCAPT_TRON_640X576 : 
		case VCAPT_TRON_1920X1080I : return 1;		

		default					: return 0;
	}
}

stuint	GetG_VCAPT_CHtoGRP(DWORD IN_dwModel, DWORD IN_dwCH)
{

	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	stuint		stADC;
	switch(IN_dwModel)
	{	case CAT3D_ID_HSDI1 : stADC =  0;			break;			
		case CAT3D_ID_HSDI4 : stADC = IN_dwCH %  4;	break;
		case CAT3D_ID_HSDI8 : stADC = IN_dwCH %  8;	break;
		case CAT3D_ID_FDI60S: stADC = IN_dwCH %  3;	break;		
		case CAT3D_ID_HSDI9 : stADC = IN_dwCH %  9; break;

		case CAT3D_ID_HD08SH :
		case CAT3D_ID_AHD08H :
		case CAT3D_ID_4KEXDI4:
		case CAT3D_ID_HD2704K:
		case CAT3D_ID_HSDI8EX:stADC = IN_dwCH % 12;	break;

		case CAT3D_ID_ALCAP16:stADC = IN_dwCH % 16;	break;

		case CAT3D_ID_LIV04 : 
		case CAT3D_ID_LIV08 : 
		case CAT3D_ID_LIV16 : stADC = IN_dwCH % 16; break;

		case CAT3D_ID_CAP02 : stADC = IN_dwCH %  2;	break;
		case CAT3D_ID_CAP04 : stADC = IN_dwCH %  4;	break;
		case CAT3D_ID_CAP08 : stADC = IN_dwCH %  8;	break;

		case CAT3D_ID_CAP4PE: stADC = IN_dwCH %  4;	break;
		case CAT3D_ID_CAP8PE: stADC = IN_dwCH %  8;	break;
		case CAT3D_ID_CAP16E: stADC = IN_dwCH % 16;	break;

		case CAT3D_ID_CAP16LP:stADC = IN_dwCH & 40;	break;
		case CAT3D_ID_CAP16HD:stADC = IN_dwCH % 40; break;	//i:0~15:SD, 16~23:HD
		case CAT3D_ID_CAP32E: stADC = IN_dwCH % 40; break;	//i:0~31:SD, 32~39:HD

		case CAT3D_ID_SD16EH: stADC = IN_dwCH % 40; break;	//i:0~15:SD, 16~23:HD

		default				: stADC = 0;
	}

	return stADC;
}





// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//	VSTRM  VIDEO
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	GetG_VSTRM_XZ(DWORD IN_dw960H, DWORD IN_dwInpHDXZ, DWORD IN_dwResConst)
{
	DWORD	dwrv = 0;
	switch (IN_dwResConst)
	{	case VSTRM_TRON_4HDCIF	: dwrv = IN_dwInpHDXZ;		break;
		case VSTRM_TRON_2HDCIF	: dwrv = IN_dwInpHDXZ;	 	break;
		case VSTRM_TRON_HDCIF	: dwrv = IN_dwInpHDXZ /2 ;	break;
	
		case VSTRM_TRON_FD1     : dwrv = (IN_dw960H)? 960 : 720;	break;
		case VSTRM_TRON_4CIF	: dwrv = (IN_dw960H)? 960 : 720;	break;
		case VSTRM_TRON_2CIF	: dwrv = (IN_dw960H)? 960 : 720;	break;
		case VSTRM_TRON_CIF		: dwrv = (IN_dw960H)? 480 : 352;	break;
		default					: dwrv = 0;							break;
	}
	return dwrv;
}

DWORD	GetG_VSTRM_YZ(DWORD IN_dwNTSC, DWORD IN_dwInpHDYZ, DWORD IN_dwResConst)
{
	DWORD	dwrv = 0;
	switch (IN_dwResConst)
	{	case VSTRM_TRON_4HDCIF	: dwrv = IN_dwInpHDYZ;		break;
		case VSTRM_TRON_2HDCIF	: dwrv = IN_dwInpHDYZ /2;	break;
		case VSTRM_TRON_HDCIF	: dwrv = IN_dwInpHDYZ /2 ;	break;

		case VSTRM_TRON_FD1		: dwrv = (IN_dwNTSC)? 480 : 576;	break;
		case VSTRM_TRON_4CIF	: dwrv = (IN_dwNTSC)? 480 : 576;	break;
		case VSTRM_TRON_2CIF	: dwrv = (IN_dwNTSC)? 240 : 288;	break;
		case VSTRM_TRON_CIF		: dwrv = (IN_dwNTSC)? 240 : 288;	break;
		default					: dwrv = 0;							break;
	}
	return dwrv;
}

DWORD	GetG_VSTRM_IP(DWORD IN_dwInpHDIP, DWORD IN_dwResConst)
{
	switch (IN_dwResConst)
	{	case VSTRM_TRON_4HDCIF	: if (IN_dwInpHDIP == 0) return 1;
		case VSTRM_TRON_2HDCIF	: break;
		case VSTRM_TRON_HDCIF	: break;

		case VSTRM_TRON_4CIF	: return 0;
		case VSTRM_TRON_FD1		: return 1;
		case VSTRM_TRON_2CIF	: break;
		case VSTRM_TRON_CIF		: break;
		default					: break;
	}

	return 0;
}









// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//	DQUAD  VIDEO
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	GetG_DQUAD_SCRN_SZ(DWORD IN_dwVtyp, DWORD IN_dwMODE, RECT *io_rv)
{		
	if (io_rv == NULL) return;



	io_rv->left		= g_dquad_area_tbl[IN_dwVtyp & 3][IN_dwMODE][0];
	io_rv->top		= g_dquad_area_tbl[IN_dwVtyp & 3][IN_dwMODE][1];
	io_rv->right	= g_dquad_area_tbl[IN_dwVtyp & 3][IN_dwMODE][2];
	io_rv->bottom	= g_dquad_area_tbl[IN_dwVtyp & 3][IN_dwMODE][3];
	
}


















// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//	ACAPT TRON SIZE
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//	1SEC DAT	16000B, 22050B, 32000B, 44100B, 192000B
//	DISABLE		8Khz,   11.025, 16Khz,  22.05,  48Khz(stereo)
//	WAKE time	200mS,  X,      200mS,	X,      100mS
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

static	DWORD	g_dtResolCONST_AudSizTBL	[] =
{		0,		3200,	11008,	6400,	22144,	19200,	0,0,0,0,0,	
};
static	DWORD	g_dtResolCONST_WakeTimeTBL	[] =
{		0,		25,		86,		50,		173,	150,	0,0,0,0,0,
};

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	GetG_ACAPT_AudSiz(DWORD IN_dwCAT3DID, DWORD IN_dwResConst) 
{	
		//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
		if ( (IN_dwCAT3DID == CAT3D_ID_LIV04) ||	//i> XED series Fixed Audio Capture Block size
			 (IN_dwCAT3DID == CAT3D_ID_LIV08) ||	
			 (IN_dwCAT3DID == CAT3D_ID_LIV16) )
			 return (4 * 4096);
	
		return (g_dtResolCONST_AudSizTBL[IN_dwResConst]);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	GetG_ACAPT_WakeTime(DWORD IN_dwResConst) 
{		return (g_dtResolCONST_WakeTimeTBL	[IN_dwResConst]);
}









// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// * VIDEO MUX FRAME TABLE
// * XECAP 50B, 100B, 200B, 100E, 200E, 400E
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

static	DWORD	g_dwXEC400_TBL[2][HWI_MAX_VID_CH] = 
{	
	{	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,  
		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0, },
	{	0,16, 0,16,		0,16, 0,16,		0,16, 0,16,		0,16, 0,16,		 
		0,16, 0,16,		0,16, 0,16,		0,16, 0,16,		0,16, 0,16,	},
};

static	DWORD	g_dwXEC200_TBL[4][HWI_MAX_VID_CH] = 
{	
	{	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,  
		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0, },
	{	0, 8, 0, 8,		0, 8, 0, 8,		0, 8, 0, 8,		0, 8, 0, 8,		 
		0, 8, 0, 8,		0, 8, 0, 8,		0, 8, 0, 8,		0, 8, 0, 8,	},

	{	0, 8,16, 0,		8,16, 0, 8,	   16, 0, 8,16,		0, 8,16, 0,		
		8,16, 0, 8,	   16, 0, 8,16,	    0, 8,16, 0,		8,16, 0, 8,	},		
	{	0, 8,16,24,		0, 8,16,24,		0, 8,16,24,		0, 8,16,24,	 
		0, 8,16,24,		0, 8,16,24,		0, 8,16,24,		0, 8,16,24, },
};

static	DWORD	g_dwXEC100_TBL[8][HWI_MAX_VID_CH] = 
{	
	{	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,  
		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0, },
	{	0, 4, 0, 4,		0, 4, 0, 4,		0, 4, 0, 4,		0, 4, 0, 4,	
		0, 4, 0, 4,		0, 4, 0, 4,		0, 4, 0, 4,		0, 4, 0, 4,	},
	{	0, 4, 8, 0,		4, 8, 0, 4,		8, 0, 4, 8,		0, 4, 8, 0,	
		4, 8, 0, 4,		8, 0, 4, 8,		0, 4, 8, 0,		4, 8, 0, 4,	},
	{	0, 4, 8,12,		0, 4, 8,12,		0, 4, 8,12,		0, 4, 8,12,	
		0, 4, 8,12,		0, 4, 8,12,		0, 4, 8,12,		0, 4, 8,12,	},

	{	0, 4, 8,12,	   16, 0, 4, 8,	   12,16, 0, 4,		8,12,16, 0,	
		4, 8,12,16,		0, 4, 8,12,	   16, 0, 4, 8,	   12,16, 0, 4,	},
	{	0, 4, 8,12,	   16,20, 0, 4,	    8,12,16,20,		0, 4, 8,12,		
	   16,20, 0, 4,	    8,12,16,20,	    0, 4, 8,12,	   16,20, 0, 4,	},		
	{	0, 4, 8,12,	   16,20,24, 0,		4, 8,12,16,    20,24, 0, 4,
	    8,12,16,20,    24, 0, 4, 8,    12,16,20,24,     0, 4, 8,12, },
	{	0, 4, 8,12,	   16,20,24,28,		0, 4, 8,12,	   16,20,24,28,
	    0, 4, 8,12,	   16,20,24,28,	    0, 4, 8,12,	   16,20,24,28, },
};

static	DWORD	g_dwXEC50_TBL[8][HWI_MAX_VID_CH] = 
{
	{	0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,  
		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0,		0, 0, 0, 0, },
	{	0, 2, 0, 2,		0, 2, 0, 2,		0, 2, 0, 2,		0, 2, 0, 2,		 
		0, 2, 0, 2,		0, 2, 0, 2,		0, 2, 0, 2,		0, 2, 0, 2,	},
	{	0, 2, 4, 0,		2, 4, 0, 2,	    4, 0, 2, 4,		0, 2, 4, 0,
		2, 4, 0, 2,		4, 0, 2, 4,	    0, 2, 4, 0,		2, 4, 0, 2,	},
	{	0, 2, 4, 6,		0, 2, 4, 6,	    0, 2, 4, 6,		0, 2, 4, 6,
		0, 2, 4, 6,		0, 2, 4, 6,	    0, 2, 4, 6,		0, 2, 4, 6,	},

	{	0, 2, 4, 6,		8, 0, 2, 4,	    6, 8, 0, 2,		4, 6, 8, 0,
		2, 4, 6, 8,		0, 2, 4, 6,	    8, 0, 2, 4,		6, 8, 0, 2,	},
	{	0, 2, 4, 6,		8,10, 0, 2,	    4, 6, 8,10,		0, 2, 4, 6,
		8,10, 0, 2,		4, 6, 8,10,	    0, 2, 4, 6,		8,10, 0, 2,	},
	{	0, 2, 4, 6,		8,10,12, 0,	    2, 4, 6, 8,	   10,12, 0, 2,
		4, 6, 8,10,	   12, 0, 2, 4,	    6, 8,10,12,		0, 2, 4, 6,	},
	{	0, 2, 4, 6,	    8,10,12,14,		0, 2, 4, 6,	    8,10,12,14,	 
		0, 2, 4, 6,	    8,10,12,14,		0, 2, 4, 6,	    8,10,12,14, },
};



















































// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// * Constructor Destructor
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
cHWIF::		cHWIF()
{	
	m_nOPEN			 = 0;
	m_stbd_opn_opt	 = 0;
	m_st960h_mdl_flg = 0;

	ZeroMemory(&m_stVSTRM_staTBL[0], sizeof(m_stVSTRM_staTBL));
	ZeroMemory(&m_stVCAPT_staTBL[0], sizeof(m_stVCAPT_staTBL));

	ZeroMemory(&m_stpAudSTA [0], sizeof(m_stpAudSTA));
	ZeroMemory(&m_stpUartSTA[0], sizeof(m_stpUartSTA));
	
	// uart
	m_dwuart_CFG_done = 0;

	// wdog
	m_stWDOG_En	= 0;
	m_stWDOG_Sec= 0;

	// mux out 
	m_stAUDMux_En	= 0;
	m_stAUDMux_CH	= 0;
	m_stAUDMux_Vol	= 0;

	m_stTV1Mux_En	= 0;
	m_stTV1Mux_CH	= 0;

	m_stTV2Mux_En	= 0;
	m_stTV2Mux_CH	= 0;

}

cHWIF::		~cHWIF()
{
}
























// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// * @ DETECT FUNCTION
//				  
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
int		cHWIF::_DETECT(DWORD IN_dwTYPE) 
{	
	tCArTBDList			tCArTList;
	tCArTBDDetailInfo	tDtail;
	stuint				strv, sti, stED, stCNT;
	stuint				nmodelid;
	

	ZeroMemory(&tCArTList, sizeof(tCArTBDList));
	strv = CAT3DI_BDSearch(&tCArTList);
	LogPrintf("CALL : %2X = CAT3DI_BDSearch()", strv);
	if (strv != CAT3DI_FUNC_OK) return -1;


	stED = (tCArTList.dwTNumB > HWI_MAX_BORAD)? HWI_MAX_BORAD : tCArTList.dwTNumB;	
	for (stCNT = 0, sti = 0; sti < stED; sti++) 
	{			
		nmodelid = tCArTList.dwaModel[sti];		
		CAT3DI_BDDetailInfo(sti, &tDtail);
		LogPrintf("CALL : ComArT BOARD CAT3DI_BDDetailInfo()");

		if (nmodelid >= CAT3D_MAX_ID) continue;

		m_tDETECT.dwEXT		[stCNT] = 0;
		m_tDETECT.dwTYPE	[stCNT] = (DWORD)tCArTList.dwaModel[sti];

		StringCbPrintf(m_tDETECT.lpszNAME[stCNT], sizeof(m_tDETECT.lpszNAME[stCNT]), _T("%s"), g_szBDname[nmodelid]);

		m_tDETECT.dtEnVLIVE[stCNT] = g_nEnVLIVE[nmodelid];

		m_tDETECT.dtEnVCAPT[stCNT] = g_nEnVCAPT[nmodelid];
		m_tDETECT.dtEnHCAPT[stCNT] = g_nEnHCAPT[nmodelid];
		m_tDETECT.dtEnSD_VS[stCNT] = g_nEnSD_VS[nmodelid];
		m_tDETECT.dtEnHD_VS[stCNT] = g_nEnHD_VS[nmodelid];

		m_tDETECT.dtEnACAPT[stCNT] = g_nEnACAPT[nmodelid];
		m_tDETECT.dtEnAPLAY[stCNT] = g_nEnAPLAY[nmodelid];
		m_tDETECT.dtEnRUART[stCNT] = 1;
		m_tDETECT.dtEnSQUAD[stCNT] = g_nEnSQUAD[nmodelid];

		m_tDETECT.dtMustVID[stCNT]=g_nMuxstVCAPT[nmodelid];

				
		if (nmodelid == CAT3D_ID_CAP04)
		{	if ((tDtail.dwFPGA_RevID >= 0x05012400) &&
				(tDtail.dwFPGA_RevID <= 0x79123100) )	//INF: CHECK -1
			{	m_tDETECT.dwEXT		[stCNT] = 1;
				m_tDETECT.dtEnSQUAD	[stCNT]	= 1;
				StringCbPrintf(m_tDETECT.lpszNAME[stCNT], sizeof(m_tDETECT.lpszNAME[stCNT]), _T("CAP04B"));
			}
		}
		if (nmodelid == CAT3D_ID_CAP08)
		{	if (tDtail.dwCAT_BDID == 0x20) 
			{	m_tDETECT.dwEXT		[stCNT] = 1;
				m_tDETECT.dtEnSQUAD	[stCNT]	= 1;
				StringCbPrintf(m_tDETECT.lpszNAME[stCNT], sizeof(m_tDETECT.lpszNAME[stCNT]), _T("CAP08B"));
			}
		}
		if (nmodelid == CAT3D_ID_CAP32E)
		{	if (tDtail.dwCAT_BDID == 0x37) 
			{	StringCbPrintf(m_tDETECT.lpszNAME[stCNT], sizeof(m_tDETECT.lpszNAME[stCNT]), _T("CAP16LP"));
			}
			if (tDtail.dwCAT_BDID == 0x38) 
			{	StringCbPrintf(m_tDETECT.lpszNAME[stCNT], sizeof(m_tDETECT.lpszNAME[stCNT]), _T("HYC164"));
			}
		}


		
		CopyMemory(& (m_tDETECT.tDetail[stCNT]), &tDtail, sizeof(tCArTBDDetailInfo));
		stCNT++;
		
	}

	if (stCNT == 0)  return -1;
	
	m_tDETECT.dwNumB   = (DWORD)stCNT;

	if (IN_dwTYPE      == HWI_FIND_ALL) return 0;
	if (m_tDETECT.FIND(IN_dwTYPE) >= 0) return 1;	

	return 0;
}



























// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// * @ open / close
//				  
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
DWORD		cHWIF::_GET_OPEN_FLAG
(DWORD IN_dwInx,	DWORD IN_dwVIDEO, DWORD IN_dwAUDIO, 
 DWORD IN_dwAPLAY,	DWORD IN_dwRUART, DWORD IN_dwSQUAD)
{
	DWORD	dwFLAG = 0;

	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	if (IN_dwVIDEO)
	{	if ((m_tDETECT.dwTYPE[IN_dwInx] == CAT3D_ID_LIV16) ||
			(m_tDETECT.dwTYPE[IN_dwInx] == CAT3D_ID_LIV08) ||
			(m_tDETECT.dwTYPE[IN_dwInx] == CAT3D_ID_LIV04) )
		{	dwFLAG |= (CAT3DI_VLIVE | CAT3DI_VTRAN | CAT3DI_XED_SQUAD);
		}else 
		{	dwFLAG |= CAT3DI_VCAPT;
		}
	}
	if (IN_dwAUDIO)
	{	dwFLAG |=  ((m_tDETECT.dtEnACAPT[IN_dwInx])? CAT3DI_ACAPT : 0);
	}
	if (IN_dwAPLAY)
	{	dwFLAG |=  ((m_tDETECT.dtEnAPLAY[IN_dwInx])? CAT3DI_APLAY : 0);
	}
	if (IN_dwSQUAD)
	{	dwFLAG |=  ((m_tDETECT.dtEnSQUAD[IN_dwInx])? CAT3DI_SQUAD : 0);
	}
	if (IN_dwRUART)
	{	dwFLAG |=  CAT3DI_RUART;
	}	

	return dwFLAG;
}



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
BOOL		cHWIF::_OPEN
(DWORD	IN_dwInx, DWORD IN_dwFLG, DWORD IN_dwNTSC, DWORD *IO_dwErrCode)
{
	stuint		strv;

	_CLOSE();			

	
	strv =  CAT3DI_BDOpen((stuint)IN_dwInx, (stuint)IN_dwFLG, (stuint)IN_dwNTSC);	
	LogPrintf("CALL : %02X = ComArT BOARD OPEN(%X, %X, %X)", strv, IN_dwInx, IN_dwFLG, IN_dwNTSC);		
	*IO_dwErrCode = (DWORD)strv;
	if (strv != CAT3DI_FUNC_OK) 
	{	
		return FALSE;
	}


	
	m_stSLCTInx		= (stuint)IN_dwInx;
	m_dwSLCTMdl		= m_tDETECT.dwTYPE[IN_dwInx];
	m_dwNTSC		= IN_dwNTSC;
	m_stbd_opn_opt	= IN_dwFLG;


	m_vidthrd_cnt	= 0;
	m_audthrd_cnt	= 0;
		
	BD_FEATURE	 (IN_dwNTSC);

	ZeroMemory(m_stpVL_O, sizeof(m_stpVL_O));
	m_nOPEN		= 1;




	
	//i: if (h.264 ovi model) then process priority up
	//i: if (h.264 ovi model and h264 ovi ch) then  thread priority up	
	int		nprocess_priority_up = ((m_dwSLCTMdl == CAT3D_ID_SD16EH) ||
									(m_dwSLCTMdl == CAT3D_ID_HD08SH) || 
									(m_dwSLCTMdl == CAT3D_ID_AHD08H) )? 1 : 0;
									
	
	if (nprocess_priority_up)
	{	
		if (!SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS))
		{	DWORD dwErr = GetLastError();
			boxlog(1, DBGTR, _T("h.264 ovi model process priority up : HIGH : fail : %d"), dwErr);
		} else 
		{	boxlog(1, DBGTR, _T("h.264 ovi model process priority up : HIGH : success"));
		}
	}
		


	return TRUE;
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::_CLOSE()
{
	if (m_nOPEN == 0) return;
	
	
	CAT3DI_BDClose(m_stSLCTInx);
	
	m_nOPEN			  = 0;
	m_stbd_opn_opt	  = 0;
	m_dwuart_CFG_done = 0;

	LogPrintf("CALL : ComArT BOARD CLOSE(%X)", m_stSLCTInx);	
}





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::BD_FEATURE(DWORD IN_dwNTSC)
{
	stuint		ni, nk;

	// board type feature select  *---*   *---*-   *---*   *---*-   *---*   *---*-  

	// 0. BOARD TYPE FEATURE SELECT		
	// m_stSLCTInx;
	// m_dwSLCTMdl;

	m_nEnVLIVE	= m_tDETECT.dtEnVLIVE[m_stSLCTInx];
	m_nEnVCAPT	= m_tDETECT.dtEnVCAPT[m_stSLCTInx];
	m_nEnHCAPT	= m_tDETECT.dtEnHCAPT[m_stSLCTInx];
	m_nEnSD_VS	= m_tDETECT.dtEnSD_VS[m_stSLCTInx];
	m_nEnHD_VS	= m_tDETECT.dtEnHD_VS[m_stSLCTInx];
	m_nEnACAPT	= m_tDETECT.dtEnACAPT[m_stSLCTInx];
	m_nEnAPLAY	= m_tDETECT.dtEnAPLAY[m_stSLCTInx];
	m_nEnRUART	= m_tDETECT.dtEnRUART[m_stSLCTInx];
	m_nEnSQUAD	= m_tDETECT.dtEnSQUAD[m_stSLCTInx];


	m_npci_mdl_flg		= 0;		//i> OLD PCI model flag	
	m_nahd_mdl_flg		= 0;		//i> AHD model flag	
	m_n4k_mdl_flg		= 0;		//i> 4K  model flag	

	m_tvosd_mdl_flg		= 0;		//i> TV OSD model flag
	m_stosd_pitch		= 24;		//i: tvosd width char 

	m_st960h_mdl_flg	= 0;		//i> SD 960H model flag	

	m_stvet_mdl_flg		= 0;		//i> VIDEO EXT TYPE model flag

	m_tvquad_mdl_flg	= 0;		//i> TV LIVE QUAD screen model flag
	m_tvquad_maxmode	= 0;		//i> TV LIVE QUAD screen MAX MODE

	m_dquadcap_mdl_flg	= 0;		//i: device live screen model flag
	m_dquadcap_chinx	= 0;
	m_dquadcap_maxch	= 0;
	m_dquadcap_maxmode	= 0;
	m_dquadcap_defmode	= 0;

	m_exsdi_mdl_flg		= 0;
	ZeroMemory(&(m_exsdi_chtbl[0]), sizeof(m_exsdi_chtbl));


	//i> H264 OVER VIDEO model flag	: XEC400EH / HDC200H  
	m_h264ovi_mdl_flg	= 0;		
	ZeroMemory(&m_h264ovi, sizeof(m_h264ovi));



	m_acapt_65k_flg		= 0;


	//1. BOARD feature *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   
		
	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	switch (m_dwSLCTMdl)
	{	

		case CAT3D_ID_CAP4PE:	case CAT3D_ID_CAP8PE:	
		case CAT3D_ID_CAP16E:	
		{	m_tvosd_mdl_flg		  = 1;
			m_stosd_pitch		  = 24;
		
			g_nVCAPT_MAX_SDCH[m_dwSLCTMdl] = (int)m_tDETECT.tDetail[m_stSLCTInx].dwMaxVCapCHNumB;
		}	break;

		case CAT3D_ID_CAP32E:	case CAT3D_ID_CAP16LP:
		case CAT3D_ID_CAP16HD:
		{	m_tvosd_mdl_flg		  = 1;
			m_stosd_pitch		  = 32;

			m_st960h_mdl_flg	  = 1;	
			m_stvet_mdl_flg		  = 1;
		}	break;

		case CAT3D_ID_SD16EH:
		{	m_tvquad_mdl_flg	  = 1;		//i> TV LIVE QUAD screen model flag
			m_tvquad_maxmode  	  = 4;		//i> TV LIVE QUAD screen MAX MODE

			m_st960h_mdl_flg	  = 1;	
			m_stvet_mdl_flg		  = 2;


			m_dquadcap_mdl_flg	  = 1;
			m_dquadcap_chinx	  = 33;			//inf: real ch number
			m_dquadcap_maxch	  = 16;			//INF: MAX 16CH
			m_dquadcap_maxmode	  = 11;			//INF: SCREEN QUAD MODE [0~10]	
			m_dquadcap_defmode	  = 7;

			m_h264ovi_mdl_flg	  = 1;
			m_h264ovi.stch_sta	  = 32;
			m_h264ovi.stch_cnt	  = 1;			

			m_h264ovi.chsta_4vc[0]  = 0;	m_h264ovi.chnum_4vc[0]  = 16;			
			m_h264ovi.chsta_4vc[1]  = 0;	m_h264ovi.chnum_4vc[1]  = 0;			
			m_h264ovi.chsta_4vc[2]  = 0;	m_h264ovi.chnum_4vc[2]  = 0;			
			m_h264ovi.chsta_4vc[3]  = 0;	m_h264ovi.chnum_4vc[3]  = 0;		

			m_acapt_65k_flg		   = 1;

		}	break;

		
		case CAT3D_ID_HD08SH:		
		{	m_dquadcap_mdl_flg	  = 1;
			m_dquadcap_chinx	  = 2;			//inf: real ch number	
			m_dquadcap_maxch	  = 8;			//INF: MAX 8CH
			m_dquadcap_maxmode	  = 11;			//INF: SCREEN QUAD MODE [0~10]	
			m_dquadcap_defmode	  = 7;

			m_h264ovi_mdl_flg	  = 1;
			m_h264ovi.stch_sta	  = 0;
			m_h264ovi.stch_cnt	  = 2;
			
			m_h264ovi.chsta_4vc[0]  = 0;	m_h264ovi.chnum_4vc[0]  = 4;			
			m_h264ovi.chsta_4vc[1]  = 4;	m_h264ovi.chnum_4vc[1]  = 4;			
			m_h264ovi.chsta_4vc[2]  = 0;	m_h264ovi.chnum_4vc[2]  = 0;			
			m_h264ovi.chsta_4vc[3]  = 0;	m_h264ovi.chnum_4vc[3]  = 0;			

		}	break;

		
		case CAT3D_ID_AHD08H:		
		{	
			m_nahd_mdl_flg		  = 1;	
			m_dquadcap_mdl_flg	  = 1;
			m_dquadcap_chinx	  = 2;			//inf: real ch number	
			m_dquadcap_maxch	  = 8;			//INF: MAX 8CH
			m_dquadcap_maxmode	  = 11;			//INF: SCREEN QUAD MODE [0~10]	
			m_dquadcap_defmode	  = 7;

			m_h264ovi_mdl_flg	  = 1;
			m_h264ovi.stch_sta	  = 0;
			m_h264ovi.stch_cnt	  = 2;
			
			m_h264ovi.chsta_4vc[0]  = 0;	m_h264ovi.chnum_4vc[0]  = 4;			
			m_h264ovi.chsta_4vc[1]  = 4;	m_h264ovi.chnum_4vc[1]  = 4;			
			m_h264ovi.chsta_4vc[2]  = 0;	m_h264ovi.chnum_4vc[2]  = 0;			
			m_h264ovi.chsta_4vc[3]  = 0;	m_h264ovi.chnum_4vc[3]  = 0;			

		}	break;


		case CAT3D_ID_ALCAP16:
		{	m_nahd_mdl_flg		  = 1;	
			m_st960h_mdl_flg	  = 1;	
			m_stvet_mdl_flg		  = 3;
		}	break;


		case CAT3D_ID_4KEXDI4:
		{	m_n4k_mdl_flg		= 1;
			
		}	break;


		case CAT3D_ID_HD2704K:
		{	m_n4k_mdl_flg		= 1;
			m_exsdi_mdl_flg		= 1;
			m_stvet_mdl_flg		= 4;
		}	break;

			

		case CAT3D_ID_CAP02	:	case CAT3D_ID_CAP04	:
		case CAT3D_ID_CAP08	:
		{	m_tvosd_mdl_flg		  = 1;
			m_stosd_pitch		  = 24;

			m_npci_mdl_flg		  = 1;
			
			g_nVCAPT_MAX_SDCH[m_dwSLCTMdl] = (int)m_tDETECT.tDetail[m_stSLCTInx].dwMaxVCapCHNumB;
		}	break;

		case CAT3D_ID_LIV04 :	case CAT3D_ID_LIV08 :
		case CAT3D_ID_LIV16 :
		{	m_tvosd_mdl_flg		  = 1;
			m_stosd_pitch		  = 24;

			m_npci_mdl_flg		  = 1;
		}	break;
		
		default: break;
	}




	// *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  
	// * video capture 
	// *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  



	//1. MAX channel *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   
	m_stVCAPT_MaxSDCH	= g_nVCAPT_MAX_SDCH [m_dwSLCTMdl];
	m_stVCAPT_MaxHDCH	= g_nVCAPT_MAX_HDCH [m_dwSLCTMdl];	
	m_stVCAPT_MaxVICH	= g_nVCAPT_MAX_VICH [m_dwSLCTMdl];	
	m_stVCAPT_MaxHDRAW	= 0;

	m_stVCAPT_staSDCH	= 0;
	m_stVCAPT_staHDCH	= g_nVCAPT_STA_HDCH [m_dwSLCTMdl];
	m_stVCAPT_staHDRAW	= 0;

	m_stVCAPT_MAX_GRP	= g_nVCAPT_MAX_VIGRP[m_dwSLCTMdl];
	m_stVCAPT_CH_pGRP	= m_stVCAPT_MaxVICH / m_stVCAPT_MAX_GRP;

	ZeroMemory(m_stVCAPT_vidTBL, sizeof(m_stVCAPT_vidTBL));


	for (ni = 0; ni < m_stVCAPT_MaxVICH; ni++)
	{	
		if (ni >= m_stVCAPT_staHDCH) 
		{	m_stVCAPT_vidTBL[ni] = HWI_VIDTYPE_HD;
			m_stVCAPT_ResTBL[ni] = g_ndefaultResol_HD[m_dwSLCTMdl];			

		} else 
		{	m_stVCAPT_vidTBL[ni] = HWI_VIDTYPE_SD;
			m_stVCAPT_ResTBL[ni] = g_ndefaultResol_SD[m_dwSLCTMdl];			
		}		
		m_stVCAPT_411TBL[ni] =  g_ndefault411[m_dwSLCTMdl];		
		m_stVCAPT_vexTBL[ni] =  g_ndefaultVET[m_dwSLCTMdl];		
		if (m_exsdi_mdl_flg)
		{	// 0 : normal, 1: exsdi, 2: casdi
			CAT3DI_CASDI_SET(m_stSLCTInx, (stuint)ni,  m_stVCAPT_vexTBL[ni]%3);	
		}
	}	


	if ((m_dwSLCTMdl == CAT3D_ID_HD08SH) || (m_dwSLCTMdl == CAT3D_ID_AHD08H))
	{	m_stVCAPT_MaxHDRAW = 8;
		m_stVCAPT_staHDRAW = 3;	//i> 0.1 : H264OVI, 2 : DQUAD, 3 : 0-HD, ..
		m_stVCAPT_ResTBL[0]	= VCAPT_TRON_DISABLE;
		m_stVCAPT_ResTBL[1]	= VCAPT_TRON_DISABLE;
	}




	//2. frame table *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  
	stuint		stframeTBL	[30];
	stuint		stCHFPSTBL	[HWI_MAX_VID_CH];
	stuint		nsinx, ndinx;

	if (m_stVCAPT_MaxSDCH) 
	{	GetG_VCAPT_SliceFPSTBL(IN_dwNTSC, m_stVCAPT_CH_pGRP, stframeTBL);

		for (ni = 0; ni < m_stVCAPT_MAX_GRP; ni++)
		{	
			for (nsinx = 0, nk = 0; nk < m_stVCAPT_CH_pGRP; nk++)
			{
				ndinx = ni + (nk * m_stVCAPT_MAX_GRP);
				if (ndinx >= m_stVCAPT_MaxSDCH) continue;

				stCHFPSTBL[ndinx] = stframeTBL[nsinx++];
			}

			
			//i: if default capture 1/2 fps then
			//nouse>if (m_dwSLCTMdl == CAT3D_ID_SD16EH)
			//nouse>{	stCHFPSTBL[ni] >>= 1;
			//nouse>}	
		}
	}
	if (m_stVCAPT_MaxHDCH)
	{
		for (ni = m_stVCAPT_staHDCH; ni < m_stVCAPT_MaxVICH; ni++)
		{	stCHFPSTBL[ni] = (IN_dwNTSC)? 30 : 25;
			
			//i: if dquad cap model then capture 1/2 fps 
			if ((m_dwSLCTMdl == CAT3D_ID_HD08SH) ||
				(m_dwSLCTMdl == CAT3D_ID_AHD08H) )
			{	if (ni > m_dquadcap_chinx) stCHFPSTBL[ni] >>= 1;
			}			
		}
	}
	for (ni = 0; ni < m_stVCAPT_MaxVICH; ni++)
	{	m_stVCAPT_FPSTBL[ni] = stCHFPSTBL[ni];
	}



	//3. color adjust *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  
	stuint		stBRI, stCON, stHUE, stSAT;
			
	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	
	switch (m_dwSLCTMdl)
	{	case CAT3D_ID_HSDI1	 :	case CAT3D_ID_HSDI4	:	
		case CAT3D_ID_HSDI8	 :	case CAT3D_ID_HSDI9	:	
		case CAT3D_ID_FDI60S :	case CAT3D_ID_HSDI8EX:	
		case CAT3D_ID_HD2704K:
		case CAT3D_ID_4KEXDI4:
		{	stBRI = 0x80;	stCON = 0x80; 
			stHUE = 0x80;	stSAT = 0x808080;	//INF: RGB		
		}	break;
		
		case CAT3D_ID_HD08SH:		
		{	stBRI = 0x80;	stCON = 0x80; 
			stHUE = 0x80;	stSAT = 0x80;	
			stBRI = 0x80;	stCON = 0x80; 
			stHUE = 0x80;	stSAT = 0x808080;	//INF: RGB		
		}	break;

		case CAT3D_ID_ALCAP16:
		case CAT3D_ID_AHD08H:
		{	stBRI = 0x80;	stCON = 0x80; 
			stHUE = 0x80;	stSAT = 0x80;	
			stBRI = 0x80;	stCON = 0x80; 
			stHUE = 0x80;	stSAT = 0x80;	//INF: RGB		
		}	break;



		case CAT3D_ID_CAP16E:	case CAT3D_ID_CAP4PE:
		case CAT3D_ID_CAP8PE:
		{	if (IN_dwNTSC)
			{	stBRI = 0x6E;	stCON = 0x6E; 
				stHUE = 0x02;	stSAT = 0x9E;
			} else 
			{	stBRI = 0x6E;	stCON = 0x6E; 
				stHUE = 0x00;	stSAT = 0x7E;					
			}					
		}	break;

		case CAT3D_ID_CAP32E:	case CAT3D_ID_CAP16LP:
		case CAT3D_ID_CAP16HD:
		{	stBRI = 0x80;	stCON = 0x80; 
			stHUE = 0x00;	stSAT = 0x80;			
		}	break;

		case CAT3D_ID_SD16EH:
		{	stBRI = 0x80;	stCON = 0x80; 
			stHUE = 0x80;	stSAT = 0x80;	
		}	break;


		case CAT3D_ID_CAP02	:	case CAT3D_ID_CAP04	:
		case CAT3D_ID_CAP08	:
		{	stBRI = 0x6E;	stCON = 0x6E; 
			stHUE = 0x80;	stSAT = 0x80;							
		}	break;

		case CAT3D_ID_LIV04	:	case CAT3D_ID_LIV08	:
		case CAT3D_ID_LIV16	:
		{	stBRI = 0x6E;	stCON = 0x6E; 
			stHUE = 0x08;	stSAT = 0x80;		
		}	break;

		default: break;
	}

	
	for (ni = 0; ni < m_stVCAPT_MaxVICH; ni++)
	{	
		m_stBRI	 [ni] = stBRI;			
		m_stCON	 [ni] = stCON;
		m_stHUE	 [ni] = stHUE;
		m_stSAT	 [ni] = stSAT;
		
		m_stRED	 [ni] = 0x80;
		m_stGREEN[ni] = 0x80;
		m_stBLUE [ni] = 0x80;			

	}	












	// *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  
	// * video stream 
	// *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  
	m_stVSTRM_MaxSDCH	= 0;
	m_stVSTRM_MaxHDCH	= 0;	
	m_stVSTRM_MaxVICH	= 0;	
	m_stVSTRM_staSDCH	= 0;
	m_stVSTRM_staHDCH	= 0;

	if (m_dwSLCTMdl == CAT3D_ID_SD16EH)
	{	m_stVSTRM_MaxSDCH	= 16;
		m_stVSTRM_MaxHDCH	= 0;	
		m_stVSTRM_MaxVICH	= 16;	
		m_stVSTRM_staSDCH	= 0;
		m_stVSTRM_staHDCH	= 0;
	}
	if ((m_dwSLCTMdl == CAT3D_ID_HD08SH) || (m_dwSLCTMdl == CAT3D_ID_AHD08H))
	{	m_stVSTRM_MaxSDCH	= 0;
		m_stVSTRM_MaxHDCH	= 8;	
		m_stVSTRM_MaxVICH	= 8;	
		m_stVSTRM_staSDCH	= 0;
		m_stVSTRM_staHDCH	= 0;
	}

	if (m_h264ovi_mdl_flg)
	{
		for (ni = 0; ni < m_stVSTRM_MaxVICH; ni++)
		{	if (m_dwSLCTMdl == CAT3D_ID_SD16EH)
			{	m_stVSTRM_resTBL[ni] = VSTRM_TRON_4CIF;
				m_stVSTRM_cbrTBL[ni] = 1;
				m_stVSTRM_qpvTBL[ni] = 30;
				m_stVSTRM_minTBL[ni] = 30;
				m_stVSTRM_minTBL[ni] = 50;
			} else 
			{	m_stVSTRM_resTBL[ni] = VSTRM_TRON_4HDCIF;
				m_stVSTRM_cbrTBL[ni] = 1;
				m_stVSTRM_qpvTBL[ni] = 76;
				m_stVSTRM_minTBL[ni] = 30;
				m_stVSTRM_minTBL[ni] = 50;
			}
			
			m_stVSTRM_fpsTBL[ni] = (IN_dwNTSC)? 30 : 25;
			m_stVSTRM_prfTBL[ni] = 0;		//i:0:base,1:main:2:high
			m_stVSTRM_gopTBL[ni] = (IN_dwNTSC)? 30 : 25;
			
		}
	}


	






	
	
	// *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  
	// audio capture 
	// *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  *- - -*   *- - -*  

	m_stACAPT_MaxCH	= g_nACAPT_MAX_CH [m_dwSLCTMdl];
	m_stMaxVolume	= g_nACAPT_MAX_VOL[m_dwSLCTMdl];
	m_stMaxPreAMP	= 7;	//i:2012.10.24 current coamrt card fix>
	m_stMaxRefer	= 63;	//i:2012.10.24 current coamrt card fix>


	m_stAudioRef	= 31;

	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	for (ni = 0; ni < HWI_MAX_AUD_CH; ni++)
	{	
		m_stpVolumeTBL[ni] = g_nACAPT_DEF_VOL[m_dwSLCTMdl];		
		
		if   ((m_dwSLCTMdl == CAT3D_ID_HSDI8EX) || 
			  (m_dwSLCTMdl == CAT3D_ID_HD08SH ) ||
			  (m_dwSLCTMdl == CAT3D_ID_HD2704K)	)	
			 m_stpAudResTBL	[ni] = ACAPT_TRON_48KHZ;

		else m_stpAudResTBL	[ni] = ACAPT_TRON_16KHZ;

		m_stpPreAmpTBL	[ni] =  1;		
		m_stpAudWakeTBL	[ni] = GetG_ACAPT_WakeTime(ACAPT_TRON_16KHZ);
	}

	

	//i: support resolution for audio
	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	m_stACAPT_MaxHDCH	= 0;
	ZeroMemory(m_stAudResNoTBL, sizeof(m_stAudResNoTBL));
	switch (m_dwSLCTMdl)
	{	
		case CAT3D_ID_CAP32E:	case CAT3D_ID_CAP16LP:
		case CAT3D_ID_CAP16HD:

		case CAT3D_ID_ALCAP16:
		case CAT3D_ID_SD16EH:
				m_stAudResNoTBL[ACAPT_TRON_DISABLE] = 1;
				m_stAudResNoTBL[ACAPT_TRON_8KHZ   ] = 0;
				m_stAudResNoTBL[ACAPT_TRON_11KHZ  ] = 0;
				m_stAudResNoTBL[ACAPT_TRON_16KHZ  ] = 1;
				m_stAudResNoTBL[ACAPT_TRON_22KHZ  ] = 0;
				m_stAudResNoTBL[ACAPT_TRON_48KHZ  ] = 0;
				break;

		case CAT3D_ID_CAP02	:	case CAT3D_ID_CAP04	:
		case CAT3D_ID_CAP08	:
		case CAT3D_ID_LIV04	:	case CAT3D_ID_LIV08	:
		case CAT3D_ID_LIV16	:		
				m_stAudResNoTBL[ACAPT_TRON_DISABLE] = 1;
				m_stAudResNoTBL[ACAPT_TRON_8KHZ   ] = 1;
				m_stAudResNoTBL[ACAPT_TRON_11KHZ  ] = 0;
				m_stAudResNoTBL[ACAPT_TRON_16KHZ  ] = 1;
				m_stAudResNoTBL[ACAPT_TRON_22KHZ  ] = 1;
				m_stAudResNoTBL[ACAPT_TRON_48KHZ  ] = 0;
				break;

		case CAT3D_ID_HD08SH:
		case CAT3D_ID_HSDI8EX:
		case CAT3D_ID_HD2704K:
				m_stAudResNoTBL[ACAPT_TRON_DISABLE] = 1;
				m_stAudResNoTBL[ACAPT_TRON_8KHZ   ] = 0;
				m_stAudResNoTBL[ACAPT_TRON_11KHZ  ] = 0;
				m_stAudResNoTBL[ACAPT_TRON_16KHZ  ] = 0;
				m_stAudResNoTBL[ACAPT_TRON_22KHZ  ] = 0;
				m_stAudResNoTBL[ACAPT_TRON_48KHZ  ] = 1;
				m_stACAPT_MaxHDCH					= 1;
				break;

		case CAT3D_ID_AHD08H:		
			m_stAudResNoTBL[ACAPT_TRON_DISABLE] = 1;
			m_stAudResNoTBL[ACAPT_TRON_8KHZ   ] = 1;
			m_stAudResNoTBL[ACAPT_TRON_11KHZ  ] = 0;
			m_stAudResNoTBL[ACAPT_TRON_16KHZ  ] = 1;
			m_stAudResNoTBL[ACAPT_TRON_22KHZ  ] = 0;
			m_stAudResNoTBL[ACAPT_TRON_48KHZ  ] = 0;
			m_stACAPT_MaxHDCH					= 0;
			break;

		default : 
				m_stAudResNoTBL[ACAPT_TRON_DISABLE] = 1;				
				break;

	}
	
}





































// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// * @	DQUAD SCREEN
//				  
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::_QUAD_SCRN	(DWORD in_dwstach, DWORD in_dwmode)
{
	boxreturn0((m_nOPEN ==    0), DBGTR, _T("board not opened\n"));


	CAT3DI_DQUAD_Mode	(m_stSLCTInx, (stuint)in_dwstach, (stuint)in_dwmode);
	
}





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::_TVQUAD_SCRN	(DWORD in_dwstach, DWORD in_dwmode)
{
	boxreturn0((m_nOPEN ==    0), DBGTR, _T(" board not opened\n"));

	
	CAT3DI_TVQUAD_Mode	(m_stSLCTInx, (stuint)in_dwstach, (stuint)in_dwmode);

}



































































// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// * @ STRM (H264)
//				  
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-



// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
BOOL	cHWIF::_VSTRM_STA
(DWORD IN_dwCH, DWORD IN_dwRC, DWORD IN_dwFPS,   DWORD IN_dwPROF, DWORD IN_dwGOPN, 
 DWORD IN_dwCBR,DWORD IN_dwQP, DWORD IN_dwMINQP, DWORD IN_dwMAXQP)
{	
	boxreturn((m_nOPEN ==    0),			FALSE, DBGTR, _T("no vstrm sta, board not opened\n"));
	boxreturn((m_stVSTRM_staTBL[IN_dwCH]),	FALSE, DBGTR, _T("vstrm already start\n"));

	if ( !((m_dwSLCTMdl == CAT3D_ID_HD08SH) || 
		   (m_dwSLCTMdl == CAT3D_ID_AHD08H) || 
		   (m_dwSLCTMdl == CAT3D_ID_SD16EH) ) )
		return FALSE;


	stuint		strv;
	stuint		stCH		= (stuint)IN_dwCH;
	

	stuint	stVS_opttbl[10];
	
	stVS_opttbl[HENC_RESOLC_OFST]		= (stuint)IN_dwRC;
	stVS_opttbl[HENC_FPS_OFST]			= (stuint)IN_dwFPS;
	stVS_opttbl[HENC_PROFILE_OFST]		= (stuint)IN_dwPROF;
	stVS_opttbl[HENC_GOPN_OFST]			= (stuint)IN_dwGOPN;
	stVS_opttbl[HENC_CBR_OFST]			= (stuint)IN_dwCBR;
	stVS_opttbl[HENC_KBPSQP_OFST]		= (stuint)IN_dwQP;
	stVS_opttbl[HENC_MINQP_OFST]		= (stuint)IN_dwMINQP;
	stVS_opttbl[HENC_MAXQP_OFST]		= (stuint)IN_dwMAXQP;


	strv = CAT3DI_HENC_Start(m_stSLCTInx, stCH, &(stVS_opttbl[0]), VSTRM_STA_OPTCNT);

	LogPrintf("CALL : %X = CAT3DI_HENC_Start(%X,%X,X,..)", strv, m_stSLCTInx, stCH);	
	if (strv != CAT3DI_FUNC_OK)  
	{	LogPrintf("CALL : %X CH VSTRM START FAIL", stCH);
		return FALSE;
	}


	m_stVSTRM_staTBL[stCH] = 1;
	return TRUE;


}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::_VSTRM_STO(DWORD IN_dwCH)
{	
	if (m_nOPEN == 0) return;
	if (m_stVSTRM_staTBL[IN_dwCH] == 0) return;

	
	stuint stCH = (stuint)IN_dwCH;

	
	CAT3DI_HENC_Stop(m_stSLCTInx, stCH);
	LogPrintf("CALL : CAT3DI_HENC_Stop(%X,%X)", m_stSLCTInx, stCH);	
	

	m_stVSTRM_staTBL[IN_dwCH] = 0;
}









































// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// * @ VCAPT
//				  
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::ADC_frameWait(DWORD IN_dwCH)
{
	boxreturn0((m_nOPEN ==    0), DBGTR, _T("frameWait, board not opened\n"));


	stuint		stADC, stMax;
	stuint		stOuTBL[HWI_MAX_VID_CH];	

	stMax = (m_dwNTSC)? 30 : 25;
	stADC = GetG_VCAPT_CHtoGRP(m_dwSLCTMdl, IN_dwCH);	
	

	for (int nk = 0; nk < HWI_MAX_VID_CH; nk++)
	{	stOuTBL[nk] = SLICE_SLEEP_CODE;
	}


	CAT3DI_VideoSliceCtrl	(m_stSLCTInx, stADC, &(stOuTBL[0]), stMax, 0);
}




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::ADC_frameCtrl(DWORD IN_dwCH)
{		
	boxreturn0((m_nOPEN		 ==    0), DBGTR, _T("frameCtrl, board not opened\n"));


	stuint		strv,		sti;
	stuint		stChkCH;
	stuint		stADC,		stMax,		stDLY;
	stuint		stInTBL[HWI_MAX_VID_CH],stIoTBL[64];			//b.of> + 34 cnt : no use 


	stMax = (m_dwNTSC)? 30 : 25;
	stADC = GetG_VCAPT_CHtoGRP(m_dwSLCTMdl, IN_dwCH);
	stDLY = (m_dwSLCTMdl == CAT3D_ID_HSDI4)? 16 : 0;	//b.of> 1x PCIe 4CH HD => change 15 fps


	ZeroMemory(stInTBL, sizeof(stInTBL));
	ZeroMemory(stIoTBL, sizeof(stIoTBL));
	
	for (sti = 0; sti < m_stVCAPT_CH_pGRP; sti++)
	{	stChkCH   = stADC + (sti * m_stVCAPT_MAX_GRP);
		if ((m_stVCAPT_staTBL[stChkCH] == 0) && (stChkCH != IN_dwCH)) continue;

		stInTBL[stChkCH] =  m_stVCAPT_FPSTBL[stChkCH];
	}

	Get_frameTBL(stInTBL, HWI_MAX_VID_CH,  stIoTBL, stMax);	

	//i> if odd channel rotate . b.of> allch 15 frps -> band width
	if (IN_dwCH & 1)
	{	stuint	stTemp  = stIoTBL[0];
		for (sti = 1; sti < stMax; sti++)
		{	stIoTBL[sti-1] = stIoTBL[sti];
		}
		stIoTBL[stMax-1] = stTemp;
	}


	CopyMemory(&(m_stVCAPT_divTBL[stADC][0]), stIoTBL, stMax * sizeof(stuint));



	//i:DBG string	: eye check 
	BYTE	btASCII[256];
	int		nASCIInx = 0;

	ZeroMemory(btASCII, sizeof(btASCII));

	for (sti = 0; sti < stMax; sti++) 
	{	stChkCH  = stIoTBL[sti];
		if (stChkCH == SLICE_SLEEP_CODE)
		{	StringCbPrintf  ((TCHAR *)&(btASCII[nASCIInx]), 5, _T("%02d,"), SLICE_SLEEP_CODE);			 
			nASCIInx += 3;
			continue;
		}

		stChkCH %= m_stVCAPT_MAX_GRP;
		boxassert((stChkCH != stADC), BOXEXIT, _T("ivalid frame table"));
		
		StringCbPrintf  ((TCHAR *)&(btASCII[nASCIInx]), 5, _T("%02d,"), stIoTBL[sti]);
		nASCIInx += 3;
	}



	boxlog(1, EYECHK, _T("GRP:%02d, CH:%02d][%s"), stADC, IN_dwCH, btASCII);



	
	strv = CAT3DI_VideoSliceCtrl	(m_stSLCTInx, stADC, &(m_stVCAPT_divTBL[stADC][0]), stMax, stDLY);
	LogPrintf("CALL : %X = CAT3DI_VideoSliceCtrl(%X,%X,X,X, 0)", strv, m_stSLCTInx,  stADC);	
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
stuint	cHWIF::_VCAPT_frameChk(DWORD IN_dwCH, DWORD IN_dtframeTBL[])
{
	boxassert ((IN_dtframeTBL== NULL), BOXEXIT, _T("input pointer (IN_FUNC) is null\n"));
	boxreturn((m_nOPEN ==    0), FALSE, DBGTR,  _T("no vid sta, board not opened\n"));

	stuint		sti,  stMax, stADC;
	stuint		stChkCH,     stUseFPS;

	stMax		= (m_dwNTSC)? 30 : 25;
	stADC		= GetG_VCAPT_CHtoGRP(m_dwSLCTMdl, IN_dwCH);
	stUseFPS	= 0;

	//i:if HD then no use slice frame table
	if (IN_dwCH >= m_stVCAPT_MaxSDCH) return stMax;
		

	for (sti = 0; sti < m_stVCAPT_CH_pGRP; sti++)
	{	stChkCH   = stADC + (sti * m_stVCAPT_MAX_GRP);
		stUseFPS += IN_dtframeTBL[stChkCH];
	}
	return stUseFPS;
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::_VCAPT_frameWr(DWORD IN_dwSTACH, DWORD IN_dwENCH, DWORD IN_dtframeTBL[])
{
	boxassert ((IN_dtframeTBL== NULL), BOXEXIT, _T("input pointer (IN_FUNC) is null\n"));
	boxreturn0((m_nOPEN		 ==    0), DBGTR,   _T("no vid sta, board not opened\n"));


	stuint		sti,  stk,  stCH;	
	stuint		stBUP  [HWI_MAX_VID_CH]; 

	stuint		stUPT;
	stuint		stUPTBL  [HWI_MAX_VID_CH]; 
	

	
	//i: table update
	for (sti = IN_dwSTACH; sti < IN_dwENCH; sti++)
	{	stBUP           [sti] = m_stVCAPT_FPSTBL[sti];
		m_stVCAPT_FPSTBL[sti] = (stuint)IN_dtframeTBL[sti];
	}

	//i: find update adc table
	stUPT = 0;
	for (sti = 0; sti < m_stVCAPT_MAX_GRP; sti++)
	{
		for (stk = 0; stk < m_stVCAPT_CH_pGRP; stk++)
		{	stCH	= sti + (stk * m_stVCAPT_MAX_GRP);

			if (stBUP           [stCH] == IN_dtframeTBL[stCH]) continue;		
			if (m_stVCAPT_staTBL[stCH] ==			        0) continue;

			stUPTBL[stUPT] = sti;
			stUPT++;
		}
	}

	//i: frame control
	for (sti = 0; sti < stUPT; sti++)
	{
		stCH = stUPTBL[sti];
		ADC_frameCtrl((DWORD)stCH);
	}

	
}



// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
void	cHWIF::_VCAPT_colorWr(DWORD IN_dwCH, DWORD IN_dwHWICNM, DWORD IN_dwValue)
{
	boxreturn0((m_nOPEN ==    0), DBGTR, _T("no vid sta, board not opened\n"));


	stuint	strv;
	stuint	stRGB;
	stuint	stch = (stuint)IN_dwCH;

	switch (IN_dwHWICNM)
	{	case HWISD_BRI	:	m_stBRI  [stch]	= IN_dwValue;	
							strv = CAT3DI_BRIGHTNESSControl (m_stSLCTInx, stch, (int)IN_dwValue);
							break;
		case HWISD_CON	:	m_stCON  [stch]	= IN_dwValue;	
							strv = CAT3DI_CONTRASTControl   (m_stSLCTInx, stch, (int)IN_dwValue);
							break;
		case HWISD_HUE	:	m_stHUE  [stch]	= IN_dwValue;	
							strv = CAT3DI_HUEControl        (m_stSLCTInx, stch, (int)IN_dwValue);
							break;
		case HWISD_SAT	:	m_stSAT  [stch]	= IN_dwValue;
							strv = CAT3DI_SATURATIONUControl(m_stSLCTInx, stch, (int)IN_dwValue);
							break;	
										 

		case HWIHD_BRI	:	m_stBRI  [stch]	= IN_dwValue;	
							strv = CAT3DI_BRIGHTNESSControl (m_stSLCTInx, stch, (int)IN_dwValue);			
							break;			
		case HWIHD_RED	:	m_stRED  [stch]	= IN_dwValue;
							stRGB  = ((m_stBLUE [stch] & 0xFF) << 16);
							stRGB |= ((m_stGREEN[stch] & 0xFF) <<  8);
							stRGB |= ((m_stRED  [stch] & 0xFF)      );
							strv = CAT3DI_SATURATIONUControl (m_stSLCTInx, stch, (int)stRGB);
							break;
		case HWIHD_GRN	:	m_stGREEN[stch]	= IN_dwValue;	
							stRGB  = ((m_stBLUE [stch] & 0xFF) << 16);
							stRGB |= ((m_stGREEN[stch] & 0xFF) <<  8);
							stRGB |= ((m_stRED  [stch] & 0xFF)      );
							strv = CAT3DI_SATURATIONUControl (m_stSLCTInx, stch, (int)stRGB);
							break;
		case HWIHD_BLU	:	m_stBLUE [stch]	= IN_dwValue;	
							stRGB  = ((m_stBLUE [stch] & 0xFF) << 16);
							stRGB |= ((m_stGREEN[stch] & 0xFF) <<  8);
							stRGB |= ((m_stRED  [stch] & 0xFF)      );	
							strv = CAT3DI_SATURATIONUControl (m_stSLCTInx, stch, (int)stRGB);
							break;

		default : return;
	}	

}





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//del>void	cHWIF::_960H_SET	(DWORD in_dwch, DWORD IN_dwEn)
//del>{
//del>boxreturn0((m_nOPEN          == 0), DBGTR, _T("frameWait, board not opened\n"));	
//del>	boxreturn0((m_st960h_mdl_flg == 0), DBGTR, _T("not support 960h model\n"));
//del>	
//del>
//del>	m_st960h_chtbl[in_dwch] = (IN_dwEn)? 1 : 0;	
//del>
//del>}

// -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- 
void	cHWIF::_VCAPT_VET(DWORD IN_dwCH,  DWORD IN_dwEXT, DWORD IN_dwOVI)
{
	boxreturn0((m_nOPEN ==				0), DBGTR, _T("no vid sta, board not opened\n"));
	boxreturn0((m_stVCAPT_staTBL[IN_dwCH]), DBGTR, _T("video  sta, not changed\n"));
		

	stuint	strv, stcvrt;

	if (m_st960h_mdl_flg)
	{	stcvrt = (stuint)((IN_dwEXT)? 1 : 0);
		
		m_stVCAPT_vexTBL[IN_dwCH] = stcvrt;
		strv = CAT3DI_SD960_SET(m_stSLCTInx, (stuint)IN_dwCH, stcvrt);
		return;
	}

	if ((m_exsdi_mdl_flg) && (IN_dwCH <= 7))	// ext channel : 0 ~ 7 : total 8ch
	{	stcvrt = (stuint)((IN_dwOVI)%3);		// 0 : normal, 1: exsdi, 2: casdi
	
		m_exsdi_chtbl[IN_dwCH] = stcvrt;	
		strv = CAT3DI_CASDI_SET(m_stSLCTInx, (stuint)IN_dwCH, stcvrt);
		return;
	}

	
	//later: universal model check
	//..
	//..


}









// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
BOOL	cHWIF::_VCAPT_STA
(void	*IN_FUNC, void *IN_ARGS,  DWORD IN_dwCH, 
 DWORD   IN_dwRC, DWORD IN_dw411, DWORD IN_dw960, DWORD IN_dwOVI)

{	
	boxassert((IN_FUNC == NULL), BOXEXIT, _T("input pointer (IN_FUNC) is null\n"));
	boxassert((IN_ARGS == NULL), BOXEXIT, _T("input pointer (IN_ARGS) is null\n"));
	boxreturn((m_nOPEN == 0), FALSE, DBGTR, _T("no vid sta, board not opened\n"));

	DWORD		nCH;
	DWORD		dwID, dwYC;
	stuint		stCH, sdMAXFPS, strv;

	nCH = IN_dwCH;
	stCH = (stuint)IN_dwCH;
	sdMAXFPS = (m_dwNTSC) ? 30 : 25;



	dwYC = IN_dw411;
	//dle>if ((m_dwSLCTMdl == CAT3D_ID_CAP32E) || (m_dwSLCTMdl == CAT3D_ID_SD16EH))
	if (m_st960h_mdl_flg)
	{
		dwYC = (IN_dw960) ? (IN_dw411 | VID_EXT_10) : IN_dw411;
	}

	if (m_exsdi_mdl_flg)
	{
		switch (IN_dwOVI)
		{
		case 1: dwYC = (IN_dw411 | VID_EXT_20);	break;
		case 2: dwYC = (IN_dw411 | VID_EXT_40);	break;
		case 0:
		default: dwYC = (IN_dw411);
		}
		m_exsdi_chtbl[stCH] = (IN_dwOVI) ? 1 : 0;
	}


	if (IN_dwCH < m_stVCAPT_MaxSDCH)
	{
		strv = CAT3DI_BRIGHTNESSControl(m_stSLCTInx, stCH, (int)m_stBRI[stCH]);
		strv = CAT3DI_CONTRASTControl(m_stSLCTInx, stCH, (int)m_stCON[stCH]);
		strv = CAT3DI_HUEControl(m_stSLCTInx, stCH, (int)m_stHUE[stCH]);
		strv = CAT3DI_SATURATIONUControl(m_stSLCTInx, stCH, (int)m_stSAT[stCH]);

		m_stVCAPT_vidTBL[IN_dwCH] = (IN_dw960) ? HWI_VIDTYPE_EFFIO : HWI_VIDTYPE_SD;

	}
	else if ((IN_dwCH >= m_stVCAPT_staHDCH) && (IN_dwCH < m_stVCAPT_MaxVICH))
	{
		stuint	stRGB;
		strv = CAT3DI_BRIGHTNESSControl(m_stSLCTInx, stCH, (int)m_stBRI[stCH]);
		stRGB = ((m_stBLUE[stCH] & 0xFF) << 16);
		stRGB |= ((m_stGREEN[stCH] & 0xFF) << 8);
		stRGB |= ((m_stRED[stCH] & 0xFF));
		strv = CAT3DI_SATURATIONUControl(m_stSLCTInx, stCH, (int)stRGB);

		m_stVCAPT_vidTBL[IN_dwCH] = HWI_VIDTYPE_HD;

	}
	else
	{
		boxreturn((m_nOPEN == 0), FALSE, DBGTR, _T("invalid channel nummber \n"));
	}



	ADC_frameWait(nCH);

	strv = CAT3DI_CaptureVideoTransferON(m_stSLCTInx, stCH, (stuint)IN_dwRC, (stuint)dwYC);
	LogPrintf("CALL : %X = CAT3DI_CaptureVideoTransferON(%X,%X,X,1)", strv, m_stSLCTInx, stCH);
	if (strv != CAT3DI_FUNC_OK)
	{
		LogPrintf("CALL : %X CH VIDEO DMA START FAIL", stCH);
		return FALSE;
	}




	m_tpVidTHRD[nCH].nVidDMA_ACT = 1;
	m_tpVidTHRD[nCH].nThread_ACT = 1;
	m_tpVidTHRD[nCH].stCH = stCH;
	m_tpVidTHRD[nCH].sdBDINX = m_stSLCTInx;

	m_tpVidTHRD[nCH].dwCH_XZ = GetG_VCAPT_XZ(IN_dw960, IN_dwRC);
	m_tpVidTHRD[nCH].dwCH_YZ = GetG_VCAPT_YZ(m_dwNTSC, IN_dwRC);
	m_tpVidTHRD[nCH].dwCH_YC = dwYC;

	m_tpVidTHRD[nCH].vfExtFunc = (EXT_FUNC)IN_FUNC;
	m_tpVidTHRD[nCH].vpExtArg = IN_ARGS;
	m_tpVidTHRD[nCH].vpthis = this;

	m_tpVidTHRD[nCH].pcThread_ID =
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)VidTHRD_FUNC, &(m_tpVidTHRD[nCH]), 0, (LPDWORD)&dwID);

#ifdef MAX_THRD_CTRL
	SetThreadAffinityMask(m_tpVidTHRD[nCH].pcThread_ID, 1 << g_nvid_thrd_tbl[m_vidthrd_cnt]);
	m_vidthrd_cnt++;
	m_vidthrd_cnt %= MAX_THRD_CTRL;
#endif

	m_tpVidTHRD[nCH].dwThread_ID = dwID;



	if (m_tpVidTHRD[nCH].pcThread_ID == NULL)
	{
		LogPrintf("CALL : %X CH video thread FAIL", nCH);
		_VCAPT_STO(IN_dwCH);
		return FALSE;
	}

	ADC_frameCtrl(nCH);



	//i: if (h.264 ovi model) then process priority up
	//i: if (h.264 ovi model and h264 ovi ch) then  thread priority up
	int		nthread_priority_up = 0;

	if ((m_dwSLCTMdl == CAT3D_ID_SD16EH) && (nCH == m_h264ovi.stch_sta))
	{
		nthread_priority_up = 1;
	}
	if ((m_dwSLCTMdl == CAT3D_ID_HD08SH) && (nCH < m_h264ovi.stch_cnt))
	{
		nthread_priority_up = 1;
	}
	if ((m_dwSLCTMdl == CAT3D_ID_AHD08H) && (nCH < m_h264ovi.stch_cnt))
	{
		nthread_priority_up = 1;
	}
	if (nthread_priority_up)
	{
		if (!SetThreadPriority(m_tpVidTHRD[nCH].pcThread_ID, THREAD_PRIORITY_HIGHEST))
		{
			DWORD dwErr = GetLastError();
			boxlog(1, DBGTR, _T("h.264 ovi model Thread priority up : HIGHEST : fail : %d"), dwErr);
		}
		else
		{
			boxlog(1, DBGTR, _T("h.264 ovi model Thread priority up : HIGHEST : success"));
		}
	}





	m_stVCAPT_staTBL[nCH] = 1;
	return TRUE;
	
	
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::_VCAPT_STO(DWORD IN_dwCH)
{
	int		nCH;
	stuint	stCH;
	

	if (m_nOPEN == 0) return;
	if (m_stVCAPT_staTBL[IN_dwCH] == 0) return;

	
	nCH  = (int   )IN_dwCH;
	stCH = (stuint)IN_dwCH;


	if (m_tpVidTHRD[nCH].pcThread_ID)  
	{	m_tpVidTHRD[nCH].nThread_ACT	= 0;
	stuint ack = CAT3DI_CancelLockWait4GetImageAddress(m_stSLCTInx, stCH);
		LogPrintf("CALL : CAT3DI_CancelLockWait4GetImageAddress(%X,%X)", m_stSLCTInx, stCH);	
	
// 		WaitForSingleObject(m_tpVidTHRD[nCH].pcThread_ID, INFINITE);	
// 		m_tpVidTHRD[nCH].pcThread_ID = NULL;


		DWORD  dwExitCode = NULL;
		GetExitCodeThread(m_tpVidTHRD[nCH].pcThread_ID, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_tpVidTHRD[nCH].pcThread_ID, dwExitCode);
			WaitForSingleObject(m_tpVidTHRD[nCH].pcThread_ID, 3000);
			CloseHandle(m_tpVidTHRD[nCH].pcThread_ID);
			m_tpVidTHRD[nCH].pcThread_ID = NULL;
		}



	}

	if (m_tpVidTHRD[nCH].nVidDMA_ACT)
	{	CAT3DI_CaptureVideoTransferOFF(m_stSLCTInx, stCH);
		LogPrintf("CALL : CAT3DI_CaptureVideoTransferOFF(%X,%X)", m_stSLCTInx, stCH);	

		m_tpVidTHRD[nCH].nVidDMA_ACT = 0;
	}

	m_stVCAPT_staTBL[IN_dwCH] = 0;
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::_VCAPT_GetFPS(DWORD *IO_dtTBL)
{
	boxassert ((IO_dtTBL== NULL), BOXEXIT, _T("input pointer (IN_FUNC) is null\n"));
	boxreturn0((m_nOPEN ==    0), DBGTR,   _T("func fail, board not opened\n"));

	stuint	sti, strv;
	stuint	stpTBL[HWI_MAX_VID_CH];	
	strv	= CAT3DI_GET_CaptureFPS(m_stSLCTInx, stpTBL);


	for (sti = 0; sti < (int)m_stVCAPT_MaxVICH; sti++)
	{	if (m_stVCAPT_staTBL[sti] == 0)
			 IO_dtTBL[sti] = 0;
		else IO_dtTBL[sti] = (DWORD)stpTBL[sti];
	}

	//del>LogPrintf("CALL : %X = CAT3DI_GET_CaptureFPS(%X,%X)", strv);	
}












































// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// * @ ACAPT
//				  
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

BOOL	cHWIF::_ACAPT_STA
(void	*IN_FUNC, void *IN_ARGS, DWORD IN_dwCH, DWORD IN_dwTRON, DWORD IN_dwSTRO)

{
	boxassert((IN_FUNC == NULL), BOXEXIT, _T("input pointer (IN_FUNC) is null\n"));
	boxassert((IN_ARGS == NULL), BOXEXIT, _T("input pointer (IN_ARGS) is null\n"));
	boxreturn((m_nOPEN==    0), FALSE,   DBGTR, _T("no aud sta, board not opened\n"));


	DWORD		dwWakeCNT, dwConstON;
	DWORD		dwID, nCH;
	stuint		stCH, sdRV;
	

	nCH  = IN_dwCH;
	stCH = (stuint)IN_dwCH;

	sdRV = CAT3DI_CaptureAudioReference	(m_stSLCTInx, m_stAudioRef);
	sdRV = CAT3DI_CaptureAudioVolume	(m_stSLCTInx, stCH, m_stpVolumeTBL[nCH]);
	sdRV = CAT3DI_CaptureAudioPreAMP	(m_stSLCTInx, stCH, m_stpPreAmpTBL[nCH]);
		
	dwWakeCNT	= GetG_ACAPT_WakeTime(IN_dwTRON);

	dwConstON   = IN_dwTRON;
	if ((IN_dwSTRO) && (m_dwSLCTMdl == CAT3D_ID_HSDI8EX))
	{	dwConstON = IN_dwTRON | 0x8000;		
	}

		
	sdRV		= CAT3DI_CaptureAudioTransferON(m_stSLCTInx, (stuint)nCH, (stuint)dwConstON, (stuint)dwWakeCNT);
	LogPrintf("CALL : %X = CAT3DI_CaptureAudioTransferON(%X,%X,X,1)", sdRV, m_stSLCTInx, nCH);	
	if (sdRV != CAT3DI_FUNC_OK)  
	{	LogPrintf("CALL : %X CH AUDIO DMA START FAIL", nCH);			
		return FALSE;
	}	


	m_tpAudTHRD[nCH].nAudDMA_ACT= 1;
	m_tpAudTHRD[nCH].nThread_ACT= 1;
	m_tpAudTHRD[nCH].stCH		= stCH;
	m_tpAudTHRD[nCH].sdBDINX	= m_stSLCTInx;
	m_tpAudTHRD[nCH].dwCH_AUDSZ	= GetG_ACAPT_AudSiz(m_dwSLCTMdl, IN_dwTRON);		
	m_tpAudTHRD[nCH].vfExtFunc	= (EXT_FUNC)IN_FUNC;
	m_tpAudTHRD[nCH].vpExtArg	= IN_ARGS;
	m_tpAudTHRD[nCH].pcThread_ID= CreateThread(	NULL, 0, (LPTHREAD_START_ROUTINE)AudTHRD_FUNC, 
												&(m_tpAudTHRD[nCH]), 0, (LPDWORD)&dwID);

#ifdef MAX_THRD_CTRL
	SetThreadAffinityMask(m_tpVidTHRD[nCH].pcThread_ID, 1 << g_netc_thrd_tbl[m_vidthrd_cnt]);
	m_vidthrd_cnt++;
	m_vidthrd_cnt %= MAX_THRD_CTRL;
#endif


	m_tpAudTHRD[nCH].dwThread_ID= dwID;
	if (m_tpAudTHRD[nCH].pcThread_ID == NULL) 
	{	LogPrintf("CALL : %X CH Audio Create Thread FAIL", nCH);				
	}

	if (m_tpAudTHRD[nCH].pcThread_ID)  
	{	m_stpAudSTA[nCH] = 1;

		//del>Sleep(10);

		return TRUE;
	}	

	_ACAPT_STO(IN_dwCH);
	return FALSE;
}


// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
void	cHWIF::_ACAPT_STO(DWORD IN_dwCH)
{
	int		nCH;
	
	if (m_stpAudSTA[IN_dwCH] == 0) return;	

	
	nCH  = (int) IN_dwCH;		

	if (m_tpAudTHRD[nCH].pcThread_ID)  
	{	m_tpAudTHRD[nCH].nThread_ACT	= 0;	//i>ESC command		
		CAT3DI_CancelLockWait4GetSoundAddress(m_stSLCTInx, (stuint)nCH);
		LogPrintf("CALL : CAT3DI_CancelLockWait4GetSoundAddress(%X,%X)", m_stSLCTInx, nCH);	
	
		WaitForSingleObject(m_tpAudTHRD[nCH].pcThread_ID, INFINITE);	
		m_tpAudTHRD[nCH].nThread_ACT	= 0;	//i>ESC command		
		m_tpAudTHRD[nCH].vfExtFunc		= NULL;
		m_tpAudTHRD[nCH].vpExtArg		= NULL;	
		m_tpAudTHRD[nCH].pcThread_ID	= NULL;
		m_tpAudTHRD[nCH].dwThread_ID	= 0;		
	}

	if (m_tpAudTHRD[nCH].nAudDMA_ACT)
	{	CAT3DI_CaptureAudioTransferOFF(m_stSLCTInx, (stuint)nCH);
		LogPrintf("CALL : CAT3DI_CaptureAudioTransferOFF(%X,%X)", m_stSLCTInx, nCH);	

		m_tpAudTHRD[nCH].nAudDMA_ACT	= 0;
	}

	m_stpAudSTA[IN_dwCH] = 0;
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::_ACAPT_VOL	(DWORD IN_dwCH, DWORD IN_dwD)
{	
	stuint		strv;
	if (m_nOPEN == 0) return;

	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	if (!((m_dwSLCTMdl == CAT3D_ID_LIV04) || (m_dwSLCTMdl == CAT3D_ID_LIV08) || (m_dwSLCTMdl == CAT3D_ID_LIV16) || 
		  (m_dwSLCTMdl == CAT3D_ID_CAP02) || (m_dwSLCTMdl == CAT3D_ID_CAP04) || (m_dwSLCTMdl == CAT3D_ID_CAP08) ||
		  (m_dwSLCTMdl == CAT3D_ID_CAP4PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP8PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP16E)||
		  (m_dwSLCTMdl == CAT3D_ID_SD16EH)|| (m_dwSLCTMdl == CAT3D_ID_ALCAP16) )	)
		   return;		

	m_stpVolumeTBL[IN_dwCH] = (stuint)IN_dwD;

	strv	= CAT3DI_CaptureAudioVolume(m_stSLCTInx, IN_dwCH, IN_dwD);
	LogPrintf("CALL : %X = CAT3DI_CaptureAudioVolume(%X,%X)", strv, m_stSLCTInx, IN_dwCH, IN_dwD);		
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::_ACAPT_AMP	(DWORD IN_dwCH, DWORD IN_dwD)
{	
	stuint		strv;
	if (m_nOPEN == 0) return;

	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	if (!((m_dwSLCTMdl == CAT3D_ID_LIV04) || (m_dwSLCTMdl == CAT3D_ID_LIV08) || (m_dwSLCTMdl == CAT3D_ID_LIV16) || 
		  (m_dwSLCTMdl == CAT3D_ID_CAP02) || (m_dwSLCTMdl == CAT3D_ID_CAP04) || (m_dwSLCTMdl == CAT3D_ID_CAP08) ||
		  (m_dwSLCTMdl == CAT3D_ID_CAP4PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP8PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP16E) )	)
		   return;		

	m_stpVolumeTBL[IN_dwCH] = (stuint)IN_dwD;

	strv	= CAT3DI_CaptureAudioPreAMP(m_stSLCTInx, IN_dwCH, IN_dwD);
	LogPrintf("CALL : %X = CAT3DI_CaptureAudioPreAMP(%X,%X)", strv, m_stSLCTInx, IN_dwCH, IN_dwD);	
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	cHWIF::_ACAPT_REF	(DWORD IN_dwD)
{	
	stuint		strv;
	if (m_nOPEN == 0) return;

	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	if (!((m_dwSLCTMdl == CAT3D_ID_LIV04) || (m_dwSLCTMdl == CAT3D_ID_LIV08) || (m_dwSLCTMdl == CAT3D_ID_LIV16) || 
		  (m_dwSLCTMdl == CAT3D_ID_CAP02) || (m_dwSLCTMdl == CAT3D_ID_CAP04) || (m_dwSLCTMdl == CAT3D_ID_CAP08) ||
		  (m_dwSLCTMdl == CAT3D_ID_CAP4PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP8PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP16E) )	)
		   return;		

	m_stAudioRef = (stuint)IN_dwD;

	strv	= CAT3DI_CaptureAudioReference(m_stSLCTInx, IN_dwD);
	LogPrintf("CALL : %X = CAT3DI_CaptureAudioReference(%X,%X)", strv, m_stSLCTInx, IN_dwD);	
}



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	cHWIF::_ACAPT_GET_BSZ(DWORD IN_dwD)
{
	DWORD  dwrv = GetG_ACAPT_AudSiz(m_dwSLCTMdl, IN_dwD);
	return dwrv;
}















// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// * @ UART
//				  
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//inf: RTS, CTS CTRL : Append later
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define CAT3DI_RTS_CTRL(a,b)	CAT3DI_IO_STATUS(a, 92, 0, 0x1E0, (b&1))	

int		cHWIF::_UART_RTS(DWORD IN_dwEN)
{
	boxreturn((m_nOPEN == 0), -1, DBGTR, _T("_UART_RTS() but not opend\n"));

	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	if (!((m_dwSLCTMdl == CAT3D_ID_LIV04) || (m_dwSLCTMdl == CAT3D_ID_LIV08) || (m_dwSLCTMdl == CAT3D_ID_LIV16) || 
		  (m_dwSLCTMdl == CAT3D_ID_CAP02) || (m_dwSLCTMdl == CAT3D_ID_CAP04) || (m_dwSLCTMdl == CAT3D_ID_CAP08) ||
		  (m_dwSLCTMdl == CAT3D_ID_CAP4PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP8PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP16E) )	)
		   return -1;		

	
	CAT3DI_RTS_CTRL(m_stSLCTInx, IN_dwEN);
	return 1;
}








// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
int 	cHWIF::_UART_CFG(DWORD IN_dwBPS_inx, DWORD IN_dwPAR_inx)
{
	boxreturn((m_nOPEN == 0), -1, DBGTR, _T("_UART_CFG() but not opend\n"));

	
	stuint	sdRv;
	stuint	sdPAR, sdBPS;


	sdBPS = IN_dwBPS_inx % MAX_RUART_BDR;	//inf: 6
	sdPAR = IN_dwPAR_inx % MAX_RUART_PAR;	//inf: 4

	m_dwuart_BPS_inx = (DWORD)sdBPS;
	
	sdRv = CAT3DI_RUART_CONFIG(m_stSLCTInx, sdBPS, sdPAR);
	_UART_RTS(0);		//inf: RTS, CTS CTRL : Append later

	LogPrintf("CALL : %X = CAT3DI_RUART_CONFIG(%X,%X, %X)", sdRv, m_stSLCTInx, sdBPS, sdPAR);	

	if (sdRv == CAT3DI_FUNC_OK) m_dwuart_CFG_done = 1;	
	return (int) sdRv;
}

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
static	DWORD		g_dtSPDTbl[] = 
{	1000000/120,	1000000/240,	1000000/480, 
	1000000/9600,   1000000/1920,	1000000/3840,  
};

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int 	cHWIF::_UART_TX(BYTE *IN_bpTx, DWORD IN_nTxSZ)
{
	boxassert((IN_bpTx  == NULL), BOXEXIT,	_T("input pointer (IN_bpTx) is null\n"));
	boxreturn((IN_nTxSZ ==    0), -1,		DBGTR, _T(" IN_nTxSZ is 0\n"));
	boxreturn((m_nOPEN ==     0), -2,		DBGTR, _T("_UART_TX() but not opend\n"));
	boxreturn((m_dwuart_CFG_done == 0), -3,	DBGTR, _T("_UART_TX() but config port \n"));


	DWORD		dwSTic, dwSLP, dwDly = 500;
	stuint		sdRv;


	_UART_RTS(1);		//inf: RTS, CTS CTRL : Append later
	
	

	dwSTic = GetTickCount() + dwDly;
	sdRv   = CAT3DI_RUART_WRITE(m_stSLCTInx, IN_bpTx, (stuint)IN_nTxSZ, (stuint)dwDly);
	LogPrintf("CALL : %X = CAT3DI_RUART_WRITE(%X, PTR, %X, %X)", sdRv, m_stSLCTInx, IN_nTxSZ, dwDly);	


	dwSLP = 0;
	//i:[if (dwDly is 0) then under line process]
	//i:dwSLP = g_dtSPDTbl[m_dwuart_BPS_inx] * IN_nTxSZ / 1000;
	//i:dwSLP++;	
	//i:Sleep(dwSLP);


	_UART_RTS(0);		//inf: RTS, CTS CTRL : Append later	
	if ((GetTickCount() - 1000) >= dwSTic)
	{	LogPrintf("CALL : MAY BE ERROR \n");		
	}

	return 1;
}




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
BOOL 	cHWIF::_UART_RX_STA
(void	*IN_FUNC, void *IN_ARGS, DWORD IN_dwCH)
{
	boxassert((IN_FUNC == NULL), BOXEXIT, _T("input pointer (IN_FUNC) is null\n"));
	boxassert((IN_ARGS == NULL), BOXEXIT, _T("input pointer (IN_ARGS) is null\n"));
	boxreturn((m_nOPEN==    0), FALSE,   DBGTR, _T("no aud sta, board not opened\n"));


	IN_dwCH = 0;	//12.11.23 : ALL BD UART 0
	


	DWORD	dwID;
	int		nCH  = (int)IN_dwCH;
	
	m_tpUartTHRD[nCH].nThread_ACT= 1;
	m_tpUartTHRD[nCH].stCH		= (stuint)IN_dwCH;
	m_tpUartTHRD[nCH].sdBDINX	= m_stSLCTInx;

	m_tpUartTHRD[nCH].vfExtFunc	= (EXT_FUNC)IN_FUNC;
	m_tpUartTHRD[nCH].vpExtArg	= IN_ARGS;
	m_tpUartTHRD[nCH].pcThread_ID= CreateThread(	NULL, 0, (LPTHREAD_START_ROUTINE)UartTHRD_FUNC, 
													&(m_tpUartTHRD[nCH]), 0, (LPDWORD)&dwID);
	m_tpUartTHRD[nCH].dwThread_ID= dwID;
	if (m_tpUartTHRD[nCH].pcThread_ID == NULL) 
	{	LogPrintf("CALL : %X CH Uart Create Thread FAIL", nCH);				
	}

	if (m_tpUartTHRD[nCH].pcThread_ID)  
	{	m_stpUartSTA[nCH] = 1;
		return TRUE;
	}	

	return FALSE;

}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void		cHWIF::_UART_RX_STO(DWORD IN_dwCH)
{	
	IN_dwCH = 0;	//12.11.23 : ALL BD UART 0
	
	if (m_stpUartSTA[IN_dwCH] == 0) return;	
	

	int		nCH  = (int) IN_dwCH;		

	if (m_tpUartTHRD[nCH].pcThread_ID)  
	{	m_tpUartTHRD[nCH].nThread_ACT	= 0;	//i>ESC command		
		CAT3DI_RUART_ESCAPE(m_stSLCTInx);
		LogPrintf("CALL : CAT3DI_RUART_ESCAPE(%X)", m_stSLCTInx);	
	
		WaitForSingleObject(m_tpUartTHRD[nCH].pcThread_ID, INFINITE);	

		m_tpUartTHRD[nCH].nThread_ACT	= 0;	//i>ESC command		
		m_tpUartTHRD[nCH].vfExtFunc		= NULL;
		m_tpUartTHRD[nCH].vpExtArg		= NULL;	
		m_tpUartTHRD[nCH].pcThread_ID	= NULL;
		m_tpUartTHRD[nCH].dwThread_ID	= 0;		
	}

	
	m_stpUartSTA[IN_dwCH] = 0;
}














// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//!	@ DIO
//!	@
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-



// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
DWORD		g_dtDIO_ID_TBL[] = { 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB,	};
DWORD		g_dtDIO_CT_TBL[] = { 1,    2,    3,    4,     5,   6,		};
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		cHWIF::_DIO_DTECT(DIO_TYPE *IO_nMADIO, DIO_TYPE *IO_nSLDIO)
{
	boxassert((IO_nMADIO == NULL), BOXEXIT,  _T("input pointer (IO_nMADIO) is null\n"));
	boxassert((IO_nSLDIO == NULL), BOXEXIT,  _T("input pointer (IO_nSLDIO) is null\n"));
	boxreturn((m_nOPEN  ==    0), -1, DBGTR, _T("_DIO_DTECT() but not opend\n"));

	int		ni;
	stuint	sdRv, sdDIO_ID;
	DWORD	dwMA_ID, dwSL_ID;
	DWORD	dwMADIO, dwSLDIO;

	sdRv = CAT3DI_DIGITALIO_ID(m_stSLCTInx, &sdDIO_ID);
	dwMA_ID = (DWORD)(sdDIO_ID & 0xFF);
	dwSL_ID = (DWORD)(sdDIO_ID >> 8) & 0xFF;

	dwMADIO = DIO_NO;
	dwSLDIO = DIO_NO;
	for (ni = 0; ni < 4; ni++)
	{	if (g_dtDIO_ID_TBL[ni] == dwMA_ID)
			dwMADIO = g_dtDIO_CT_TBL[ni];
		if (g_dtDIO_ID_TBL[ni] == dwSL_ID)
			dwSLDIO = g_dtDIO_CT_TBL[ni];			
	}

	*IO_nMADIO = (DIO_TYPE)dwMADIO;
	*IO_nSLDIO = (DIO_TYPE)dwSLDIO;
	return 1;
}



// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
int 	cHWIF::_DIN(DWORD *IO_dwDI_bit)
{	
	boxassert((IO_dwDI_bit== NULL), BOXEXIT,  _T("input pointer (IO_dwDI) is null\n"));
	boxreturn((m_nOPEN   ==    0), -1, DBGTR, _T("_DIN() but not opend\n"));
	
	DWORD	dwDI, dwi;
	stuint	sdRv, sdtblDI [HWI_MAX_VID_CH];		
	

	ZeroMemory(sdtblDI, sizeof(sdtblDI));
	sdRv = CAT3DI_DIGITALIN_SIGNAL(m_stSLCTInx, sdtblDI);
	
	dwDI = 0;
	for (dwi = 0; dwi < HWI_MAX_VID_CH; dwi++)
	{
		if (sdtblDI[dwi]) dwDI |= (1 << dwi);
	}
	
	*IO_dwDI_bit = dwDI;
	return 1;
}



// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
int 	cHWIF::_DOUT(DWORD IN_dwDO_bit)
{	
	boxreturn((m_nOPEN == 0), -1, DBGTR, _T("_DOUT() but not opend\n"));
	
	int		ni;
	stuint	sdRv;
	stuint	sdtblDO [HWI_MAX_VID_CH];		
	

	ZeroMemory(sdtblDO, sizeof(sdtblDO));
	for (ni = 0; ni < HWI_MAX_VID_CH; ni++)
	{
		if (IN_dwDO_bit & 1) sdtblDO[ni] = 1;
		IN_dwDO_bit >>= 1;
	}
	
	sdRv = CAT3DI_DIGITALOUT_SIGNAL(m_stSLCTInx, sdtblDO);

	return (int) sdRv;
}






















// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//!	@ WDOG
//!	@
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
int 	cHWIF::_WDOG_EN(DWORD IN_dwEN, DWORD IN_dwSEC)
{
	boxreturn((m_nOPEN == 0), -1, DBGTR, _T("_DOUT() but not opend\n"));
	

	m_stWDOG_En	= (stuint)  IN_dwEN;
	m_stWDOG_Sec= (stuint)((IN_dwSEC > 128)? 128 : IN_dwSEC);

	stuint	strv;
	if (m_stWDOG_En)
		 strv = CAT3DI_WDOGEnable (m_stSLCTInx, m_stWDOG_Sec);
	else strv = CAT3DI_WDOGDisable(m_stSLCTInx);

	return (int) strv;
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		cHWIF::_WDOG_RST()
{
	if (m_stWDOG_En == 0) return 0;

	stuint	strv;
	strv = CAT3DI_WDOGEnable (m_stSLCTInx, m_stWDOG_Sec);
	return (int)strv;
}




















// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//!	@ MUX
//!	@
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
int		cHWIF::_AUD_MUX(DWORD IN_dwEN, DWORD IN_dwCH, DWORD IN_dwVOL)
{

	boxreturn((m_nOPEN == 0), -1, DBGTR, _T("_DOUT() but not opend\n"));

	if (!  ((m_dwSLCTMdl == CAT3D_ID_CAP02 ) || (m_dwSLCTMdl == CAT3D_ID_CAP04 ) ||
			(m_dwSLCTMdl == CAT3D_ID_CAP08 ) || (m_dwSLCTMdl == CAT3D_ID_CAP32E) ||
			(m_dwSLCTMdl == CAT3D_ID_SD16EH) )	)
	{	return -1;
	}


	m_stAUDMux_En	= (stuint)  IN_dwEN;
	m_stAUDMux_CH	= (stuint)  IN_dwCH;
	m_stAUDMux_Vol	= (stuint)  IN_dwVOL;

	stuint	strv;


	if (m_dwSLCTMdl == CAT3D_ID_SD16EH) 
	{	strv =	CAT3DI_APLAY_Monitor (m_stSLCTInx, m_stAUDMux_En, m_stAUDMux_CH, 1);	
		return (int)strv;
	}


	if (m_dwSLCTMdl == CAT3D_ID_CAP32E) 
	{	stuint	stch = (IN_dwEN == 0)? 0x11 : m_stAUDMux_CH;				
		strv =	CAT3DI_APLAY_Monitor (m_stSLCTInx, m_stAUDMux_En, stch, 1);	
		return (int)strv;
	}
	

	if ((m_stbd_opn_opt & CAT3DI_APLAY) == 0) return -1;

	strv =	CAT3DI_APLAY_Monitor (m_stSLCTInx, m_stAUDMux_En, m_stAUDMux_CH, 1);	

	if (m_stAUDMux_En)
	{	CAT3DI_APLAY_Volume(m_stSLCTInx, m_stAUDMux_Vol);
	}

	return (int) strv;
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		cHWIF::_TV1_MUX(DWORD IN_dwEN, DWORD IN_dwCH, DWORD IN_dwResolCONST)
{
	boxreturn((m_nOPEN == 0), -1, DBGTR, _T("_DOUT() but not opend\n"));

	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	if (!((m_dwSLCTMdl == CAT3D_ID_LIV04) || (m_dwSLCTMdl == CAT3D_ID_LIV08) || (m_dwSLCTMdl == CAT3D_ID_LIV16) || 
		  (m_dwSLCTMdl == CAT3D_ID_CAP02) || (m_dwSLCTMdl == CAT3D_ID_CAP04) || (m_dwSLCTMdl == CAT3D_ID_CAP08) ||
		  (m_dwSLCTMdl == CAT3D_ID_CAP4PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP8PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP16E)||
		  (m_dwSLCTMdl == CAT3D_ID_CAP32E) )	)
		   return -1;		



	m_stTV1Mux_En	= (stuint)  IN_dwEN;
	m_stTV1Mux_CH	= (stuint)((IN_dwCH >= m_stVCAPT_MaxVICH)? (m_stVCAPT_MaxVICH-1) : IN_dwCH);


	stuint	strv = 0;
	if (m_dwSLCTMdl != CAT3D_ID_CAP32E) 
	{	strv = CAT3DI_TVOUT(m_stSLCTInx, m_stTV1Mux_CH);
		return (int)strv;
	}

	
	strv = CAT3DI_TVOUT3_HDCAP(m_stSLCTInx, IN_dwResolCONST, IN_dwCH, IN_dwEN);

	return (int)strv;
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		cHWIF::_TV2_MUX(DWORD IN_dwEN, DWORD IN_dwCH)
{
	return CAT3DI_FUNC_OK;
	//i: old model not support


	boxreturn((m_nOPEN == 0), -1, DBGTR, _T("_DOUT() but not opend\n"));

	
	m_stTV2Mux_En	= (stuint)  IN_dwEN;
	m_stTV2Mux_CH	= (stuint)((IN_dwCH >= m_stVCAPT_MaxVICH)? (m_stVCAPT_MaxVICH-1) : IN_dwCH);

	stuint	strv = 0;
	if (m_stTV2Mux_En)
	{	strv = CAT3DI_TVOUT2(m_stSLCTInx, m_stTV1Mux_CH);
	}

	return (int)strv;
}




// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//!	@ TV OSD
//!	@
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
int		cHWIF::_TVX_OSD_WRT(DWORD IN_dwCH, DWORD IN_dwPOS,  TCHAR *IN_szStr, DWORD IN_dwStrCnt)
{
	boxassert((IN_szStr == NULL),  BOXEXIT,  _T("input pointer (IN_szStr) is null\n"));
	boxreturn((m_nOPEN  == 0), -1, DBGTR,    _T("_TVX_OSD() but not opend\n"));	
	
	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	if (!((m_dwSLCTMdl == CAT3D_ID_LIV04) || (m_dwSLCTMdl == CAT3D_ID_LIV08) || (m_dwSLCTMdl == CAT3D_ID_LIV16) || 
		  (m_dwSLCTMdl == CAT3D_ID_CAP02) || (m_dwSLCTMdl == CAT3D_ID_CAP04) || (m_dwSLCTMdl == CAT3D_ID_CAP08) ||
	   	  (m_dwSLCTMdl == CAT3D_ID_CAP4PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP8PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP16E)||
		  (m_dwSLCTMdl == CAT3D_ID_CAP32E) )	)
		   return -1;		


	stuint		strv;
	stuint		dtTBL[256];	

	for (DWORD dwi = 0; dwi < IN_dwStrCnt; dwi++)
	{	if (IN_szStr[dwi] == ' ') dtTBL[dwi] = 0x7F;
		else					  dtTBL[dwi] = (IN_szStr[dwi]-0x10);	//i:view document
	}
	
	if (IN_dwCH == 0)
	{	strv = CAT3DI_OSD (m_stSLCTInx, IN_dwPOS, IN_dwStrCnt, dtTBL);
		return (int)strv;
	}

	//i: old model not support
	return CAT3DI_FUNC_OK;
	


	strv = CAT3DI_OSD2(m_stSLCTInx, IN_dwPOS, IN_dwStrCnt, dtTBL);
	return	(int)strv;
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		cHWIF::_TVX_OSD_CLR(DWORD IN_dwCH, DWORD IN_dwPOS, DWORD IN_dwStrCnt)
{
	boxreturn((m_nOPEN  == 0), -1, DBGTR,    _T("_TVX_OSD() but not opend\n"));	

	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	if (!((m_dwSLCTMdl == CAT3D_ID_LIV04) || (m_dwSLCTMdl == CAT3D_ID_LIV08) || (m_dwSLCTMdl == CAT3D_ID_LIV16) || 
		  (m_dwSLCTMdl == CAT3D_ID_CAP02) || (m_dwSLCTMdl == CAT3D_ID_CAP04) || (m_dwSLCTMdl == CAT3D_ID_CAP08) ||
		  (m_dwSLCTMdl == CAT3D_ID_CAP4PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP8PE)|| (m_dwSLCTMdl == CAT3D_ID_CAP16E)||
		  (m_dwSLCTMdl == CAT3D_ID_CAP32E) )	)
		   return -1;		


	stuint		strv;
	stuint		dtTBL[256];	

	for (DWORD dwi = 0; dwi < IN_dwStrCnt; dwi++)
	{	dtTBL[dwi] = 0x7F;		
	}

	if (IN_dwCH == 0)
	{	strv = CAT3DI_OSD (m_stSLCTInx, IN_dwPOS, IN_dwStrCnt, dtTBL);
		return (int)strv;
	}

	//i: old model not support
	return CAT3DI_FUNC_OK;

	strv = CAT3DI_OSD2(m_stSLCTInx, IN_dwPOS, IN_dwStrCnt, dtTBL);
	return	(int)strv;
}














// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
int 	cHWIF::_VL_STATUS(DWORD *IO_dtVL)
{
	boxassert((IO_dtVL  == NULL), BOXEXIT,   _T("input pointer (IO_sdtblVL) is null\n"));
	boxreturn((m_nOPEN  ==    0), -1, DBGTR, _T("_VL_STATUS() but not opend\n"));

	int		ni, nrv;
	stuint	stpVL_N[HWI_MAX_VID_CH];		
	stuint	sdRv = CAT3DI_CAMERA_SIGNAL(m_stSLCTInx, stpVL_N);


	
	nrv = 0;
	for (ni = 0; ni < (int)m_stVCAPT_MaxVICH; ni++)
	{	if (stpVL_N[ni] != m_stpVL_O[ni]) nrv = 1;
		
		IO_dtVL[ni] = (DWORD)stpVL_N[ni];
		
	}

	
	CopyMemory(&(m_stpVL_O[0]),	&(stpVL_N[0]), m_stVCAPT_MaxVICH * sizeof(stuint));

	return nrv;
}


// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
int		cHWIF::_HDV_STATUS
(DWORD *IO_dtXZ, DWORD *IO_dtYZ, DWORD *IO_dtIP, DWORD *IO_dtFPS, DWORD *IO_dtVTG)
{
	boxassert((IO_dtXZ  == NULL), BOXEXIT, _T("input pointer (IO_dtXZ ) is null\n"));
	boxassert((IO_dtYZ  == NULL), BOXEXIT, _T("input pointer (IO_dtYZ ) is null\n"));
	boxassert((IO_dtIP  == NULL), BOXEXIT, _T("input pointer (IO_dtIP ) is null\n"));
	boxassert((IO_dtFPS == NULL), BOXEXIT, _T("input pointer (IO_dtFPS) is null\n"));
	boxassert((IO_dtVTG == NULL), BOXEXIT, _T("input pointer (IO_dtVTG) is null\n"));

	boxreturn((m_nOPEN == 0), -1, DBGTR,   _T("_SID_STATUS() but not opend\n"));
	boxreturn((m_stVCAPT_MaxHDCH <= 0), -1, DBGTR, _T("no have sdi ch"));


	if (!	((m_dwSLCTMdl == CAT3D_ID_HSDI8		) || 
			 (m_dwSLCTMdl == CAT3D_ID_HSDI9		) ||
			 (m_dwSLCTMdl == CAT3D_ID_HSDI8EX	) ||
			 (m_dwSLCTMdl == CAT3D_ID_HD2704K	) ||
			 (m_dwSLCTMdl == CAT3D_ID_4KEXDI4	) ||
			 (m_dwSLCTMdl == CAT3D_ID_ALCAP16	) ||

			 (m_dwSLCTMdl == CAT3D_ID_HD08SH    ) ||
			 (m_dwSLCTMdl == CAT3D_ID_AHD08H    ) ||

			 (m_dwSLCTMdl == CAT3D_ID_CAP32E	) ||
			 (m_dwSLCTMdl == CAT3D_ID_SD16EH	) ||
			 (m_dwSLCTMdl == CAT3D_ID_CAP16HD	) )		)
	{	return -1;
	}

	int		ni, nTMP;
	stuint	stpTBL1[HWI_MAX_VID_CH], stpTBL2[HWI_MAX_VID_CH], stpTBL3[HWI_MAX_VID_CH];		


	ZeroMemory(stpTBL1, sizeof(stpTBL1));
	ZeroMemory(stpTBL2, sizeof(stpTBL2));
	ZeroMemory(stpTBL3, sizeof(stpTBL3));

	CAT3DI_CAMERA_INFO(m_stSLCTInx, stpTBL1, stpTBL2, stpTBL3);

	for (ni = 0; ni < (int)m_stVCAPT_MaxHDCH; ni++)
	{
		//i:GET XSZ >
		nTMP  = (stpTBL1[ni]) & 0xFFF;	
		nTMP %= 9999;	//i: MAX 9999
		IO_dtXZ[ni] = (DWORD)nTMP;
	
		//i:GET YSZ >
		nTMP = (stpTBL1[ni] >> 12) & 0xFFF;	
		nTMP %= 9999;	//i: MAX 9999
		IO_dtYZ[ni] = (DWORD)nTMP;

		//i:GET interlace>
		switch (nTMP)
		{	case 540 :	//i: 1080 i
						IO_dtIP[ni] = 1;	IO_dtYZ[ni] = 1080;	break;

			case 517 :	//i: 1035 i
			case 518 :	IO_dtIP[ni] = 1;	IO_dtYZ[ni] = 1035;	break;


			//i: 480 i
			case 244 : case 243 : case 242 : 	
			case 241 : case 240 : case 239 :
			case 238 : IO_dtIP[ni] = 1;	IO_dtYZ[ni] = 480;	break;

			//i: 576 i
			case 292 : case 291 : case 290 :
			case 289 : case 288 : case 287 :
			case 286 : IO_dtIP[ni] = 1;	IO_dtYZ[ni] = 576;	break;

			case   0 : 	IO_dtIP[ni] = 0;	break;
			default  : 	IO_dtIP[ni] = 2;	break;

		}
				
		//i:GET FPS >
		nTMP = (stpTBL1[ni] >> 24) & 0x7F;	

		if		  ((stpTBL2[ni] <= 300000000) && (stpTBL2[ni] >= 150000001))
		{	nTMP *= 10;
		} else if ((stpTBL2[ni] <= 150000000) && (stpTBL2[ni] >= 145000001))
		{	nTMP *= 10;		//i:60.00
		} else if ((stpTBL2[ni] <= 140000000) && (stpTBL2[ni] >= 145000000))
		{	nTMP -= 1;		//i:59.94
			nTMP *= 10;
			nTMP += 9;
		} else if ((stpTBL2[ni] <=  74400000) && (stpTBL2[ni] >=  74200000))
		{	nTMP *= 10;		//i:30.00
		} else if ((stpTBL2[ni] <=  74199999) && (stpTBL2[ni] >=  74100000))
		{	nTMP -= 1;		//i:29.97
			nTMP *= 10;
			nTMP += 9;
		} else 
		{	nTMP *= 10;
		}
		if ((nTMP > 2000) || (nTMP < 0 )) nTMP = 0;
		IO_dtFPS[ni] = (DWORD)nTMP;

		if (m_dwSLCTMdl == CAT3D_ID_AHD08H )
		{	// AHD TYPE CAMERA
			IO_dtVTG[ni] = (DWORD)stpTBL3[ni] & 0xFF;
		} 
		else if (m_dwSLCTMdl == CAT3D_ID_ALCAP16)
		{	// AHD,TVI,CVI TYPE CAMERA
			IO_dtVTG[ni] = (DWORD)stpTBL3[ni] & 0xFFFF;

		} else 
		{	//i:Get VOLTAGE
			nTMP = (int)stpTBL3[ni];
			nTMP = 0xC2 - nTMP;
			if (nTMP <= 0) nTMP = 0;
			else 
			{	nTMP = (nTMP * 1000) / 94;
				if (nTMP >= 1000) nTMP = 1000;		
			}
			
			IO_dtVTG[ni] = (DWORD)nTMP;
		}
	}

	return 0;

}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		cHWIF::_SDI_ResolC(DWORD IN_dwCH)
{
	boxreturn((m_nOPEN == 0),-1, DBGTR,   _T("_SID_STATUS() but not opend\n"));
	boxreturn((m_stVCAPT_MaxHDCH <= 0),		  -1, DBGTR, _T("no have sdi ch"));
	boxreturn((m_stVCAPT_MaxVICH <= IN_dwCH), -1, DBGTR, _T("input ch invalid"));
	boxreturn((m_stVCAPT_staHDCH >  IN_dwCH), -1, DBGTR, _T("input ch invalid"));



	if (!	((m_dwSLCTMdl == CAT3D_ID_HSDI8		) || 
			 (m_dwSLCTMdl == CAT3D_ID_HSDI9		) ||
			 (m_dwSLCTMdl == CAT3D_ID_CAP32E	) ||
			 (m_dwSLCTMdl == CAT3D_ID_SD16EH	) ||
			 (m_dwSLCTMdl == CAT3D_ID_HD08SH    ) ||
			 (m_dwSLCTMdl == CAT3D_ID_AHD08H    ) ||

			 (m_dwSLCTMdl == CAT3D_ID_HSDI8EX   ) ||
			 (m_dwSLCTMdl == CAT3D_ID_HD2704K   ) ||
			 (m_dwSLCTMdl == CAT3D_ID_HD08SH    ) ||
			 (m_dwSLCTMdl == CAT3D_ID_AHD08H    ) ||

			 (m_dwSLCTMdl == CAT3D_ID_CAP16HD	) )		)
	{	return -1;
	}

	
	int			nrv;
	DWORD		dwSDIresolC = 0;

	DWORD		dtSDIXZTBL[HWI_MAX_VID_CH];
	DWORD		dtSDIYZTBL[HWI_MAX_VID_CH];
	DWORD		dtSDIIPTBL[HWI_MAX_VID_CH];
	DWORD		dtSDIFPTBL[HWI_MAX_VID_CH];
	DWORD		dtSDIVGTBL[HWI_MAX_VID_CH];
	

	nrv = g_hw._HDV_STATUS(	dtSDIXZTBL, dtSDIYZTBL, dtSDIIPTBL, dtSDIFPTBL, dtSDIVGTBL);

	if (nrv < 0) return -1;			

	DWORD	dwSCH = IN_dwCH - (DWORD)m_stVCAPT_staHDCH;

	
	
	if ((dtSDIXZTBL[dwSCH] == 1920) && (dtSDIYZTBL[dwSCH] == 1080) && (dtSDIIPTBL[dwSCH] ==    2) )
	{	 dwSDIresolC = VCAPT_TRON_1920X1080P;
		 return dwSDIresolC;
	}

	if ((dtSDIXZTBL[dwSCH] == 1920) && (dtSDIYZTBL[dwSCH] == 1080) && (dtSDIIPTBL[dwSCH] ==    1) )
	{	 dwSDIresolC = VCAPT_TRON_1920X1080I;
		 return dwSDIresolC;
	}

	if ((dtSDIXZTBL[dwSCH] == 1280) && (dtSDIYZTBL[dwSCH] == 720) && (dtSDIIPTBL[dwSCH] ==    2) )
	{	 dwSDIresolC = VCAPT_TRON_1280X720;			
		 return dwSDIresolC;
	}
				
	return 0;

}
















// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
int		cHWIF::_PCI_BANDWIDTH_CTRL(DWORD IN_nEN, DWORD IN_nNumB)
{

	boxreturn((m_nOPEN == 0), -1, DBGTR, _T("_SID_STATUS() but not opend\n"));


	//i:drv mmodel>CAT3D_ID_LIV16, CAP02, CAP16, HSDI1, CAP32E, SD16EH, HD08SH
	
	if (!	((m_dwSLCTMdl == CAT3D_ID_CAP02		) || 
			 (m_dwSLCTMdl == CAT3D_ID_CAP04		) ||
			 (m_dwSLCTMdl == CAT3D_ID_CAP08		) ||

			 (m_dwSLCTMdl == CAT3D_ID_LIV04		) ||
			 (m_dwSLCTMdl == CAT3D_ID_LIV08		) ||
			 (m_dwSLCTMdl == CAT3D_ID_LIV16		) )		)
	{	return -1;
	}

	if (IN_nEN)
		 CAT3DI_PCI_BAND_WIDTH_Control(m_stSLCTInx, 1, (stuint)IN_nNumB);
	else CAT3DI_PCI_BAND_WIDTH_Control(m_stSLCTInx, 0, 0);

	return 0;
}
































#pragma optimize("", on)

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//!	@brief		MESSAGE MAP CODE	INIT
//!	@
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
DWORD	WINAPI	cHWIF::VidTHRD_FUNC(LPVOID lpvParam)
{
	T_THREAD	*pcT	 = static_cast <T_THREAD *> (lpvParam);
	cHWIF		*pthis	 = (pcT  )? static_cast <cHWIF *>(pcT->vpthis) : NULL;
	stuint		stmdl	 = (pthis)? pthis->m_dwSLCTMdl : CAT3D_ID_CAP02; //23
			
	DWORD		dwXZ	 = (pcT)? pcT->dwCH_XZ	: 0;//720
	DWORD		dwYZ	 = (pcT)? pcT->dwCH_YZ	: 0;//480
	DWORD		dwYC     = (pcT)? pcT->dwCH_YC	: 0;//1
	stuint		sdBD_CH  = (pcT)? pcT->stCH		: 0;//0
	stuint		sdBD_INX = (pcT)? pcT->sdBDINX	: 0;//0
	stuint		sdFC_ARG = (pcT)? (stuint)(pcT->vpExtArg) : 0;
	
	stuint		stRV, OUT_sdAddr, OUT_sdSize;	

	//del>Sleep(400);	//b.of message print

	boxlog(1, EYECHK, _T("video capture thread start][ch:%2d, inx:%2d, xz:%4d, yz:%4d, 411:%2d"), 
			sdBD_CH, sdBD_INX, dwXZ, dwYZ, dwYC);
	

	while(pcT->nThread_ACT)
	{	
		if (pcT->nVidDMA_ACT == 0) continue;

		OUT_sdSize = 0;
		//new hw : 400eh, hdc240sh, 2704k later model: stream size append
		//del>stRV = CAT3DI_LockWait4GetImageAddress(sdBD_INX, sdBD_CH, &OUT_sdAddr, INFINITE);	

		stRV = CAT3DI_LockWait4GetImageAddress2(sdBD_INX, sdBD_CH, &OUT_sdAddr, &OUT_sdSize, INFINITE);	
		if (stRV  != CAT3DI_FUNC_OK) continue;
		
		//i:ovi h264 ch
		if	( ((stmdl == CAT3D_ID_SD16EH) &&  (sdBD_CH == 32))	||
			  ((stmdl == CAT3D_ID_HD08SH) &&  (sdBD_CH <=  1))	||	
			  ((stmdl == CAT3D_ID_AHD08H) &&  (sdBD_CH <=  1))	)
		{	//max size 32MB : 4MB * 8ea : 1920x1088x2 : 4177920(0x3FC000) 
			OUT_sdSize &= 0x1FFFFFF;		

		} else if (stmdl == CAT3D_ID_HD2704K)
		{	//max size 20MB
			OUT_sdSize &= 0x3FFFFF; 
			OUT_sdSize *= 5;

		} else 
		{	OUT_sdSize  = 0;
		}

		if (pcT->vfExtFunc)
		{	pcT->vfExtFunc(sdFC_ARG, sdBD_CH, OUT_sdAddr, OUT_sdSize, dwXZ, dwYZ, dwYC);
		}
		
	}
	boxlog(1, DBGTR, _T("video capture thread end][ch:%2d, inx:%2d"), sdBD_CH, sdBD_INX);

	ExitThread(1);
	return 1;
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	WINAPI	cHWIF::AudTHRD_FUNC(LPVOID lpvParam)
{
	T_THREAD	*pcT = static_cast <T_THREAD *> (lpvParam);
	
	tu64		llCNT;	
	DWORD		dwAUDSZ;
	stuint		sdRV;
	stuint		sdBD_CH,  sdBD_INX;
	stuint		sdFC_ARG, OUT_sdAddr;	
	

	llCNT    = 0;
	dwAUDSZ	 = pcT->dwCH_AUDSZ;
	sdBD_CH  = pcT->stCH;	
	sdBD_INX = pcT->sdBDINX;
	sdFC_ARG = (stuint)pcT->vpExtArg;

	boxlog(1, DBGTR, _T("audio capture thread start [ch:%2d, inx:%2d, hz:%d]"), 
		sdBD_CH, sdBD_INX, (dwAUDSZ / 2));

	

	while(pcT->nThread_ACT)
	{	
		if (pcT->nAudDMA_ACT == 0) continue;

		sdRV = CAT3DI_LockWait4GetSoundAddress(sdBD_INX, sdBD_CH, &OUT_sdAddr, INFINITE);
		if (sdRV  != CAT3DI_FUNC_OK) continue;

		//i: 1st 1 time drop, because other process, if u want no skip, delette under line
		llCNT++;
		if (llCNT < 2) continue;	
		//del>TRACE("AUD CAP : [%d]%08X \n", sdBD_CH, dwCNT++);

		if (pcT->vfExtFunc)
		{	pcT->vfExtFunc(sdFC_ARG, sdBD_CH, OUT_sdAddr, dwAUDSZ, 0, 0, 0);
		}
		
	}
	boxlog(1, DBGTR, _T("audio capture thread end [ch:%2d, inx:%2d]"), sdBD_CH, sdBD_INX);


	ExitThread(1);
	return 1;

}





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	WINAPI	cHWIF::UartTHRD_FUNC(LPVOID lpvParam)
{
	T_THREAD	*pcT = static_cast <T_THREAD *> (lpvParam);
	
	
	stuint		strv, rxreqsz;
	stuint		sdBD_CH  = pcT->stCH;	
	stuint		sdBD_INX = pcT->sdBDINX;
	stuint		sdFC_ARG = (stuint)pcT->vpExtArg;
	stuint		OUT_sdAddr;
	
	BYTE		btrxed[256];


	while (pcT->nThread_ACT)
	{	
		strv		= CAT3DI_RUART_QUERY_RXBUFSIZE(sdBD_INX);
		//del>dwRv	= CAT3DI_R2UAT_QUERY_RXBUFSIZE(sdBD_INX, g_COMEXT, g_COMX);

		rxreqsz = (strv ==    0)?   1 : strv; 
		rxreqsz	= (strv >= 0x10)? 0xF : rxreqsz;
			
		strv		= CAT3DI_RUART_READ(sdBD_INX, btrxed, rxreqsz, INFINITE);	
		//del>dwRv	= CAT3DI_R2UAT_READ(sdBD_INX, btrxed, rxreqsz, INFINITE, g_COMEXT, g_COMX);	
			
		if (strv	== CAT3DI_FUNC_OK)
		{	
			if (pcT->vfExtFunc)
			{	OUT_sdAddr = (stuint)&(btrxed [0]);
				pcT->vfExtFunc(sdFC_ARG, sdBD_CH, OUT_sdAddr, rxreqsz, 0, 0, 0);
			}
		}
	}

	ExitThread(1);
	return 1;

}













