﻿
/* 	filename: 	ddi div wnd
 * **************************************************************************************
 *
 * 	created  :	2013/01/31
 *	author   :	boxucom@gmail.com
 * 	file base:	MFC
 *
 *  version  :  1	[2003/01/27] : START CODE -> CLASS MADE / CODE WRITING..
 *  version  :  2	[2004/08/23] : CODE APPEND..
 *  ..
 *  ..
 *  version  :  10	[2013/01/31] : 960H support (code alignment)
 *
 * **************************************************************************************
 */

#include "stdafx.h"
#include "..\\boxinc\\box_type_win.h"
#include "..\\boxinc\\box_log_win.h"

#include "ddi_divwnd.h"

#include "..\\boxlib\\cspace_lite.h"
#include "..\\boxlib\\ddif.h"

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#include <vfw.h>
#include <strsafe.h>

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// * DEFINE		
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define SYNC_CH_WAIT_10K						10000

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef _DEBUG
#undef	THIS_FILE
static	char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * 
// * MESSAGE MAP DEFINE	 
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
BEGIN_MESSAGE_MAP(ddi_div_wnd,		CWnd)	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP	 ()
	ON_WM_CLOSE		 ()
	ON_WM_PAINT		 ()	
END_MESSAGE_MAP()

/* 
*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
* CLASS		: ddi_div_wnd
*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
*/

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		Construction / Destruction / Initialize
//!	@		
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
ddi_div_wnd::ddi_div_wnd()  
{ 
	m_pddif				= NULL;
	m_division_inited	= 0;

	ZeroMemory(&(m_tdivVCH[0]), sizeof(m_tdivVCH));

	ZeroMemory(m_lpszWndName, sizeof(m_lpszWndName));
	m_nPaintEn = 0;
}

ddi_div_wnd::~ddi_div_wnd() 
{
	ddi_close();
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//!	@brief		MESSAGE MAP CODE
//!	@
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

void ddi_div_wnd::OnClose() {	}		//{ CWnd::OnClose();	}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void ddi_div_wnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CWnd::OnLButtonDown(nFlags, point);
    PostMessage(WM_NCLBUTTONDOWN, HTCAPTION);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void ddi_div_wnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CWnd::OnLButtonUp(nFlags, point);
    PostMessage(WM_NCLBUTTONUP, HTCAPTION);
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
BOOL ddi_div_wnd::DestroyWindow() 
{	
	ddi_close();

	return CWnd::DestroyWindow();
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void ddi_div_wnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting	
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * Purpose	: WINDOWS CREATE 
// 
// * Arguments	: IN_dwClientXZ : WINDOW CLIENT WIDTH
//                IN_dwClientYZ : WINDOW CLIENT HEIGHT
// * Return		: none
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	ddi_div_wnd::CreateNShow
(DWORD	IN_dwClientXZ, DWORD IN_dwClientYZ, TCHAR *IN_lptcszWndName, DWORD IN_dwTOP)
{	
	RECT			rect;
	int				nxz, nyz;

	DWORD			dwstyle = 1, dwstyle_flg = 0;
	
	if (dwstyle)
	{	// NORMAL WINDOW:
		dwstyle_flg = WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_OVERLAPPEDWINDOW | WS_POPUP;
	} else 
	{	//NO FRAME WINDOW:
		dwstyle_flg = WS_VISIBLE | WS_POPUP;
	}
	
	rect.left	= 100;
	rect.top	= 100;
	rect.right	= rect.left + IN_dwClientXZ;
	rect.bottom	= rect.top  + IN_dwClientYZ;
	::AdjustWindowRectEx(&rect, dwstyle_flg, 0, 0);

	nxz	= rect.right  - rect.left + 1;
	nyz	= rect.bottom - rect.top  + 1;

	if ((::GetSystemMetrics(SM_CXSCREEN) <= nxz) || (::GetSystemMetrics(SM_CYSCREEN) <= nyz) ) 
	{	m_rectOrSz.left		= 0;
		m_rectOrSz.top		= 0;
		m_rectOrSz.right	= nxz;
		m_rectOrSz.bottom	= nyz;
	} else 
	{	m_rectOrSz.left		= ((::GetSystemMetrics(SM_CXSCREEN) - nxz) >> 1);
		m_rectOrSz.top		= ((::GetSystemMetrics(SM_CYSCREEN) - nyz) >> 1);
		m_rectOrSz.right	= nxz + m_rectOrSz.left;
		m_rectOrSz.bottom	= nyz + m_rectOrSz.top;
	}

	StringCbPrintf(m_lpszWndName, sizeof(m_lpszWndName), _T("%s"), IN_lptcszWndName);
	CreateEx(0, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, ::LoadCursor(NULL, IDC_ARROW), 
									::CreateSolidBrush(RGB(0xFF, 0xFF, 0xFF)), 0), m_lpszWndName, 
									dwstyle_flg, m_rectOrSz, NULL, 0, NULL);
	ShowWindow(SW_SHOW);

	if (IN_dwTOP) 
	{	SetWindowPos(&CWnd::wndTop, m_rectOrSz.left, m_rectOrSz.top, nxz, nyz, SWP_SHOWWINDOW);
		UpdateWindow();	
	}
}

/* 
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 * CLASS		: ddi_div_wnd
 *-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=*-*=
 */

#define RECT_SET(a, b,c,d,e)	{	a.left = b;	a.top = c;	a.right = d;	a.bottom = e;	}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD	g_dtDIVmode_MaxCHtbl[]	=  {	1, 2, 2, 4, 6, 6, 8, 9, 10, 13, 16, };	

// -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- -=- 
static	int		g_dtDIVmode_tblsum = 0;
static	RECT	g_dtDIVmode_Rectbl[DDIV_MODE_MAXMODE][DDIV_MODE_MAXCH] = 
{
	// DDIV_MODE_01SCRN
	{	{   0,   0,1920,1080,},
	},
	// DDIV_MODE_02SCRN	: 2X1
	{	{   0, 270, 960, 540,},	{ 960, 270, 960, 540,},	
	},	
	// DDIV_MODE_02SCRN : 1X2
	{	{   0,   0,1920, 540,},	{   0, 540,1920, 540,},	
	},		
	// DDIV_MODE_04SCRN : 2X2
	{	{   0,   0, 960, 540,},	{ 960,   0, 960, 540,},	{   0, 540, 960, 540,},	{ 960, 540, 960, 540,},	
	},		
	// DDIV_MODE_06SCRN1 : 3X2
	{	{   0,   0, 640, 540,},	{ 640,   0, 640, 540,},	{1280,   0, 640, 540,},	
		{   0, 540, 640, 540,},	{ 640, 540, 640, 540,},	{1280, 540, 640, 540,},	
	},	
	// DDIV_MODE_06SCRN2 : 1big 
	{	{   0,   0,1280, 720,},	
		{1280,   0, 640, 360,},	{1280, 360, 640, 360,},	
		{   0, 720, 640, 360,},	{ 640, 720, 640, 360,},	{1280, 720, 640, 360,},	
	},
	// DDIV_MODE_08SCRN  : 1big 
	{	{   0,   0,1440, 810,},	
		{1440,   0, 480, 270,},	{1440, 270, 480, 270,},	{1440, 540, 480, 270,},	
		{   0, 810, 480, 270,},	{ 480, 810, 480, 270,},	{ 960, 810, 480, 270,},	{1440, 810, 480, 270,},	
	},
	// DDIV_MODE_09SCRN  : 3X3
	{	{   0,   0, 640, 360,},	{ 640,   0, 640, 360,},	{1280,   0, 640, 360,},	
		{   0, 360, 640, 360,},	{ 640, 360, 640, 360,},	{1280, 360, 640, 360,},	
		{   0, 720, 640, 360,},	{ 640, 720, 640, 360,},	{1280, 720, 640, 360,},	
	},
	// DDIV_MODE_10SCRN  : 2big
	{	{   0,   0, 960, 540,},	{ 960,   0, 960, 540,},			
		{   0, 540, 480, 270,},	{ 480, 540, 480, 270,},	{ 960, 540, 480, 270,},	{1440, 540, 480, 270,},	
		{   0, 810, 480, 270,},	{ 480, 810, 480, 270,},	{ 960, 810, 480, 270,},	{1440, 810, 480, 270,},	
	},
	// DDIV_MODE_13SCRN  : 1big: 
	{	{ 480, 270, 960, 540,},			
		{   0,   0, 480, 270,},	{ 480,   0, 480, 270,},	{ 960,   0, 480, 270,},	{1440,   0, 480, 270,},	
		{   0, 270, 480, 270,},	{1440, 270, 480, 270,},	
		{   0, 540, 480, 270,},	{1440, 540, 480, 270,},	
		{   0, 810, 480, 270,},	{ 480, 810, 480, 270,},	{ 960, 810, 480, 270,},	{1440, 810, 480, 270,},	
	},
	// DDIV_MODE_16SCRN  : 4X4
	{	
		{   0,   0, 480, 270,},	{ 480,   0, 480, 270,},	{ 960,   0, 480, 270,},	{1440,   0, 480, 270,},	
		{   0, 270, 480, 270,},	{ 480, 270, 480, 270,},	{ 960, 270, 480, 270,},	{1440, 270, 480, 270,},	
		{   0, 540, 480, 270,},	{ 480, 540, 480, 270,},	{ 960, 540, 480, 270,},	{1440, 540, 480, 270,},			
		{   0, 810, 480, 270,},	{ 480, 810, 480, 270,},	{ 960, 810, 480, 270,},	{1440, 810, 480, 270,},	
	},
};

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * ddi_open		: direct draw open 
// * 
// * in_nmaxch		: open max ch
// * in_nDDIF_type	: reserved, later append UYVY
// * in_nHDbase1or0 : current use only 1 : no support 0 (SD)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	ddi_div_wnd::ddi_open
(int	in_nmaxch, int in_nDDIF_type, int in_nHDbase1or0)
{
	boxassert((in_nmaxch >  DDIV_MODE_MAXCH), BOXEXIT, _T("in_nmaxch >= DDIV_MODE_MAXCH"));
	boxassert((in_nHDbase1or0 ==          0), BOXEXIT, _T("in_nHDbase1or0 == 0, not support sd mode"));


	boxreturn0((GetSafeHwnd() == NULL), DBGTR, _T("window handle is null"));

	if (m_pddif)
	{	ddi_close();
	}

	m_pddif = new ddif;
	if (m_pddif == NULL) return;

	if (m_pddif->ddi_open(this, 1) < 0)
	{	ddi_close();
		return;
	}	

	int			ni, nk;

	m_dwInpMaxCH	= in_nmaxch;

	for (ni = 0; ni < in_nmaxch; ni++)
	{	ddi_ch_open(ni, DIVW_BASE_XZ, DIVW_BASE_YZ, DDIF_UYVY);
	}

	if (g_dtDIVmode_tblsum == 0)
	{	//i> ddraw not use size, use area
		g_dtDIVmode_tblsum = 1;	
		for (ni = 0; ni < DDIV_MODE_MAXMODE; ni++)
		{
			if (ni == 0) continue;

			for (nk = 0; nk < (int)g_dtDIVmode_MaxCHtbl[ni]; nk++)
			{	g_dtDIVmode_Rectbl[ni][nk].right = 	g_dtDIVmode_Rectbl[ni][nk].left + g_dtDIVmode_Rectbl[ni][nk].right;
				g_dtDIVmode_Rectbl[ni][nk].bottom= 	g_dtDIVmode_Rectbl[ni][nk].top  + g_dtDIVmode_Rectbl[ni][nk].bottom;
			}
		}
	}

	m_division_inited	= 1;
	m_nddif_syncch		= SYNC_CH_WAIT_10K;

	ZeroMemory(m_dwDrawEn_DIVCHtbl, sizeof(m_dwDrawEn_DIVCHtbl));
	ZeroMemory(m_rtDrawRecttbl,	    sizeof(m_rtDrawRecttbl));
	division(0, DDIV_MODE_09SCRN);	
}

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
void	ddi_div_wnd::ddi_close()
{		
	int		ni;

	for (ni = 0; ni < (int)m_dwInpMaxCH; ni++)
	{	ddi_ch_close(ni);	
	}


	if (m_pddif)
	{	m_pddif->ddi_close();
		
		delete m_pddif;
		m_pddif = NULL;
	}	

	m_division_inited = 0;		
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		ddi_div_wnd::ddi_ch_open(DWORD IN_nch, DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC)
{	
	if (m_pddif == NULL) return -1;

	int		nrv;	
	nrv = m_pddif->chraw_open(IN_nch, IN_dwXZ, IN_dwYZ, IN_dw4CC);	
	if (nrv < 0) return nrv;

	m_tdivVCH[IN_nch].dwXZ		 = IN_dwXZ;
	m_tdivVCH[IN_nch].dwYZ		 = IN_dwYZ;
	m_tdivVCH[IN_nch].dw4CC		 = IN_dw4CC;	
	m_tdivVCH[IN_nch].dwInited	 = 1;

	return  nrv;
}

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
void	ddi_div_wnd::ddi_ch_close(DWORD IN_nch)
{
	if (m_pddif					   == NULL) return;
	if (m_tdivVCH[IN_nch].dwInited ==    0) return;

	m_pddif->chraw_close(IN_nch);

	m_tdivVCH[IN_nch].dwXZ		 = 0;
	m_tdivVCH[IN_nch].dwYZ		 = 0;
	m_tdivVCH[IN_nch].dw4CC		 = 0;	
	m_tdivVCH[IN_nch].dwInited	 = 0;
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * render
//
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	ddi_div_wnd::division(DWORD IN_dwsta, DWORD IN_dwmode)
{
	if (m_pddif			  == NULL) return;
	if (m_division_inited ==    0) return;

	boxassert((IN_dwmode>= DDIV_MODE_MAXMODE), BOXEXIT, _T("IN_dwmode>= DIV_MODE_MAXMODE"));

	m_dwDIVsta	=	IN_dwsta;
	m_dwDIVmode	=	IN_dwmode;
	m_dwDIVMaxCH=	g_dtDIVmode_MaxCHtbl[IN_dwmode];
	
	int		ni, nch;

	ZeroMemory(m_dwDrawEn_DIVCHtbl, sizeof(m_dwDrawEn_DIVCHtbl));
	ZeroMemory(m_rtDrawRecttbl,	    sizeof(m_rtDrawRecttbl));

	for (ni = 0; ni < (int)g_dtDIVmode_MaxCHtbl[IN_dwmode]; ni++)
	{	nch = (ni + IN_dwsta) % m_dwInpMaxCH;
	
		m_dwDrawEn_DIVCHtbl[nch] = 1;
		CopyRect(&(m_rtDrawRecttbl[nch]),	&(g_dtDIVmode_Rectbl[IN_dwmode][ni]) );	
	}

	m_pddif->divscrn_clear();
	//CALL BLACK SCREEN UPDATE
}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * render
//
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	ddi_div_wnd::render(DWORD IN_dwCH, DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_bpYC)
{
	if (m_pddif			  == NULL) return;
	if (m_division_inited ==    0) return;

	if (m_tdivVCH[IN_dwCH].dwInited     == 0) return;
	if (m_pddif->chraw_isalive(IN_dwCH) == 0) return;

	if (m_dwDrawEn_DIVCHtbl[IN_dwCH]    == 0) return;

	if ((m_tdivVCH[IN_dwCH].dwXZ != IN_dwXZ) || 
		(m_tdivVCH[IN_dwCH].dwYZ != IN_dwYZ) ||
		(m_tdivVCH[IN_dwCH].dw4CC!= IN_dw4CC) )
	{	
		ddi_ch_close(IN_dwCH);
		ddi_ch_open (IN_dwCH, IN_dwXZ, IN_dwYZ, IN_dw4CC);
		return;
	}

	RECT	srcrect;
	DWORD	ni, nyz, nxcpysz;	
	BYTE	*bpsrc = NULL;
	BYTE	*bpdst	= m_pddif->chraw_get_buf(IN_dwCH);	//ddraw lock

	boxreturn0((bpdst == NULL), DBGTR, _T("invalid target mem"));

	if (IN_dwYZ <= 540)
	{	CopyMemory(bpdst, IN_bpYC, IN_dwXZ * IN_dwYZ * 2);
		RECT_SET  (srcrect,	0, 0,  IN_dwXZ,  IN_dwYZ);
	} else 
	{
		if (m_dwDIVmode == DDIV_MODE_01SCRN)	
		{	
			CopyMemory(bpdst, IN_bpYC, IN_dwXZ * IN_dwYZ * 2);
			RECT_SET  (srcrect,	0, 0,  IN_dwXZ,  IN_dwYZ);
		} else 
		{
			nxcpysz	= IN_dwXZ << 1;	// mul xsz * 2
			nyz		= IN_dwYZ >> 1;	// div ysz 1/2
			bpsrc	= IN_bpYC;					

			for (ni = 0; ni <nyz; ni++)
			{	//del>CSPACE_COPY_MEMORY(bpdst, bpsrc, nxcpysz, 0);			
				//del>	
				CopyMemory(bpdst, bpsrc, nxcpysz);
				bpsrc	+= (nxcpysz * 2);	// 2 line inc
				bpdst	+= nxcpysz;			// 1 line inc
			}				

			RECT_SET(srcrect,	0, 0, (IN_dwXZ), (IN_dwYZ >> 1));		
		}
	}

	m_pddif->chraw_put_buf(IN_dwCH);				// ddraw unlock

	m_pddif->divchnn_set_rect(IN_dwCH, &srcrect, &(m_rtDrawRecttbl[IN_dwCH]));		


	if (m_nddif_syncch == SYNC_CH_WAIT_10K)
	{	m_nddif_syncch = (int)IN_dwCH;		
	} else
	{	if (m_nddif_syncch == (int)IN_dwCH)
		{	m_pddif->own_sync_ext();
		}
	}


}

