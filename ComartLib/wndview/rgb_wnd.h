﻿
/* 	filename: 	RGB WND
 * **************************************************************************************
 * 	created  :	2012/10/25
 *	author   :	boxucom@gmail.com
 * 	file base:	MFC
 *
 *  version  :  1	[2003/01/27] : START CODE -> CLASS MADE / CODE WRITING..
 *  version  :  2	[2004/08/23] : CODE APPEND..
 *  ..
 *  ..
 *  version  :  10	[2012/10/22] : 960H support (code alignment)
 *
 * **************************************************************************************
 */

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#if !defined(_RGBWND_d7_I_)
#define _RGBWND_d7_I_

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef __cplusplus
//extern "C" {
//#endif

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*
//*	
//*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//#include "..\\resource.h"
#include "..\\boxlib\\stools.h"

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// *	C/C++ CLASS (STRUCT) USE DEFINE
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//!		@class		
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 

typedef struct _tag_PtnNoiseRect
{
	RECT	Region;
	BOOL	bSelect;
	BOOL	bFail;
	DOUBLE	dDbThreshold;

	_tag_PtnNoiseRect()
	{
		Reset();
	};

	void Reset()
	{
		ZeroMemory(&Region, sizeof(RECT));
		bSelect = TRUE;
		bFail = FALSE;
		dDbThreshold	= 0;
	};
}ST_PtnNoiseRect, *PST_PtnNoiseRect;

class	 RGBWnd		: public CWnd
{
public:
		 RGBWnd();
virtual ~RGBWnd();

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	void	CreateNShow	(DWORD IN_dwClientXZ, DWORD IN_dwClientYZ, TCHAR *IN_lptcszWndName, DWORD IN_dwTOP);
	void	render		(DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422);
	void	render_bypass	(DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422);

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:	
	void	DIBBLT (CDC *IN_pDC, ULONG IN_nLineInc);

	void	SAVEDIB(DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422);
	void	RGBCONV(DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422);

	void	RGB_BYPASS(DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422);

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
private:
	int			m_nPaintEn;

	TCHAR		m_lpszWndName[MAX_PATH];	
    RECT		m_rectOrSz;	

	BITMAPINFO	m_biInf;
	LPBYTE		m_lpbRGB;
	DWORD		m_dwRGBSZ;

	aligmem		m_amem;
	
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * MFC MESSAGE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	virtual BOOL	DestroyWindow();
	
public:
	DECLARE_MESSAGE_MAP()	
	afx_msg void	OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void	OnLButtonUp(  UINT nFlags, CPoint point);
	afx_msg void	OnClose();
	afx_msg void	OnPaint();
	
public:
	// 오버레이
	BOOL		m_bUseOverlay;
	CArray <RECT, RECT&> m_arrRect;
	void		SetUseOverlay(BOOL bUse = TRUE)
	{
		m_bUseOverlay = bUse;
	};

	void		AddOverlayRect(RECT rect);

	void		ResetOverlayRect()
	{
		m_arrRect.RemoveAll();
	};
	
	void		DrawEmpty(CDC* pDC);

	void		DrawRect(CDC* pDC);

	void		DrawRect(CDC* pDC, UINT nIndex);
	void		DrawNoiseText(CDC* pDC, UINT nIndex);

	void		UpdateEmpty();

	// 영역, 사용여부, 판정
	ST_PtnNoiseRect	rectNoise[9];
	void		ResetRect_Noise();
	void		ResetRect_NoiseJudge();
	void		SetNoiseFail(UINT nIndex, BOOL bFail);
	void		SetNoiseResult(UINT nIndex, DOUBLE dDbNoise);
};

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef  __cplusplus
//}
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_RGBWND_d7_I_)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-

