﻿// List_IndicatorOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_IndicatorOp.h"

#define IRtOp_ED_CELLEDIT		6010

// CList_IndicatorOp

IMPLEMENT_DYNAMIC(CList_IndicatorOp, CListCtrl)

CList_IndicatorOp::CList_IndicatorOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;

	m_fIndicator_Std_X = 0;
	m_fIndicator_Std_Y = 0;

	m_fIndicator_Spec_X = 0;
	m_fIndicator_Spec_Y = 0;

	m_pstModelInfo = NULL;
}

CList_IndicatorOp::~CList_IndicatorOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_IndicatorOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_IndicatorOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_IndicatorOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtOp_ED_CELLEDIT, &CList_IndicatorOp::OnEnKillFocusECpOpellEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_IndicatorOp 메시지 처리기입니다.
int CList_IndicatorOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER/* | ES_NUMBER*/, CRect(0, 0, 0, 0), this, IRtOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);
	
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_IndicatorOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[IndicatorOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	for (int nCol = 0; nCol < IndicatorOp_MaxCol; nCol++)
	{
		iColDivide += iHeaderWidth_IndicatorOp[nCol];
	}

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < IndicatorOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() * iHeaderWidth_IndicatorOp[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((rectClient.Width() * iHeaderWidth_IndicatorOp[IndicatorOp_Object]) / iColDivide) + (rectClient.Width() - iMisc);
	SetColumnWidth(IndicatorOp_Object, iUnitWidth);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_IndicatorOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_IndicatorOp::InitHeader()
{
	for (int nCol = 0; nCol < IndicatorOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_IndicatorOp[nCol], iListAglin_IndicatorOp[nCol], iHeaderWidth_IndicatorOp[nCol]);
	}

	for (int nCol = 0; nCol < IndicatorOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_IndicatorOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_IndicatorOp::InsertFullData()
{
	if (m_pstModelInfo == NULL)
		return;

 	DeleteAllItems();
 
	m_fIndicator_Std_X = m_pstModelInfo->fIndicator_Std_X;
	m_fIndicator_Std_Y = m_pstModelInfo->fIndicator_Std_Y;
	
	m_fIndicator_Spec_X = m_pstModelInfo->fIndicator_Spec_X;
	m_fIndicator_Spec_Y = m_pstModelInfo->fIndicator_Spec_Y;

	for (UINT nIdx = 0; nIdx < IndicatorOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_IndicatorOp::SetRectRow(UINT nRow)
{
	CString strText;
	strText.Format(_T("%s"), g_lpszItem_IndicatorOp[nRow]);
	SetItemText(nRow, IndicatorOp_Object, strText);

	switch (nRow)
	{
	case IndicatorOp_Standard:
		strText.Format(_T("%.2f"), m_fIndicator_Std_X);
		SetItemText(nRow, IndicatorOp_X, strText);
		strText.Format(_T("%.2f"), m_fIndicator_Std_Y);
		SetItemText(nRow, IndicatorOp_Y, strText);
		break;
	case IndicatorOp_Spec:
		strText.Format(_T("%.2f"), m_fIndicator_Spec_X);
		SetItemText(nRow, IndicatorOp_X, strText);
		strText.Format(_T("%.2f"), m_fIndicator_Spec_Y);
		SetItemText(nRow, IndicatorOp_Y, strText);
		break;
	default:
		break;
	}

 }

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_IndicatorOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_IndicatorOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < IndicatorOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusECpOpellEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_IndicatorOp::OnEnKillFocusECpOpellEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (UpdateCellData(m_nEditRow, m_nEditCol, _ttof(strText)))
	{
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_IndicatorOp::UpdateCellData(UINT nRow, UINT nCol, FLOAT fValue)
{
	switch (nRow)
	{
	case IndicatorOp_Standard:
		if (IndicatorOp_X == nCol)
			m_fIndicator_Std_X = fValue;
		else			  
			m_fIndicator_Std_Y = fValue;

		break;
	case IndicatorOp_Spec:
		if (IndicatorOp_X == nCol)
			m_fIndicator_Spec_X = fValue;
		else
			m_fIndicator_Spec_Y = fValue;
		break;
	default:
		break;
	}
 
	CString str;
	str.Format(_T("%.2f"), fValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);
 
	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_IndicatorOp::UpdateCellData_double(UINT nRow, UINT nCol, double dValue)
{
	CString str;
	str.Format(_T("%.1f"), dValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}
//=============================================================================
// Method		: CheckRectValue
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in const CRect * pRegionz
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_IndicatorOp::CheckRectValue(__in const CRect* pRegionz)
{
	if (NULL == pRegionz)
		return FALSE;

	if (pRegionz->left < 0)
		return FALSE;

	if (pRegionz->right < 0)
		return FALSE;

	if (pRegionz->top < 0)
		return FALSE;

	if (pRegionz->bottom < 0)
		return FALSE;

	if (pRegionz->Width() < 0)
		return FALSE;

	if (pRegionz->Height() < 0)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_IndicatorOp::GetCellData()
{
	if (m_pstModelInfo == NULL)
		return;

	m_pstModelInfo->fIndicator_Std_X = m_fIndicator_Std_X;
	m_pstModelInfo->fIndicator_Std_Y = m_fIndicator_Std_Y;

	m_pstModelInfo->fIndicator_Spec_X = m_fIndicator_Spec_X;
	m_pstModelInfo->fIndicator_Spec_Y = m_fIndicator_Spec_Y;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_IndicatorOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);
		FLOAT fValue = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			fValue = fValue + ((zDelta / 120)*0.1);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < fValue)
				fValue = fValue + ((zDelta / 120)*0.1);
			}

		if (iValue < 0)
			iValue = 0;

		if (iValue >1000)
			iValue = 1000;

		if (fValue < 0.0)
			fValue = 0.0;

		if (fValue > 2.0)
			fValue = 2.0;
		
		if (UpdateCellData(m_nEditRow, m_nEditCol, fValue))
		{
			strText.Format(_T("%.2f"), fValue);
			m_ed_CellEdit.SetWindowText(strText);
		}
	}
	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
