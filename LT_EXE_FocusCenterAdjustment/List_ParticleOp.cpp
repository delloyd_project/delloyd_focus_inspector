﻿
// List_ParticleOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_ParticleOp.h"

// CList_ParticleOp
#define IPtOp_ED_CELLEDIT		5001

IMPLEMENT_DYNAMIC(CList_ParticleOp, CListCtrl)

CList_ParticleOp::CList_ParticleOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0; 
	m_pstParticle = NULL;
}

CList_ParticleOp::~CList_ParticleOp()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_ParticleOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_ParticleOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_ParticleOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IPtOp_ED_CELLEDIT, &CList_ParticleOp::OnEnKillFocusEPtOpellEdit)
END_MESSAGE_MAP()

// CList_ParticleOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/12 - 19:05
// Desc.		:
//=============================================================================
int CList_ParticleOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER | LVS_EX_CHECKBOXES);

	InitHeader();

	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IPtOp_ED_CELLEDIT);

	this->GetHeaderCtrl()->EnableWindow(FALSE);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/3/12 - 19:05
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[PtOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	for (int nCol = 0; nCol < PtOp_MaxCol; nCol++)
		iColDivide += iHeaderWidth_PtOp[nCol];

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < PtOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() * iHeaderWidth_PtOp[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((rectClient.Width() * iHeaderWidth_PtOp[PtOp_Object]) / iColDivide) + (rectClient.Width() - iMisc);
	SetColumnWidth(PtOp_Object, iUnitWidth);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/3/12 - 19:06
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 18:56
// Desc.		:
//=============================================================================
void CList_ParticleOp::InitHeader()
{
	for (int nCol = 0; nCol < PtOp_MaxCol; nCol++)
		InsertColumn(nCol, g_lpszHeader_PtOp[nCol], iListAglin_PtOp[nCol], iHeaderWidth_PtOp[nCol]);

	for (int nCol = 0; nCol < PtOp_MaxCol; nCol++)
		SetColumnWidth(nCol, iHeaderWidth_PtOp[nCol]);
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 19:07
// Desc.		:
//=============================================================================
void CList_ParticleOp::InsertFullData()
{
	DeleteAllItems();

	for (int i = 0; i < PtOp_ItemNum; i++)
	{
		InsertItem(i, _T(""));
		SetRectRow(i);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/3/12 - 19:10
// Desc.		:
//=============================================================================
void CList_ParticleOp::SetRectRow(UINT nRow)
{
	CString strText;

	strText.Format(_T("%s"), g_lpszItem_PtOp[nRow]);
	SetItemText(nRow, PtOp_Object, strText);

	if(m_pstParticle->stParticleOp.rectData[nRow].bEllipse == TRUE)
		SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

	strText.Format(_T("%d"), m_pstParticle->stParticleOp.rectData[nRow].RegionList.CenterPoint().x);
	SetItemText(nRow, PtOp_PosX, strText);

	strText.Format(_T("%d"), m_pstParticle->stParticleOp.rectData[nRow].RegionList.CenterPoint().y);
	SetItemText(nRow, PtOp_PosY, strText);

	strText.Format(_T("%d"), m_pstParticle->stParticleOp.rectData[nRow].RegionList.Width());
	SetItemText(nRow, PtOp_Width, strText);

	strText.Format(_T("%d"), m_pstParticle->stParticleOp.rectData[nRow].RegionList.Height());
	SetItemText(nRow, PtOp_Height, strText);

	strText.Format(_T("%6.2f"), m_pstParticle->stParticleOp.rectData[nRow].dbBruiseConc);
	SetItemText(nRow, PtOp_BruiseConc, strText);

	strText.Format(_T("%d"), m_pstParticle->stParticleOp.rectData[nRow].iBruiseSize);
	SetItemText(nRow, PtOp_BruiseSize, strText);

}

//=============================================================================
// Method		: OnNMClick
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/3/12 - 19:11
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem == PtOp_Object)
		{
			UINT nBuffer;

			nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

			if (nBuffer == 0x2000)
			{
				m_pstParticle->stParticleOp.rectData[pNMItemActivate->iItem].bEllipse = FALSE;
				SetItemState(pNMItemActivate->iItem, 0x1000, LVIS_STATEIMAGEMASK);
			}

			if (nBuffer == 0x1000)
			{
				m_pstParticle->stParticleOp.rectData[pNMItemActivate->iItem].bEllipse = TRUE;
				SetItemState(pNMItemActivate->iItem, 0x2000, LVIS_STATEIMAGEMASK);
			}
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/3/12 - 19:11
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < PtOp_MaxCol && pNMItemActivate->iSubItem > PtOp_Object)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			ModifyStyle(WS_VSCROLL, 0);
		
			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEPtOpellEdit
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 19:11
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnEnKillFocusEPtOpellEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == PtOp_BruiseConc)
		UpdateCellData_double(m_nEditRow, m_nEditCol, _ttof(strText));
	else
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/3/12 - 19:12
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	CString str;

	if (iValue < 0)
		iValue = 0;

	CRect Data = m_pstParticle->stParticleOp.rectData[nRow].RegionList;

	switch (nCol)
	{
	case PtOp_PosX :
		m_pstParticle->stParticleOp.rectData[nRow]._Rect_Position_Sum((int)iValue, Data.CenterPoint().y, Data.Width(), Data.Height());
		break;

	case PtOp_PosY :
		m_pstParticle->stParticleOp.rectData[nRow]._Rect_Position_Sum(Data.CenterPoint().x, (int)iValue, Data.Width(), Data.Height());
		break;

	case PtOp_Width :
		m_pstParticle->stParticleOp.rectData[nRow]._Rect_Position_Sum2((int)iValue, Data.Height());
		break;

	case PtOp_Height:
		m_pstParticle->stParticleOp.rectData[nRow]._Rect_Position_Sum2(Data.Width(), (int)iValue);
		break;

	case PtOp_BruiseSize:
		m_pstParticle->stParticleOp.rectData[nRow].iBruiseSize = iValue;
		break;

	default:
		break;
	}

	if (m_pstParticle->stParticleOp.rectData[nRow].RegionList.left < 0)
	{
		m_pstParticle->stParticleOp.rectData[nRow].RegionList.left = 0;
		m_pstParticle->stParticleOp.rectData[nRow].RegionList.right = m_pstParticle->stParticleOp.rectData[nRow].RegionList.left + m_pstParticle->stParticleOp.rectData[nRow].RegionList.Width();
	}

	if (m_pstParticle->stParticleOp.rectData[nRow].RegionList.right > CAM_IMAGE_WIDTH)
	{
		m_pstParticle->stParticleOp.rectData[nRow].RegionList.right = CAM_IMAGE_WIDTH;
		int iGap = CAM_IMAGE_WIDTH - m_pstParticle->stParticleOp.rectData[nRow].RegionList.Width();
		if (iGap >= 0)
			m_pstParticle->stParticleOp.rectData[nRow].RegionList.left = iGap;
	}

	if (m_pstParticle->stParticleOp.rectData[nRow].RegionList.top < 0)
	{
		m_pstParticle->stParticleOp.rectData[nRow].RegionList.top = 0;
		m_pstParticle->stParticleOp.rectData[nRow].RegionList.bottom = m_pstParticle->stParticleOp.rectData[nRow].RegionList.Height() + m_pstParticle->stParticleOp.rectData[nRow].RegionList.top;
	}

	if (m_pstParticle->stParticleOp.rectData[nRow].RegionList.bottom > CAM_IMAGE_HEIGHT)
	{
		m_pstParticle->stParticleOp.rectData[nRow].RegionList.bottom = CAM_IMAGE_HEIGHT;
		int iGap = CAM_IMAGE_HEIGHT - m_pstParticle->stParticleOp.rectData[nRow].RegionList.Height();
		if (iGap >= 0)
			m_pstParticle->stParticleOp.rectData[nRow].RegionList.top = iGap;
	}

	if (m_pstParticle->stParticleOp.rectData[nRow].RegionList.Height() <= 0)
		m_pstParticle->stParticleOp.rectData[nRow]._Rect_Position_Sum2(m_pstParticle->stParticleOp.rectData[nRow].RegionList.Width(), 1);

	if (m_pstParticle->stParticleOp.rectData[nRow].RegionList.Width() > CAM_IMAGE_WIDTH)
		m_pstParticle->stParticleOp.rectData[nRow]._Rect_Position_Sum2(m_pstParticle->stParticleOp.rectData[nRow].RegionList.Width(), CAM_IMAGE_HEIGHT);

	if (m_pstParticle->stParticleOp.rectData[nRow].RegionList.Width() <= 0)
		m_pstParticle->stParticleOp.rectData[nRow]._Rect_Position_Sum2(1, m_pstParticle->stParticleOp.rectData[nRow].RegionList.Height());

	if (m_pstParticle->stParticleOp.rectData[nRow].RegionList.Height() > CAM_IMAGE_HEIGHT)
		m_pstParticle->stParticleOp.rectData[nRow]._Rect_Position_Sum2(CAM_IMAGE_WIDTH, m_pstParticle->stParticleOp.rectData[nRow].RegionList.Height());

	if (m_pstParticle->stParticleOp.rectData[nRow].iBruiseSize < 0)
		m_pstParticle->stParticleOp.rectData[nRow].iBruiseSize = 0;

	switch (nCol)
	{
	case PtOp_PosX:
		str.Format(_T("%d"), m_pstParticle->stParticleOp.rectData[nRow].RegionList.CenterPoint().x);
		break;

	case PtOp_PosY:
		str.Format(_T("%d"), m_pstParticle->stParticleOp.rectData[nRow].RegionList.CenterPoint().y);
		break;

	case PtOp_Width:
		str.Format(_T("%d"), m_pstParticle->stParticleOp.rectData[nRow].RegionList.Width());
		break;

	case PtOp_Height:
		str.Format(_T("%d"), m_pstParticle->stParticleOp.rectData[nRow].RegionList.Height());
		break;

	case PtOp_BruiseSize:
		str.Format(_T("%d"), m_pstParticle->stParticleOp.rectData[nRow].iBruiseSize);
		break;

	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);
	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/3/17 - 13:05
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::UpdateCellData_double(UINT nRow, UINT nCol, double dbValue)
{
	CString str;

	if (dbValue < 0)
		dbValue = 0;

	CRect Data = m_pstParticle->stParticleOp.rectData[nRow].RegionList;

	m_pstParticle->stParticleOp.rectData[nRow].dbBruiseConc = dbValue;

	if (m_pstParticle->stParticleOp.rectData[nRow].dbBruiseConc > 100)
		m_pstParticle->stParticleOp.rectData[nRow].dbBruiseConc = 100;

	str.Format(_T("%.2f"), m_pstParticle->stParticleOp.rectData[nRow].dbBruiseConc);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);
	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 19:13
// Desc.		:
//=============================================================================
void CList_ParticleOp::GetCellData()
{
	UINT nCheck;

	for (UINT nIdx = 0; nIdx < PtOp_ItemNum; nIdx++)
	{
		m_pstParticle->stParticleOp.rectData[nIdx]._Rect_Position_Sum(m_pstParticle->stParticleOp.rectData[nIdx].RegionList.CenterPoint().x, m_pstParticle->stParticleOp.rectData[nIdx].RegionList.CenterPoint().y, m_pstParticle->stParticleOp.rectData[nIdx].RegionList.Width(), m_pstParticle->stParticleOp.rectData[nIdx].RegionList.Height());


		nCheck = GetItemState(nIdx, LVIS_STATEIMAGEMASK);

		if (nCheck == 0x2000)
			m_pstParticle->stParticleOp.rectData[nIdx].bEllipse = TRUE;
		
		if (nCheck == 0x1000)
			m_pstParticle->stParticleOp.rectData[nIdx].bEllipse = FALSE;
	}

}

//=============================================================================
// Method		: OnMouseWheel
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/3/17 - 14:48
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	int znum = zDelta / 120;
	double dbznum = 0.1;
	bool FLAG = 0;

	if (znum > 0)
		FLAG = 1;
	else if (znum < 0)
		FLAG = 0;
	else if (znum == 0)
		return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);

	int casenum = pWndFocus->GetDlgCtrlID();

	if ((Change_DATA_CHECK(m_nEditRow) == TRUE))
	{
		CString str_buf;
		int buf = 0;
		double dbbuf = 0;
		int retval = 0;

		m_ed_CellEdit.GetWindowText(str_buf);

		str_buf.Remove(' ');

		if (str_buf == "")
		{
			buf = 0;
		}
		else
		{
			if (m_nEditCol == PtOp_BruiseConc)
			{
				dbbuf = _ttof(str_buf);
				dbbuf += dbznum;
			}
			else
			{
				buf = _ttoi(str_buf);
				buf += znum;
			}
		}

		if (m_nEditCol == PtOp_BruiseConc)
			str_buf.Format(_T("%.2f"), dbbuf);
		else
			str_buf.Format(_T("%d"), buf);

		m_ed_CellEdit.SetWindowText(str_buf);
		m_ed_CellEdit.SetSel(-2, -1);

		if (m_nEditCol == PtOp_BruiseConc)
			UpdateCellData_double(m_nEditRow, m_nEditCol, _ttof(str_buf));
		else
			UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(str_buf));
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: Change_DATA_CHECK
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nIdx
// Qualifier	:
// Last Update	: 2017/3/17 - 14:48
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::Change_DATA_CHECK(UINT nIdx)
{
	BOOL STAT = TRUE;

	/*상한*/
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.Width() > CAM_IMAGE_WIDTH) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.Height() > CAM_IMAGE_HEIGHT) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.CenterPoint().x > CAM_IMAGE_WIDTH) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.CenterPoint().y > CAM_IMAGE_WIDTH) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.top > CAM_IMAGE_HEIGHT) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.bottom > CAM_IMAGE_HEIGHT) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.left > CAM_IMAGE_WIDTH) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.right > CAM_IMAGE_WIDTH) { STAT = FALSE; }

	/*하한*/
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.Width() < 1) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.Height() < 1) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.CenterPoint().x < 0) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.CenterPoint().y < 0) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.top < 0) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.bottom < 0) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.left < 0) { STAT = FALSE; }
	if (m_pstParticle->stParticleOp.rectData[nIdx].RegionList.right < 0) { STAT = FALSE; }

	return STAT;
}
