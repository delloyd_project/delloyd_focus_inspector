﻿//*****************************************************************************
// Filename	: 	Wnd_MotionTable.h
// Created	:	2017/03/28 - 13:47
// Modified	:	2017/03/28 - 13:47
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************

#pragma once
#include "VGStatic.h"
#include "VGGroupWnd.h"
#include "resource.h"
#include "Def_TestDevice.h"

#define  MAX_AIXS		 100

#define	 MT_WIDTH_OFFSET 1.0
#define	 HEIGHT_OFFSET	 1.0

typedef enum MotionTable_ID
{
	IDC_ST_AXIS_NAME	= 1000,
	IDC_ST_SEN_AMP		= 1100,
	IDC_ST_SEN_POS		= 1200,
	IDC_ST_SEN_ORI		= 1300,
	IDC_ST_SEN_MOTION	= 1400,
	IDC_ST_SEN_LLIT		= 1500,
	IDC_ST_SEN_HOME		= 1600,
	IDC_ST_SEN_HLIT		= 1700,
	IDC_ST_SEN_ALRAM	= 1800,
	IDC_ST_SEN_MAXNUM = IDC_ST_SEN_ALRAM + 1000,
};

enum enMotionStatic
{
	ST_MT_AXIS_NAME = 0,
	ST_MT_POWER_NAME,
	ST_MT_POS_NAME,
	ST_MT_ORI_NAME,
	ST_MT_MOTION_NAME,
	ST_MT_HLIT_NAME,
	ST_MT_HOME_NAME,
	ST_MT_LLIT_NAME,
	ST_MT_ALRAM_NAME,
	ST_MT_MAXNUM,
};

static LPCTSTR g_szMotionStatusName[] =
{
	_T("AXIS NAME"),
	_T("POWER"),
	_T("POSITION"),
	_T("ORIGIN"),
	_T("IN MOTION"),
	_T("+ LIMIT"),
	_T("HOME"),
	_T("- LIMIT"),
	_T("ALRAM"),
	NULL
};

// CWnd_MotionTable
class CWnd_MotionTable : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MotionTable)

public:
	CWnd_MotionTable();
	virtual ~CWnd_MotionTable();

	ST_Device*	m_pstDevice;

	void SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnAxisSelect			(UINT nID);
	afx_msg void	OnAxisAmpCtr			(UINT nID);
	afx_msg void	OnAxisAlramCtr			(UINT nID);

	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	void	SetUpdataAxisName			();

	void	SetUpdataDataReset			(UINT nAxis);

	void	SetDeleteTimer				();

	DECLARE_MESSAGE_MAP()

protected:

	UINT			m_nAxisNum;

	//	타이머
	HANDLE			m_hTimerQueue;
	HANDLE			m_hTimer_SensorCheck;
	
	CVGGroupWnd		m_Group_Nmae;

	CVGStatic		m_st_AxisName[ST_MT_MAXNUM];
	CVGStatic		m_st_AxisSen[ST_MT_MAXNUM][MAX_AIXS];

	static VOID CALLBACK TimerRoutine_SensorCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);

	void	OnMonitorSensorCheck		();
	void	CreateTimerQueue_Mon		();
	void	DeleteTimerQueue_Mon		();
	void	CreateTimerSensorCheck		();
	void	DeleteTimerSensorCheck		();

	void	GetMotorAmpStatus			();
	void	GetMotorOriginStatus		();
	void	GetMotorCurrentPosStatus	();
	void	GetMotorMotionStatus		();
	void	GetMotorLimitStatus			();
	void	GetMotorHomeStatus			();
	void	GetMotorAlarmStatus			();
};