﻿//*****************************************************************************
// Filename	: Def_DataStruct.h
// Created	: 2012/11/1
// Modified	: 2016/12/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Def_DataStruct_h__
#define Def_DataStruct_h__

#include <afxwin.h>
#include "Def_Enum.h"
#include "Def_Test.h"
#include "Def_DataStruct_Cm.h"
#include "Def_MES_Cm.h"
#include "Def_Motion.h"
#include "Def_DigitiIO.h"
#include "Def_IndicatorSensor.h"
#include "Def_EVMS_Cm.h"
#include "Def_MES_Mcnex.h"

#include "List_Work_Total.h"
#include "List_Work_Current.h"
#include "List_Work_CenterPoint.h"
#include "List_Work_EIAJ.h"
#include "List_Work_SFR.h"
#include "List_Work_Rotate.h"
#include "List_Work_Particle.h"

#pragma pack(push, 1)

//---------------------------------------------------------
// 검사 결과 저장용 구조체
//---------------------------------------------------------
typedef struct _tag_Worklist
{
	CList_Work_Total*			pList_Total;
	CList_Work_Current*			pList_Current;
	CList_Work_CenterPoint*		pList_CenterPoint;
	CList_Work_EIAJ*			pList_EIAJ;
	CList_Work_SFR*				pList_SFR;
	CList_Work_Rotate*			pList_Rotate;
	CList_Work_Particle*		pList_Particle;

	_tag_Worklist()
	{
		pList_Total			= NULL;
		pList_Current		= NULL;
		pList_CenterPoint	= NULL;
		pList_EIAJ			= NULL;
		pList_SFR			= NULL;
		pList_Rotate		= NULL;
		pList_Particle		= NULL;
	};

	void Reset()
	{
		pList_Total			= NULL;
		pList_Current		= NULL;
		pList_CenterPoint	= NULL;
		pList_EIAJ			= NULL;
		pList_SFR			= NULL;
		pList_Rotate		= NULL;
		pList_Particle		= NULL;
	};

	_tag_Worklist& operator= (_tag_Worklist& ref)
	{
		pList_Total			= ref.pList_Total;			
		pList_Current		= ref.pList_Current;
		pList_CenterPoint	= ref.pList_CenterPoint;
		pList_EIAJ			= ref.pList_EIAJ;
		pList_SFR			= ref.pList_SFR;
		pList_Rotate		= ref.pList_Rotate;
		pList_Particle		= ref.pList_Particle;
		return *this;
	};

}ST_Worklist, *PST_Worklist;

//---------------------------------------------------------
// POGO 카운트, Beat 카운트
//---------------------------------------------------------
typedef struct _tag_PogoInfo
{
	CString		szPogoFilename;

	DWORD		dwCount_Max[MAX_SITE_CNT];
	DWORD		dwCount[MAX_SITE_CNT];

	_tag_PogoInfo()
	{
		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			dwCount_Max[nIdx] = 50000;
			dwCount[nIdx] = 0;
		}
	};

	_tag_PogoInfo& operator= (_tag_PogoInfo& ref)
	{
		szPogoFilename = ref.szPogoFilename;

		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			dwCount_Max[nIdx] = ref.dwCount_Max[nIdx];
			dwCount[nIdx] = ref.dwCount[nIdx];
		}

		return *this;
	};

	void ResetCount(UINT nPogoIdx)
	{
		if (nPogoIdx < MAX_SITE_CNT)
			dwCount[nPogoIdx] = 0;
	}

	BOOL IsMaxCount(UINT nPogoIdx)
	{
		if (nPogoIdx < MAX_SITE_CNT)
			return dwCount_Max[nPogoIdx] <= dwCount[nPogoIdx] ? TRUE : FALSE;
		else
			return FALSE;
	};

	void IncreasePogoCount(UINT nPogoIdx)
	{
		if (nPogoIdx < MAX_SITE_CNT)
			++dwCount[nPogoIdx];
	};

}ST_PogoInfo, PST_PogoInfo;


//---------------------------------------------------------
// DriverCount
//---------------------------------------------------------
typedef struct _tag_DriverCountInfo
{
	CString		szDriverCountFilename;

	DWORD		dwCount_Max[MAX_SITE_CNT];
	DWORD		dwCount[MAX_SITE_CNT];

	_tag_DriverCountInfo()
	{
		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			dwCount_Max[nIdx] = 50000;
			dwCount[nIdx] = 0;
		}
	};

	_tag_DriverCountInfo& operator= (_tag_DriverCountInfo& ref)
	{
		szDriverCountFilename = ref.szDriverCountFilename;

		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			dwCount_Max[nIdx] = ref.dwCount_Max[nIdx];
			dwCount[nIdx] = ref.dwCount[nIdx];
		}

		return *this;
	};

	void ResetCount(UINT nDriverCountIdx)
	{
		if (nDriverCountIdx < MAX_SITE_CNT)
			dwCount[nDriverCountIdx] = 0;
	}

	BOOL IsMaxCount(UINT nDriverCountIdx)
	{
		if (nDriverCountIdx < MAX_SITE_CNT)
			return dwCount_Max[nDriverCountIdx] <= dwCount[nDriverCountIdx] ? TRUE : FALSE;
		else
			return FALSE;
	};

	void IncreaseDriverCount(UINT nDriverCountIdx)
	{
		if (nDriverCountIdx < MAX_SITE_CNT)
			++dwCount[nDriverCountIdx];
	};

}ST_DriverCountInfo, PST_DriverCountInfo;



//---------------------------------------------------------
// Vision Option
//---------------------------------------------------------
typedef struct _tag_VisionOptInfo
{
	int		iColletType;
	int		iSearchArea;
	int		iBinaryData;
	int		iHoleCount;
	double	dErrorStep;
	double	dLensCTRatio;
	double	dHoleLengthMin;
	double	dHoleLengthMax;

	int		iBrightness;

	void Reset()
	{
		iColletType		= 0;
		iSearchArea		= 0;
		iBinaryData		= 0;
		iHoleCount		= 0;
		dErrorStep = 0;
		dLensCTRatio = 0;
		dHoleLengthMin = 0;
		dHoleLengthMax = 0;
		iBrightness = 0;
	};

	_tag_VisionOptInfo()
	{
		iColletType		= 0;
		iSearchArea		= 0;
		iBinaryData		= 0;
		iHoleCount		= 0;
		dErrorStep		= 0;
		dLensCTRatio	= 0;
		dHoleLengthMin	= 0;
		dHoleLengthMax	= 0;
		iBrightness = 0;
	};

	_tag_VisionOptInfo& operator= (_tag_VisionOptInfo& ref)
	{
		iColletType		= ref.iColletType;
		iSearchArea		= ref.iSearchArea;
		iBinaryData		= ref.iBinaryData;
		iHoleCount		= ref.iHoleCount;
		dErrorStep		= ref.dErrorStep;
		dLensCTRatio	= ref.dLensCTRatio;
		dHoleLengthMin	= ref.dHoleLengthMin;
		dHoleLengthMax = ref.dHoleLengthMax;
		iBrightness = ref.iBrightness;

		return *this;
	};

}ST_VisionOptInfo, PST_VisionOptInfo;

//---------------------------------------------------------
// AF 정보
//---------------------------------------------------------
typedef struct _tag_AFInfo
{
	double	dSTD_Displace;
	double	dSTD_DisplaceDev;
	double	dLensConchoid;
	BOOL	bLensDirection;
	double	dAFStepDegree1;
	double	dAFStepDegree2;
	double	dAFStepDegree3;
	double	dAFRotateCnt;

	void Reset()
	{
		dSTD_Displace		= 0;
		dSTD_DisplaceDev	= 0;
		dLensConchoid		= 0;
		bLensDirection		= Reverse;
		dAFStepDegree1		= 0;
		dAFStepDegree2		= 0;
		dAFStepDegree3		= 0;
		dAFRotateCnt		= 0;
	};

	_tag_AFInfo()
	{
		Reset();
	};

	_tag_AFInfo& operator= (_tag_AFInfo& ref)
	{
		dSTD_Displace		= ref.dSTD_Displace;
		dSTD_DisplaceDev	= ref.dSTD_DisplaceDev;
		dLensConchoid		= ref.dLensConchoid;
		bLensDirection		= ref.bLensDirection;
		dAFStepDegree1		= ref.dAFStepDegree1;
		dAFStepDegree2		= ref.dAFStepDegree2;
		dAFStepDegree3		= ref.dAFStepDegree3;
		dAFRotateCnt		= ref.dAFRotateCnt;

		return *this;
	};

}ST_AFInfo, PST_AFInfo;

//---------------------------------------------------------
// Customer Motion Teach
//---------------------------------------------------------
typedef struct _tag_CustomTeach
{
	int iUnloadingMotionSeq;
	int iVisionMotionSeq;
	int iAFPositionSeq;
	int iDisplaceMotionASeq;
	int iDisplaceMotionBSeq;

	double	dUnloadingMotion_OffsetZ;
	double	dUnloadingMotion_OffsetX;
	double	dUnloadingMotion_OffsetY;
	double	dVisionMotion_OffsetZ;
	double	dVisionMotion_OffsetX;
	double	dVisionMotion_OffsetY;
	double	dDisplaceMotionA_OffsetZ;
	double	dDisplaceMotionA_OffsetX;
	double	dDisplaceMotionA_OffsetY;
	double	dDisplaceMotionB_OffsetZ;
	double	dDisplaceMotionB_OffsetX;
	double	dDisplaceMotionB_OffsetY;
	double	dAFPosition_OffsetZ;
	double	dAFPosition_OffsetX;
	double	dAFPosition_OffsetY;

	void Reset()
	{
		iUnloadingMotionSeq			= 0;
		iVisionMotionSeq			= 0;
		iAFPositionSeq				= 0;
		iDisplaceMotionASeq			= 0;
		iDisplaceMotionBSeq			= 0;
		dVisionMotion_OffsetZ		= 0;
		dVisionMotion_OffsetX		= 0;
		dVisionMotion_OffsetY		= 0;
		dDisplaceMotionA_OffsetZ	= 0;
		dDisplaceMotionA_OffsetX	= 0;
		dDisplaceMotionA_OffsetY	= 0;
		dDisplaceMotionB_OffsetZ	= 0;
		dDisplaceMotionB_OffsetX	= 0;
		dDisplaceMotionB_OffsetY	= 0;
		dAFPosition_OffsetZ			= 0;
		dAFPosition_OffsetX			= 0;
		dAFPosition_OffsetY			= 0;
	};

	_tag_CustomTeach()
	{
		iUnloadingMotionSeq			= 0;
		iVisionMotionSeq			= 0;
		iAFPositionSeq				= 0;
		iDisplaceMotionASeq			= 0;
		iDisplaceMotionBSeq			= 0;
		dVisionMotion_OffsetZ		= 0;
		dVisionMotion_OffsetX		= 0;
		dVisionMotion_OffsetY		= 0;
		dDisplaceMotionA_OffsetZ	= 0;
		dDisplaceMotionA_OffsetX	= 0;
		dDisplaceMotionA_OffsetY	= 0;
		dDisplaceMotionB_OffsetZ	= 0;
		dDisplaceMotionB_OffsetX	= 0;
		dDisplaceMotionB_OffsetY	= 0;
		dAFPosition_OffsetZ			= 0;
		dAFPosition_OffsetX			= 0;
		dAFPosition_OffsetY			= 0;
	};

	_tag_CustomTeach& operator= (_tag_CustomTeach& ref)
	{
		iUnloadingMotionSeq			= ref.iUnloadingMotionSeq;
		iVisionMotionSeq			= ref.iVisionMotionSeq;
		iAFPositionSeq				= ref.iAFPositionSeq;
		iDisplaceMotionASeq			= ref.iDisplaceMotionASeq;
		iDisplaceMotionBSeq			= ref.iDisplaceMotionBSeq;
		dVisionMotion_OffsetZ		= ref.dVisionMotion_OffsetZ;
		dVisionMotion_OffsetX		= ref.dVisionMotion_OffsetX;
		dVisionMotion_OffsetY		= ref.dVisionMotion_OffsetY;
		dDisplaceMotionA_OffsetZ	= ref.dDisplaceMotionA_OffsetZ;
		dDisplaceMotionA_OffsetX	= ref.dDisplaceMotionA_OffsetX;
		dDisplaceMotionA_OffsetY	= ref.dDisplaceMotionA_OffsetY;
		dDisplaceMotionB_OffsetZ	= ref.dDisplaceMotionB_OffsetZ;
		dDisplaceMotionB_OffsetX	= ref.dDisplaceMotionB_OffsetX;
		dDisplaceMotionB_OffsetY	= ref.dDisplaceMotionB_OffsetY;
		dAFPosition_OffsetZ			= ref.dAFPosition_OffsetZ;
		dAFPosition_OffsetX			= ref.dAFPosition_OffsetX;
		dAFPosition_OffsetY			= ref.dAFPosition_OffsetY;

		return *this;
	};

}ST_CustomTeach, PST_CustomTeach;

//---------------------------------------------------------
// LOT 정보
//---------------------------------------------------------
typedef struct _tag_LOTInfo
{
	CString			szLotName;		// LOT 이름
	CString			szOperator;		// 작업자
	
	UINT			nLotCount;
	BOOL			bLotStatus;		// LOT 진행 상태

	SYSTEMTIME		StartTime;		// 전체 검사 시작 시간

	_tag_LOTInfo()
	{
		ZeroMemory(&StartTime, sizeof(SYSTEMTIME));
		nLotCount	= 0;
		bLotStatus	= FALSE;
	};

	_tag_LOTInfo& operator= (_tag_LOTInfo& ref)
	{
		szLotName		= ref.szLotName;
		szOperator		= ref.szOperator;

		memcpy(&StartTime, &ref.StartTime, sizeof(SYSTEMTIME));

		nLotCount		= ref.nLotCount;
		bLotStatus		= ref.bLotStatus;

		return *this;
	};

	void Reset()
	{
		szLotName.Empty();
		szOperator.Empty();

		ZeroMemory(&StartTime, sizeof(SYSTEMTIME));

		nLotCount = 0;
		bLotStatus = FALSE;
	};

	virtual void SetLotInfo(__in LPCTSTR szInLOTNum, __in LPCTSTR szInOperator)
	{
		szLotName = szInLOTNum;
		szOperator = szInOperator;		
	};

}ST_LOTInfo, *PST_LOTInfo;

//---------------------------------------------------------
// 모델 설정값 구조체
//---------------------------------------------------------
typedef struct _tag_ModelInfo
{
	CString				szModelCode;	// 제품 Model 명칭
	CString				szModelFile;	// 모델 설정 파일
	CString				szMotorFile;	// 모델 설정 파일

	FLOAT				fVoltage;		// Voltage
	CString				szBinFile;		// Bin File
	CString				szPogoName;		// Pogo 설정 파일
	CString				szDriverCountName;		// DriverCount 설정 파일

	UINT				nCameraDelay;		// 카메라 안정화 시간
	UINT				nCameraDistortion;	// 카메라 안정화 시간

	DWORD				dwWidth;		// Width 
	DWORD				dwHeight;		// Height 

	UINT				nQuadRectPos; // Chart 의 4방면중 rect 위치

	BOOL				bModuleFixCheck; // Module Fix Check 유무 Flag
	BOOL				bMoveStageCheck; // MoveStage Check 유무 Flag

	UINT				nGrabType;

	UINT				nLotTryCnt;		// MES 차수
	CString				szLotID;		// MES LOT ID

	ST_AllMotorData*	pstAllMotorData;

	CArray <UINT, UINT&> TestItemz;	// 선택된 Test Itemz

	// AF Info
	ST_AFInfo				stAFInfo;

	// Custom Teach
	ST_CustomTeach			stCustomTeach;

	// 각 검사 항목별 설정
	ST_LT_TI_Current		stCurrent;
	ST_LT_TI_Rotate			stRotate;
	ST_LT_TI_EIAJ			stEIAJ;
	ST_LT_TI_SFR			stSFR;
	ST_LT_TI_CenterPoint	stCenterPoint;
	ST_LT_TI_Particle		stParticle;

	/*기준*/
	FLOAT				fIndicator_Std_X; // Indicator Std X
	FLOAT				fIndicator_Std_Y; // Indicator Std Y

	/*편차*/
	FLOAT				fIndicator_Spec_X; // Indicator Spec X
	FLOAT				fIndicator_Spec_Y; // Indicator Spec Y

	UINT 				nMasterSpcX;
	UINT				nMasterSpcY;

	int					iMasterInfoX;
	int					iMasterInfoY;

	UINT				nPicViewMode;


	UINT				nAlign;
	UINT				nDistortion;

	_tag_ModelInfo()
	{
		nGrabType		= GrabType_NTSC;
		pstAllMotorData = NULL;
		fVoltage		= 5.5f;
		nCameraDelay	= 500;
		nMasterSpcX		= 0;
		nMasterSpcY		= 0;
		iMasterInfoX	= 0;
		iMasterInfoY	= 0;
		dwWidth			= 720;
		dwHeight		= 480;
		nQuadRectPos	= 0;
		fIndicator_Std_X = 0;
		fIndicator_Std_Y = 0;
		fIndicator_Spec_X = 0;
		fIndicator_Spec_Y = 0;
		bModuleFixCheck = 0;
		bMoveStageCheck = 0;
		nLotTryCnt		= 0 ;

		stAFInfo.Reset();
		stCustomTeach.Reset();

		nPicViewMode	= PIC_Standby;
		nAlign = 0;
		nDistortion = 0;
	};

	_tag_ModelInfo& operator= (_tag_ModelInfo& ref)
	{
		nGrabType		= ref.nGrabType;
		szModelCode		= ref.szModelCode;
		szModelFile		= ref.szModelFile;
		fVoltage		= ref.fVoltage;
		nCameraDelay    = ref.nCameraDelay;
		szBinFile		= ref.szBinFile;
		szPogoName		= ref.szPogoName;
		szDriverCountName = ref.szDriverCountName;
		dwWidth			= ref.dwWidth;
		dwHeight		= ref.dwHeight;
		nQuadRectPos	= ref.nQuadRectPos;
		fIndicator_Std_X = ref.fIndicator_Std_X;
		fIndicator_Std_Y = ref.fIndicator_Std_Y;
		fIndicator_Spec_X = ref.fIndicator_Spec_X;
		fIndicator_Spec_Y = ref.fIndicator_Spec_Y;
		bModuleFixCheck = ref.bModuleFixCheck;
		bMoveStageCheck = ref.bMoveStageCheck;
		nMasterSpcX		= ref.nMasterSpcX;
		nMasterSpcY		= ref.nMasterSpcY;
		stAFInfo		= ref.stAFInfo;
		stCustomTeach	= ref.stCustomTeach;
		nPicViewMode	= ref.nPicViewMode;

		iMasterInfoX	= ref.iMasterInfoX;	
		iMasterInfoY	= ref.iMasterInfoY;

		nLotTryCnt		= ref.nLotTryCnt;
		szLotID			= ref.szLotID;
		szMotorFile		= ref.szMotorFile;
		nAlign = ref.nAlign;
		nDistortion = ref.nDistortion;

		TestItemz.RemoveAll();
		TestItemz.Copy(ref.TestItemz);

		return *this;
	};

	void Reset()
	{
		nGrabType = GrabType_NTSC;

		szModelCode.Empty();
		szModelFile.Empty();
		szBinFile.Empty();
		szPogoName.Empty();
		szDriverCountName.Empty();
		szMotorFile.Empty();

		TestItemz.RemoveAll();	

		fVoltage		= 5.5f;
		nCameraDelay	= 500;
		nMasterSpcX		= 0;
		nMasterSpcY		= 0;

		iMasterInfoX	= 0;
		iMasterInfoY	= 0;
		dwWidth			= 720;
		dwHeight		= 480;
		nQuadRectPos	= 0;
		fIndicator_Std_X = 0;
		fIndicator_Std_Y = 0;
		fIndicator_Spec_X = 0;
		fIndicator_Spec_Y = 0;
		bModuleFixCheck = 0;
		bMoveStageCheck = 0;
		nDistortion = 0;

		nLotTryCnt		= 0;
		szLotID.Empty();

		stAFInfo.Reset();
		nPicViewMode = PIC_Standby;
	};
}ST_ModelInfo, *PST_ModelInfo;

//---------------------------------------------------------
// 사이트별 검사 정보
//---------------------------------------------------------
typedef struct _tag_SiteInfo : public ST_SiteInfo_Base
{
	_tag_SiteInfo()
	{
		
	};

	_tag_SiteInfo& operator= (_tag_SiteInfo& ref)
	{
		__super::operator=(ref);


		return *this;
	};

	void Reset()
	{
		__super::Reset();
	};

	void SetResult(enTestResult ResultPara)
	{
		__super::SetResult(ResultPara);
		
	};
}ST_SiteInfo, *PST_SiteInfo;

//---------------------------------------------------------
// 프로그램에 사용되는 경로
//---------------------------------------------------------
typedef struct _tag_ProgramPath
{
	CString		szProgram;		// 프로그램 시작 경로
	CString		szLog;			// LOG 경로
	CString		szReport;		// 검사 결과 Report 경로
	CString		szModel;		// 모델 설정 파일 경로
	CString		szImage;		// 이미지 저장 경로
	CString		szPogo;			// 포고 설정 파일 저장 경로
	CString		szDriverCount;	// Driver Count 설정 파일 저장 경로
	CString		szBin;			// Bin File 저장 경로
	CString		szMotor;		// Motor File 저장 경로
	CString		szMes;			// Mes File 저장 경로
	CString		szMes2;			// Mes File 저장 경로

	CString		szInitIniFile;
	CString		szDefModelFile;
	CString		szDefaultEnv;

}ST_ProgramPath, *PST_ProgramPath;

//---------------------------------------------------------
// 검사 진행 시간 측정용 구조체
//---------------------------------------------------------
typedef struct _tag_TestTime : public ST_TestTime_Base
{
	void Reset()
	{
		__super::Reset();
	};
}ST_TestTime, *PST_TestTime;



//---------------------------------------------------------
// 검사 결과 저장용 구조체 (MES)
//---------------------------------------------------------
typedef struct _tag_MES_Worklist_Mcnex
{
	CString szEqpCode;	// 설비 코드 : MES 파일 읽어올 때 매칭.
	CString szMESPath;	// MES 경로 : 읽어올 MES 파일 경로.

	BOOL bStatus;
	ST_MES_TotalResult stMESResult;

	_tag_MES_Worklist_Mcnex()
	{
		bStatus = FALSE;
	};

	CString GetTextFileData(__in UINT nHeader)
	{
		CString szOutData;

		if (szMESPath.IsEmpty())
			return szOutData;

		if (szEqpCode.IsEmpty())
			return szOutData;

		CFileFind finder;
		CStdioFile file;
		CFileException e;

		CString szFileName;
		CString szFilePath = szMESPath + _T("\\") + _T("*.txt");

		BOOL bWorking = finder.FindFile(szFilePath);
		while (bWorking)
		{
			bWorking = finder.FindNextFile();

			if (finder.IsArchived())
			{
				szFileName = finder.GetFileName();
				if (szFileName == _T(".") || szFileName == _T("..") || szFileName == _T("Thumbs.db"))
					continue;

				szFileName = finder.GetFileTitle();
			}
		}

		finder.Close();

		if (szFileName.Find(szEqpCode) < 0)
			return szOutData;

		szFilePath = szMESPath + _T("\\") + szFileName + _T(".txt");

		if (file.Open(szFilePath, CFile::modeRead, &e))
		{
			CString szText;
			CString szSubText;

			if (file.ReadString(szText))
			{
				AfxExtractSubString(szSubText, szText, nHeader, _T(','));
				szOutData = szSubText;
			}

			file.Close();
		}

		return szOutData;
	};


	BOOL GetMESParameter(__in CString szEqpCode)
	{
		if (szEqpCode.IsEmpty())
		{
			return bStatus = FALSE;
		}

		stMESResult.Reset();
		stMESResult.szLotNo = GetTextFileData(enHeader_FOC_LOT_NO);
		stMESResult.szLotCardNo = GetTextFileData(enHeader_FOC_LOT_CARD_NO);
		stMESResult.szChannel = GetTextFileData(enHeader_FOC_Channel);
		stMESResult.szEqpCode = GetTextFileData(enHeader_FOC_EqpCode);
		stMESResult.szItemNumber = GetTextFileData(enHeader_FOC_ItemNumber);
		stMESResult.szBarcode = GetTextFileData(enHeader_FOC_Barcode);
		stMESResult.szOperator = GetTextFileData(enHeader_FOC_Operator);
		stMESResult.szTestDate = GetTextFileData(enHeader_FOC_TestDate);
		stMESResult.szTestTime = GetTextFileData(enHeader_FOC_TestTime);

		if (szEqpCode != stMESResult.szEqpCode)
		{
			return bStatus = FALSE;
		}

		return bStatus = TRUE;
	};

	void SetMESTestResult(__in ST_CamInfo stCamInfo)
	{
		CString szBuf;
		UINT nResult = 0;

		// 헤더 삽입
		for (int i = 0; i < enHeader_FOC_Item_Max; i++)
		{
			stMESResult.ItemHeaderz.Add(g_lpszHeader_FOC_Item_MES[i]);
		}

		// 아이템 결과 삽입
		// LOT_NO
		stMESResult.Itemz.Add(GetTextFileData(enHeader_FOC_LOT_NO));

		// LOT_CARD_NO
		stMESResult.Itemz.Add(GetTextFileData(enHeader_FOC_LOT_CARD_NO));

		// 채널번호
		stMESResult.Itemz.Add(GetTextFileData(enHeader_FOC_Channel));

		// 설비코드
		stMESResult.Itemz.Add(GetTextFileData(enHeader_FOC_EqpCode));

		// 지시번호
		stMESResult.Itemz.Add(GetTextFileData(enHeader_FOC_ItemNumber));

		// 품번
		stMESResult.Itemz.Add(GetTextFileData(enHeader_FOC_Barcode));

		// 작업자
		stMESResult.Itemz.Add(GetTextFileData(enHeader_FOC_Operator));

		// 처리일자
		stMESResult.Itemz.Add(GetTextFileData(enHeader_FOC_TestDate));

		// 처리일시
		stMESResult.Itemz.Add(GetTextFileData(enHeader_FOC_TestTime));

		// 최종결과
		nResult = stCamInfo.nJudgment;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);

		// 전류
		nResult = stCamInfo.stCurrentData.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);

		// 전류 값
		szBuf.Format(_T("%.1f"), stCamInfo.stCurrentData.nCurrent);
		stMESResult.Itemz.Add(szBuf);

	
		// 중심점 검출 1
		nResult = stCamInfo.stCenterPointData.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);
		// X
		szBuf.Format(_T("%d"), stCamInfo.stCenterPointData.iResult_Offset_X);
		stMESResult.Itemz.Add(szBuf);
		// Y
		szBuf.Format(_T("%d"), stCamInfo.stCenterPointData.iResult_Offset_Y);
		stMESResult.Itemz.Add(szBuf);

	
		// 해상력 SFR
		nResult = stCamInfo.stSFRData.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);
		// S1~S30
		for (int i = 0; i < Region_SFR_MaxEnum; i++)
		{
			szBuf.Format(_T("%.2f"), stCamInfo.stSFRData.dbResultValue[i]);
			stMESResult.Itemz.Add(szBuf);
		}

	
	

	};



}ST_MES_Worklist_Mcnex, *PST_MES_Worklist_Mcnex;

//---------------------------------------------------------
// 전체 검사에 관련된 데이터 기록용 구조체
//---------------------------------------------------------
typedef struct _tag_InspectionInfo : public ST_InspectionInfo_Base
{
	ST_CamInfo			CamInfo;			// Camera 검사 정보
	ST_SiteInfo			SiteInfo;			// 사이트 검사 정보

	ST_ModelInfo		ModelInfo;			// 모델 정보
	ST_LOTInfo			LotInfo;			// LOT 정보
	ST_Worklist			WorklistInfo;		// 검사 결과 워크리스트
	ST_Yield			YieldInfo;			// 수율
	ST_ProgramPath		Path;				// 프로그램에 사용되는 폴더
	ST_TestTime			Time;				// 검사 진행 시간	
	ST_PogoInfo			PogoInfo;			// 포고 카운트, 비트 카운트
	ST_DriverCountInfo	DriverCountInfo;	// Driver Count
	ST_CycleTime		CycleTime;			// 검사 진행 시간
	ST_MES_Worklist_Mcnex	MESInfo;			// MES 정보

	CString				szRecvLotID;

	UINT				nRecvLotTryCnt;

	CString				szBarcodeBuf;		// 바코드 수신시 저장용 버퍼 (1 소켓)
	
	// 시업 점검
	BOOL m_bMasterCheck;

	// 영상 신호 상태
	BOOL				bVideoSignal[MAX_MODULE_CNT];

	// Digital In 신호 
	BOOL				bDigitalIn[MAX_DIGITAL_IO];

	// 이전 제품 배출 시간
	DWORD				dwPrevOutputTime;
	// 현재 Tact Time
	DWORD				dwTactTime;

	_tag_InspectionInfo()
	{
		for (UINT nUnitIdx = 0; nUnitIdx < MAX_MODULE_CNT; nUnitIdx++)
		{
			bVideoSignal[nUnitIdx] = FALSE;
		}

		Judgment_All		= TR_Empty;
		dwPrevOutputTime	= 0;
		dwTactTime			= 0;
	};
	
	void ResetCamInfo()	// 전체 카메라 정보 초기화
	{
		CamInfo.Reset();
		SiteInfo.Reset();

		WorklistInfo.Reset();
		Judgment_All	= TR_Empty;
	};
	
	void SetInformation_Input (__in LPCTSTR szBarcode)
	{
		CamInfo.szLotID			= szLotName;
		CamInfo.szBarcode		= szBarcode;		
		CamInfo.szModelName		= szModelName;
		CamInfo.szOperatorName	= szOperatorName;
	};

	DWORD SetTactTime(__in DWORD dwOutputTime)
	{
		if (0 != dwPrevOutputTime)
		{
			if (dwPrevOutputTime < dwOutputTime)
			{
				dwTactTime = dwOutputTime - dwPrevOutputTime;
			}
			else
			{
				dwTactTime = 0xFFFFFFFF - dwPrevOutputTime + dwOutputTime;
			}

			dwPrevOutputTime = dwOutputTime;
		}
		else
		{
			dwTactTime = 0;
			dwPrevOutputTime = dwOutputTime;
		}

		return dwTactTime;
	};

	virtual void SetLotInfo(__in LPCTSTR szInLOTNum, __in LPCTSTR szInOperator)
	{
		__super::SetLotInfo(szInLOTNum, szInOperator);
		szLotName = szInLOTNum;
		szOperatorName = szInOperator;
		LotInfo.SetLotInfo(szInLOTNum, szInOperator);		
	};

	void SetLotStatus(__in BOOL bStart)
	{
		LotInfo.bLotStatus = bStart;
	};

	void SetLotName(__in LPCTSTR szInLOTNum)
	{
		szLotName = szInLOTNum;
		LotInfo.szLotName = szInLOTNum;
	};

	void ResetLotName()
	{
		szLotName.Empty();
		LotInfo.szLotName.Empty();
	};

	void SetInputTime()
	{
		GetLocalTime(&Time.tmStart_All);
		Time.dwStart_All = timeGetTime();

		CamInfo.SetInputTime(Time.tmStart_All, Time.dwStart_All);		
	};

	void SetOutputTime()
	{
		GetLocalTime(&Time.tmEnd_All);
		Time.dwDuration_All = timeGetTime();
		dwTactTime = SetTactTime(Time.dwDuration_All);

		CamInfo.SetOutputTime(Time.tmEnd_All, Time.dwDuration_All);				
		CamInfo.dwTactTime = dwTactTime;
	};

	void ResetBarcodeBuffer()
	{
		szBarcodeBuf.Empty();
		//szarBarcodeBuf.RemoveAll();
	};
}ST_InspectionInfo, *PST_InspectionInfo;
//---------------------------------------------------------
// Image Mode 관련 구조체
//---------------------------------------------------------
typedef struct _tag_ImageMode
{
	enImageMode eImageMode;
	BOOL		bImageSaveMode;
	CString		szImageCommonPath;
	CString		szImagePath;

	_tag_ImageMode()
	{
		eImageMode = ImageMode_LiveCam;
	};

	_tag_ImageMode& operator= (_tag_ImageMode& ref)
	{
		bImageSaveMode		= ref.bImageSaveMode;
		szImageCommonPath	= ref.szImageCommonPath;
		eImageMode			= ref.eImageMode;
		szImagePath			= ref.szImagePath;
		return *this;
	};
}ST_ImageMode, *PST_ImageMode;

typedef struct _tag_PicImageCaptureMode
{
	BOOL	bImageCaptureMode;
	CString szImagePath;

	_tag_PicImageCaptureMode()
	{
		bImageCaptureMode = 0;
		szImagePath.Empty();
	};

	_tag_PicImageCaptureMode& operator= (_tag_PicImageCaptureMode& ref)
	{
		bImageCaptureMode	= ref.bImageCaptureMode;
		szImagePath			= ref.szImagePath;
		return *this;
	};
}ST_PicImageCaptureMode, *PST_PicImageCaptureMode;


#pragma pack (pop)

#endif // Def_DataStruct_h__
