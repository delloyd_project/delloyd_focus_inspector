﻿#pragma once

#include "TestItem.h"
#include "Def_TestDevice.h"
#include "cv.h"
#include "highgui.h"

class CTI_Collet
{
public:
	CTI_Collet();
	~CTI_Collet();

//	void Image_Convert(__in IplImage *ipOrgImage, __out int** iArrDataY);
//	BOOL Matching_ColletImage(__in IplImage* ipSrcImage, __in int iColletType, __out CvPoint& MatchingPoint, __out double& dPercent_min, __out double& dPercent_max, __out int& iMatchWidth, __out int& iMatchHeight);
//	BOOL TestColletAngle(__in LPBYTE Original_image, __in UINT nImageW, __in UINT nImageH, __in ST_VisionOptInfo* pstVisionOpt, __out double& dResultDegree);

	UINT	TestColletAngle(unsigned char *Original_image, int nWidth, int nHeight, __in ST_VisionOptInfo* pstVisionOpt, __out double& dResultDegree);
	void	Image_Convert(IplImage* orgImage);
	void	matching_Image();
	
	int Data_Static[2560][2048];
	int ORIImage_W;
	int ORIImage_H;

	double Percent_min;
	double Percent_max;
	CvPoint Matching_Point;

	IplImage *m_RGBResultImage;
	IplImage *m_RGBResultImage_dst;
	IplImage* srcImage;
	IplImage* edgeImage;
	IplImage* dstImage;
	IplImage* Teaching_Image;
	IplImage* Convert_Image;

	CvMemStorage* storage;
	CvMemStorage* storage_Circle;

	bool Match_INFlag;
	bool Match_F_IN;
	int m_MODEL;
	double current_sec;
	double end_sec;

	CvRect maxAreaRect;
};
