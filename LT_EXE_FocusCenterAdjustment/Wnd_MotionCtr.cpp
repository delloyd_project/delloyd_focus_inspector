﻿// Wnd_MotionCtr.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MotionCtr.h"

// CWnd_MotionCtr

IMPLEMENT_DYNAMIC(CWnd_MotionCtr, CWnd)

CWnd_MotionCtr::CWnd_MotionCtr()
{
	m_pstDevice				= NULL;
	m_nAxisNum				= 0;
	m_bRepeatStop			= FALSE;
	m_bRepeatStop_Flag		= TRUE;
	m_bEStop_Flag			= TRUE;
	m_bSStop_Flag			= TRUE;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_MotionCtr::~CWnd_MotionCtr()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_MotionCtr, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_MC_BN_ABS_MOVE, OnBnClickedBnAbsMove)
	ON_BN_CLICKED(IDC_MC_BN_REL_MOVE, OnBnClickedBnRelMove)
	ON_BN_CLICKED(IDC_MC_BN_REPEAT_MOVE, OnBnClickedBnRepeatStart)
	ON_BN_CLICKED(IDC_MC_BN_MANUAL_CLEAR, OnBnClickedBnClear)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/28 - 17:02
// Desc.		:
//=============================================================================
int CWnd_MotionCtr::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < ST_MC_JOG_SPEED; nIdx++)
	{
		m_st_TitleName[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_TitleName[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_TitleName[nIdx].SetFont_Gdip(L"Arial", 11.5F);
		m_st_TitleName[nIdx].Create(g_szMotionCtrTitleName[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = ST_MC_JOG_SPEED; nIdx < ST_MC_MAXNUM; nIdx++)
	{
		m_st_TitleName[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_TitleName[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_TitleName[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_TitleName[nIdx].Create(g_szMotionCtrTitleName[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < ED_MC_MAXNUM; nIdx++)
	{
		DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_CHILD;
		m_ed_Item[nIdx].Create(dwEtStyle, CRect(0, 0, 0, 0), this, IDC_MC_ED_JOG_SPEED + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0"));
	}

	for (UINT nIdx = 0; nIdx < ED_MC_MAXNUM; nIdx++)
		m_ed_Item[nIdx].SetValidChars(_T("0123456789-"));

	m_ed_Item[ED_MC_JOG_SPEED].SetWindowText(_T("1"));
	m_ed_Item[ED_MC_JOG_SPEED].SetValidChars(_T("01234567"));
	m_ed_Item[ED_MC_REPEAT_DLY].SetValidChars(_T("0123456789"));
	m_ed_Item[ED_MC_REPEAT_CNT].SetValidChars(_T("0123456789"));

	for (UINT nIdx = 0; nIdx < BN_MC_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szMotionCtrButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_MC_BN_JOG_MINUS + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	m_Group_Nmae.SetTitle(L"MOTOR MANUL CONTROL");

	if (!m_Group_Nmae.Create(_T("MOTOR MANUL CONTROL"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	cx = (int)(cx * MC_WIDTH_OFFSET);
	
	int iMargin  = 11;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = 0;
	int iWidth	 = cx;
	int iHeight  = cy - iMargin;
	int iWidthTemp = iHeight * 2 / 3;
	
	int iStNameH = 28;
	int iEdNameH = 28;
	int iStSensorH = 0;
	int iHeightTemp = 0;
	int iStTitleNameH = 35;

	int iTitleW = (iWidth - (iSpacing * 9)) / 3;

	m_Group_Nmae.MoveWindow(iLeft, iTop, iWidth, iHeight);

	iTop += iSpacing * 6;
	iLeft = iMargin;
	m_st_TitleName[ST_MC_JOG_NAME].MoveWindow(iLeft, iTop, iTitleW, iStTitleNameH);

	iLeft += iTitleW + iSpacing * 2;
	m_st_TitleName[ST_MC_REPEAT_NAME].MoveWindow(iLeft, iTop, iTitleW, iStTitleNameH);

	iLeft += iTitleW + iSpacing * 2;
	m_st_TitleName[ST_MC_MANUAL_CTR].MoveWindow(iLeft, iTop, iTitleW, iStTitleNameH);
	
	iTop += iStTitleNameH + iSpacing;
	iLeft = iMargin;
	int iTitleSubW = (iTitleW - (iSpacing * 2)) / 2;

	int iTitleSubW2 = (iTitleW - (iSpacing * 3)) / 3;

	m_st_TitleName[ST_MC_JOG_SPEED].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft += iTitleSubW + iSpacing + iSpacing;
	m_ed_Item[ED_MC_JOG_SPEED].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft += iTitleSubW + (iSpacing * 2);
	m_st_TitleName[ST_MC_REPEAT_CNT].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft += iTitleSubW + iSpacing + iSpacing;
	m_ed_Item[ED_MC_REPEAT_CNT].MoveWindow(iLeft, iTop, iTitleSubW / 2, iEdNameH);

	iLeft += (iTitleSubW / 2);
	m_st_TitleName[ST_MC_REPEAT_READ_CNT].MoveWindow(iLeft, iTop, iTitleSubW / 2, iEdNameH);

	iLeft += iTitleSubW / 2 + (iSpacing * 2);
	m_bn_Item[BN_MC_MANUAL_ESTOP].MoveWindow(iLeft, iTop, iTitleW, iEdNameH);

	iLeft = iMargin;
	iTop += iEdNameH + iSpacing;
	m_bn_Item[BN_MC_MINUS_MOVE].MoveWindow(iLeft, iTop, iTitleSubW, iStNameH);

	iLeft += iTitleSubW + iSpacing + iSpacing;
	m_bn_Item[BN_MC_PLUS_MOVE].MoveWindow(iLeft, iTop, iTitleSubW + 1, iStNameH);

	iLeft += iTitleSubW + (iSpacing * 2);
	m_st_TitleName[ST_MC_REPEAT_DLY].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft += iTitleSubW + iSpacing + iSpacing;
	m_ed_Item[ED_MC_REPEAT_DLY].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

 	iLeft += iTitleSubW + (iSpacing * 2);
	m_bn_Item[BN_MC_MANUAL_SSTOP].MoveWindow(iLeft, iTop, iTitleW, iEdNameH);

	iLeft = iMargin;
	iTop += iEdNameH + iSpacing;
	m_st_TitleName[ST_MC_STEP_NAME].MoveWindow(iLeft, iTop, iTitleW, iEdNameH);

	iLeft += iTitleW + (iSpacing * 2);
	m_st_TitleName[ST_MC_REPEAT_1P].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);
	
	iLeft += iTitleSubW + iSpacing + iSpacing;
	m_ed_Item[ED_MC_REPEAT_P1].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft += iTitleSubW + (iSpacing * 2);
	m_bn_Item[BN_MC_MANUAL_CLEAR].MoveWindow(iLeft, iTop, iTitleW, iEdNameH);

	iLeft = iMargin;
	iTop += iEdNameH + iSpacing;
	m_st_TitleName[ST_MC_STEP_POS].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft += iTitleSubW + iSpacing + iSpacing;
	m_ed_Item[ED_MC_STEP_POS].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft += iTitleSubW + (iSpacing * 2);
	m_st_TitleName[ST_MC_REPEAT_2P].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft += iTitleSubW + iSpacing + iSpacing;
	m_ed_Item[ED_MC_REPEAT_P2].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft = iMargin;
	iTop += iEdNameH + iSpacing;
	m_bn_Item[BN_MC_ABS_MOVE].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft += iTitleSubW + iSpacing + iSpacing;
	m_bn_Item[BN_MC_REL_MOVE].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft += iTitleSubW + (iSpacing * 2);
	m_bn_Item[BN_MC_REPEAT_START].MoveWindow(iLeft, iTop, iTitleSubW, iEdNameH);

	iLeft += iTitleSubW + iSpacing + iSpacing;
	m_bn_Item[BN_MC_REPEAT_STOP].MoveWindow(iLeft, iTop, iTitleSubW + 1, iEdNameH);
}

//=============================================================================
// Method		: OnBnClickedBnAbsMove
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 16:27
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::OnBnClickedBnAbsMove()
{
	if (m_pstDevice == NULL)
		return;

	CString str;
	double dbPos = 0.0;

	m_ed_Item[ED_MC_STEP_POS].GetWindowText(str);
	dbPos = _ttof(str);

	m_pstDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, m_nAxisNum, dbPos);
}

//=============================================================================
// Method		: OnBnClickedBnRelMove
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 16:27
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::OnBnClickedBnRelMove()
{
	CString str;
	double dbPos = 0.0;

	m_ed_Item[ED_MC_STEP_POS].GetWindowText(str);
	dbPos = _ttof(str);

	m_pstDevice->MotionManager.MotorAxisMove(POS_REL_MODE, m_nAxisNum, dbPos);
}

//=============================================================================
// Method		: OnBnClickedBnRepeatStart
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 16:27
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::OnBnClickedBnRepeatStart()
{
	if (m_pstDevice == NULL)
		return;

	CString str;
	UINT nCount		= 0;
	UINT nDelay		= 0;
	double dbAPointPos = 0.0;
	double dbBPointPos = 0.0;
	m_bRepeatStop = FALSE;

	m_ed_Item[ED_MC_REPEAT_CNT].GetWindowText(str);
	nCount = _ttoi(str);

	m_ed_Item[ED_MC_REPEAT_DLY].GetWindowText(str);
	nDelay = _ttoi(str);

	m_ed_Item[ED_MC_REPEAT_P1].GetWindowText(str);
	dbAPointPos = _ttof(str);

	m_ed_Item[ED_MC_REPEAT_P2].GetWindowText(str);
	dbBPointPos = _ttof(str);

	m_st_TitleName[ST_MC_REPEAT_READ_CNT].SetWindowText(_T("0"));

	for (UINT nCnt = 0; nCnt < nCount; nCnt++)
	{
		// 시작 POS
		if (!m_pstDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, m_nAxisNum, dbAPointPos))
			return;

		// 대기 시간
		m_pstDevice->MotionManager.DoEvents(nDelay);

		// STOP 을 누를 경우 
		if (m_bRepeatStop)
			break;

		// 종료 POS
		if (!m_pstDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, m_nAxisNum, dbBPointPos))
			return;

		// 대기 시간
		m_pstDevice->MotionManager.DoEvents(nDelay);

		str.Format(_T("%d"), nCnt + 1);
		m_st_TitleName[ST_MC_REPEAT_READ_CNT].SetWindowText(str);

		// STOP 을 누를 경우 
		if (m_bRepeatStop)
			break;
	}
}

//=============================================================================
// Method		: OnBnClickedBnClear
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/31 - 15:53
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::OnBnClickedBnClear()
{
	if (m_pstDevice == NULL)
		return;

	m_bRepeatStop = TRUE;
	m_pstDevice->MotionManager.SetMotorPosClear(m_nAxisNum);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
BOOL CWnd_MotionCtr::PreTranslateMessage(MSG* pMsg)
{
	CString str;
	UINT nSpeed = 0;

	m_ed_Item[ED_MC_JOG_SPEED].GetWindowText(str);
	nSpeed = _ttoi(str);

	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if ((::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 86)) // Ctrl + V
		{
			CWnd* pWnd = CWnd::FromHandle(pMsg->hwnd);
			pWnd->SetWindowText(_T(""));
			pMsg->message = WM_PASTE;
		}

		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 67)  // Ctrl + C
			pMsg->message = WM_COPY; 

// 		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 90)  // Ctrl + Z
// 			pMsg->message = WM_UNDO;

		break;

	case WM_LBUTTONDOWN:
		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_JOG_MINUS)->GetSafeHwnd())
			m_pstDevice->MotionManager.MotorJogMove(m_nAxisNum, 0, nSpeed);

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_JOG_PLUS)->GetSafeHwnd())
			m_pstDevice->MotionManager.MotorJogMove(m_nAxisNum, 1, nSpeed);

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_REPEAT_STOP)->GetSafeHwnd())
		{
			if (m_bRepeatStop_Flag == TRUE)
			{
				m_bRepeatStop_Flag = FALSE;
				m_bRepeatStop = TRUE;
			}
		}

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_MANUAL_ESTOP)->GetSafeHwnd())
		{
			if (m_bEStop_Flag == TRUE)
			{
				m_bEStop_Flag = FALSE;

				if (m_pstDevice != NULL)
				{
					m_bRepeatStop = TRUE;
					m_pstDevice->MotionManager.SetMotorEStop(m_nAxisNum);
				}
			}
		}

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_MANUAL_SSTOP)->GetSafeHwnd())
		{
			if (m_bSStop_Flag == TRUE)
			{
				m_bSStop_Flag = FALSE;

				if (m_pstDevice != NULL)
				{
					m_bRepeatStop = TRUE;
					m_pstDevice->MotionManager.SetMotorSStop(m_nAxisNum);
				}
			}
		}

		break;

	case WM_LBUTTONUP:
		if ((pMsg->hwnd == GetDlgItem(IDC_MC_BN_JOG_MINUS)->GetSafeHwnd()) || (pMsg->hwnd == GetDlgItem(IDC_MC_BN_JOG_PLUS)->GetSafeHwnd()))
			m_pstDevice->MotionManager.SetMotorSStop(m_nAxisNum);

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_REPEAT_STOP)->GetSafeHwnd())
			m_bRepeatStop_Flag = TRUE;

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_MANUAL_ESTOP)->GetSafeHwnd())
			m_bEStop_Flag = TRUE;

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_MANUAL_SSTOP)->GetSafeHwnd())
			m_bSStop_Flag = TRUE;

		break;
	}

	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_MotionCtr::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetSelectAxis
// Access		: public  
// Returns		: void
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/3 - 11:49
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::SetSelectAxis(UINT nAxis)
{
 	m_nAxisNum = nAxis;
}
