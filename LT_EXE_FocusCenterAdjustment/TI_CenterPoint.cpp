﻿#include "stdafx.h"
#include "TI_CenterPoint.h"


CTI_CenterPoint::CTI_CenterPoint()
{
	m_nWidth = 720;
	m_nHeight = 480;
}

CTI_CenterPoint::~CTI_CenterPoint()
{
}

BOOL CTI_CenterPoint::SetImageSize(DWORD dwWidth, DWORD dwHeight)
{
	if (dwWidth <= 0 || dwHeight <= 0)
		return FALSE;

	m_nWidth = dwWidth;
	m_nHeight = dwHeight;

	return TRUE;
}

UINT CTI_CenterPoint::CenterPoint_Test(ST_LT_TI_CenterPoint *pstCenterPoint, LPBYTE pImageBuf, UINT nQuadRectPos)
{
	auto SearchPoint = SearchCenterPoint(&pstCenterPoint->stCenterPointOp, pImageBuf);

	Sleep(1);

	UINT nCamState = pstCenterPoint->stCenterPointOp.nCameraState;

	if (SearchPoint.x != (-1) * m_nWidth || SearchPoint.y != (-1) * m_nHeight) // 측정이 된경우
	{
		auto CenterPoint = Cam_State_Offset(nCamState, SearchPoint);

		pstCenterPoint->stCenterPointData.iResult_RealPos_X = SearchPoint.x;
		pstCenterPoint->stCenterPointData.iResult_RealPos_Y = SearchPoint.y;
		pstCenterPoint->stCenterPointData.iResult_Pos_X = CenterPoint.x;
		pstCenterPoint->stCenterPointData.iResult_Pos_Y = CenterPoint.y;

		pstCenterPoint->stCenterPointData.iResult_Offset_X = CenterPoint.x - pstCenterPoint->stCenterPointOp.nStandard_X;
		pstCenterPoint->stCenterPointData.iResult_Offset_Y = CenterPoint.y - pstCenterPoint->stCenterPointOp.nStandard_Y;
	}
	else // 측정이 안된경우
	{
		pstCenterPoint->stCenterPointData.iResult_RealPos_X = -99999;
		pstCenterPoint->stCenterPointData.iResult_RealPos_Y = -99999;
		pstCenterPoint->stCenterPointData.iResult_Pos_X = -99999;
		pstCenterPoint->stCenterPointData.iResult_Pos_Y = -99999;

		pstCenterPoint->stCenterPointData.iResult_Offset_X = -99999;
		pstCenterPoint->stCenterPointData.iResult_Offset_Y = -99999;
	}

	UINT GapX = abs(pstCenterPoint->stCenterPointData.iResult_Offset_X);
	UINT GapY = abs(pstCenterPoint->stCenterPointData.iResult_Offset_Y);

	if (pstCenterPoint->stCenterPointOp.nStandard_OffsetX >= GapX &&
		pstCenterPoint->stCenterPointOp.nStandard_OffsetY >= GapY)
	{
		pstCenterPoint->stCenterPointData.nResult = TER_Pass;
	}
	else
	{
		pstCenterPoint->stCenterPointData.nResult = TER_Fail;
	}

	if (TER_Pass == pstCenterPoint->stCenterPointData.nResult)
	{
		// 역삽 방지
		auto QuadRectPoint = SearchQuadRectPoint(&pstCenterPoint->stCenterPointOp, pImageBuf);

		pstCenterPoint->stCenterPointData.iQuadRect_Pos_X = QuadRectPoint.x;
		pstCenterPoint->stCenterPointData.iQuadRect_Pos_Y = QuadRectPoint.y;

		pstCenterPoint->stCenterPointData.bQuadRectUse		= TRUE;
		pstCenterPoint->stCenterPointData.bQuadRectResult	= FALSE;

		switch (nQuadRectPos)
		{
		case QRP_NOTUSE:
			pstCenterPoint->stCenterPointData.bQuadRectUse = FALSE;
			pstCenterPoint->stCenterPointData.bQuadRectResult = TRUE;
			break;

		case QRP_LT:
			if (QuadRectPoint.x < SearchPoint.x && QuadRectPoint.y < SearchPoint.y)
				pstCenterPoint->stCenterPointData.bQuadRectResult = TRUE;
			break;

		case QRP_LB:
			if (QuadRectPoint.x < SearchPoint.x && QuadRectPoint.y > SearchPoint.y)
				pstCenterPoint->stCenterPointData.bQuadRectResult = TRUE;
			break;

		case QRP_RT:
			if (QuadRectPoint.x > SearchPoint.x && QuadRectPoint.y < SearchPoint.y)
				pstCenterPoint->stCenterPointData.bQuadRectResult = TRUE;
			break;

		case QRP_RB:
			if (QuadRectPoint.x > SearchPoint.x && QuadRectPoint.y > SearchPoint.y)
				pstCenterPoint->stCenterPointData.bQuadRectResult = TRUE;
			break;

		default:
			break;
		}

		if (FALSE == pstCenterPoint->stCenterPointData.bQuadRectResult)
			return TER_CamStateFail;
	}

	return pstCenterPoint->stCenterPointData.nResult;
}

CvPoint CTI_CenterPoint::SearchCenterPoint(__in const ST_CenterPoint_Op  *pstCenterPointOp, LPBYTE IN_RGB)
{
	CvPoint resultPt = cvPoint((-1) * m_nWidth, (-1) * m_nHeight);

	IplImage *OriginImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *CannyImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage	= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBOrgImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);

	BYTE R, G, B;
	//double Sum_Y;

	for (UINT y = 0; y < m_nHeight; y++)
	{
		for (UINT x = 0; x < m_nWidth; x++)
		{
			B = IN_RGB[y*(m_nWidth * 3) + x * 3];
			G = IN_RGB[y*(m_nWidth * 3) + x * 3 + 1];
			R = IN_RGB[y*(m_nWidth * 3) + x * 3 + 2];

			if (R < 100 && G < 100 && B < 100)
				OriginImage->imageData[y*OriginImage->widthStep + x] = (char)255;
			else
				OriginImage->imageData[y*OriginImage->widthStep + x] = 0;

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}

	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvCopyImage(OriginImage, SmoothImage);

	cvCanny(SmoothImage, CannyImage, 0, 255);

	cvCopyImage(CannyImage, temp_PatternImage);

	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;

	int counter = 0;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
		counter++;

	if (counter == 0)
	{
		cvReleaseMemStorage(&contour_storage);
		cvReleaseImage(&OriginImage);
		cvReleaseImage(&PreprecessedImage);
		cvReleaseImage(&CannyImage);
		cvReleaseImage(&DilateImage);
		cvReleaseImage(&SmoothImage);
		cvReleaseImage(&RGBResultImage);
		cvReleaseImage(&temp_PatternImage);
		cvReleaseImage(&RGBOrgImage);

		return resultPt;
	}


	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area = 0, arcCount = 0;
	counter = 0;
	double old_dist = 999999;

	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size = 0, old_size = 0;

	CvRect rtFinal;
	rtFinal.x = 0;
	rtFinal.y = 0;
	rtFinal.height = 0;
	rtFinal.width = 0;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		double circularity = (4.0*3.14*area) / (arcCount*arcCount);
		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 255), 1, 8);
		// 값이 작을 수록 찌그러진 것도 잡는다.
		if (circularity > 0.8)
		{
			cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 0, 255), 1, 8);
			rect = cvContourBoundingRect(contour, 1);

			int center_x, center_y;
			center_x = rect.x + rect.width / 2;
			center_y = rect.y + rect.height / 2;

			if (center_x > (m_nWidth*0.2) && center_x < (m_nWidth *0.8) && center_y >(m_nHeight*0.2) && center_y < (m_nHeight*0.8))
			{
				if (rect.width < m_nWidth / 2 && rect.height < m_nHeight / 2 && rect.width > 20 && rect.height > 20)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
					double distance = GetDistance(rect.x + rect.width / 2, rect.y + rect.height / 2, pstCenterPointOp->nStandard_X, pstCenterPointOp->nStandard_Y);

					obj_Cnt++;

					size = rect.width * rect.height;

					if (distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;
						old_dist = distance;
						rtFinal = rect;
					}
					//cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 0), 1, 8);
				}
			}
		}

		counter++;
	}
	cvRectangle(RGBResultImage, cvPoint(rtFinal.x, rtFinal.y), cvPoint(rtFinal.x + rtFinal.width, rtFinal.y + rtFinal.height), CV_RGB(255, 0, 0), 1, 8);

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);

	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);
	cvReleaseImage(&PreprecessedImage);
	cvReleaseImage(&CannyImage);

	delete[]rectArray;
	delete[]areaArray;


	return resultPt;
}

CvPoint CTI_CenterPoint::SearchQuadRectPoint(__in const ST_CenterPoint_Op *pstCenterPointOp, LPBYTE IN_RGB)
{
	CvPoint resultPt = cvPoint((-1) * m_nWidth, (-1) * m_nHeight);

	IplImage *OriginImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *PreprecessedImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *CannyImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *SmoothImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage	= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);
	IplImage *temp_PatternImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBOrgImage		= cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);

	BYTE R, G, B;
	//double Sum_Y;

	for (int y = 0; y < m_nHeight; y++)
	{
		for (int x = 0; x < m_nWidth; x++)
		{
			B = IN_RGB[y*(m_nWidth * 3) + x * 3];
			G = IN_RGB[y*(m_nWidth * 3) + x * 3 + 1];
			R = IN_RGB[y*(m_nWidth * 3) + x * 3 + 2];

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}

	// Gray 영상으로 변환
	cvCvtColor(RGBOrgImage, OriginImage, CV_BGR2GRAY);
	cvThreshold(OriginImage, OriginImage, 0, 255, CV_THRESH_OTSU);

	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvDilate(OriginImage, OriginImage);
	cvErode(OriginImage, OriginImage);
	cvCopyImage(OriginImage, SmoothImage);

	cvSmooth(SmoothImage, SmoothImage, CV_MEDIAN, 5, 5);
	cvDilate(SmoothImage, SmoothImage);
	cvErode(SmoothImage, SmoothImage);
	cvCanny(SmoothImage, CannyImage, 0, 255);

	cvCopyImage(CannyImage, temp_PatternImage);

	cvCvtColor(CannyImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(CannyImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;

	int counter = 0;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
		counter++;

	if (counter == 0)
	{
		cvReleaseMemStorage(&contour_storage);
		cvReleaseImage(&OriginImage);
		cvReleaseImage(&PreprecessedImage);
		cvReleaseImage(&CannyImage);
		cvReleaseImage(&DilateImage);
		cvReleaseImage(&SmoothImage);
		cvReleaseImage(&RGBResultImage);
		cvReleaseImage(&temp_PatternImage);
		cvReleaseImage(&RGBOrgImage);

		return resultPt;
	}

	CvRect *rectArray = new CvRect[counter];
	double *areaArray = new double[counter];
	CvRect rect;
	double area = 0, arcCount = 0;
	counter = 0;
	double old_dist = 999999;

	int old_center_pos_x = 0;
	int old_center_pos_y = 0;
	int center_pt_x = 0;
	int center_pt_y = 0;
	int obj_Cnt = 0;
	int size = 0, old_size = 0;

	CvRect rtFinal;
	rtFinal.x = 0;
	rtFinal.y = 0;
	rtFinal.height = 0;
	rtFinal.width = 0;

	int nCenterX = 0;
	int nCenterY = 0;
	double circularity = 0;
	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		rectArray[counter] = rect;
		areaArray[counter] = area;

		nCenterX = rect.x + rect.width/2;
		nCenterY = rect.y + rect.height / 2;

		B = RGBOrgImage->imageData[nCenterY*RGBOrgImage->widthStep + 3 * nCenterX + 0];
		G = RGBOrgImage->imageData[nCenterY*RGBOrgImage->widthStep + 3 * nCenterX + 1];
		R = RGBOrgImage->imageData[nCenterY*RGBOrgImage->widthStep + 3 * nCenterX + 2];

		if (abs(R - G) > 30 && G > 60 && abs(B - G) > 30) // 영역에 중앙이 초록색이 경우에만 
		{
			circularity = (4.0*3.14*area) / (arcCount*arcCount);
		//	cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 255), 1, 8);

			// 값이 작을 수록 찌그러진 것도 잡는다.
			if (circularity > 0.6 && circularity < 0.9)
			{
				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 0, 255), 1, 8);

				int center_x, center_y;
				center_x = rect.x + rect.width / 2;
				center_y = rect.y + rect.height / 2;

				if (rect.width < m_nWidth / 2 && rect.height < m_nHeight / 2 && rect.width > 20 && rect.height > 20)
				{
					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 255, 0), 1, 8);
					double distance = GetDistance(rect.x + rect.width / 2, rect.y + rect.height / 2, pstCenterPointOp->nStandard_X, pstCenterPointOp->nStandard_Y);

					obj_Cnt++;

					size = rect.width * rect.height;

					if (distance < old_dist)
					{
						old_size = size;
						resultPt.x = center_x;
						resultPt.y = center_y;
						old_dist = distance;
						rtFinal = rect;
					}
				}
			}
		}

		counter++;
	}

	cvRectangle(RGBResultImage, cvPoint(rtFinal.x, rtFinal.y), cvPoint(rtFinal.x + rtFinal.width, rtFinal.y + rtFinal.height), CV_RGB(255, 0, 0), 1, 8);

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&SmoothImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&temp_PatternImage);
	cvReleaseImage(&RGBOrgImage);
	cvReleaseImage(&PreprecessedImage);
	cvReleaseImage(&CannyImage);

	delete[]rectArray;
	delete[]areaArray;

	return resultPt;
}

//=============================================================================
// Method		: GetDistance
// Access		: public  
// Returns		: double
// Parameter	: int ix1
// Parameter	: int iy1
// Parameter	: int ix2
// Parameter	: int iy2
// Qualifier	:
// Last Update	: 2017/1/12 - 17:23
// Desc.		:
//=============================================================================
double CTI_CenterPoint::GetDistance(int ix1, int iy1, int ix2, int iy2)
{
	double result;

	result = sqrt((double)((ix2 - ix1)*(ix2 - ix1)) + ((iy2 - iy1)*(iy2 - iy1)));

	return result;
}

//=============================================================================
// Method		: Cam_State_Offset
// Access		: public  
// Returns		: CvPoint
// Parameter	: int CamState
// Parameter	: CvPoint CenterPoint
// Qualifier	:
// Last Update	: 2017/1/12 - 17:20
// Desc.		:
//=============================================================================
CvPoint CTI_CenterPoint::Cam_State_Offset(int iCamState, CvPoint CenterPoint)
{
	CvPoint buf_Point;

	buf_Point.x = -99999;
	buf_Point.y = -99999;

	if (iCamState == CAM_STATE_ORIGINAL)
	{
		buf_Point.x = CenterPoint.x;
		buf_Point.y = CenterPoint.y;
	}
	else if (iCamState == CAM_STATE_MIRROR)
	{
		buf_Point.x = m_nWidth - CenterPoint.x;
		buf_Point.y = CenterPoint.y;
	}
	else if (iCamState == CAM_STATE_FLIP)
	{
		buf_Point.x = CenterPoint.x;
		buf_Point.y = m_nHeight - CenterPoint.y;
	}
	else if (iCamState == CAM_STATE_ROTATE)
	{
		buf_Point.x = m_nWidth - CenterPoint.x;
		buf_Point.y = m_nHeight - CenterPoint.y;
	}

	return buf_Point;
}
