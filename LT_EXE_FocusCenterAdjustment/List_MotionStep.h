﻿#ifndef List_MotionStep_h__
#define List_MotionStep_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_MotionStep
{
	MotionStep_Use,
	MotionStep_Axis,
	MotionStep_Position,
	MotionStep_Vel,
	MotionStep_Acc,
	MotionStep_Dec,
	MotionStep_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_MotionStep[] =
{
	_T(""),
	_T("Axis"),
	_T("Pos"),
	_T("Vel"),
	_T("Acc"),
	_T("Dec"),

	NULL
};

typedef enum enListItemNum_MotionStep
{
	MotionStep_Axis_1,
	MotionStep_Axis_2,
	MotionStep_Axis_3,
	MotionStep_Axis_4,

	MotionStep_ItemNum,
};

static const TCHAR*	g_lpszItem_MotionStep[] =
{
	_T("AXIS Z"),
	_T("AXIS X"),
	_T("AXIS Y"),
	_T("AXIS R"),

	NULL
};

const int	iListAglin_MotionStep[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_MotionStep[] =
{
	35,
	95,
	95,
	95,
	95,
	95,
};
// List_MotionStep

class CList_MotionStep : public CListCtrl
{
	DECLARE_DYNAMIC(CList_MotionStep)

public:
	CList_MotionStep();
	virtual ~CList_MotionStep();

	void InitHeader();
	void InsertFullData();
	void GetFullData();
	
	void SetRectRow(UINT nRow);
	void GetRectRow(UINT nRow);

	void GetCellData();

	void SetPtr_MotionStep(ST_SeqStepTeach *pstSeqStep)
	{
		if (pstSeqStep == NULL)
			return;

		m_pstSeqStep = pstSeqStep;
	};

protected:

	ST_SeqStepTeach*	m_pstSeqStep;

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;

	BOOL		UpdateCellData(UINT nRow, UINT nCol, int  iValue);
	BOOL		UpdateCellData_double(UINT nRow, UINT nCol, double dValue);
	BOOL		CheckRectValue(__in const CRect* pRegionz);

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnEnKillFocusECurrOpellEdit();

	afx_msg BOOL	OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()
};

#endif // List_MotionStep_h__
