#pragma once
#include "VGStatic.h"
#include "resource.h"
#include "Def_TestDevice.h"

#include "VGGroupWnd.h"
#include "List_MotionStep.h"

// CWnd_MotionTeach

typedef enum MotionTeach_ID
{
	IDC_MS_CB_SEQUENCE_SELECT = 1000,
	IDC_MS_CB_STEP_SELECT,

	IDC_MS_ED_SEQUENCE_NAME = 2000,
	IDC_MS_ED_STEP_COUNT,

	IDC_MS_LC_STEP_TABLE = 3000,

	IDC_MS_BN_POSITION_SET = 4000,
	IDC_MS_BN_SEQUENCE_SAVE,
	IDC_MS_BN_STEP_TEST,
	IDC_MS_BN_SEQUENCE_TEST,
	IDC_MS_BN_MOTION_STOP,
};

enum enMotionTeachStatic
{
	ST_MS_SEQUENCE_SELECT = 0,
	ST_MS_SEQUENCE_NAME,
	ST_MS_STEP_COUNT,
	ST_MS_STEP_SELECT,
	ST_MS_MOTION_TEST,

	ST_MS_MAXNUM,
};

enum enMotionTeachComboBox
{
	CB_MS_SEQUENCE_SELECT = 0,
	CB_MS_STEP_SELECT,
	
	CB_MS_MAXNUM,
};

enum enMotionTeachEdit
{
	ED_MS_SEQUENCE_NAME = 0,
	ED_MS_STEP_COUNT,
	
	ED_MS_MAXNUM,
};

enum enMotionTeachListCtrl
{
	LC_MS_STEP_TABLE = 0,

	LC_MS_MAXNUM,
};

enum enMotionTeachButton
{
	BN_MS_POSITION_SET = 0,
	BN_MS_SEQUENCE_SAVE,
	BN_MS_STEP_TEST,
	BN_MS_SEQUENCE_TEST,
	BN_MS_MOTION_STOP,

	BN_MS_MAXNUM,
};

static LPCTSTR g_szMotionTeachStatic[] =
{
	_T("SEQUENCE SELECT"),
	_T("SEQUENCE NAME"),
	_T("STEP COUNT"),
	_T("STEP SELECT"),
	_T("MOTION TEST"),

	NULL
};


static LPCTSTR g_szMotionTeachButton[] =
{
	_T("POSITION SET"),
	_T("SAVE"),
	_T("STEP TEST"),
	_T("SEQUENCE TEST"),
	_T("MOTION STOP"),

	NULL
};


static LPCTSTR g_szSequenceSelect[] =
{
	_T(" Sequence Number : 1"),
	_T(" Sequence Number : 2"),
	_T(" Sequence Number : 3"),
	_T(" Sequence Number : 4"),
	_T(" Sequence Number : 5"),
	_T(" Sequence Number : 6"),
	_T(" Sequence Number : 7"),
	_T(" Sequence Number : 8"),
	_T(" Sequence Number : 9"),
	_T(" Sequence Number : 10"),

	NULL
};

static LPCTSTR g_szStepSelect[] =
{
	_T(" Motion Step : 1"),
	_T(" Motion Step : 2"),
	_T(" Motion Step : 3"),
	_T(" Motion Step : 4"),
	_T(" Motion Step : 5"),
	_T(" Motion Step : 6"),
	_T(" Motion Step : 7"),
	_T(" Motion Step : 8"),
	_T(" Motion Step : 9"),
	_T(" Motion Step : 10"),

	NULL
};

class CWnd_MotionTeach : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MotionTeach)

public:
	CWnd_MotionTeach();
	virtual ~CWnd_MotionTeach();

	ST_Device*			m_pstDevice;
	ST_MotionSeqTeach*	m_pstMotionSeq;
	ST_SeqStepTeach*	m_pstSeqStep;

	void SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	void SetUpdataData();
	void SetUpdatePosition();

protected:
	CFont				m_font;
	CVGGroupWnd			m_Group_Name;
	CVGStatic			m_st_Item[ST_MS_MAXNUM];
	CComboBox			m_cb_Item[CB_MS_MAXNUM];
	CMFCMaskedEdit		m_ed_Item[ED_MS_MAXNUM];
	CList_MotionStep	m_lc_Item[LC_MS_MAXNUM];
	CMFCButton			m_bn_Item[BN_MS_MAXNUM];

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage(MSG* pMsg);
	afx_msg void	OnEnSelectSequenceCombo();
	afx_msg void	OnEnSelectStepCombo();
	afx_msg void	OnBnClickedBnSequenceSave();
	afx_msg void	OnBnClickedBnPositionSet();
	afx_msg void	OnBnClickedBnStepTest();
	afx_msg void	OnBnClickedBnSequenceTest();
	afx_msg void	OnBnClickedBnMotionStop();

	void GetUpdataData();

	DECLARE_MESSAGE_MAP()
	
};


