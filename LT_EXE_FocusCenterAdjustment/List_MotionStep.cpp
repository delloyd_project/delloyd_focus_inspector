﻿// List_MotionStep.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_MotionStep.h"

#define ICurrOp_ED_CELLEDIT		5001

// CList_MotionStep

IMPLEMENT_DYNAMIC(CList_MotionStep, CListCtrl)

CList_MotionStep::CList_MotionStep()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	m_pstSeqStep = NULL;
}

CList_MotionStep::~CList_MotionStep()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_MotionStep, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_MotionStep::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_MotionStep::OnNMDblclk)
	ON_EN_KILLFOCUS(ICurrOp_ED_CELLEDIT, &CList_MotionStep::OnEnKillFocusECurrOpellEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_MotionStep 메시지 처리기입니다.
int CList_MotionStep::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER );

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER/* | ES_NUMBER*/, CRect(0, 0, 0, 0), this, ICurrOp_ED_CELLEDIT);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:36
// Desc.		:
//=============================================================================
void CList_MotionStep::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iUnitWidth	= 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = MotionStep_Axis; nCol < MotionStep_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() - iHeaderWidth_MotionStep[MotionStep_Use]) / (MotionStep_MaxCol - MotionStep_Axis);
		SetColumnWidth(nCol, iUnitWidth);
	}

}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:55
// Desc.		:
//=============================================================================
BOOL CList_MotionStep::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | LVS_EX_CHECKBOXES | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:56
// Desc.		:
//=============================================================================
void CList_MotionStep::InitHeader()
{
	for (UINT nCol = 0; nCol < MotionStep_MaxCol; nCol++)
		InsertColumn(nCol, g_lpszHeader_MotionStep[nCol], iListAglin_MotionStep[nCol], iHeaderWidth_MotionStep[nCol]);

	for (UINT nCol = 0; nCol < MotionStep_MaxCol; nCol++)
		SetColumnWidth(nCol, iHeaderWidth_MotionStep[nCol]);
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:56
// Desc.		:
//=============================================================================
void CList_MotionStep::InsertFullData()
{
	if (m_pstSeqStep == NULL)
		return;

	DeleteAllItems();

	for (UINT nIndx = 0; nIndx < MotionStep_ItemNum; nIndx++)
	{
		InsertItem(nIndx, _T(""));
		SetRectRow(nIndx);
	}

}

//=============================================================================
// Method		: GetFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/30 - 19:26
// Desc.		:
//=============================================================================
void CList_MotionStep::GetFullData()
{
	if (m_pstSeqStep == NULL)
		return;

	for (UINT nIndx = 0; nIndx < MotionStep_ItemNum; nIndx++)
	{
		GetRectRow(nIndx);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_MotionStep::SetRectRow(UINT nRow)
{
	CString strText;

	strText.Format(_T("%d"), nRow + 1);
	SetItemText(nRow, MotionStep_Use, strText);

	strText.Format(_T("%s"), g_lpszItem_MotionStep[nRow]);
	SetItemText(nRow, MotionStep_Axis, strText);

	ListView_SetCheckState(this->m_hWnd, nRow, m_pstSeqStep->stAxisParam[nRow].bAxisUse);

	//SetItemText(nRow, MotionStep_Use, strText);

	strText.Format(_T("%.0f"), m_pstSeqStep->stAxisParam[nRow].dbPos);
	SetItemText(nRow, MotionStep_Position, strText);

	strText.Format(_T("%.0f"), m_pstSeqStep->stAxisParam[nRow].dbVel);
	SetItemText(nRow, MotionStep_Vel, strText);

	strText.Format(_T("%.0f"), m_pstSeqStep->stAxisParam[nRow].dbAcc);
	SetItemText(nRow, MotionStep_Acc, strText);

	strText.Format(_T("%.0f"), m_pstSeqStep->stAxisParam[nRow].dbDec);
	SetItemText(nRow, MotionStep_Dec, strText);
 }

//=============================================================================
// Method		: GetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/9/30 - 19:27
// Desc.		:
//=============================================================================
void CList_MotionStep::GetRectRow(UINT nRow)
{
	CString strText;

	m_pstSeqStep->stAxisParam[nRow].bAxisUse = ListView_GetCheckState(this->m_hWnd, nRow);

	strText = GetItemText(nRow, MotionStep_Position);
	m_pstSeqStep->stAxisParam[nRow].dbPos = _ttof(strText);

	strText = GetItemText(nRow, MotionStep_Vel);
	m_pstSeqStep->stAxisParam[nRow].dbVel = _ttof(strText);

	strText = GetItemText(nRow, MotionStep_Acc);
	m_pstSeqStep->stAxisParam[nRow].dbAcc = _ttof(strText);

	strText = GetItemText(nRow, MotionStep_Dec);
	m_pstSeqStep->stAxisParam[nRow].dbDec = _ttof(strText);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_MotionStep::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_MotionStep::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < MotionStep_MaxCol && pNMItemActivate->iSubItem > MotionStep_Axis)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusECurrOpellEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_MotionStep::OnEnKillFocusECurrOpellEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (0 != strText.Compare(GetItemText(m_nEditRow, m_nEditCol)))
	{
// 		if (m_nEditCol == MotionStep_Position 
// 			|| m_nEditCol == MotionStep_Vel
// 			|| m_nEditCol == MotionStep_Acc
// 			|| m_nEditCol == MotionStep_Dec)
// 		{
			if (UpdateCellData_double(m_nEditRow, m_nEditCol, _ttof(strText)))
			{
				
			}
// 		}
// 		else
// 		{
// 			if (UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText)))
// 			{
// 
// 			}
// 		}

	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
BOOL CList_MotionStep::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
//	switch (nCol)
//	{
// 		case CurrOp_Min:
// 			m_pstCurrent->stMotionStep[nRow].nMinCurrent = iValue;
// 			break;
// 
// 		case CurrOp_Max:
// 			if (m_pstCurrent->stMotionStep[nRow].nMinCurrent >= iValue)
// 				iValue = m_pstCurrent->stMotionStep[nRow].nMinCurrent + 1;
// 			
// 			m_pstCurrent->stMotionStep[nRow].nMaxCurrent = iValue;
// 			break;

//		default:
//			break;
// }
 
	CString str;
	str.Format(_T("%d"), iValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);
 
	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
BOOL CList_MotionStep::UpdateCellData_double(UINT nRow, UINT nCol, double dValue)
{
	switch (nCol)
	{
	case MotionStep_Position:
		m_pstSeqStep->stAxisParam[nRow].dbPos = dValue;
		break;

	case MotionStep_Vel:
		m_pstSeqStep->stAxisParam[nRow].dbVel = dValue;
		break;

	case MotionStep_Acc:
		m_pstSeqStep->stAxisParam[nRow].dbAcc = dValue;
		break;

	case MotionStep_Dec:
		m_pstSeqStep->stAxisParam[nRow].dbDec = dValue;
		break;
	
	default:
		break;
	}

	CString str;
	str.Format(_T("%.0f"), dValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: CheckRectValue
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in const CRect * pRegionz
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
BOOL CList_MotionStep::CheckRectValue(__in const CRect* pRegionz)
{
	if (NULL == pRegionz)
		return FALSE;

	if (pRegionz->left < 0)
		return FALSE;

	if (pRegionz->right < 0)
		return FALSE;

	if (pRegionz->top < 0)
		return FALSE;

	if (pRegionz->bottom < 0)
		return FALSE;

	if (pRegionz->Width() < 0)
		return FALSE;

	if (pRegionz->Height() < 0)
		return FALSE;


	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Parameter	: ST_LT_TI_Current & stCurrent
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
void CList_MotionStep::GetCellData()
{
	if (m_pstSeqStep == NULL)
		return;


}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
BOOL CList_MotionStep::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);
		double dValue = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dValue = dValue + ((zDelta / 120));
		}
		else
		{
			if (0 < iValue)
			{
				iValue = iValue + ((zDelta / 120));
				

			}

			if (0 < dValue)
			{
				dValue = dValue + ((zDelta / 120));
			}
		}

// 		if (m_nEditCol == CurrOp_Offset || m_nEditCol == CurrOp_Voltage)
// 		{
			if (UpdateCellData_double(m_nEditRow, m_nEditCol, dValue))
			{
				strText.Format(_T("%0.0f"), dValue);
				m_ed_CellEdit.SetWindowText(strText);
			}
// 		}
// 		else
// 		{
// 			if (UpdateCellData(m_nEditRow, m_nEditCol, iValue))
// 			{
// 				strText.Format(_T("%d"), iValue);
// 				m_ed_CellEdit.SetWindowText(strText);
// 			}
// 		}
	}
	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);

}
