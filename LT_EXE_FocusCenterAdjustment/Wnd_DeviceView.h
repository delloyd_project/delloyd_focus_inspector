﻿//*****************************************************************************
// Filename	: Wnd_DeviceView.h
// Created	: 2016/05/29
// Modified	: 2016/09/22
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_DeviceView_h__
#define Wnd_DeviceView_h__

#pragma once

#include "Def_TestDevice.h"
#include "VGStatic.h"
#include "Wnd_BaseView.h"
#include "Wnd_IOTable.h"

//=============================================================================
// CWnd_DeviceView
//=============================================================================
class CWnd_DeviceView : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_DeviceView)

public:
	CWnd_DeviceView();
	virtual ~CWnd_DeviceView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	
	CWnd_IOTable	m_Wnd_IOTable;
	ST_Device*		m_pDevice;

public:
	
	void	SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pDevice = pstDevice;
		m_Wnd_IOTable.SetPtr_Device(m_pDevice);
	};

	void	SetDeleteTimer	();

};

#endif // Wnd_DeviceView_h__


