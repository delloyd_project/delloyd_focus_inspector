//*****************************************************************************
// Filename	: 	Wnd_Cfg_DriverCount.cpp
// Created	:	2016/10/31 - 21:02
// Modified	:	2016/10/31 - 21:02
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************

#include "stdafx.h"
#include "Wnd_Cfg_DriverCount.h"
#include "Reg_InspInfo.h"

#define		IDC_CB_FILE				1001
#define		IDC_BN_SAVE				1002
#define		IDC_ED_CNT_MAX			1100
#define		IDC_ED_COUNT			1200
#define		IDC_BN_RESET_CNT		1300
#define		IDC_BN_RESET_CNT_E		1300 + USE_CHANNEL_CNT

#define		IDC_BN_APPLY			1404
#define		IDC_BN_CANCEL			1405

#define		WM_REFRESH_DriverCount			WM_USER + 201

//-----------------------------------------------------------------------------
// CWnd_Cfg_DriverCount
//-----------------------------------------------------------------------------
IMPLEMENT_DYNAMIC(CWnd_Cfg_DriverCount, CWnd_BaseView)

CWnd_Cfg_DriverCount::CWnd_Cfg_DriverCount()
{
	VERIFY(m_font_Default.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_pstDriverCount = NULL;
}

CWnd_Cfg_DriverCount::~CWnd_Cfg_DriverCount()
{
	m_font_Default.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_DriverCount, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_CBN_SELENDOK(IDC_CB_FILE, OnCbnSelendokFile)
	ON_BN_CLICKED(IDC_BN_SAVE, OnBnClickedBnSave)
	ON_COMMAND_RANGE(IDC_BN_RESET_CNT, IDC_BN_RESET_CNT_E, OnBnClickedReset)
	ON_MESSAGE(WM_REFRESH_DriverCount, OnRefreshFileList)
END_MESSAGE_MAP()


// CWnd_Cfg_DriverCount message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
int CWnd_Cfg_DriverCount::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_File.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_File.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_File.SetFont_Gdip(L"Arial", 9.0F);

	m_st_File.Create(_T("DriverCount File"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_cb_File.Create(dwStyle | CBS_DROPDOWNLIST | CBS_SORT, rectDummy, this, IDC_CB_FILE);
	m_bn_NewFile.Create(_T("Add New File"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE);


	CString szText;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_stCount_Max[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_stCount_Max[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_stCount_Max[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_stCount[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_stCount[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_stCount[nIdx].SetFont_Gdip(L"Arial", 9.0F);

		szText.Format(_T(" Socket %d: Max Count"), nIdx + 1);
		m_stCount_Max[nIdx].Create(szText, dwStyle, rectDummy, this, IDC_STATIC);
		szText.Format(_T(" Socket %d: Count"), nIdx + 1);
		m_stCount[nIdx].Create(szText, dwStyle, rectDummy, this, IDC_STATIC);

		m_ed_Count_Max[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER | ES_NUMBER, rectDummy, this, IDC_ED_CNT_MAX + nIdx);
		m_ed_Count[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER | ES_NUMBER, rectDummy, this, IDC_ED_COUNT + nIdx);

		m_bn_ResetCount[nIdx].Create(_T("Reset"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_RESET_CNT + nIdx);

		m_ed_Count_Max[nIdx].SetFont(&m_font_Default);
		m_ed_Count[nIdx].SetFont(&m_font_Default);
	}

	m_bn_Cancel.Create(_T("Cancel"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_CANCEL);

	m_cb_File.SetFont(&m_font_Default);


	// 파일 감시 쓰레드 설정
	//	m_IniWatch.SetOwner(GetSafeHwnd(), WM_REFRESH_DriverCount);
	if (!m_strDriverCountPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_strDriverCountPath, DRIVERCOUNT_FILE_EXT);
		//m_IniWatch.BeginWatchThrFunc();

		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());

		if (m_szDriverCountFile.IsEmpty())
		{
			m_cb_File.SetCurSel(0);
		}
	}

	// 기본 데이터
	CString strFile;
	m_cb_File.GetWindowText(strFile);
	SetDriverCountFile(strFile);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_DriverCount::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iSubTop = 0;
	int iSubLeft = 0;

	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin;

	int iCtrlHeight = 26;
	int iCapWidth = 140;
	int Combo_W = (int)(iCapWidth * 1.5);

	int iTempWidth = 0;

	m_st_File.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);

	iLeft += iCapWidth + iSpacing;
	m_cb_File.MoveWindow(iLeft, iTop, Combo_W, iCtrlHeight);

	iLeft += Combo_W + iSpacing;
	m_bn_NewFile.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);

	iLeft = iMagrin;
	iTop += iCtrlHeight + iSpacing;

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		iSubTop = iTop + iCtrlHeight + iSpacing;
		m_stCount_Max[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_stCount[nIdx].MoveWindow(iLeft, iSubTop, iCapWidth, iCtrlHeight);

		iSubLeft = iLeft + iCapWidth + iSpacing;
		m_ed_Count_Max[nIdx].MoveWindow(iSubLeft, iTop, Combo_W, iCtrlHeight);
		m_ed_Count[nIdx].MoveWindow(iSubLeft, iSubTop, Combo_W, iCtrlHeight);

		iSubLeft += Combo_W + iSpacing;
		m_bn_ResetCount[nIdx].MoveWindow(iSubLeft, iTop, iCapWidth, iCtrlHeight * 2 + iSpacing);

		iTop += iCtrlHeight + iCateSpacing;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_DriverCount::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_DriverCount::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class

	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2016/10/31 - 22:38
// Desc.		:
//=============================================================================
void CWnd_Cfg_DriverCount::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_BaseView::OnShowWindow(bShow, nStatus);

	if (bShow)
	{
		LoadIniFile(m_szDriverCountFile);
	}
}

//=============================================================================
// Method		: OnCbnSelendokFile
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/6 - 11:01
// Desc.		:
//=============================================================================
void CWnd_Cfg_DriverCount::OnCbnSelendokFile()
{
	// 선택된 파일 로드
	CString szFile;

	int iSel = m_cb_File.GetCurSel();
	if (0 <= iSel)
	{
		m_cb_File.GetWindowText(szFile);

		SelectEndDriverCountFile(szFile);
	}
}

//=============================================================================
// Method		: OnBnClickedBnSave
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/6 - 11:01
// Desc.		:
//=============================================================================
void CWnd_Cfg_DriverCount::OnBnClickedBnSave()
{
	// UI에서 데이터 구함
	CString szText = _T("");
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_ed_Count_Max[nIdx].GetWindowText(szText);
		m_pstDriverCount->dwCount_Max[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();

		m_ed_Count[nIdx].GetWindowText(szText);
		m_pstDriverCount->dwCount[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();
	}

	// 파일 다른이름으로 저장
	CString strFullPath;
	CString strFullTitle;
	CString strFileExt;
	strFileExt.Format(_T("Ini File (*.%s)| *.%s||"), DRIVERCOUNT_FILE_EXT, DRIVERCOUNT_FILE_EXT);

	CFileDialog fileDlg(FALSE, DRIVERCOUNT_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strDriverCountPath;

	if (fileDlg.DoModal() == IDOK)
	{
		strFullPath = fileDlg.GetPathName();
		strFullTitle = fileDlg.GetFileTitle();

		if (m_fileModel.SaveDriverCountIniFile(strFullPath, m_pstDriverCount))
		{
			// 리스트 모델 갱신
			m_szDriverCountFile = strFullTitle;
			m_IniWatch.RefreshList();
			RefreshFileList(m_IniWatch.GetFileList());
		}
	}
}

//=============================================================================
// Method		: OnBnClickedReset
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2016/10/31 - 21:45
// Desc.		:
//=============================================================================
void CWnd_Cfg_DriverCount::OnBnClickedReset(UINT nID)
{
	UINT nIndex = nID - IDC_BN_RESET_CNT;

	m_ed_Count[nIndex].SetWindowText(_T("0"));
}

//=============================================================================
// Method		: OnRefreshFileList
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/1/6 - 11:01
// Desc.		:
//=============================================================================
LRESULT CWnd_Cfg_DriverCount::OnRefreshFileList(WPARAM wParam, LPARAM lParam)
{
	RefreshFileList(m_IniWatch.GetFileList());

	return 0;
}

//=============================================================================
// Method		: LoadIniFile
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_DriverCount::LoadIniFile(__in LPCTSTR szFile)
{
	CString strFilePath;

	strFilePath.Format(_T("%s%s.%s"), m_strDriverCountPath, szFile, DRIVERCOUNT_FILE_EXT);

	if (m_fileModel.LoadDriverCountIniFile(strFilePath, *m_pstDriverCount))
	{
		// 화면에 표시
		CString szText;
		for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
		{
			szText.Format(_T("%d"), m_pstDriverCount->dwCount_Max[nIdx]);
			m_ed_Count_Max[nIdx].SetWindowText(szText);
			szText.Format(_T("%d"), m_pstDriverCount->dwCount[nIdx]);
			m_ed_Count[nIdx].SetWindowText(szText);
		}
		GetOwner()->SendNotifyMessage(WM_UPDATE_DRIVERCOUNT, 0, 0);

		return TRUE;
	}
	return FALSE;
}

//=============================================================================
// Method		: SaveIniFile
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_DriverCount::SaveIniFile(__in LPCTSTR szFile)
{
	CString strFilePath;

	strFilePath.Format(_T("%s%s.%s"), m_strDriverCountPath, szFile, DRIVERCOUNT_FILE_EXT);

	// UI에서 데이터 구함
	CString szText;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_ed_Count_Max[nIdx].GetWindowText(szText);
		m_pstDriverCount->dwCount_Max[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();

		m_ed_Count[nIdx].GetWindowText(szText);
		m_pstDriverCount->dwCount[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();
	}

	GetOwner()->SendNotifyMessage(WM_UPDATE_DRIVERCOUNT, 0, 0);
	return m_fileModel.SaveDriverCountIniFile(strFilePath, m_pstDriverCount);
}

//=============================================================================
// Method		: SelectEndDriverCountFile
// Access		: protected  
// Returns		: void
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_DriverCount::SelectEndDriverCountFile(__in LPCTSTR szFile)
{
	// 파일 로드
	if (LoadIniFile(szFile))
	{
		m_szDriverCountFile = szFile;
	}
}

//=============================================================================
// Method		: RefreshFileList
// Access		: public  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_DriverCount::RefreshFileList(__in const CStringList* pFileList)
{
	m_cb_File.ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_File.AddString(pFileList->GetNext(pos));
	}

	// 이전에 선택되어있는 파일 다시 선택
	if (!m_szDriverCountFile.IsEmpty())
	{
		int iSel = m_cb_File.FindStringExact(0, m_szDriverCountFile);

		if (0 <= iSel)
		{
			m_cb_File.SetCurSel(iSel);
		}
	}
}

//=============================================================================
// Method		: SetDriverCountFile
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_DriverCount::SetDriverCountFile(__in LPCTSTR szFile)
{
	int iSel = m_cb_File.FindStringExact(0, szFile);

	if (0 <= iSel)
	{
		m_cb_File.SetCurSel(iSel);

		SelectEndDriverCountFile(szFile);
	}
}

//=============================================================================
// Method		: GetDriverCountInfo
// Access		: public  
// Returns		: ST_DriverCountInfo
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
ST_DriverCountInfo CWnd_Cfg_DriverCount::GetDriverCountInfo()
{
	CString szText;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_ed_Count_Max[nIdx].GetWindowText(szText);
		m_pstDriverCount->dwCount_Max[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();

		m_ed_Count[nIdx].GetWindowText(szText);
		m_pstDriverCount->dwCount[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();
	}

	m_pstDriverCount->szDriverCountFilename = m_szDriverCountFile;

	return *m_pstDriverCount;
}

//=============================================================================
// Method		: GetDriverCountName
// Access		: public  
// Returns		: CString
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
CString CWnd_Cfg_DriverCount::GetDriverCountName()
{
	return m_szDriverCountFile;
}

//=============================================================================
// Method		: SaveDriverCountSetting
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_DriverCount::SaveDriverCountSetting()
{
	CString szFile;
	m_cb_File.GetWindowText(szFile);

	return SaveIniFile(szFile);
}
