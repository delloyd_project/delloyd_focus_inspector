﻿#ifndef List_Work_EIAJ_h__
#define List_Work_EIAJ_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_Resl_Worklist
{
	Resl_W_Time,
	Resl_W_Equipment,
	Resl_W_Model,
	Resl_W_SWVersion,
	Resl_W_LOTNum,
	Resl_W_Barcode,
	Resl_W_Operator,
	Resl_W_Result,
	Resl_W_LEFT,
	Resl_W_RIGHT,
	Resl_W_TOP,
	Resl_W_BOTTOM,
	Resl_W_S1,
	Resl_W_S2,
	Resl_W_S3,
	Resl_W_S4,
	Resl_W_S5,
	Resl_W_S6,
	Resl_W_S7,
	Resl_W_S8,
	Resl_W_MaxCol,

};

// 헤더
static const TCHAR*	g_lpszHeader_Resl_Worklist[] =
{
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SWVersion"),
	_T("LOTNum"),
	_T("Barcode"),
	_T("Operator"),
	_T("Result"),
	_T("C_L"),
	_T("C_R"),
	_T("C_T"),
	_T("C_B"),
	_T("S1"),
	_T("S2"),
	_T("S3"),
	_T("S4"),
	_T("S5"),
	_T("S6"),
	_T("S7"),
	_T("S8"),
	NULL
};

const int	iListAglin_Resl_Worklist[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_Resl_Worklist[] =
{
	130,
	120,
	80,
	80,
	80,
	80,
	80,
	80,
	60,
	60,
	60,
	60,
	60,
	60,
	60,
	60,
	60,
	60,
	60,
	60,
	60,
	60,
};

// CList_Work_EIAJ

class CList_Work_EIAJ : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_EIAJ)

public:
	CList_Work_EIAJ();
	virtual ~CList_Work_EIAJ();

	CFont		m_Font;
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	UINT Header_MaxNum();

	void InitHeader();
	void InsertFullData(__in const ST_CamInfo* pstCamInfo);
	void SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo);

	void GetData(UINT nRow, UINT &DataNum, CString *Data);
};

#endif // List_Work_EIAJ_h__
