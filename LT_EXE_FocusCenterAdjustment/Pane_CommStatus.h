﻿//*****************************************************************************
// Filename	: 	Pane_CommStatus.h
// Created	:	2014/7/5 - 10:23
// Modified	:	2016/09/21
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Pane_CommStatus_h__
#define Pane_CommStatus_h__

#pragma once

#include "Def_Enum.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"

//#define		USE_LOG_WND // 테스트 용도 출력 창

#ifdef USE_LOG_WND
#include "LogOutput.h"
#endif

//=============================================================================
// CPane_CommStatus
//=============================================================================
class CPane_CommStatus : public CMFCTasksPane
{
	DECLARE_DYNAMIC(CPane_CommStatus)

public:
	CPane_CommStatus();
	virtual ~CPane_CommStatus();

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);	
	afx_msg void	OnUpdateCmdUI_Test	(CCmdUI* pCmdUI);
	afx_msg void	OnBnClickedTest		(UINT nID);

	afx_msg void	OnUpdateCmdUI_Dev	(CCmdUI* pCmdUI);
	afx_msg void	OnBnClicked_Dev		(UINT nID);

	CFont			m_Font;

#ifdef USE_LOG_WND
	CVGStatic		m_st_WarningEvent;
	CLogOutput		m_ed_WarningLog;
#endif
	
	CButton			m_bn_Test[4];
	
	enum enDeviceStaticIndex
	{
		DevSI_BaseIndex			= 0,
		DevSI_PermissionMode	= 0,
		Dev_MES,
		DevSI_PCIMotorBrd,
		DevSI_PCIIoBrd,
 		DevSI_GrabberBrd_ComArt,
//		DevSI_GrabberBrd_DAQ,
		DevSI_BCR,
		DevSI_Cam_PCB_1,
		DevSI_Cam_PCB_2,
		DevSI_Cam_PCB_3,
		DevSI_Cam_PCB_4,
		DevSI_Light_PCB_1,
		DevSI_Light_PCB_2,
		DevSI_Light_PCB_3,
		DevSI_Indicator_1,
		DevSI_Indicator_2,
// 		DevSi_Displace,
// 		DevSi_VisionCam,
// 		DevSi_VisionLight,
		DevSI_MaxCount,
	};

	enum enDeviceButtonIndex
	{
		DEV_BN_PermissionChange,
		DEV_BN_Keyboard,
		DEV_BN_Barcode,
		DEV_BN_MaxCount,
	};

	enum enDeviceIndicator
	{
		DEV_ST_IndicatorX,
		DEV_ST_IndicatorY,
		DEV_ST_IndicatorMax,
	};


	// -- 제어 권한 모드 --
	// 작업자, 상급자, 관리자
	// 접속	

	// -- 주변 장치 --
	// 프레임 그래버 보드 (최대 6개, PC당 2보드 총 4채널) 
	// 바코드 리더기

	// -- Digital In --
	// I/O 상태 (최대 16)


	CVGStatic		m_st_Device[DevSI_MaxCount];
	CButton			m_bn_Device[DEV_BN_MaxCount];
	CVGStatic		m_st_Indicator[DEV_ST_IndicatorMax];
	BOOL			m_bIndcatorEnable[DEV_ST_IndicatorMax];
	
	void			AddSystemInfo			();

	void			AddPermissionMode		();	

	void			AddBarcode				();
	void			AddPCISlot				();
	void			AddSerialComm			();
	void			AddTCPIP				();
	void			AddGrabber				();
	void			AddDisplay				();
	void			AddUtilities			();
	
#ifdef USE_LOG_WND
	void			AddWarningStatus		();
#endif
	void			AddTestCtrl				();	

public:

	virtual CSize	CalcFixedLayout			(BOOL, BOOL);
	
	void			SetStatus_PermissionMode	(__in enPermissionMode InspMode);
	void			SetStatus_MES				(__in UINT nCommStatus);
	void			SetStatus_Motor				(__in UINT nConStatus);
	void			SetStatus_IO				(__in UINT nConStatus);
	void			SetStatus_GrabberBrd_ComArt	(__in UINT nConStatus);
	void			SetStatus_BCR				(__in BOOL bConStatus);
	void			SetStatus_CameraBoard		(__in UINT nBrdIdx, __in UINT nConStatus);
	void			SetStatus_Indicator			(__in UINT nConStatus, __in UINT nChIdx);
	void			Set_Barcode					(__in LPCTSTR szBarcode);
	void			SetIndicatorDisplay			(__in ST_ModelInfo &stModelInfo, __in float fAxisX, __in float fAxisY);
	void			SetIndicatorButtonEnable	(__in BOOL bMode);

};

#endif // Pane_CommStatus_h__
