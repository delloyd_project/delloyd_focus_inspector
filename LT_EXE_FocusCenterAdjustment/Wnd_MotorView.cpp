﻿//*****************************************************************************
// Filename	: Wnd_MotorView.cpp
// Created	: 2017/04/03
// Modified	: 2017/04/03
//
// Author	: KHO
//	
// Purpose	: 
//*****************************************************************************
// Wnd_MotorView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MotorView.h"

#define		IDC_CB_FILE				1001
#define		IDC_BN_SAVE				1002

//=============================================================================
// CWnd_MotorView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_MotorView, CWnd_BaseView)

CWnd_MotorView::CWnd_MotorView()
{
	m_pDevice		= NULL;

	VERIFY(m_font.CreateFont(
		17,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_MotorView::~CWnd_MotorView()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_MotorView, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE	()
	ON_MESSAGE(WM_SELECT_AXIS, OnMotorSelect)
	ON_MESSAGE(WM_UPDATA_AXIS, OnMotorUpdata)
	ON_BN_CLICKED(IDC_BN_SAVE, OnBnClickedBnNew)
	ON_CBN_SELENDOK(IDC_CB_FILE, OnCbnSelendokFile)
END_MESSAGE_MAP()

//=============================================================================
// CWnd_MotorView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_MotorView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
int CWnd_MotorView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_BOTTOM);

	m_st_File.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_File.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_File.SetFont_Gdip(L"Arial", 10.5F);

	m_st_File.Create(_T("MODEL FILE"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_cb_File.Create(dwStyle | CBS_DROPDOWNLIST | CBS_SORT, rectDummy, this, IDC_CB_FILE);
	m_cb_File.SetFont(&m_font);

	m_bn_NewFile.Create(_T("Add New File"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE);

	if (!m_szMotorpath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szMotorpath, MOTOR_FILE_EXT);
		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());
		m_cb_File.SetCurSel(0);
	}

	m_Wnd_MotionTable.SetOwner(this);
	m_Wnd_MotionTable.Create(NULL, _T(""), dwStyle, rectDummy, this, 10);

	m_Wnd_MotionCtr.SetOwner(this);
	m_Wnd_MotionCtr.Create(NULL, _T(""), dwStyle, rectDummy, this, 11);
	
	m_Wnd_OriginOp.SetOwner(GetOwner());
	m_Wnd_OriginOp.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Option, 12);

	m_Wnd_MotionOp.SetOwner(this);
	m_Wnd_MotionOp.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Option, 13);

	m_Wnd_MotionTeach.SetOwner(this);
	m_Wnd_MotionTeach.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Option, 14);

	m_tc_Option.AddTab(&m_Wnd_OriginOp,		_T("Origin"),	0, FALSE);
	m_tc_Option.AddTab(&m_Wnd_MotionOp,		_T("Motion"),	1, FALSE);
	m_tc_Option.AddTab(&m_Wnd_MotionTeach,	_T("Sequence"),	2, FALSE);
	m_tc_Option.EnableTabSwap(FALSE);

	m_tc_Option.SetActiveTab(0);

	m_Wnd_OriginOp.SetUpdataData();
	m_Wnd_MotionOp.SetUpdataData();

	m_st_Filename.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Filename.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Filename.SetFont_Gdip(L"Arial", 11.0F);
	m_st_Filename.Create(_T("파일 경로"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	return 0;
}

//=============================================================================
// Method		: CWnd_MotorView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
void CWnd_MotorView::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;
	
	int iCtrlWidth = (iWidth - iSpacing * 3) / 9;
	int iCtrlHeight = 25;

	m_st_Filename.MoveWindow(iLeft, iTop, iCtrlWidth * 4, iCtrlHeight);
	iLeft += iCtrlWidth * 4 + iSpacing;

	m_st_File.MoveWindow(iLeft, iTop, iCtrlWidth , iCtrlHeight);
	iLeft += iCtrlWidth + iSpacing;

	m_cb_File.MoveWindow(iLeft, iTop, iCtrlWidth * 3, iCtrlHeight);
	iLeft += iCtrlWidth * 3 + iSpacing;

	m_bn_NewFile.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iSpacing = 10;
	iTop += iCtrlHeight;
	iLeft = iMagrin;
	iHeight = iHeight - iTop;
	iHeight = (int)iHeight / 8;

	m_Wnd_MotionTable.MoveWindow(iLeft, iTop, iWidth, iHeight * 3);
	iTop += iHeight * 3 + iSpacing;

	m_Wnd_MotionCtr.MoveWindow(iLeft, iTop, iWidth, iHeight * 2 + iSpacing * 5);

	iTop += iHeight * 2 + iSpacing * 5;
	m_tc_Option.MoveWindow(iLeft, iTop, iWidth, iHeight * 2 + iSpacing);
}

//=============================================================================
// Method		: OnBnClickedBnNew
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/4/4 - 16:52
// Desc.		:
//=============================================================================
void CWnd_MotorView::OnBnClickedBnNew()
{
	if (IDYES == AfxMessageBox(_T("Create a New Motor file?"), MB_YESNO))
	{
		CString strFullPath;
		CString strFolderPath;
		CString strFileTitle;
		CString strFileExt;
		strFileExt.Format(_T("Motor File (*.%s)| *.%s||"), MOTOR_FILE_EXT, MOTOR_FILE_EXT);

		CFileDialog fileDlg(FALSE, MOTOR_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
		fileDlg.m_ofn.lpstrInitialDir = m_szMotorpath;

		if (fileDlg.DoModal() == IDOK)
		{
			strFullPath = fileDlg.GetPathName();
			//strFolderPath = fileDlg.();
			//strFullPath.Find()
			strFileTitle = fileDlg.GetFileTitle();

			//m_szMotorpath = strFolderPath;

			m_pDevice->MotionManager.SetPrtMotorPath(&m_szMotorpath, strFileTitle);

			if (m_pDevice->MotionManager.SaveMotionParam())
			{
				;
			}

			GetOwner()->SendNotifyMessage(WM_CHANGED_MOTOR, (WPARAM)strFileTitle.GetBuffer(), 0);
			SetModel(m_szMotorpath, strFileTitle);
		}
	}
}

//=============================================================================
// Method		: OnCbnSelendokFile
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/4/4 - 16:53
// Desc.		:
//=============================================================================
void CWnd_MotorView::OnCbnSelendokFile()
{
	// 선택된 파일 로드
	CString szFile;
	CString szMotorFileFull;

	int iSel = m_cb_File.GetCurSel();
	if (0 <= iSel)
	{
		m_cb_File.GetWindowText(szFile);
	
		m_pDevice->MotionManager.SetPrtMotorPath(&m_szMotorpath, szFile);
		m_pDevice->MotionManager.LoadMotionParam();
		SetUpdataData();
	}

	szMotorFileFull = m_szMotorpath + szFile;
	m_st_Filename.SetWindowText(szMotorFileFull + _T(".mot"));

	GetOwner()->SendNotifyMessage(WM_CHANGED_MOTOR, (WPARAM)szFile.GetBuffer(), 0);
	szFile.ReleaseBuffer();

}

//=============================================================================
// Method		: OnMotorSelect
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/4/3 - 11:32
// Desc.		:
//=============================================================================
LRESULT CWnd_MotorView::OnMotorSelect(WPARAM wParam, LPARAM lParam)
{
	UINT nAxis = (UINT)wParam;

	m_Wnd_MotionOp.SetSelectAxis(nAxis);
	m_Wnd_OriginOp.SetSelectAxis(nAxis);
	m_Wnd_MotionCtr.SetSelectAxis(nAxis);

	return TRUE;
}

//=============================================================================
// Method		: OnMotorUpdata
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/4/4 - 10:45
// Desc.		:
//=============================================================================
LRESULT CWnd_MotorView::OnMotorUpdata(WPARAM wParam, LPARAM lParam)
{
	m_Wnd_MotionTable.SetUpdataAxisName();

	return 0;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
BOOL CWnd_MotorView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetDeleteTimer
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/28 - 12:36
// Desc.		:
//=============================================================================
void CWnd_MotorView::SetDeleteTimer()
{
	m_Wnd_MotionTable.SetDeleteTimer();
}

//=============================================================================
// Method		: SetOriginEnable
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 17:29
// Desc.		:
//=============================================================================
void CWnd_MotorView::SetOriginEnable()
{
	m_Wnd_OriginOp.EnableWindow(TRUE);
}

//=============================================================================
// Method		: SetModel
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/4/11 - 15:08
// Desc.		:
//=============================================================================
void CWnd_MotorView::SetModel(__in LPCTSTR szPath, __in LPCTSTR szFile)
{
	CString strFile;
	CString strFullPath;

	if (m_st_Filename.m_hWnd == NULL)
		return;

	strFile		= szFile;
	strFullPath = szPath + strFile;

	if (!m_szMotorpath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szMotorpath, MOTOR_FILE_EXT);
		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());
	}

	if (!strFile.IsEmpty())
	{
		int iSel = m_cb_File.FindStringExact(0, strFile);

		if (0 <= iSel)
			m_cb_File.SetCurSel(iSel);

		if (!strFullPath.IsEmpty())
			m_st_Filename.SetWindowText(strFullPath + _T(".mot"));
	}
}

//=============================================================================
// Method		: SetInspectionMode
// Access		: public  
// Returns		: void
// Parameter	: enum_Inspection_Mode InspMode
// Qualifier	:
// Last Update	: 2016/1/13 - 14:18
// Desc.		:
//=============================================================================
void CWnd_MotorView::SetInspectionMode(enPermissionMode InspMode)
{
	switch (InspMode)
	{
	case Permission_Operator:
		break;

	case Permission_Manager:
	case Permission_Administrator:
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdataData
// Access		: public  
// Returns		: void
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/4 - 16:43
// Desc.		:
//=============================================================================
void CWnd_MotorView::SetUpdataData()
{
	if (m_pDevice == NULL)
		return;

	UINT nSelectAxis = 0;

	for (UINT nAxis = 0; nAxis < (UINT)m_pDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (m_pDevice->MotionManager.GetAxisUseStatus(nAxis))
		{
			nSelectAxis = nAxis;
			break;
		}
	}

	m_pDevice->MotionManager.SetAllMotionSetting();
	m_Wnd_MotionTable.SetUpdataDataReset(nSelectAxis);
	m_Wnd_MotionCtr.SetSelectAxis(nSelectAxis);
	m_Wnd_MotionOp.SetSelectAxis(nSelectAxis);
	m_Wnd_OriginOp.SetSelectAxis(nSelectAxis);
	m_Wnd_MotionTeach.SetUpdataData();
}

//=============================================================================
// Method		: RefreshFileList
// Access		: public  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
void CWnd_MotorView::RefreshFileList(__in const CStringList* pFileList)
{
	m_cb_File.ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_File.AddString(pFileList->GetNext(pos));
	}

	// 이전에 선택되어있는 파일 다시 선택
	if (!m_szMotorpath.IsEmpty())
	{
		int iSel = m_cb_File.FindStringExact(0, m_szMotorpath);

		if (0 <= iSel)
		{
			m_cb_File.SetCurSel(iSel);
		}
	}
}
