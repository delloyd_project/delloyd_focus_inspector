﻿//*****************************************************************************
// Filename	: 	TestManager_Device.h
// Created	:	2016/9/28 - 19:54
// Modified	:	2016/9/28 - 19:54
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_Device_h__
#define TestManager_Device_h__

#pragma once

#include "TestManager_Base.h"

#include "Def_TestDevice.h"
#include "Def_DataStruct.h"
#include "LT_Option.h"
#include "Def_ErrorCode.h"

//-----------------------------------------------------------------------------
// CTestManager_Device
//-----------------------------------------------------------------------------
class CTestManager_Device : public CTestManager_Base
{
public:
	CTestManager_Device();
	virtual ~CTestManager_Device();

protected:

	//-------------------------------------------------------------------------
	// 옵션
	//-------------------------------------------------------------------------
	stLT_Option		m_stOption;
	// 환경설정 데이터 불러오기
	virtual BOOL	OnLoadOption					();
	//virtual void	OnInitUISetting					(__in HWND hWndOwner = NULL);

	//-------------------------------------------------------------------------
	// 주변장치 제어
	//-------------------------------------------------------------------------

	// 전체 주변장치와 통신 연결 전의 초기 작업
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);

	// 전체 주변장치와 통신 연결 시도
	virtual void	ConnectDevicez					();

	// 전체 주변장치의 연결 해제
	virtual void	DisconnectDevicez				();

	// MES TCP/IP 통신 연결 및 해제
	virtual BOOL	ConnectMES						(__in BOOL bConnect);

	// 프레임 그래버 보드 연결 및 해제
	virtual BOOL	ConnectGrabberBrd_ComArt		(__in BOOL bConnect);

	// 바코드 리더기 연결 및 해제
	virtual BOOL	ConnectBCR						(__in BOOL bConnect);

	// PCB 보드 연결 및 해제
	virtual BOOL	ConnectPCB_CameraBrd			(__in BOOL bConnect);

	// 모션보드 & IO 연결 및 해제
	virtual BOOL	ConnectIO						(__in BOOL bConnect);
	virtual BOOL	ConnectMotor					(__in BOOL bConnect);

	// Digital Indicator 제어 연결 및 해제
	virtual BOOL	ConnectIndicator				(__in BOOL bConnect);

	// 비전 카메라 연결
	//virtual BOOL	ConnectVisionCam				(__in BOOL bConnect, __in UINT nCamIdx = 0 );

	// 비전 조명 연결
	//virtual BOOL	ConnectVisionLight				(__in BOOL bConnect, __in UINT nChIdx = 0 );

	//-------------------------------------------------------------------------
	// 통신 연결 상태 UI에 표시 
	//-------------------------------------------------------------------------
	
	// 그래버 보드
	virtual void	OnSetStatus_GrabberBrd_ComArt	(__in BOOL bConnect){};

	// 프레임 그래버, 채널 영상 상태
	virtual void	OnSetStatus_VideoStatus			(__in UINT nChIdx, __in UINT nConnect){};

	// 바코드 리더기 통신 연결 상태
	virtual void	OnSetStatus_BCR					(__in UINT nConnect){};

	// PCB 보드 통신 연결상태
	virtual void	OnSetStatus_PCBCamBrd			(__in UINT nConnect, __in UINT nIdxBrd){};

	// I/O 보드 연결 상태
	virtual void	OnSetStatus_IOBoard				(__in BOOL bConnect){};
	virtual void	OnSetStatus_MotorBoard			(__in BOOL bConnect){};

	// MES 통신 상태
	virtual void	OnSetStatus_MES					(__in UINT nConnect){};

	// Digital Indicator 연결 상태
	virtual void	OnSetStatus_Indicator			(__in BOOL bConnect, __in UINT nChIdx = 0){};

	// 비전 카메라 연결 상태
	virtual void	OnSetStatus_VisionCam			(__in BOOL bConnect, __in UINT nCamIdx = 0){};

	// 비전 조명 연결 상태
	virtual void	OnSetStatus_VisionLight			(__in BOOL bConnect, __in UINT nChIdx = 0){};

	//-------------------------------------------------------------------------
	// 바코드
	//-------------------------------------------------------------------------
	// 바코드 입력
	virtual void	OnSet_Barcode					(__in LPCTSTR szBarcode){};

	//-----------------------------------------------------
	// Digital I/O
	//-----------------------------------------------------
	virtual void	OnFunc_IO_EMO					(){};
	virtual void	OnFunc_IO_ErrorReset			(){};
	virtual void	OnFunc_IO_Init					(){};
	virtual void	OnFunc_IO_Door					(){};
	virtual void	OnFunc_IO_Stop					(){};

	virtual void	OnAddErrorInfo					(__in enErrorCode lErrorCode){};
	
	//-----------------------------------------------------
	// 경광등, 알람 제어
	//-----------------------------------------------------
	BOOL			LEDControl_Init					();
	BOOL			LEDControl						(__in enLampColor LampColor);
	BOOL			LEDControl_Judge				(__in enTestResult Judgment);
	// 알람
	BOOL			AlarmControl					(__in BOOL bOn, __in enBuzzerType BuzzerType);

	//-----------------------------------------------------
	// 주변 장치 제어용 클래스 모음
	//-----------------------------------------------------
	ST_Device			m_Device;
	
public:

	// 생성자 처리용 코드
	virtual void	OnInitialize					();
	// 소멸자 처리용 코드
	virtual	void	OnFinalize						();

	BOOL			b_IndicatorConnect[Indicator_Max];

};

#endif // TestManager_Device_h__

