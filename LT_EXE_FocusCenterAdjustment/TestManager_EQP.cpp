﻿//*****************************************************************************
// Filename	: 	TestManager_EQP.cpp
// Created	:	2016/5/9 - 13:32
// Modified	:	2016/08/10
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_EQP.h"
#include "CommonFunction.h"
#include "File_Model.h"
#include "File_MES_Mcnex.h"

#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

CTestManager_EQP::CTestManager_EQP()
{
	m_bBusyIndicator = FALSE;
	// 쓰레드 관련
	m_hThrTest_All		= NULL;
	//m_hThrAutoFocusing	= NULL;

	for (UINT nCnt = 0; nCnt < MAX_OPERATION_THREAD; nCnt++)
		m_hThrTest_Unit[nCnt] = NULL;

	for (UINT nCnt = 0; nCnt < 12; nCnt++)
		m_bFlag_EtcControlSwitch[nCnt] = FALSE;

	for (UINT nCnt = 0; nCnt < Indicator_Max; nCnt++)
		m_bFlag_Indicator[nCnt] = TRUE;

	for (UINT nIdx = 0; nIdx < Indicator_Max; nIdx++)
		m_fValue[nIdx] = 0.00;

	for (UINT nIdx = 0; nIdx < DI_NotUseBit_Max; nIdx++)
		m_bFlag_Button[nIdx] = TRUE;

	for (UINT nIdx = 0; nIdx < TIID_MaxEnum; nIdx++)
		m_bFlag_TestItem[nIdx] = FALSE;

	m_bFlag_ReadyTest		= FALSE;
	m_dwTimeCheck			= 0;
	m_bFlag_InitialSwitch	= FALSE;
	m_bFlag_MasterSet		= TRUE;
	m_bFlag_UseStop			= FALSE;
	m_bFlag_FocusTest		= FALSE;
	m_bFlag_AutoFocus		= TRUE;
	m_bFlag_ActiveAlginTest = FALSE;
	m_bFixStatus			= FALSE;
	m_bFailBoxStatus		= FALSE;

	m_hTimer_Update_Indicator = NULL;
	m_hTimerQueue_Indicator   = NULL;

	OnInitialize();
}

CTestManager_EQP::~CTestManager_EQP()
{
	TRACE(_T("<<< Start ~CTestManager_EQP >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_EQP >>> \n"));
}


//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/28 - 20:04
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnLoadOption()
{
	BOOL bReturn = __super::OnLoadOption();

	if (!m_stOption.Inspector.szPath_Model.IsEmpty())
		m_stInspInfo.Path.szModel = m_stOption.Inspector.szPath_Model + _T("\\");

	if (!m_stOption.Inspector.szPath_Log.IsEmpty())
		m_stInspInfo.Path.szLog = m_stOption.Inspector.szPath_Log + _T("\\");

	if (!m_stOption.Inspector.szPath_Report.IsEmpty())
		m_stInspInfo.Path.szReport = m_stOption.Inspector.szPath_Report + _T("\\");
	
	if (!m_stOption.Inspector.szPath_Image.IsEmpty())
		m_stInspInfo.Path.szImage = m_stOption.Inspector.szPath_Image + _T("\\");

	if (!m_stOption.Inspector.szPath_Pogo.IsEmpty())
		m_stInspInfo.Path.szPogo = m_stOption.Inspector.szPath_Pogo + _T("\\");

	if (!m_stOption.Inspector.szPath_DriverCount.IsEmpty())
		m_stInspInfo.Path.szDriverCount = m_stOption.Inspector.szPath_DriverCount + _T("\\");

// 	if (!m_stOption.Inspector.szPath_Motor.IsEmpty())
// 		m_stInspInfo.Path.szMotor = m_stOption.Inspector.szPath_Motor + _T("\\");

	if (!m_stOption.Inspector.szPath_MES.IsEmpty())
		m_stInspInfo.Path.szMes = m_stOption.Inspector.szPath_MES + _T("\\");

	if (!m_stOption.Inspector.szPath_MES2.IsEmpty())
		m_stInspInfo.Path.szMes2 = m_stOption.Inspector.szPath_MES2 + _T("\\");

	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in HWND hWndOwner
// Qualifier	:
// Last Update	: 2017/2/15 - 16:11
// Desc.		:
//=============================================================================
void CTestManager_EQP::InitDevicez(__in HWND hWndOwner /*= NULL*/)
{
	__super::InitDevicez(hWndOwner);
	m_Device.MotionSequence.SetLogMsgID(hWndOwner, WM_LOGMSG);
	m_TIProcessing.SetLogMsgID(hWndOwner, WM_LOGMSG_PROC);
}

//=============================================================================
// Method		: OnInitDigitalIOSignal
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/13 - 11:59
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitDigitalIOSignal()
{

}

//=============================================================================
// Method		: OnFunc_IO_EMO
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/28 - 10:54
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_EMO()
{
	m_bFlag_ReadyTest = FALSE;

	if (FALSE == m_stInspInfo.CamInfo.szBarcode.IsEmpty())
	{
		// 검사 중인가?
		// 검사가 끝난 제품인가???
		if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
		{
			m_stInspInfo.CamInfo.nJudgment = TR_Fail;
		}
	}

	if (IsTesting())
		StopProcess_All();
	
	// Error Dialog 팝업
	OnAddErrorInfo(Err_EMO);
}

//=============================================================================
// Method		: OnFunc_IO_ErrorReset
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/31 - 0:26
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_ErrorReset()
{
	OnInitDigitalIOSignal();
}

//=============================================================================
// Method		: OnFunc_IO_Init
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/31 - 0:27
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_Init()
{
	if (FALSE == IsTesting())
	{
		OnShowSplashScreen(TRUE, _T("모터 원점 잡기 수행 중입니다."));

		// LED, 경광등 초기화
		OnInitDigitalIOSignal();

		OnShowSplashScreen(FALSE);
	}
	else
	{
		MessageView(_T("Inspection is Running. Try after Inspection."));
	}
}

//=============================================================================
// Method		: OnFunc_IO_Door
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/15 - 13:36
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_Door()
{
	if (FALSE == m_stOption.Inspector.bUseDoorOpen_Err)
		return;

	m_Device.MotionSequence.TowerLamp(TowerLampRed, ON);
	m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLampBuzzer, ON);

	// 현재 검사 중인 제품은 모두 재검 처리
	if (FALSE == m_stInspInfo.CamInfo.szBarcode.IsEmpty())
	{
		// 검사 중인가?
		// 검사가 끝난 제품인가???
		if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
		{
			m_stInspInfo.CamInfo.nJudgment = TR_Rework;
		}
	}

	if (IsTesting())
	{
		StopProcess_All();
	}

	OnAddErrorInfo(Err_DoorSensor);
}

//=============================================================================
// Method		: OnFunc_IO_AirCheck
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/15 - 13:36
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_AirCheck()
{
// 	if (FALSE == m_stOption.Inspector.bUseAirCheck_Err)
// 		return;

	m_Device.MotionSequence.TowerLamp(TowerLampRed, ON);
	m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLampBuzzer, ON);

	// 현재 검사 중인 제품은 모두 재검 처리
	if (FALSE == m_stInspInfo.CamInfo.szBarcode.IsEmpty())
	{
		// 검사 중인가?
		// 검사가 끝난 제품인가???
		if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
		{
			m_stInspInfo.CamInfo.nJudgment = TR_Rework;
		}
	}

	if (IsTesting())
	{
		StopProcess_All();
	}

	OnAddErrorInfo(Err_MainPress);
}

//=============================================================================
// Method		: OnFunc_IO_MainPower
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/6 - 16:54
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_MainPower()
{
	m_Device.MotionSequence.TowerLamp(TowerLampRed, ON);
	m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLampBuzzer, ON);

	// 현재 검사 중인 제품은 모두 재검 처리
	if (FALSE == m_stInspInfo.CamInfo.szBarcode.IsEmpty())
	{
		// 검사 중인가?
		// 검사가 끝난 제품인가???
		if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
		{
			m_stInspInfo.CamInfo.nJudgment = TR_Rework;
		}
	}

	if (IsTesting())
	{
		StopProcess_All();
	}

	OnAddErrorInfo(Err_MainPower);
}

//=============================================================================
// Method		: OnFunc_IO_Stop
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/12/14 - 11:46
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFunc_IO_Stop()
{
	// 현재 검사 중인 제품은 모두 재검 처리
	if (FALSE == m_stInspInfo.CamInfo.szBarcode.IsEmpty())
	{
		// 검사 중인가?
		// 검사가 끝난 제품인가???
		if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
		{
			m_stInspInfo.CamInfo.nJudgment = TR_Rework;
		}
	}

	if (IsTesting())
		StopProcess_All();
}

//=============================================================================
// Method		: OnAddErrorInfo
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enErrorCode lErrorCode
// Qualifier	:
// Last Update	: 2016/10/31 - 1:05
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnAddErrorInfo(__in enErrorCode lErrorCode)
{
	CDlg_ErrView Dlg_ErrView;
	Dlg_ErrView.ErrMessage(lErrorCode);
	Dlg_ErrView.DoModal();
}

//=============================================================================
// Method		: OnDetectDigtalInSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnDetectDigitalInSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	enIO_In_BitOffset DInOffset = (enIO_In_BitOffset)byBitOffset;

	enPermissionMode enAcessMode = GetPermissionMode();

	switch (DInOffset)
	{
	case DI_EMO:
		if (!bOnOff)
		{
			if (m_bFlag_Button[DI_EMO] == FALSE)
			{
				m_bFlag_Button[DI_EMO] = TRUE;

				OnFunc_IO_EMO();

				m_bFlag_Button[DI_EMO] = FALSE;
			}
		}
		break;
	case DI_MainPowerBtn:
		if (bOnOff)
		{
			if (m_bFlag_Button[DI_MainPowerBtn] == FALSE)
			{
				m_bFlag_Button[DI_MainPowerBtn] = TRUE;

				OnFunc_IO_MainPower();

				m_bFlag_Button[DI_MainPowerBtn] = FALSE;
			}
		}
		break;

	case DI_ModuleFixCYLSensor:
		if (bOnOff)
		{
			m_bFixStatus = TRUE;
		}
		else{
			m_bFixStatus = FALSE;
		}

		break;
	case DI_ModuleUnfixCYLSensor:
		if (bOnOff)
		{
			m_bFixStatus = FALSE;
		}

		break;
	case DI_ModuleFixBtn:
		if (bOnOff)
		{
			if (m_bFlag_Button[DI_ModuleFixBtn] == FALSE)
			{
				m_bFlag_Button[DI_ModuleFixBtn] = TRUE;

				//if (!IsTesting() || m_bMessageViewMode == TRUE)
				{
					m_Device.MotionSequence.ModuleFixUnFixCYLMotion(ON);
				}

				m_bFlag_Button[DI_ModuleFixBtn] = FALSE;
			}
		}
		break;

	case DI_ModuleUnFixBtn:
		if (bOnOff)
		{
			if (m_bFlag_Button[DI_ModuleUnFixBtn] == FALSE)
			{
				m_bFlag_Button[DI_ModuleUnFixBtn] = TRUE;

				//if (!IsTesting() )
				{
					m_Device.MotionSequence.ModuleFixUnFixCYLMotion(OFF);
				}
				m_bFlag_Button[DI_ModuleUnFixBtn] = FALSE;
			}
		}
		break;

	case DI_PCBFixBtn:
		if (bOnOff)
		{
			if (enAcessMode != Permission_Operator)
			{
				if (m_bFlag_Button[DI_PCBFixBtn] == FALSE)
				{
					m_bFlag_Button[DI_PCBFixBtn] = TRUE;

					if (!IsTesting())
					{
						m_Device.MotionSequence.PCBFixUnFixCYLMotion(ON);
					}
					m_bFlag_Button[DI_PCBFixBtn] = FALSE;
				}
			}
		}
		break;

	case DI_PCBUnFixBtn:
		if (bOnOff)
		{
			if (enAcessMode != Permission_Operator)
			{
				if (m_bFlag_Button[DI_PCBUnFixBtn] == FALSE)
				{
					m_bFlag_Button[DI_PCBUnFixBtn] = TRUE;
					if (!IsTesting())
					{
						m_Device.MotionSequence.PCBFixUnFixCYLMotion(OFF);
					}
					m_bFlag_Button[DI_PCBUnFixBtn] = FALSE;
				}
			}
		}
		break;

	case DI_DriverInBtn:
		if (bOnOff)
		{
			if (enAcessMode != Permission_Operator)
			{
				if (m_bFlag_Button[DI_DriverInBtn] == FALSE)
				{
					m_bFlag_Button[DI_DriverInBtn] = TRUE;

					// PCB UnFix
					if (!IsTesting())
					{
						m_Device.MotionSequence.PCBFixUnFixCYLMotion(UNFIX);
						m_Device.MotionSequence.DriverInOutCYLMotion(ON);
					}
					m_bFlag_Button[DI_DriverInBtn] = FALSE;
				}
			}
		}
		break;

	case DI_DriverOutBtn:
		if (bOnOff)
		{
			if (enAcessMode != Permission_Operator)
			{
				if (m_bFlag_Button[DI_DriverOutBtn] == FALSE)
				{
					m_bFlag_Button[DI_DriverOutBtn] = TRUE;

					// PCB UnFix
					if (!IsTesting())
					{
						m_Device.MotionSequence.PCBFixUnFixCYLMotion(UNFIX);
						m_Device.MotionSequence.DriverInOutCYLMotion(OFF);
					}
					m_bFlag_Button[DI_DriverOutBtn] = FALSE;
				}
			}
		}
		break;

//  [12/26/2018 ysJang] 양수 버튼
 	case DI_StartBtn_L:
		if (bOnOff)
		{
			if (m_Device.DigitalIOCtrl.GetInStatus(DI_StartBtn_R) == TRUE){
				if (m_bFlag_Button[DI_StartBtn_L] == FALSE && m_bFlag_Button[DI_StartBtn_R] == FALSE)
				{
					m_bFlag_Button[DI_StartBtn_L] = TRUE;
					m_bFlag_Button[DI_StartBtn_R] = TRUE;
					
					if (!IsTesting())
					{
						if (GetPermissionMode() != Permission_Operator)
							StartOperation_AutoAll();
						else if (m_stInspInfo.LotInfo.bLotStatus == TRUE)
							StartOperation_AutoAll();
					}
					else
					{
						if (m_bTestFinishMode == TRUE)
						{
							m_bTestFinishMode = FALSE;
							m_bFlag_FocusTest = FALSE;
						}
					}
					m_bFlag_Button[DI_StartBtn_L] = FALSE;
					m_bFlag_Button[DI_StartBtn_R] = FALSE;
				}
			}
		}
		break;

 	case DI_StartBtn_R:
 		if (bOnOff)
 		{
 			if (m_Device.DigitalIOCtrl.GetInStatus(DI_StartBtn_L) == TRUE){
				if (m_bFlag_Button[DI_StartBtn_L] == FALSE && m_bFlag_Button[DI_StartBtn_R] == FALSE)
				{
					m_bFlag_Button[DI_StartBtn_L] = TRUE;
					m_bFlag_Button[DI_StartBtn_R] = TRUE;

					if (!IsTesting())
					{
						if (GetPermissionMode() != Permission_Operator)
							StartOperation_AutoAll();
						else if (m_stInspInfo.LotInfo.bLotStatus == TRUE)
							StartOperation_AutoAll();
					}
					else
					{
						if (m_bTestFinishMode == TRUE)
						{
							m_bTestFinishMode = FALSE;
							m_bFlag_FocusTest = FALSE;
						}
					}
					m_bFlag_Button[DI_StartBtn_L] = FALSE;
					m_bFlag_Button[DI_StartBtn_R] = FALSE;
				}
 			}
 			else{
				Delay(500);
				if (m_Device.DigitalIOCtrl.GetInStatus(DI_StartBtn_R) == TRUE)
				{
					if (m_bFlag_Button[DI_StartBtn_R] == FALSE)
					{
						m_bFlag_Button[DI_StartBtn_R] = TRUE;

						m_bFlag_UseStop = TRUE;
						m_bFlag_Button[DI_StartBtn_R] = FALSE;
					}
				}
 			}		
 		}
 		break;
		
	case DI_FailBoxSensor:
		if (bOnOff)
			m_bFailBoxStatus = TRUE;
		else
			m_bFailBoxStatus = FALSE;
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: StartOperation_Auto
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in UINT nTestItemID
// Qualifier	:
// Last Update	: 2017/1/4 - 14:23
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::StartOperation_Auto(__in UINT nUnitIdx, __in UINT nTestItemID)
{
	if (FALSE == m_bFlag_ReadyTest)
		return FALSE;

	if (IsTesting_Unit(nUnitIdx))
	{
		TRACE(_T("Inspection is Running.\n"));
		return FALSE;
	}

	if (NULL != m_hThrTest_Unit[nUnitIdx])
	{
		CloseHandle(m_hThrTest_Unit[nUnitIdx]);
		m_hThrTest_Unit[nUnitIdx] = NULL;
	}

	stThreadParam* pParam = new stThreadParam;
	pParam->pOwner = this;
	pParam->nIndex = nUnitIdx;
	pParam->nArg_1 = nTestItemID;
	pParam->nArg_2 = 0;

	m_hThrTest_Unit[nUnitIdx] = HANDLE(_beginthreadex(NULL, 0, ThreadTest_Unit, pParam, 0, NULL));

	return TRUE;
}

//=============================================================================
// Method		: StartOperation_AutoAll
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 15:55
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::StartOperation_AutoAll()
{
	if (FALSE == m_bFlag_ReadyTest)
	{
		MessageView(_T("Not int Test able mode. \nPlease Check the Initial setup and restart the Program."));
		return FALSE;
	}

	if (IsTesting_All())
	{
		if (m_Device.MotionSequence.m_Dlg_Popup.IsWindowVisible() == TRUE)
		{
			m_Device.MotionSequence.m_Dlg_Popup.OnBnClickedButtonOK();
			return TRUE;
		}

		return FALSE;
	}

	if (NULL != m_hThrTest_All)
	{
		CloseHandle(m_hThrTest_All);
		m_hThrTest_All = NULL;
	}

	// TestInfo UI Button Change
	OnChangeBtnState(FALSE);
	OnChangeStartBtnState(FALSE);

	m_bTestFinishMode = FALSE;
	m_bFlag_UseStop = FALSE;

	stThreadParam* pParam = new stThreadParam;
	pParam->pOwner = this;
	pParam->nIndex = 0;
	pParam->nArg_1 = 0;
	pParam->nArg_2 = 0;

	m_hThrTest_All = HANDLE(_beginthreadex(NULL, 0, ThreadZone_All, pParam, 0, NULL));

	return TRUE;
}

//=============================================================================
// Method		: StartOperation_Manual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2016/5/26 - 10:17
// Desc.		:
//=============================================================================
void CTestManager_EQP::StartOperation_Manual(__in UINT nUnitIdx)
{
	if (IsTesting_Unit(nUnitIdx))
	{
		MessageView(_T("Inspection is Running."));
		return;
	}

	if (NULL != m_hThrTest_Unit[nUnitIdx])
	{
		CloseHandle(m_hThrTest_Unit[nUnitIdx]);
		m_hThrTest_Unit[nUnitIdx] = NULL;
	}

	stThreadParam* pParam = new stThreadParam;
	pParam->pOwner = this;
	pParam->nIndex = nUnitIdx;
	pParam->nArg_1 = FALSE;
	pParam->nArg_2 = 0;

	m_hThrTest_Unit[nUnitIdx] = HANDLE(_beginthreadex(NULL, 0, ThreadTest_Unit, pParam, 0, NULL));
}

//=============================================================================
// Method		: ThreadTest_Unit
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2016/5/18 - 17:37
// Desc.		:
//=============================================================================
UINT WINAPI CTestManager_EQP::ThreadTest_Unit(__in LPVOID lParam)
{
	CTestManager_EQP* pThis		= (CTestManager_EQP*)((stThreadParam*)lParam)->pOwner;
	UINT	nUnitIdx			= ((stThreadParam*)lParam)->nIndex;
	UINT	nTestItemID			= ((stThreadParam*)lParam)->nArg_1;
	UINT	nThrType			= ((stThreadParam*)lParam)->nArg_2;

	if (NULL != lParam)
		delete lParam;

	pThis->AutomaticProcess(nUnitIdx, nTestItemID);
		
	//_endthreadex(0);
	return 0;
}

//=============================================================================
// Method		: ThreadZone_All
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2016/5/26 - 10:17
// Desc.		:
//=============================================================================
UINT WINAPI CTestManager_EQP::ThreadZone_All(__in LPVOID lParam)
{
	CTestManager_EQP* pThis = (CTestManager_EQP*)((stThreadParam*)lParam)->pOwner;
	UINT bUpdateFlash = ((stThreadParam*)lParam)->nArg_1;

	if (NULL != lParam)
		delete lParam;

	pThis->AutomaticProcess_All();

	return 0;
}

//=============================================================================
// Method		: AutomaticProcess
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in UINT nTestItemID
// Qualifier	:
// Last Update	: 2017/1/4 - 14:24
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::AutomaticProcess(__in UINT nUnitIdx, __in UINT nTestItemID)
{
	TRACE(_T("=-=-= 채널 테스트 작업 시작 =-=-=\n"));
	OnLog(_T("=-=-= %d 채널 테스트 작업 시작 =-=-="), nUnitIdx + 1);
	
	LRESULT lReturn = RCC_OK;
	__try
	{
		OnInitialTest_Unit(nUnitIdx);

		lReturn = OnStartTest_Unit(nUnitIdx, nTestItemID);
	}
	__finally
	{
		if (RCC_OK == lReturn)
			OnFinallyTest_Unit(nUnitIdx, TRUE);
		else
			OnFinallyTest_Unit(nUnitIdx, FALSE);
	}

	// 불량 코드
	if ((RCC_OK != lReturn) && (RCC_TestSkip != lReturn))
	{
		m_stInspInfo.CamInfo.ResultCode = lReturn;
	}

	// 오류 처리
	if (RCC_OK == lReturn)
	{
		TRACE(_T("=-=-= 채널 테스트 작업 완료 =-=-=\n"));
		OnLog(_T("=-=-= %d 채널 테스트 작업 완료 =-=-="), nUnitIdx + 1);
		return TRUE;
	}
	else
	{
		TRACE(_T("=-=-= 채널 테스트 작업 중지 =-=-=\n"));
		OnLog(_T("=-=-= %d 채널 테스트 작업 중지 : Code -> %d =-=-="), nUnitIdx + 1, lReturn);
		return FALSE;
	}
}

//=============================================================================
// Method		: AutomaticProcess_All
// Access		: protected  
// Returns		: void
// Parameter	: void
// Qualifier	:
// Last Update	: 2016/7/15 - 14:10
// Desc.		:
//=============================================================================
void CTestManager_EQP::AutomaticProcess_All()
{
	TRACE(_T("=-=-= Start All Test Operation =-=-=\n"));
	OnLog(_T("=-=-= Start All Test Operation =-=-="));

	LRESULT lReturn = RCAF_OK;

	__try
	{
		// 초기화
		OnInitialTest_All();

		lReturn = OnStartTest_All();
	}
	__finally
	{
		if (lReturn != RCAF_ModuleFix_Check_Err && lReturn != RCAF_ModuleFix_Check && lReturn != RCAF_Barcode_Empty_Err)
		{
			// 작업 종료 처리
			if (RCAF_OK == lReturn)
			{
				OnFinallyTest_All(TP_Completed);
				TRACE(_T("=-=-= All Test Operation Success =-=-=\n"));
				OnLog(_T("=-=-= All Test Operation Success =-=-="));
			}
			else
			{
				OnFinallyTest_All(TP_Stop);
				TRACE(_T("=-=-= All Test Operation Stop =-=-=\n"));
				OnLog(_T("=-=-= All Test Operation Stop =-=-="));
			}

			// 검사 판정
			OnJugdement_And_Report();

			// 에러 코드 처리
			OnPopupMessageTest_All((enResultCode_AF)lReturn);
		}
		else{
			OnFinallyTest_All(TP_Stop, TRUE);
			OnPopupMessageTest_All((enResultCode_AF)lReturn);
		}

		m_Device.MotionSequence.ButtonLampControl(DO_StartBtnL_Lamp, IO_SignalT_ToggleStart);
		m_Device.MotionSequence.ButtonLampControl(DO_StartBtnR_Lamp, IO_SignalT_ToggleStart);
	}

	//m_stInspInfo.szBarcodeBuf.Empty();
	m_stInspInfo.ResetBarcodeBuffer();
}

//=============================================================================
// Method		: StopProcess
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2016/5/26 - 10:17
// Desc.		:
//=============================================================================
void CTestManager_EQP::StopProcess(__in UINT nUnitIdx)
{
	if (NULL != m_hThrTest_Unit[nUnitIdx])
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_Unit[nUnitIdx], &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_hThrTest_Unit[nUnitIdx], dwExitCode);
			WaitForSingleObject(m_hThrTest_Unit[nUnitIdx], WAIT_ABANDONED);
			CloseHandle(m_hThrTest_Unit[nUnitIdx]);
			m_hThrTest_Unit[nUnitIdx] = NULL;
		}
	}
}

void CTestManager_EQP::StopProcess_All()
{
	m_Device.MotionSequence.ReleaseCYLMotion();

	OnTestProgress(TP_Error);
	m_stInspInfo.ModelInfo.nPicViewMode	 = PIC_Standby;
	
	//m_bFlag_UseStop	= TRUE;

	for (UINT nIdx = 0; nIdx < TIID_MaxEnum; nIdx++)
		m_bFlag_TestItem[nIdx] = FALSE;

	UINT nTestItemID = TIID_Base;
	UINT nTestResult = 0;
	INT_PTR iTestCnt = m_stInspInfo.ModelInfo.TestItemz.GetCount();
	CString str;

	for (INT_PTR iIdx = 0; iIdx < iTestCnt; iIdx++)
	{
		nTestItemID = m_stInspInfo.ModelInfo.TestItemz.GetAt(iIdx);
		switch (nTestItemID)
		{
		case TIID_Current:
			str.Format(_T("%.1fmA"), m_stInspInfo.ModelInfo.stCurrent.stCurrentData.nCurrent);
			OnSetTestCurrent((enTestEachResult)m_stInspInfo.ModelInfo.stCurrent.stCurrentData.nResult, str);
			break;

		case TIID_EIAJ:
			break;

		case TIID_CenterPointAdj:
			break;

		case TIID_ActiveAlgin:
			str.Format(_T("(X:%d, Y:%d), %d / %d"), 
				m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_X,
				m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_Y,
				m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.nResult_cnt,
				m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.nTotal_cnt
			//	m_stInspInfo.ModelInfo.stSFR.stSFRData.nPassCnt,
			//	m_stInspInfo.ModelInfo.stSFR.stSFRData.nTotalCnt
				);

			nTestResult = m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.nResult && m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.nResult;
			OnSetTestActiveAlgin((enTestEachResult)nTestResult, str);
			break;

		case TIID_Particle:
			break;

		default:
			break;
		}
	}
	
	CameraTestInitialize(Finalize);

}

//=============================================================================
// Method		: OnInitTest_All
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 10:25
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitialTest_All()
{
	// 테스트 상태 : Run	
	OnTestProgress(TP_Run);	
	OnSetResetSiteInfo();

	// Test 정보 초기화
	m_stInspInfo.CamInfo.Reset();
	m_stInspInfo.ModelInfo.stCurrent.stCurrentData.reset();
	m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.reset();
	m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.reset();

	m_stInspInfo.Time.Reset();
	GetLocalTime(&m_stInspInfo.Time.tmStart_All);
	m_stInspInfo.Time.dwStart_All = timeGetTime();

	// 채널 카메라 데이터 초기화
	m_stInspInfo.WorklistInfo.Reset();

	// 타워램프 RUN 중 표시
	m_Device.MotionSequence.TowerLamp(TowerLampYellow, ON);

	// START 버튼 토글 정지
	m_Device.MotionSequence.ButtonLampControl(DO_StartBtnL_Lamp, IO_SignalT_ToggleStop);
	m_Device.MotionSequence.ButtonLampControl(DO_StartBtnR_Lamp, IO_SignalT_ToggleStop);

	if (m_stInspInfo.PermissionMode == Permission_MES)
	{
		m_stInspInfo.MESInfo.szEqpCode = m_stOption.Inspector.EqpCode;
		m_stInspInfo.MESInfo.szMESPath = m_stInspInfo.Path.szMes;

		if (m_stInspInfo.MESInfo.GetMESParameter(m_stOption.Inspector.EqpCode))
		{
			OnChangeLotInfo(MESMode);
		}
	}

	m_bFlag_FocusTest = TRUE;
	//m_bFlag_AutoFocus = TRUE;
	//m_bFlag_ActiveAlginTest = TRUE;
}

void CTestManager_EQP::OnFinallyTest_All(__in enTestProcess TestProc, BOOL bMotionNoMove /*= FALSE*/)
{
	for (UINT nIdx = 0; nIdx < TIID_MaxEnum; nIdx++)
		m_bFlag_TestItem[nIdx] = FALSE;

	GetLocalTime(&m_stInspInfo.Time.tmEnd_All);
	OnTestProgress(TestProc);

	// TestInfo UI Button Change
	OnChangeBtnState(TRUE);
	OnChangeStartBtnState(TRUE);

	// 테스트 상태 : 종료

	if (bMotionNoMove == FALSE)
	{
		m_stInspInfo.ModelInfo.nPicViewMode = PIC_Standby;
		CameraTestInitialize(Finalize);

		//m_Device.MotionSequence.ModuleFixUnFixCYLMotion(UNFIX);


		//- 정상검사 종료 및 비정상 종료 모두 stage release
		// 보드 연결 상태 확인
		if (!m_Device.MotionSequence.m_pDigitalIOCtrl->AXTState())
		{
			m_Device.MotionSequence.m_Log.LogMsg_Err(_T("Disconnect IoBoard....."));
			return ;
		}
		else if (m_Device.DigitalIOCtrl.GetInStatus(DI_DriverOutCYLSensor) == FALSE || m_Device.DigitalIOCtrl.GetInStatus(DI_PCBUnFixCYLSensor) == FALSE)
		{
			MoveStage(FALSE);
		}
	}
}

//=============================================================================
// Method		: OnInitialTest_Unit
// Access		: virtual protected  
// Returns		: voidnPicViewMode
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2016/6/12 - 15:03
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitialTest_Unit(__in UINT nUnitIdx)
{	
	OnTestProgress(TP_Run);
	GetLocalTime(&m_stInspInfo.Time.tmStart_Unit[nUnitIdx]);
	m_stInspInfo.Time.dwStart[nUnitIdx] = timeGetTime();

	//m_Site[nUnitIdx].OnSetTestStatus(m_stInspInfo.CamObject[nUnitIdx]->nProgressStatus);
	
}

//=============================================================================
// Method		: OnFinallyTest_Unit
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in BOOL bResult
// Qualifier	:
// Last Update	: 2016/6/12 - 15:03
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFinallyTest_Unit(__in UINT nUnitIdx, __in BOOL bResult)
{
	GetLocalTime(&m_stInspInfo.Time.tmEnd_Unit[nUnitIdx]);

	if (bResult)
	{
		//OnTestProgress_Unit(nUnitIdx, TP_Completed);
		//OnTestResult_Unit(nUnitIdx, TR_Pass);
	}
	else
	{
		//OnTestProgress_Unit(nUnitIdx, TP_Stop);
		//OnTestResult_Unit(nUnitIdx, TR_Fail);
	}
	//m_Site[nUnitIdx].OnSetTestStatus(m_stInspInfo.CamObject[nUnitIdx]->nProgressStatus);
	
	// 실제 테스트 시간 누적
	//m_stInspInfo.CamInfo[nUnitIdx].dwTestTime += m_stInspInfo.Time.dwDuration[nUnitIdx];

	// Site별 검사 시간
	if (1000 < m_stInspInfo.Time.dwDuration[nUnitIdx])
	{
		m_stInspInfo.CycleTime.AddTestTimeSite(nUnitIdx, m_stInspInfo.Time.dwDuration[nUnitIdx]);

		// 테스트 항목별 검사 시간??


	}
}

//=============================================================================
// Method		: OnStartTest_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: void
// Qualifier	:
// Last Update	: 2016/7/15 - 14:17
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStartTest_All()
{
	LRESULT lReturn = RCAF_OK;

	// 데이터 초기화
	OnResetCamInfo();

	// Indicator Limit 체크
	if (m_stInspInfo.ModelInfo.fIndicator_Std_X + m_stInspInfo.ModelInfo.fIndicator_Spec_X < m_fValue[IndicatorX] ||
		m_stInspInfo.ModelInfo.fIndicator_Std_X - m_stInspInfo.ModelInfo.fIndicator_Spec_X > m_fValue[IndicatorX] ||
		m_stInspInfo.ModelInfo.fIndicator_Std_Y + m_stInspInfo.ModelInfo.fIndicator_Spec_Y < m_fValue[IndicatorY] ||
		m_stInspInfo.ModelInfo.fIndicator_Std_Y - m_stInspInfo.ModelInfo.fIndicator_Spec_Y > m_fValue[IndicatorY] )
	{
		m_stInspInfo.CamInfo.nJudgment = TR_MachineCheck;
		MessageView(_T("Indicator limit err"));
		return RCAF_Indicator_Limit_Err;
	}

	// ModuleFix Flag, Sensor 체크
	if (m_stInspInfo.ModelInfo.bModuleFixCheck == TRUE && m_Device.DigitalIOCtrl.GetInStatus(DI_ModuleFixCYLSensor) == FALSE)
	{
		if (!MessageView(_T("Module Fix Check Err"), FALSE, TRUE)){
			m_stInspInfo.CamInfo.nJudgment = TR_MachineCheck;
			return RCAF_ModuleFix_Check_Err;
		}
		else
		{
			//m_Device.MotionSequence.ModuleFixUnFixCYLMotion(FIX);
			m_stInspInfo.CamInfo.nJudgment = TR_MachineCheck;
			return RCAF_ModuleFix_Check;
		}
	}

	// LOT 체크
	if (FALSE == CheckLOTInfo())
	{
		m_stInspInfo.CamInfo.nJudgment = TR_Init;
		return RCAF_LotID_Empty_Err;
	}

	// 모델 체크
	if (FALSE == CheckModelInfo())
	{
		m_stInspInfo.CamInfo.nJudgment = TR_Init;
		return RCAF_Model_Err;
	}

 	// 바코드 설정
	if (FALSE == CheckBarCodeInfo())
	{
		m_stInspInfo.CamInfo.nJudgment = TR_Init;
		return RCAF_Barcode_Empty_Err;
	}

	// pogo
	if (FALSE == CheckPogoCount())
	{
		m_stInspInfo.CamInfo.nJudgment = TR_Init;
		return RCAF_MaxPogoCount;
	}

	// DriverCount
	if (FALSE == CheckDriverCount())
	{
		m_stInspInfo.CamInfo.nJudgment = TR_Init;
		return RCAF_MaxDriverCount;
	}
	
	// 투입시간 설정
	OnSetInputTime();

	//  [12/24/2018 ysJang] MoveStage condition
	// 스테이지 인 , 그립퍼 인
	if (m_stInspInfo.ModelInfo.bMoveStageCheck == TRUE)
	{
		if (MoveStage(TRUE))
		{
			//  [1/25/2019 ysJang] 드라이버 카운터 상승
			IncreaseDriverCount();
		}
		else
		{
			m_stInspInfo.CamInfo.nJudgment = TR_MachineCheck;
			return RCAF_MoveStageErr;
		}
	}
		

	// 1. PowerOn
	lReturn = OnStartTest_Unit(0, TIID_TestInitialize);
	
	if (lReturn != RCAF_OK)
	{
		m_stInspInfo.SetOutputTime();
		OnSetCycleTime();

		lReturn = RCAF_PowerOn_Err;

		// 전원 On / 카메라 영상 On Error 시 Report 안 남긴다.
		m_stInspInfo.CamInfo.nJudgment = TR_Init;
		return lReturn;
	}

	// 선택한 검사 항목 개수
	INT_PTR iTestCnt = m_stInspInfo.ModelInfo.TestItemz.GetCount();

	if (iTestCnt <= USE_TEST_ITEM_CNT)
	{
		for (UINT nIdx = 0; nIdx < (UINT)iTestCnt; nIdx++)
		{
			lReturn = OnStartTest_Unit(0, m_stInspInfo.ModelInfo.TestItemz.GetAt(nIdx));
			
			if (lReturn != RCAF_OK)
			{
				m_stInspInfo.SetOutputTime();
				OnSetCycleTime();

				return lReturn;
			}

			//	다음 테스트 안정화 시간
			Sleep(500);
		}
	}

 	// 시간 체크, KHO 확인 필요
	m_stInspInfo.SetOutputTime();
	OnSetCycleTime();

	return lReturn;
}

//=============================================================================
// Method		: OnStartTest_All_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Qualifier	:
// Last Update	: 2017/1/4 - 14:28
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStartTest_All_TestItem(__in UINT nUnitIdx, __in UINT nTestItemID)
{
	LRESULT lReturn = RCAF_OK;

	//  개별 검사 쓰레드 실행
	HANDLE	hEventz[USE_CHANNEL_CNT] = { NULL, };
	UINT	nIndex = 0;
	// 채널별 검사 사용여부
	// 제품 초기화 상태이거나 검사 진행이 Pass 상태이면 다음 검사 계속 진행
	if ((TR_Pass == m_stInspInfo.CamInfo.nJudgment) || (TR_Init == m_stInspInfo.CamInfo.nJudgment))
	{
		if (StartOperation_Auto(nUnitIdx, nTestItemID))
		{
			hEventz[nIndex++] = m_hThrTest_Unit[nUnitIdx];
		}
	}

	// 모든 채널의 검사 쓰레드가 종료 될때까지 대기
// 	DWORD dwEvent = WaitForMultipleObjects(nIndex, hEventz, TRUE, 300000);
// 	if (WAIT_TIMEOUT == dwEvent)
// 	{
// 		; // 타임 아웃 처리?
// 	}
// 	else if (WAIT_FAILED == dwEvent)
// 	{
// 		;
// 	}

	return lReturn;
}

//=============================================================================
// Method		: OnStartTest_Unit
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in UINT nTestItemID
// Qualifier	:
// Last Update	: 2017/1/4 - 14:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStartTest_Unit(__in UINT nUnitIdx, __in UINT nTestItemID)
{
	LRESULT lReturn = RCAF_OK;

	switch (nTestItemID)
	{
	case TIID_TestInitialize:	
		CameraTestInitialize(Initial);
		if (m_stInspInfo.CamInfo.nJudgmentInitial == TER_Fail)
		{
			lReturn = RCAF_PowerOn_Err;
		}
		break;

	case TIID_TestFinalize:		
		CameraTestInitialize(Finalize);
		if (m_stInspInfo.CamInfo.nJudgmentFinalize == TER_Fail)
		{
			lReturn = RCAF_PowerOff_Err;
		}
		break;

	case TIID_Current:			// 전류 측정
		CameraTestCurrent();
		if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
		{
			lReturn = RCAF_Current_Err;
		}
		break;

	case TIID_ActiveAlgin:		// 해상력, 광축 측정
		CameraTestActiveAlign();
		if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
		{
			lReturn = RCAF_LensInspErr;
		}
		else if (m_stInspInfo.CamInfo.nJudgment == TR_NoImage)
		{
			lReturn = RCAF_NoImage_Err;
		}
		else if (m_stInspInfo.CamInfo.nJudgment == TR_CamStateFail)
		{
			lReturn = RCAF_CamState_Err;
		}

		switch (m_stOption.Inspector.nImageSaveType)
		{
		case SaveType_All:
			ImageSave(nTestItemID);
			break;
		case SaveType_OnlyPass:
			if (lReturn == RCAF_OK)
				ImageSave(nTestItemID);
			break;
		case SaveType_OnlyFail:
			if (lReturn != RCAF_OK)
				ImageSave(nTestItemID);
			break;
		case SaveType_NotSave:
			break;
		default:
			break;
		}
		
		break;
	default:
		break;

	}

	if (m_bFlag_UseStop)
		lReturn = RCAF_UserStop;

	return lReturn;
}

//=============================================================================
// Method		: OnStartTest_Unit_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Qualifier	:
// Last Update	: 2017/1/8 - 16:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStartTest_Unit_TestItem(__in UINT nTestItemID)
{
	LRESULT lReturn = RCC_OK;

	for (UINT nIndex = 0; nIndex < USE_CHANNEL_CNT; nIndex++)
	{
		if (m_stInspInfo.bTestEnable[nIndex])
		{
			switch (nTestItemID)
			{
			case TIID_TestInitialize:
				// 
				break;

			case TIID_TestFinalize:
				//
				break;

			case TIID_Current:			// 전류 측정
				// 전류 측정 검사 수행

				break;

			case TIID_CenterPointAdj:	// 광축 조정
				// 광축 조정 검사 수행

				break;

			default:
				break;
			}
		}
	}
	return lReturn;
}

//=============================================================================
// Method		: OnPopupMessageTest_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode_All nResultCode
// Qualifier	:
// Last Update	: 2016/7/21 - 18:59
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnPopupMessageTest_All(__in enResultCode_AF nResultCode)
{
	if (RCAF_OK == nResultCode)
		return;

	OnLog_Err(g_szResultCode_AF[nResultCode]);
	OnTestErrCode(g_szResultCode_AF[nResultCode]);
}

//=============================================================================
// Method		: OnCheckSaftyJIG
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/6/13 - 21:33
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnCheckSaftyJIG()
{
	return TRUE;
}

//=============================================================================
// Method		: OnMakeReport
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 14:25
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnJugdement_And_Report()
{
	CString path = m_Worklist.Get_Report_Yield_FullPath(m_stInspInfo.Path.szReport, m_stInspInfo.szModelName, m_stInspInfo.LotInfo.szLotName, &m_stInspInfo.CamInfo.tmInputTime);

	// 최종 검사 판정 업데이트?
	switch (m_stInspInfo.CamInfo.nJudgment)
	{
	case TR_Fail:
	//case TR_Init:
		OnTestResult(TR_Fail);
		OnSetResult(TR_Fail); // Judgement UI Set
		

		m_Device.MotionSequence.TowerLamp(TowerLampRed, ON);
		// Tower Lamp Buzzer 사용 유무
		if (m_stOption.Inspector.bUseTowerLampBuzzer == TRUE)
			m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLampBuzzer, ON);
		
		m_stInspInfo.YieldInfo.IncreaseFail();

		if (m_stInspInfo.LotInfo.bLotStatus == TRUE) //LOT 모드 일 경우에만 WORKLIST 저장
		{
			++m_stInspInfo.LotInfo.nLotCount;
			OnSetLotInfo();

			m_FileReport.SaveYield_LOT(path, &m_stInspInfo.YieldInfo);

			OnUpdateYield();
		}

		OnChangWorklistData();

		// Fail Box 루틴 추가.
		if (m_stInspInfo.LotInfo.bLotStatus == TRUE && m_stOption.Inspector.bUseFailBox == TRUE) //LOT 모드 일 경우에만 WORKLIST 저장
		{
			//m_Device.MotionSequence.ModuleFixUnFixCYLMotion(UNFIX);
			OnPopupFailBoxConfirm();
		}

		break;

	case TR_Pass:
		OnTestResult(TR_Pass);
		OnSetResult(TR_Pass);
		
		m_Device.MotionSequence.TowerLamp(TowerLampGreen, ON);
		//m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLampBuzzer, ON);

		m_stInspInfo.YieldInfo.IncreasePass();
		
		if (m_stInspInfo.LotInfo.bLotStatus == TRUE) //LOT 모드 일 경우에만 WORKLIST 저장
		{
			++m_stInspInfo.LotInfo.nLotCount;
			OnSetLotInfo();

			m_FileReport.SaveYield_LOT(path, &m_stInspInfo.YieldInfo);

			OnUpdateYield();
		}
		OnChangWorklistData();
		break;

	case TR_Empty:
	case TR_Skip:
	case TR_Init:
		break;

	case TR_Rework:
	case TR_UserStop:
		OnTestResult(TR_UserStop);
		OnSetResult(TR_UserStop);
		OnChangWorklistData();
		break;
	case TR_MachineCheck:
		OnTestResult(TR_MachineCheck);
		OnSetResult(TR_MachineCheck);
		break;
	case TR_CamStateFail:
		OnTestResult(TR_CamStateFail);
		OnSetResult(TR_CamStateFail);
		break;
	case TR_Timeout:
		break;

	default:
		break;
	}

}

//=============================================================================
// Method		: OnSetCycleTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/14 - 14:28
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSetCycleTime()
{
	m_stInspInfo.CycleTime.IncreaseOutputCount();

	if (0 < m_stInspInfo.dwTactTime)
	{
		m_stInspInfo.CycleTime.AddTactTime(m_stInspInfo.dwTactTime);
	}

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		if (m_stInspInfo.bTestEnable[nIdx])
		{
			m_stInspInfo.CycleTime.AddCycleTime(m_stInspInfo.dwTactTime);
		}
	}

	// UI 갱신
}

//=============================================================================
// Method		: OnResetCamInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/7/15 - 14:30
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnResetCamInfo()
{
	m_stInspInfo.ResetCamInfo();
}

//=============================================================================
// Method		: CheckModelInfo
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/7/21 - 11:48
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckModelInfo()
{
	enPermissionMode enAcessMode = GetPermissionMode();

	if (enAcessMode != Permission_Operator)
		return TRUE;

	if (m_stInspInfo.ModelInfo.szModelFile.IsEmpty())
	{
		MessageView(g_szResultCode_AF[RCAF_Model_Empty_Err]);
		OnLog_Err(_T("Not Model Check."));
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: CheckLOTInfo
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/21 - 16:40
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckLOTInfo()
{
	enPermissionMode enAcessMode = GetPermissionMode();

	if (enAcessMode != Permission_Operator)
		return TRUE;

	if (m_stInspInfo.LotInfo.szLotName.IsEmpty())
	{
		MessageView(g_szResultCode_AF[RCAF_LotID_Empty_Err]);
		OnLog_Err(_T("Not Lot ID Check."));
		return FALSE;
	}

	if (m_stInspInfo.LotInfo.szOperator.IsEmpty())
	{
		MessageView(g_szResultCode_AF[RCAF_Operator_Empty_Err]);
		OnLog_Err(_T("Not Operator Check."));
		return FALSE;
	}

	return TRUE;
}


//=============================================================================
// Method		: CheckMasterSetInfo
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/24 - 9:35
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckMasterSetInfo()
{
	enPermissionMode enAcessMode = GetPermissionMode();

	if (enAcessMode != Permission_Operator)
		return TRUE;

	if (m_bFlag_MasterSet == FALSE)
	{
		//MessageView(g_szResultCode_AF[RC3_MasterSet_Err]);
		//OnLog_Err(_T("Model Cheang Empty MasterSet"));
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: CheckBarCodeInfo
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/21 - 16:48
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckBarCodeInfo()
{
	enPermissionMode enAcessMode = GetPermissionMode();

	if (enAcessMode != Permission_Operator)
	{
		m_stInspInfo.CamInfo.szBarcode = _T("Administrator Mode");
		OnSetInsertBarcode(m_stInspInfo.CamInfo.szBarcode);
		return TRUE;
	}

	// 바코드 설정
	if (m_stInspInfo.szBarcodeBuf.IsEmpty() && m_stOption.Inspector.bUseBarcode == TRUE)
	{
		MessageView(g_szResultCode_AF[RCAF_Barcode_Empty_Err]);
		OnLog_Err(_T("Barcode is Empty."));
		return FALSE;
	}
	else if (!m_stInspInfo.szBarcodeBuf.IsEmpty() && m_stOption.Inspector.bUseBarcode == TRUE)
	{
		m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.szBarcodeBuf;
		OnSetInsertBarcode(m_stInspInfo.CamInfo.szBarcode);
	}
	else
	{
		m_stInspInfo.CamInfo.szBarcode = _T("No Barcode");
		OnSetInsertBarcode(m_stInspInfo.CamInfo.szBarcode);
		//m_stInspInfo.szBarcodeBuf.Empty();
	}

	return TRUE;
}

//=============================================================================
// Method		: CheckEASCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/11/8 - 11:20
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckEASCount()
{
	return TRUE;
}

//=============================================================================
// Method		: CreateTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 14:20
// Desc.		:
//=============================================================================
void CTestManager_EQP::CreateTimer_UpdateUI()
{
	__try
	{
		if (NULL == m_hTimer_Update_UI)
		if (!CreateTimerQueueTimer(&m_hTimer_Update_UI, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutine_UpdateUI, (PVOID)this, 5000, 100, WT_EXECUTEDEFAULT))
		{
			TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CTestManager_Base::CreateTimer_UpdateUI()\n"));
	}
}

//=============================================================================
// Method		: OnMonitor_TimeCheck
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 13:28
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnMonitor_TimeCheck()
{
	m_dwTimeCheck = timeGetTime();

	// 전체 검사 시간 체크
	if (TP_Run == m_stInspInfo.GetTestStatus())
	{
		if (m_stInspInfo.Time.dwStart_All < m_dwTimeCheck)
			m_stInspInfo.Time.dwDuration_All = m_dwTimeCheck - m_stInspInfo.Time.dwStart_All;
		else
			m_stInspInfo.Time.dwDuration_All = 0xFFFFFFFF - m_stInspInfo.Time.dwStart_All + m_dwTimeCheck;

		OnUpdateElapsedTime_All(m_stInspInfo.Time.dwDuration_All);
	}
}

//=============================================================================
// Method		: OnMonitor_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		: Digital In 신호 체크
//=============================================================================
void CTestManager_EQP::OnMonitor_UpdateUI()
{
	if (m_bExitFlag == TRUE)
		return;

	if (USE_TEST_MODE)
	{
		if (!m_TIProcessing.CameraConnect())
			DisplayVideo_NoSignal(0);

		if (m_Device.DigitalIOCtrl.AXTState() == TRUE)
		{
			for (BYTE nBitOffset = 0; nBitOffset < DI_NotUseBit_Max; nBitOffset++)
			{
				BOOL bStatus = m_Device.DigitalIOCtrl.GetInStatus(nBitOffset);

				if (bStatus != m_stInspInfo.bDigitalIn[nBitOffset])
				{
					m_stInspInfo.bDigitalIn[nBitOffset] = bStatus;
					OnDetectDigitalInSignal(nBitOffset, bStatus);
				}
			}
		}
	}
}

//=============================================================================
// Method		: OnTestProgress
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestProcess nProcess
// Qualifier	:
// Last Update	: 2016/5/30 - 10:25
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnTestProgress(__in enTestProcess nProcess)
{
	m_stInspInfo.SetTestStatus(nProcess);
	m_stInspInfo.CamInfo.nProgressStatus = nProcess;
}

void CTestManager_EQP::OnTestFailResultCode_Unit(__in UINT nUnitIdx, __in enResultCode_TestItem nFailCode)
{
	//m_stInspInfo.CamObject[nUnitIdx].RomWriteCode = nFailCode;
}

//=============================================================================
// Method		: OnTestResult
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestResult nResult
// Qualifier	:
// Last Update	: 2017/2/21 - 17:10
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnTestResult(__in enTestResult nResult)
{
	m_stInspInfo.CamInfo.nJudgment = nResult;
	m_stInspInfo.SiteInfo.SetResult(nResult);
}

//=============================================================================
// Method		: OnSetInputTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/2 - 13:16
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSetInputTime()
{
	m_stInspInfo.SetInputTime();
}

//=============================================================================
// Method		: IncreasePogoCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/7/21 - 11:27
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IncreasePogoCount()
{
	OnSetStatus_PogoCount();
	
	return SavePogoCount();
}

//=============================================================================
// Method		: SavePogoCount
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in enPogoCntIndex nPogoIndex
// Qualifier	:
// Last Update	: 2016/6/22 - 14:52
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::SavePogoCount()
{
	CFile_Model	m_fileModel;
	CString		strFullPath;
	CString		strLog;
	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szPogo, m_stInspInfo.ModelInfo.szPogoName, POGO_FILE_EXT);

	if (FALSE == m_fileModel.SavePogoCount(strFullPath, 0, m_stInspInfo.PogoInfo.dwCount[0]))
	{
		strLog.Format(_T("Pogo INI File Save Fail."));
		OnLog_Err(strLog);
		MessageView(strLog);

		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: LoadPogoCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/20 - 17:13
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::LoadPogoCount()
{
	CFile_Model	m_fileModel;
	CString		strFullPath;
	CString		strLog;
	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szPogo, m_stInspInfo.ModelInfo.szPogoName, POGO_FILE_EXT);

	if (FALSE == m_fileModel.LoadPogoIniFile(strFullPath, m_stInspInfo.PogoInfo))
	{
		strLog.Format(_T("Cannot load Pogo File."));
		OnLog_Err(strLog);
		MessageView(strLog);
		
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: CheckPogoCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/21 - 16:42
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckPogoCount()
{
	// POGO 카운트 값이 최대치에 도달했으면 경고 창 팝업
	if (m_stInspInfo.PogoInfo.IsMaxCount(0))
	{	
		MessageView(_T("Pogo Count Max.\r\n Engineer Call."));
		return FALSE;
	}

	return TRUE;
}

//  [1/25/2019 ysJang]
//=============================================================================
// Method		: IncreaseDriverCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/7/21 - 11:27
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IncreaseDriverCount()
{
	OnSetStatus_DriverCount();

	return SaveDriverCount();
}

//=============================================================================
// Method		: SaveDriverCount
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in enDriverCntIndex nDriverIndex
// Qualifier	:
// Last Update	: 2016/6/22 - 14:52
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::SaveDriverCount()
{
	CFile_Model	m_fileModel;
	CString		strFullPath;
	CString		strLog;
	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szDriverCount, m_stInspInfo.ModelInfo.szDriverCountName, DRIVERCOUNT_FILE_EXT);

	if (FALSE == m_fileModel.SaveDriverCount(strFullPath, 0, m_stInspInfo.DriverCountInfo.dwCount[0]))
	{
		strLog.Format(_T("Driver Count INI File Save Fail."));
		OnLog_Err(strLog);
		MessageView(strLog);

		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: LoadDriverCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/20 - 17:13
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::LoadDriverCount()
{
	CFile_Model	m_fileModel;
	CString		strFullPath;
	CString		strLog;
	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szDriverCount, m_stInspInfo.ModelInfo.szDriverCountName, DRIVERCOUNT_FILE_EXT);

	if (FALSE == m_fileModel.LoadDriverCountIniFile(strFullPath, m_stInspInfo.DriverCountInfo))
	{
		strLog.Format(_T("DriverCount 파일을 불러 올 수 없습니다."));
		OnLog_Err(strLog);
		MessageView(strLog);

		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: CheckDriverCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/21 - 16:42
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::CheckDriverCount()
{
	// Driver 카운트 값이 최대치에 도달했으면 경고 창 팝업
	if (m_stInspInfo.DriverCountInfo.IsMaxCount(0))
	{
		MessageView(_T("Driver Count Max.\r\n Engineer Call."));
		return FALSE;
	}

	return TRUE;
}


//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitialize()
{
	//__super::OnInitialize();	

	CreateTimerQueue_Mon_Indicator();
	CreateTimer_TimeCheck();
	CreateTimer_UpdateUI();
	CreateTimer_UpdateIndicatorDisplay();
}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFinalize()
{
	//__super::OnFinalize();

	DeleteTimer_TimeCheck();
	DeleteTimer_UpdateUI();
	DeleteTimer_UpdateIndicatorDisplay();
	DeleteTimerQueue_Mon_Indicator();
}

//=============================================================================
// Method		: IsTesting
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/5/16 - 10:57
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting()
{
	if (NULL != m_hThrTest_All)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_All, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return TRUE;
		}
	}

	for (UINT nUnitIdx = 0; nUnitIdx < MAX_OPERATION_THREAD; nUnitIdx++)
	{
		if (IsTesting_Unit(nUnitIdx))
			return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: IsTesting_All
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/6/9 - 11:52
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting_All()
{
	if (NULL != m_hThrTest_All)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_All, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return TRUE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: IsTesting_Unit
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2016/5/16 - 10:57
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting_Unit(__in UINT nUnitIdx)
{
	if (NULL != m_hThrTest_Unit[nUnitIdx])
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_Unit[nUnitIdx], &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return TRUE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: IsTesting_Exc
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nExcUnitIdx
// Qualifier	:
// Last Update	: 2016/6/30 - 16:59
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting_Exc(__in UINT nExcUnitIdx)
{
	DWORD dwExitCode = NULL;
	GetExitCodeThread(m_hThrTest_All, &dwExitCode);
	if (STILL_ACTIVE == dwExitCode)
	{
		return TRUE;
	}

	for (UINT nUnitIdx = 0; nUnitIdx < MAX_OPERATION_THREAD; nUnitIdx++)
	{
		if (nUnitIdx != nExcUnitIdx)
		{
			if (IsTesting_Unit(nUnitIdx))
				return TRUE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: IsManagerMode
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/1/13 - 10:32
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsManagerMode()
{
	if (Permission_Manager == m_stInspInfo.PermissionMode)
		return TRUE;
	else
		return FALSE;
}

//=============================================================================
// Method		: GetPermissionMode
// Access		: public  
// Returns		: enPermissionMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
enPermissionMode CTestManager_EQP::GetPermissionMode()
{
	return m_stInspInfo.PermissionMode;
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
void CTestManager_EQP::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	m_stInspInfo.PermissionMode = nAcessMode;
}

void CTestManager_EQP::PermissionStatsView()
{
}

BOOL CTestManager_EQP::CameraTestInitialize(BOOL bMode)
{
	enTestEachResult enResult;
	
	if (bMode == Initial)
	{
		// 카메라 ON
		enResult = (enTestEachResult)m_TIProcessing.CameraPowerOnOff(ON);
		m_stInspInfo.CamInfo.nJudgmentInitial = enResult;

		OnSetTestInitialize(enResult, g_TestEachResult[enResult].szText);
	}
	else if (bMode == Finalize)
	{
		// 카메라 OFF
		enResult = (enTestEachResult)m_TIProcessing.CameraPowerOnOff(OFF);
		m_stInspInfo.CamInfo.nJudgmentFinalize = enResult;

		OnSetTestFinalize(enResult, g_TestEachResult[enResult].szText);

		m_stInspInfo.ModelInfo.nPicViewMode = PIC_Standby;
	}

	// 이전 TEST 최종 결과가 PASS 
	if (m_stInspInfo.CamInfo.nJudgment != TR_UserStop && 
		m_stInspInfo.CamInfo.nJudgment != TR_CamStateFail && 
		m_stInspInfo.CamInfo.nJudgment != TR_MachineCheck)
	{
		if (m_stInspInfo.CamInfo.nJudgment == TR_Pass || m_stInspInfo.CamInfo.nJudgment == TR_Init)
		{
			if (bMode == Finalize)
			{
				if (m_stInspInfo.CamInfo.nJudgment == TR_Init)
				{
					m_stInspInfo.CamInfo.nJudgment = TR_Fail;
				}
				else
				{
					if (enResult == TER_Pass)
						m_stInspInfo.CamInfo.nJudgment = TR_Pass;
				}
				
			}
			else{
				if (enResult == TER_Pass)
					m_stInspInfo.CamInfo.nJudgment = TR_Pass;

				if (enResult == TER_Fail)
					m_stInspInfo.CamInfo.nJudgment = TR_Fail;
			}
		}
		else
		{
			m_stInspInfo.CamInfo.nJudgment = TR_Fail;
		}
	}

	return TRUE;
}

BOOL CTestManager_EQP::CameraTestCurrent()
{
	enTestEachResult enResult;
	CString	strText;
	INT_PTR iCnt = m_stInspInfo.ModelInfo.TestItemz.GetCount();

	m_bFlag_TestItem[TIID_Current] = TRUE;

	if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		//TEST SKIP
		m_bFlag_TestItem[TIID_Current] = FALSE;
		m_stInspInfo.ModelInfo.stCurrent.stCurrentData.nResult = TER_SKIP;
		OnSetTestCurrent(TER_SKIP, _T("TEST SKIP")); 
		return FALSE;
	}

	enResult = (enTestEachResult)m_TIProcessing.CameraCurrent();

	//m_stInspInfo.CamInfo.stCurrentData.nCurrent = m_stInspInfo.ModelInfo.stCurrent.stCurrentData.nCurrent;
	strText.Format(_T("%.1fmA"), m_stInspInfo.ModelInfo.stCurrent.stCurrentData.nCurrent);

	if (iCnt <= USE_TEST_ITEM_CNT)
	{
		for (UINT nIdx = 0; nIdx < (UINT)iCnt; nIdx++)
		{
			// TEST 아이템이 있는지 확인 후 UI 출력
			if (m_stInspInfo.ModelInfo.TestItemz.GetAt(nIdx) == TIID_Current)
				OnSetTestCurrent((enTestEachResult)m_stInspInfo.ModelInfo.stCurrent.stCurrentData.nResult, strText);
		}
	}

	OnSetResult((enTestResult)m_stInspInfo.ModelInfo.stCurrent.stCurrentData.nResult, strText);

	if (enResult == TER_Pass)
		m_stInspInfo.CamInfo.nJudgment = TR_Pass;

	if (enResult == TER_Fail)
		m_stInspInfo.CamInfo.nJudgment = TR_Fail;

	m_bFlag_TestItem[TIID_Current] = FALSE;

	return TRUE;
}

BOOL CTestManager_EQP::CameraTestActiveAlign()
{
	CString	strText;
	INT_PTR iCnt = m_stInspInfo.ModelInfo.TestItemz.GetCount();
	auto enResult = TER_Fail;

	m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.reset();
	m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.reset();

	if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.nResult = TER_SKIP;
		m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.nResult = TER_SKIP;
		OnSetTestResolution(TER_SKIP, _T("TEST SKIP"));
		return FALSE;
	}

	OnChangeStartBtnState(TRUE);
	m_bTestFinishMode = TRUE;

	while (m_bFlag_FocusTest)
	{
		if (m_bFlag_UseStop == TRUE)
		{
			m_stInspInfo.CamInfo.nJudgment = TR_UserStop;
			break;
		}

		Sleep(33);
		enResult = (enTestEachResult)m_TIProcessing.ActiveAlignRun();

		switch (enResult)
		{
		case TER_Pass:
			m_stInspInfo.CamInfo.nJudgment = TR_Pass;
			break;
		case TER_NoImage:
			m_stInspInfo.CamInfo.nJudgment = TR_NoImage;
			break;
		case TER_CamStateFail:
			m_stInspInfo.CamInfo.nJudgment = TR_CamStateFail;
			break;
		default:
			m_stInspInfo.CamInfo.nJudgment = TR_Fail;
			break;
		}

		if (m_stInspInfo.CamInfo.nJudgment != TR_NoImage)
		{
			if (m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_X == -99999 ||
				m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_Y == -99999)
			{
				strText.Format(_T("(X:No Detect, Y:No Detect), %d / %d"),
					m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.nResult_cnt,
					m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.nTotal_cnt);
			}
			else
			{
				strText.Format(_T("(X:%d, Y:%d), %d / %d"),
					m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_X,
					m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_Y,
					m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.nResult_cnt,
					m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.nTotal_cnt);
			}			

			if (iCnt <= USE_TEST_ITEM_CNT)
			{
				for (UINT nIdx = 0; nIdx < (UINT)iCnt; nIdx++)
				{
					// TEST 아이템이 있는지 확인 후 UI 출력
					if (m_stInspInfo.ModelInfo.TestItemz.GetAt(nIdx) == TIID_ActiveAlgin)
						OnSetTestFocus((enTestEachResult)enResult, strText);
				}
			}

			OnSetResult((enTestResult)enResult, strText);
		}
		else
		{
			strText.Format(_T("Camera Disconnect"));
			m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.nResult = enResult;
			m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.nResult = enResult;

			if (iCnt <= USE_TEST_ITEM_CNT)
			{
				for (UINT nIdx = 0; nIdx < (UINT)iCnt; nIdx++)
				{
					// TEST 아이템이 있는지 확인 후 UI 출력
					if (m_stInspInfo.ModelInfo.TestItemz.GetAt(nIdx) == TIID_ActiveAlgin)
						OnSetTestFocus((enTestEachResult)enResult, strText);
				}
			}

			OnSetResult((enTestResult)enResult, strText);
			m_bFlag_FocusTest = FALSE;
			return FALSE;
		}

		// 역삽
		if (m_stInspInfo.ModelInfo.nQuadRectPos != QRP_NOTUSE && m_stInspInfo.CamInfo.nJudgment == TR_CamStateFail)
		{
			strText.Format(_T("Cam State Fail"));
			m_stInspInfo.ModelInfo.stEIAJ.stEIAJData.nResult = enResult;
			m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.nResult = enResult;

			if (iCnt <= USE_TEST_ITEM_CNT)
			{
				for (UINT nIdx = 0; nIdx < (UINT)iCnt; nIdx++)
				{
					// TEST 아이템이 있는지 확인 후 UI 출력
					if (m_stInspInfo.ModelInfo.TestItemz.GetAt(nIdx) == TIID_ActiveAlgin)
						OnSetTestFocus((enTestEachResult)enResult, strText);
				}
			}

			OnLog(_T("Cam State Fail"));
			OnSetResult((enTestResult)enResult, strText);
			m_bFlag_FocusTest = FALSE;
			return FALSE;
		}
	}

	return TRUE;
}

void CTestManager_EQP::OnChangWorklistData()
{
	// DATA MOVE
	m_stInspInfo.CamInfo.stCurrentData		= m_stInspInfo.ModelInfo.stCurrent.stCurrentData;
	m_stInspInfo.CamInfo.stCenterPointData	= m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData;
	m_stInspInfo.CamInfo.stEIAJData			= m_stInspInfo.ModelInfo.stEIAJ.stEIAJData;
	m_stInspInfo.CamInfo.stRotateData		= m_stInspInfo.ModelInfo.stRotate.stRotateData;
	m_stInspInfo.CamInfo.stParticleData		= m_stInspInfo.ModelInfo.stParticle.stParticleData;
	m_stInspInfo.CamInfo.stSFRData			= m_stInspInfo.ModelInfo.stSFR.stSFRData;

	// TOTAL
	if (m_stInspInfo.LotInfo.bLotStatus == TRUE || m_stInspInfo.PermissionMode == Permission_MES) //LOT 모드 일 경우에만 WORKLIST 저장
	{
		if (m_stInspInfo.PermissionMode == Permission_MES)
		{
			if (TRUE == m_stInspInfo.MESInfo.bStatus)
			{
				m_stInspInfo.MESInfo.SetMESTestResult(m_stInspInfo.CamInfo);
				if (m_stInspInfo.MESInfo.bStatus)
				{
					SaveFileMesSystem_Mcnex();
				}
			}
		}
		SaveFinalMesSystem();

		// 선택한 검사 항목 개수
		INT_PTR iTestCnt = m_stInspInfo.ModelInfo.TestItemz.GetCount();
		for (INT_PTR iIdx = 0; iIdx < iTestCnt; iIdx++)
		{
			UINT nTestItemID = m_stInspInfo.ModelInfo.TestItemz.GetAt(iIdx);

			switch (nTestItemID)
			{
			case TIID_Current:
				SaveCurrentMesSystem();
				break;
			case TIID_CenterPointAdj:
				break;	
			case TIID_Rotation:
				break;
			case TIID_Particle:
				break;
			case TIID_ActiveAlgin:
				//SaveSFRMesSystem();
				SaveEIAJMesSystem();
				SaveCenterPointMesSystem();
				break;
 			
			case TIID_EIAJ:
				break;
			default:
				break;
			}
		}
	}
}

BOOL CTestManager_EQP::SetAutoMasterMode()
{
	// 수정 필요 [2/7/2019 Seongho.Lee]
	// FIX 이 되었는지 확인

	//현장에서 풀어야함
	//if (m_Device.DigitalIOCtrl.GetInStatus(DI_ModuleFixCYLSensor) == FALSE)
	//{
	//	MessageView(_T("Module Not Fix!"));
	//	return FALSE;
	//}

	// UI 초기화
	OnSetResetSiteInfo();

	if (CameraTestInitialize(Initial) == FALSE)
	{
		MessageView(_T("Master Set Camera ON Fail"));
		OnSetResult(TR_Fail, _T("Master Set Camera ON Fail"));
		return FALSE;
	}

	// 광축 측정
	(enTestEachResult)m_TIProcessing.CenterPointRun();

	// 로테이트 측정
//	(enTestEachResult)m_TIProcessing.RotateRun();

	m_stInspInfo.ModelInfo.nPicViewMode = PIC_CenterAdjust;

	if (abs(m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_X) > 999 || abs(m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_Y) > 999)
	{
		MessageView(_T("Master Set CenterPoint Fail"));
		OnSetResult(TR_Fail, _T("Master Set CenterPoint Fail"));
		m_bFlag_MasterSet = FALSE;
		return FALSE;
	}

	// 1. 광축 측정 값 - 마스터 카메라 정보 값, 로테이트
	int iResultX	 = abs(m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_X) - abs(m_stInspInfo.ModelInfo.iMasterInfoX);
	int iResultY	 = abs(m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_Y) - abs(m_stInspInfo.ModelInfo.iMasterInfoY);

	// 결과 출력
	if (abs(iResultX) > m_stInspInfo.ModelInfo.nMasterSpcX || abs(iResultY) > m_stInspInfo.ModelInfo.nMasterSpcY)
	{
		OnSetResult(TR_Fail, _T("Master Set Spc Over"));
		m_bFlag_MasterSet = FALSE;
	}
	else
	{
		OnSetResult(TR_Pass, _T("Master Set Pass"));
		m_bFlag_MasterSet = TRUE;
	}

	// 마스터 정보 값을 기준으로 옵셋값 환산
	int iOffsetX	 = m_stInspInfo.ModelInfo.iMasterInfoX - m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_X;
	int iOffsetY	 = m_stInspInfo.ModelInfo.iMasterInfoY - m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_Y;

	// 옵셋값을 기준 점에 적용
	OnSetMasterOffSetData(iOffsetX, iOffsetY);


	if (CameraTestInitialize(Finalize) == FALSE)
	{
		MessageView(_T("Master Set Camera OFF Fail"));
		OnSetResult(TR_Fail, _T("Master Set Camera OFF Fail"));
		return FALSE;
	}

	//현장에서 주석풀어야함
	//if (m_Device.DigitalIOCtrl.SetOutPortStatus(DO_ModuleUnFixCYL,ON) == FALSE)
	//{
	//	MessageView(_T("Module Not UNFix!"));
	//	return FALSE;
	//}



	return TRUE;
}

//=============================================================================
// Method		: SaveFinalMesSystem
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/22 - 15:24
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::SaveFinalMesSystem()
{
	ST_MES_FinalResult m_MesData;

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	//- Equipment
	m_MesData.Equipment = m_stInspInfo.CamInfo.szEquipment = g_szInsptrSysType[SET_INSPECTOR];
	//- Model
	CString strModel = m_stInspInfo.ModelInfo.szModelCode;
	if (strModel.IsEmpty())
		strModel = _T("DEFAULT");
	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = strModel; //m_stInspInfo.szModelName;
	//- SW Ver
	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strIndex;
	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	m_stInspInfo.CamInfo.szIndex = strIndex;

	m_WorklistPtr.pList_Total->InsertFullData(&m_stInspInfo.CamInfo);
	m_WorklistPtr.pList_Total->GetData(m_WorklistPtr.pList_Total->GetItemCount() - 1, DataNum, Data);

	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	m_MesData.Result = Data[Total_W_Result];
	
	// 결과
	for (UINT nIdx = 0; nIdx < Total_W_MaxCol - Total_W_Result; nIdx++)
		m_MesData.Itemz.Add(Data[Total_W_Result + nIdx + 1]);

	// Data에 대한 헤더 입력
	for (UINT nIdx = 0; nIdx < Total_W_MaxCol - Total_W_Result; nIdx++)
		m_MesData.ItemHeaderz.Add(g_lpszHeader_Total_Worklist[Total_W_Result + nIdx + 1]);

	if (m_stInspInfo.ModelInfo.szLotID.IsEmpty() == FALSE && m_stOption.MES.bMesUse == TRUE)
	{
		CFile_Model fileModel;
		fileModel.SaveMESFile(m_stInspInfo.Path.szMes, &m_stInspInfo, m_stOption.MES.szEquipmentID);
	}

//	m_Worklist.Save_FinalResult_List(m_stInspInfo.CamInfo.szReportFilePath, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData);

	m_Worklist.Save_TotalResult_List(m_stInspInfo.CamInfo.szReportFilePath, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData);

	//  [1/17/2019 Admin]
// 	CString szImagePath;
// 	szImagePath.Format(_T("%s\\%s"),
// 		m_stInspInfo.CamInfo.szReportFilePath,
// 		m_MesData.Time);
// 
// 	m_stInspInfo.ModelInfo.nPicViewMode = PIC_TotalPic;
// 	m_TIProcessing.ReportImageSaveOriginal(szImagePath);
// 	m_TIProcessing.ReportImageSavePic(szImagePath);

	//MES Function
	//CFile_Report Report;
	//Report.SaveMES_Report(m_stInspInfo.Path.szMes, &m_stInspInfo.CamInfo);

	return TRUE;
}

BOOL CTestManager_EQP::SaveFileMesSystem_Mcnex()
{
	CFile_MES_Mcnex m_MESFile;

	m_MESFile.Delete_MES_TextFile(m_stInspInfo.Path.szMes);
	return m_MESFile.Make_MES_Log_File(m_stInspInfo.Path.szMes2, m_stInspInfo.szModelName, m_stInspInfo.LotInfo.nLotCount, &m_stInspInfo.MESInfo.stMESResult);
}

//=============================================================================
// Method		: SaveCurrentMesSystem
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/22 - 15:30
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::SaveCurrentMesSystem()
{
	ST_MES_TestItemLog m_MesData;

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	//- Equipment
	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
	//- Model
	CString strModel = m_stInspInfo.ModelInfo.szModelCode;
	if (strModel.IsEmpty())
		strModel = _T("DEFAULT");
	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = strModel; //m_stInspInfo.szModelName;
	//- SW Ver
	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strIndex;
	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	m_stInspInfo.CamInfo.szIndex = strIndex;

	m_WorklistPtr.pList_Current->InsertFullData(&m_stInspInfo.CamInfo);
	m_WorklistPtr.pList_Current->GetData(m_WorklistPtr.pList_Current->GetItemCount() - 1, DataNum, Data);

	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	//- Result
	m_MesData.Result = Data[Curr_W_Result];

	// 결과
	for (UINT nIdx = 0; nIdx < Curr_W_MaxCol - Curr_W_Result; nIdx++)
		m_MesData.Itemz.Add(Data[Curr_W_Result + nIdx + 1]);

	// Data에 대한 헤더 입력
	for (UINT nIdx = 0; nIdx < Curr_W_MaxCol - Curr_W_Result; nIdx++)
		m_MesData.ItemHeaderz.Add(g_lpszHeader_Curr_Worklist[Curr_W_Result + nIdx + 1]);

	CString szResult;
	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("Current"));
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("Current"));
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_UserStop)
	{
		szResult = _T("UserStop");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("Current"));
	}

	//  [1/17/2019 Admin]
// 	CString szImagePath;
// 	szImagePath.Format(_T("%s\\%s"),
// 		m_stInspInfo.CamInfo.szReportFilePath,
// 		m_MesData.Time);
// 
// 	m_stInspInfo.ModelInfo.nPicViewMode = PIC_Current;
// 	m_TIProcessing.ReportImageSavePic(szImagePath);

	//MES Function
	//CFile_Report Report;
	//Report.SaveMES_Report(m_stInspInfo.Path.szMes, &m_stInspInfo.CamInfo);

	return TRUE;
}

//=============================================================================
// Method		: SaveResolutionMesSystem
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/22 - 15:32
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::SaveEIAJMesSystem()
{
	ST_MES_TestItemLog m_MesData;

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	//- Equipment
	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
	//- Model
	CString strModel = m_stInspInfo.ModelInfo.szModelCode;
	if (strModel.IsEmpty())
		strModel = _T("DEFAULT");
	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = strModel; //m_stInspInfo.szModelName;
	//- SW Ver
	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strIndex;
	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	m_stInspInfo.CamInfo.szIndex = strIndex;

	m_WorklistPtr.pList_EIAJ->InsertFullData(&m_stInspInfo.CamInfo);
	m_WorklistPtr.pList_EIAJ->GetData(m_WorklistPtr.pList_EIAJ->GetItemCount() - 1, DataNum, Data);

	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	m_MesData.Result = Data[Resl_W_Result];

	// 결과
	for (UINT nIdx = 0; nIdx < Resl_W_MaxCol - Resl_W_Result; nIdx++)
		m_MesData.Itemz.Add(Data[Resl_W_Result + nIdx + 1]);

	// Data에 대한 헤더 입력
	for (UINT nIdx = 0; nIdx < Resl_W_MaxCol - Resl_W_Result; nIdx++)
		m_MesData.ItemHeaderz.Add(g_lpszHeader_Resl_Worklist[Resl_W_Result + nIdx + 1]);

	CString szResult;
	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("EIAJ"));
	} 
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("EIAJ"));
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_UserStop)
	{
		szResult = _T("UserStop");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("EIAJ"));
	}

	//  [1/17/2019 Admin]
// 	CString szImagePath;
// 	szImagePath.Format(_T("%s\\%s"),
// 		m_stInspInfo.CamInfo.szReportFilePath,
// 		m_MesData.Time);
// 
// 	m_stInspInfo.ModelInfo.nPicViewMode = PIC_EIAJ;
// 	m_TIProcessing.ReportImageSavePic(szImagePath);

	//MES Function
	//CFile_Report Report;
	//Report.SaveMES_Report(m_stInspInfo.Path.szMes, &m_stInspInfo.CamInfo);

	
	
	return TRUE;
}


BOOL CTestManager_EQP::SaveSFRMesSystem()
{
	ST_MES_TestItemLog m_MesData;

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	//- Equipment
	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
	//- Model
	CString strModel = m_stInspInfo.ModelInfo.szModelCode;
	if (strModel.IsEmpty())
		strModel = _T("DEFAULT");
	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = strModel; //m_stInspInfo.szModelName;
	//- SW Ver
	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strIndex;
	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	m_stInspInfo.CamInfo.szIndex = strIndex;

	m_WorklistPtr.pList_SFR->InsertFullData(&m_stInspInfo.CamInfo);
	m_WorklistPtr.pList_SFR->GetData(m_WorklistPtr.pList_SFR->GetItemCount() - 1, DataNum, Data);

	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	m_MesData.Result = Data[SFR_W_Result];

	// 결과
	for (UINT nIdx = 0; nIdx < SFR_W_MaxCol - SFR_W_Result; nIdx++)
		m_MesData.Itemz.Add(Data[SFR_W_Result + nIdx + 1]);

	// Data에 대한 헤더 입력
	for (UINT nIdx = 0; nIdx < SFR_W_MaxCol - SFR_W_Result; nIdx++)
		m_MesData.ItemHeaderz.Add(g_lpszHeader_SFR_Worklist[SFR_W_Result + nIdx + 1]);

	CString szResult;
	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("SFR"));
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("SFR"));
	}

	//  [1/17/2019 Admin]
// 	CString szImagePath;
// 	szImagePath.Format(_T("%s\\%s"),
// 		m_stInspInfo.CamInfo.szReportFilePath,
// 		m_MesData.Time);
// 
// 	m_stInspInfo.ModelInfo.nPicViewMode = PIC_SFR;
// 	m_TIProcessing.ReportImageSavePic(szImagePath);

	//MES Function
	//CFile_Report Report;
	//Report.SaveMES_Report(m_stInspInfo.Path.szMes, &m_stInspInfo.CamInfo);

	return TRUE;
}

//************************************
// Method:    SaveCenterPointMesSystem
// FullName:  CTestManager_EQP::SaveCenterPointMesSystem
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL CTestManager_EQP::SaveCenterPointMesSystem()
{
	ST_MES_TestItemLog m_MesData;

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
	//- Equipment
	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
	//- Model
	CString strModel = m_stInspInfo.ModelInfo.szModelCode;
	if (strModel.IsEmpty())
		strModel = _T("DEFAULT");
	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = strModel; //m_stInspInfo.szModelName;
	//- SW Ver
	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
	//- LOT
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName = m_stInspInfo.MESInfo.stMESResult.szLotNo;
	else
		m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;

	//- Operator
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator = m_stInspInfo.MESInfo.stMESResult.szOperator;
	else
		m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;

	//- Barcode
	if (m_stInspInfo.PermissionMode == Permission_MES)
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode = m_stInspInfo.MESInfo.stMESResult.szBarcode;
	else
		m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strIndex;
	strIndex.Format(_T("%d"), m_stInspInfo.LotInfo.nLotCount);
	m_stInspInfo.CamInfo.szIndex = strIndex;

	m_WorklistPtr.pList_CenterPoint->InsertFullData(&m_stInspInfo.CamInfo);
	m_WorklistPtr.pList_CenterPoint->GetData(m_WorklistPtr.pList_CenterPoint->GetItemCount() - 1, DataNum, Data);

	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
	m_MesData.Result = Data[CP_W_Result];

	// 결과
	for (UINT nIdx = 0; nIdx < CP_W_MaxCol - CP_W_Result; nIdx++)
		m_MesData.Itemz.Add(Data[CP_W_Result + nIdx + 1]);

	// Data에 대한 헤더 입력
	for (UINT nIdx = 0; nIdx < CP_W_MaxCol - CP_W_Result; nIdx++)
		m_MesData.ItemHeaderz.Add(g_lpszHeader_CP_Worklist[CP_W_Result + nIdx + 1]);

	CString szResult;
	if (m_stInspInfo.CamInfo.nJudgment == TR_Pass)
	{
		szResult = _T("PASS");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("CenterPoint"));
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_Fail)
	{
		szResult = _T("FAIL");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("CenterPoint"));
	}
	else if (m_stInspInfo.CamInfo.nJudgment == TR_UserStop)
	{
		szResult = _T("UserStop");
		m_Worklist.Save_TestItemLog_List(m_stInspInfo.CamInfo.szReportFilePath, szResult, m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("CenterPoint"));
	}

	//  [1/17/2019 Admin]
// 	CString szImagePath;
// 	szImagePath.Format(_T("%s\\%s"),
// 		m_stInspInfo.CamInfo.szReportFilePath,
// 		m_MesData.Time);
// 
// 	m_stInspInfo.ModelInfo.nPicViewMode = PIC_CenterAdjust;
// 	m_TIProcessing.ReportImageSavePic(szImagePath);

	//MES Function
	//CFile_Report Report;
	//Report.SaveMES_Report(m_stInspInfo.Path.szMes, &m_stInspInfo.CamInfo);

	return TRUE;
}

//  [1/17/2019 Admin]
void CTestManager_EQP::GetSaveImageFilePath(__out CString& szOutPath)
{
	CString szPath = m_stInspInfo.Path.szImage;
	CString szPathTime;
	SYSTEMTIME tmLocal;

	GetLocalTime(&tmLocal);

	//- 경로 /날짜/ 모델/ Lot명 / 결과

	m_TIProcessing.GetSaveImageFileModelPath(szPath);

	//Lot 모드 여부
	if (m_stInspInfo.LotInfo.bLotStatus == TRUE)
	{
		szPath += _T("\\LotMode");

		if (!m_stInspInfo.LotInfo.szLotName.IsEmpty())
			szPath += _T("\\") + m_stInspInfo.LotInfo.szLotName;
	}
	else
	{
		szPath += _T("\\Not_LotMode");
	}

	MakeDirectory(szPath);

	szOutPath.Format(_T("%04d%02d%02d_%02d%02d%02d"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	szOutPath = szPath + _T("\\") + szOutPath;

}

//=============================================================================
// Method		: StartAutoFocusingThr
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/10/22 - 14:55
// Desc.		:
//=============================================================================
// BOOL CTestManager_EQP::StartAutoFocusingThr()
// {
// 	if (NULL != m_hThrAutoFocusing)
// 	{
// 		CloseHandle(m_hThrAutoFocusing);
// 		m_hThrAutoFocusing = NULL;
// 	}
// 
// 	stThreadParam* pParam = new stThreadParam;
// 	pParam->pOwner = this;
// 	pParam->nIndex = 0;
// 	pParam->nArg_1 = 0;
// 	pParam->nArg_2 = 0;
// 
// 	m_hThrAutoFocusing = HANDLE(_beginthreadex(NULL, 0, StartAutoFocusing, pParam, 0, NULL));
// 
// 	return TRUE;
// }

//=============================================================================
// Method		: StartAutoFocusing
// Access		: public static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2017/10/22 - 14:47
// Desc.		:
//=============================================================================
// UINT WINAPI CTestManager_EQP::StartAutoFocusing(__in LPVOID lParam)
// {
// 	CTestManager_EQP* pThis = (CTestManager_EQP*)((stThreadParam*)lParam)->pOwner;
// 
// 	if (NULL != lParam)
// 		delete lParam;
// 
// 	pThis->StartAutoFocusingProcess();
// 
// 	return 0;
// }


//=============================================================================
// Method		: EndAutoFocusingThr
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/22 - 14:56
// Desc.		:
//=============================================================================
// void CTestManager_EQP::EndAutoFocusingThr()
// {
// 	DWORD dwEvent = WaitForSingleObject(m_hThrAutoFocusing, 1000);
// 
// 	if (WAIT_TIMEOUT == dwEvent)
// 	{
// 		TRACE(_T("Auto Focusing Thread Timeout\n"));
// 	}
// 	else if (WAIT_FAILED == dwEvent)
// 	{
// 		TRACE(_T("Auto Focusing Thread WAIT_FAILED:%d\n"), GetLastError());
// 	}
// }

// 
// void CTestManager_EQP::StartAutoFocusingProcess()
// {
// 	//m_TIProcessing.InitFocusingRun(m_stInspInfo.CamInfo.dInitPosY, m_stInspInfo.CamInfo.dInitPosR);
// 
// 	Sleep(500);
// 	m_TIProcessing.AutoFocusingRun(m_stInspInfo.CamInfo.dDisplaceAvg);
// 	
// 	//m_bFlag_AutoFocus = TRUE;
// }

//=============================================================================
// Method		: SaveSFRMesSystem
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/8/11 - 10:49
// Desc.		:
//=============================================================================
// BOOL CTestManager_EQP::SaveSFRMesSystem()
// {
// 
// 	ST_MES_TestItemLog m_MesData;
// 
// 	//- 시간
// 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
// 	//- Equipment
// 	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
// 	//- Model
// 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
// 	//- SW Ver
// 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
// 	//- LOT
// 	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;
// 	//- Operator
// 	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;
// 	//- Barcode
// 	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
// 
// 	//- Result + Data
// 	UINT DataNum = 0;
// 	CString Data[100];
// 
// 	m_WorklistPtr.pList_SFR->InsertFullData(&m_stInspInfo.CamInfo);
// 	m_WorklistPtr.pList_SFR->GetData(m_WorklistPtr.pList_SFR->GetItemCount() - 1, DataNum, Data);
// 
// 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
// 	m_MesData.Result = Data[SFR_W_Result];
// 
// 	// 결과
// 	for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum - SFR_W_Result; nIdx++)
// 		m_MesData.Itemz.Add(Data[SFR_W_Result + nIdx + 1]);
// 
// 	// Data에 대한 헤더 입력
// 	for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
// 	{
// 		m_MesData.ItemHeaderz.Add(g_lpszHeader_SFR_Worklist[SFR_W_Result + nIdx + 1]);
// 	}
// 
// 	m_Worklist.Save_TestItemLog_List(m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("SFR"));
// 
// 	return TRUE;
// }

//=============================================================================
// Method		: SaveRotateMesSystem
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/22 - 15:34
// Desc.		:
//=============================================================================
// BOOL CTestManager_EQP::SaveRotateMesSystem()
// {
// 	ST_MES_TestItemLog m_MesData;
// 
// 	//- 시간
// 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
// 	//- Equipment
// 	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
// 	//- Model
// 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
// 	//- SW Ver
// 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
// 	//- LOT
// 	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;
// 	//- Operator
// 	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;
// 	//- Barcode
// 	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
// 
// 	//- Result + Data
// 	UINT DataNum = 0;
// 	CString Data[100];
// 
// 	m_WorklistPtr.pList_Rotate->InsertFullData(&m_stInspInfo.CamInfo);
// 	m_WorklistPtr.pList_Rotate->GetData(m_WorklistPtr.pList_Rotate->GetItemCount() - 1, DataNum, Data);
// 
// 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
// 	m_MesData.Result = Data[Rota_W_Result];
// 
// 	// 결과
// 	for (UINT nIdx = 0; nIdx < Rota_W_MaxCol - Rota_W_Result; nIdx++)
// 		m_MesData.Itemz.Add(Data[Rota_W_Result + nIdx + 1]);
// 
// 	// Data에 대한 헤더 입력
// 	for (UINT nIdx = 0; nIdx < Rota_W_MaxCol - Rota_W_Result; nIdx++)
// 		m_MesData.ItemHeaderz.Add(g_lpszHeader_Rota_Worklist[Rota_W_Result + nIdx + 1]);
// 
// 	m_Worklist.Save_TestItemLog_List(m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("Rotate"));
// 
// 	return TRUE;
// }

//=============================================================================
// Method		: SaveParticleMesSystem
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/22 - 15:35
// Desc.		:
//=============================================================================
// BOOL CTestManager_EQP::SaveParticleMesSystem()
// {
// 	ST_MES_TestItemLog m_MesData;
// 
// 	//- 시간
// 	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&m_stInspInfo.CamInfo.tmInputTime);
// 	//- Equipment
// 	m_MesData.Equipment = g_szInsptrSysType[SET_INSPECTOR];
// 	//- Model
// 	m_MesData.Model = m_stInspInfo.CamInfo.szModelName = m_stInspInfo.szModelName;
// 	//- SW Ver
// 	m_MesData.SWVersion = m_stInspInfo.CamInfo.szSWVersion = m_Worklist.GetSWVersion(g_szProgramName[SET_INSPECTOR]);
// 	//- LOT
// 	m_MesData.LOTName = m_stInspInfo.CamInfo.szLotID = m_stInspInfo.LotInfo.szLotName;
// 	//- Operator
// 	m_MesData.Operator = m_stInspInfo.CamInfo.szOperatorName = m_stInspInfo.LotInfo.szOperator;
// 	//- Barcode
// 	m_MesData.Barcode = m_stInspInfo.CamInfo.szBarcode;
// 
// 	//- Result + Data
// 	UINT DataNum = 0;
// 	CString Data[100];
// 
// 	m_WorklistPtr.pList_Particle->InsertFullData(&m_stInspInfo.CamInfo);
// 	m_WorklistPtr.pList_Particle->GetData(m_WorklistPtr.pList_Particle->GetItemCount() - 1, DataNum, Data);
// 
// 	//-------Worklist에 Data와 Result을 가지고 와서 Las에 추가
// 	m_MesData.Result = Data[Par_W_Result];
// 
// 	// 결과
// 	for (UINT nIdx = 0; nIdx < Par_W_MaxCol - Par_W_Result; nIdx++)
// 		m_MesData.Itemz.Add(Data[Par_W_Result + nIdx + 1]);
// 
// 	// Data에 대한 헤더 입력
// 	for (UINT nIdx = 0; nIdx < Par_W_MaxCol - Par_W_Result; nIdx++)
// 		m_MesData.ItemHeaderz.Add(g_lpszHeader_Par_Worklist[Par_W_Result + nIdx + 1]);
// 
// 	m_Worklist.Save_TestItemLog_List(m_stInspInfo.Path.szReport, &m_stInspInfo.CamInfo.tmInputTime, &m_MesData, _T("Particle"));
// 
// 	return TRUE;
// }


LRESULT CTestManager_EQP::MoveStage(BOOL bSet){

	BOOL bResult = TRUE;

	if (bSet == TRUE)
	{
		bResult = m_Device.MotionSequence.DriverInOutCYLMotion(ON);
		DoEvents(500);
		bResult &= m_Device.MotionSequence.PCBFixUnFixCYLMotion(ON);
	}
	else{
		bResult = m_Device.MotionSequence.PCBFixUnFixCYLMotion(OFF);
		DoEvents(500);
		bResult &= m_Device.MotionSequence.DriverInOutCYLMotion(OFF);
	}
	return bResult;
}

//  [1/17/2019 Admin]
void CTestManager_EQP::ImageSave(UINT nTestItemID){


	//-이미지 풀 path + 이미지 파일명 앞에 날짜까지 strpath에 입력 됨.
	CString strPath;
	GetSaveImageFilePath(strPath);

	//날짜와 시간 이외에 다른 파일명을 밑에 소스로 생성 시킨다.

	//- 바코드가 있는 경우에는 주석을 풀도록 함.
	CString szBarcode;
	if (m_stInspInfo.CamInfo.szBarcode.IsEmpty() || m_stInspInfo.CamInfo.szBarcode == _T("No Barcode"))
	{
		szBarcode = _T("_NoBarcode");
	}
	else{
		szBarcode = m_stInspInfo.CamInfo.szBarcode;
	}
	strPath += szBarcode;


	CString szTestName = g_szLT_TestItem_Name[nTestItemID];

	strPath += _T("_") + szTestName;

	// 날짜 / 모델 / Lot / 날짜시간_바코드_test명 

	m_TIProcessing.ReportImageSaveOriginal(strPath);
	m_TIProcessing.ReportImageSavePic(strPath);

}

void CTestManager_EQP::CreateTimer_UpdateIndicatorDisplay()
{
	__try
	{
		if (NULL == m_hTimer_Update_Indicator)
		if (!CreateTimerQueueTimer(&m_hTimer_Update_Indicator, m_hTimerQueue_Indicator, (WAITORTIMERCALLBACK)TimerRoutine_UpdateIndicator, (PVOID)this, 5000, 250, WT_EXECUTEDEFAULT))
		{
			TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CTestManager_Base::CreateTimer_UpdateUI()\n"));
	}
}

void CTestManager_EQP::DeleteTimer_UpdateIndicatorDisplay()
{
	__try
	{
		if (NULL != m_hTimer_Update_Indicator)
		{
			if (DeleteTimerQueueTimer(m_hTimerQueue_Indicator, m_hTimer_Update_Indicator, NULL))
			{
				m_hTimer_Update_Indicator = NULL;
			}
			else
			{
				TRACE(_T("DeleteTimerQueueTimer : m_hTimer_Update_UI failed (%d)\n"), GetLastError());
			}
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CTestManager_Base::DeleteTimer_UpdateUI()\n"));
	}

	TRACE(_T("타이머 종료 : CTestManager_Base::DeleteTimer_UpdateUI()\n"));
}

VOID CALLBACK CTestManager_EQP::TimerRoutine_UpdateIndicator(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	CTestManager_EQP* pThis = (CTestManager_EQP*)lpParam;

	pThis->OnMonitor_UpdateIndicator();
}


void CTestManager_EQP::OnMonitor_UpdateIndicator(){

	if (m_bBusyIndicator == TRUE)
	{
		return;
	}
	m_bBusyIndicator = TRUE;
	for (UINT nIdx = 0; nIdx < Indicator_Max; nIdx++)
	{
		if (m_bFlag_Indicator[nIdx] == TRUE && b_IndicatorConnect[nIdx] == TRUE)
			m_Device.Indicator[nIdx].Send_ReadIndicatorData(IndicatorSensor::Cmd_Real_Data, m_fValue[nIdx]);
	}
	if (b_IndicatorConnect[IndicatorX] && b_IndicatorConnect[IndicatorY])
		OnSetIndicatorData(m_stInspInfo.ModelInfo, m_fValue[IndicatorX], m_fValue[IndicatorY]);
	m_bBusyIndicator = FALSE;
}

void CTestManager_EQP::CreateTimerQueue_Mon_Indicator()
{
	__try
	{
		// Create the timer queue.
		m_hTimerQueue_Indicator = CreateTimerQueue();
		if (NULL == m_hTimerQueue_Indicator)
		{
			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
			return;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CreateTimerQueue_Mon ()"));
	}
}

//=============================================================================
// Method		: DeleteTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/31 - 22:10
// Desc.		:
//=============================================================================
void CTestManager_EQP::DeleteTimerQueue_Mon_Indicator()
{
	__try
	{
		//DeleteTimer_TimeCheck();
		//DeleteTimer_UpdateUI();

		if (!DeleteTimerQueue(m_hTimerQueue_Indicator))
			TRACE(_T("DeleteTimerQueue failed (%d)\n"), GetLastError());
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CTestManager_Base::DeleteTimerQueue_Mon()\n"));
	}

	TRACE(_T("타이머 종료 : CTestManager_Base::DeleteTimerQueue_Mon()\n"));
}
