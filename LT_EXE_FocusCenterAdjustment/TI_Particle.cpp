﻿#include "stdafx.h"
#include "TI_Particle.h"

CTI_Particle::CTI_Particle()
{
	m_nWidth = 720;
	m_nHeight = 480;

	m_cAreaBuf	= NULL;
	m_fData		= NULL;
	pRectArray	= NULL;
}

CTI_Particle::~CTI_Particle()
{
	m_nCounter = 0;
}

//=============================================================================
// Method		: SetImageSize
// Access		: public  
// Returns		: BOOL
// Parameter	: DWORD dwWidth
// Parameter	: DWORD dwHeight
// Qualifier	:
// Last Update	: 2017/7/7 - 10:47
// Desc.		:
//=============================================================================
BOOL CTI_Particle::SetImageSize(DWORD dwWidth, DWORD dwHeight)
{
	if (dwWidth <= 0 || dwHeight <= 0)
		return FALSE;

	m_nWidth = dwWidth;
	m_nHeight = dwHeight;

	return TRUE;
}

//=============================================================================
// Method		: Particle_Test
// Access		: public  
// Returns		: UINT
// Parameter	: ST_LT_TI_Particle * pstParticle
// Parameter	: LPBYTE pImageBuf
// Qualifier	:
// Last Update	: 2017/7/28 - 10:51
// Desc.		:
//=============================================================================
UINT CTI_Particle::Particle_Test(ST_LT_TI_Particle *pstParticle, LPBYTE pImageBuf)
{
	int iStartX			= 0;
	int iStartY			= 0;
	int iEndX			= m_nWidth;
	int	iEndY			= m_nHeight;
	double dbAVG_Y		= 0.0;
	int iIncrease_RageX = 15;
	int iIncrease_RageY = 10;
	UINT nCnt			= 0;
	DWORD RGBLINE = iStartY * IMAGE_WX4;
	BYTE R, G, B;

	MemoryCreat();
	m_nCounter = 0;

	IplImage *OriginImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);

	FullRegionSum(pstParticle);

	for (int ilopY = iStartY; ilopY < iEndY; ilopY++)
	{
		for (int ilopX = iStartX; ilopX < iEndX; ilopX++)
		{
			if ((ilopX >= iEndX - iIncrease_RageX) || (ilopX <= iStartX + iIncrease_RageX))
			{
				if (ilopX >= m_nWidth / 2)
				{
					B = pImageBuf[ilopY * (m_nWidth * 3) + 3 * (iEndX - iIncrease_RageX)];
					G = pImageBuf[ilopY * (m_nWidth * 3) + 3 * (iEndX - iIncrease_RageX) + 1];
					R = pImageBuf[ilopY * (m_nWidth * 3) + 3 * (iEndX - iIncrease_RageX) + 2];
				}
				else
				{
					B = pImageBuf[ilopY * (m_nWidth * 3) + 3 * (iStartX + iIncrease_RageX)];
					G = pImageBuf[ilopY * (m_nWidth * 3) + 3 * (iStartX + iIncrease_RageX) + 1];
					R = pImageBuf[ilopY * (m_nWidth * 3) + 3 * (iStartX + iIncrease_RageX) + 2];
				}

				m_fData[ilopY][ilopX] = (float)((0.29900*R) + (0.58700*G) + (0.11400*B));
			}
			else
			{
				B = pImageBuf[ilopY * (m_nWidth * 3) + 3 * ilopX];
				G = pImageBuf[ilopY * (m_nWidth * 3) + 3 * ilopX + 1];
				R = pImageBuf[ilopY * (m_nWidth * 3) + 3 * ilopX + 2];
				m_fData[ilopY][ilopX] = (float)((0.29900*R) + (0.58700*G) + (0.11400*B));
			}
			
			dbAVG_Y += m_fData[ilopY][ilopX];
			OriginImage->imageData[(ilopY*OriginImage->widthStep) + (ilopX)] = m_fData[ilopY][ilopX];

			nCnt++;
		}
	}

	if (nCnt > 0)
	{
		dbAVG_Y = dbAVG_Y / nCnt;
	}

	for (int ilopY = iStartY; ilopY < iEndY; ilopY++)
	{
		for (int lopx = iStartX; lopx < iEndX; lopx++)
		{
			if (ilopY >= (iEndY - iIncrease_RageY))
			{
				B = OriginImage->imageData[((iEndY - iIncrease_RageY)*OriginImage->widthStep) + (lopx)];
				G = OriginImage->imageData[((iEndY - iIncrease_RageY)*OriginImage->widthStep) + (lopx)+1];
				R = OriginImage->imageData[((iEndY - iIncrease_RageY)*OriginImage->widthStep) + (lopx)+2];
			
				m_fData[ilopY][lopx] = (float)((0.29900*R) + (0.58700*G) + (0.11400*B));
			}
			
			OriginImage->imageData[(ilopY*OriginImage->widthStep) + (lopx)] = m_fData[ilopY][lopx];
		}
	}

	for (int i = 0; i < (int)m_nWidth; i++)
	{
		for (int j = 0; j < (int)m_nHeight; j++)
		{
			pImageBuf[j * (m_nWidth * 3) + 3 * i] = OriginImage->imageData[j * OriginImage->widthStep + i + 0];
			pImageBuf[j * (m_nWidth * 3) + 3 * i + 1] = OriginImage->imageData[j * OriginImage->widthStep + i + 1];
			pImageBuf[j * (m_nWidth * 3) + 3 * i + 2] = OriginImage->imageData[j * OriginImage->widthStep + i + 2];
		}
	}

	// 덩어리 검출
	LumpDetection(pstParticle, pImageBuf);

	if (pstParticle->stParticleData.iResultcnt > 0)
	{
		for (int lop = 0; lop < pstParticle->stParticleData.iResultcnt; lop++)
		{
			///STDEV 구해보자~
			double Y_AVG = 0.0;
			double Y_STDEV = 0.0;
			double Temp_S = 0;
			int Pixel_Cnt = 0;
			int Range_Pixel = 5;

			//평균
			for (int y = pstParticle->stParticleData.ErrRegionList[lop].RegionList.top - Range_Pixel; y <= pstParticle->stParticleData.ErrRegionList[lop].RegionList.bottom + Range_Pixel; y++)
			{
				for (int x = pstParticle->stParticleData.ErrRegionList[lop].RegionList.left - Range_Pixel; x <= pstParticle->stParticleData.ErrRegionList[lop].RegionList.right + Range_Pixel; x++)
				{
					if (y > 0 && y < (int)m_nHeight && x > 0 && x < (int)m_nWidth)
					{
						Y_AVG += m_fData[y][x];
						Pixel_Cnt++;
					}
				}
			}
			Y_AVG /= Pixel_Cnt;

			//분산
			for (int y = pstParticle->stParticleData.ErrRegionList[lop].RegionList.top - Range_Pixel; y <= pstParticle->stParticleData.ErrRegionList[lop].RegionList.bottom + Range_Pixel; y++)
			{
				for (int x = pstParticle->stParticleData.ErrRegionList[lop].RegionList.left - Range_Pixel; x <= pstParticle->stParticleData.ErrRegionList[lop].RegionList.right + Range_Pixel; x++)
				{
					if (y > 0 && y < (int)m_nHeight && x > 0 && x < (int)m_nWidth)
					{
						Temp_S = Y_AVG - m_fData[y][x];
						Y_STDEV += pow(Temp_S, 2);
					}
				}
			}

			//표준편차
			Y_STDEV = sqrt(Y_STDEV / Pixel_Cnt);
			pstParticle->stParticleData.ErrRegionList[lop].dbStainBlemish = Y_STDEV;

			if (pstParticle->stParticleData.ErrRegionList[lop].dbStainBlemish > 100)
				pstParticle->stParticleData.ErrRegionList[lop].dbStainBlemish = 100;

			if (pstParticle->stParticleData.ErrRegionList[lop].dbStainBlemish < 0)
				pstParticle->stParticleData.ErrRegionList[lop].dbStainBlemish = 0;
		}

		// Blemish가 불량 기준보다 작은 멍은 List 에서 제거
		for (int lop = 0; lop < pstParticle->stParticleData.iResultcnt; lop++)
		{
			for (int lop3 = 0; lop3 < pstParticle->stParticleData.iResultcnt; lop3++)
			{
				int Center_X = (pstParticle->stParticleData.ErrRegionList[lop3].RegionList.left + pstParticle->stParticleData.ErrRegionList[lop3].RegionList.right) / 2;
				int Center_Y = (pstParticle->stParticleData.ErrRegionList[lop3].RegionList.top + pstParticle->stParticleData.ErrRegionList[lop3].RegionList.bottom) / 2;
				//// 중심 -> A
				if (pstParticle->stParticleData.ErrRegionList[lop3].dbStainBlemish <= pstParticle->stParticleOp.rectData[Particle_Region_Center].dbBruiseConc &&
					pstParticle->stParticleData.ErrRegionList[lop3].dbStainBlemish >= 0 && m_cAreaBuf[Center_X][Center_Y] == 1){
					for (int lop2 = lop3; lop2 < pstParticle->stParticleData.iResultcnt; lop2++){
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.top = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.top;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.bottom = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.bottom;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.left = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.left;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.right = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.right;
						pstParticle->stParticleData.ErrRegionList[lop2].dbStainBlemish = pstParticle->stParticleData.ErrRegionList[lop2 + 1].dbStainBlemish;
					}
					pstParticle->stParticleData.iResultcnt--;
					break;
				}
				//// 중간 -> B
				if (pstParticle->stParticleData.ErrRegionList[lop3].dbStainBlemish <= pstParticle->stParticleOp.rectData[Particle_Region_Middle].dbBruiseConc &&
					pstParticle->stParticleData.ErrRegionList[lop3].dbStainBlemish >= 0 && m_cAreaBuf[Center_X][Center_Y] == 2){
					for (int lop2 = lop3; lop2 < pstParticle->stParticleData.iResultcnt; lop2++){
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.top = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.top;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.bottom = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.bottom;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.left = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.left;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.right = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.right;
						pstParticle->stParticleData.ErrRegionList[lop2].dbStainBlemish = pstParticle->stParticleData.ErrRegionList[lop2 + 1].dbStainBlemish;
					}
					pstParticle->stParticleData.iResultcnt--;
					break;
				}
				//// 바깥 -> C
				if (pstParticle->stParticleData.ErrRegionList[lop3].dbStainBlemish <= pstParticle->stParticleOp.rectData[Particle_Region_Side].dbBruiseConc &&
					pstParticle->stParticleData.ErrRegionList[lop3].dbStainBlemish >= 0 && m_cAreaBuf[Center_X][Center_Y] == 3){
					for (int lop2 = lop3; lop2 < pstParticle->stParticleData.iResultcnt; lop2++){
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.top = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.top;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.bottom = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.bottom;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.left = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.left;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.right = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.right;
						pstParticle->stParticleData.ErrRegionList[lop2].dbStainBlemish = pstParticle->stParticleData.ErrRegionList[lop2 + 1].dbStainBlemish;
					}
					pstParticle->stParticleData.iResultcnt--;
					break;
				}
				int Size_x = pstParticle->stParticleData.ErrRegionList[lop3].RegionList.right - pstParticle->stParticleData.ErrRegionList[lop3].RegionList.left;
				int Size_y = pstParticle->stParticleData.ErrRegionList[lop3].RegionList.bottom - pstParticle->stParticleData.ErrRegionList[lop3].RegionList.top;
				int Size_Limit = 12;
				//// 바깥 -> C  -> 노이즈관련 예외
				if (pstParticle->stParticleData.ErrRegionList[lop3].dbStainBlemish <= pstParticle->stParticleOp.rectData[Particle_Region_Side].dbBruiseConc * 2 &&
					pstParticle->stParticleData.ErrRegionList[lop3].dbStainBlemish >= 0 && m_cAreaBuf[Center_X][Center_Y] == 3 && (Size_x <= Size_Limit || Size_y <= Size_Limit))
				{
					for (int lop2 = lop3; lop2 < pstParticle->stParticleData.iResultcnt; lop2++){
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.top = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.top;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.bottom = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.bottom;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.left = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.left;
						pstParticle->stParticleData.ErrRegionList[lop2].RegionList.right = pstParticle->stParticleData.ErrRegionList[lop2 + 1].RegionList.right;
						pstParticle->stParticleData.ErrRegionList[lop2].dbStainBlemish = pstParticle->stParticleData.ErrRegionList[lop2 + 1].dbStainBlemish;
					}
					pstParticle->stParticleData.iResultcnt--;
					break;
				}
			}
		}

		for (int i = 0; i < pstParticle->stParticleData.iResultcnt; i++)
		{
			if (pstParticle->stParticleData.ErrRegionList[i].dbStainBlemish > 100)
				pstParticle->stParticleData.ErrRegionList[i].dbStainBlemish = 100;
			else if (pstParticle->stParticleData.ErrRegionList[i].dbStainBlemish < 0)
				pstParticle->stParticleData.ErrRegionList[i].dbStainBlemish = 0;
		}
	}

	if (pstParticle->stParticleData.iResultcnt > 0 )
		pstParticle->stParticleData.nResult = TER_Fail;
	else
		pstParticle->stParticleData.nResult = TER_Pass;
		
	cvReleaseImage(&OriginImage);
	MemoryDelete();

	return pstParticle->stParticleData.nResult;
}

//=============================================================================
// Method		: FullRegionSum
// Access		: public  
// Returns		: void
// Parameter	: ST_LT_TI_Particle * pstParticle
// Qualifier	:
// Last Update	: 2017/3/22 - 21:48
// Desc.		:
//=============================================================================
void CTI_Particle::FullRegionSum(ST_LT_TI_Particle *pstParticle)
{
	BOOL bFlag	= FALSE;
	BOOL bFlag2 = FALSE;

	for (int y = 0; y < (int)m_nHeight; y++)
	{
		for (int x = 0; x < (int)m_nWidth; x++)
		{
			m_cAreaBuf[x][y] = 0;
			bFlag = FALSE;
			bFlag2 = FALSE;

			// C영역
			if ((pstParticle->stParticleOp.rectData[Particle_Region_Side].RegionList.top <= y) && (pstParticle->stParticleOp.rectData[Particle_Region_Side].RegionList.bottom >= y)	
				&& (pstParticle->stParticleOp.rectData[Particle_Region_Side].RegionList.left <= x) && (pstParticle->stParticleOp.rectData[Particle_Region_Side].RegionList.right >= x))
			{
				if (pstParticle->stParticleOp.rectData[Particle_Region_Side].bEllipse == 1)
				{
					if (EllipseDistanceSum(0, pstParticle->stParticleOp.rectData[Particle_Region_Side].RegionList.left, pstParticle->stParticleOp.rectData[Particle_Region_Side].RegionList.top, ((double)pstParticle->stParticleOp.rectData[Particle_Region_Side].RegionList.Width() / 2.0), ((double)pstParticle->stParticleOp.rectData[Particle_Region_Side].RegionList.Height() / 2.0), x, y) <= 1)
					{	//X, Y 가 타원 안에 들어온다면
						m_cAreaBuf[x][y] = 3;
						bFlag = TRUE;
					}
					else
					{
						m_cAreaBuf[x][y] = 0;
					}
				}
				else
				{
					m_cAreaBuf[x][y] = 3;
					bFlag = TRUE;
				}

				// B영역 + A영역
				if (bFlag == TRUE)
				{
					if ((pstParticle->stParticleOp.rectData[Particle_Region_Middle].RegionList.top <= y) && (pstParticle->stParticleOp.rectData[Particle_Region_Middle].RegionList.bottom >= y)	
						&& (pstParticle->stParticleOp.rectData[Particle_Region_Middle].RegionList.left <= x) && (pstParticle->stParticleOp.rectData[Particle_Region_Middle].RegionList.right >= x))
					{
						if (pstParticle->stParticleOp.rectData[Particle_Region_Middle].bEllipse == 1)
						{
							if (EllipseDistanceSum(0, pstParticle->stParticleOp.rectData[Particle_Region_Middle].RegionList.left, pstParticle->stParticleOp.rectData[Particle_Region_Middle].RegionList.top, ((double)pstParticle->stParticleOp.rectData[Particle_Region_Middle].RegionList.Width() / 2.0), ((double)pstParticle->stParticleOp.rectData[Particle_Region_Middle].RegionList.Height() / 2.0), x, y) <= 1)
							{	//X, Y 가 타원 안에 들어온다면
								m_cAreaBuf[x][y] = 2;
								bFlag2 = TRUE;
							}
						}
						else
						{
							m_cAreaBuf[x][y] = 2;
							bFlag2 = TRUE;
						}

						// A영역 or B영역
						if (bFlag2 == TRUE)
						{
							if ((pstParticle->stParticleOp.rectData[Particle_Region_Center].RegionList.top <= y) && (pstParticle->stParticleOp.rectData[Particle_Region_Center].RegionList.bottom >= y)	
								&& (pstParticle->stParticleOp.rectData[Particle_Region_Center].RegionList.left <= x) && (pstParticle->stParticleOp.rectData[Particle_Region_Center].RegionList.right >= x))
							{
								if (pstParticle->stParticleOp.rectData[Particle_Region_Center].bEllipse == 1)
								{
									//X, Y 가 타원 안에 들어온다면
									if (EllipseDistanceSum(0, pstParticle->stParticleOp.rectData[Particle_Region_Center].RegionList.left, pstParticle->stParticleOp.rectData[Particle_Region_Center].RegionList.top, ((double)pstParticle->stParticleOp.rectData[Particle_Region_Center].RegionList.Width() / 2.0), ((double)pstParticle->stParticleOp.rectData[Particle_Region_Center].RegionList.Height() / 2.0), x, y) <= 1)
										m_cAreaBuf[x][y] = 1;
									else
										m_cAreaBuf[x][y] = 2;
								}
								else
								{
									m_cAreaBuf[x][y] = 1;
								}
							}
						}
					}
				}
			}
		}
	}
}

//=============================================================================
// Method		: EllipseDistanceSum
// Access		: public  
// Returns		: double
// Parameter	: BOOL bMode
// Parameter	: int iLeft
// Parameter	: int iTop
// Parameter	: double dbA
// Parameter	: double dbB
// Parameter	: int iX1
// Parameter	: int iY1
// Qualifier	:
// Last Update	: 2017/3/22 - 21:54
// Desc.		:
//=============================================================================
double CTI_Particle::EllipseDistanceSum(BOOL bMode, int iLeft, int iTop, double dbA, double dbB, int iX1, int iY1)
{
	double DistX, DistY, x, y;

	int XC = 0, YC = 0;

	if (bMode == FALSE)
	{
		XC = iLeft + (int)dbA;
		YC = iTop + (int)dbB;
	}
	else
	{
		XC = iLeft;
		YC = iTop;
	}

	x = (double)(iX1 - XC);
	y = (double)(iY1 - YC);

	DistX = (x * x) / (double)((dbA * dbA));
	DistY = (y * y) / (double)((dbB * dbB));

	return DistX + DistY;
}

//=============================================================================
// Method		: LumpDetection
// Access		: public  
// Returns		: void
// Parameter	: ST_LT_TI_Particle * pstParticle
// Parameter	: LPBYTE ImageBuf
// Qualifier	:
// Last Update	: 2017/3/27 - 11:34
// Desc.		:
//=============================================================================
void CTI_Particle::LumpDetection(ST_LT_TI_Particle *pstParticle, LPBYTE ImageBuf)
{
	CvRect	Rect;
	CvPoint	CurrPt;

	IplImage *OriginImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);
	IplImage *tempOriginImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_8U, 3);

	BYTE R, G, B;
	double Sum_Y;
	int iCnt = 0;

	for (int y = 0; y<(int)m_nHeight; y++)
	{
		for (int x = 0; x<(int)m_nWidth; x++)
		{
			B = ImageBuf[y*(m_nWidth * 3) + x * 3 + 0];
			G = ImageBuf[y*(m_nWidth * 3) + x * 3 + 1];
			R = ImageBuf[y*(m_nWidth * 3) + x * 3 + 2];
			tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3] = B;
			tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3 + 1] = G;
			tempOriginImage->imageData[y * tempOriginImage->widthStep + x * 3 + 2] = R;

			Sum_Y = (BYTE)((0.29900*R) + (0.58700*G) + (0.11400*B));
			OriginImage->imageData[y*OriginImage->widthStep + x] = (char)Sum_Y;
		}
	}

	int Particle_Size_Side	 = pstParticle->stParticleOp.rectData[Particle_Region_Side].iBruiseSize;
	int Particle_Size_Middle = pstParticle->stParticleOp.rectData[Particle_Region_Middle].iBruiseSize;
	int Particle_Size_Center = pstParticle->stParticleOp.rectData[Particle_Region_Center].iBruiseSize;

	cvAdaptiveThreshold(OriginImage, DilateImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 51, pstParticle->stParticleOp.iDustDis);
	cvNot(DilateImage, DilateImage);
	cvErode(DilateImage, DilateImage);

	for (int y = 0; y < (int)m_nHeight; y++)
	{
		for (int x = 0; x < (int)m_nWidth; x++)
		{
			if (m_cAreaBuf[x][y] == 0)
			{
				DilateImage->imageData[y*OriginImage->widthStep + x] = 0;
			}
		}
	}
	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;
	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	temp_contour = contour;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
		m_nCounter++;

	pRectArray = new CvRect[m_nCounter];

	double area = 0, arcCount = 0;
	m_nCounter = 0;
	double old_dist = 999999;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		Rect = cvContourBoundingRect(contour, 1);
		double circularity = (4.0*3.14*area) / (arcCount*arcCount);

		Rect = cvContourBoundingRect(contour, 1);

		int center_x, center_y;
		center_x = Rect.x + Rect.width / 2;
		center_y = Rect.y + Rect.height / 2;

		BYTE R, G, B;
		B = ImageBuf[center_y*(m_nWidth * 3) + center_x * 3 + 0];
		G = ImageBuf[center_y*(m_nWidth * 3) + center_x * 3 + 1];
		R = ImageBuf[center_y*(m_nWidth * 3) + center_x * 3 + 2];
		BOOL Rate_Part = FALSE;
		double  Rate = 0;
		if (Rect.width > Rect.height)
			Rate = (double)Rect.width / Rect.height;
		else
			Rate = (double)Rect.height / Rect.width;
		if (Rate >= 9.0)
			Rate_Part = TRUE;

		if (Rect.width < ((int)m_nWidth) && Rect.height < ((int)m_nHeight)){
			int Center_X = Rect.x + Rect.width / 2;
			int Center_Y = Rect.y + Rect.height / 2;
			// 중심 -> A
			if (Rect.width > Particle_Size_Center && Rect.height > Particle_Size_Center  && m_cAreaBuf[Center_X][Center_Y] == 1 && Rate_Part == FALSE)
			{
				pRectArray[m_nCounter] = Rect;
				m_nCounter++;

				double dist = GetDistance(center_x, center_y, m_nWidth / 2, m_nHeight / 2);
				if (old_dist > dist)
				{
					CurrPt.x = center_x - (m_nWidth / 2);
					CurrPt.y = center_y - (m_nHeight / 2);
					CurrPt.x = CurrPt.x / 2;
					CurrPt.y = CurrPt.y / 2;
					old_dist = dist;
				}
				cvRectangle(RGBResultImage, cvPoint(Rect.x, Rect.y), cvPoint(Rect.x + Rect.width, Rect.y + Rect.height), CV_RGB(0, 255, 0), 1, 8);
			}
			// 중간 -> B
			if (Rect.width > Particle_Size_Middle && Rect.height > Particle_Size_Middle  && m_cAreaBuf[Center_X][Center_Y] == 2 && Rate_Part == FALSE)
			{
				pRectArray[m_nCounter] = Rect;
				m_nCounter++;
				double dist = GetDistance(center_x, center_y, m_nWidth / 2, m_nHeight / 2);
				if (old_dist > dist)
				{
					CurrPt.x = center_x - (m_nWidth / 2);
					CurrPt.y = center_y - (m_nHeight / 2);
					CurrPt.x = CurrPt.x / 2;
					CurrPt.y = CurrPt.y / 2;
					old_dist = dist;
				}
				cvRectangle(RGBResultImage, cvPoint(Rect.x, Rect.y), cvPoint(Rect.x + Rect.width, Rect.y + Rect.height), CV_RGB(0, 255, 0), 1, 8);
			}
			// 바깥 -> C
			if (Rect.width > Particle_Size_Side && Rect.height > Particle_Size_Side  && m_cAreaBuf[Center_X][Center_Y] == 3 && Rate_Part == FALSE)
			{
				pRectArray[m_nCounter] = Rect;
				m_nCounter++;
				double dist = GetDistance(center_x, center_y, m_nWidth / 2, m_nHeight / 2);
				if (old_dist > dist)
				{
					CurrPt.x = center_x - (m_nWidth / 2);
					CurrPt.y = center_y - (m_nHeight / 2);
					CurrPt.x = CurrPt.x / 2;
					CurrPt.y = CurrPt.y / 2;
					old_dist = dist;
				}
				cvRectangle(RGBResultImage, cvPoint(Rect.x, Rect.y), cvPoint(Rect.x + Rect.width, Rect.y + Rect.height), CV_RGB(0, 255, 0), 1, 8);
				
			}
		}
	}


	for (int i = 0; i < (int)m_nCounter; i++)
	{
		pstParticle->stParticleData.ErrRegionList[i].RegionList.left	= pRectArray[i].x;
		pstParticle->stParticleData.ErrRegionList[i].RegionList.top		= pRectArray[i].y;
		pstParticle->stParticleData.ErrRegionList[i].RegionList.right	= pRectArray[i].x + pRectArray[i].width;
		pstParticle->stParticleData.ErrRegionList[i].RegionList.bottom	= pRectArray[i].y + pRectArray[i].height;

		iCnt++;
	}

	//농도
	pstParticle->stParticleData.iResultcnt = iCnt;

	delete[]pRectArray;

// 	cvSaveImage("D:\\test\\RGBResultImage.bmp", RGBResultImage);
	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&RGBResultImage);
	cvReleaseImage(&tempOriginImage);
}

//=============================================================================
// Method		: GetDistance
// Access		: public  
// Returns		: double
// Parameter	: int ix1
// Parameter	: int iy1
// Parameter	: int ix2
// Parameter	: int iy2
// Qualifier	:
// Last Update	: 2017/3/20 - 9:50
// Desc.		:
//=============================================================================
double CTI_Particle::GetDistance(int ix1, int iy1, int ix2, int iy2)
{
	return sqrt((double)((ix2 - ix1)*(ix2 - ix1)) + ((iy2 - iy1)*(iy2 - iy1)));
}

//=============================================================================
// Method		: MemoryCreat
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/28 - 12:55
// Desc.		:
//=============================================================================
void CTI_Particle::MemoryCreat()
{
	m_cAreaBuf = new char*[m_nWidth];

	for (UINT nSize = 0; nSize < m_nWidth; nSize++)
	{
		m_cAreaBuf[nSize] = new char[m_nHeight];
		memset(m_cAreaBuf[nSize], 0, sizeof(char)*m_nHeight);
	}

	m_fData = (float **)malloc(sizeof(float *)* m_nHeight);

	for (UINT nSize = 0; nSize < m_nHeight; nSize++)
		m_fData[nSize] = (float *)malloc(sizeof(float)* m_nWidth);
}

//=============================================================================
// Method		: MemoryDelete
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/28 - 12:55
// Desc.		:
//=============================================================================
void CTI_Particle::MemoryDelete()
{
	for (UINT nSize = 0; nSize < m_nWidth; nSize++)
		delete	m_cAreaBuf[nSize];

	delete[] m_cAreaBuf;

	for (UINT nSize = 0; nSize < m_nHeight; nSize++)
		free(m_fData[nSize]);
	
	free(m_fData);

}
