﻿#ifndef Wnd_MasterSet_h__
#define Wnd_MasterSet_h__

#pragma once

#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "TI_PicControl.h"
#include "VGGroupWnd.h"

// CWnd_MasterSet

enum enMasterSet_Static{
	STI_MST_SET_X = 0,
	STI_MST_SET_Y,
	STI_MST_SET_MasterInfo,
	STI_MST_SET_MasterSpc,
	STI_MST_SET_MasterOffset,
	STI_MST_SET_MAXNUM,
};
static LPCTSTR	g_szMasterSet_Static[] =
{
	_T("OFFSET X \n [Pix]"),
	_T("OFFSET Y \n [Pix]"),
	_T("MASTER \n INFO"),
	_T("MASTER \n SPC"),
	_T("MASTER \n OFFSET"),
	NULL
};

enum enMasterSet_Edit{
	EIT_MST_SET_MasterInfoX = 0,
	EIT_MST_SET_MasterInfoY,
	EIT_MST_SET_MasterSpcX,
	EIT_MST_SET_MasterSpcY,
	EIT_MST_SET_MasterOffsetX,
	EIT_MST_SET_MasterOffsetY,
	EIT_MST_SET_MAXNUM,
};

class CWnd_MasterSet : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MasterSet)

public:
	CWnd_MasterSet();
	virtual ~CWnd_MasterSet();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedTest();
	afx_msg void	OnBnClickedSave();
	afx_msg void	OnBnClickedExit();
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage(MSG* pMsg);

	CFont			m_font;
	CFont			m_font_Default;

	CMFCButton		m_bn_MasterTest;
	CMFCButton		m_bn_MasterSave;
	CMFCButton		m_bn_MasterExit;

	CVGStatic		m_st_UIItem;
	CVGStatic		m_st_Item[STI_MST_SET_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[EIT_MST_SET_MAXNUM];
	CVGGroupWnd		m_Group;

	ST_ModelInfo	*m_pstModelInfo;

public:

	void		GetUIData		();
	void		SetUIData		();
	void		SetMasterData	(__in int iOffsetX, __in int iOffsetY);
	void		PermissionMode		(enPermissionMode InspMode);


	void		SetModelInfo(__in ST_ModelInfo* pModelInfo)
	{
		if (pModelInfo == NULL)
			return;

		m_pstModelInfo = pModelInfo;
		SetUIData();
	}

};
#endif // Wnd_CameraView_h__
