﻿//*****************************************************************************
// Filename	: Wnd_WorklistView.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistView_h__
#define Wnd_WorklistView_h__

#pragma once

//#include "VGStatic.h"
#include "List_Worklist.h"

#include "List_Work_Total.h"
#include "List_Work_Current.h"
#include "List_Work_EIAJ.h"

// #include "List_Work_CenterPoint.h"
// #include "List_Work_SFR.h"
// #include "List_Work_Rotate.h"
// #include "List_Work_Particle.h"

//=============================================================================
// Wnd_WorklistView
//=============================================================================
class CWnd_WorklistView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistView)

public:
	CWnd_WorklistView();
	virtual ~CWnd_WorklistView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );


	CFont						m_font_Default;
	CFont						m_font_Data;

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

	CList_Work_Total			m_List_Total;
	CList_Work_Current			m_List_Current;
	CList_Work_EIAJ				m_List_EIAJ;
 	CList_Work_CenterPoint		m_List_CenterPoint;
// 	CList_Work_SFR				m_List_SFR;
// 	CList_Work_Rotate			m_List_Rotate;
// 	CList_Work_Particle			m_List_Particle;

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistView_h__


