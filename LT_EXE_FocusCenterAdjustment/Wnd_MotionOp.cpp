﻿// Wnd_MotionOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MotionOp.h"

// CWnd_MotionOp

IMPLEMENT_DYNAMIC(CWnd_MotionOp, CWnd)

CWnd_MotionOp::CWnd_MotionOp()
{
	m_pstDevice				= NULL;
	m_nAxis					= 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_MotionOp::~CWnd_MotionOp()
{
	m_font.DeleteObject		();
}

BEGIN_MESSAGE_MAP(CWnd_MotionOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_MO_BN_MO_SAVE,	OnBnClickedBnSave	)
	ON_BN_CLICKED(IDC_MO_BN_MO_SETTING, OnBnClickedBnSetting)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/28 - 17:02
// Desc.		:
//=============================================================================
int CWnd_MotionOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER;
	DWORD dwStyle	= WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = ST_MO_PULSE; nIdx < ST_MO_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 10.0F);
		m_st_Item[nIdx].Create(g_szMotionOpName[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < CB_MO_MAXNUM; nIdx++)
	{
		m_cb_Item[nIdx].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_MO_CB_PULSE_DATA + nIdx);
		m_cb_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; NULL != g_szMotorOpPuls[nIdx]; nIdx++)
		m_cb_Item[CB_MO_PULSE].AddString(g_szMotorOpPuls[nIdx]);

	for (UINT nIdx = 0; NULL != g_szMotorOpEnc[nIdx]; nIdx++)
		m_cb_Item[CB_MO_ENC].AddString(g_szMotorOpEnc[nIdx]);

	for (UINT nIdx = 0; NULL != g_szMotorOpServo[nIdx]; nIdx++)
		m_cb_Item[CB_MO_SERVO].AddString(g_szMotorOpServo[nIdx]);

	for (UINT nIdx = 0; NULL != g_szMotorOpUse[nIdx]; nIdx++)
		m_cb_Item[CB_MO_USED].AddString(g_szMotorOpUse[nIdx]);

	for (UINT nIdx = 0; NULL != g_szMotorOpLevel[nIdx]; nIdx++)
	{
		m_cb_Item[CB_MO_HOME].AddString(g_szMotorOpLevel[nIdx]);
		m_cb_Item[CB_MO_ALRAM].AddString(g_szMotorOpLevel[nIdx]);
		m_cb_Item[CB_MO_HLIMIT].AddString(g_szMotorOpLevel[nIdx]);
		m_cb_Item[CB_MO_LLIMIT].AddString(g_szMotorOpLevel[nIdx]);
		m_cb_Item[CB_MO_POSITION].AddString(g_szMotorOpLevel[nIdx]);
		m_cb_Item[CB_MO_EMO].AddString(g_szMotorOpLevel[nIdx]);
	}

	for (UINT nIdx = 0; nIdx < BN_MO_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szMotionOpButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_MO_BN_MO_SETTING + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; nIdx < ED_MO_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwEtStyle, rectDummy, this, IDC_MO_ED_MO_NUMBER + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0"));
	}

	m_ed_Item[ED_MO_VAL].SetValidChars(_T("0123456789.-"));
	m_ed_Item[ED_MO_ACC].SetValidChars(_T("0123456789.-"));
	m_ed_Item[ED_MO_NUMBER].SetValidChars(_T("0123456789.-"));

	m_Group_Nmae.SetTitle(L"MOTOR PARAMETER SETTING");

	if (!m_Group_Nmae.Create(_T("MOTOR PARAMETER SETTING"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MotionOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	cx = (int)(cx * MO_WIDTH_OFFSET);
	
	int iMargin  = 7;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = iMargin;
	int iWidth	 = cx;
	int iHeight  = cy - iMargin - iMargin;
	int iEdWidth = (iWidth - iMargin - iMargin - (iSpacing * 5)) / 6;
	int iBnWidth = (iWidth - iMargin - iMargin - (iSpacing * 2)) / 3;

	int iEditHeight	= 24;
	int iStTitleNameH = 37;

	m_Group_Nmae.MoveWindow(iLeft, iTop, iWidth, iHeight);

	iTop += iSpacing * 6;
	iLeft = iMargin;

	for (UINT nIdx = 0; nIdx < ST_MO_ENC; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;

		m_cb_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;
	}

	iLeft = iMargin;
	iTop += iEditHeight + iSpacing;

	for (UINT nIdx = ST_MO_ENC; nIdx < ST_MO_HLIMIT; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;

		m_cb_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;
	}

	iLeft = iMargin;
	iTop += iEditHeight + iSpacing;

	for (UINT nIdx = ST_MO_HLIMIT; nIdx < ST_MO_USE; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;

		m_cb_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;
	}

	iLeft = iMargin;
	iTop += iEditHeight + iSpacing;

	for (UINT nIdx = ST_MO_USE; nIdx < ST_MO_NUMBER; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;

		m_cb_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;
	}

	for (UINT nIdx = 0; nIdx < ED_MO_VAL; nIdx++)
	{
		m_st_Item[nIdx + ST_MO_NUMBER].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;

		m_ed_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;
	}

	iLeft = iMargin;
	iTop += iEditHeight + iSpacing;

	for (UINT nIdx = ED_MO_VAL; nIdx < ED_MO_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx + ST_MO_NUMBER].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;

		m_ed_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;
	}

	for (UINT nIdx = 0; nIdx < BN_MO_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;
	}
}

//=============================================================================
// Method		: OnBnClickedBnSave
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 9:56
// Desc.		:
//=============================================================================
void CWnd_MotionOp::OnBnClickedBnSave()
{
	// UI 데이터 수집
	GetUpdataData();

	// 수집된 데이터 셋팅
	m_pstDevice->MotionManager.SetMotionSetting(m_nAxis);

	// 수집된 데이터 저장
	m_pstDevice->MotionManager.SaveMotionParam();

	SetUpdataData();
	GetOwner()->SendNotifyMessage(WM_UPDATA_AXIS, 0, 0);
}

//=============================================================================
// Method		: OnBnClickedBnSetting
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/4/6 - 10:20
// Desc.		:
//=============================================================================
void CWnd_MotionOp::OnBnClickedBnSetting()
{
	// UI 데이터 수집
	GetUpdataData();

	// 수집된 데이터 셋팅
	m_pstDevice->MotionManager.SetMotionSetting(m_nAxis);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
BOOL CWnd_MotionOp::PreTranslateMessage(MSG* pMsg)
{
	CString str;
	long lAxis = 0;

	m_ed_Item[ED_MO_NUMBER].GetWindowText(str);
	lAxis = _ttol(str);

	if (lAxis > m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt - 1)
	{
		lAxis = m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt - 1;

		str.Format(_T("%d"), lAxis);
		m_ed_Item[ED_MO_NUMBER].SetWindowText(str);
	}

	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if ((::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 86)) // Ctrl + V
		{
			CWnd* pWnd = CWnd::FromHandle(pMsg->hwnd);
			pWnd->SetWindowText(_T(""));
			pMsg->message = WM_PASTE;
		}

		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 67)  // Ctrl + C
			pMsg->message = WM_COPY;

// 		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 90)  // Ctrl + Z
// 			pMsg->message = WM_UNDO;

		break;
	}

	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_MotionOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetSelectAxis
// Access		: public  
// Returns		: void
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/3 - 11:49
// Desc.		:
//=============================================================================
void CWnd_MotionOp::SetSelectAxis(UINT nAxis)
{
	m_nAxis = nAxis;
	SetUpdataData();
}

//=============================================================================
// Method		: SetUpdataData
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 10:29
// Desc.		:
//=============================================================================
void CWnd_MotionOp::SetUpdataData()
{
	if (m_pstDevice == NULL)
		return;

	if (m_pstDevice->MotionManager.m_AllMotorData.pMotionParam == NULL)
		return;

	CString strData;

	ST_MotionParam stMotorParam = m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis];
	
	// Puls Output
	m_cb_Item[CB_MO_PULSE].SetCurSel(stMotorParam.iAxisOutputMethod);

	// Servo Level
	m_cb_Item[CB_MO_SERVO].SetCurSel(stMotorParam.iAxisServoLevel);

	// In Position Level
	m_cb_Item[CB_MO_POSITION].SetCurSel(stMotorParam.iAxisInpositionLevel);

	// Enc Input Level
	m_cb_Item[CB_MO_ENC].SetCurSel(stMotorParam.iAxisEncInputMethod);

	// Alram Level
	m_cb_Item[CB_MO_ALRAM].SetCurSel(stMotorParam.iAxisAlarmLevel);

	// Home Level
	m_cb_Item[CB_MO_HOME].SetCurSel(stMotorParam.iAxisHomeSenLevel);

	// PosLimit Level
	m_cb_Item[CB_MO_HLIMIT].SetCurSel(stMotorParam.iAxisPosLimitSenLevel);

	// NegLimit Level
	m_cb_Item[CB_MO_LLIMIT].SetCurSel(stMotorParam.iAxisNegLimitSenLevel);

	// EMO Level
	m_cb_Item[CB_MO_EMO].SetCurSel(stMotorParam.iAxisEmergencyLevel);

	// USE AXIS
	m_cb_Item[CB_MO_USED].SetCurSel(stMotorParam.iAxisUse);

	// USE Number
	strData.Format(_T("%d"), stMotorParam.iAxisNum);
	m_ed_Item[ED_MO_NUMBER].SetWindowText(strData);

	// AXIS NAME
	m_ed_Item[ED_MO_NAME].SetWindowText(stMotorParam.szAxisName);

	// AXIS VAL
	strData.Format(_T("%.1f"), stMotorParam.dbVel);
	m_ed_Item[ED_MO_VAL].SetWindowText(strData);

	// AXIS ACC
	strData.Format(_T("%.1f"), stMotorParam.dbAcc);
	m_ed_Item[ED_MO_ACC].SetWindowText(strData);
}

//=============================================================================
// Method		: GetUpdataData
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 10:41
// Desc.		:
//=============================================================================
void CWnd_MotionOp::GetUpdataData()
{
	if (m_pstDevice == NULL)
		return;

	CString strData;

	// Puls Output
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].iAxisOutputMethod = m_cb_Item[CB_MO_PULSE].GetCurSel();

	// Servo Level
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].iAxisServoLevel = m_cb_Item[CB_MO_SERVO].GetCurSel();

	// In Position Level
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].iAxisInpositionLevel = m_cb_Item[CB_MO_POSITION].GetCurSel();

	// Enc Input Level
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].iAxisEncInputMethod = m_cb_Item[CB_MO_ENC].GetCurSel();

	// Alram Level
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].iAxisAlarmLevel = m_cb_Item[CB_MO_ALRAM].GetCurSel();

	// Home Level
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].iAxisHomeSenLevel = m_cb_Item[CB_MO_HOME].GetCurSel();

	// PosLimit Level
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].iAxisPosLimitSenLevel = m_cb_Item[CB_MO_HLIMIT].GetCurSel();

	// NegLimit Level
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].iAxisNegLimitSenLevel = m_cb_Item[CB_MO_LLIMIT].GetCurSel();

	// EMO Level
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].iAxisEmergencyLevel = m_cb_Item[CB_MO_EMO].GetCurSel();

	// USE AXIS
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].iAxisUse = m_cb_Item[CB_MO_USED].GetCurSel();

	// USE Number
	m_ed_Item[ED_MO_NUMBER].GetWindowText(strData);
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].iAxisNum = _ttoi(strData);

	// Axis Name
	m_ed_Item[ED_MO_NAME].GetWindowText(strData);
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].szAxisName = strData;

	// AXIS VAL
	m_ed_Item[ED_MO_VAL].GetWindowText(strData);
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].dbVel = _ttoi(strData);

	// AXIS ACC
	m_ed_Item[ED_MO_ACC].GetWindowText(strData);
	m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[m_nAxis].dbAcc = _ttoi(strData);
}
