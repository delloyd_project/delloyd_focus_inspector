﻿//*****************************************************************************
// Filename	: 	Def_TestDevice.h
// Created	:	2016/5/26 - 5:44
// Modified	:	2016/5/26 - 5:44
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_TestDevice_h__
#define Def_TestDevice_h__

#include "Def_CompileOption.h"

// Device
#include "CatWrapper.h"
#include "BCRCtrl.h"
#include "MES_LG.h"
#include "PCBCamBrd.h"
#include "IndicatorSensor.h"	
#include "MotionSequence.h"
#include "MotionManager.h"
#include "DigitalIOCtrl.h"
#include "IF_Illumination.h"

#include "Def_Test.h"
#include "Def_Motion.h"

#include "cv.h"
#include "highgui.h"

// 핸들러 / 테스터 선택 ------------------------------------
typedef struct _tag_Device
{
	CCatWrapper			Cat3DCtrl;						// 컴아트 (NTSC) 그래버 보드

//	CDAQWrapper			DAQCtrl;						// DAQ (LVDS) 그래버 보드	 // 제외

	CMES_LG				Mes;														 // 제외
	
	CBCRCtrl			BCR;							// 바코드 리더기

	CPCBCamBrd			PCBCamBrd[CAM_MAX_NUM];			// Camera 제어보드

//	CPCBLightBrd		PCBLightBrd[MAX_PCB_LIGHT_CNT];	// Light 제어보드			 // 제외

	CIndicatorSensor	Indicator[Indicator_Max];		//Indicator 

	CMotionSequence		MotionSequence;

	CMotionManager		MotionManager;

	CDigitalIOCtrl		DigitalIOCtrl;

//	CLaserSensorCtrl	LaserSensor;					// 변위 센서				 // 제외

//	CVisionCam			VisionCam;						// 비전 카메라

//	CIF_Illumination	IF_Illumination;				// 비전 조명

}ST_Device, *PST_Device;


#endif // Def_TestDevice_h__
