﻿// List_Work_Reslent.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_EIAJ.h"

// CList_Work_EIAJ
IMPLEMENT_DYNAMIC(CList_Work_EIAJ, CListCtrl)

CList_Work_EIAJ::CList_Work_EIAJ()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_EIAJ::~CList_Work_EIAJ()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_Work_EIAJ, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CList_Work_EIAJ 메시지 처리기입니다.
int CList_Work_EIAJ::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);
	InitHeader();
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 12:44
// Desc.		:
//=============================================================================
void CList_Work_EIAJ::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	CRect rc;
	GetClientRect(&rc);

	for (int nCol = Resl_W_Time; nCol < Resl_W_MaxCol; nCol++)
	{
		//SetColumnWidth(nCol, (rc.Width() - iHeaderWidth_Resl_Worklist[Resl_W_Time]) / (12 - Resl_W_Time));
		SetColumnWidth(nCol, iHeaderWidth_Resl_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
BOOL CList_Work_EIAJ::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: Header_MaxNum
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
UINT CList_Work_EIAJ::Header_MaxNum()
{
	return (UINT)Resl_W_MaxCol;
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_EIAJ::InitHeader()
{
	for (int nCol = 0; nCol < Resl_W_MaxCol; nCol++)
		InsertColumn(nCol, g_lpszHeader_Resl_Worklist[nCol], iListAglin_Resl_Worklist[nCol], iHeaderWidth_Resl_Worklist[nCol]);
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_EIAJ::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;
	int iNewCount = GetItemCount();


	InsertItem(iNewCount, _T(""));

	SetRectRow(iNewCount, pstCamInfo);
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_EIAJ::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	CString strText;

	strText.Format(_T("%s-%s"), pstCamInfo->szDay, pstCamInfo->szTime);
	SetItemText(nRow, Resl_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, Resl_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, Resl_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, Resl_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, Resl_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, Resl_W_Barcode, strText);

	strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	SetItemText(nRow, Resl_W_Operator, strText);

	if (pstCamInfo->nJudgment == TR_UserStop)
	{
		strText.Format(_T("%s"), g_TestEachResult[TR_UserStop].szText);
		SetItemText(nRow, Resl_W_Result, strText);

		strText.Format(_T(""));
		for (int i = Resl_W_LEFT; i < Resl_W_MaxCol; i++)
		{
			SetItemText(nRow, i, strText);
		}

	}
	else
	{
		strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stEIAJData.nResult].szText);
		SetItemText(nRow, Resl_W_Result, strText);

		if (pstCamInfo->stEIAJData.nResult == TER_Init)
		{
			for (UINT nIdx = Resl_W_LEFT; nIdx < Resl_W_MaxCol; nIdx++)
			{
				strText.Format(_T("X"));
				SetItemText(nRow, nIdx, strText);
			}
		}
		else
		{
			for (UINT nIdx = 0; nIdx < 12; nIdx++)
			{
				if (pstCamInfo->stEIAJData.bUse[nIdx + 2] == TRUE)
					strText.Format(_T("%d"), pstCamInfo->stEIAJData.nValueResult[nIdx + 2]);
				else
					strText.Format(_T("X"));

				SetItemText(nRow, Resl_W_LEFT + nIdx, strText);
			}
		}
	}
}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 12:47
// Desc.		:
//=============================================================================
void CList_Work_EIAJ::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = Resl_W_MaxCol;
	CString temp[Resl_W_MaxCol];
	for (int t = 0; t < Resl_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
