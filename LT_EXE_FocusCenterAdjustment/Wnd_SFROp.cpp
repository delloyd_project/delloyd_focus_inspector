﻿// Wnd_SFROp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_SFROp.h"
#include "resource.h"

typedef enum SFROp_ID
{
	IDC_BTN_SFR_TEST = 1001,
	IDC_BTN_SFR_TEST_STOP,
	IDC_BTN_SFR_DEFULT,
	IDC_BTN_SFR_SMOOTH,
	IDC_BTN_SFR_FIELD,
	IDC_BTN_SFR_EDGE,
	IDC_BTN_SFR_DISTORTION,
	IDC_CB_SFR_DATA = 2001,
};

IMPLEMENT_DYNAMIC(CWnd_SFROp, CWnd)

CWnd_SFROp::CWnd_SFROp()
{
	m_pstModelInfo = NULL;

	m_bTest_Flag = FALSE;

	VERIFY(m_font_Data.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_SFROp::~CWnd_SFROp()
{
	m_font_Data.DeleteObject();
	m_bTest_Flag = FALSE;
}

BEGIN_MESSAGE_MAP(CWnd_SFROp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BTN_SFR_DEFULT,			OnBnClickedBnDefaultSet )
	ON_BN_CLICKED(IDC_BTN_SFR_TEST,				OnBnClickedBnTest		)
	ON_BN_CLICKED(IDC_BTN_SFR_TEST_STOP,		OnBnClickedBnTestStop	)
	ON_BN_CLICKED(IDC_BTN_SFR_SMOOTH,			OnBnClickedBnSmooth		)
	ON_BN_CLICKED(IDC_BTN_SFR_FIELD,			OnBnClickedBnField		)
	ON_BN_CLICKED(IDC_BTN_SFR_EDGE,				OnBnClickedBnEdge		)
	ON_BN_CLICKED(IDC_BTN_SFR_DISTORTION,		OnBnClickedBnDistortion	)
END_MESSAGE_MAP()

// CWnd_SFROp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/13 - 13:29
// Desc.		:
//=============================================================================
int CWnd_SFROp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < BTN_SF_MAXNUM; nIdex++)
	{
		if (nIdex == BTN_SF_SMOOTH || nIdex == BTN_SF_FIELD || nIdex == BTN_SF_EDGE || nIdex == BTN_SF_DISTORTION)
			m_bn_Item[nIdex].Create(g_szSFR_Button[nIdex], dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_SFR_TEST + nIdex);
		else
			m_bn_Item[nIdex].Create(g_szSFR_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_SFR_TEST + nIdex);

		m_bn_Item[nIdex].SetFont(&m_font_Data);
	}

	for (UINT nIdex = 0; nIdex < STI_SF_MAXNUM; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 8.0F);
		m_st_Item[nIdex].Create(g_szSFR_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < Edt_SF_MAXNUM; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, CRect(0, 0, 0, 0), this, 2000 + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0.00"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	m_bn_Item[BTN_SF_FIELD].SetImage(IDB_UNCHECKED_16);
	m_bn_Item[BTN_SF_FIELD].SetCheckedImage(IDB_CHECKED_16);
	m_bn_Item[BTN_SF_FIELD].SizeToContent();
	m_bn_Item[BTN_SF_FIELD].SetCheck(BST_UNCHECKED);

	m_bn_Item[BTN_SF_EDGE].SetImage(IDB_UNCHECKED_16);
	m_bn_Item[BTN_SF_EDGE].SetCheckedImage(IDB_CHECKED_16);
	m_bn_Item[BTN_SF_EDGE].SizeToContent();
	m_bn_Item[BTN_SF_EDGE].SetCheck(BST_UNCHECKED);

	m_bn_Item[BTN_SF_DISTORTION].SetImage(IDB_UNCHECKED_16);
	m_bn_Item[BTN_SF_DISTORTION].SetCheckedImage(IDB_CHECKED_16);
	m_bn_Item[BTN_SF_DISTORTION].SizeToContent();
	m_bn_Item[BTN_SF_DISTORTION].SetCheck(BST_UNCHECKED);

	m_bn_Item[BTN_SF_SMOOTH].SetImage(IDB_UNCHECKED_16);
	m_bn_Item[BTN_SF_SMOOTH].SetCheckedImage(IDB_CHECKED_16);
	m_bn_Item[BTN_SF_SMOOTH].SizeToContent();
	m_bn_Item[BTN_SF_SMOOTH].SetCheck(BST_UNCHECKED);
	m_bn_Item[BTN_SF_SMOOTH].ShowWindow(SW_HIDE);

	m_ListSFROp.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, 10);

	m_CbDataType.Create(WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_SFR_DATA);
	m_CbDataType.SetFont(&m_font_Data);
	for (int i = 0; i < enSFRDataType_Max; i++)
	{
		m_CbDataType.AddString(g_szSFRDataType[i]);
	}
	m_CbDataType.SetCurSel(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/3/13 - 13:29
// Desc.		:
//=============================================================================
void CWnd_SFROp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int Static_W = iWidth / 4;
	int Static_H = 26;
	int Combo_W = iWidth / 3;
	int Combo_H = 26;

	int TempLeft = iLeft;

	int iBtnWidth = iWidth / 7;
	int iBtnHeight = 26;

	iLeft = iWidth - iBtnWidth + iMargin;
	m_bn_Item[BTN_SF_SFR_TEST].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop += iBtnHeight + iSpacing;
	m_bn_Item[BTN_SF_SFR_TEST_STOP].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop += iBtnHeight + iSpacing;
	m_bn_Item[BTN_SF_DEFAULT_SET].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop += iBtnHeight + iSpacing;
	m_bn_Item[BTN_SF_FIELD].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop += iBtnHeight + iSpacing;
	m_bn_Item[BTN_SF_EDGE].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop += iBtnHeight + iSpacing;
	m_bn_Item[BTN_SF_DISTORTION].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop += iBtnHeight + iSpacing;
	m_CbDataType.MoveWindow(iLeft, iTop, iBtnWidth, 100);

	iTop	= iMargin;
	iLeft	= iMargin;

	m_st_Item[STI_SF_PIXSIZE].MoveWindow(iLeft, iTop, Static_W, Static_H);
	iLeft += Static_W + iSpacing;
	m_ed_Item[Edt_SF_PIXELSIZE].MoveWindow(iLeft, iTop, Combo_W, Static_H);
	

	iLeft = iMargin;
	iTop += Static_H + iSpacing;

	int List_W = (Static_W + Static_W + Combo_W + iMargin);
	int List_H = iHeight - Static_H - iMargin;

	m_ListSFROp.MoveWindow(iLeft, iTop, List_W, List_H);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/3/13 - 13:29
// Desc.		:
//=============================================================================
void CWnd_SFROp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);
	
	if (bShow)
	{
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_SFR, 0);
	}
	else
	{
		OnBnClickedBnTestStop();
		DoEvents(100);
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_Standby, 0);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/3/13 - 13:29
// Desc.		:
//=============================================================================
BOOL CWnd_SFROp::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
BOOL CWnd_SFROp::PreTranslateMessage(MSG* pMsg)
{
	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnBnClickedBnTest
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/13 - 17:05
// Desc.		:
//=============================================================================
void CWnd_SFROp::OnBnClickedBnTest()
{
	m_bTest_Flag = TRUE;
	m_bn_Item[BTN_SF_SFR_TEST].EnableWindow(FALSE);

// 	m_TIProcessing.DistortionCorrection(m_stModelInfo.nDistortion);
// 	Sleep(500);
// 
// 	m_TIProcessing.EdgeOnOff(m_stModelInfo.stSFR.stSFROp.bEdge);
// 	Sleep(500);

	while (m_bTest_Flag == TRUE)
	{
		DoEvents(50);
		GetOwner()->SendNotifyMessage(WM_MANUAL_TEST, 1, MT_Cmd_SFR);
	}
}

//=============================================================================
// Method		: OnBnClickedBnTestStop
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/13 - 17:05
// Desc.		:
//=============================================================================
void CWnd_SFROp::OnBnClickedBnTestStop()
{
	if (m_pstModelInfo == NULL)
		return;

	for (int q = 0; q < Region_SFR_MaxEnum; q++)
	{
		m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[q].nPos_X = m_pstModelInfo->stSFR.stSFROp.stSFR_Region[q].nPos_X;
		m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[q].nPos_Y = m_pstModelInfo->stSFR.stSFROp.stSFR_Region[q].nPos_Y;
	}
	
	SetUI();

	m_bTest_Flag = FALSE;
	m_bn_Item[BTN_SF_SFR_TEST].EnableWindow(TRUE);
}


//=============================================================================
// Method		: OnBnClickedBnDefaultSet
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/13 - 17:05
// Desc.		:
//=============================================================================
void CWnd_SFROp::OnBnClickedBnDefaultSet()
{
}
//=============================================================================
// Method		: SetUI
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/7 - 22:13
// Desc.		:
//=============================================================================
void CWnd_SFROp::SetUI()
{
	CString str;

	if (m_pstModelInfo == NULL)
		return;

	for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
		m_pstModelInfo->stSFR.stSFROp.stSFR_Region[nIdx] = m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[nIdx];

	m_ListSFROp.SetPtr_SFR(&m_pstModelInfo->stSFR);
	m_ListSFROp.InsertFullData();

	str.Format(_T("%.2f"), m_pstModelInfo->stSFR.stSFROp.dbPixelSizeW);
	m_ed_Item[Edt_SF_PIXELSIZE].SetWindowText(str);

	m_bn_Item[BTN_SF_FIELD].SetCheck(m_pstModelInfo->stSFR.stSFROp.bField);
	m_bn_Item[BTN_SF_EDGE].SetCheck(m_pstModelInfo->stSFR.stSFROp.bEdge);
	m_bn_Item[BTN_SF_DISTORTION].SetCheck(m_pstModelInfo->stSFR.stSFROp.bDistortion);

	m_CbDataType.SetCurSel(m_pstModelInfo->stSFR.stSFROp.nDataType);
}

//=============================================================================
// Method		: GetUI
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/7 - 22:13
// Desc.		:
//=============================================================================
void CWnd_SFROp::GetUI()
{
	CString str;

	m_ed_Item[Edt_SF_PIXELSIZE].GetWindowText(str);
	m_pstModelInfo->stSFR.stSFROp.dbPixelSizeW = _ttof(str);

	m_pstModelInfo->stSFR.stSFROp.bField = m_bn_Item[BTN_SF_FIELD].GetCheck();
	m_pstModelInfo->stSFR.stSFROp.bEdge = m_bn_Item[BTN_SF_EDGE].GetCheck();
	m_pstModelInfo->stSFR.stSFROp.bDistortion = m_bn_Item[BTN_SF_DISTORTION].GetCheck();

	m_pstModelInfo->stSFR.stSFROp.nDataType = m_CbDataType.GetCurSel();
}

//=============================================================================
// Method		: OnBnClickedBnGraph
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/28 - 14:09
// Desc.		:
//=============================================================================
void CWnd_SFROp::OnBnClickedBnSmooth()
{
	if (m_bn_Item[BTN_SF_SMOOTH].GetCheck())
	{
		m_pstModelInfo->stSFR.stSFROp.bSmoothMode = TRUE;
	}
	else
	{
		m_pstModelInfo->stSFR.stSFROp.bSmoothMode = FALSE;
	}
}

void CWnd_SFROp::OnBnClickedBnField()
{
	if (m_bn_Item[BTN_SF_FIELD].GetCheck())
	{
		m_pstModelInfo->stSFR.stSFROp.bField = TRUE;
	}
	else
	{
		m_pstModelInfo->stSFR.stSFROp.bField = FALSE;
	}
}

void CWnd_SFROp::OnBnClickedBnEdge()
{
	if (m_bn_Item[BTN_SF_EDGE].GetCheck())
	{
		m_pstModelInfo->stSFR.stSFROp.bEdge = TRUE;
		// Edge on
		GetOwner()->SendNotifyMessage(WM_EDGE_ONOFF, 1, 0);
	}
	else
	{
		m_pstModelInfo->stSFR.stSFROp.bEdge = FALSE;
		// Edge off
		GetOwner()->SendNotifyMessage(WM_EDGE_ONOFF, 0, 0);
	}
}

void CWnd_SFROp::OnBnClickedBnDistortion()
{
	if (m_bn_Item[BTN_SF_DISTORTION].GetCheck())
	{
		m_pstModelInfo->stSFR.stSFROp.bDistortion = TRUE;
		// Edge on
		GetOwner()->SendNotifyMessage(WM_DISTORTION_CORRECT, 1, 0);
	}
	else
	{
		m_pstModelInfo->stSFR.stSFROp.bDistortion = FALSE;
		// Edge off
		GetOwner()->SendNotifyMessage(WM_DISTORTION_CORRECT, 0, 0);
	}
}

//=============================================================================
// Method		: IsTest
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/8/7 - 22:13
// Desc.		:
//=============================================================================
BOOL CWnd_SFROp::IsTest()
{
	return m_bTest_Flag;
}

//=============================================================================
// Method		: SetStatusEngineerMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2017/8/7 - 22:13
// Desc.		:
//=============================================================================
void CWnd_SFROp::SetStatusEngineerMode(__in enPermissionMode InspMode)
{
	if (InspMode == Permission_Engineer)
	{
		m_bn_Item[BTN_SF_SMOOTH].ShowWindow(SW_SHOW);
	}
	else
	{
		m_bn_Item[BTN_SF_SMOOTH].ShowWindow(SW_HIDE);
	}
}
