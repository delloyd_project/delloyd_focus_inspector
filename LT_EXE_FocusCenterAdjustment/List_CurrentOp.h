﻿#ifndef List_CurrentOp_h__
#define List_CurrentOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_CurrOp
{
	CurrOp_Channel,
//	CurrOp_Voltage,
	CurrOp_Min,
	CurrOp_Max,
	CurrOp_Offset,
	CurrOp_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_CurrOp[] =
{
	_T("CH"),
	//_T("Voltage [ V ]"),
	_T("Min [ mA ]"),
	_T("Max [ mA ]"),
	_T("Offset [ mA ]"),
	NULL
};

typedef enum enListItemNum_
{
	CurrOp_Site1,
// 	CurrOp_Site2,
// 	CurrOp_Site3,
// 	CurrOp_Site4,
// 	CurrOp_Site5,
	CurrOp_ItemNum,
};

static const TCHAR*	g_lpszItem_CurrOp[] =
{
	_T("1"),
// 	_T("3.3 V"),
// 	_T("9.0 V"),
// 	_T("14.7 V"),
// 	_T("-5.7 V"),
	NULL
};

const int	iListAglin_CurrOp[] =
{
	LVCFMT_LEFT,
	//LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_CurrOp[] =
{
	60,
	//95,
	95,
	95,
	140,
	95,
	95,
};
// List_CurrentInfo

class CList_CurrentOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_CurrentOp)

public:
	CList_CurrentOp();
	virtual ~CList_CurrentOp();

	void InitHeader();
	void InsertFullData();
	void SetRectRow(UINT nRow);
	void GetCellData();

	void SetPtr_Current(ST_LT_TI_Current *pstCurrent)
	{
		if (pstCurrent == NULL)
			return;

		m_pstCurrent = pstCurrent;
	};

protected:

	ST_LT_TI_Current*	m_pstCurrent;

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;

	BOOL		UpdateCellData(UINT nRow, UINT nCol, int  iValue);
	BOOL		UpdateCellData_double(UINT nRow, UINT nCol, double dValue);
	BOOL		CheckRectValue(__in const CRect* pRegionz);

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnEnKillFocusECurrOpellEdit();

	afx_msg BOOL	OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()
};

#endif // List_CurrentInfo_h__
