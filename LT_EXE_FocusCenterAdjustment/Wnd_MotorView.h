﻿//*****************************************************************************
// Filename	: Wnd_MotorView.h
// Created	: 2017/04/03
// Modified	: 2017/04/03
//
// Author	: KHO
//	
// Purpose	: 기본 화면용 윈도우
//*****************************************************************************
#ifndef Wnd_MotorView_h__
#define Wnd_MotorView_h__

#pragma once

#include "Wnd_BaseView.h"
#include "Def_TestDevice.h"
#include "VGStatic.h"
#include "Wnd_OriginOp.h"
#include "Wnd_MotionOp.h"
#include "Wnd_MotionTeach.h"
#include "Wnd_MotionTable.h"
#include "Wnd_MotionCtr.h"
#include "File_WatchList.h"

//=============================================================================
// CWnd_MotorView
//=============================================================================
class CWnd_MotorView : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_MotorView)

public:
	CWnd_MotorView();
	virtual ~CWnd_MotorView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnNew	();
	afx_msg void	OnCbnSelendokFile	();

	afx_msg LRESULT	OnMotorUpdata		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnMotorSelect		(WPARAM wParam, LPARAM lParam);

	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	CMFCTabCtrl			m_tc_Option;

	ST_Device*			m_pDevice;

	CWnd_OriginOp		m_Wnd_OriginOp;
	CWnd_MotionOp		m_Wnd_MotionOp;
	CWnd_MotionTeach	m_Wnd_MotionTeach;
	CWnd_MotionTable	m_Wnd_MotionTable;
	CWnd_MotionCtr		m_Wnd_MotionCtr;

	CString				m_szMotorpath;
	CFont				m_font;

	CVGStatic			m_st_File;
	CComboBox			m_cb_File;
	CButton				m_bn_NewFile;

	CFile_WatchList		m_IniWatch;

	CVGStatic			m_st_Filename;

public:

	void	SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pDevice = pstDevice;

		m_Wnd_OriginOp.SetPtr_Device(pstDevice);
		m_Wnd_MotionOp.SetPtr_Device(pstDevice);
		m_Wnd_MotionTeach.SetPtr_Device(pstDevice);
		m_Wnd_MotionCtr.SetPtr_Device(pstDevice);
		m_Wnd_MotionTable.SetPtr_Device(pstDevice);
	};

	void	SetPath	(__in LPCTSTR szMotorPath)
	{
		if (NULL != szMotorPath)
			m_szMotorpath = szMotorPath;
	};

	void	SetDeleteTimer		();

	void	SetOriginEnable		();

	void	SetModel			(__in LPCTSTR szPath, __in LPCTSTR szFile);

	void	SetInspectionMode	(enPermissionMode InspMode);

	void	SetUpdataData		();

	void	RefreshFileList		(__in const CStringList* pFileList);

};

#endif // Wnd_MotorView_h__
