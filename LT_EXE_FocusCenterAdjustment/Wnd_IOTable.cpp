﻿// Wnd_IOTable.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_IOTable.h"

// CWnd_IOTable

IMPLEMENT_DYNAMIC(CWnd_IOTable, CWnd)

#define	LIGHT_RED			RGB(255,	000,	000)

#define IDC_ST_DIN_DESC			1000
#define IDC_ST_DIN_SIGNAL		1200
#define IDC_ST_DOUT_DESC		1400
#define IDC_ST_DOUT_SIGNAL		1600
#define IDC_ST_DOUT_SIGNAL_END	IDC_ST_DOUT_SIGNAL + 255

CWnd_IOTable::CWnd_IOTable()
{
	m_hTimer_InputCheck = NULL;
	m_hTimerQueue = NULL;
	m_pstDevice = NULL;

	m_nInCnt = 0;
	m_nOutCnt = 0;

	CreateTimerQueue_Mon();
	CreateTimer_InputCheck();

	for (UINT nIdx = 0; nIdx < 255; nIdx++)
		m_Inport_Flag[nIdx] = FALSE;

	VERIFY(m_font_Data.CreateFont(
		11,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_SEMIBOLD,			// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_IOTable::~CWnd_IOTable()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_IOTable, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_ST_DOUT_SIGNAL, IDC_ST_DOUT_SIGNAL_END, OnRangeCmds)
END_MESSAGE_MAP()

//=============================================================================
// Method		: CreateTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_IOTable::CreateTimerQueue_Mon()
{
	__try
	{
		// Create the timer queue.
		m_hTimerQueue = CreateTimerQueue();
		if (NULL == m_hTimerQueue)
		{
			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
			return;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		//	AddLog(_T("*** Exception Error : CreateTimerQueue_Mon ()"));
	}
}

//=============================================================================
// Method		: DeleteTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_IOTable::DeleteTimerQueue_Mon()
{
	if (m_hTimerQueue == NULL)
		return;

	// 타이머가 종료될때까지 대기
	__try
	{
		if (!DeleteTimerQueue(m_hTimerQueue))
			TRACE(_T("DeleteTimerQueue failed (%d)\n"), GetLastError());
		else
			m_hTimerQueue = NULL;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_IOTable::DeleteTimerQueue_Mon()\n"));
	}

	TRACE(_T("타이머 종료 : CWnd_IOTable::DeleteTimerQueue_Mon()\n"));
}

//=============================================================================
// Method		: CreateTimer_InputCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_IOTable::CreateTimer_InputCheck()
{
	__try
	{
		// Time Check Timer
		if (NULL == m_hTimer_InputCheck)
		if (!CreateTimerQueueTimer(&m_hTimer_InputCheck, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutine_InputCheck, (PVOID)this, 3000, 500, WT_EXECUTEDEFAULT))
		{
			TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_IOTable::CreateTimer_InputCheck()\n"));
	}
}

//=============================================================================
// Method		: DeleteTimer_InputCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_IOTable::DeleteTimer_InputCheck()
{
	__try
	{
		if (DeleteTimerQueueTimer(m_hTimerQueue, m_hTimer_InputCheck, NULL))
		{
			m_hTimer_InputCheck = NULL;
		}
		else
		{
			TRACE(_T("DeleteTimerQueueTimer : m_hTimer_InputCheck failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_IOTable::DeleteTimer_InputCheck()\n"));
	}
}

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
int CWnd_IOTable::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIo = 0; nIo < m_nInCnt; nIo++)
	{
		m_st_InputName[nIo].SetFont_Gdip(L"Arial", 8.0f, Gdiplus::FontStyleRegular);
		m_st_InputName[nIo].SetTextAlignment(StringAlignmentNear);
		m_st_InputName[nIo].Create(_T(""), WS_VISIBLE | WS_CHILD | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_DIN_DESC + nIo);
		m_st_InputName[nIo].SetText(g_lpszDI_bit_Desc[nIo]);

		m_st_InputStatus[nIo].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_InputStatus[nIo].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_InputStatus[nIo].Create(_T(""), WS_VISIBLE | WS_CHILD | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_ST_DIN_SIGNAL + nIo);
		m_st_InputStatus[nIo].SetBackColor(LIGHT_RED);
	}

	for (UINT nIo = 0; nIo < m_nOutCnt; nIo++)
	{
		m_st_OutportName[nIo].SetFont_Gdip(L"Arial", 8.0f, Gdiplus::FontStyleRegular);
		m_st_OutportName[nIo].SetTextAlignment(StringAlignmentNear);
		m_st_OutportName[nIo].Create(_T(""), WS_VISIBLE | WS_CHILD | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_DOUT_DESC + nIo);
		m_st_OutportName[nIo].SetText(g_lpszDO_bit_Desc[nIo]);

		m_st_OutportStatus[nIo].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_OutportStatus[nIo].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_OutportStatus[nIo].SetFont_Gdip(L"Arial", 8.0f, Gdiplus::FontStyleRegular);
		m_st_OutportStatus[nIo].Create(_T(""), WS_VISIBLE | WS_CHILD | SS_NOTIFY | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_DOUT_SIGNAL + nIo);
	}

	m_InputGroup.SetTitle(L"INPUT LIST");
	if (!m_InputGroup.Create(_T("INPUT LIST"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	m_OutputGroup.SetTitle(L"OUTPUT LIST");
	if (!m_OutputGroup.Create(_T("OUTPUT LIST"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 21))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_IOTable::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	if (m_pstDevice == NULL)
		return;

	if (m_nInCnt <= 0 || m_nOutCnt <= 0)
		return;

	int iMargin = 10;
	int iSpacing = 5;

	int iLeft = iMargin;
	int iTop = 27;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iTop - iMargin;
	int iGroupHeight = cx / 2 - iSpacing;

	int iInCnt = m_nInCnt / 2;
	int iOutCnt = m_nOutCnt / 2;

	if (m_nInCnt % 2 != 0)
		iInCnt += 1;

	if (m_nOutCnt % 2 != 0)
		iOutCnt += 1;

	int iCtrlH = (iHeight - (iSpacing * iInCnt - 1)) / iInCnt;
	int iCtrlW = (iGroupHeight - iSpacing * 5) / 8;
	int iStatusW = iCtrlW;
	int iItemW = iCtrlW * 3;
	int iLeftSub = iLeft + iItemW + 2;

	m_InputGroup.MoveWindow(0, 0, iGroupHeight, cy);
	m_OutputGroup.MoveWindow(iGroupHeight + iSpacing, 0, iGroupHeight, cy);


	for (decltype(iInCnt) nIo = 0; nIo < iInCnt; nIo++)
	{
		m_st_InputName[nIo].MoveWindow(iLeft, iTop, iItemW, iCtrlH);
		m_st_InputStatus[nIo].MoveWindow(iLeftSub, iTop, iStatusW, iCtrlH);
		iTop += iCtrlH + iSpacing;
	}

	iLeft += iItemW + iStatusW + (iSpacing * 2);
	iLeftSub = iLeft + iItemW + 3;
	iTop = 27;

	for (decltype(m_nInCnt) nIo = iInCnt; nIo < m_nInCnt; nIo++)
	{
		m_st_InputName[nIo].MoveWindow(iLeft, iTop, iItemW, iCtrlH);
		m_st_InputStatus[nIo].MoveWindow(iLeftSub, iTop, iStatusW, iCtrlH);
		iTop += iCtrlH + iSpacing;
	}

	iLeft += iItemW + iStatusW + (iSpacing * 3) + iMargin + 2;
	iLeftSub = iLeft + iItemW + 3;
	iTop = 27;

	for (decltype(iOutCnt) nIo = 0; nIo < iOutCnt; nIo++)
	{
		m_st_OutportName[nIo].MoveWindow(iLeft, iTop, iItemW, iCtrlH);
		m_st_OutportStatus[nIo].MoveWindow(iLeftSub, iTop, iStatusW, iCtrlH);
		iTop += iCtrlH + iSpacing;
	}

	iLeft += iItemW + iStatusW + (iSpacing * 2);
	iLeftSub = iLeft + iItemW + 3;
	iTop = 27;

	for (decltype(m_nInCnt) nIo = iOutCnt; nIo < m_nInCnt; nIo++)
	{
		m_st_OutportName[nIo].MoveWindow(iLeft, iTop, iItemW, iCtrlH);
		m_st_OutportStatus[nIo].MoveWindow(iLeftSub, iTop, iStatusW, iCtrlH);
		iTop += iCtrlH + iSpacing;
	}
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
BOOL CWnd_IOTable::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_ESCAPE)
			return TRUE;

		if (pMsg->wParam == VK_RETURN)
			return TRUE;
	}
	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_IOTable::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}
//=============================================================================
// Method		: TimerRoutine_InputCheck
// Access		: protected static  
// Returns		: VOID CALLBACK
// Parameter	: __in PVOID lpParam
// Parameter	: __in BOOLEAN TimerOrWaitFired
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
VOID CALLBACK CWnd_IOTable::TimerRoutine_InputCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	CWnd_IOTable* pThis = (CWnd_IOTable*)lpParam;

	if (USE_TEST_MODE)
		pThis->OnMonitor_InputCheck();
}

//=============================================================================
// Method		: OnMonitor_InputCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_IOTable::OnMonitor_InputCheck()
{
	UpdateInputStatus();
	Sleep(100);
	UpdateOutputStatus();
}

//=============================================================================
// Method		: OnRangeCmds
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_IOTable::OnRangeCmds(UINT nID)
{
	UINT nIndex = nID - IDC_ST_DOUT_SIGNAL;

	enum_IO_SignalType BtnState = IO_SignalT_SetOff;

	if ((m_pstDevice == NULL) || (m_pstDevice->DigitalIOCtrl.AXTState() == FALSE))
		return;

	CString sz;
	m_st_OutportStatus[nIndex].GetText(sz);

	if (TRUE == sz.IsEmpty())
	{
		m_st_OutportStatus[nIndex].SetText(_T("Ⅴ"));
		BtnState = IO_SignalT_SetOn;
	}
	else
	{
		m_st_OutportStatus[nIndex].SetText(_T(""));
		BtnState = IO_SignalT_SetOff;
	}

	m_pstDevice->DigitalIOCtrl.SetOutPort(nIndex, (enum_IO_SignalType)BtnState);

}

//=============================================================================
// Method		: UpdateIOStatus
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_IOTable::UpdateInputStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_pstDevice->DigitalIOCtrl.AXTState() == FALSE)
		return;

	UINT nPutCnt = m_pstDevice->DigitalIOCtrl.GetInputPortCnt();

	for (UINT nIoNum = 0; nIoNum < nPutCnt; nIoNum++)
	{
		BOOL bStatus = m_pstDevice->DigitalIOCtrl.GetInStatus(nIoNum);

		if (m_pstDevice->DigitalIOCtrl.AXTState() == FALSE)
			return;

		if (bStatus != m_Inport_Flag[nIoNum])
		{
			m_Inport_Flag[nIoNum] = bStatus;

			if (bStatus)
			{
				if (m_st_InputStatus[nIoNum].m_hWnd)
				{
					m_st_InputStatus[nIoNum].SetColorStyle(CVGStatic::ColorStyle_Green);
				}

				if (m_st_InputName[nIoNum].m_hWnd)
				{
					m_st_InputName[nIoNum].SetBackColor(RGB(204, 255, 204));
				}
				
			}
			else
			{
				if (m_st_InputStatus[nIoNum].m_hWnd)
				{
					m_st_InputStatus[nIoNum].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
				}

				if (m_st_InputName[nIoNum].m_hWnd)
				{
					m_st_InputName[nIoNum].SetBackColor(RGB(255, 255, 255));
				}
			}
		}
	}
}

//=============================================================================
// Method		: SetDeleteTimer
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/28 - 12:33
// Desc.		:
//=============================================================================
void CWnd_IOTable::SetDeleteTimer()
{
	DeleteTimer_InputCheck();
	DeleteTimerQueue_Mon();
}

//=============================================================================
// Method		: UpdateOutputStatus
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/8/17 - 17:27
// Desc.		:
//=============================================================================
void CWnd_IOTable::UpdateOutputStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_pstDevice->DigitalIOCtrl.AXTState() == FALSE)
		return;

	UINT nPutCnt = m_pstDevice->DigitalIOCtrl.GetInputPortCnt();

	for (UINT nIoNum = 0; nIoNum < nPutCnt; nIoNum++)
	{
		BOOL bStatus = m_pstDevice->DigitalIOCtrl.GetOutStatus(nIoNum);

		if (m_pstDevice->DigitalIOCtrl.AXTState() == FALSE)
			return;

		if (m_st_OutportStatus[nIoNum].m_hWnd == NULL)
			return;

		if (bStatus == TRUE)
		{
			m_st_OutportStatus[nIoNum].SetWindowText(_T("Ⅴ"));
		}
		else
		{
			m_st_OutportStatus[nIoNum].SetWindowText(_T(""));
		}
	}
}
