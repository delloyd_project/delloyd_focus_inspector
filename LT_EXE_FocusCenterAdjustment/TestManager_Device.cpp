﻿//*****************************************************************************
// Filename	: 	TestManager_Device.cpp
// Created	:	2016/10/6 - 13:46
// Modified	:	2016/10/6 - 13:46
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_Device.h"
#include "CommonFunction.h"

CTestManager_Device::CTestManager_Device()
{
	OnInitialize();
}


CTestManager_Device::~CTestManager_Device()
{
	TRACE(_T("<<< Start ~CTestManager_Device >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_Device >>> \n"));
}

//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/5/18 - 18:58
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::OnLoadOption()
{
	CLT_Option	stOption;
	stOption.SetInspectorType((enInsptrSysType)SET_INSPECTOR);
	BOOL		bReturn = TRUE;

	stOption.SetRegistryPath(REG_PATH_OPTION_BASE);
	bReturn &= stOption.LoadOption_Inspector(m_stOption.Inspector);
	bReturn &= stOption.LoadOption_BCR(m_stOption.BCR);
	bReturn &= stOption.LoadOption_PCB(m_stOption.PCB);
	bReturn &= stOption.LoadOption_MES(m_stOption.MES);
	
	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: HWND hWndOwner
// Qualifier	:
// Last Update	: 2016/5/18 - 18:41
// Desc.		: 주변 장치 초기화
//=============================================================================
void CTestManager_Device::InitDevicez(HWND hWndOwner /*= NULL*/)
{
	CString strText;

	// 바코드 리더기
	m_Device.BCR.SetOwnerHwnd(hWndOwner);
	m_Device.BCR.SetAckMsgID(WM_RECV_BARCODE);

	// Camera 보드
	for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nPCBCamCnt; nIdxBrd++)
	{
		strText.Format(_T("Smph_CameraBoard_%02d"), nIdxBrd + 1);
		m_Device.PCBCamBrd[nIdxBrd].SetOwnerHwnd(hWndOwner);
		m_Device.PCBCamBrd[nIdxBrd].SetExitEvent(m_hEvent_ProgramExit);
		m_Device.PCBCamBrd[nIdxBrd].SetSemaphore(strText);
		m_Device.PCBCamBrd[nIdxBrd].SetLogMsgID(WM_LOGMSG_PCB_CAM);
		m_Device.PCBCamBrd[nIdxBrd].SetAckMsgID(WM_RECV_MAIN_BRD_ACK);
		//m_Device.PCBCamBrd[nIdxBrd].SetAckMsgID(WM_RECV_PCB_LIGHT);
	}


	// Indicator 센서
	for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nIndigatorCnt; nIdxBrd++)
	{
		strText.Format(_T("Smph_IndicatorBoard_%02d"), nIdxBrd + 1);
		m_Device.Indicator[nIdxBrd].SetOwnerHwnd(hWndOwner);
		m_Device.Indicator[nIdxBrd].SetExitEvent(m_hEvent_ProgramExit);
		m_Device.Indicator[nIdxBrd].SetSemaphore(strText);
		m_Device.Indicator[nIdxBrd].SetLogMsgID(WM_LOGMSG_INDICATOR);
		//m_Device.PCBCamBrd[nIdxBrd].SetAckMsgID(WM_RECV_PCB_LIGHT);
	}

	// 프레임 그래버 보드 (NTSC)
	m_Device.Cat3DCtrl.SetOwner(hWndOwner);
	m_Device.Cat3DCtrl.SetGrabberBoardType(GBT_XECAP_400EH);
	m_Device.Cat3DCtrl.SetWM_ChgStatus(WM_CAMERA_CHG_STATUS);
	m_Device.Cat3DCtrl.SetWM_RecvVideo(WM_CAMERA_RECV_VIDEO);

	// MES
#ifdef USE_EVMS_MODE
	m_Device.Mes.SetOwnerHWND(hWndOwner);
	m_Device.Mes.SetWmLog(WM_LOGMSG);
	m_Device.Mes.SetWmRecv(WM_RECV_MES);
	m_Device.Mes.SetWmCommStatus(WM_COMM_STATUS_MES);
	m_Device.Mes.SetExitEvent(m_hEvent_ProgramExit);
	m_Device.Mes.SetAddress(ntohl(m_stOption.MES.Address.dwAddress), m_stOption.MES.Address.dwPort);
#endif

	
}

//=============================================================================
// Method		: ConnectDevicez
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/18 - 18:58
// Desc.		:
//=============================================================================
void CTestManager_Device::ConnectDevicez()
{
	OnShowSplashScreen(TRUE, _T("Connect Devices"));

#ifdef USE_EVMS_MODE
	OnShowSplashScreen(TRUE, _T("Connect TCP/IP MES"));
	ConnectMES(TRUE);
#endif

	OnShowSplashScreen(TRUE, _T("Connect Barcode"));
	ConnectBCR(TRUE);

	OnShowSplashScreen(TRUE, _T("Connect Digital I/O & Motion"));
	ConnectIO(TRUE);
	//ConnectMotor(TRUE);

	OnShowSplashScreen(TRUE, _T("Connect Indicator"));
	ConnectIndicator(TRUE);

	OnShowSplashScreen(TRUE, _T("Connect Frame Grabber Board"));
	ConnectGrabberBrd_ComArt(TRUE);

	OnShowSplashScreen(TRUE, _T("Connect Interface Board"));
	ConnectPCB_CameraBrd(TRUE);

}

//=============================================================================
// Method		: DisconnectDevicez
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/31 - 22:11
// Desc.		:
//=============================================================================
void CTestManager_Device::DisconnectDevicez()
{
	OnShowSplashScreen(TRUE, _T("Disconnect Devices"));

#ifdef USE_EVMS_MODE
	OnShowSplashScreen(TRUE, _T("Disconnect TCP/IP MES"));
	ConnectMES(FALSE);
#endif

	OnShowSplashScreen(TRUE, _T("Disconnect Frame Grabber Board"));
	ConnectGrabberBrd_ComArt(FALSE);

	OnShowSplashScreen(TRUE, _T("Disconnect Camera Board"));
	ConnectPCB_CameraBrd(FALSE);

	OnShowSplashScreen(TRUE, _T("Disconnect Digital I/O & Motion"));
	ConnectIO(FALSE);
	//ConnectMotor(FALSE);

	OnShowSplashScreen(TRUE, _T("Disconnect Indicator"));
	ConnectIndicator(FALSE);

	OnShowSplashScreen(TRUE, _T("Disconnect Barcode"));
	ConnectBCR(FALSE);

	OnShowSplashScreen(FALSE);
}

//=============================================================================
// Method		: ConnectMES
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/12/28 - 15:56
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectMES(__in BOOL bConnect)
{
	if (bConnect)
	{
		OnLog(_T("MES 주소 : %s : %d에 접속 시도"), ConvIPAddrToString(ntohl(m_stOption.MES.Address.dwAddress)), m_stOption.MES.Address.dwPort);

		m_Device.Mes.ContinuousConnectServer();
	}
	else
	{
		m_Device.Mes.DisconnectSever();
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectGrabberBrd
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/10/6 - 20:28
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectGrabberBrd_ComArt(__in BOOL bConnect)
{
	if (bConnect)
	{
		if (m_Device.Cat3DCtrl.Search_Cat3d())
		{
			if (m_Device.Cat3DCtrl.Open_Board(1)) // NTSC
			{
				m_Device.Cat3DCtrl.Init_Begin();

				OnSetStatus_GrabberBrd_ComArt(TRUE);

				// 밝기, 채도 설정
				m_Device.Cat3DCtrl.SetBrightnessContrast(110, 110);

				// 기본 프레임 30으로 설정 (XCap400E 모델)
				m_Device.Cat3DCtrl.SetFPS();

				// 영상 시작
				m_Device.Cat3DCtrl.Start_CaptureAll();

				return TRUE;
			}
			else
			{
				OnLog_Err(_T("ComArt Board : Open Fail"));
			}
		}
		else
		{
			OnLog_Err(_T("ComArt Board : PCI Board Recognition fail in Device"));
		}
	}
	else
	{
		// 카메라 영상 중지
		m_Device.Cat3DCtrl.Stop_CaptureAll();

		m_Device.Cat3DCtrl.Final_End();
		m_Device.Cat3DCtrl.Close_Board(); // NTSC

		OnSetStatus_GrabberBrd_ComArt(FALSE);

		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: ConnectBCR
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/10/6 - 19:59
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectBCR(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
		CString strComPort;
		HANDLE hBCR = NULL;

		strComPort.Format(_T("//./COM%d"), m_stOption.BCR.ComPort.Port);

		hBCR = m_Device.BCR.Connect(strComPort,
									m_stOption.BCR.ComPort.BaudRate,
									m_stOption.BCR.ComPort.Parity,
									m_stOption.BCR.ComPort.StopBits,
									m_stOption.BCR.ComPort.ByteSize);

		if (NULL != hBCR)// 접속에 성공
		{
			OnLog(_T("BCR: COMM%d Comm Connect Success"), m_stOption.BCR.ComPort.Port);
			OnSetStatus_BCR(TRUE);
		}
		else
		{
			OnLog_Err(_T("BCR: COMM%d Comm Connect Fail"), m_stOption.BCR.ComPort.Port);
			OnSetStatus_BCR(FALSE);
		}
	}
	else // 연결 해제
	{
		if (m_Device.BCR.Disconnect())
		{
			OnSetStatus_BCR(TRUE);
		}
		else
		{
			;
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectPCB_CameraBrd
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/9/28 - 18:15
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectPCB_CameraBrd(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
		CString	strLog;
		CString strComPort;
		HANDLE hBCR = NULL;

		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nPCBCamCnt; nIdxBrd++)
		{
			strComPort.Format(_T("//./COM%d"), m_stOption.PCB.ComPort_CamBrd[nIdxBrd].Port);

			hBCR = m_Device.PCBCamBrd[nIdxBrd].Connect(strComPort,
													m_stOption.PCB.ComPort_CamBrd[nIdxBrd].BaudRate,
													m_stOption.PCB.ComPort_CamBrd[nIdxBrd].Parity,
													m_stOption.PCB.ComPort_CamBrd[nIdxBrd].StopBits,
													m_stOption.PCB.ComPort_CamBrd[nIdxBrd].ByteSize);

			if (NULL != hBCR)// 접속에 성공
			{
				OnLog(_T("PCB Camera Board : COM %d Connect Success"), m_stOption.PCB.ComPort_CamBrd[nIdxBrd].Port);
				OnSetStatus_PCBCamBrd(COMM_STATUS_CONNECT, nIdxBrd);

				// 보드 체크로 통신 싱크 확인
				BYTE byPort = 0;
				if (m_Device.PCBCamBrd[nIdxBrd].Send_BoardCheck(byPort))
				{
					OnSetStatus_PCBCamBrd(COMM_STATUS_SYNC_OK, nIdxBrd);
				}
			}
			else
			{
				OnLog_Err(_T("PCB Camera Board : COM %d Connect Fail"), m_stOption.PCB.ComPort_CamBrd[nIdxBrd].Port);
				OnSetStatus_PCBCamBrd(COMM_STATUS_NOTCONNECTED, nIdxBrd);
			}
		}
	}
	else // 연결 해제
	{
		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nPCBCamCnt; nIdxBrd++)
		{
			if (m_Device.PCBCamBrd[nIdxBrd].Disconnect())
			{
				OnSetStatus_PCBCamBrd(COMM_STATUS_DISCONNECT, nIdxBrd);
			}
			else
			{
				;
			}
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectMotion
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/10/16 - 16:13
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectIO(__in BOOL bConnect)
{
	if (bConnect)
	{
		if (m_Device.DigitalIOCtrl.AXTState())
		{
			OnLog(_T("IO Board Connect Success"));
			OnSetStatus_IOBoard(TRUE);

			for (UINT nPort = DO_ChartLeft; nPort <= DO_ChartRight; nPort++)
				m_Device.DigitalIOCtrl.SetOutPort(nPort, IO_SignalT_SetOn);

			//m_Device.MotionSequence.ReleaseCYLMotion();
		}
		else
		{
			OnLog(_T("IO Board Connect Fail"));
			OnSetStatus_IOBoard(FALSE);
		}
	}
	else
	{
		for (UINT nPort = 0; nPort < DO_NotUseBit_Max; nPort++)
		{
			m_Device.DigitalIOCtrl.SetOutPort(nPort, IO_SignalT_SetOff);
			m_Device.DigitalIOCtrl.SetOutPort(nPort, IO_SignalT_ToggleStop);
		}

//		m_Device.DigitalIOCtrl.SetOutPort(DO_BoardPower, IO_SignalT_SetOn);
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectMotor
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/6/28 - 10:24
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectMotor(__in BOOL bConnect)
{
	if (bConnect)
	{
		if (m_Device.DigitalIOCtrl.AXTState())
		{
			OnLog(_T("Motor Board Connect Success"));
			OnSetStatus_MotorBoard(TRUE);
		}
		else
		{
			OnLog(_T("Motor Board Connect Fail"));
			OnSetStatus_MotorBoard(FALSE);
		}
	}
	else
	{
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectIndicator
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/1/2 - 16:51
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectIndicator(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
		CString strComPort;
		CStringA strCheck;
		HANDLE hBCR = NULL;

		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nIndigatorCnt; nIdxBrd++)
		{
			strComPort.Format(_T("//./COM%d"), m_stOption.BCR.ComPort_Motor[nIdxBrd].Port);

			hBCR = m_Device.Indicator[nIdxBrd].Connect(strComPort,
				m_stOption.BCR.ComPort_Motor[nIdxBrd].BaudRate,
				m_stOption.BCR.ComPort_Motor[nIdxBrd].Parity,
				m_stOption.BCR.ComPort_Motor[nIdxBrd].StopBits,
				m_stOption.BCR.ComPort_Motor[nIdxBrd].ByteSize);

			if (NULL != hBCR)// 접속에 성공
			{
				OnLog(_T("Indicator: COMM%d Comm Contact OK"), m_stOption.BCR.ComPort_Motor[nIdxBrd].Port);
				OnSetStatus_Indicator(COMM_STATUS_CONNECT, nIdxBrd);

				if (m_Device.Indicator[nIdxBrd].Send_PortCheck(nIdxBrd, strCheck))
				{
					CString strID;

					strID.Format(_T("%02d"), nIdxBrd + 1);
					if (strCheck == (CStringA)strID)
					{
						OnSetStatus_Indicator(COMM_STATUS_SYNC_OK, nIdxBrd);
						b_IndicatorConnect[nIdxBrd] = TRUE;
					}
					else
					{
						b_IndicatorConnect[nIdxBrd] = FALSE;
					}
				}
			}
			else
			{
				OnLog_Err(_T("Indicator: COMM%d Comm Contact Fail"), m_stOption.BCR.ComPort_Motor[nIdxBrd].Port);
				OnSetStatus_Indicator(COMM_STATUS_NOTCONNECTED, nIdxBrd);
				b_IndicatorConnect[nIdxBrd] = FALSE;
			}
		}
	}
	else // 연결 해제
	{
		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[SET_INSPECTOR].nIndigatorCnt; nIdxBrd++)
		{
			if (m_Device.Indicator[nIdxBrd].Disconnect())
			{
				b_IndicatorConnect[nIdxBrd] = FALSE;
				OnSetStatus_Indicator(COMM_STATUS_DISCONNECT, nIdxBrd);
			}
		}
	}

	return TRUE;
}


//=============================================================================
// Method		: ConnectVisionCam
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Parameter	: __in UINT nCamIdx
// Qualifier	:
// Last Update	: 2017/9/29 - 13:43
// Desc.		:
//=============================================================================
// BOOL CTestManager_Device::ConnectVisionCam(__in BOOL bConnect, __in UINT nCamIdx /*= 0 */)
// {
// 	BOOL bReturn = TRUE;
// 
// 	if (bConnect)
// 	{
// 		stOpt_Ethernet* pOpt = NULL;
// 
// 		pOpt = &m_stOption.VisionCam.Address_Cam;
// 
// 		CString strAddr;
// 		in_addr address;
// 		address.s_addr = pOpt->dwAddress;
// 		strAddr = inet_ntoa(address);
// 
// 		OnLog(_T("비전 카메라 [%d]: 통신 연결 시작[%s]"), nCamIdx + 1, strAddr);
// 		m_Device.VisionCam.ConnectAndRun(strAddr.GetBuffer());
// 
// 		strAddr.ReleaseBuffer();
// 	}
// 	else
// 	{
// 		m_Device.VisionCam.DisconnectAndExit();
// 	}
// 
// 	return bReturn;
// }


//=============================================================================
// Method		: ConnectVisionLight
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Parameter	: __in UINT nChIdx
// Qualifier	:
// Last Update	: 2017/10/18 - 10:32
// Desc.		:
//=============================================================================
// BOOL CTestManager_Device::ConnectVisionLight(__in BOOL bConnect, __in UINT nChIdx /*= 0 */)
// {
// 	BOOL bReturn = TRUE;
// 
// 	if (bConnect) // 연결
// 	{
// 		CString	strLog;
// 		CString strComPort;
// 		HANDLE hBCR = NULL;
// 
// 		strComPort.Format(_T("//./COM%d"), m_stOption.SerialDevice.VisionLightBrd.Port);
// 
// 		hBCR = m_Device.IF_Illumination.Connect(strComPort,
// 			m_stOption.SerialDevice.VisionLightBrd.BaudRate,
// 			m_stOption.SerialDevice.VisionLightBrd.Parity,
// 			m_stOption.SerialDevice.VisionLightBrd.StopBits,
// 			m_stOption.SerialDevice.VisionLightBrd.ByteSize);
// 
// 		if (NULL != hBCR)// 접속에 성공
// 		{
// 			OnLog(_T("비전 광원 보드: COM %d 통신 연결 성공"), m_stOption.SerialDevice.VisionLightBrd.Port);
// 			OnSetStatus_VisionLight(COMM_STATUS_CONNECT, nChIdx);
// 		}
// 		else
// 		{
// 			OnLog_Err(_T("비전 광원 보드: COM %d 통신 연결 실패"), m_stOption.SerialDevice.VisionLightBrd.Port);
// 			OnSetStatus_VisionLight(COMM_STATUS_NOTCONNECTED, nChIdx);
// 		}
// 		
// 	}
// 	else // 연결 해제
// 	{
// 		if (m_Device.IF_Illumination.Disconnect())
// 		{
// 			OnSetStatus_VisionLight(COMM_STATUS_NOTCONNECTED, nChIdx);
// 		}
// 	}
// 
// 	return TRUE;
// }

//=============================================================================
// Method		: LEDControl_Init
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/28 - 20:07
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::LEDControl_Init()
{

	return TRUE;
}

BOOL CTestManager_Device::LEDControl(__in enLampColor LampColor)
{

	return TRUE;
}

BOOL CTestManager_Device::LEDControl_Judge(__in enTestResult Judgment)
{

	return TRUE;
}

BOOL CTestManager_Device::AlarmControl(__in BOOL bOn, __in enBuzzerType BuzzerType)
{

	return TRUE;
}

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/9/28 - 20:10
// Desc.		:
//=============================================================================
void CTestManager_Device::OnInitialize()
{
	for (UINT nIdx = 0; nIdx < Indicator_Max; nIdx++)
	{
		b_IndicatorConnect[nIdx] = FALSE;
	}
}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/9/28 - 20:10
// Desc.		:
//=============================================================================
void CTestManager_Device::OnFinalize()
{

}
