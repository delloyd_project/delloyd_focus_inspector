﻿//*****************************************************************************
// Filename	: View_MainCtrl.cpp
// Created	: 2010/11/26
// Modified	: 2016/07/21
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// View_MainCtrl.cpp : CView_MainCtrl 클래스의 구현
//

#include "stdafx.h"
#include "resource.h"

#include "View_MainCtrl.h"
#include "Def_CompileOption.h"
#include "CommonFunction.h"
#include "Registry.h"
#include "Pane_CommStatus.h"
#include "AToken.h"
#include "File_Report.h"
#include "Dlg_Barcode.h"
#include "Dlg_SelectLot.h"
#include "CommonFunction.h"
#include "Dlg_FailConfirm.h"

#include <strsafe.h>
#include <winsock2.h>
#include <iphlpapi.h>
#include <icmpapi.h>

#pragma comment(lib, "iphlpapi.lib")

//msec 측정 라이브러리 추가
#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//=============================================================================
// CView_MainCtrl 생성자
//=============================================================================
CView_MainCtrl::CView_MainCtrl()
{
	// 윈도우 포인터	
	m_nWndIndex				= -1;
		
	for (int iCnt = 0; iCnt < SUBVIEW_MAX; iCnt++)
 		m_pWndPtr[iCnt]		= NULL;
	
	m_pwndCommPane			= NULL;
	m_pdlgBarcode			= NULL;

	m_bFlag_StartBtn		= FALSE;
	m_bFlag_StopBtn			= FALSE;
	m_bFlag_FixBtn			= FALSE;
	m_bFlag_UnFixBtn		= FALSE;

	m_hTimer_FailBox		= NULL;
	m_hTimerQueue_FailBox	= NULL;
	m_pbFailBoxStatus		= NULL;

	//CreateTimerQueue_FailBox();
	InitConstructionSetting();
}

//=============================================================================
// CView_MainCtrl 소멸자
//=============================================================================
CView_MainCtrl::~CView_MainCtrl()
{
	TRACE(_T("<<< Start ~CView_MainCtrl >>> \n"));

	if (NULL != m_pdlgBarcode)
	{
		m_pdlgBarcode->DestroyWindow();
		delete m_pdlgBarcode;
		m_pdlgBarcode = NULL;
	}

	DeleteSplashScreen();
	//DeleteTimerQueue_FailBox();

	TRACE(_T("<<< End ~CView_MainCtrl >>> \n"));
}

BEGIN_MESSAGE_MAP(CView_MainCtrl, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_MESSAGE	(WM_LOGMSG,				OnLogMsg)
	ON_MESSAGE	(WM_LOGMSG_PROC,		OnLogMsg)
	ON_MESSAGE	(WM_TEST_START,			OnTestStart)
	ON_MESSAGE	(WM_TEST_STOP,			OnTestStop)
	ON_MESSAGE	(WM_TEST_INIT,			OnTestInit)
	ON_MESSAGE	(WM_TEST_COMPLETED,		OnTestCompleted)
	ON_MESSAGE	(WM_PERMISSION_MODE,	OnSwitchPermissionMode)
	ON_MESSAGE	(WM_CHANGED_MODEL,		OnChangeModel)
	ON_MESSAGE	(WM_CHANGE_MODE,		OnChangeMode)
	ON_MESSAGE	(WM_MANUAL_DEV_CTRL,	OnDeviceCtrl)
	ON_MESSAGE	(WM_RECV_BARCODE,		OnRecvBarcode)
	ON_MESSAGE	(WM_INCREASE_POGO_CNT,	OnPogoCnt_Increase)
	ON_MESSAGE	(WM_UPDATE_POGO_CNT,	OnPogoCnt_Update)
	ON_MESSAGE	(WM_INCREASE_DRIVERCOUNT,OnDriverCnt_Increase)
	ON_MESSAGE	(WM_UPDATE_DRIVERCOUNT,	OnDriverCnt_Update)
	ON_MESSAGE	(WM_CAMERA_SELECT,		OnCameraSelect)
	ON_MESSAGE	(WM_CAMERA_CHG_STATUS,	OnCameraChgStatus)
	ON_MESSAGE	(WM_CAMERA_RECV_VIDEO,	OnCameraRecvVideo)
	ON_MESSAGE	(WM_MODEL_SAVE,			OnModelSave)
	ON_MESSAGE	(WM_MASTER_SET,			OnMasterSet)
	ON_MESSAGE	(WM_RECV_MAIN_BRD_ACK,	OnRecvMainBrd)
	ON_MESSAGE	(WM_COMM_STATUS_MES,	OnCommStatus_MES)
	ON_MESSAGE	(WM_RECV_MES,			OnRecvMES)
	ON_MESSAGE	(WM_CHANGED_MOTOR,		OnChangeMotor)
	//ON_MESSAGE	(WM_COMM_STATUS_VC,		OnCommStatus_VisionCam)
	//ON_MESSAGE	(WM_VC_RECV_VIDEO,		OnRecvVideo_VisionCam)
	//ON_MESSAGE	(WM_MANUAL_DEGREE,		OnManualFocusing)
	ON_MESSAGE(WM_MASTER_MODE,			OnMasterMode)

END_MESSAGE_MAP()


//=============================================================================
// CView_MainCtrl 메시지 처리기
//=============================================================================
//=============================================================================
// Method		: CView_MainCtrl::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2010/11/26 - 13:59
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

//=============================================================================
// Method		: CView_MainCtrl::OnPaint
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/11/26 - 14:00
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnPaint() 
{
	CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트
	
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	
	// 그리기 메시지에 대해서는 CWnd::OnPaint()를 호출하지 마십시오.
}

//=============================================================================
// Method		: CView_MainCtrl::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		:
//=============================================================================
int CView_MainCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	//m_wndMainView.SetBackgroundColor(RGB(0xFF, 0xFF, 0xFF));
	//m_wndMainView.SetBackgroundColor(RGB(0, 0, 0));

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	if (!m_wndMainView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 1, NULL))
	{
		TRACE0("m_wndMainView 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}

	if (!m_wndRecipeView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 2, NULL))
	{
		TRACE0("m_wndRecipeView 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}	

	if (!m_wndDeviceView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 3, NULL))
	{
		TRACE0("m_wndDeviceView 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}		

	m_wndWorklistView.GetPtr_Worklist(m_WorklistPtr);
	if (!m_wndWorklistView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 4, NULL))
	{
		TRACE0("m_wndWorklistView 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}

// 	if (!m_wndMotorView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 5, NULL))
// 	{
// 		TRACE0("m_wndMotorView 뷰 창을 만들지 못했습니다.\n");
// 		return -1;
// 	}
	
	if (!m_wndLogView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 5, NULL))
	{
		TRACE0("m_wndLogView 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}	

	if (!m_wndAccessMode.CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 1)), _T("Access Mode"), WS_POPUPWINDOW | WS_SIZEBOX | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL))
	{
		TRACE0("m_wndAccessMode 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}

	if (!m_wndModelMode.CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 2)), _T("Model Mode"), WS_POPUPWINDOW | WS_SIZEBOX | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL))
	{
		TRACE0("m_wndModelMode 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}

	m_wndOrigin.SetPtr_Device(&m_Device);
	if (!m_wndOrigin.CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 1)), _T("Oirigin"), WS_POPUPWINDOW | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL))
	{
		TRACE0("m_wndOrigin창을 만들지 못했습니다.\n");
		return -1;
	}

	m_wndOrigin.SetOwner(GetOwner());
	m_wndOrigin.ShowWindow(SW_HIDE);

	m_wndModelMode.SetOwner(this);
	m_wndMainView.ShowWindow(SW_SHOW);

	m_pdlgBarcode = new CDlg_Barcode;
	m_pdlgBarcode->SetBarcodeCount(USE_CHANNEL_CNT);
	m_pdlgBarcode->Create(CDlg_Barcode::IDD, GetDesktopWindow());


	m_pWndPtr[0]	= (CWnd*)&m_wndMainView;	// Main 화면
	m_pWndPtr[1]	= (CWnd*)&m_wndRecipeView;	// 모듈 설정
	m_pWndPtr[2]	= (CWnd*)&m_wndDeviceView;	// 장비 제어
	//m_pWndPtr[3]	= (CWnd*)&m_wndMotorView;	// 모터 제어
	m_pWndPtr[3]	= (CWnd*)&m_wndWorklistView;// Worklist
	m_pWndPtr[4]	= (CWnd*)&m_wndLogView;		// 로그

	for (UINT nCh = 0; nCh < 1; nCh++)
	{
		DisplayVideo_NoSignal(nCh);
	}

	// 초기 세팅
	CreateSplashScreen (this, IDB_BITMAP_Luritech);
	InitUISetting ();
	InitDeviceSetting();

	return 0;
}

//=============================================================================
// Method		: CView_MainCtrl::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	m_wndMainView.MoveWindow(0, 0, cx, cy);
	m_wndRecipeView.MoveWindow(0, 0, cx, cy);
	m_wndDeviceView.MoveWindow(0, 0, cx, cy);
	//m_wndMotorView.MoveWindow(0, 0, cx, cy);
	m_wndWorklistView.MoveWindow(0, 0, cx, cy);
	m_wndLogView.MoveWindow(0, 0, cx, cy);
}

//=============================================================================
// Method		: CView_MainCtrl::OnEraseBkgnd
// Access		: protected 
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2010/10/13 - 10:40
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::OnEraseBkgnd(CDC* pDC)
{
	if (m_brBkgr.GetSafeHandle() == NULL)
	{
		return CWnd::OnEraseBkgnd(pDC);
	}

	ASSERT_VALID(pDC);

	CRect rectClient;
	GetClientRect(rectClient);

	if (m_brBkgr.GetSafeHandle() != NULL)
	{
		pDC->FillRect(rectClient, &m_brBkgr);
	}
	else
	{
		CWnd::OnEraseBkgnd(pDC);
	}

	return TRUE;
}

//=============================================================================
// Method		: CView_MainCtrl::OnCtlColor
// Access		: protected 
// Returns		: HBRUSH
// Parameter	: CDC * pDC
// Parameter	: CWnd * pWnd
// Parameter	: UINT nCtlColor
// Qualifier	:
// Last Update	: 2010/10/12 - 17:35
// Desc.		:
//=============================================================================
HBRUSH CView_MainCtrl::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	if (m_brBkgr.GetSafeHandle() != NULL)
	{
#define AFX_MAX_CLASS_NAME 255
#define AFX_STATIC_CLASS _T("Static")
#define AFX_BUTTON_CLASS _T("Button")

		if (nCtlColor == CTLCOLOR_STATIC)
		{
			TCHAR lpszClassName [AFX_MAX_CLASS_NAME + 1];

			::GetClassName(pWnd->GetSafeHwnd(), lpszClassName, AFX_MAX_CLASS_NAME);
			CString strClass = lpszClassName;

			if (strClass == AFX_STATIC_CLASS)
			{
				pDC->SetBkMode(TRANSPARENT);
				return(HBRUSH) ::GetStockObject(HOLLOW_BRUSH);
			}

			if (strClass == AFX_BUTTON_CLASS)
			{
				if ((pWnd->GetStyle() & BS_GROUPBOX) == 0)
				{
					pDC->SetBkMode(TRANSPARENT);
				}

				return(HBRUSH) ::GetStockObject(HOLLOW_BRUSH);
			}
		}
	}

	return CWnd::OnCtlColor(pDC, pWnd, nCtlColor);
}

//=============================================================================
// Method		: CView_MainCtrl::OnTimer
// Access		: protected 
// Returns		: void
// Parameter	: UINT_PTR nIDEvent
// Qualifier	:
// Last Update	: 2010/12/9 - 15:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnTimer(UINT_PTR nIDEvent)
{
	// 타이머 처리
	

	CWnd::OnTimer(nIDEvent);
}

//=============================================================================
// Method		: CView_MainCtrl::OnLogMsg
// Access		: protected 
// Returns		: LRESULT
// Parameter	: WPARAM wParam	-> 메세지 문자열
// Parameter	: LPARAM lParam	
//					-> HIWORD : 오류 메세지 인가?
//					-> LOWORD : 로그 종류 (기본, PLC, 관리PC 등)
// Qualifier	:
// Last Update	: 2010/10/14 - 17:38
// Desc.		: 로그 처리용
//	LOG_TAB_PLC		= 0,
//	LOG_TAB_MANPC,
//	LOG_TAB_IRDA,
//	LOG_TAB_BCR,
//=============================================================================
LRESULT CView_MainCtrl::OnLogMsg( WPARAM wParam, LPARAM lParam )
{
	BOOL	bError = (BOOL)HIWORD(lParam);
	UINT	nType  = LOWORD(lParam);

	if (NULL == (LPCTSTR)wParam)
	{
		return FALSE;
	}
	
	AddLog((LPCTSTR)wParam, bError, nType);

	return TRUE;
}

//=============================================================================
// Method		: OnTestStart
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/5/29 - 0:12
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnTestStart(WPARAM wParam, LPARAM lParam)
{
// 	if (IsTesting_All())
// 	{
// // 		TRACE(_T("오류 : 전체 검사 작업이 진행 중\n"));
// // 		return FALSE;
// 	}

	if (!IsTesting())
	{
		StartOperation_AutoAll();
	}
	else
	{
		if (m_bTestFinishMode == TRUE)
		{
			m_bTestFinishMode = FALSE;
			m_bFlag_FocusTest = FALSE;
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: OnTestStop
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/5/29 - 0:16
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnTestStop(WPARAM wParam, LPARAM lParam)
{
	
	m_bFlag_UseStop = TRUE;

	return 1;
}

//=============================================================================
// Method		: OnTestInit
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/11/11 - 1:58
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnTestInit(WPARAM wParam, LPARAM lParam)
{
	if (IsTesting())
	{
		MessageView(_T("Inspection is Running. Try after Inspection."));
		return FALSE;
	}

// 	if (IDYES == AfxMessageBox(_T("데이터를 초기화 하시겠습니까?"), MB_YESNO))
// 	{
// 		// 알람 초기화
// 		OnInitDigitalIOSignal();
// 
// 		// 데이터 초기화
// 		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
// 		{	
// 			m_stInspInfo.DualCamInfo[nIdx].Reset();
// 		}
// 		OnResetCamInfo();
// 		//OnUpdateCamInfo_All();
// 		OnUpdateSiteCamInfo();
// 	}

	return TRUE;
}

//=============================================================================
// Method		: OnTestCompleted
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/5/30 - 13:43
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnTestCompleted(WPARAM wParam, LPARAM lParam)
{
	// 최종 검사 판정 업데이트?
	OnJugdement_And_Report();

	return TRUE;
}

//=============================================================================
// Method		: OnSwitchPermissionMode
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/29 - 16:46
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnSwitchPermissionMode(WPARAM wParam, LPARAM lParam)
{
	if (IsTesting())
 	{
 		//m_wndMainView.SetInspectionMode_Cancle();
		MessageView(_T("Inspection is Running.\r\nTry after Inspection."));
 		return FALSE;
 	}

	enPermissionMode InspMode = (enPermissionMode)wParam;

	m_stInspInfo.PermissionMode = InspMode;

	m_wndMainView.SetInspectionMode(InspMode);

	// MainFrm으로 권한 변경 통보
	GetParent()->SendMessage(WM_PERMISSION_MODE, (WPARAM)InspMode, 0);

	return TRUE;
}

//=============================================================================
// Method		: OnChangeModel
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/25 - 18:31
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnChangeModel(WPARAM wParam, LPARAM lParam)
{
 	if (IsTesting())
 	{
		MessageView(_T("Inspection is Running.\r\nTry after Inspection."));
 		return FALSE;
 	}

	//DEBUG_ONLY();
	
	// 모델 파일에서 모델 정보 불러오기
	CString strModel = (LPCTSTR)wParam;
	BOOL bNotifyModelView = (BOOL)lParam;
	
	LoadModelInfo(strModel, bNotifyModelView);

	OnChangeLotInfo(LOT_End);

	return TRUE;
}

//=============================================================================
// Method		: OnChangeLot
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/10/28 - 16:03
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnChangeMode(WPARAM wParam, LPARAM lParam)
{
	enModelStatus LotStatus = (enModelStatus)wParam;

	if (IsTesting())
	{
		MessageView(_T("Inspection is Running.\r\nTry after Inspection."));
		return FALSE;
	}

	switch (LotStatus)
	{
	case LOT_Start:
		OnChangeLotInfo(LOT_Start);
		break;
	case LOT_End:
		OnChangeLotInfo(LOT_End);
		break;
	case Model_Change:
		OnChangeModelInfo();
		break;
	case MASTER_Start:
		OnSetMaterSetView(TRUE);
		break;
	case MASTER_End:

		//마스터 모드 끝낼시
		OnShowSplashScreen(TRUE, _T("Master Setting Finish"));

		OnSetMaterSetView(FALSE);

		Sleep(1000);

		OnShowSplashScreen(FALSE);
		break;
		//Exit 기능 추가 해야하는지 물어봐야함! [2/7/2019 Seongho.Lee]
	case ManualMode:
		m_bFlag_AutoFocus = FALSE;
		ManualAutoMode(TRUE);
		break;
	case AutoMode:
		m_bFlag_AutoFocus = TRUE;
		ManualAutoMode(FALSE);
		break;
	default:
		break;
	}

	return TRUE;
}

//=============================================================================
// Method		: OnCommStatus_MES
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnCommStatus_MES(WPARAM wParam, LPARAM lParam)
{
	UINT	nDevice = (UINT)wParam;
	UINT	nStatus = (UINT)lParam;

	OnSetStatus_MES(nStatus);

	return 0;
}

//=============================================================================
// Method		: OnRecvMES
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnRecvMES(WPARAM wParam, LPARAM lParam)
{
	ST_LG_MES_Protocol	m_stRecvProtocol;
	m_stRecvProtocol.SetRecvProtocol((const char*)wParam, (DWORD)lParam);

	USES_CONVERSION;
	m_stInspInfo.szRecvLotID.Format(_T("%s"), A2T(m_stRecvProtocol.szLotID));
	m_stInspInfo.nRecvLotTryCnt = atoi(m_stRecvProtocol.szLotTryCount.GetBuffer());
	m_stRecvProtocol.szLotTryCount.ReleaseBuffer();
	m_stRecvProtocol.szProtocol.ReleaseBuffer();

	m_stInspInfo.ModelInfo.szLotID		= m_stInspInfo.szRecvLotID;
	m_stInspInfo.szBarcodeBuf			= m_stInspInfo.szRecvLotID;
	m_stInspInfo.ModelInfo.nLotTryCnt	= m_stInspInfo.nRecvLotTryCnt;

	ZeroMemory(&m_stInspInfo.LotInfo.StartTime, sizeof(SYSTEMTIME));
	GetLocalTime(&m_stInspInfo.LotInfo.StartTime);

	// UI에 표시
	TRACE(_T("Barcode : %s\n"), m_stInspInfo.szRecvLotID);
	OnSetResetSiteInfo();

	OnSet_Barcode(m_stInspInfo.szRecvLotID);
	return 0;
}

//=============================================================================
// Method		: OnCameraSelect
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnCameraSelect(WPARAM wParam, LPARAM lParam)
{
	UINT nZoneIdx = (UINT)wParam;

	//m_Cat3DCtrl.SelectCamera(nZoneIdx);

	return 0;
}

//=============================================================================
// Method		: OnCameraChgStatus
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnCameraChgStatus(WPARAM wParam, LPARAM lParam)
{
	UINT nGrabType = m_stInspInfo.ModelInfo.nGrabType;

	if (nGrabType == GrabType_NTSC)
	{
		DWORD* pdwStatus = m_Device.Cat3DCtrl.GetStatus();

		// 1 -> 0으로 바뀌면 화면에 표시
		for (UINT nChIdx = 0; nChIdx < g_InspectorTable[SET_INSPECTOR].nPCBCamCnt; nChIdx++)
		{
			if (m_stInspInfo.bVideoSignal[nChIdx] != pdwStatus[nChIdx])
			{
				m_stInspInfo.bVideoSignal[nChIdx] = (BOOL)pdwStatus[nChIdx];
				OnSetStatus_Signal_CA(nChIdx, m_stInspInfo.bVideoSignal[nChIdx]);
			}
		}
	}
	return TRUE;
}

//=============================================================================
// Method		: OnCameraRecvVideo
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnCameraRecvVideo(WPARAM wParam, LPARAM lParam)
{
	DWORD dwChIdx = (DWORD)wParam;
	//	TRACE(_T("** OnCameraRecvVideo: %d **\n"), dwChIdx);

	UINT nGrabType = m_stInspInfo.ModelInfo.nGrabType;

	if (nGrabType == GrabType_NTSC)
	{
		ST_VideoRGB_NTSC* pRGB = m_Device.Cat3DCtrl.GetRecvRGBData(dwChIdx);
		RGBQUAD** pRGBItr = (m_Device.Cat3DCtrl.GetRecvRGBData(dwChIdx))->m_pMatRGB;
		LPBYTE pRGBDATA = (LPBYTE)((m_Device.Cat3DCtrl.GetRecvRGBData(dwChIdx))->m_pMatRGB[0]);

		IplImage *Testimage = cvCreateImage(cvSize(pRGB->m_dwWidth, pRGB->m_dwHeight), IPL_DEPTH_8U, 3);

		int widthstep_x3 = pRGB->m_dwWidth * 3;
		int widthstep_x4 = pRGB->m_dwWidth * 4;

		for (int y = 0; y < (int)pRGB->m_dwHeight; y++)
		{
			for (int x = 0; x < (int)pRGB->m_dwWidth; x++)
			{
				Testimage->imageData[y*widthstep_x3 + 3 * x + 0] = pRGBDATA[y*(widthstep_x4)+x * 4];
				Testimage->imageData[y*widthstep_x3 + 3 * x + 1] = pRGBDATA[y*(widthstep_x4)+x * 4 + 1];
				Testimage->imageData[y*widthstep_x3 + 3 * x + 2] = pRGBDATA[y*(widthstep_x4)+x * 4 + 2];
			}
		}
		//DisplayVideo((UINT)dwChIdx, pRGBDATA, pRGB->m_dwSize, pRGB->m_dwWidth, pRGB->m_dwHeight);

		if (m_wndMainView.IsWindowVisible()){
			DisplayVideo_Overlay(dwChIdx, (enPic_TestItem)m_stInspInfo.ModelInfo.nPicViewMode, Testimage);
			m_wndMainView.m_wnd_SiteInfo.ShowVideo(dwChIdx, (LPBYTE)Testimage->imageData, pRGB->m_dwWidth, pRGB->m_dwHeight);
		}
		else if (m_wndRecipeView.IsWindowVisible() && m_stImageMode.eImageMode == ImageMode_LiveCam){
			m_wndRecipeView.DisplayVideo_Overlay(dwChIdx, (enPic_TestItem)m_wndRecipeView.GetVideoPicStatus(), Testimage);
			m_wndRecipeView.ShowVideo(dwChIdx, (LPBYTE)Testimage->imageData, pRGB->m_dwWidth, pRGB->m_dwHeight);
		}

		if (m_stImageMode.eImageMode == ImageMode_LiveCam)
		{
			if (m_stPicImageCaptureMode.bImageCaptureMode == TRUE)
			{
				cvSaveImage((CStringA)m_stPicImageCaptureMode.szImagePath, Testimage);
				m_stPicImageCaptureMode.bImageCaptureMode = FALSE;
			}
		}
		cvReleaseImage(&Testimage);
	}

	return TRUE;
}

//=============================================================================
// Method		: OnDeviceCtrl
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/8/10 - 10:42
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnDeviceCtrl(WPARAM wParam, LPARAM lParam)
{
	UINT nCtrlIdx = (UINT)wParam;
	return TRUE;
}

//=============================================================================
// Method		: OnRecvBarcode
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/10/21 - 17:33
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnRecvBarcode(WPARAM wParam, LPARAM lParam)
{
	// ::SendNotifyMessage(m_hOwnerWnd, m_WM_Ack, (WPARAM)m_szACKBuf, (LPARAM)m_dwACKBufSize);
	CStringA szTemp = (char*)wParam;
	DWORD dwLength = (DWORD)lParam;

	szTemp.Remove('\r');
	szTemp.Remove('\n');

	USES_CONVERSION;
	CString szBarcode = A2T(szTemp.GetBuffer());
	szTemp.ReleaseBuffer();
		
	TRACE(_T("Barcode : %s (Length : %d)\n"), szBarcode, dwLength);
	OnSet_Barcode(szBarcode);

	return TRUE;
}

//=============================================================================
// Method		: OnRecvMainBrd
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/6/21 - 11:05
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnRecvMainBrd(WPARAM wParam, LPARAM lParam)
{

	return 0;
}

//=============================================================================
// Method		: OnPogoCnt_Increase
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 18:04
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnPogoCnt_Increase(WPARAM wParam, LPARAM lParam)
{
	OnSetStatus_PogoCount();
	return TRUE;
}

//=============================================================================
// Method		: OnPogoCnt_Update
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 18:04
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnPogoCnt_Update(WPARAM wParam, LPARAM lParam)
{
 	m_wndMainView.UpdatePogoCnt();
	return TRUE;
}

//  [1/25/2019 ysJang]
LRESULT CView_MainCtrl::OnDriverCnt_Increase(WPARAM wParam, LPARAM lParam)
{
	OnSetStatus_DriverCount();
	return TRUE;
}

LRESULT CView_MainCtrl::OnDriverCnt_Update(WPARAM wParam, LPARAM lParam)
{
	m_wndMainView.UpdateDriverCnt();
	return TRUE;
}

//=============================================================================
// Method		: OnMasterSet
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/2/27 - 19:40
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnMasterSet(WPARAM wParam, LPARAM lParam)
{
	SetAutoMasterMode();
	return 0;
}

LRESULT CView_MainCtrl::OnMasterMode(WPARAM wParam, LPARAM lParam)
{
	LRESULT	lReturn = RCA_OK;

	m_DlgMasterMode.SetOwner(this);
	m_DlgMasterMode.SetPtr_Device(&m_Device);
	m_DlgMasterMode.SetPtr_Modelinfo(&m_stInspInfo.ModelInfo);
	m_DlgMasterMode.SetVideoChannel(VideoView_Ch_1);

	if (m_stInspInfo.PermissionMode == Permission_Administrator || 
		m_stInspInfo.PermissionMode == Permission_Engineer || 
		m_stInspInfo.PermissionMode == Permission_CNC)
	{
		m_DlgMasterMode.SetMode(0);
		m_DlgMasterMode.SetPermisionMode(m_stInspInfo.PermissionMode);

		m_DlgMasterMode.DoModal();
		m_stInspInfo.m_bMasterCheck = m_DlgMasterMode.GetResult();
	}
	else
	{
		AfxMessageBox(_T("Start Good Master Inspection."));

		// 양품
		BOOL bOK_Master = FALSE;

		m_DlgMasterMode.SetMode(0);
		m_DlgMasterMode.SetPermisionMode(m_stInspInfo.PermissionMode);

		m_DlgMasterMode.DoModal();
		bOK_Master = m_DlgMasterMode.GetResult();

		AfxMessageBox(_T("Start Fail Master Inspection."));

		// 불량
		BOOL bNG_Master = FALSE;
		m_DlgMasterMode.SetMode(1);

		m_DlgMasterMode.SetPermisionMode(m_stInspInfo.PermissionMode);

		m_DlgMasterMode.DoModal();
		bNG_Master = m_DlgMasterMode.GetResult();


		m_stInspInfo.m_bMasterCheck = bOK_Master & bNG_Master;
	}

	return lReturn;
}
//=============================================================================
// Method		: OnModelSave
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/2/28 - 16:07
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnModelSave(WPARAM wParam, LPARAM lParam)
{
	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), MODEL_FILE_EXT, MODEL_FILE_EXT);

	if (m_stInspInfo.ModelInfo.szModelFile.IsEmpty())
	{
		CFileDialog fileDlg(FALSE, MODEL_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
		fileDlg.m_ofn.lpstrInitialDir = m_stInspInfo.Path.szModel;

		if (fileDlg.DoModal() == IDOK)
		{
			strFullPath = fileDlg.GetPathName();
			strFileTitle = fileDlg.GetFileTitle();

			// 저장	 		
			if (m_fileModel.SaveModelFile(strFullPath, &m_stInspInfo.ModelInfo))
			{
				// 리스트 모델 갱신
			}

			LoadModelInfo(m_stInspInfo.Path.szModel, 0);

			m_wndRecipeView.SetModel(m_stInspInfo.ModelInfo.szModelFile);
			m_stInspInfo.ModelInfo.szModelFile.ReleaseBuffer();
		}
	}
	else
	{
		strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szModel, m_stInspInfo.ModelInfo.szModelFile, MODEL_FILE_EXT);

		// 저장	 	
		m_fileModel.SaveModelFile(strFullPath, &m_stInspInfo.ModelInfo);

		// 모델 데이터 불러오기
		LoadModelInfo(m_stInspInfo.ModelInfo.szModelFile, 0);

		m_wndRecipeView.SetModel(m_stInspInfo.ModelInfo.szModelFile);
		m_stInspInfo.ModelInfo.szModelFile.ReleaseBuffer();
	}

	return 0;
}

//=============================================================================
// Method		: OnChangeMotor
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/8/12 - 18:27
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnChangeMotor(WPARAM wParam, LPARAM lParam)
{
	CString strMotor = (LPCTSTR)wParam;

	ShowSplashScreen();
	m_wndSplash.SetText(_T("Changing Motor..."));

	m_stInspInfo.ModelInfo.szMotorFile = strMotor;

	m_Device.MotionManager.SetPrtMotorPath(&m_stInspInfo.Path.szMotor, strMotor);
	if (m_Device.MotionManager.LoadMotionParam())
	{
		m_regInspInfo.SaveSelectedMotor(m_stInspInfo.ModelInfo.szMotorFile);
		//m_wndMotorView.SetModel(m_stInspInfo.Path.szMotor, m_stInspInfo.ModelInfo.szMotorFile);
	}

	//m_wndMotorView.SetUpdataData();

	ShowSplashScreen(FALSE);

	return 0;
}


//************************************
// Method:    OnManualFocusing
// FullName:  CView_MainCtrl::OnManualFocusing
// Access:    protected 
// Returns:   LRESULT
// Qualifier:
// Parameter: WPARAM wParam
// Parameter: LPARAM lParam
//************************************
// LRESULT CView_MainCtrl::OnManualFocusing(WPARAM wParam, LPARAM lParam)
// {
// 	UINT nDirection = (UINT)wParam;
// 	double dDegree = (double)lParam;
// 
// 	if(nDirection == TB_ManualDeg)
// 		dDegree /= 100;
// 
// 	if (nDirection == TB_ManualDeg1)
// 		dDegree /= -100;
// 	
// 	m_TIProcessing.ManualFocusingRun(dDegree);
// 
// 	return TRUE;
// }

//=============================================================================
// Method		: OnInitLogFolder
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/19 - 15:04
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnInitLogFolder()
{
#ifdef USE_EVMS_MODE

	m_logFile.SetPath(EVMS_PATH_LOG_OP, _T("Inspector"));
	m_Log_ErrLog.SetPath(EVMS_PATH_LOG_OP, _T("Error"));
	m_Log_ErrLog.SetLogFileName_Prefix(_T("Err"));

#else

	// 로그 처리
	if (!m_stInspInfo.Path.szLog.IsEmpty())
		m_logFile.SetPath(m_stInspInfo.Path.szLog, _T("Inspector"));

	if (!m_stInspInfo.Path.szLog.IsEmpty())
		m_Log_ErrLog.SetPath(m_stInspInfo.Path.szLog, _T("Error"));

	m_Log_ErrLog.SetLogFileName_Prefix(_T("Err"));

#endif
}

//=============================================================================
// Method		: CView_MainCtrl::InitConstructionSetting
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/13 - 15:13
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitConstructionSetting()
{
	// 프로그램 폴더 구하기
	TCHAR szExePath[MAX_PATH] = {0};	
	GetModuleFileName(NULL, szExePath, MAX_PATH);

	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];	
	TCHAR file[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];
	_tsplitpath_s (szExePath, drive, _MAX_DRIVE, dir, _MAX_DIR, file, _MAX_FNAME, ext, _MAX_EXT);	
	
	m_stInspInfo.Path.szProgram.	Format(_T("%s%s"), drive, dir);
	m_stInspInfo.Path.szLog	.		Format(_T("%s%sLOG\\"), drive, dir);
	m_stInspInfo.Path.szReport.		Format(_T("%s%sReport\\"), drive, dir);
	m_stInspInfo.Path.szModel.		Format(_T("%s%sModel\\"), drive, dir);
	m_stInspInfo.Path.szImage.		Format(_T("%s%sImage\\"), drive, dir);
	m_stInspInfo.Path.szPogo.		Format(_T("%s%sPogo\\"), drive, dir);
	m_stInspInfo.Path.szDriverCount.Format(_T("%s%sDriverCount\\"), drive, dir);
	m_stInspInfo.Path.szMotor.		Format(_T("%s%sMotor\\"), drive, dir);
	m_stInspInfo.Path.szMes.		Format(_T("%s%sMes\\"), drive, dir);

	OnLoadOption();
	OnInitLogFolder();

	MakeSubDirectory(m_stInspInfo.Path.szReport);
	MakeSubDirectory(m_stInspInfo.Path.szModel);
	MakeSubDirectory(m_stInspInfo.Path.szImage);
	MakeSubDirectory(m_stInspInfo.Path.szPogo);
	MakeSubDirectory(m_stInspInfo.Path.szDriverCount);
	MakeSubDirectory(m_stInspInfo.Path.szMotor);
	MakeSubDirectory(m_stInspInfo.Path.szMes);

	// 모델, 모터 파일 체크
	InitEVMS_EnvFile_All();

	// IO Board Setting
	if (g_InspectorTable[SET_INSPECTOR].bPCIIOControl == TRUE)
	{
		m_Device.DigitalIOCtrl.AXTInit();
		m_wndDeviceView.SetPtr_Device(&m_Device);
		m_Device.MotionSequence.SetPtrDigitalIOCtrl(&m_Device.DigitalIOCtrl);
	}

 	// Motor Board Setting
	if (g_InspectorTable[SET_INSPECTOR].bPCIMotion == TRUE)
	{
		// Motor Board Setting
		m_Device.MotionManager.SetPrtMotorPath(&m_stInspInfo.Path.szMotor);
		m_Device.MotionManager.SetAllMotorOpen();

		//m_wndMotorView.SetPtr_Device(&m_Device);
		//m_wndMotorView.SetPath(m_stInspInfo.Path.szMotor);

		m_Device.MotionSequence.SetPtrMotionManger(&m_Device.MotionManager);
	}

	m_Device.MotionSequence.SetModelInfoData(&m_stInspInfo.ModelInfo);
	
	// Motion & IO 
	m_Device.MotionSequence.SetLTOption(&m_stOption);
	m_wndRecipeView.SetPtr_ImageMode(&m_stImageMode);
	m_wndRecipeView.SetPtr_Device(&m_Device);
	m_wndRecipeView.SetLTOption(&m_stOption);
	m_wndRecipeView.SetPtr_PogoCnt(&m_stInspInfo.PogoInfo);
	m_wndRecipeView.SetPtr_DriverCnt(&m_stInspInfo.DriverCountInfo);
	m_wndRecipeView.SetPath(m_stInspInfo.Path.szModel, m_stInspInfo.Path.szImage, m_stInspInfo.Path.szPogo, m_stInspInfo.Path.szDriverCount);

	m_wndMainView.SetPtrInspectionInfo(&m_stInspInfo);

 	m_TIProcessing.SetLTOption(&m_stOption);
	m_TIProcessing.SetPtr_Device(&m_Device);
	m_TIProcessing.SetPtr_ModelInfo(&m_stInspInfo.ModelInfo);
	m_TIProcessing.SetPtr_ImageMode(&m_stImageMode);
	m_TIProcessing.SetPtr_PicImageCaptureMode(&m_stPicImageCaptureMode);
	m_wndRecipeView.m_TIProcessing.SetPtr_PicImageCaptureMode(&m_stPicImageCaptureMode);

	m_TIPicControl.SetPtr_ModelInfo(&m_stInspInfo.ModelInfo);
}

//=============================================================================
// Method		: CView_MainCtrl::InitUISetting
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2013/1/2 - 16:23
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitUISetting()
{
	CReg_InspInfo regInfo;
	DWORD dwValue = 0;

	m_TIProcessing.SetImagehwndMain(m_wndMainView.m_wnd_SiteInfo.m_wnd_ImgProc.m_hWnd);
}

//=============================================================================
// Method		: CView_MainCtrl::InitDeviceSetting
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2012/12/17 - 17:53
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitDeviceSetting()
{
	InitDevicez(GetSafeHwnd());
}

//=============================================================================
// Method		: InitEVMS_EnvFile
// Access		: protected  
// Returns		: void
// Parameter	: __in LPCTSTR szModelFile
// Qualifier	:
// Last Update	: 2016/12/15 - 12:28
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitEVMS_EnvFile(__in LPCTSTR szModelFile)
{
#ifdef USE_EVMS_MODE
	// 기본 모델파일, 기본 모터 파일   
	// 복사

	//szModelFile;
	CString szEnvFullPath;
	CString szDefFullPath;
	// OP/ENV/
	szEnvFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szModel, szModelFile, MODEL_FILE_EXT);
	// OP/EXE/../ENV/
	szDefFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szDefaultEnv, szModelFile, MODEL_FILE_EXT);

	// 모델 파일이 없으면
	if (!PathFileExists(szEnvFullPath))
	{
		// 복사
		if (PathFileExists(szDefFullPath))
		{
			::CopyFile(szDefFullPath, szEnvFullPath, TRUE);
		}
		else
		{
			// Default.luri 사용??
			// 오류 처리 ??
		}
	}
	else // 파일이 있을 경우
	{
		// 최신 날짜 파일 사용?
		if (PathFileExists(szDefFullPath))
		{
			FILETIME tmEnv, tmDef;
			GetLastWriteTime(szEnvFullPath, tmEnv);
			GetLastWriteTime(szDefFullPath, tmDef);

			if (0 < CompareFileTime(&tmEnv, &tmDef))
			{
				::CopyFile(szDefFullPath, szEnvFullPath, TRUE);
			}
		}
	}

#endif
}

//=============================================================================
// Method		: InitEVMS_EnvFile_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/15 - 10:19
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitEVMS_EnvFile_All()
{
	CFileFind finder;
	CString szModelFile;
	CString strWildcard;

	// OP/EXE/../ENV/
	strWildcard.Format(_T("%s*.%s"), m_stInspInfo.Path.szDefaultEnv, MODEL_FILE_EXT);

	BOOL bWorking = finder.FindFile(strWildcard);

	while (bWorking)
	{
		bWorking = finder.FindNextFile();

		if (finder.IsDots())
			continue;

		if (finder.IsDirectory())
			continue;

		szModelFile = finder.GetFileTitle();

		InitEVMS_EnvFile(szModelFile);
	}

	finder.Close();

}

//=============================================================================
// Method		: OnSetStatus_GrabberBrd_ComArt
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/12/28 - 16:02
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_GrabberBrd_ComArt(__in BOOL bConnect)
{
	__super::OnSetStatus_GrabberBrd_ComArt(bConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_GrabberBrd_ComArt((UINT)bConnect);
}


//=============================================================================
// Method		: OnSetStatus_BCR
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Qualifier	:
// Last Update	: 2017/8/12 - 16:52
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_BCR(__in UINT nConnect)
{
	__super::OnSetStatus_BCR(nConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_BCR(nConnect);
}

//=============================================================================
// Method		: OnSetStatus_PCBCamBrd
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2017/8/12 - 16:52
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_PCBCamBrd(__in UINT nConnect, __in UINT nIdxBrd)
{
	__super::OnSetStatus_PCBCamBrd(nConnect, nIdxBrd);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_CameraBoard(nIdxBrd, nConnect);
}



//=============================================================================
// Method		: OnSetStatus_IOBoard
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/8/12 - 16:53
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_IOBoard(__in BOOL bConnect)
{
	__super::OnSetStatus_IOBoard(bConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_IO(bConnect);
}

//=============================================================================
// Method		: OnSetStatus_MotorBoard
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/8/12 - 16:53
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_MotorBoard(__in BOOL bConnect)
{
	__super::OnSetStatus_MotorBoard(bConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_Motor(bConnect);
}

//=============================================================================
// Method		: OnSetStatus_Motor
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bConnect
// Parameter	: __in UINT nChIdx
// Qualifier	:
// Last Update	: 2017/1/2 - 16:58
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_Indicator(__in BOOL bConnect, __in UINT nChIdx /*= 0*/)
{
	__super::OnSetStatus_Indicator(bConnect, nChIdx);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_Indicator(bConnect, nChIdx);
}

//=============================================================================
// Method		: OnSetStatus_Signal_CA
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nIndex
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2016/12/29 - 15:43
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_Signal_CA(__in UINT nIndex, __in BOOL bOn)
{
	if (FALSE == bOn)
	{
		DisplayVideo_NoSignal((UINT)nIndex);
	}
}

//=============================================================================
// Method		: OnSetStatus_MES
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Qualifier	:
// Last Update	: 2017/8/12 - 16:55
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_MES(__in UINT nConnect)
{
	__super::OnSetStatus_MES(nConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_MES(nConnect);
}


//=============================================================================
// Method		: OnSetStatus_VisionCam
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bConnect
// Parameter	: __in UINT nCamIdx
// Qualifier	:
// Last Update	: 2017/9/29 - 13:49
// Desc.		:
//=============================================================================
// void CView_MainCtrl::OnSetStatus_VisionCam(__in BOOL bConnect, __in UINT nCamIdx /*= 0*/)
// {
// 	__super::OnSetStatus_VisionCam(bConnect, nCamIdx);
// 	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_VisionCam(bConnect);
// }


//=============================================================================
// Method		: OnSetStatus_VisionLight
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bConnect
// Parameter	: __in UINT nChIdx
// Qualifier	:
// Last Update	: 2017/10/18 - 10:38
// Desc.		:
//=============================================================================
// void CView_MainCtrl::OnSetStatus_VisionLight(__in BOOL bConnect, __in UINT nChIdx /*= 0*/)
// {
// 	__super::OnSetStatus_VisionLight(bConnect, nChIdx);
// 	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_VisionLight(bConnect, nChIdx);
// }

//=============================================================================
// Method		: OnSet_Barcode
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2016/10/20 - 21:20
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSet_Barcode(__in LPCTSTR szBarcode)
{
	__super::OnSet_Barcode(szBarcode);
	m_wndMainView.InsertBarcode(szBarcode);

	 	CString szNotUse = _T("NotUse");
	 
	 	if (NULL != m_pdlgBarcode)
	 	{
	 		m_pdlgBarcode->ShowWindow(SW_SHOW);
	 
	 		if (m_pdlgBarcode->InsertBarcode(szBarcode))
	 		{
	 			m_pdlgBarcode->GetBarcode(m_stInspInfo.szBarcodeBuf);
	 			m_wndMainView.InsertBarcode(m_stInspInfo.szBarcodeBuf);
	 			//OnSetBarcode(m_stInspInfo.szarBarcodeBuf.GetAt(nIndex), nIndex);
	 		}
	 	}
	 	((CPane_CommStatus*)m_pwndCommPane)->Set_Barcode(szBarcode);
}

//=============================================================================
// Method		: OnAddErrorInfo
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enErrorCode lErrorCode
// Qualifier	:
// Last Update	: 2016/10/31 - 1:04
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnAddErrorInfo(__in enErrorCode lErrorCode)
{
	ST_ErrorInfo stErrInfo;

	stErrInfo.lCode = lErrorCode;
	stErrInfo.nType = 0;
	GetLocalTime(&stErrInfo.tmTime);
	stErrInfo.szDesc = g_szErrorCode_H_Desc[lErrorCode];

	//m_wndErrorView.InsertErrorInfo(&stErrInfo);

	__super::OnAddErrorInfo(lErrorCode);
}

//=============================================================================
// Method		: OnSetCycleTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/14 - 17:50
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetCycleTime()
{
	__super::OnSetCycleTime();

	//m_wndMainView.UpdateCycleTime();
}

//=============================================================================
// Method		: OnUpdateElapsedTime_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in DWORD dwTime
// Qualifier	:
// Last Update	: 2017/2/18 - 14:03
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnUpdateElapsedTime_All(__in DWORD dwTime)
{	
	m_wndMainView.UpdateElapsedTime_All(dwTime);
}

//=============================================================================
// Method		: OnTestProgress
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestProcess nProcess
// Qualifier	:
// Last Update	: 2016/5/29 - 16:19
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnTestProgress(__in enTestProcess nProcess)
{
	__super::OnTestProgress(nProcess);

	m_wndMainView.UpdateTestProcess();
}

//=============================================================================
// Method		: OnTestResult
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestResult nResult
// Qualifier	:
// Last Update	: 2017/2/21 - 17:07
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnTestResult(__in enTestResult nResult)
{
	__super::OnTestResult(nResult);

	m_wndMainView.UpdateTestResult();
}

//=============================================================================
// Method		: OnTestErrCode
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LPCTSTR szErrorCode
// Qualifier	:
// Last Update	: 2017/2/21 - 17:07
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnTestErrCode(__in LPCTSTR szErrorCode)
{
	m_wndMainView.UpdatSetErrorCode(szErrorCode);
}

//=============================================================================
// Method		: OnTestFailResultCode_Unit
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in enResultCode_TestItem nFailCode
// Qualifier	:
// Last Update	: 2017/7/7 - 9:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnTestFailResultCode_Unit(__in UINT nUnitIdx, __in enResultCode_TestItem nFailCode)
{
	__super::OnTestFailResultCode_Unit(nUnitIdx, nFailCode);
}

//=============================================================================
// Method		: OnSetInputTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/7 - 9:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetInputTime()
{
	__super::OnSetInputTime();

	m_wndMainView.UpdateInputTime();
}

//=============================================================================
// Method		: OnSetResetSiteInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/18 - 15:29
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetResetSiteInfo()
{
	m_wndMainView.UpdateResetSiteInfo();
}

//=============================================================================
// Method		: OnSetInsertBarcode
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2017/2/21 - 16:58
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetInsertBarcode(__in LPCTSTR szBarcode)
{
	m_wndMainView.InsertBarcode(szBarcode);
}

//=============================================================================
// Method		: OnSetTestInitialize
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/7/7 - 9:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetTestInitialize(__in enTestEachResult EachResult, __in CString strText)
{
	__super::OnSetTestInitialize(EachResult, strText);
	m_wndMainView.UpdateTestInitialize(EachResult, strText);
}

//=============================================================================
// Method		: OnSetTestFinalize
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/7/7 - 9:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetTestFinalize(__in enTestEachResult EachResult, __in CString strText)
{
	__super::OnSetTestFinalize(EachResult, strText);
	m_wndMainView.UpdateTestFinalize(EachResult, strText);
}

//=============================================================================
// Method		: OnSetTestCurrent
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/2/21 - 9:36
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetTestCurrent(__in enTestEachResult EachResult, __in CString strText)
{
	__super::OnSetTestCurrent(EachResult, strText);
	m_wndMainView.UpdateTestCurrent(EachResult, strText);
}

//=============================================================================
// Method		: OnSetTestActiveAlgin
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/7/10 - 13:28
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetTestActiveAlgin(__in enTestEachResult EachResult, __in CString strText)
{
	__super::OnSetTestActiveAlgin(EachResult, strText);
	m_wndMainView.UpdateTestActiveAlgin(EachResult, strText);
}

//=============================================================================
// Method		: OnSetTestFocus
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/9/18 - 10:19
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetTestFocus(__in enTestEachResult EachResult, __in CString strText)
{
	__super::OnSetTestFocus(EachResult, strText);
	m_wndMainView.UpdateTestFocus(EachResult, strText);
}

//=============================================================================
// Method		: OnSetTestCenterAdjust
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/2/21 - 9:42
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetTestCenterAdjust(__in enTestEachResult EachResult, __in CString strText)
{
	__super::OnSetTestCenterAdjust(EachResult, strText);
	m_wndMainView.UpdateTestCenterAdjust(EachResult, strText);
}

//=============================================================================
// Method		: OnSetTestResolution
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/2/21 - 9:43
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetTestResolution(__in enTestEachResult EachResult, __in CString strText)
{
	__super::OnSetTestResolution(EachResult, strText);
	m_wndMainView.UpdateTestResolution(EachResult, strText);
}

//=============================================================================
// Method		: OnSetTestRotation
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/2/21 - 9:43
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetTestRotation(__in enTestEachResult EachResult, __in CString strText)
{
	__super::OnSetTestRotation(EachResult, strText);
	m_wndMainView.UpdateTestRotation(EachResult, strText);
}

//=============================================================================
// Method		: OnSetTestParticle
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Qualifier	:
// Last Update	: 2017/2/21 - 9:44
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetTestParticle(__in enTestEachResult EachResult, __in CString strText)
{
	__super::OnSetTestParticle(EachResult, strText);
	m_wndMainView.UpdateTestParticle(EachResult, strText);
}

//=============================================================================
// Method		: OnSetResult
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestResult Result
// Qualifier	:
// Last Update	: 2017/2/21 - 9:58
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetResult(__in enTestResult Result)
{
	__super::OnSetResult(Result);
	m_wndMainView.UpdatSetResult(Result);
}

void CView_MainCtrl::OnSetResult(__in enTestResult Result, __in CString strText)
{
	__super::OnSetResult(Result, strText);
	m_wndMainView.UpdatSetResult(Result, strText);
}

//=============================================================================
// Method		: OnBarcode_Input
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/25 - 14:01
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnBarcode_Input()
{	
	//m_Site[Site_A].OnSetBarcode(m_stInspInfo.CamObject[Site_A]->szBarcode);
}


//=============================================================================
// Method		: OnSetLotInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/2 - 18:55
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetLotInfo()
{
	m_wndMainView.UpdateLotInfo();
}


void CView_MainCtrl::OnSetMESInfo()
{
	m_wndMainView.UpdateMESInfo();
}


//=============================================================================
// Method		: OnSetMaterSetView
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bEnable
// Qualifier	:
// Last Update	: 2017/9/18 - 10:19
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetMaterSetView(__in BOOL bEnable)
{
	if (bEnable == TRUE)
	{
		OnSetResult(TR_Init, _T("Master Setting Mode"));
	}
	else
	{
		OnSetResult(TR_Pass, _T("Stand By"));
	}

	m_wndMainView.SetMasterMode(bEnable);
}

//=============================================================================
// Method		: OnSetIndicatorData
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in float fAxisX
// Parameter	: __in float fAxisY
// Qualifier	:
// Last Update	: 2017/1/12 - 13:52
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetIndicatorData(__in ST_ModelInfo &stModelInfo, __in float fAxisX, __in float fAxisY)
{
	((CPane_CommStatus*)m_pwndCommPane)->SetIndicatorDisplay(stModelInfo, fAxisX, fAxisY);
}

//=============================================================================
// Method		: OnHidePopupUI
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/22 - 23:03
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnHidePopupUI()
{
	__super::OnHidePopupUI();
}

void CView_MainCtrl::OnSetMasterOffSetData(__in int iOffsetX, __in int iOffsetY)
{
	m_wndMainView.UpdateMasterInfo(iOffsetX, iOffsetY);
}


void CView_MainCtrl::OnChangeBtnState(__in BOOL bMode)
{
	m_wndMainView.UpdateSetBtnChange(bMode);
}

void CView_MainCtrl::OnChangeStartBtnState(__in BOOL bMode)
{
	m_wndMainView.UpdateSetStartBtnChange(bMode);
}

//=============================================================================
// Method		: OnUpdateSiteCamInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/24 - 19:40
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnUpdateSiteCamInfo()
{
	//__super::OnUpdateSiteCamInfo();
}

//=============================================================================
// Method		: OnResetCamInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/7/15 - 14:34
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnResetCamInfo()
{
	__super::OnResetCamInfo();
	//m_wndMainView.ResetCamInfo();
}

//=============================================================================
// Method		: OnInsertWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/7 - 17:13
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnInsertWorklist()
{
	
}

//=============================================================================
// Method		: OnSaveWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/7 - 17:14
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSaveWorklist()
{
	

	// 파일 저장
	//m_FileReport.Save_Worklist(m_stInspInfo.Path.szLOT, &m_stInspInfo.Worklist_Array);
	
	// 수율 업데이트

	// UI 표시	
	
	//m_wndDeviceView.InsertWorklist(&m_stInspInfo.Worklist_Array);
}

//=============================================================================
// Method		: OnLoadWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/11 - 15:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnLoadWorklist()
{
	SYSTEMTIME tmLocal;
	GetLocalTime(&tmLocal);

	// 패스 (Report/2016/06/11)
// 	CString szPath;
// 	szPath.Format(_T("%s%04d\\%02d\\%02d\\"), m_stInspInfo.Path.szReport, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
// 	MakeDirectory(szPath);

// 	if (NULL != m_WorklistPtr.pList_TotalTest)
// 	{
// 		m_WorklistPtr.pList_TotalTest->DeleteAllItems();
// 		m_FileReport.LoadFile_List_TotalTest(szPath, *m_WorklistPtr.pList_TotalTest);
// 	}
}

//=============================================================================
// Method		: OnUpdateYield
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/28 - 10:25
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnUpdateYield()
{
	m_wndMainView.UpdateYield();
}

//=============================================================================
// Method		: OnSetStatus_PogoCount
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/20 - 16:32
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_PogoCount()
{
	m_stInspInfo.PogoInfo.IncreasePogoCount(0);
	SavePogoCount();
	m_wndMainView.UpdatePogoCnt();
}

//  [1/25/2019 ysJang]
void CView_MainCtrl::OnSetStatus_DriverCount()
{
	m_stInspInfo.DriverCountInfo.IncreaseDriverCount(0);
	SaveDriverCount();
	m_wndMainView.UpdateDriverCnt();
}

//=============================================================================
// Method		: OnChangeLotInfo
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in enModelStatus bLotStatus
// Qualifier	:
// Last Update	: 2017/1/4 - 13:27
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::OnChangeLotInfo(__in enModelStatus LotStatus)
{
	if (MESMode == LotStatus)
	{
		// MES & LOT Start
		m_stInspInfo.SetLotStatus(TRUE);

		if (m_stInspInfo.ModelInfo.szModelCode.IsEmpty())
		{
			m_stInspInfo.szModelName = _T("DEFAULT");
		}
		else
		{
			m_stInspInfo.szModelName = m_stInspInfo.ModelInfo.szModelCode;
		}

		if (0 != m_stInspInfo.szLotName.Compare(m_stInspInfo.MESInfo.stMESResult.szLotNo))
		{
			// LOT 변경시 수율, Cycle Time 초기화 및 UI 업데이트
			OnResetYieldCycleTime();
		}

		if ((0 != m_stInspInfo.szLotName.Compare(m_stInspInfo.MESInfo.stMESResult.szLotNo))
			|| (0 != m_stInspInfo.szOperatorName.Compare(m_stInspInfo.MESInfo.stMESResult.szOperator)))
		{
			m_stInspInfo.SetLotInfo(m_stInspInfo.MESInfo.stMESResult.szLotNo, m_stInspInfo.MESInfo.stMESResult.szOperator);
			OnSetLotInfo();
		}
		else
		{
			m_stInspInfo.SetLotInfo(m_stInspInfo.MESInfo.stMESResult.szLotNo, m_stInspInfo.MESInfo.stMESResult.szOperator);
		}

		SYSTEMTIME Time;
		GetLocalTime(&Time);

		CString path = m_Worklist.Get_Report_Yield_FullPath(m_stInspInfo.Path.szReport, m_stInspInfo.szModelName, m_stInspInfo.MESInfo.stMESResult.szLotNo, &Time);
		m_FileReport.LoadYield_LOT(path, m_stInspInfo.YieldInfo);
		m_wndMainView.UpdateYield();

		m_stInspInfo.LotInfo.nLotCount = m_stInspInfo.YieldInfo.dwTotal;
		OnSetLotInfo();
		OnSetMESInfo();


	}
	else if (LOT_Start == LotStatus)
	{
		CDlg_SelectLot dlgLot;
		dlgLot.SetInfo(m_stInspInfo.Path.szReport, m_stInspInfo.ModelInfo.szModelCode);
		dlgLot.SetLotName(m_stInspInfo.szLotName);
		dlgLot.SetOperator(m_stInspInfo.szOperatorName);

		if (m_stInspInfo.ModelInfo.szModelCode.IsEmpty())
		{
			m_stInspInfo.szModelName = _T("DEFAULT");
		}
		else
		{
			m_stInspInfo.szModelName = m_stInspInfo.ModelInfo.szModelCode;
		}

		if (IDOK == dlgLot.DoModal())
		{
			// LOT Start
			m_stInspInfo.SetLotStatus(TRUE);

			if (0 != m_stInspInfo.szLotName.Compare(dlgLot.GetLotName()))
			{
				// LOT 변경시 수율, Cycle Time 초기화 및 UI 업데이트
				OnResetYieldCycleTime();
			}

			if ((0 != m_stInspInfo.szLotName.Compare(dlgLot.GetLotName())) || (0 != m_stInspInfo.szOperatorName.Compare(dlgLot.GetOperatorName())))
			{
				m_stInspInfo.SetLotInfo(dlgLot.GetLotName(), dlgLot.GetOperatorName());			
				OnSetLotInfo();
			}
			else
			{
				m_stInspInfo.SetLotInfo(m_stInspInfo.szLotName, m_stInspInfo.szOperatorName);
			}

			SYSTEMTIME Time;
			GetLocalTime(&Time);

			CString path = m_Worklist.Get_Report_Yield_FullPath(m_stInspInfo.Path.szReport, m_stInspInfo.szModelName, m_stInspInfo.LotInfo.szLotName, &Time);
			m_FileReport.LoadYield_LOT(path, m_stInspInfo.YieldInfo);
			m_wndMainView.UpdateYield();

			m_stInspInfo.LotInfo.nLotCount = m_stInspInfo.YieldInfo.dwTotal;
			OnSetLotInfo();
		}
		else
		{
			return FALSE;
		}
	}
	else if (LOT_End == LotStatus)
	{
		// LOT End
		m_stInspInfo.SetLotStatus(FALSE);
		OnSetLotInfo();
	}

	return TRUE;
}

//=============================================================================
// Method		: OnChangeModelInfo
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/6/22 - 10:38
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::OnChangeModelInfo()
{
	AfxGetApp()->GetMainWnd()->EnableWindow(FALSE);
	m_wndModelMode.MoveWindow(0, 0, 420, 200);
	m_wndModelMode.CenterWindow();

	m_wndModelMode.SetModleInfo(m_stInspInfo.Path.szModel, MODEL_FILE_EXT);
	m_wndModelMode.SetIniFileInfo(Common_AppName, ModelCode_KeyName);
	m_wndModelMode.SetCurrentModel(m_stInspInfo.ModelInfo.szModelFile);

	m_wndModelMode.ShowWindow(SW_SHOW);
	m_wndModelMode.EnableWindow(TRUE);

	return TRUE;
}

//=============================================================================
// Method		: OnResetYieldCycleTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/21 - 11:07
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnResetYieldCycleTime()
{
	m_stInspInfo.YieldInfo.Reset();
	m_stInspInfo.CycleTime.Reset();

	m_wndMainView.UpdateYield();
}

//=============================================================================
// Method		: LoadModelInfo
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szModel
// Parameter	: BOOL bNotifyModelWnd
// Qualifier	:
// Last Update	: 2016/5/28 - 14:50
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::LoadModelInfo(__in LPCTSTR szModel, BOOL bNotifyModelWnd /*= TRUE*/)
{
		// 모델 파일에서 모델 정보 불러오기
	CString strFullPath;
	CString strRomFullPath;
	CString strLog;

	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szModel, szModel, MODEL_FILE_EXT);

	if (!bNotifyModelWnd)
	{
		ShowSplashScreen();
		m_wndSplash.SetText(_T("Changing Model..."));
	}

	// 모델 변경
	m_stInspInfo.ModelInfo.szModelFile = szModel;
	
	// 파일 불러오기
	CFile_Model		m_fileModel;
 	if (m_fileModel.LoadModelFile(strFullPath, m_stInspInfo.ModelInfo))
 	{
		// 선택한 모델 레지스트리에 저장
		m_regInspInfo.SaveSelectedModel(m_stInspInfo.ModelInfo.szModelFile, m_stInspInfo.ModelInfo.szModelCode);

		// UI 갱신
		m_wndMainView.UpdateModelInfo();

		//OnSetGrabberBrd_ComArt_Reboot(m_stInspInfo.ModelInfo.nVideoType);

		// 데이터 초기화
		OnResetCamInfo();

		//if (bNotifyModelWnd)
		{
			m_wndRecipeView.SetModel(m_stInspInfo.ModelInfo.szModelFile);
		}	

		// 모델 정보 불러오기 완료
		strLog.Format(_T("File load completed. [File: %s]"), m_stInspInfo.ModelInfo.szModelFile);
		AddLog(strLog);
 	}
 	else
 	{
		ShowSplashScreen(FALSE);

 		strLog.Format(_T("Cannot load the Model File. [File: %s.luri]"), szModel);
 		AddLog(strLog);
		strLog.Format(_T("Cannot load the Model File.\r\nFile: %s"), strFullPath);
		MessageView(strLog);

		// UI 갱신
		m_wndMainView.UpdateModelInfo();

		//OnSetGrabberBrd_ComArt_Reboot(m_stInspInfo.ModelInfo.nVideoType);

		// 데이터 초기화
		OnResetCamInfo();

		//if (bNotifyModelWnd)
		{
			m_wndRecipeView.SetModel(m_stInspInfo.ModelInfo.szModelFile);
		}
 	}

	ShowSplashScreen(FALSE);
	
	// 포고 카운트 설정
	LoadPogoCount();

// 	CString strText;
// 	strText = m_stInspInfo.Path.szI2C + m_stInspInfo.ModelInfo.szI2CFile + _T(".") + I2C_FILE_EXT;
// 	
// 	// 파일이 존재하는가?
// 	if (PathFileExists(strText))
// 	{
// 		m_Device.DAQCtrl.SetI2CFilePath_All(strText);
// 	}


	//----------------------------
	// LOG Model Info
	//----------------------------
	// Model
	// Modle Code
	//----------------------------
	

	return TRUE;
}

//=============================================================================
// Method		: InitLoadModelInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/8/11 - 15:09
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitLoadModelInfo()
{
	CString strModelFile;
	CString strModelCode;
	CString strMotorFile;

	if (m_regInspInfo.LoadSelectedModel(strModelFile, strModelCode))
	{
		m_stInspInfo.ModelInfo.szModelFile = strModelFile;
		LoadModelInfo(strModelFile);
	}
	else
	{
		MessageView(_T("Set the Model Data."));
	}


	if (m_regInspInfo.LoadSelectedMotor(strMotorFile))
	{
		m_stInspInfo.ModelInfo.szMotorFile = strMotorFile;
		m_Device.MotionManager.SetPrtMotorPath(&m_stInspInfo.Path.szMotor, m_stInspInfo.ModelInfo.szMotorFile);
		
		if (m_Device.MotionManager.LoadMotionParam())
		{
			m_regInspInfo.SaveSelectedMotor(m_stInspInfo.ModelInfo.szMotorFile);
			//m_wndMotorView.SetModel(m_stInspInfo.Path.szMotor, m_stInspInfo.ModelInfo.szMotorFile);
		}

		//m_wndMotorView.SetUpdataData();
	}
	else
	{
		//AfxMessageBox(_T("MOTOR 데이터를 설정하세요."), MB_SYSTEMMODAL);
	}
}

//=============================================================================
// Method		: EVMSPathSearch
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __out CString & szEVNPath
// Qualifier	:
// Last Update	: 2017/8/12 - 17:18
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::EVMSPathSearch(__out CString &szEVNPath)
{
	// 프로그램 폴더 구하기
	TCHAR szExePath[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, szExePath, MAX_PATH);

	CString	szStr;

	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];
	TCHAR file[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];
	TCHAR backPath[2048];

	_tsplitpath_s(szExePath, drive, _MAX_DRIVE, dir, _MAX_DIR, file, _MAX_FNAME, ext, _MAX_EXT);

	szEVNPath.Format(_T("%s%s"), drive, dir);

	szStr = szEVNPath + _T("..");
	PathCanonicalize(backPath, szStr);//패스 정리
	szEVNPath.Format(_T("%s"), backPath);

	szEVNPath += _T("\\ENV\\MODEL");

	MakeSubDirectory(szEVNPath);

	return TRUE;
}

//=============================================================================
// Method		: DisplayVideo
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Parameter	: __in LPBYTE lpbyRGB
// Parameter	: __in DWORD dwRGBSize
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Qualifier	:
// Last Update	: 2016/12/28 - 16:18
// Desc.		:영상 뿌려지는 부분/ Pic
//=============================================================================
void CView_MainCtrl::DisplayVideo(__in UINT nChIdx, __in LPBYTE lpbyRGB, __in DWORD dwRGBSize, __in UINT nWidth, __in UINT nHeight)
{
// 	BITMAPINFO		m_biInfo;
// 	ZeroMemory(&m_biInfo, sizeof(m_biInfo));
// 	m_biInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
// 	m_biInfo.bmiHeader.biWidth = nWidth;//CAM_IMAGE_WIDTH;
// 	m_biInfo.bmiHeader.biHeight = nHeight;//CAM_IMAGE_HEIGHT;
// 	m_biInfo.bmiHeader.biPlanes = 1;
// 	m_biInfo.bmiHeader.biBitCount = 32;
// 	m_biInfo.bmiHeader.biCompression = 0;
// 	m_biInfo.bmiHeader.biSizeImage = dwRGBSize;
// 
// 	CDC *pcDC;
// 
// 	//-----------가상메모리를 만든다.------------------
// 	CDC	*pcMemDC = new CDC;
// 	CBitmap	*pcDIB = new CBitmap;
// 
// 	pcDC = m_wndMainView.m_wnd_SiteInfo.m_wnd_Video.m_stVideo.GetDC();
// 	pcMemDC->CreateCompatibleDC(pcDC);
// 	pcDIB->CreateCompatibleBitmap(pcDC, nWidth, nHeight);
// 	CBitmap	*pcOldDIB = pcMemDC->SelectObject(pcDIB);
// 
// 	::SetStretchBltMode(pcMemDC->m_hDC, COLORONCOLOR);
// 	::StretchDIBits(pcMemDC->m_hDC,
// 		0, nHeight - 1,
// 		nWidth, (-1) * nHeight,
// 		0, 0,
// 		m_biInfo.bmiHeader.biWidth,
// 		m_biInfo.bmiHeader.biHeight,
// 		lpbyRGB, &m_biInfo, DIB_RGB_COLORS, SRCCOPY);
// 
// 
// 
// 	if (m_wndRecipeView.IsWindowVisible() == TRUE)
// 	{
// 		CRect rcWnd;
// 		m_wndRecipeView.m_wnd_ImageView.m_stVideo.GetClientRect(rcWnd);
// 		DisplayVideoRecipePic(rcWnd, pcMemDC);
// 	}
// 	else
// 	{
// 		CRect rcWnd;
// 		m_wndMainView.m_wnd_SiteInfo.m_wnd_Video.m_stVideo.GetClientRect(rcWnd);
// 		DisplayVideoPic(rcWnd, pcMemDC);
// 	}
// 
// 	//--------------모니터 창에 맞추어 RGB정보를 할당한다.-------------------------
// 
// 	CRect rt;
// 	m_wndMainView.m_wnd_SiteInfo.m_wnd_Video.m_stVideo.GetClientRect(rt);
// 	::SetStretchBltMode(pcDC->m_hDC, COLORONCOLOR);
// 	::StretchBlt(pcDC->m_hDC, 0, 0, rt.Width(), rt.Height(), pcMemDC->m_hDC, 0, 0, nWidth, nHeight, SRCCOPY);
// 	m_wndMainView.m_wnd_SiteInfo.m_wnd_Video.m_stVideo.ReleaseDC(pcDC);
// 
// 	// Recipe View
// 	if (m_wndRecipeView.m_wnd_ImageView.IsWindowVisible() == TRUE)
// 	{
// 		CDC *pcDC2;
// 		pcDC2 = m_wndRecipeView.m_wnd_ImageView.m_stVideo.GetDC();
// 
// 		CRect rt;
// 		m_wndRecipeView.m_wnd_ImageView.m_stVideo.GetClientRect(rt);
// 		::SetStretchBltMode(pcDC2->m_hDC, COLORONCOLOR);
// 		::StretchBlt(pcDC2->m_hDC, 0, 0, rt.Width(), rt.Height(), pcMemDC->m_hDC, 0, 0, nWidth, nHeight, SRCCOPY);
// 		m_wndRecipeView.m_wnd_ImageView.m_stVideo.ReleaseDC(pcDC2);
// 	}
// 
// 	delete pcDIB;
// 	delete pcMemDC;
}

void CView_MainCtrl::DisplayVideo_Overlay(__in UINT nChIdx, __in enPic_TestItem enItem, __inout IplImage *TestImage)
{
	if (NULL == TestImage)
		return;

	switch (enItem)
	{
	case PIC_Standby:
		break;
	case PIC_Current:
		m_OverlayProc.Overlay_Current(TestImage, m_stInspInfo.ModelInfo.stCurrent.stCurrentOp[0], m_stInspInfo.ModelInfo.stCurrent.stCurrentData);
		break;
	case PIC_CenterAdjust:
		m_OverlayProc.Overlay_OpticalCenter(TestImage, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointOp, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData);
		break;
	case PIC_EIAJ:
		m_OverlayProc.Overlay_EIAJ(TestImage, m_stInspInfo.ModelInfo.stEIAJ.stEIAJOp, m_stInspInfo.ModelInfo.stEIAJ.stEIAJData);
		break;
	case PIC_SFR:
		m_OverlayProc.Overlay_SFR(TestImage, m_stInspInfo.ModelInfo.stSFR.stSFROp, m_stInspInfo.ModelInfo.stSFR.stSFRData);
		break;
	case PIC_Particle:
		break;
	case PIC_Rotation:
		break;
	case PIC_ActiveAlgin:
		m_OverlayProc.Overlay_OpticalCenter(TestImage, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointOp, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData);
		m_OverlayProc.Overlay_EIAJ(TestImage, m_stInspInfo.ModelInfo.stEIAJ.stEIAJOp, m_stInspInfo.ModelInfo.stEIAJ.stEIAJData);
		break;
	case PIC_Center_EIAJ:
		m_OverlayProc.Overlay_OpticalCenter(TestImage, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointOp, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData);
		m_OverlayProc.Overlay_EIAJ(TestImage, m_stInspInfo.ModelInfo.stEIAJ.stEIAJOp, m_stInspInfo.ModelInfo.stEIAJ.stEIAJData);
		break;
	case PIC_Center_SFR:
		m_OverlayProc.Overlay_OpticalCenter(TestImage, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointOp, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData);
		m_OverlayProc.Overlay_SFR(TestImage, m_stInspInfo.ModelInfo.stSFR.stSFROp, m_stInspInfo.ModelInfo.stSFR.stSFRData);
		break;
	case PIC_Center_Rotation:
		break;
	case PIC_EIAJ_Rotation:
		break;
	case PIC_SFR_Rotation:
		break;
	case PIC_SFR_Rotation_Center:
		break;
	case PIC_TotalPic:
		m_OverlayProc.Overlay_Current(TestImage, m_stInspInfo.ModelInfo.stCurrent.stCurrentOp[0], m_stInspInfo.ModelInfo.stCurrent.stCurrentData);
		m_OverlayProc.Overlay_OpticalCenter(TestImage, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointOp, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData);
		m_OverlayProc.Overlay_EIAJ(TestImage, m_stInspInfo.ModelInfo.stEIAJ.stEIAJOp, m_stInspInfo.ModelInfo.stEIAJ.stEIAJData);
		break;
	case PIC_Align:
		m_OverlayProc.Overlay_OpticalCenter(TestImage, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointOp, m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData);
		m_OverlayProc.Overlay_EIAJ(TestImage, m_stInspInfo.ModelInfo.stEIAJ.stEIAJOp, m_stInspInfo.ModelInfo.stEIAJ.stEIAJData);

		break;
	case PIC_MaxEnum:
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: DisplayVideoPic
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in CDC * pCDC
// Qualifier	:
// Last Update	: 2017/7/7 - 9:45
// Desc.		:
//=============================================================================
// void CView_MainCtrl::DisplayVideoPic(__in CRect rcSize, __in CDC *pCDC)
// {
// 	switch (m_stInspInfo.ModelInfo.nPicViewMode)
// 	{
// 	case PIC_Current:
// 		m_TIPicControl.CurrentPic(pCDC);
// 		break;
// 
// 	case PIC_CenterAdjust:
// 		m_TIPicControl.CenterPointPic(pCDC);
// 		break;
// 
// 	case PIC_EIAJ:
// 		m_TIPicControl.EIAJPic(pCDC);
// 		break;
// 
// 	case PIC_SFR:
// 		m_TIPicControl.SFRPic(rcSize, pCDC);
// 		break;
// 
// 	case PIC_Rotation:
// 		m_TIPicControl.RotatePic(pCDC);
// 		break;
// 
// 	case PIC_Center_EIAJ:
// 		m_TIPicControl.CenterPointPic(pCDC);
// 		m_TIPicControl.EIAJPic(pCDC);
// 		break;
// 
// 	case PIC_Center_SFR:
// 		m_TIPicControl.CenterPointPic(pCDC);
// 		m_TIPicControl.SFRPic(rcSize, pCDC);
// 		break;
// 
// 	case PIC_Center_Rotation:
// 		m_TIPicControl.CenterPointPic(pCDC);
// 		m_TIPicControl.RotatePic(pCDC);
// 		break;
// 
// 	case PIC_EIAJ_Rotation:
// 		m_TIPicControl.EIAJPic(pCDC);
// 		m_TIPicControl.RotatePic(pCDC);
// 		break;
// 
// 	case PIC_SFR_Rotation:
// 		m_TIPicControl.SFRPic(rcSize, pCDC);
// 		m_TIPicControl.RotatePic(pCDC);
// 		break;
// 
// 	case PIC_SFR_Rotation_Center:
// 		m_TIPicControl.SFRPic(rcSize, pCDC);
// 		m_TIPicControl.RotatePic(pCDC);
// 		m_TIPicControl.CenterPointPic(pCDC);
// 		break;
// 
// 	case PIC_Particle:
// 		m_TIPicControl.ParticlePic(pCDC);
// 		break;
// 
// 	case PIC_TotalPic:
// 		m_TIPicControl.CenterPointPic(pCDC);
// 		m_TIPicControl.SFRPic(rcSize, pCDC);
// 		m_TIPicControl.CurrentPic(pCDC);
// 		//m_TIPicControl.RotatePic(pCDC);
// 		break;
// 
// 	case PIC_ActiveAlgin:
// 		m_TIPicControl.CenterPointPic(pCDC);
// 		m_TIPicControl.EIAJPic(pCDC);
// 		//m_TIPicControl.SFRPic(rcSize, pCDC);
// 		break;
// 
// 	default:
// 		break;
// 	}
// }

//=============================================================================
// Method		: DisplayVideoRecipePic
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in CDC * pCDC
// Qualifier	:
// Last Update	: 2017/7/7 - 9:45
// Desc.		:
//=============================================================================
// void CView_MainCtrl::DisplayVideoRecipePic(__in CRect rcSize, __in CDC *pCDC)
// {
// 	UINT nMode = 0;
// 
// 	nMode = m_wndRecipeView.GetVideoPicStatus();
// 
// 	switch (nMode)
// 	{
// 	case PIC_Current:
// 		m_wndRecipeView.m_TIPicControl.CurrentPic(pCDC);
// 		break;
// 
// 
// 	case PIC_CenterAdjust:
// 		m_wndRecipeView.m_TIPicControl.CenterPointPic(pCDC);
// 		break;
// 
// 	case PIC_EIAJ:
// 		m_wndRecipeView.m_TIPicControl.EIAJPic(pCDC);
// 		break;
// 
// 	case PIC_SFR:
// 		m_wndRecipeView.m_TIPicControl.SFRPic(rcSize, pCDC);
// 		break;
// 
// 	case PIC_Rotation:
// 		m_wndRecipeView.m_TIPicControl.RotatePic(pCDC);
// 		break;
// 
// 	case PIC_Particle:
// 		m_wndRecipeView.m_TIPicControl.ParticlePic(pCDC);
// 		break;
// 
// 	case PIC_Align:
// 		m_wndRecipeView.m_TIPicControl.AlignPic(pCDC);
// 		break;
// 
// 	default:
// 		break;
// 	}
// }

//=============================================================================
// Method		: DisplayVideo_NoSignal
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Qualifier	:
// Last Update	: 2016/12/28 - 16:18
// Desc.		: 영상 신호 없을 때
//=============================================================================
void CView_MainCtrl::DisplayVideo_NoSignal(__in UINT nChIdx)
{
	m_wndMainView.m_wnd_SiteInfo.m_wnd_ImgProc.Render_Empty();
// 	Delay(100);
// 
// 	CDC *pcDC;
// 
// 	m_stInspInfo.ModelInfo.nPicViewMode = PIC_Standby;
// 	//-----------가상메모리를 만든다.------------------
// 	CDC	*pcMemDC = new CDC;
// 	CBitmap	*pcDIB = new CBitmap;
// 
// 	CRect rt1;
// 	m_wndMainView.m_wnd_SiteInfo.m_wnd_Video.m_stVideo.GetClientRect(rt1);
// 
// 	pcDC = m_wndMainView.m_wnd_SiteInfo.m_wnd_Video.m_stVideo.GetDC();
// 	pcMemDC->CreateCompatibleDC(pcDC);
// 	pcDIB->CreateCompatibleBitmap(pcDC, rt1.Width(), rt1.Height());
// 	CBitmap	*pcOldDIB = pcMemDC->SelectObject(pcDIB);
// 
// 	//---------------------------------------------
// 	::SetStretchBltMode(pcMemDC->m_hDC, COLORONCOLOR);
// 
// 	//----
// 	CBrush NewBrush;
// 	NewBrush.CreateStockObject(BLACK_BRUSH);
// 	CBrush* pOldBrush = pcMemDC->SelectObject(&NewBrush);
// 
// 	pcMemDC->SetBkMode(TRANSPARENT);
// 
// 	LOGBRUSH lb;
// 	lb.lbStyle = BS_SOLID;
// 	lb.lbColor = RGB(0, 0, 0);
// 	CPen NewPen;
// 	NewPen.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT, 1, &lb);
// 	CPen* pOldPen = pcMemDC->SelectObject(&NewPen);
// 
// 	CRect rt;
// 	m_wndMainView.m_wnd_SiteInfo.m_wnd_Video.m_stVideo.GetClientRect(rt);
// 
// 	pcMemDC->Rectangle(CRect(0, 0, m_stInspInfo.ModelInfo.dwWidth, m_stInspInfo.ModelInfo.dwHeight));
// 
// 	CFont font;
// 	font.CreatePointFont(200, _T("Arial"));
// 	pcMemDC->SelectObject(&font);
// 	pcMemDC->SetTextAlign(TA_LEFT | TA_BASELINE);
// 	CString strText = _T("No Signal");
// 	pcMemDC->SetTextColor(RGB(255, 0, 0));
// 	pcMemDC->TextOut(20, 60, strText, strText.GetLength());
// 	font.DeleteObject();
// 
// 	pcMemDC->SelectObject(pOldBrush);
// 	pcMemDC->SelectObject(pOldPen);
	
	// Recipe View
	if (m_wndRecipeView.IsWindowVisible() == TRUE && m_stImageMode.eImageMode == ImageMode_LiveCam)
	{
		if (m_wndRecipeView.IsWindowVisible() == TRUE)
		{
			m_wndRecipeView.m_wnd_ImgProc.Render_Empty();
// 			CDC *pcDC2;
// 			pcDC2 = m_wndRecipeView.m_wnd_ImageView.m_stVideo.GetDC();
//			CRect rt;
// 			m_wndRecipeView.m_wnd_ImageView.m_stVideo.SetColorStyle(CVGStatic::ColorStyle_Black);
// 			m_wndRecipeView.m_wnd_ImageView.m_stVideo.SetText(_T(""));

//			m_wndRecipeView.m_wnd_ImageView.m_stVideo.GetClientRect(rt);
// 			::SetStretchBltMode(pcDC2->m_hDC, COLORONCOLOR);
// 			::StretchBlt(pcDC2->m_hDC,
// 				0, 0,
// 				rt.Width(), rt.Height(),
// 				pcMemDC->m_hDC,
// 				0, 0,
// 				m_stInspInfo.ModelInfo.dwWidth,
// 				m_stInspInfo.ModelInfo.dwHeight, SRCCOPY);
// 			m_wndRecipeView.m_wnd_ImageView.m_stVideo.ReleaseDC(pcDC2);
		}
	}

	//--------------모니터 창에 맞추어 RGB정보를 할당한다.-------------------------
// 	::SetStretchBltMode(pcDC->m_hDC, COLORONCOLOR);
// 	::StretchBlt(pcDC->m_hDC, 0, 0, rt.Width(), rt.Height(), pcMemDC->m_hDC, 0, 0, m_stInspInfo.ModelInfo.dwWidth, m_stInspInfo.ModelInfo.dwHeight, SRCCOPY);

// 	m_wndMainView.m_wnd_SiteInfo.m_wnd_Video.m_stVideo.SetColorStyle(CVGStatic::ColorStyle_Black);
// 	m_wndMainView.m_wnd_SiteInfo.m_wnd_Video.m_stVideo.SetText(_T(""));

// 	m_wndMainView.m_wnd_SiteInfo.m_wnd_Video.m_stVideo.ReleaseDC(pcDC);
// 	delete pcDIB;
// 	delete pcMemDC;
}


//=============================================================================
// Method		: DisplayVideo_LastImage
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Qualifier	:
// Last Update	: 2016/12/28 - 16:18
// Desc.		: 마지막 이미지
//=============================================================================
void CView_MainCtrl::DisplayVideo_LastImage(__in UINT nChIdx)
{

}

//=============================================================================
// Method		: CView_MainCtrl::AddLog
// Access		: public 
// Returns		: void
// Parameter	: LPCTSTR lpszLog
// Parameter	: BOOL bError
// Parameter	: UINT nLogType
// Parameter	: BOOL bOnlyLogType
// Qualifier	:
// Last Update	: 2013/1/16 - 15:39
// Desc.		:
//=============================================================================
void CView_MainCtrl::AddLog(LPCTSTR lpszLog, BOOL bError /*= FALSE*/, UINT nLogType /*= LOGTYPE_NORMAL*/, BOOL bOnlyLogType /*= FALSE*/)
{
	if (!GetSafeHwnd())
		return;

	if (NULL == lpszLog)
		return;

	__try
	{
		TCHAR		strTime[255] = { 0 };
		UINT_PTR	nLogSize = _tcslen(lpszLog) + 255;
		LPTSTR		lpszOutLog = new TCHAR[nLogSize];
		SYSTEMTIME	LocalTime;

		// **** 시간 추가 ****
		GetLocalTime(&LocalTime);
		StringCbPrintf(strTime, sizeof(strTime), _T("[%02d:%02d:%02d.%03d] "), LocalTime.wHour, LocalTime.wMinute, LocalTime.wSecond, LocalTime.wMilliseconds);

		// 파일 처리 ------------------------------------------------
		StringCbPrintf(lpszOutLog, nLogSize, _T("%s%s \r\n"), strTime, lpszLog);

		//switch (nLogType)
		//{
// 		case LOGTYPE_GUI:
// 			StringCbPrintf(lpszOutLog, nLogSize, _T("%s[GUI] %s \r\n"), strTime, lpszLog);
// 			break;
// 
// 		case LOGTYPE_COM:
// 			StringCbPrintf(lpszOutLog, nLogSize, _T("%s[COM] %s \r\n"), strTime, lpszLog);
// 			break;
// 
// 		case LOGTYPE_OPR:
// 			StringCbPrintf(lpszOutLog, nLogSize, _T("%s[OPR] %s \r\n"), strTime, lpszLog);
// 			break;
// 
// 		case LOGTYPE_NORMAL:
		//default:
		//	StringCbPrintf(lpszOutLog, nLogSize, _T("%s%s \r\n"), strTime, lpszLog);
		//	break;
		//}

		if (bError)
			m_Log_ErrLog.LogWrite(lpszOutLog);

		// UI 처리 --------------------------------------------------
		m_wndLogView.AddLog(lpszOutLog, bError, nLogType, RGB(0, 0, 0));
		m_logFile.LogWrite(lpszOutLog);

		delete[] lpszOutLog;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CView_MainCtrl::AddLog () \n"));
	}
}

//=============================================================================
// Method		: CView_MainCtrl::SetBackgroundColor
// Access		: protected 
// Returns		: void
// Parameter	: COLORREF color
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2010/10/13 - 10:40
// Desc.		:
//=============================================================================
void CView_MainCtrl::SetBackgroundColor(COLORREF color, BOOL bRepaint /*= TRUE*/)
{
	if (m_brBkgr.GetSafeHandle() != NULL)
	{
		m_brBkgr.DeleteObject();
	}

	if (color != (COLORREF)-1)
	{
		m_brBkgr.CreateSolidBrush(color);
	}

	if (bRepaint && GetSafeHwnd() != NULL)
	{
		Invalidate();
		UpdateWindow();
	}
}

//=============================================================================
// Method		: CView_MainCtrl::SwitchWindow
// Access		: public 
// Returns		: UINT
// Parameter	: UINT nIndex
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		: 자식 윈도우 전환하는 함수
// MainView에서 선택된 검사기 번호를 다른 윈도우로 넘긴다.
//=============================================================================
UINT CView_MainCtrl::SwitchWindow(UINT nIndex)
{
	if (m_nWndIndex == nIndex)
		return m_nWndIndex;

	UINT nOldView = m_nWndIndex;
	m_nWndIndex = nIndex;

	if (nOldView != -1)
	{
		if (m_pWndPtr[nOldView]->GetSafeHwnd())
		{
			m_pWndPtr[nOldView]->ShowWindow(SW_HIDE);
		}
	}

	if (m_pWndPtr[m_nWndIndex]->GetSafeHwnd())
	{
		m_pWndPtr[m_nWndIndex]->ShowWindow(SW_SHOW);
	}

	// 모델뷰 -> 메인뷰로 전환시 모델 데이터 갱신
	if ((SUBVIEW_RECIPE == nOldView) && (SUBVIEW_MAIN == m_nWndIndex))
	{
		m_wndRecipeView.InitOptionView();
	}

	if ((SUBVIEW_RECIPE == m_nWndIndex))
	{
		m_wndRecipeView.InitOptionView();
		m_wndRecipeView.SetModel(m_stInspInfo.ModelInfo.szModelFile);
	}

	m_wndRecipeView.ChangeImageMode(ImageMode_LiveCam);

	return m_nWndIndex;
}

//=============================================================================
// Method		: CView_MainCtrl::SetCommPanePtr
// Access		: public 
// Returns		: void
// Parameter	: CWnd * pwndCommPane
// Qualifier	:
// Last Update	: 2013/7/16 - 16:51
// Desc.		:
//=============================================================================
void CView_MainCtrl::SetCommPanePtr(CWnd* pwndCommPane)
{
	m_pwndCommPane = pwndCommPane;
}

//=============================================================================
// Method		: ReloadOption
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2014/8/3 - 22:27
// Desc.		:
//=============================================================================
void CView_MainCtrl::ReloadOption()
{
	stLT_Option tempOpt = m_stOption;

	OnLoadOption();

	BOOL bChanged = FALSE;

	// MES 주소 변경
	//m_stOption.MES.Address.dwAddress;
	//m_stOption.MES.Address.dwPort;

}

//=============================================================================
// Method		: CView_MainCtrl::InitStartProgress
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2014/7/5 - 10:49
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitStartProgress()
{
	
	ShowSplashScreen();

	m_wndSplash.SetText(_T("Loading Model File..."));

	OnLoadWorklist();

	// 모델 정보 로드
	InitLoadModelInfo();

	m_wndSplash.SetText(_T("Start Device Connection...."));

	// 주변 장치 연결
	__try
	{
		ConnectDevicez();
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : InitStartProgress ()"));
	}

	m_wndSplash.SetText(_T("Start Device init...."));

	// 검사 가능 상태로 변경
	if (InitStartBoardProgress() == FALSE)
	{
		m_bFlag_ReadyTest = FALSE;
	}
	else
	{
		m_bFlag_ReadyTest = TRUE;
	}

	ShowSplashScreen(FALSE);
}

//=============================================================================
// Method		: InitStartBoardProgress
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/7 - 10:26
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::InitStartBoardProgress()
{
	// IO 초기값
	if (m_Device.DigitalIOCtrl.AXTState() == TRUE)
	{
		for (BYTE nBitOffset = 0; nBitOffset < DI_NotUseBit_Max; nBitOffset++)
		{
			m_stInspInfo.bDigitalIn[nBitOffset] = m_Device.DigitalIOCtrl.GetInStatus(nBitOffset);
		}

		// EMO  
		if (m_Device.DigitalIOCtrl.GetInStatus(DI_EMO) == FALSE)
		{
			ShowSplashScreen(FALSE);
			MessageView(_T("Err, The EMO Button is Pressed"));
			return FALSE;
		}
	}

	// 카메라 OFF
	for (UINT nIdx = 0; nIdx < g_InspectorTable[SET_INSPECTOR].nPCBCamCnt; nIdx++)
	{

		float volt = 0;

		// VOLT
		if (m_Device.PCBCamBrd[nIdx].Send_SetVolt(volt) == FALSE)
		{
			ShowSplashScreen(FALSE);
			MessageView(_T("Camera Board Init Err"));
			//return FALSE;
		}
	}

	// 준비 상태
	if (m_Device.DigitalIOCtrl.AXTState() == TRUE)
	{
		if (m_Device.MotionSequence.TowerLamp(TowerLampGreen, ON) == FALSE)
		{
			ShowSplashScreen(FALSE);
			MessageView(_T("Tower Lamp Err"));
			//return FALSE;
		}

		if (m_Device.MotionSequence.ButtonLampControl(DO_StartBtnL_Lamp, IO_SignalT_ToggleStart) == FALSE)
		{
			ShowSplashScreen(FALSE);
			MessageView(_T("Btn Toggle Err"));
			//return FALSE;
		}
		if (m_Device.MotionSequence.ButtonLampControl(DO_StartBtnR_Lamp, IO_SignalT_ToggleStart) == FALSE)
		{
			ShowSplashScreen(FALSE);
			MessageView(_T("Btn Toggle Err"));
			//return FALSE;
		}

	}

	// 버튼 사용 가능
	for (UINT nIdx = 0; nIdx < DI_NotUseBit_Max; nIdx++)
		m_bFlag_Button[nIdx] = FALSE;

	return TRUE;
}

//=============================================================================
// Method		: CView_MainCtrl::FinalExitProgress
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2016/06/13
// Desc.		: 프로그램 종료시 처리해야 할 코드들..
//=============================================================================
void CView_MainCtrl::FinalExitProgress()
{
	// 검사 불가 상태로 변경
	m_bFlag_ReadyTest = FALSE;
	FinalExitBoardProgress();

	TRACE(_T("Set Exit Program External Event\n"));
	m_bExitFlag = TRUE;

	if (FALSE == SetEvent(m_hEvent_ProgramExit))
	{
		TRACE(_T("Set Exit Program External Event Fail!!\n"));
	}

	m_wndDeviceView.SetDeleteTimer();
	//m_wndMotorView.SetDeleteTimer();

	OnShowSplashScreen(TRUE, _T("Program Exit..."));

	// 주변 장치 연결 해제
	DisconnectDevicez();

	//m_wndSplash.SetText(_T("-- Exit --"));
	OnShowSplashScreen(TRUE, _T("-- Exit --"));
	Sleep(300);
	ShowSplashScreen(FALSE);
	TRACE(_T("- End ExitProgramCtrl -\n"));
}

//=============================================================================
// Method		: FinalExitBoardProgress
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/7 - 10:26
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::FinalExitBoardProgress()
{
	// 카메라 OFF
	m_TIProcessing.CameraPowerOnOff(OFF);

	// 비전 조명 OFF
	//m_Device.IF_Illumination.Send_SetOnOff(0, OFF);

	// 경광등 
	m_Device.MotionSequence.TowerLamp(TowerLampYellow, OFF);
	m_Device.MotionSequence.ChartOnoff(OFF);


	return TRUE;
}

//=============================================================================
// Method		: ManualBarcode
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/10 - 13:28
// Desc.		:
//=============================================================================
void CView_MainCtrl::ManualBarcode()
{
	CDlg_Barcode	dlgBarcode;

	dlgBarcode.SetBarcodeCount(USE_CHANNEL_CNT);
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		dlgBarcode.SetUsableSite(nIdx, m_stInspInfo.bTestEnable[nIdx]);
	}
	
	if (IDOK == dlgBarcode.DoModal())
	{
		dlgBarcode.GetBarcode(m_stInspInfo.szBarcodeBuf);

		OnSetInsertBarcode(m_stInspInfo.szBarcodeBuf);
	}
}

//=============================================================================
// Method		: PermissionView
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 16:17
// Desc.		:
//=============================================================================
void CView_MainCtrl::PermissionView()
{
	if (m_stInspInfo.LotInfo.bLotStatus == TRUE)
	{
		AfxMessageBox(_T("Please try after disable LOT MODE."));
		return;
	}

	AfxGetApp()->GetMainWnd()->EnableWindow(FALSE);
	m_wndAccessMode.MoveWindow(0, 0, 450, 200);
	m_wndAccessMode.CenterWindow();
	m_wndAccessMode.ShowWindow(SW_SHOW);
	m_wndAccessMode.EnableWindow(TRUE);
}

//=============================================================================
// Method		: WarringView
// Access		: public  
// Returns		: BOOL
// Parameter	: __in CString szText
// Qualifier	:
// Last Update	: 2017/7/12 - 10:56
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::MessageView(__in CString szText, __in BOOL bMode /*= FALSE*/, __in BOOL bFixCheckMode /*= FALSE*/)
{
	CWnd_MessageView*	m_pwndMessageView;
	m_pwndMessageView = new CWnd_MessageView;

	BOOL bResult = FALSE;

	AfxGetApp()->GetMainWnd()->EnableWindow(FALSE);

	m_pwndMessageView->CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 10)), _T("Message Mode"), WS_POPUPWINDOW | WS_SIZEBOX | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL);
	m_pwndMessageView->EnableWindow(TRUE);
	m_pwndMessageView->CenterWindow();
	m_pwndMessageView->SetWarringMessage(szText);

	//  [1/28/2019 ysJang] 주소값을 참조해서 변수 변화를 바로 감지하도록..
	m_pwndMessageView->m_pbFixStatus = &m_bFixStatus; 
	m_bMessageViewMode = TRUE;
	if (m_pwndMessageView->DoModal(bMode, bFixCheckMode) == TRUE)
		bResult = TRUE;
	else
		bResult = FALSE;

	AfxGetApp()->GetMainWnd()->EnableWindow(TRUE);
	m_bMessageViewMode = FALSE;

	delete m_pwndMessageView;

	return bResult;
}

//=============================================================================
// Method		: IndicatorZero
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nAxis
// Qualifier	:
// Last Update	: 2017/7/7 - 10:27
// Desc.		:
//=============================================================================
void CView_MainCtrl::IndicatorZero(__in UINT nAxis)
{
	m_bFlag_Indicator[nAxis] = FALSE;

	Sleep(300);
	m_Device.Indicator[nAxis].Send_SetZero();

	DoEvents(300);
	m_bFlag_Indicator[nAxis] = TRUE;
}

//=============================================================================
// Method		: PermissionStatsView
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 17:22
// Desc.		:
//=============================================================================
void CView_MainCtrl::PermissionStatsView()
{
	__super::PermissionStatsView();

	enPermissionMode	AcessMode = m_wndAccessMode.GetAcessMode();

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_PermissionMode(AcessMode);
	
	if (AcessMode == Permission_Administrator || AcessMode == Permission_Engineer)
		((CPane_CommStatus*)m_pwndCommPane)->SetIndicatorButtonEnable(TRUE);
	else
		((CPane_CommStatus*)m_pwndCommPane)->SetIndicatorButtonEnable(FALSE);

	m_wndRecipeView.SetStatusEngineerMode(AcessMode);
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/12/16 - 9:54
// Desc.		:
//=============================================================================
void CView_MainCtrl::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	__super::SetPermissionMode(nAcessMode);

	m_wndMainView.SetInspectionMode(nAcessMode);
}

// void CView_MainCtrl::ManualAutoMode(__in BOOL bMode)
// {
// 	__super::ManualAutoMode(bMode);
// 
// 	m_wndMainView.ManualAutoMode(bMode);
// }

void CView_MainCtrl::ManualBtnEnable(__in BOOL bMode)
{
	__super::ManualBtnEnable(bMode);

	m_wndMainView.ManualBtnEnable(bMode);
}

//=============================================================================
// Method		: IsRecipeTest
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/13 - 17:36
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::IsRecipeTest()
{
	return m_wndRecipeView.IsRecipeTest();
}

//=============================================================================
// Method		: CView_MainCtrl::Test_Process
// Access		: public 
// Returns		: void
// Parameter	: UINT nTestNo
// Qualifier	:
// Last Update	: 2014/7/10 - 9:54
// Desc.		:
//=============================================================================
void CView_MainCtrl::Test_Process( UINT nTestNo )
{

// 	m_stInspInfo.ModelInfo.stCurrent.stCurrentData.nResult		 = TR_Pass;
// 
// 
// 	m_stInspInfo.ModelInfo.stRotate.stRotateData.dbDegree = 0.4;
// 	m_stInspInfo.ModelInfo.stRotate.stRotateData.nResult  = TR_Pass;
// 
// 	for (UINT nIdx = 0; nIdx < 30; nIdx++)
// 	{
// 		m_stInspInfo.ModelInfo.stSFR.stSFRData.dbResultValue[nIdx] = 200 + nIdx;
// 		m_stInspInfo.ModelInfo.stSFR.stSFRData.nEachResult[nIdx]	= TR_Pass;
// 	}
// 
// 	m_stInspInfo.ModelInfo.stSFR.stSFRData.nResult = TR_Pass;
// 	
// 	m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_X = 0;
// 	m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_Y = 0;
// 	m_stInspInfo.ModelInfo.stCenterPoint.stCenterPointData.nResult			= TR_Pass;
// 
// 
// 	m_stInspInfo.ModelInfo.stParticle.stParticleData.nResult = TR_Pass;

	CStringA strCheck;
	switch (nTestNo)
	{
	case 0:
	{
			  BYTE nBoard = 0;
			  float data = 5.5f;
			  m_Device.PCBCamBrd[0].Send_SetVolt(data);
	}
		break;
	case 1:
	{
			  float data = 0;
			  m_Device.PCBCamBrd[0].Send_SetVolt(data);
	}
		break;
	case 2:
		m_Device.MotionSequence.TowerLampBuzzer(DO_TowerLampBuzzer, ON);
		break;
	case 3:
		break;
	case 4:
		m_Device.Indicator[0].Send_SetZero();
		break;
	case 5:
		m_Device.Indicator[1].Send_SetZero();
		break;
	default:
		break;
	}
}

void CView_MainCtrl::OnPopupFailBoxConfirm()
{
	m_DlgFailBox.SetOwner(this);
	m_DlgFailBox.SetPtr_Device(&m_Device);

// 	m_pbFailBoxStatus = &m_bFailBoxStatus;
// 
// 	// Create timer
// 	if (NULL == m_hTimer_FailBox)
// 	{
// 		if (!CreateTimerQueueTimer(&m_hTimer_FailBox, m_hTimerQueue_FailBox, (WAITORTIMERCALLBACK)TimerRoutine_FailBox, (PVOID)this, 500, 250, WT_EXECUTEDEFAULT))
// 		{
// 			TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
// 		}
// 	}

	m_DlgFailBox.DoModal();

	// Delete timer
// 	if (NULL != m_hTimer_FailBox)
// 	{
// 		if (DeleteTimerQueueTimer(m_hTimerQueue_FailBox, m_hTimer_FailBox, NULL))
// 		{
// 			m_hTimer_FailBox = NULL;
// 		}
// 		else
// 		{
// 			TRACE(_T("DeleteTimerQueueTimer : m_hTimer_FailBox failed (%d)\n"), GetLastError());
// 		}
// 	}
}

VOID CALLBACK CView_MainCtrl::TimerRoutine_FailBox(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	CView_MainCtrl* pThis = (CView_MainCtrl*)lpParam;

	if (*pThis->m_pbFailBoxStatus == TRUE)
	{
		pThis->m_DlgFailBox.EndDialog(IDOK);
	}
}

void CView_MainCtrl::CreateTimerQueue_FailBox()
{
	m_hTimerQueue_FailBox = CreateTimerQueue();
	if (NULL == m_hTimerQueue_FailBox)
	{
		TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
		return;
	}
}

void CView_MainCtrl::DeleteTimerQueue_FailBox()
{
	if (!DeleteTimerQueue(m_hTimerQueue_FailBox))
		TRACE(_T("DeleteTimerQueue failed (%d)\n"), GetLastError());
}

BOOL CView_MainCtrl::OnPopupFailBoxConfirmVisible()
{
	return m_DlgFailBox.IsWindowVisible();
}