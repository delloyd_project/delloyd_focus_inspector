﻿//*****************************************************************************
// Filename	: 	TestManager_EQP.h
// Created	:	2016/5/9 - 13:31
// Modified	:	2016/07/22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_EQP_h__
#define TestManager_EQP_h__

#pragma once

#include "TestManager_Device.h"
#include "TI_Processing.h"

#include "Reg_InspInfo.h"
#include "File_Report.h"

#include "TI_PicControl.h"
#include "Dlg_ErrView.h"

//-----------------------------------------------------------------------------
// CTestManager_EQP
//-----------------------------------------------------------------------------
class CTestManager_EQP : public CTestManager_Device
{
public:
	CTestManager_EQP();
	virtual ~CTestManager_EQP();
	
protected:

	//-----------------------------------------------------
	// 옵션
	//-----------------------------------------------------
	// 환경설정 데이터 불러오기
	virtual BOOL	OnLoadOption					();	
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);
	//virtual void	OnInitUISetting					(__in HWND hWndOwner = NULL);

	//-----------------------------------------------------
	// 쓰레드 작업 처리
	//-----------------------------------------------------
	typedef struct
	{
		LPVOID		pOwner;
		UINT		nIndex;		// Socket Index
		UINT		nArg_1;		// Test Item
		UINT		nArg_2;		// 
	}stThreadParam;

	// Start 신호에 의해 구동되는 쓰레드
	HANDLE			m_hThrTest_Unit[MAX_OPERATION_THREAD];
	HANDLE			m_hThrTest_All;

	// 자동화 처리용 함수	
	BOOL			StartOperation_Auto				(__in UINT nUnitIdx, __in UINT nTestItemID);
	BOOL			StartOperation_AutoAll			();	
	// 수동 모드일때 자동화 처리용 함수
	void			StartOperation_Manual			(__in UINT nUnitIdx);

	// 검사 쓰레드
	static UINT WINAPI	ThreadTest_Unit				(__in LPVOID lParam);
	static UINT WINAPI	ThreadZone_All				(__in LPVOID lParam);

	// 쓰레드 내에서 처리될 자동 작업 함수
	BOOL			AutomaticProcess				(__in UINT nUnitIdx, __in UINT nTestItemID);
	void			AutomaticProcess_All			();
	// 자동으로 처리되고 있는 쓰레드 강제 종료
	void			StopProcess						(__in UINT nUnitIdx);
	void			StopProcess_All					();	

	// 전체 테스트 초기 세팅
	virtual void	OnInitialTest_All				();
	// 전체 테스트 종료 세팅
	virtual void OnFinallyTest_All                  (__in enTestProcess TestProc, BOOL bMotionNoMove = FALSE);
	// 개별 테스트 초기 세팅
	virtual void	OnInitialTest_Unit				(__in UINT nUnitIdx);
	// 개별 테스트 종료 세팅
	virtual void	OnFinallyTest_Unit				(__in UINT nUnitIdx, __in BOOL bResult);

	// 전체 검사 작업 시작
	virtual LRESULT	OnStartTest_All					();
	virtual LRESULT	OnStartTest_All_TestItem		(__in UINT nUnitIdx, __in UINT nTestItemID);
	// 개별 검사 작업 시작
	virtual LRESULT	OnStartTest_Unit				(__in UINT nUnitIdx, __in UINT nTestItemID);
	virtual LRESULT	OnStartTest_Unit_TestItem		(__in UINT nTestItemID);
	// 검사 결과에 따른 메세지 팝업
	virtual void	OnPopupMessageTest_All			(__in enResultCode_AF nResultCode);

	//-----------------------------------------------------
	// 테스트
	//-----------------------------------------------------
	virtual BOOL	OnCheckDeviceComm				(){return TRUE;};
	// 기구물 안전 체크
	virtual BOOL	OnCheckSaftyJIG					();	
	// 전체 채널의 카메라 정보 업데이트
	virtual void	OnUpdateCamInfo_All				(){};
	// 제품 결과 판정 및 리포트 처리
	virtual void	OnJugdement_And_Report			();
	// Cycle Time Check
	virtual void	OnSetCycleTime					();
	// 카메라 데이터 초기화
	virtual void	OnResetCamInfo					();
	//-----------------------------------------------------	

	// 모델 설정 데이터가 정상인가 판단
	BOOL			CheckModelInfo					();	
	BOOL			CheckLOTInfo					();
	BOOL			CheckMasterSetInfo				();
	BOOL			CheckBarCodeInfo				();
	BOOL			CheckEASCount					();

	// 타이머 ---------------------------------------------
	virtual void	CreateTimer_UpdateUI			();
	virtual void	OnMonitor_TimeCheck				();
	virtual void	OnMonitor_UpdateUI				();
	DWORD			m_dwTimeCheck;	// 시간 체크용 임시 변수
	
	virtual	BOOL	EVMSPathSearch					(__out CString &szEVNPath){return TRUE;};

	//-----------------------------------------------------
	// Digital I/O
	//-----------------------------------------------------
	// I/O 신호 감지 및 신호별 기능 처리
	virtual void	OnDetectDigitalInSignal			(__in BYTE byBitOffset, __in BOOL bOnOff);
	// I/O 신호 초기화
	virtual void	OnInitDigitalIOSignal			();
	virtual void	OnFunc_IO_EMO					();
	virtual void	OnFunc_IO_ErrorReset			();
	virtual void	OnFunc_IO_Init					();
	virtual void	OnFunc_IO_Door					();
	virtual void	OnFunc_IO_MainPower				();
	virtual void	OnFunc_IO_Stop					();
	virtual void	OnFunc_IO_AirCheck				();

	virtual void	OnAddErrorInfo					(__in enErrorCode lErrorCode);
	virtual void	OnSetInsertBarcode				(__in LPCTSTR szBarcode){};

	virtual void	OnPopupFailBoxConfirm			(){};

	virtual BOOL	OnPopupFailBoxConfirmVisible	() { return TRUE; };

	//-----------------------------------------------------
	// UI 업데이트
	//-----------------------------------------------------
	//virtual void	OnUpdateElapsedTime_Unit		(__in UINT nUnitIdx);
	//virtual void	OnUpdateElapsedTime_UnitAtOnce	(){};
	virtual void	OnUpdateElapsedTime_All			(__in DWORD dwTime){};
	virtual void	OnTestProgress					(__in enTestProcess nProcess);
	//virtual void	OnTestProgress_Unit				(__in UINT nUnitIdx, __in enTestProcess nProcess);
	//virtual void	OnTestResult_Unit				(__in UINT nUnitIdx, __in enTestResult nResult);
	virtual void	OnTestFailResultCode_Unit		(__in UINT nUnitIdx, __in enResultCode_TestItem nFailCode);
	virtual void	OnTestResult					(__in enTestResult nResult);
	virtual void	OnTestErrCode					(__in LPCTSTR szErrorCode){};
	virtual void	OnSetInputTime					();
	virtual void	OnSetResetSiteInfo				(){};
	virtual void	OnSetTestInitialize				(__in enTestEachResult EachResult, __in CString strText	){};	// 검사 초기화
	virtual void	OnSetTestFinalize				(__in enTestEachResult EachResult, __in CString strText	){};	// 검사 마무리
	
	virtual void	OnSetTestCurrent				(__in enTestEachResult EachResult, __in CString strText	){};	// 소비전류 검사
	virtual void	OnSetTestCenterAdjust			(__in enTestEachResult EachResult, __in CString strText	){};	// 광축조정 검사
	virtual void	OnSetTestActiveAlgin			(__in enTestEachResult EachResult, __in CString strText	){};	// 광축조정
	virtual void	OnSetTestFocus					(__in enTestEachResult EachResult, __in CString strText	){};	// 
	virtual void	OnSetTestResolution				(__in enTestEachResult EachResult, __in CString strText	){};	// 해상력 검사
	virtual void	OnSetTestRotation				(__in enTestEachResult EachResult, __in CString strText	){};	// Rotation 검사
	virtual void	OnSetTestParticle				(__in enTestEachResult EachResult, __in CString strText	){};	// 이물 검사
	virtual void	OnSetResult						(__in enTestResult Result){};									// JudgeMent UI Set
	virtual void	OnSetResult						(__in enTestResult Result, __in CString strText	){};			//
	virtual void	OnSetLotInfo					(){};
	virtual void	OnSetMasterOffSetData			(__in int iOffsetX, __in int iOffsetY){};

	virtual void	OnSetMaterSetView				(__in BOOL bEnable){};

	//Indicator 데이터 
	virtual void	OnSetIndicatorData				(__in ST_ModelInfo &stModelInfo, __in float fAxisX,	__in float fAxisY){};

	// 팝업된 다이얼로그 닫기
	virtual void	OnHidePopupUI					(){};

	// Worklist 처리
	virtual void	OnInsertWorklist				(){};
	virtual void	OnSaveWorklist					(){};
	virtual void	OnLoadWorklist					(){};

	BOOL			m_bMessageViewMode = FALSE;
	virtual BOOL	MessageView						(__in CString szText, __in BOOL bMode = FALSE, __in BOOL bFixCheckMode = FALSE)	{ return TRUE; };

	// 수율 
	virtual void	OnUpdateYield					(){};

	// 포고 카운트 처리
	BOOL			IncreasePogoCount				();
	BOOL			SavePogoCount					();
	BOOL			LoadPogoCount					();
	BOOL			CheckPogoCount					();
	virtual void	OnSetStatus_PogoCount			(){};

	// Driver Count 처리
	BOOL			IncreaseDriverCount				();
	BOOL			SaveDriverCount					();
	BOOL			LoadDriverCount					();
	BOOL			CheckDriverCount				();
	virtual void	OnSetStatus_DriverCount			(){};

	virtual BOOL	OnChangeLotInfo					(){return TRUE;};
	virtual BOOL	OnChangeLotInfo(__in enModelStatus LotStatus)	{ return TRUE; };

	// TestInfo UI Button Change
	virtual void	OnChangeBtnState					(__in BOOL bEnable){};
	virtual void	OnChangeStartBtnState				(__in BOOL bEnable){};

	ST_ImageMode		m_stImageMode;
	ST_Worklist			m_WorklistPtr;

	// 검사에 관련된 모든 정보가 들어있는 구조체 
	ST_InspectionInfo	m_stInspInfo;

	// 레지스트리 데이터 처리용 클래스
	CReg_InspInfo		m_regInspInfo;

	// 결과 데이터 처리용 클래스
	CFile_Report		m_FileReport;

	// 검사 시작 조건이 될때까지 검사 시작 방지
	BOOL				m_bFlag_ReadyTest;

	// 마스터 SET 확인
	BOOL				m_bFlag_MasterSet;

	CTI_PicControl		m_TIPicControl;
	CTI_Processing		m_TIProcessing;

	BOOL				m_bTestFinishMode;

	BOOL				m_bFlag_InitialSwitch;

	BOOL				m_bFlag_EtcControlSwitch[12];
	BOOL				m_bFlag_Indicator[Indicator_Max];

	BOOL				m_bFlag_AutoFocus;
	BOOL				m_bFlag_FocusTest;
	BOOL				m_bFlag_ActiveAlginTest;
	BOOL				m_bFlag_UseStop;
	BOOL				m_bFlag_TestItem[TIID_MaxEnum];

	BOOL				m_bFlag_Button[DI_NotUseBit_Max];

	float				m_fValue[Indicator_Max];

	BOOL				m_bFixStatus;
	BOOL				m_bFailBoxStatus;

public:
	ST_PicImageCaptureMode m_stPicImageCaptureMode;

	// 생성자 처리용 코드
	virtual void		OnInitialize				();
	// 소멸자 처리용 코드
	virtual	void		OnFinalize					();

	virtual void		DisplayVideo_NoSignal		(__in UINT nChIdx){};

	//-----------------------------------------------------
	// 검사 상태 확인용 함수
	//-----------------------------------------------------
	// 개별, 전체 검사가 수행 중인지 확인
	BOOL				IsTesting					();
	// 전체 검사가 수행 중인지 확인
	BOOL				IsTesting_All				();
	// 개별 검사가 수행 중인지 확인
	BOOL				IsTesting_Unit				(__in UINT nUnitIdx);
	// 입력된 Unit을 제외한 검사가 수행 중인지 확인
	BOOL				IsTesting_Exc				(__in UINT nExcUnitIdx);

	// 관리자 모드 확인 (구버전)
	BOOL				IsManagerMode				();
	// 제어 권한 상태 구하기
	enPermissionMode	GetPermissionMode			();
	// 제어 권한 상태 설정
	virtual void		SetPermissionMode			(__in enPermissionMode nAcessMode);

	virtual void		ManualAutoMode				(__in BOOL bMode){};
	
	virtual void		ManualBtnEnable				(__in BOOL bMode){};

	virtual void		PermissionStatsView			();

	void				OnChangWorklistData			();

	BOOL				SetAutoMasterMode			();

	// Loading / Unloading
	BOOL				CameraTestInitialize		(BOOL bMode);

	// 최종 결과 데이터
	BOOL				SaveFinalMesSystem			();
	BOOL				SaveFileMesSystem_Mcnex();

	// 전류
	BOOL				CameraTestCurrent			();
	BOOL				SaveCurrentMesSystem		();

	// 해상력, 광축
	BOOL				CameraTestActiveAlign		();

	BOOL				SaveEIAJMesSystem			();
	BOOL				SaveSFRMesSystem			();
	BOOL				SaveCenterPointMesSystem	();

	void				GetSaveImageFilePath		(__out CString& szOutPath);

	LRESULT MoveStage(BOOL bSet);
	//LED
	BOOL				CameraTestLED				();


	// Auto Focusing Process
// 	HANDLE				m_hThrAutoFocusing;
// 	BOOL				StartAutoFocusingThr		();
// 	static UINT WINAPI	StartAutoFocusing			(__in LPVOID lParam);
// 	void				EndAutoFocusingThr			();
// 	void				StartAutoFocusingProcess	();

	void				ImageSave					(UINT nTestItemID);

	void				CreateTimer_UpdateIndicatorDisplay();
	void				DeleteTimer_UpdateIndicatorDisplay();
	static VOID CALLBACK TimerRoutine_UpdateIndicator(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);
	void            OnMonitor_UpdateIndicator();
	void            CreateTimerQueue_Mon_Indicator();
	void            DeleteTimerQueue_Mon_Indicator();
	HANDLE			m_hTimer_Update_Indicator;
	HANDLE			m_hTimerQueue_Indicator;
	
	BOOL            m_bBusyIndicator;
};

#endif // TestManager_EQP_h__

