﻿//*****************************************************************************
// Filename	: 	Wnd_MotionCtr.h
// Created	:	2017/03/28 - 13:47
// Modified	:	2017/03/28 - 13:47
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************

#pragma once
#include "VGStatic.h"
#include "VGGroupWnd.h"
#include "resource.h"
#include "Def_Motion.h"
#include "Def_TestDevice.h"

#define	 MC_WIDTH_OFFSET 1.0

typedef enum MotionCtr_ID
{
	IDC_MC_ED_JOG_SPEED	= 1000,
	IDC_MC_ED_STEP_POS,
	IDC_MC_ED_REPEAT_COUNT,
	IDC_MC_ED_REPEAT_READ_COUNT,
	IDC_MC_ED_REPEAT_DELAY,
	IDC_MC_ED_REPEAT_POINT1,
	IDC_MC_ED_REPEAT_POINT2,
	IDC_MC_ED_STEP_SEMPLE_POS,
	IDC_MC_BN_JOG_MINUS,
	IDC_MC_BN_JOG_PLUS,
	IDC_MC_BN_ABS_MOVE,
	IDC_MC_BN_REL_MOVE,
	IDC_MC_BN_REPEAT_MOVE,
	IDC_MC_BN_REPEAT_STOP,
	IDC_MC_BN_MANUAL_ESTOP,
	IDC_MC_BN_MANUAL_SSTOP,
	IDC_MC_BN_MANUAL_CLEAR,
	IDC_MC_MC_MAXNUM,
};

enum enMotionCtrTitleStatic
{
	ST_MC_JOG_NAME = 0,
	ST_MC_STEP_NAME,
	ST_MC_REPEAT_NAME,
	ST_MC_MANUAL_CTR,
	ST_MC_JOG_SPEED,
	ST_MC_STEP_POS,
	ST_MC_REPEAT_CNT,
	ST_MC_REPEAT_DLY,
	ST_MC_REPEAT_1P,
	ST_MC_REPEAT_2P,
	ST_MC_REPEAT_READ_CNT,
	ST_MC_MAXNUM,
};

static LPCTSTR g_szMotionCtrTitleName[] =
{
	_T("MOTOR JOG CONTROL"),
	_T("MOTOR STEP CONTROL"),
	_T("MOTOR REPEAT CONTROL"),
	_T("MOTOR MANUAL BUTTON"),
	_T("MOTOR JOG SPEED"),
	_T("MOTOR STEP POS"),
	_T("REPEAT COUNT"),
	_T("REPEAT DELAY [ms]"),
	_T("REPEAT START POINT"),
	_T("REPEAT END POINT"),
	_T("0"),
	_T("0"),
	NULL
};

enum enMotionCtrEditItem
{
	ED_MC_JOG_SPEED = 0,
	ED_MC_STEP_POS,
	ED_MC_REPEAT_CNT,
	ED_MC_REPEAT_DLY,
	ED_MC_REPEAT_P1,
	ED_MC_REPEAT_P2,
	ED_MC_MAXNUM,
};

enum enMotionCtrButton
{
	BN_MC_MINUS_MOVE = 0,
	BN_MC_PLUS_MOVE,
	BN_MC_ABS_MOVE,
	BN_MC_REL_MOVE,
	BN_MC_REPEAT_START,
	BN_MC_REPEAT_STOP,
	BN_MC_MANUAL_ESTOP,
	BN_MC_MANUAL_SSTOP,
	BN_MC_MANUAL_CLEAR,
	BN_MC_MAXNUM,
};

static LPCTSTR g_szMotionCtrButton[] =
{
	_T("<ㅡ"),
	_T("ㅡ>"),
	_T("ABS MOVE"),
	_T("REL MOVE"),
	_T("REPEAT START"),
	_T("REPEAT STOP"),
	_T("ESTOP"),
	_T("SSTOP"),
	_T("MOVE"),
	NULL
};

// CWnd_MotionCtr
class CWnd_MotionCtr : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MotionCtr)

public:
	CWnd_MotionCtr();
	virtual ~CWnd_MotionCtr();

	ST_Device*			m_pstDevice;

	void SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnAbsMove	();
	afx_msg void	OnBnClickedBnRelMove	();
	afx_msg void	OnBnClickedBnRepeatStart();
	afx_msg void	OnBnClickedBnClear		();

	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	void	SetSelectAxis					(UINT nAxis);
	void	SetResultMsg					(CString str);

	DECLARE_MESSAGE_MAP()

protected:

	CFont			m_font;
	UINT			m_nAxisNum;
	BOOL			m_bRepeatStop;

	BOOL			m_bRepeatStop_Flag;
	BOOL			m_bEStop_Flag;
	BOOL			m_bSStop_Flag;


	CVGGroupWnd		m_Group_Nmae;

	CVGStatic		m_st_TitleName[ST_MC_MAXNUM];

	CMFCButton		m_bn_Item[BN_MC_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[ED_MC_MAXNUM];

};