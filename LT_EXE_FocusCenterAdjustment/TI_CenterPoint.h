﻿#pragma once

#include "TestItem.h"
#include "Def_TestDevice.h"
#include "cv.h"
#include "highgui.h"

class CTI_CenterPoint : public CTestItem
{
public:
	CTI_CenterPoint();
	virtual ~CTI_CenterPoint();

	BOOL	SetImageSize			(DWORD dwWidth, DWORD dwHeight);

	/*광축 측정*/
	UINT	CenterPoint_Test		(ST_LT_TI_CenterPoint  *pstCenterPoint, LPBYTE pImageBuf, UINT nQuadRectPos);

	/*측정알고리즘*/
	CvPoint SearchCenterPoint		(__in const ST_CenterPoint_Op *pstCenterPointOp, LPBYTE IN_RGB);
	CvPoint SearchQuadRectPoint		(__in const ST_CenterPoint_Op *pstCenterPointOp, LPBYTE IN_RGB);

	double	GetDistance				(int ix1, int iy1, int ix2, int iy2);
	CvPoint Cam_State_Offset		(int iCamState, CvPoint CenterPoint);

protected:

	UINT m_nWidth;
	UINT m_nHeight;
};

