﻿#include "stdafx.h"
#include "File_Model.h"
#include "Def_Enum.h"
#include "CommonFunction.h"

#define		CURRENT_OP_AppName		_T("CURRENT_OP")
#define		CENTERPOINT_OP_AppName	_T("CENTERPOINT_OP")
#define		Rotate_OP_AppName		_T("ROTATE_OP")
#define		EIAJ_OP_AppName			_T("EIAJ_OP")
#define		SFR_OP_AppName			_T("SFR_OP")
#define		LIGHT_OP_AppName		_T("LIGHT_OP")
#define		PARTICLE_OP_AppName		_T("PARTICLE_OP")
#define		VISION_OP_AppName		_T("VISION_OP")
#define		AF_OP_AppName			_T("AF_OP")
#define		CustomTeach_OP_AppName	_T("CUSTOMTEACH_OP")

CFile_Model::CFile_Model()
{
}

CFile_Model::~CFile_Model()
{
}

//=============================================================================
// Method		: LoadModelFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 11:18
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadModelFile(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	// 파일이 존재하는가?
	if (!PathFileExists(szPath))
	{
		return FALSE;
	}

	TCHAR   inBuff[1024] = { 0, };
	CString	strKey;

	BOOL bReturn = TRUE;

	bReturn = Load_Common(szPath, stModelInfo);

//	bReturn &= LoadAFInfoFile(szPath, stModelInfo.stAFInfo);
//	bReturn &= LoadCustomTeachFile(szPath, stModelInfo.stCustomTeach);
	bReturn &= LoadCurrentOpFile(szPath, stModelInfo.stCurrent);

	bReturn &= LoadCenterPointOpFile(szPath, stModelInfo.stCenterPoint);
//	bReturn &= LoadRotateFile(szPath, stModelInfo.stRotate);
//	bReturn &= LoadParticleOpFile(szPath, stModelInfo.stParticle);
	bReturn &= LoadEIAJFile(szPath, stModelInfo.stEIAJ);
//	bReturn &= LoadSFRFile(szPath, stModelInfo.stSFR);
	
	return TRUE;
}

//=============================================================================
// Method		: SaveModelFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 11:26
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveModelFile(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstModelInfo)
		return FALSE;

	::DeleteFile(szPath);

	CString strValue;
	CString strKey;

	BOOL bReturn = TRUE;

	bReturn  = Save_Common(szPath, pstModelInfo);

//	bReturn &= SaveAFInfoFile(szPath, &pstModelInfo->stAFInfo);
//	bReturn &= SaveCustomTeachFile(szPath, &pstModelInfo->stCustomTeach);
	bReturn &= SaveCurrentOpFile(szPath, &pstModelInfo->stCurrent);

	bReturn &= SaveCenterPointOpFile(szPath, &pstModelInfo->stCenterPoint);
//	bReturn &= SaveRotateFile(szPath, &pstModelInfo->stRotate);
//	bReturn &= SaveParticleOpFile(szPath, &pstModelInfo->stParticle);
	bReturn &= SaveEIAJFile(szPath, &pstModelInfo->stEIAJ);
//	bReturn &= SaveSFRlFile(szPath, &pstModelInfo->stSFR);

	return TRUE;
}

//=============================================================================
// Method		: Load_Default
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/3 - 15:55
// Desc.		:
//=============================================================================
BOOL CFile_Model::Load_Common(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strValue;
	CString strKeyName;

	// 모델명
	GetPrivateProfileString(Common_AppName, ModelCode_KeyName, _T("Default"), inBuff, 255, szPath);
	stModelInfo.szModelCode = inBuff;

	// 카메라 전압 
	GetPrivateProfileString(Common_AppName, _T("CameraVoltage"), _T("5.5"), inBuff, 80, szPath);
	stModelInfo.fVoltage = (FLOAT)_ttof(inBuff);

	// 카메라 안정화 시간 
	GetPrivateProfileString(Common_AppName, _T("CameraDelay"), _T("500"), inBuff, 80, szPath);
	stModelInfo.nCameraDelay = _ttoi(inBuff);

	// Bin File
	GetPrivateProfileString(Common_AppName, _T("BinFile"), _T(""), inBuff, 255, szPath);
	stModelInfo.szBinFile = inBuff;

	// Pogo 설정 파일
	GetPrivateProfileString(Common_AppName, _T("PogoFile"), _T(""), inBuff, 255, szPath);
	stModelInfo.szPogoName = inBuff;

	// DriverCount 설정 파일
	GetPrivateProfileString(Common_AppName, _T("DriverCountFile"), _T(""), inBuff, 255, szPath);
	stModelInfo.szDriverCountName = inBuff;

	// 카메라 타입
	GetPrivateProfileString(Common_AppName, _T("nGrabType"), _T("0"), inBuff, 255, szPath);
	stModelInfo.nGrabType = _ttoi(inBuff);

// 	// 카메라 Width
// 	GetPrivateProfileString(Common_AppName, _T("Width"), _T("720"), inBuff, 255, szPath);
// 	stModelInfo.dwWidth = _ttoi(inBuff);
// 
// 	// 카메라 Height
// 	GetPrivateProfileString(Common_AppName, _T("Height"), _T("480"), inBuff, 255, szPath);
// 	stModelInfo.dwHeight = _ttoi(inBuff);

	//  [1/2/2019 ysJang] nQuadRectPos
	// Cam Rect Position
	GetPrivateProfileString(Common_AppName, _T("Quad_Rect_Pos"), _T("0"), inBuff, 255, szPath);
	stModelInfo.nQuadRectPos = _ttoi(inBuff);

//  [3/4/2019 ysJang] Indicator list
	// Indicator X Min
	GetPrivateProfileString(Common_AppName, _T("Indicator_Standard_X"), _T("0.00"), inBuff, 255, szPath);
	stModelInfo.fIndicator_Std_X = (FLOAT)_ttof(inBuff);

	// Indicator X Max
	GetPrivateProfileString(Common_AppName, _T("Indicator_Standard_Y"), _T("0.00"), inBuff, 255, szPath);
	stModelInfo.fIndicator_Std_Y = (FLOAT)_ttof(inBuff);

	// Indicator Y Min
	GetPrivateProfileString(Common_AppName, _T("Indicator_Spec_X"), _T("0.00"), inBuff, 255, szPath);
	stModelInfo.fIndicator_Spec_X = (FLOAT)_ttof(inBuff);

	// Indicator Y max
	GetPrivateProfileString(Common_AppName, _T("Indicator_Spec_Y"), _T("0.00"), inBuff, 255, szPath);
	stModelInfo.fIndicator_Spec_Y = (FLOAT)_ttof(inBuff);

	// Module Fix Check
	GetPrivateProfileString(Common_AppName, _T("ModuleFixCheck"), _T("0"), inBuff, 255, szPath);
	stModelInfo.bModuleFixCheck = _ttoi(inBuff);

	// Move Stage Check
	GetPrivateProfileString(Common_AppName, _T("MoveStageCheck"), _T("0"), inBuff, 255, szPath);
	stModelInfo.bMoveStageCheck = _ttoi(inBuff);

	// 테스트 항목
	stModelInfo.TestItemz.RemoveAll();

	GetPrivateProfileString(_T("TestItem"), _T("TestCount"), _T("0"), inBuff, 80, szPath);
	INT iCnt = _ttoi(inBuff);
	UINT iTestID = 0;
	for (UINT nIdx = 0; nIdx < (UINT)iCnt; nIdx++)
	{
		strKeyName.Format(_T("Test_%02d"), nIdx);

		GetPrivateProfileString(_T("TestItem"), strKeyName, _T(""), inBuff, 80, szPath);
		iTestID = (UINT)_ttoi(inBuff);
	
		stModelInfo.TestItemz.Add(iTestID);
	}

	GetPrivateProfileString(Common_AppName, _T("MasterSpcX"), _T("5"), inBuff, 80, szPath);
	stModelInfo.nMasterSpcX = _ttoi(inBuff);

	GetPrivateProfileString(Common_AppName, _T("MasterSpcY"), _T("5"), inBuff, 80, szPath);
	stModelInfo.nMasterSpcY = _ttoi(inBuff);

	GetPrivateProfileString(Common_AppName, _T("MasterInfoX"), _T("0"), inBuff, 80, szPath);
	stModelInfo.iMasterInfoX = _ttoi(inBuff);

	GetPrivateProfileString(Common_AppName, _T("MasterInfoY"), _T("0"), inBuff, 80, szPath);
	stModelInfo.iMasterInfoY = _ttoi(inBuff);

// 	GetPrivateProfileString(Common_AppName, _T("Align"), _T("0"), inBuff, 80, szPath);
// 	stModelInfo.nAlign = _ttoi(inBuff);

	GetPrivateProfileString(Common_AppName, _T("nDistortion"), _T("1"), inBuff, 80, szPath);
	stModelInfo.nDistortion = _ttoi(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: LoadCurrentOpFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_LT_TI_Current & stCurrentInfo
// Qualifier	:
// Last Update	: 2017/2/10 - 9:58
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadCurrentOpFile(__in LPCTSTR szPath, __out ST_LT_TI_Current& stCurrentInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strValue, strApp;

	for (UINT nCh = 0; nCh < CuOp_ItemNum; nCh++)
	{
		strApp.Format(_T("Min_%d"), nCh);
		strValue.Format(_T("50"));
		GetPrivateProfileString(CURRENT_OP_AppName, strApp, strValue, inBuff, 255, szPath);
		stCurrentInfo.stCurrentOp[nCh].nMinCurrent = _ttoi(inBuff);

		strApp.Format(_T("Max_%d"), nCh);
		strValue.Format(_T("80"));
		GetPrivateProfileString(CURRENT_OP_AppName, strApp, strValue, inBuff, 255, szPath);
		stCurrentInfo.stCurrentOp[nCh].nMaxCurrent = _ttoi(inBuff);

		strApp.Format(_T("Offset_%d"), nCh);
		strValue.Format(_T("0"));
		GetPrivateProfileString(CURRENT_OP_AppName, strApp, strValue, inBuff, 255, szPath);
		stCurrentInfo.stCurrentOp[nCh].nOffset = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_Default
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2017/1/3 - 15:55
// Desc.		:
//=============================================================================
BOOL CFile_Model::Save_Common(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strKeyName;

	// Version, Format
	strValue.Format(_T("%s %s"), SYS_CUSTOMER, g_szInsptrSysType[SET_INSPECTOR]);
	WritePrivateProfileString(_T("VersionInfo"), _T("Equipment"), strValue, szPath);
	strValue.Format(_T("%s (%s)"), GetVersionInfo(_T("ProductVersion")), GetVersionInfo(_T("FileVersion")));
	WritePrivateProfileString(_T("VersionInfo"), _T("SWVersion"), strValue, szPath);

	// 모델명
	strValue = pstModelInfo->szModelCode;
	WritePrivateProfileString(Common_AppName, ModelCode_KeyName, strValue, szPath);

	// 카메라 전압 
	strValue.Format(_T("%6.2f"), pstModelInfo->fVoltage);
	WritePrivateProfileString(Common_AppName, _T("CameraVoltage"), strValue, szPath);
	
	// 카메라 안정화 시간 
	strValue.Format(_T("%d"), pstModelInfo->nCameraDelay);
	WritePrivateProfileString(Common_AppName, _T("CameraDelay"), strValue, szPath);

	// Bin File
	strValue = pstModelInfo->szBinFile;
	WritePrivateProfileString(Common_AppName, _T("BinFile"), strValue, szPath);

	// Pogo 설정 파일
	strValue = pstModelInfo->szPogoName;
	WritePrivateProfileString(Common_AppName, _T("PogoFile"), strValue, szPath);

	// DriverCount 설정 파일
	strValue = pstModelInfo->szDriverCountName;
	WritePrivateProfileString(Common_AppName, _T("DriverCountFile"), strValue, szPath);

	// 카메라 타입
	strValue.Format(_T("%d"), pstModelInfo->nGrabType);
	WritePrivateProfileString(Common_AppName, _T("nGrabType"), strValue, szPath);

// 	// 카메라 Width
// 	strValue.Format(_T("%d"), pstModelInfo->dwWidth);
// 	WritePrivateProfileString(Common_AppName, _T("Width"), strValue, szPath);
// 
// 	// 카메라 Height
// 	strValue.Format(_T("%d"), pstModelInfo->dwHeight);
// 	WritePrivateProfileString(Common_AppName, _T("Height"), strValue, szPath);

	// Quad_Rect_Pos
	strValue.Format(_T("%d"), pstModelInfo->nQuadRectPos);
	WritePrivateProfileString(Common_AppName, _T("Quad_Rect_Pos"), strValue, szPath);

 	// Indicator X Min
	strValue.Format(_T("%.2f"), pstModelInfo->fIndicator_Std_X);
	WritePrivateProfileString(Common_AppName, _T("Indicator_Standard_X"), strValue, szPath);

	// Indicator X Max
	strValue.Format(_T("%.2f"), pstModelInfo->fIndicator_Std_Y);
	WritePrivateProfileString(Common_AppName, _T("Indicator_Standard_Y"), strValue, szPath);

	// Indicator Y Min
	strValue.Format(_T("%.2f"), pstModelInfo->fIndicator_Spec_X);
	WritePrivateProfileString(Common_AppName, _T("Indicator_Spec_X"), strValue, szPath);

	// Indicator Y Max
	strValue.Format(_T("%.2f"), pstModelInfo->fIndicator_Spec_Y);
	WritePrivateProfileString(Common_AppName, _T("Indicator_Spec_Y"), strValue, szPath);

	// Module Fix Check
	strValue.Format(_T("%d"), pstModelInfo->bModuleFixCheck);
	WritePrivateProfileString(Common_AppName, _T("ModuleFixCheck"), strValue, szPath);

	// Move Stage Check
	strValue.Format(_T("%d"), pstModelInfo->bMoveStageCheck);
	WritePrivateProfileString(Common_AppName, _T("MoveStageCheck"), strValue, szPath);

	// 테스트 항목
	INT_PTR iCnt = pstModelInfo->TestItemz.GetCount();

	strValue.Format(_T("%d"), iCnt);
	WritePrivateProfileString(_T("TestItem"), _T("TestCount"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < (UINT)iCnt; nIdx++)
	{
		strKeyName.Format(_T("Test_%02d"), nIdx);
		strValue.Format(_T("%d"), pstModelInfo->TestItemz.GetAt(nIdx));

		WritePrivateProfileString(_T("TestItem"), strKeyName, strValue, szPath);
	}

	strValue.Format(_T("%d"), pstModelInfo->nMasterSpcX);
	WritePrivateProfileString(Common_AppName, _T("MasterSpcX"), strValue, szPath);

	strValue.Format(_T("%d"), pstModelInfo->nMasterSpcY);
	WritePrivateProfileString(Common_AppName, _T("MasterSpcY"), strValue, szPath);

	strValue.Format(_T("%d"), pstModelInfo->iMasterInfoX);
	WritePrivateProfileString(Common_AppName, _T("MasterInfoX"), strValue, szPath);

	strValue.Format(_T("%d"), pstModelInfo->iMasterInfoY);
	WritePrivateProfileString(Common_AppName, _T("MasterInfoY"), strValue, szPath);

// 	strValue.Format(_T("%d"), pstModelInfo->nAlign);
// 	WritePrivateProfileString(Common_AppName, _T("Align"), strValue, szPath);

	strValue.Format(_T("%d"), pstModelInfo->nDistortion);
	WritePrivateProfileString(Common_AppName, _T("nDistortion"), strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: SaveCurrentOpFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_LT_TI_Current * pstCurrentInfo
// Qualifier	:
// Last Update	: 2017/2/10 - 10:00
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveCurrentOpFile(__in LPCTSTR szPath, __in const ST_LT_TI_Current* pstCurrentInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstCurrentInfo)
		return FALSE;

	CString strValue, strApp;

	for (UINT nCh = 0; nCh < CuOp_ItemNum; nCh++)
	{
		strApp.Format(_T("Min_%d"), nCh);
		strValue.Format(_T("%d"), pstCurrentInfo->stCurrentOp[nCh].nMinCurrent);
		WritePrivateProfileString(CURRENT_OP_AppName, strApp, strValue, szPath);

		strApp.Format(_T("Max_%d"), nCh);
		strValue.Format(_T("%d"), pstCurrentInfo->stCurrentOp[nCh].nMaxCurrent);
		WritePrivateProfileString(CURRENT_OP_AppName, strApp, strValue, szPath);

		strApp.Format(_T("Offset_%d"), nCh);
		strValue.Format(_T("%d"), pstCurrentInfo->stCurrentOp[nCh].nOffset);
		WritePrivateProfileString(CURRENT_OP_AppName, strApp, strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: LoadCenterPointOpFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_LT_TI_CenterPoint & stCenterPointInfo
// Qualifier	:
// Last Update	: 2017/2/10 - 10:37
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadCenterPointOpFile(__in LPCTSTR szPath, __out ST_LT_TI_CenterPoint& stCenterPointInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strValue, strApp;


	strValue.Format(_T("0"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("TEST_MODE"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.nTestMode = _ttoi(inBuff);

	strValue.Format(_T("0"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("CAM_STATE"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.nCameraState = _ttoi(inBuff);

	strValue.Format(_T("2"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("WRITECNT"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.nWriteCnt = _ttoi(inBuff);

	strValue.Format(_T("%d"), CAM_IMAGE_WIDTH_HALF);
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("Standard_X"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.nStandard_X = _ttoi(inBuff);

	strValue.Format(_T("%d"), CAM_IMAGE_HEIGHT_HALF);
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("Standard_Y"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.nStandard_Y = _ttoi(inBuff);

	strValue.Format(_T("3"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("Standard_OffsetX"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.nStandard_OffsetX = _ttoi(inBuff);

	strValue.Format(_T("3"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("Standard_OffsetY"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.nStandard_OffsetY = _ttoi(inBuff);

	strValue.Format(_T("1"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("Target_OffsetX"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.nTarget_OffsetX = _ttoi(inBuff);

	strValue.Format(_T("1"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("Target_OffsetY"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.nTarget_OffsetY = _ttoi(inBuff);

	strValue.Format(_T("1.0"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("Applied_RatioX"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.dApplied_RatioX = _ttof(inBuff);

	strValue.Format(_T("1.0"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("Applied_RatioY"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.dApplied_RatioY = _ttof(inBuff);

	strValue.Format(_T("20"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("Max_PixX"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.nMax_PixX = _ttoi(inBuff);

	strValue.Format(_T("20"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("Max_PixY"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.nMax_PixY = _ttoi(inBuff);

	strValue.Format(_T("1.0"));
	GetPrivateProfileString(CENTERPOINT_OP_AppName, _T("ImageSensorPix"), strValue, inBuff, 255, szPath);
	stCenterPointInfo.stCenterPointOp.dbImageSensorPix = _ttof(inBuff);
	

	return TRUE;
}

//=============================================================================
// Method		: SaveRotateFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_LT_TI_Rotate * pstRotateInfo
// Qualifier	:
// Last Update	: 2017/2/10 - 10:54
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveRotateFile(__in LPCTSTR szPath, __in const ST_LT_TI_Rotate* pstRotateInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstRotateInfo)
		return FALSE;

	CString strValue;
	CString strAppName;

	int ibufPosX = 0;
	int ibufPosY = 0;
	int ibufWidth = 0;
	int ibufHeight = 0;

	strValue.Format(_T("%d"), pstRotateInfo->stRotateOp.nCameraState);
	WritePrivateProfileString(Rotate_OP_AppName, _T("CamState"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRotateInfo->stRotateOp.dbMasterDegree);
	WritePrivateProfileString(Rotate_OP_AppName, _T("MasterDegree"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRotateInfo->stRotateOp.dbDeviDegree);
	WritePrivateProfileString(Rotate_OP_AppName, _T("DeviDegree"), strValue, szPath);

	strValue.Format(_T("%d"), pstRotateInfo->stRotateOp.nCameraState);
	WritePrivateProfileString(Rotate_OP_AppName, _T("CameraState"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRotateInfo->stRotateOp.dbImageSensorPix);
	WritePrivateProfileString(Rotate_OP_AppName, _T("ImageSensorPix"), strValue, szPath);

	strValue.Format(_T("%d"), pstRotateInfo->stRotateOp.nWriteCnt);
	WritePrivateProfileString(Rotate_OP_AppName, _T("WriteCnt"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRotateInfo->stRotateOp.dbMaxDegree);
	WritePrivateProfileString(Rotate_OP_AppName, _T("MaxDegree"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRotateInfo->stRotateOp.dbTargetDegree);
	WritePrivateProfileString(Rotate_OP_AppName, _T("TargetDegree"), strValue, szPath);


	for (int iRg = 0; iRg < RegionRotate_MaxEnum; iRg++)
	{
		strAppName.Format(_T("%s %s"), Rotate_OP_AppName, g_szRegionRotate[iRg]);

		ibufPosX	= pstRotateInfo->stRotateOp.rectData[iRg].RegionList.CenterPoint().x;
		ibufPosY	= pstRotateInfo->stRotateOp.rectData[iRg].RegionList.CenterPoint().y;
		ibufWidth	= pstRotateInfo->stRotateOp.rectData[iRg].RegionList.Width();
		ibufHeight	= pstRotateInfo->stRotateOp.rectData[iRg].RegionList.Height();

		strValue.Format(_T("%d"), ibufPosX);
		WritePrivateProfileString(strAppName, _T("Pos_X"), strValue, szPath);

		strValue.Format(_T("%d"), ibufPosY);
		WritePrivateProfileString(strAppName, _T("Pos_Y"), strValue, szPath);

		strValue.Format(_T("%d"), ibufWidth);
		WritePrivateProfileString(strAppName, _T("Width"), strValue, szPath);

		strValue.Format(_T("%d"), ibufHeight);
		WritePrivateProfileString(strAppName, _T("Height"), strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: LoadRotateFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_LT_TI_Rotate & stRotateInfo
// Qualifier	:
// Last Update	: 2017/2/10 - 11:00
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadRotateFile(__in LPCTSTR szPath, __out ST_LT_TI_Rotate& stRotateInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strAppName;
	CString strValue;

	TCHAR   inBuff[255] = { 0, };

	int PosX[RegionRotate_MaxEnum] = { 0, };
	int PosY[RegionRotate_MaxEnum] = { 0, };
	int Width[RegionRotate_MaxEnum] = { 0, };
	int Height[RegionRotate_MaxEnum] = { 0, };

	int RectWidth = 75;
	int RectHeight = 75;
	int Gap_Width = 150;
	int Gap_Height = 150;

	int RectPosX = (CAM_IMAGE_WIDTH / 2) - (Gap_Width / 2);
	int RectPosY = (CAM_IMAGE_HEIGHT / 2) - (Gap_Height / 2);

	int buf_PosX = 0;
	int buf_PosY = 0;
	int buf_Width = 0;
	int buf_Height = 0;

	GetPrivateProfileString(Rotate_OP_AppName, _T("CamState"), _T("0"), inBuff, 80, szPath);
	stRotateInfo.stRotateOp.nCameraState = _ttoi(inBuff);

	GetPrivateProfileString(Rotate_OP_AppName, _T("MasterDegree"), _T("0.00"), inBuff, 80, szPath);
	stRotateInfo.stRotateOp.dbMasterDegree = _ttof(inBuff);

	GetPrivateProfileString(Rotate_OP_AppName, _T("DeviDegree"), _T("0.00"), inBuff, 80, szPath);
	stRotateInfo.stRotateOp.dbDeviDegree = _ttof(inBuff);

	GetPrivateProfileString(Rotate_OP_AppName, _T("TestMode"), _T("0"), inBuff, 80, szPath);
	stRotateInfo.stRotateOp.nTestMode = _ttoi(inBuff);

	GetPrivateProfileString(Rotate_OP_AppName, _T("ImageSensorPix"), _T("0.00"), inBuff, 80, szPath);
	stRotateInfo.stRotateOp.dbImageSensorPix = _ttof(inBuff);

	GetPrivateProfileString(Rotate_OP_AppName, _T("WriteCnt"), _T("1"), inBuff, 80, szPath);
	stRotateInfo.stRotateOp.nWriteCnt = _ttoi(inBuff);

	GetPrivateProfileString(Rotate_OP_AppName, _T("MaxDegree"), _T("5.00"), inBuff, 80, szPath);
	stRotateInfo.stRotateOp.dbMaxDegree = _ttof(inBuff);

	GetPrivateProfileString(Rotate_OP_AppName, _T("TargetDegree"), _T("0.50"), inBuff, 80, szPath);
	stRotateInfo.stRotateOp.dbTargetDegree = _ttof(inBuff);


	for (int iRg = 0; iRg < RegionRotate_MaxEnum; iRg++)
	{
		buf_PosX = 0;
		buf_PosY = 0;
		buf_Width = 0;
		buf_Height = 0;

		if (iRg == 0)
		{
			PosX[iRg] = RectPosX;
			PosY[iRg] = RectPosY;
		}
		else if (iRg == 1)
		{
			PosX[iRg] = RectPosX + Gap_Width;
			PosY[iRg] = RectPosY;
		}
		else if (iRg == 2)
		{
			PosX[iRg] = RectPosX;
			PosY[iRg] = RectPosY + Gap_Height;
		}
		else if (iRg == 3)
		{
			PosX[iRg] = RectPosX + Gap_Width;
			PosY[iRg] = RectPosY + Gap_Height;
		}

		Width[iRg] = RectWidth;
		Height[iRg] = RectHeight;

		strAppName.Format(_T("%s %s"), Rotate_OP_AppName, g_szRegionRotate[iRg]);

		strValue.Format(_T("%d"), PosX[iRg]);
		GetPrivateProfileString(strAppName, _T("Pos_X"), strValue, inBuff, 80, szPath);
		buf_PosX = _ttoi(inBuff);

		strValue.Format(_T("%d"), PosY[iRg]);
		GetPrivateProfileString(strAppName, _T("Pos_Y"), strValue, inBuff, 80, szPath);
		buf_PosY = _ttoi(inBuff);

		strValue.Format(_T("%d"), Width[iRg]);
		GetPrivateProfileString(strAppName, _T("Width"), strValue, inBuff, 80, szPath);
		buf_Width = _ttoi(inBuff);

		strValue.Format(_T("%d"), Height[iRg]);
		GetPrivateProfileString(strAppName, _T("Height"), strValue, inBuff, 80, szPath);
		buf_Height = _ttoi(inBuff);

		stRotateInfo.stRotateOp.StdrectData[iRg]._Rect_Position_Sum(buf_PosX, buf_PosY, buf_Width, buf_Height);
		stRotateInfo.stRotateOp.rectData[iRg]._Rect_Position_Sum(buf_PosX, buf_PosY, buf_Width, buf_Height);
	}

	return TRUE;
}

//=============================================================================
// Method		: LoadResolutionAll
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_LT_TI_EIAJ & stResolutionInfo
// Qualifier	:
// Last Update	: 2017/2/10 - 18:23
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadEIAJFile(__in LPCTSTR szPath, __out ST_LT_TI_EIAJ& stEIAJInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[80] = { 0, };
	CString	strAppName;

	GetPrivateProfileString(EIAJ_OP_AppName, _T("GViewMode"), _T("0"), inBuff, 80, szPath);
	stEIAJInfo.stEIAJOp.bGViewMode = _ttoi(inBuff);

	for (int iRg = 0; iRg < ReOp_ItemNum; iRg++)
	{
		strAppName.Format(_T("%s %s"), EIAJ_OP_AppName, g_szResolutionRegion[iRg]);

		GetPrivateProfileString(strAppName, _T("bUse"), _T("1"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].bUse = _ttoi(inBuff);
		stEIAJInfo.stEIAJData.bUse[iRg] = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("left"), _T("-1"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].RegionList.left = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("top"), _T("-1"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].RegionList.top = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("right"), _T("-1"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].RegionList.right = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("bottom"), _T("-1"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].RegionList.bottom = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("MtfRatio"), _T("80"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].i_MtfRatio = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Thr_min"), _T("250"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].i_Threshold_Min = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Thr_max"), _T("475"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].i_Threshold_Max = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Range_min"), _T("200"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].i_Range_Min = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Range_max"), _T("500"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].i_Range_Max = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Offset"), _T("0"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].iOffset = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Mode"), _T("4"), inBuff, 80, szPath);
		stEIAJInfo.stEIAJOp.StdrectData[iRg].i_Mode = _ttoi(inBuff);

		stEIAJInfo.stEIAJOp.rectData[iRg] = stEIAJInfo.stEIAJOp.StdrectData[iRg];
	}
	return TRUE;
}

//=============================================================================
// Method		: SaveResolutionAll
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_LT_TI_EIAJ * pstResolutionInfo
// Qualifier	:
// Last Update	: 2017/2/10 - 19:08
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveEIAJFile(__in LPCTSTR szPath, __in const ST_LT_TI_EIAJ* pstEIAJInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstEIAJInfo)
		return FALSE;

	CString strValue;
	CString strAppName;
	
	strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.bGViewMode);
	WritePrivateProfileString(EIAJ_OP_AppName, _T("GViewMode"), strValue, szPath);

	for (int iRg = 0; iRg < ReOp_ItemNum; iRg++)
	{
		strAppName.Format(_T("%s %s"), EIAJ_OP_AppName, g_szResolutionRegion[iRg]);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].bUse);
		WritePrivateProfileString(strAppName, _T("bUse"), strValue, szPath);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].RegionList.left);
		WritePrivateProfileString(strAppName, _T("left"), strValue, szPath);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].RegionList.top);
		WritePrivateProfileString(strAppName, _T("top"), strValue, szPath);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].RegionList.right);
		WritePrivateProfileString(strAppName, _T("right"), strValue, szPath);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].RegionList.bottom);
		WritePrivateProfileString(strAppName, _T("bottom"), strValue, szPath);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].i_Mode);
		WritePrivateProfileString(strAppName, _T("Mode"), strValue, szPath);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].i_MtfRatio);
		WritePrivateProfileString(strAppName, _T("MtfRatio"), strValue, szPath);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].i_Range_Min);
		WritePrivateProfileString(strAppName, _T("Range_min"), strValue, szPath);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].i_Range_Max);
		WritePrivateProfileString(strAppName, _T("Range_max"), strValue, szPath);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].i_Threshold_Min);
		WritePrivateProfileString(strAppName, _T("Thr_min"), strValue, szPath);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].i_Threshold_Max);
		WritePrivateProfileString(strAppName, _T("Thr_max"), strValue, szPath);

		strValue.Format(_T("%d"), pstEIAJInfo->stEIAJOp.rectData[iRg].iOffset);
		WritePrivateProfileString(strAppName, _T("Offset"), strValue, szPath);
	}


	return TRUE;
}

//=============================================================================
// Method		: LoadSFRFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_LT_TI_SFR & stSFRInfo
// Qualifier	:
// Last Update	: 2017/8/7 - 15:56
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadSFRFile(__in LPCTSTR szPath, __out ST_LT_TI_SFR& stSFRInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strValue;
	CString strAppName;

	GetPrivateProfileString(SFR_OP_AppName, _T("SmoothMode"), _T("0"), inBuff, 255, szPath);
	stSFRInfo.stSFROp.bSmoothMode = _ttoi(inBuff);

	GetPrivateProfileString(SFR_OP_AppName, _T("bField"), _T("0"), inBuff, 255, szPath);
	stSFRInfo.stSFROp.bField = _ttoi(inBuff);

	GetPrivateProfileString(SFR_OP_AppName, _T("bEdge"), _T("0"), inBuff, 255, szPath);
	stSFRInfo.stSFROp.bEdge = _ttoi(inBuff);

	GetPrivateProfileString(SFR_OP_AppName, _T("bDistortion"), _T("0"), inBuff, 255, szPath);
	stSFRInfo.stSFROp.bDistortion = _ttoi(inBuff);

	GetPrivateProfileString(SFR_OP_AppName, _T("PixelSizeW"), _T("0.0"), inBuff, 255, szPath);
	stSFRInfo.stSFROp.dbPixelSizeW = _ttof(inBuff);

	GetPrivateProfileString(SFR_OP_AppName, _T("iOffsetX"), _T("0"), inBuff, 255, szPath);
	stSFRInfo.stSFROp.iOffsetX = _ttoi(inBuff);

	GetPrivateProfileString(SFR_OP_AppName, _T("iOffsetY"), _T("0"), inBuff, 255, szPath);
	stSFRInfo.stSFROp.iOffsetY = _ttoi(inBuff);

	for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
	{
		strAppName.Format(_T("%s_%d"), SFR_OP_AppName, nIdx);

		GetPrivateProfileStringW(strAppName, _T("bEnable"), _T("0"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].bEnable = _ttoi(inBuff);
		stSFRInfo.stSFRData.bEnable[nIdx] = _ttoi(inBuff);

		GetPrivateProfileStringW(strAppName, _T("nPos_X") , _T("360"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].nPos_X = _ttoi(inBuff);

		GetPrivateProfileStringW(strAppName, _T("nPos_Y"), _T("240"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].nPos_Y = _ttoi(inBuff);

		GetPrivateProfileStringW(strAppName, _T("nWidth"), _T("30"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].nWidth = _ttoi(inBuff);

		GetPrivateProfileStringW(strAppName, _T("nHeight"), _T("20"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].nHeight = _ttoi(inBuff);

		GetPrivateProfileStringW(strAppName, _T("nType"), _T("0"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].nType = _ttoi(inBuff);

		GetPrivateProfileStringW(strAppName, _T("dbThreshold") , _T("0"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].dbThreshold = _ttof(inBuff);

		GetPrivateProfileStringW(strAppName, _T("dbOffset"), _T("0"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].dbOffset = _ttof(inBuff);

		GetPrivateProfileStringW(strAppName, _T("dbLinePair"), _T("45"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].dbLinePair = _ttof(inBuff);

		GetPrivateProfileStringW(strAppName, _T("dbRoi_X"), _T("1.0"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].dbRoi_X = _ttof(inBuff);

		GetPrivateProfileStringW(strAppName, _T("dbRoi_Y") , _T("1.0"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].dbRoi_Y = _ttof(inBuff);

		GetPrivateProfileStringW(strAppName, _T("Font"), _T("0"), inBuff, 255, szPath);
		stSFRInfo.stSFROp.stSFR_Region[nIdx].nFont = _ttoi(inBuff);

		stSFRInfo.stSFROp.stSFR_InitRegion[nIdx] = stSFRInfo.stSFROp.stSFR_Region[nIdx];
	}
	

	return TRUE;
}

//=============================================================================
// Method		: SaveSFRlFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_LT_TI_SFR * pstSFRInfo
// Qualifier	:
// Last Update	: 2017/8/7 - 15:59
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveSFRlFile(__in LPCTSTR szPath, __in const ST_LT_TI_SFR* pstSFRInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstSFRInfo)
		return FALSE;

	CString strValue;
	CString strAppName;

	strValue.Format(_T("%d"), pstSFRInfo->stSFROp.iOffsetX);
	WritePrivateProfileString(SFR_OP_AppName, _T("iOffsetX"), strValue, szPath);
	
	strValue.Format(_T("%d"), pstSFRInfo->stSFROp.iOffsetY);
	WritePrivateProfileString(SFR_OP_AppName, _T("iOffsetY"), strValue, szPath);

	strValue.Format(_T("%d"), pstSFRInfo->stSFROp.bSmoothMode);
	WritePrivateProfileString(SFR_OP_AppName, _T("SmoothMode"), strValue, szPath);

	strValue.Format(_T("%d"), pstSFRInfo->stSFROp.bField);
	WritePrivateProfileString(SFR_OP_AppName, _T("bField"), strValue, szPath);

	strValue.Format(_T("%d"), pstSFRInfo->stSFROp.bEdge);
	WritePrivateProfileString(SFR_OP_AppName, _T("bEdge"), strValue, szPath);

	strValue.Format(_T("%d"), pstSFRInfo->stSFROp.bDistortion);
	WritePrivateProfileString(SFR_OP_AppName, _T("bDistortion"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstSFRInfo->stSFROp.dbPixelSizeW);
	WritePrivateProfileString(SFR_OP_AppName, _T("PixelSizeW"), strValue, szPath);
	
	for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
	{
		strAppName.Format(_T("%s_%d"), SFR_OP_AppName, nIdx);

		strValue.Format(_T("%d"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("bEnable"), strValue, szPath);

		strValue.Format(_T("%d"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].nPos_X);
		WritePrivateProfileString(strAppName, _T("nPos_X"), strValue, szPath);

		strValue.Format(_T("%d"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].nPos_Y);
		WritePrivateProfileString(strAppName, _T("nPos_Y"), strValue, szPath);

		strValue.Format(_T("%d"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].nWidth);
		WritePrivateProfileString(strAppName, _T("nWidth"), strValue, szPath);

		strValue.Format(_T("%d"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].nHeight);
		WritePrivateProfileString(strAppName, _T("nHeight"), strValue, szPath);

		strValue.Format(_T("%d"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].nType);
		WritePrivateProfileString(strAppName, _T("nType"), strValue, szPath);

		strValue.Format(_T("%0.2f"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].dbThreshold);
		WritePrivateProfileString(strAppName, _T("dbThreshold"), strValue, szPath);

		strValue.Format(_T("%0.2f"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].dbOffset);
		WritePrivateProfileString(strAppName, _T("dbOffset"), strValue, szPath);

		strValue.Format(_T("%0.2f"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].dbLinePair);
		WritePrivateProfileString(strAppName, _T("dbLinePair"), strValue, szPath);

		strValue.Format(_T("%0.2f"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].dbRoi_X);
		WritePrivateProfileString(strAppName, _T("dbRoi_X"), strValue, szPath);

		strValue.Format(_T("%0.2f"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].dbRoi_Y);
		WritePrivateProfileString(strAppName, _T("dbRoi_Y"), strValue, szPath);

		strValue.Format(_T("%d"), pstSFRInfo->stSFROp.stSFR_Region[nIdx].nFont);
		WritePrivateProfileString(strAppName, _T("Font"), strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: SaveCenterPointOpFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_LT_TI_CenterPoint * pstCenterPointInfo
// Qualifier	:
// Last Update	: 2017/2/10 - 10:39
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveCenterPointOpFile(__in LPCTSTR szPath, __in const ST_LT_TI_CenterPoint* pstCenterPointInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstCenterPointInfo)
		return FALSE;
	CString strValue, strApp;


	strValue.Format(_T("%d"), pstCenterPointInfo->stCenterPointOp.nTestMode);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("TEST_MODE"), strValue, szPath);
	
	strValue.Format(_T("%d"), pstCenterPointInfo->stCenterPointOp.nCameraState);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("CAM_STATE"), strValue, szPath);

	strValue.Format(_T("%d"), pstCenterPointInfo->stCenterPointOp.nWriteCnt);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("WRITECNT"), strValue, szPath);
	
	strValue.Format(_T("%d"), pstCenterPointInfo->stCenterPointOp.nStandard_X);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("Standard_X"), strValue, szPath);

	strValue.Format(_T("%d"), pstCenterPointInfo->stCenterPointOp.nStandard_Y);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("Standard_Y"), strValue, szPath);

	strValue.Format(_T("%d"), pstCenterPointInfo->stCenterPointOp.nStandard_OffsetX);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("Standard_OffsetX"), strValue, szPath);

	strValue.Format(_T("%d"), pstCenterPointInfo->stCenterPointOp.nStandard_OffsetX);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("Standard_OffsetY"), strValue, szPath);

	strValue.Format(_T("%d"), pstCenterPointInfo->stCenterPointOp.nTarget_OffsetX);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("Target_OffsetX"), strValue, szPath);

	strValue.Format(_T("%d"), pstCenterPointInfo->stCenterPointOp.nTarget_OffsetY);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("Target_OffsetY"), strValue, szPath);

	strValue.Format(_T("%6.1f"), pstCenterPointInfo->stCenterPointOp.dApplied_RatioX);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("Applied_RatioX"), strValue, szPath);

	strValue.Format(_T("%6.1f"), pstCenterPointInfo->stCenterPointOp.dApplied_RatioY);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("Applied_RatioY"), strValue, szPath);

	strValue.Format(_T("%d"), pstCenterPointInfo->stCenterPointOp.nMax_PixX);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("Max_PixX"), strValue, szPath);

	strValue.Format(_T("%d"), pstCenterPointInfo->stCenterPointOp.nMax_PixY);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("Max_PixY"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstCenterPointInfo->stCenterPointOp.dbImageSensorPix);
	WritePrivateProfileString(CENTERPOINT_OP_AppName, _T("ImageSensorPix"), strValue, szPath);

	return TRUE;
}
//=============================================================================
// Method		: LoadPogoIniFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_PogoInfo & stPogoInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 10:13
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadPogoIniFile(__in LPCTSTR szPath, __out ST_PogoInfo& stPogoInfo)
{
	if (NULL == szPath)
		return FALSE;

	// 파일이 존재하는가?
	if (!PathFileExists(szPath))
	{
		return FALSE;
	}

	TCHAR   inBuff[80] = { 0, };

	//DWORD		dwCount_Max[MAX_SITE_CNT];
	//DWORD		dwCount[MAX_SITE_CNT];

	CString strKey;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		// Max Count
		strKey.Format(_T("PogoCnt_Max_%d"), nIdx);
		GetPrivateProfileString(_T("COUNT"), strKey, _T("50000"), inBuff, 80, szPath);
		stPogoInfo.dwCount_Max[nIdx] = _ttoi(inBuff);

		// Channel Count	
		strKey.Format(_T("PogoCnt_%d"), nIdx);
		GetPrivateProfileString(_T("COUNT"), strKey, _T("0"), inBuff, 80, szPath);
		stPogoInfo.dwCount[nIdx] = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: SavePogoIniFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_PogoInfo * pstPogoInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 10:14
// Desc.		:
//=============================================================================
BOOL CFile_Model::SavePogoIniFile(__in LPCTSTR szPath, __in const ST_PogoInfo* pstPogoInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstPogoInfo)
		return FALSE;

	::DeleteFile(szPath);

	CString strValue;
	CString strKey;

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		// Max Count
		strKey.Format(_T("PogoCnt_Max_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwCount_Max[nIdx]);
		WritePrivateProfileString(_T("COUNT"), strKey, strValue, szPath);

		// Channel Count
		strKey.Format(_T("PogoCnt_%d"), nIdx);
		strValue.Format(_T("%d"), pstPogoInfo->dwCount[nIdx]);
		WritePrivateProfileString(_T("COUNT"), strKey, strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: SavePogoCount
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nPogoIdx
// Parameter	: __in DWORD dwCount
// Qualifier	:
// Last Update	: 2017/1/6 - 10:58
// Desc.		:
//=============================================================================
BOOL CFile_Model::SavePogoCount(__in LPCTSTR szPath, __in UINT nPogoIdx, __in DWORD dwCount)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strKey;

	// Channel Count
	strKey.Format(_T("PogoCnt_%d"), nPogoIdx);
	strValue.Format(_T("%d"), dwCount);
	WritePrivateProfileString(_T("COUNT"), strKey, strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: LoadPogoCount
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nPogoIdx
// Parameter	: __out DWORD & dwCount
// Qualifier	:
// Last Update	: 2017/1/6 - 10:58
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadPogoCount(__in LPCTSTR szPath, __in UINT nPogoIdx, __out DWORD& dwCount)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[80] = { 0, };
	CString strValue;
	CString strKey;

	// Channel Count
	strKey.Format(_T("PogoCnt_%d"), nPogoIdx);
	strValue.Format(_T("%d"), dwCount);
	GetPrivateProfileString(_T("COUNT"), strKey, _T("0"), inBuff, 80, szPath);
	dwCount = _ttoi(inBuff);

	return TRUE;
}

//  [1/25/2019 ysJang]
//=============================================================================
// Method		: LoadDriverCountIniFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_DriverCountInfo & stDriverCountInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 10:13
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadDriverCountIniFile(__in LPCTSTR szPath, __out ST_DriverCountInfo& stDriverCountInfo)
{
	if (NULL == szPath)
		return FALSE;

	// 파일이 존재하는가?
	if (!PathFileExists(szPath))
	{
		return FALSE;
	}

	TCHAR   inBuff[80] = { 0, };

	//DWORD		dwCount_Max[MAX_SITE_CNT];
	//DWORD		dwCount[MAX_SITE_CNT];

	CString strKey;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		// Max Count
		strKey.Format(_T("DriverCount_Max_%d"), nIdx);
		GetPrivateProfileString(_T("COUNT"), strKey, _T("50000"), inBuff, 80, szPath);
		stDriverCountInfo.dwCount_Max[nIdx] = _ttoi(inBuff);

		// Channel Count	
		strKey.Format(_T("DriverCount_%d"), nIdx);
		GetPrivateProfileString(_T("COUNT"), strKey, _T("0"), inBuff, 80, szPath);
		stDriverCountInfo.dwCount[nIdx] = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: SaveDriverCountIniFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_DriverCountInfo * pstDriverCountInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 10:14
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveDriverCountIniFile(__in LPCTSTR szPath, __in const ST_DriverCountInfo* pstDriverCountInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstDriverCountInfo)
		return FALSE;

	::DeleteFile(szPath);

	CString strValue;
	CString strKey;

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		// Max Count
		strKey.Format(_T("DriverCount_Max_%d"), nIdx);
		strValue.Format(_T("%d"), pstDriverCountInfo->dwCount_Max[nIdx]);
		WritePrivateProfileString(_T("COUNT"), strKey, strValue, szPath);

		// Channel Count
		strKey.Format(_T("DriverCount_%d"), nIdx);
		strValue.Format(_T("%d"), pstDriverCountInfo->dwCount[nIdx]);
		WritePrivateProfileString(_T("COUNT"), strKey, strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: SaveDriverCountCount
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nDriverCountIdx
// Parameter	: __in DWORD dwCount
// Qualifier	:
// Last Update	: 2017/1/6 - 10:58
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveDriverCount(__in LPCTSTR szPath, __in UINT nDriverCountIdx, __in DWORD dwCount)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strKey;

	// Channel Count
	strKey.Format(_T("DriverCount_%d"), nDriverCountIdx);
	strValue.Format(_T("%d"), dwCount);
	WritePrivateProfileString(_T("COUNT"), strKey, strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: LoadDriverCountCount
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nDriverCountIdx
// Parameter	: __out DWORD & dwCount
// Qualifier	:
// Last Update	: 2017/1/6 - 10:58
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadDriverCount(__in LPCTSTR szPath, __in UINT nDriverCountIdx, __out DWORD& dwCount)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[80] = { 0, };
	CString strValue;
	CString strKey;

	// Channel Count
	strKey.Format(_T("DriverCount_%d"), nDriverCountIdx);
	strValue.Format(_T("%d"), dwCount);
	GetPrivateProfileString(_T("COUNT"), strKey, _T("0"), inBuff, 80, szPath);
	dwCount = _ttoi(inBuff);

	return TRUE;
}





//=============================================================================
// Method		: LoadAFInfoFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_AFInfo & stAFInfo
// Qualifier	:
// Last Update	: 2017/10/2 - 13:27
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadAFInfoFile(__in LPCTSTR szPath, __out ST_AFInfo& stAFInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	GetPrivateProfileString(AF_OP_AppName, _T("dSTD_Displace"), _T("0"), inBuff, 255, szPath);
	stAFInfo.dSTD_Displace = _ttof(inBuff);

	GetPrivateProfileString(AF_OP_AppName, _T("dSTD_DisplaceDev"), _T("0"), inBuff, 255, szPath);
	stAFInfo.dSTD_DisplaceDev = _ttof(inBuff);

	GetPrivateProfileString(AF_OP_AppName, _T("dLensConchoid"), _T("0"), inBuff, 255, szPath);
	stAFInfo.dLensConchoid = _ttof(inBuff);

	GetPrivateProfileString(AF_OP_AppName, _T("bLensDirection"), _T("0"), inBuff, 255, szPath);
	stAFInfo.bLensDirection = _ttoi(inBuff);

	GetPrivateProfileString(AF_OP_AppName, _T("dAFStepDegree1"), _T("0"), inBuff, 255, szPath);
	stAFInfo.dAFStepDegree1 = _ttof(inBuff);

	GetPrivateProfileString(AF_OP_AppName, _T("dAFStepDegree2"), _T("0"), inBuff, 255, szPath);
	stAFInfo.dAFStepDegree2 = _ttof(inBuff);

	GetPrivateProfileString(AF_OP_AppName, _T("dAFStepDegree3"), _T("0"), inBuff, 255, szPath);
	stAFInfo.dAFStepDegree3 = _ttof(inBuff);

	GetPrivateProfileString(AF_OP_AppName, _T("dAFRotateCnt"), _T("0"), inBuff, 255, szPath);
	stAFInfo.dAFRotateCnt = _ttof(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: SaveAFInfoFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out const ST_AFInfo * pstAFInfo
// Qualifier	:
// Last Update	: 2017/10/2 - 13:27
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveAFInfoFile(__in LPCTSTR szPath, __out const ST_AFInfo* pstAFInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstAFInfo)
		return FALSE;

	CString strValue;

	strValue.Format(_T("%0.2f"), pstAFInfo->dSTD_Displace);
	WritePrivateProfileString(AF_OP_AppName, _T("dSTD_Displace"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstAFInfo->dSTD_DisplaceDev);
	WritePrivateProfileString(AF_OP_AppName, _T("dSTD_DisplaceDev"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstAFInfo->dLensConchoid);
	WritePrivateProfileString(AF_OP_AppName, _T("dLensConchoid"), strValue, szPath);

	strValue.Format(_T("%d"), pstAFInfo->bLensDirection);
	WritePrivateProfileString(AF_OP_AppName, _T("bLensDirection"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstAFInfo->dAFStepDegree1);
	WritePrivateProfileString(AF_OP_AppName, _T("dAFStepDegree1"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstAFInfo->dAFStepDegree2);
	WritePrivateProfileString(AF_OP_AppName, _T("dAFStepDegree2"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstAFInfo->dAFStepDegree3);
	WritePrivateProfileString(AF_OP_AppName, _T("dAFStepDegree3"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstAFInfo->dAFRotateCnt);
	WritePrivateProfileString(AF_OP_AppName, _T("dAFRotateCnt"), strValue, szPath);

	return TRUE;
}


//=============================================================================
// Method		: LoadCustomTeachFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_CustomTeach & stCustomTeach
// Qualifier	:
// Last Update	: 2017/10/11 - 13:54
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadCustomTeachFile(__in LPCTSTR szPath, __out ST_CustomTeach& stCustomTeach)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("iUnloadingMotionSeq"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.iUnloadingMotionSeq = _ttoi(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("iVisionMotionSeq"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.iVisionMotionSeq = _ttoi(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("iAFPositionSeq"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.iAFPositionSeq = _ttoi(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("iDisplaceMotionASeq"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.iDisplaceMotionASeq = _ttoi(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("iDisplaceMotionBSeq"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.iDisplaceMotionBSeq = _ttoi(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dUnloadingMotion_OffsetZ"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dUnloadingMotion_OffsetZ = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dUnloadingMotion_OffsetX"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dUnloadingMotion_OffsetX = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dUnloadingMotion_OffsetY"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dUnloadingMotion_OffsetY = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dVisionMotion_OffsetZ"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dVisionMotion_OffsetZ = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dVisionMotion_OffsetX"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dVisionMotion_OffsetX = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dVisionMotion_OffsetY"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dVisionMotion_OffsetY = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionA_OffsetZ"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dDisplaceMotionA_OffsetZ = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionA_OffsetX"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dDisplaceMotionA_OffsetX = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionA_OffsetY"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dDisplaceMotionA_OffsetY = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionB_OffsetZ"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dDisplaceMotionB_OffsetZ = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionB_OffsetX"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dDisplaceMotionB_OffsetX = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionB_OffsetY"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dDisplaceMotionB_OffsetY = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dAFPosition_OffsetZ"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dAFPosition_OffsetZ = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dAFPosition_OffsetX"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dAFPosition_OffsetX = _ttof(inBuff);

	GetPrivateProfileString(CustomTeach_OP_AppName, _T("dAFPosition_OffsetY"), _T("0"), inBuff, 255, szPath);
	stCustomTeach.dAFPosition_OffsetY = _ttof(inBuff);


	return TRUE;
}

//=============================================================================
// Method		: SaveCustomTeachFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out const ST_CustomTeach * pstCustomTeach
// Qualifier	:
// Last Update	: 2017/10/11 - 13:54
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveCustomTeachFile(__in LPCTSTR szPath, __out const ST_CustomTeach* pstCustomTeach)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstCustomTeach)
		return FALSE;

	CString strValue; 

	strValue.Format(_T("%d"), pstCustomTeach->iUnloadingMotionSeq);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("iUnloadingMotionSeq"), strValue, szPath);

	strValue.Format(_T("%d"), pstCustomTeach->iVisionMotionSeq);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("iVisionMotionSeq"), strValue, szPath);

	strValue.Format(_T("%d"), pstCustomTeach->iAFPositionSeq);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("iAFPositionSeq"), strValue, szPath);
	
	strValue.Format(_T("%d"), pstCustomTeach->iDisplaceMotionASeq);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("iDisplaceMotionASeq"), strValue, szPath);
	
	strValue.Format(_T("%d"), pstCustomTeach->iDisplaceMotionBSeq);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("iDisplaceMotionBSeq"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dUnloadingMotion_OffsetZ);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dUnloadingMotion_OffsetZ"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dUnloadingMotion_OffsetX);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dUnloadingMotion_OffsetX"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dUnloadingMotion_OffsetY);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dUnloadingMotion_OffsetY"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dVisionMotion_OffsetZ);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dVisionMotion_OffsetZ"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dVisionMotion_OffsetX);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dVisionMotion_OffsetX"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dVisionMotion_OffsetY);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dVisionMotion_OffsetY"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dDisplaceMotionA_OffsetZ);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionA_OffsetZ"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dDisplaceMotionA_OffsetX);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionA_OffsetX"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dDisplaceMotionA_OffsetY);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionA_OffsetY"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dDisplaceMotionB_OffsetZ);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionB_OffsetZ"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dDisplaceMotionB_OffsetX);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionB_OffsetX"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dDisplaceMotionB_OffsetY);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dDisplaceMotionB_OffsetY"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dAFPosition_OffsetZ);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dAFPosition_OffsetZ"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dAFPosition_OffsetX);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dAFPosition_OffsetX"), strValue, szPath);

	strValue.Format(_T("%0.2f"), pstCustomTeach->dAFPosition_OffsetY);
	WritePrivateProfileString(CustomTeach_OP_AppName, _T("dAFPosition_OffsetY"), strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: LoadParticleOpFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_LT_TI_Particle & stParticleInfo
// Qualifier	:
// Last Update	: 2017/7/28 - 14:17
// Desc.		:
//=============================================================================
BOOL CFile_Model::LoadParticleOpFile(__in LPCTSTR szPath, __out ST_LT_TI_Particle& stParticleInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strAppName;
	CString strValue;

	TCHAR   inBuff[255] = { 0, };

	GetPrivateProfileString(PARTICLE_OP_AppName, _T("FailCheck"), _T("0"), inBuff, 80, szPath);
	stParticleInfo.stParticleOp.bFailCheck = _ttoi(inBuff);

	GetPrivateProfileString(PARTICLE_OP_AppName, _T("DustDis"), _T("2"), inBuff, 80, szPath);
	stParticleInfo.stParticleOp.iDustDis = _ttoi(inBuff);

	for (int iRg = 0; iRg < Particle_Region_MaxEnum; iRg++)
	{
		strAppName.Format(_T("%s %s"), PARTICLE_OP_AppName, g_szPaticleRegion[iRg]);

		GetPrivateProfileString(strAppName, _T("bEllipse"), _T("0"), inBuff, 80, szPath);
		stParticleInfo.stParticleOp.rectData[iRg].bEllipse = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("left"), _T("10"), inBuff, 80, szPath);
		stParticleInfo.stParticleOp.rectData[iRg].RegionList.left = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("top"), _T("10"), inBuff, 80, szPath);
		stParticleInfo.stParticleOp.rectData[iRg].RegionList.top = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("right"), _T("710"), inBuff, 80, szPath);
		stParticleInfo.stParticleOp.rectData[iRg].RegionList.right = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("bottom"), _T("470"), inBuff, 80, szPath);
		stParticleInfo.stParticleOp.rectData[iRg].RegionList.bottom = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("BruiseConc"), _T("2"), inBuff, 80, szPath);
		stParticleInfo.stParticleOp.rectData[iRg].dbBruiseConc = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("BruiseSize"), _T("15"), inBuff, 80, szPath);
		stParticleInfo.stParticleOp.rectData[iRg].iBruiseSize = _ttoi(inBuff);

	}
	return TRUE;
}

//=============================================================================
// Method		: SaveParticleOpFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_LT_TI_Particle * pstParticleInfo
// Qualifier	:
// Last Update	: 2017/7/28 - 14:17
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveParticleOpFile(__in LPCTSTR szPath, __in const ST_LT_TI_Particle* pstParticleInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstParticleInfo)
		return FALSE;

	CString strValue;
	CString strAppName;

	int ibufPosX = 0;
	int ibufPosY = 0;
	int ibufWidth = 0;
	int ibufHeight = 0;

	strValue.Format(_T("%d"), pstParticleInfo->stParticleOp.bFailCheck);
	WritePrivateProfileString(PARTICLE_OP_AppName, _T("FailCheck"), strValue, szPath);

	strValue.Format(_T("%d"), pstParticleInfo->stParticleOp.iDustDis);
	WritePrivateProfileString(PARTICLE_OP_AppName, _T("DustDis"), strValue, szPath);

	for (int iRg = 0; iRg < Particle_Region_MaxEnum; iRg++)
	{
		strAppName.Format(_T("%s %s"), PARTICLE_OP_AppName, g_szPaticleRegion[iRg]);

		strValue.Format(_T("%d"), pstParticleInfo->stParticleOp.rectData[iRg].bEllipse);
		WritePrivateProfileString(strAppName, _T("bEllipse"), strValue, szPath);

		strValue.Format(_T("%d"), pstParticleInfo->stParticleOp.rectData[iRg].RegionList.left);
		WritePrivateProfileString(strAppName, _T("left"), strValue, szPath);

		strValue.Format(_T("%d"), pstParticleInfo->stParticleOp.rectData[iRg].RegionList.top);
		WritePrivateProfileString(strAppName, _T("top"), strValue, szPath);

		strValue.Format(_T("%d"), pstParticleInfo->stParticleOp.rectData[iRg].RegionList.right);
		WritePrivateProfileString(strAppName, _T("right"), strValue, szPath);

		strValue.Format(_T("%d"), pstParticleInfo->stParticleOp.rectData[iRg].RegionList.bottom);
		WritePrivateProfileString(strAppName, _T("bottom"), strValue, szPath);

		strValue.Format(_T("%6.2f"), pstParticleInfo->stParticleOp.rectData[iRg].dbBruiseConc);
		WritePrivateProfileString(strAppName, _T("BruiseConc"), strValue, szPath);

		strValue.Format(_T("%d"), pstParticleInfo->stParticleOp.rectData[iRg].iBruiseSize);
		WritePrivateProfileString(strAppName, _T("BruiseSize"), strValue, szPath);

	}

	return TRUE;
}

//=============================================================================
// Method		: SaveMESFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_InspectionInfo* pstInspectInfo
// Parameter	: __in LPCTSTR szEqpId
// Qualifier	:
// Last Update	: 2017/9/18 - 10:42
// Desc.		:
//=============================================================================
BOOL CFile_Model::SaveMESFile(__in LPCTSTR szPath, __in const ST_InspectionInfo* pstInspectInfo, __in LPCTSTR szEqpId)
{
	if (NULL == pstInspectInfo)
		return FALSE;

	// 	if (pstModelInfo->szLotID.IsEmpty())
	// 		return FALSE;


	// C:\BMS_MES\설비코드_차수_YYYYMMDDHHMISS.txt
	// ,로 아이템 구분

	/*
	NG 판정인 경우
	LOTID, 차수, 0, A1R101:항목값1(계측치) : 불량종류 : 0, A2R102 : 항목값2(계측치) : 불량종류 : 0, …, N(n) : 항목값n : 불량종류 : 0[CrLf]

	OK 판정인 경우
	LOTID, 차수, 1[CrLf]
	LOTID, 차수, 합부, 항목값1 : 항목1합부, 항목값2 : 항목2합부, …, 항목값n : 항목n합부[CrLf]
	*/

	CString strPath;
	strPath.Format(_T("%s%s_%d_%04d%02d%02d%02d%02d%02d.txt"), szPath, szEqpId, pstInspectInfo->ModelInfo.nLotTryCnt,
		pstInspectInfo->LotInfo.StartTime.wYear, pstInspectInfo->LotInfo.StartTime.wMonth, pstInspectInfo->LotInfo.StartTime.wDay,
		pstInspectInfo->LotInfo.StartTime.wHour, pstInspectInfo->LotInfo.StartTime.wMinute, pstInspectInfo->LotInfo.StartTime.wSecond);

	CString strMES_Raw;

	if (MakeMESCamData(strMES_Raw, pstInspectInfo))
	{
		CStdioFile File;
		CFileException e;

		if (!File.Open(strPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		File.WriteString(strMES_Raw.GetBuffer());
		strMES_Raw.ReleaseBuffer();

		File.Close();

		return TRUE;
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: MakeMESCamData
// Access		: public  
// Returns		: BOOL
// Parameter	: __out CString & szCamData
// Parameter	: __in const ST_InspectionInfo* pstInspectInfo
// Qualifier	:
// Last Update	: 2017/9/18 - 10:42
// Desc.		:
//=============================================================================
BOOL CFile_Model::MakeMESCamData(__out CString& szCamData, __in const ST_InspectionInfo* pstInspectInfo)
{
	/*
	NG 판정인 경우
	LOTID, 차수, 0, A1R101:항목값1(계측치) : 불량종류 : 0, A2R102 : 항목값2(계측치) : 불량종류 : 0, …, N(n) : 항목값n : 불량종류 : 0[CrLf]

	OK 판정인 경우
	LOTID, 차수, 1[CrLf]
	LOTID, 차수, 합부, 항목값1 : 항목1합부, 항목값2 : 항목2합부, …, 항목값n : 항목n합부[CrLf]
	*/

	CString szRawData;
	CString szItem;
	CString szUnit;
	BOOL bPass = TRUE;

	szRawData += pstInspectInfo->ModelInfo.szLotID + _T(",");

	// Try 회수
	szItem.Format(_T("%d,"), pstInspectInfo->ModelInfo.nLotTryCnt);
	szRawData += szItem;

	// 최종 합불 판정
	if (pstInspectInfo->CamInfo.nJudgment == TR_Pass)
		szRawData += _T("1,");
	else
		szRawData += _T("0,");

	// 동작 전류 ( mA ) ------------------------------------------
	if (pstInspectInfo->ModelInfo.stCurrent.stCurrentData.nResult == TER_Pass)
	{
		szItem.Format(_T("%d:1,"), pstInspectInfo->ModelInfo.stCurrent.stCurrentData.nCurrent);
		szRawData += szItem;
	}
	else
	{
		szItem.Format(_T("%d:1,"), pstInspectInfo->ModelInfo.stCurrent.stCurrentData.nCurrent);
		szRawData += szItem;

		//광축2,해상30,로테1,이물1
		for (UINT nIdx = 0; nIdx < 33; nIdx++)
		{
			szItem = _T(":1,");
			szRawData += szItem;
		}

		// CR LF
		szRawData += _T(":1\r\n");
		szCamData = szRawData;

		return TRUE;
	}

	// 광축 ------------------------------------------
	if (pstInspectInfo->ModelInfo.stCenterPoint.stCenterPointData.nResult == TER_Pass)
	{
		szItem.Format(_T("%d:1,"), pstInspectInfo->ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_X);
		szRawData += szItem;

		szItem.Format(_T("%d:1,"), pstInspectInfo->ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_Y);
		szRawData += szItem;
	}
	else
	{

		szItem.Format(_T("%d:0,"), pstInspectInfo->ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_X);
		szRawData += szItem;

		szItem.Format(_T("%d:0,"), pstInspectInfo->ModelInfo.stCenterPoint.stCenterPointData.iResult_Offset_Y);
		szRawData += szItem;

		//해상30,로테1,이물1
		for (UINT nIdx = 0; nIdx < 31; nIdx++)
		{
			szItem = _T(":1,");
			szRawData += szItem;
		}

		// CR LF
		szRawData += _T(":1\r\n");
		szCamData = szRawData;

		return TRUE;
	}

	// 해상력
	if (pstInspectInfo->ModelInfo.stSFR.stSFRData.nResult == TER_Pass)
	{
		for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
		{
			if (pstInspectInfo->ModelInfo.stSFR.stSFROp.stSFR_Region[nIdx].bEnable == TRUE)
			{
				szItem.Format(_T("%.f:1,"), pstInspectInfo->ModelInfo.stSFR.stSFRData.dbResultValue[nIdx]);
			}
			else
			{
				szItem = _T(":1,");
			}

			szRawData += szItem;
		}
	}
	else
	{
		for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
		{
			if (pstInspectInfo->ModelInfo.stSFR.stSFROp.stSFR_Region[nIdx].bEnable == TRUE)
			{
				szItem.Format(_T("%.f:%d,"), pstInspectInfo->ModelInfo.stSFR.stSFRData.dbResultValue[nIdx], pstInspectInfo->ModelInfo.stSFR.stSFRData.nEachResult[nIdx]);
			}
			else
			{
				szItem = _T(":1,");
			}

			szRawData += szItem;
		}

		//로테1,이물1
		for (UINT nIdx = 0; nIdx < 31 - Region_SFR_MaxEnum; nIdx++)
		{
			szItem = _T(":1,");
			szRawData += szItem;
		}

		// CR LF
		szRawData += _T(":1\r\n");
		szCamData = szRawData;

		return TRUE;
	}

	// 로테이트 ------------------------------------------
	if (pstInspectInfo->ModelInfo.stRotate.stRotateData.nResult == TER_Pass)
	{
		szItem.Format(_T("%.2f:1,"), pstInspectInfo->ModelInfo.stRotate.stRotateData.dbDegree);
		szRawData += szItem;
	}
	else
	{
		szItem.Format(_T("%.2f:0,"), pstInspectInfo->ModelInfo.stRotate.stRotateData.dbDegree);
		szRawData += szItem;

		// CR LF
		szRawData += _T(":1\r\n");
		szCamData = szRawData;

		return TRUE;
	}

	// 이물 ------------------------------------------
	if (pstInspectInfo->ModelInfo.stParticle.stParticleData.nResult == TER_Pass)
	{
		szItem.Format(_T("pass:1"));
		szRawData += szItem;
	}
	else
	{
		szItem.Format(_T("fail:0"));
		szRawData += szItem;

		// CR LF
		szRawData += _T("\r\n");
		szCamData = szRawData;

		return TRUE;
	}

	// CR LF
	szRawData += _T("\r\n");
	szCamData = szRawData;

	return TRUE;
}
