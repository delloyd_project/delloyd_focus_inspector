﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_MotionTeach.cpp
// Created	:	2016/3/14 - 10:57
// Modified	:	2016/3/14 - 10:57
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_Vision.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_MotionTeach.h"
#include "resource.h"
#include "Def_WindowMessage.h"

typedef enum CMT_ID
{
	IDC_ED_CMT_UL_OFFSET_Z = 1000,
	IDC_ED_CMT_UL_OFFSET_X,
	IDC_ED_CMT_UL_OFFSET_Y,
	IDC_ED_CMT_UL_SEG_NAME,

	IDC_ED_CMT_VM_OFFSET_Z,
	IDC_ED_CMT_VM_OFFSET_X,
	IDC_ED_CMT_VM_OFFSET_Y,
	IDC_ED_CMT_VM_SEG_NAME,

	IDC_ED_CMT_DM1_OFFSET_Z,
	IDC_ED_CMT_DM1_OFFSET_X,
	IDC_ED_CMT_DM1_OFFSET_Y,
	IDC_ED_CMT_DM1_SEG_NAME,

	IDC_ED_CMT_DM2_OFFSET_Z,
	IDC_ED_CMT_DM2_OFFSET_X,
	IDC_ED_CMT_DM2_OFFSET_Y,
	IDC_ED_CMT_DM2_SEG_NAME,

	IDC_ED_CMT_AP_OFFSET_Z,
	IDC_ED_CMT_AP_OFFSET_X,
	IDC_ED_CMT_AP_OFFSET_Y,
	IDC_ED_CMT_AP_SEG_NAME,

	IDC_CB_CMT_VISION_MOTION = 2000,
	IDC_CB_CMT_AF_POSITION,
	IDC_CB_CMT_DISPLACE_MOTION_A,
	IDC_CB_CMT_DISPLACE_MOTION_B,
	IDC_CB_CMT_UNLOADING_MOTION,
};

// CWnd_Cfg_MotionTeach
IMPLEMENT_DYNAMIC(CWnd_Cfg_MotionTeach, CWnd_BaseView)

CWnd_Cfg_MotionTeach::CWnd_Cfg_MotionTeach()
{
	VERIFY(m_font_Data.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Cb.CreateFont(
		13,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_pDevice = NULL;
}

CWnd_Cfg_MotionTeach::~CWnd_Cfg_MotionTeach()
{
	m_font_Data.DeleteObject();
}

// IDC_CB_CMT_VISION_MOTION = 2000,
// IDC_CB_CMT_AF_POSITION,
// IDC_CB_CMT_DISPLACE_MOTION_A,
// IDC_CB_CMT_DISPLACE_MOTION_B,
// IDC_CB_CMT_UNLOADING_MOTION,

BEGIN_MESSAGE_MAP(CWnd_Cfg_MotionTeach, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_CB_CMT_VISION_MOTION,		&CWnd_Cfg_MotionTeach::OnEnSelectComboVM)
	ON_CBN_SELCHANGE(IDC_CB_CMT_AF_POSITION,		&CWnd_Cfg_MotionTeach::OnEnSelectComboAF)
	ON_CBN_SELCHANGE(IDC_CB_CMT_DISPLACE_MOTION_A,	&CWnd_Cfg_MotionTeach::OnEnSelectComboDMA)
	ON_CBN_SELCHANGE(IDC_CB_CMT_DISPLACE_MOTION_B,	&CWnd_Cfg_MotionTeach::OnEnSelectComboDMB)
	ON_CBN_SELCHANGE(IDC_CB_CMT_UNLOADING_MOTION,	&CWnd_Cfg_MotionTeach::OnEnSelectComboUL)
END_MESSAGE_MAP()


// CWnd_Cfg_MotionTeach message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
int CWnd_Cfg_MotionTeach::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < STI_CMT_MAXNUM; nIdx++)
	{
		if (nIdx == STI_CMT_Vision_Motion
			|| nIdx == STI_CMT_Displace_Motion_A
			|| nIdx == STI_CMT_Displace_Motion_B
			|| nIdx == STI_CMT_AF_Position
			|| nIdx == STI_CMT_UnLoading_Motion)
		{
			m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		} 
		else
		{
			m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		}

		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szCMT_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < Edit_CMT_MAXNUM; nIdx++)
	{
		if (   nIdx == Edit_CMT_UL_SEQ_NAME 
			|| nIdx == Edit_CMT_VM_SEQ_NAME
			|| nIdx == Edit_CMT_DM1_SEQ_NAME
			|| nIdx == Edit_CMT_DM2_SEQ_NAME
			|| nIdx == Edit_CMT_AP_SEQ_NAME
		)
		{
			m_ed_Item[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER | ES_READONLY, rectDummy, this, IDC_ED_CMT_VM_OFFSET_Z + nIdx);
		}
		else
		{
			m_ed_Item[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_CMT_VM_OFFSET_Z + nIdx);
		}

		m_ed_Item[nIdx].SetFont(&m_font_Data);
	}

	for (UINT nIdx = 0; nIdx < Combo_CMT_MAXNUM; nIdx++)
	{
		m_cb_Item[nIdx].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_CMT_VISION_MOTION + nIdx);
		m_cb_Item[nIdx].SetFont(&m_font_Cb);
	}

	for (UINT nIdx = 0; NULL != g_szCMT_Combo[nIdx]; nIdx++)
		m_cb_Item[Combo_CMT_UnloadingMotion].AddString(g_szCMT_Combo[nIdx]);

	for (UINT nIdx = 0; NULL != g_szCMT_Combo[nIdx]; nIdx++)
		m_cb_Item[Combo_CMT_VisionMotion].AddString(g_szCMT_Combo[nIdx]);

	for (UINT nIdx = 0; NULL != g_szCMT_Combo[nIdx]; nIdx++)
		m_cb_Item[Combo_CMT_AFPosition].AddString(g_szCMT_Combo[nIdx]);

	for (UINT nIdx = 0; NULL != g_szCMT_Combo[nIdx]; nIdx++)
		m_cb_Item[Combo_CMT_DisplaceMotion_A].AddString(g_szCMT_Combo[nIdx]);

	for (UINT nIdx = 0; NULL != g_szCMT_Combo[nIdx]; nIdx++)
		m_cb_Item[Combo_CMT_DisplaceMotion_B].AddString(g_szCMT_Combo[nIdx]);

	m_cb_Item[Combo_CMT_UnloadingMotion].SetCurSel(0);
	m_cb_Item[Combo_CMT_VisionMotion].SetCurSel(0);
	m_cb_Item[Combo_CMT_AFPosition].SetCurSel(0);
	m_cb_Item[Combo_CMT_DisplaceMotion_A].SetCurSel(0);
	m_cb_Item[Combo_CMT_DisplaceMotion_B].SetCurSel(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
void CWnd_Cfg_MotionTeach::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iCnt = 0;
	//(iWidth - (iSpacing * 5)) / 6;
	int iMagrin		 = 10;
	int iSpacing	 = 5;
	int iCateSpacing = 5;
	int iLeft		 = iMagrin;
	int iTop		 = iMagrin;
	int iWidth		 = cx - iMagrin - iMagrin;
	int iHeight		 = cy - iMagrin - iMagrin;

	int iMStWidth	 = (iWidth - (iSpacing * 2)) / 3;
	int iStWidth	 = (iMStWidth - iSpacing) / 2;
	int iEdWidth	 = iStWidth;
	int iStHeight	 = 21;
	int iHalfLeft	 = iWidth / 2;

	m_st_Item[STI_CMT_Vision_Motion].MoveWindow(iLeft, iTop, iMStWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_cb_Item[Combo_CMT_VisionMotion].MoveWindow(iLeft, iTop, iMStWidth, iStHeight);
	//m_ed_Item[Edit_CMT_VM_SEQ_NAME].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iStWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_VM_OFFSET_Z].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_VM_OFFSET_Z].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_VM_OFFSET_X].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_VM_OFFSET_X].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_VM_OFFSET_Y].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_VM_OFFSET_Y].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iMagrin;
	m_st_Item[STI_CMT_Displace_Motion_A].MoveWindow(iLeft, iTop, iMStWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_cb_Item[Combo_CMT_DisplaceMotion_A].MoveWindow(iLeft, iTop, iMStWidth, iStHeight);
	//m_ed_Item[Edit_CMT_DM1_SEQ_NAME].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iStWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_DM1_OFFSET_Z].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_DM1_OFFSET_Z].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_DM1_OFFSET_X].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_DM1_OFFSET_X].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_DM1_OFFSET_Y].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_DM1_OFFSET_Y].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iLeft += iMStWidth + iSpacing;
	iTop = iMagrin;

	m_st_Item[STI_CMT_AF_Position].MoveWindow(iLeft, iTop, iMStWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_cb_Item[Combo_CMT_AFPosition].MoveWindow(iLeft, iTop, iMStWidth, iStHeight);
	//m_ed_Item[Edit_CMT_AP_SEQ_NAME].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iStWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_AP_OFFSET_Z].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_AP_OFFSET_Z].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_AP_OFFSET_X].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_AP_OFFSET_X].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_AP_OFFSET_Y].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_AP_OFFSET_Y].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iMagrin;
	m_st_Item[STI_CMT_Displace_Motion_B].MoveWindow(iLeft, iTop, iMStWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_cb_Item[Combo_CMT_DisplaceMotion_B].MoveWindow(iLeft, iTop, iMStWidth, iStHeight);
	//m_ed_Item[Edit_CMT_DM2_SEQ_NAME].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iStWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_DM2_OFFSET_Z].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_DM2_OFFSET_Z].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_DM2_OFFSET_X].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_DM2_OFFSET_X].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_DM2_OFFSET_Y].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_DM2_OFFSET_Y].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iLeft += iMStWidth + iSpacing;
	iTop = iMagrin;

	m_st_Item[STI_CMT_UnLoading_Motion].MoveWindow(iLeft, iTop, iMStWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_cb_Item[Combo_CMT_UnloadingMotion].MoveWindow(iLeft, iTop, iMStWidth, iStHeight);
	//m_ed_Item[Edit_CMT_UL_SEQ_NAME].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iStWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_UL_OFFSET_Z].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_UL_OFFSET_Z].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_UL_OFFSET_X].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_UL_OFFSET_X].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);

	iTop += iStHeight + iSpacing;
	m_st_Item[STI_CMT_UL_OFFSET_Y].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
	m_ed_Item[Edit_CMT_UL_OFFSET_Y].MoveWindow(iLeft + (iStWidth + iSpacing), iTop, iEdWidth, iStHeight);
}


//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_MotionTeach::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_MotionTeach::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if ((::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 86)) // Ctrl + V
		{
			CWnd* pWnd = CWnd::FromHandle(pMsg->hwnd);
			pWnd->SetWindowText(_T(""));
			pMsg->message = WM_PASTE;
		}

		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 67)  // Ctrl + C
			pMsg->message = WM_COPY;

		// 		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 90)  // Ctrl + Z
		// 			pMsg->message = WM_UNDO;

		break;
	}

	return CWnd_BaseView::PreTranslateMessage(pMsg);
}



void CWnd_Cfg_MotionTeach::OnEnSelectComboVM()
{

}

void CWnd_Cfg_MotionTeach::OnEnSelectComboAF()
{

}

void CWnd_Cfg_MotionTeach::OnEnSelectComboUL()
{

}

void CWnd_Cfg_MotionTeach::OnEnSelectComboDMA()
{

}

void CWnd_Cfg_MotionTeach::OnEnSelectComboDMB()
{

}

//=============================================================================
// Method		: GetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 13:13
// Desc.		:
//=============================================================================
void CWnd_Cfg_MotionTeach::GetUIData(__out ST_ModelInfo& stModelInfo)
{
	CString strValue;

	stModelInfo.stCustomTeach.iUnloadingMotionSeq	= m_cb_Item[Combo_CMT_UnloadingMotion].GetCurSel();
	stModelInfo.stCustomTeach.iVisionMotionSeq		= m_cb_Item[Combo_CMT_VisionMotion].GetCurSel();
	stModelInfo.stCustomTeach.iAFPositionSeq		= m_cb_Item[Combo_CMT_AFPosition].GetCurSel();
	stModelInfo.stCustomTeach.iDisplaceMotionASeq	= m_cb_Item[Combo_CMT_DisplaceMotion_A].GetCurSel();
	stModelInfo.stCustomTeach.iDisplaceMotionBSeq	= m_cb_Item[Combo_CMT_DisplaceMotion_B].GetCurSel();

	m_ed_Item[Edit_CMT_UL_OFFSET_Z].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dUnloadingMotion_OffsetZ = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_UL_OFFSET_Y].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dUnloadingMotion_OffsetY = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_UL_OFFSET_X].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dUnloadingMotion_OffsetX = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_VM_OFFSET_Z].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dVisionMotion_OffsetZ = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_VM_OFFSET_Y].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dVisionMotion_OffsetY = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_VM_OFFSET_X].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dVisionMotion_OffsetX = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_DM1_OFFSET_Z].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dDisplaceMotionA_OffsetZ = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_DM1_OFFSET_Y].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dDisplaceMotionA_OffsetY = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_DM1_OFFSET_X].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dDisplaceMotionA_OffsetX = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_DM2_OFFSET_Z].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dDisplaceMotionB_OffsetZ = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_DM2_OFFSET_Y].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dDisplaceMotionB_OffsetY = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_DM2_OFFSET_X].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dDisplaceMotionB_OffsetX = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_AP_OFFSET_Z].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dAFPosition_OffsetZ = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_AP_OFFSET_Y].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dAFPosition_OffsetY = _ttof(strValue);
	strValue.ReleaseBuffer();

	m_ed_Item[Edit_CMT_AP_OFFSET_X].GetWindowTextW(strValue);
	stModelInfo.stCustomTeach.dAFPosition_OffsetX = _ttof(strValue);
	strValue.ReleaseBuffer();

}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_MotionTeach::SetUIData(__in const ST_ModelInfo* pModelInfo)
{
	CString strValue;

	if (m_pDevice != NULL)
	{
		m_cb_Item[Combo_CMT_UnloadingMotion].ResetContent();
		m_cb_Item[Combo_CMT_VisionMotion].ResetContent();
		m_cb_Item[Combo_CMT_AFPosition].ResetContent();
		m_cb_Item[Combo_CMT_DisplaceMotion_A].ResetContent();
		m_cb_Item[Combo_CMT_DisplaceMotion_B].ResetContent();

		for (int i = 0; i < DEF_MOTION_SEQ_MAX; i++)
			m_cb_Item[Combo_CMT_UnloadingMotion].AddString(m_pDevice->MotionManager.m_AllMotorData.MotionSeq[i].szSequenceName);

		for (int i = 0; i < DEF_MOTION_SEQ_MAX; i++)
			m_cb_Item[Combo_CMT_VisionMotion].AddString(m_pDevice->MotionManager.m_AllMotorData.MotionSeq[i].szSequenceName);

		for (int i = 0; i < DEF_MOTION_SEQ_MAX; i++)
			m_cb_Item[Combo_CMT_AFPosition].AddString(m_pDevice->MotionManager.m_AllMotorData.MotionSeq[i].szSequenceName);

		for (int i = 0; i < DEF_MOTION_SEQ_MAX; i++)
			m_cb_Item[Combo_CMT_DisplaceMotion_A].AddString(m_pDevice->MotionManager.m_AllMotorData.MotionSeq[i].szSequenceName);

		for (int i = 0; i < DEF_MOTION_SEQ_MAX; i++)
			m_cb_Item[Combo_CMT_DisplaceMotion_B].AddString(m_pDevice->MotionManager.m_AllMotorData.MotionSeq[i].szSequenceName);
	}

	m_cb_Item[Combo_CMT_UnloadingMotion].SetCurSel(pModelInfo->stCustomTeach.iUnloadingMotionSeq);
	m_cb_Item[Combo_CMT_VisionMotion].SetCurSel(pModelInfo->stCustomTeach.iVisionMotionSeq);
	m_cb_Item[Combo_CMT_AFPosition].SetCurSel(pModelInfo->stCustomTeach.iAFPositionSeq);
	m_cb_Item[Combo_CMT_DisplaceMotion_A].SetCurSel(pModelInfo->stCustomTeach.iDisplaceMotionASeq);
	m_cb_Item[Combo_CMT_DisplaceMotion_B].SetCurSel(pModelInfo->stCustomTeach.iDisplaceMotionBSeq);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dUnloadingMotion_OffsetZ);
	m_ed_Item[Edit_CMT_UL_OFFSET_Z].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dUnloadingMotion_OffsetY);
	m_ed_Item[Edit_CMT_UL_OFFSET_Y].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dUnloadingMotion_OffsetX);
	m_ed_Item[Edit_CMT_UL_OFFSET_X].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dVisionMotion_OffsetZ);
	m_ed_Item[Edit_CMT_VM_OFFSET_Z].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dVisionMotion_OffsetY);
	m_ed_Item[Edit_CMT_VM_OFFSET_Y].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dVisionMotion_OffsetX);
	m_ed_Item[Edit_CMT_VM_OFFSET_X].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dDisplaceMotionA_OffsetZ);
	m_ed_Item[Edit_CMT_DM1_OFFSET_Z].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dDisplaceMotionA_OffsetY);
	m_ed_Item[Edit_CMT_DM1_OFFSET_Y].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dDisplaceMotionA_OffsetX);
	m_ed_Item[Edit_CMT_DM1_OFFSET_X].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dDisplaceMotionB_OffsetZ);
	m_ed_Item[Edit_CMT_DM2_OFFSET_Z].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dDisplaceMotionB_OffsetY);
	m_ed_Item[Edit_CMT_DM2_OFFSET_Y].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dDisplaceMotionB_OffsetX);
	m_ed_Item[Edit_CMT_DM2_OFFSET_X].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dAFPosition_OffsetZ);
	m_ed_Item[Edit_CMT_AP_OFFSET_Z].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dAFPosition_OffsetY);
	m_ed_Item[Edit_CMT_AP_OFFSET_Y].SetWindowTextW(strValue);

	strValue.Format(_T("%0.2f"), pModelInfo->stCustomTeach.dAFPosition_OffsetX);
	m_ed_Item[Edit_CMT_AP_OFFSET_X].SetWindowTextW(strValue);
}

//=============================================================================
// Method		: SetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_ModelInfo * pModelInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_MotionTeach::SetModelInfo(__in const ST_ModelInfo* pModelInfo)
{
	SetUIData(pModelInfo);
}

//=============================================================================
// Method		: GetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/4 - 10:42
// Desc.		:
//=============================================================================
void CWnd_Cfg_MotionTeach::GetModelInfo(__out ST_ModelInfo& stModelInfo)
{
	GetUIData(stModelInfo);
}
