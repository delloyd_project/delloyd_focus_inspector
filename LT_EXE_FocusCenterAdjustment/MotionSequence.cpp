﻿//*****************************************************************************
// Filename	: 	MotionSequence.cpp
// Created	:	2017/01/09 - 13:54
// Modified	:	2017/01/09 - 13:54
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "MotionSequence.h"
#include "CommonFunction.h"

CMotionSequence::CMotionSequence(LPVOID p)
{
	m_hOwnerWnd			= NULL;
	m_pstOption			= NULL;
	m_pstModelInfo		= NULL;
	m_pDigitalIOCtrl	= NULL;
	m_pMotion			= NULL;
	m_bAreaCheck		= FALSE;
	m_bOriginStop		= FALSE;
	m_bXmlStop			= FALSE;
	m_bIoLoopStop		= FALSE;

}

CMotionSequence::~CMotionSequence(void)
{
}

//=============================================================================
// Method		: AllMotorOrigin
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/28 - 15:15
// Desc.		:
//=============================================================================
BOOL CMotionSequence::AllMotorOrigin()
{
	if (!m_pMotion->SetAllAmpCtr(ON))
		return FALSE;

	if (!m_pMotion->SetAllAlarmClear())
		return FALSE;

	m_bOriginStop = FALSE;

	for (UINT nStep = 0; nStep < 2; nStep++)
	{
		if (m_bOriginStop == TRUE)
			return FALSE;

		if(!OriginProcedure(nStep))
			return FALSE;

		Sleep(1000);
	}

	return TRUE;
}

//=============================================================================
// Method		: RunProcedure
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nStep
// Qualifier	:
// Last Update	: 2017/3/9 - 18:35
// Desc.		:
//=============================================================================
BOOL CMotionSequence::OriginProcedure(UINT nStep)
{
	clock_t start_tm;

	switch (nStep)
	{
	case 0:
		m_pMotion->SetMotorEStop(MotorAxisY);
		Sleep(200);

		m_pMotion->SetMotorOrigin(MotorAxisY);

		start_tm = clock();
		do
		{
			// 제한 시간 동안 마무리 되지 않을 경우 자동 STOP	
			if ((DWORD)clock() - start_tm > MotorOriginWaitTime )
			{
				m_pMotion->m_pMotionCtr[MotorAxisY].OriginStopAxis();
				return FALSE;
			}

			// 원점 정지 시
			if (m_pMotion->GetOriginStopStatus(MotorAxisY))
			{
				m_bOriginStop = TRUE;
				return FALSE;
			}

			DoEvents(1);

		} while (!m_pMotion->m_pMotionCtr[MotorAxisY].GetOriginStatus());
		
		break;

	case 1:
		m_pMotion->SetMotorEStop(MotorAxisX);
		m_pMotion->SetMotorEStop(MotorAxisZ);
		m_pMotion->SetMotorEStop(MotorAxisR);

		Sleep(200);

		m_pMotion->SetMotorOrigin(MotorAxisX);
		m_pMotion->SetMotorOrigin(MotorAxisZ);
		m_pMotion->SetMotorOrigin(MotorAxisR);

		start_tm = clock();
		do
		{
			// 제한 시간 동안 마무리 되지 않을 경우 자동 STOP	
			if ((DWORD)clock() - start_tm > MotorOriginWaitTime)
			{
				m_pMotion->m_pMotionCtr[MotorAxisX].OriginStopAxis();
				m_pMotion->m_pMotionCtr[MotorAxisZ].OriginStopAxis();
				m_pMotion->m_pMotionCtr[MotorAxisR].OriginStopAxis();
				return FALSE;
			}

			// 원점 정지 시
			if (m_pMotion->GetOriginStopStatus(MotorAxisX) || m_pMotion->GetOriginStopStatus(MotorAxisZ) || m_pMotion->GetOriginStopStatus(MotorAxisR))
			{
				m_bOriginStop = TRUE;
				return FALSE;
			}

			DoEvents(1);

		} while (!m_pMotion->m_pMotionCtr[MotorAxisX].GetOriginStatus() || !m_pMotion->m_pMotionCtr[MotorAxisZ].GetOriginStatus() || !m_pMotion->m_pMotionCtr[MotorAxisR].GetOriginStatus());

		break;

	default:
		break;
	}

	return TRUE;
}

//=============================================================================
// Method		: XmlProcedure
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/5/11 - 11:57
// Desc.		:
//=============================================================================
BOOL CMotionSequence::XmlProcedure()
{
// 	m_bXmlStop = FALSE;
// 	ST_SequenceItem stSeqItem;
// 	for (int iCnt = 0; iCnt <= m_pSeqList->StepList.GetUpperBound(); iCnt++)
// 	{
// 		stSeqItem = m_pSeqList->StepList.GetAt(iCnt);
// 
// 		if (m_bXmlStop)
// 			break;
// 
// 		m_pstMotion->MotorAxisDitailMove(POS_ABS_MODE, stSeqItem.nAxis, stSeqItem.dbPos, stSeqItem.dbVel, stSeqItem.dbAcc);
// 		
// 		if (m_bXmlStop)
// 			break;
// 		
// 		Sleep(stSeqItem.nDelay);
// 	}

	return TRUE;
}

//=============================================================================
// Method		: Stage3AxisInit
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/9/18 - 11:21
// Desc.		:
//=============================================================================
BOOL CMotionSequence::Stage3AxisInit()
{
	ST_AxisMoveParam stAxisParam[MotorAxisNum];

	stAxisParam[MotorAxisX].lAxisNum = MotorAxisX;
	stAxisParam[MotorAxisX].dbPos = 0;
	stAxisParam[MotorAxisX].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisX].lAxisNum].dbVel;
	stAxisParam[MotorAxisX].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisX].lAxisNum].dbAcc;
	stAxisParam[MotorAxisX].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisX].lAxisNum].dbAcc;

	stAxisParam[MotorAxisY].lAxisNum = MotorAxisY;
	stAxisParam[MotorAxisY].dbPos = 0;
	stAxisParam[MotorAxisY].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbVel;
	stAxisParam[MotorAxisY].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbAcc;
	stAxisParam[MotorAxisY].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbAcc;

	if (m_pMotion->MotorMultiMove(POS_ABS_MODE, 2, stAxisParam) == FALSE)
	{
		return FALSE;
	}

	if (m_pMotion->MotorAxisMove(POS_ABS_MODE, MotorAxisR, 0) == FALSE)
	{
		return FALSE;
	}

	return TRUE;
}


//=============================================================================
// Method		: IsReadyOk
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nPort
// Parameter	: BOOL bStatus
// Qualifier	:
// Last Update	: 2017/8/13 - 9:44
// Desc.		:
//=============================================================================
BOOL CMotionSequence::IsReadyOk(UINT nPort, BOOL bStatus /*= TRUE*/)
{
	CString str;
	m_bIoLoopStop = TRUE;

	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
		return FALSE;

	BOOL bRet(FALSE);

	clock_t	start_tm = clock();
	do
	{
		if (m_pDigitalIOCtrl->GetInStatus(nPort) == bStatus)
		{
			bRet = TRUE;
			break;
		}
		m_pDigitalIOCtrl->DoEvents(1);

	} while (((DWORD)(clock() - start_tm) < DIoWaitTime) && (m_bIoLoopStop));

	return bRet;
}

//=============================================================================
// Method		: CenterPointAdjustment
// Access		: public  
// Returns		: BOOL
// Parameter	: int iOffsetX
// Parameter	: int iOffsetY
// Qualifier	:
// Last Update	: 2017/8/12 - 21:44
// Desc.		:
//=============================================================================
BOOL CMotionSequence::CenterPointAdjustment(int iOffsetX, int iOffsetY)
{
	// 광축 값 * 1픽세 이동 마이크론
	double dbOffsetX = double(iOffsetX) * m_pstModelInfo->stCenterPoint.stCenterPointOp.dbImageSensorPix * (-1) * 10;
	double dbOffsetY = double(iOffsetY) * m_pstModelInfo->stCenterPoint.stCenterPointOp.dbImageSensorPix * 10;

	// 멀티축 구조체
	ST_AxisMoveParam stAxisParam[MotorAxisNum];

	stAxisParam[MotorAxisX].lAxisNum = MotorAxisX;
	stAxisParam[MotorAxisX].dbPos = dbOffsetX;
	stAxisParam[MotorAxisX].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisX].lAxisNum].dbVel;
	stAxisParam[MotorAxisX].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisX].lAxisNum].dbAcc;
	stAxisParam[MotorAxisX].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisX].lAxisNum].dbAcc;

	stAxisParam[MotorAxisY].lAxisNum = MotorAxisY;
	stAxisParam[MotorAxisY].dbPos = dbOffsetY;
	stAxisParam[MotorAxisY].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbVel;
	stAxisParam[MotorAxisY].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbAcc;
	stAxisParam[MotorAxisY].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbAcc;

	if (m_pMotion->MotorMultiMove(POS_REL_MODE, 2, stAxisParam))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//=============================================================================
// Method		: RotateAdjustment
// Access		: public  
// Returns		: BOOL
// Parameter	: double dbDegree
// Qualifier	:
// Last Update	: 2017/8/13 - 9:16
// Desc.		:
//=============================================================================
BOOL CMotionSequence::RotateAdjustment(double dbDegree)
{
	double dbOffsetR = dbDegree * m_pstModelInfo->stCenterPoint.stCenterPointOp.dbImageSensorPix * 800 * (-1);

	if (m_pMotion->MotorAxisMove(POS_REL_MODE, MotorAxisR, dbOffsetR))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: FocusAdjustment
// Access		: public  
// Returns		: BOOL
// Parameter	: __in int iDetectCnt
// Parameter	: __in double dbStepDegree
// Parameter	: __in double dbLensConchoid
// Parameter	: __in int iStepData
// Parameter	: __in int iMDevData
// Parameter	: __in int * iCurrData
// Parameter	: __in int * iThrMin
// Parameter	: __in int * iThrMax
// Qualifier	:
// Last Update	: 2017/9/27 - 21:30
// Desc.		:
//=============================================================================
BOOL CMotionSequence::FocusAdjustment
(
	__in int	iDetectCnt,		// 측정 영역 개수
	__in double dbStepDegree,	// 해상력 단위 각도.	
	__in double dbLensConchoid,	// 렌즈 360도 회전 시 나사선 간격
	__in int	iStepData,		// 각도 단위 해상력
	__in int	iMDevData,		// 해상력 측정 편차
	__in int*	iCurrData,		// 해상력 측정 데이터
	__in int*	iThrMin,		// 해상력 측정 데이터 하한선
	__in int*	iThrMax			// 해상력 측정 데이터 상한선
)
{
	if (iDetectCnt <= 0)
		return TRUE;

	int iAdjData = 0;

	for (int i = 0; i < iDetectCnt; i++)
	{
		int nAvgThr = (iThrMax[i] + iThrMin[i]) / 2;
		iAdjData += (nAvgThr - iCurrData[i]);
	}

	iAdjData /= iDetectCnt;

	if (abs(iAdjData) >= iMDevData)
	{
		double dbCalData		= (double)iAdjData * dbStepDegree;
		double dbCalDegree		= dbCalData / (double)iStepData;
		double dbCalDistance	= ((dbCalDegree / 360) * dbLensConchoid) * 1000.0;

		dbCalDegree *= 100;

		// 멀티축 구조체
		ST_AxisMoveParam stAxisParam[MotorAxisNum];

		stAxisParam[MotorAxisR].lAxisNum = MotorAxisR;
		stAxisParam[MotorAxisR].dbPos = dbCalDegree;
		stAxisParam[MotorAxisR].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisR].lAxisNum].dbVel;
		stAxisParam[MotorAxisR].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisR].lAxisNum].dbAcc;
		stAxisParam[MotorAxisR].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisR].lAxisNum].dbAcc;

		stAxisParam[MotorAxisY].lAxisNum = MotorAxisY;
		stAxisParam[MotorAxisY].dbPos = dbCalDistance;
		stAxisParam[MotorAxisY].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbVel;
		stAxisParam[MotorAxisY].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbAcc;
		stAxisParam[MotorAxisY].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[MotorAxisY].lAxisNum].dbAcc;

		// 보간 축 이동으로 변경..
		if (!m_pMotion->MotorMultiMove(POS_REL_MODE, 2, stAxisParam))
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}


	return TRUE;
}



//=============================================================================
// Method		: ColletDegreeMove
// Access		: public  
// Returns		: BOOL
// Parameter	: __in AXT_MOTION_ABSREL_MODE nMode
// Parameter	: __in double dDegree
// Qualifier	:
// Last Update	: 2017/10/19 - 13:55
// Desc.		:
//=============================================================================
BOOL CMotionSequence::ColletDegreeMove(__in AXT_MOTION_ABSREL_MODE nMode, __in double dDegree)
{
	double dPos_Degree = dDegree * 100;

	if (m_pMotion->MotorAxisMove(nMode, MotorAxisR, dPos_Degree) == FALSE)
	{
		return FALSE;
	}

	return TRUE;
}



//=============================================================================
// Method		: FocusingDegreeMove
// Access		: public  
// Returns		: BOOL
// Parameter	: __in double dDegree
// Parameter	: __in double dConchoid
// Qualifier	:
// Last Update	: 2017/10/22 - 15:21
// Desc.		:
//=============================================================================
BOOL CMotionSequence::FocusingDegreeMove(__in double dDegree, __in double dConchoid)
{
	double dPos_Degree		= dDegree * 100;
	double dPos_Conchoid	= dConchoid * 1000;

	ST_AxisMoveParam stAxisParam[DEF_MULTI_AXIS_CTRL];

	stAxisParam[0].lAxisNum		= MotorAxisY;
	stAxisParam[0].dbPos		= dPos_Conchoid;
	stAxisParam[0].dbVel		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbVel;
	stAxisParam[0].dbAcc		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbAcc;
	stAxisParam[0].dbDec		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbAcc;

	stAxisParam[1].lAxisNum		= MotorAxisR;
	stAxisParam[1].dbPos		= dPos_Degree;
	stAxisParam[1].dbVel		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbVel;
	stAxisParam[1].dbAcc		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbAcc;
	stAxisParam[1].dbDec		= m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbAcc;

	if (!m_pMotion->MotorAxisLinearMove(POS_REL_MODE, DEF_MULTI_AXIS_CTRL, stAxisParam))
	{
		return FALSE;
	}

	return TRUE;
}


//************************************
// Method:    FocusingInitMove
// FullName:  CMotionSequence::FocusingInitMove
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: __in double dInitY
// Parameter: __in double dInitR
//************************************
BOOL CMotionSequence::FocusingInitMove(__in double dInitY, __in double dInitR)
{
	ST_AxisMoveParam stAxisParam[DEF_MULTI_AXIS_CTRL];

	stAxisParam[0].lAxisNum = MotorAxisY;
	stAxisParam[0].dbPos = dInitY;
	stAxisParam[0].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbVel;
	stAxisParam[0].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbAcc;
	stAxisParam[0].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[0].lAxisNum].dbAcc;

	stAxisParam[1].lAxisNum = MotorAxisR;
	stAxisParam[1].dbPos = dInitR;
	stAxisParam[1].dbVel = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbVel;
	stAxisParam[1].dbAcc = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbAcc;
	stAxisParam[1].dbDec = m_pMotion->m_AllMotorData.pMotionParam[stAxisParam[1].lAxisNum].dbAcc;

	if (!m_pMotion->MotorAxisLinearMove(POS_ABS_MODE, DEF_MULTI_AXIS_CTRL, stAxisParam))
	{
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: InterlockMove
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/10/19 - 14:23
// Desc.		:
//=============================================================================
// BOOL CMotionSequence::InterlockMove(__in BOOL bOnOff)
// {
// 	// 보드 연결 상태 확인
// 	if (!m_pDigitalIOCtrl->AXTState())
// 	{
// 		m_Log.LogMsg_Err(_T("Disconnect IoBoard..."));
// 		return FALSE;
// 	}
// 
// 	m_pDigitalIOCtrl->SetOutPort(DO_InterLockIn, IO_SignalT_SetOff);
// 	m_pDigitalIOCtrl->SetOutPort(DO_InterLockOut, IO_SignalT_SetOff);
// 
// 	if (bOnOff == ON)
// 	{
// 		m_pDigitalIOCtrl->SetOutPort(DO_InterLockIn, IO_SignalT_SetOn);
// 	}
// 	else if (bOnOff == OFF)
// 	{
// 		m_pDigitalIOCtrl->SetOutPort(DO_InterLockOut, IO_SignalT_SetOn);
// 	}
// 
// 	return TRUE;
// }

//=============================================================================
// Method		: CYLJigControl
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/9/27 - 19:43
// Desc.		:
//=============================================================================
// BOOL CMotionSequence::CYLJigControl(__in BOOL bOnOff)
// {
// 	// 보드 연결 상태 확인
// 	if (!m_pDigitalIOCtrl->AXTState())
// 	{
// 		m_Log.LogMsg_Err(_T("Disconnect IoBoard....."));
// 		return FALSE;
// 	}
// 
// 	// 안전 센서
// // 	if (m_pDigitalIOCtrl->GetInStatus(DI_Safety) == FALSE && m_pstOption->Inspector.bUseAreaSen_Err == TRUE)
// // 	{
// // 		MessageView(_T("Safety Sensor Detect, Check the Safety Sensor!!"));
// // 		m_Log.LogMsg_Err(_T("Area Sensor Detect !!"));
// // 		return FALSE;
// // 
// 
// // 	if (bOnOff == ON)
// // 	{
// // 		m_Log.LogMsg(_T("Jig Cylinder In Start....."));
// // 
// // 		m_pDigitalIOCtrl->SetOutPort(DO_OutCylinder,	IO_SignalT_SetOff);
// // 		m_pDigitalIOCtrl->SetOutPort(DO_InCylinder,		IO_SignalT_SetOn);
// // 
// // 		if (IsReadyOk(DI_InCylinder) == TRUE)
// // 		{
// // 			m_Log.LogMsg(_T("Jig Cylinder In End....."));
// // 			return TRUE;
// // 		}
// // 		else
// // 		{
// // 			m_Log.LogMsg(_T("Jig Cylinder In End Error....."));
// // 			return FALSE;
// // 		}
// // 	}
// // 	else if (bOnOff == OFF)
// // 	{
// // 		m_Log.LogMsg(_T("Jig Cylinder Out Start....."));
// // 
// // 		m_pDigitalIOCtrl->SetOutPort(DO_OutCylinder, IO_SignalT_SetOn);
// // 		m_pDigitalIOCtrl->SetOutPort(DO_InCylinder, IO_SignalT_SetOff);
// // 
// // 		if (IsReadyOk(DI_OutCylinder) == TRUE)
// // 		{
// // 			m_Log.LogMsg(_T("Jig Cylinder Out End....."));
// // 			return TRUE;
// // 		}
// // 		else
// // 		{
// // 			m_Log.LogMsg(_T("Jig Cylinder Out End Error....."));
// // 			return FALSE;
// // 		}
// 
// 	return TRUE;
// }


BOOL CMotionSequence::ModuleFixUnFixCYLMotion(__in BOOL bFixUnFix)
{
	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
	{
		m_Log.LogMsg_Err(_T("Disconnect IoBoard....."));
		return FALSE;
	}

	m_pDigitalIOCtrl->SetOutPort(DO_ModuleFixCYL, IO_SignalT_SetOff);
	m_pDigitalIOCtrl->SetOutPort(DO_ModuleUnFixCYL, IO_SignalT_SetOff);

	if (bFixUnFix == FIX)
	{
		m_Log.LogMsg(_T("Module Fix Cylinder Start....."));

		m_pDigitalIOCtrl->SetOutPort(DO_ModuleFixCYL, IO_SignalT_SetOn);

		if (IsReadyOk(DI_ModuleFixCYLSensor) == TRUE)
		{
			m_Log.LogMsg(_T("Module Fix Cylinder End....."));
			return TRUE;
		}
		else
		{
			m_Log.LogMsg(_T("Module Fix Cylinder End Error....."));
			return FALSE;
		}
	}
	else if (bFixUnFix == UNFIX)
	{
		m_Log.LogMsg(_T("Module UnFix Cylinder Start....."));

		m_pDigitalIOCtrl->SetOutPort(DO_ModuleUnFixCYL, IO_SignalT_SetOn);

		if (IsReadyOk(DI_ModuleUnfixCYLSensor) == TRUE)
		{
			m_Log.LogMsg(_T("Module UnFix Cylinder End....."));
			return TRUE;
		}
		else
		{
			m_Log.LogMsg(_T("Module UnFix Cylinder End Error....."));
			return FALSE;
		}
	}

	return TRUE;
}


BOOL CMotionSequence::PCBFixUnFixCYLMotion(__in BOOL bFixUnFix)
{
	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
	{
		m_Log.LogMsg_Err(_T("Disconnect IoBoard....."));
		return FALSE;
	}

	m_pDigitalIOCtrl->SetOutPort(DO_PCBFixCYL, IO_SignalT_SetOff);
	m_pDigitalIOCtrl->SetOutPort(DO_PCBUnFixCYL, IO_SignalT_SetOff);

	if (bFixUnFix == FIX)
	{
		m_Log.LogMsg(_T("PCB Fix Cylinder Start....."));

		m_pDigitalIOCtrl->SetOutPort(DO_PCBFixCYL, IO_SignalT_SetOn);

		if (IsReadyOk(DI_PCBFixCYLSensor) == TRUE)
		{
			m_Log.LogMsg(_T("PCB Fix Cylinder End....."));
			return TRUE;
		}
		else
		{
			m_Log.LogMsg(_T("PCB Fix Cylinder End Error....."));
			return FALSE;
		}
	}
	else if (bFixUnFix == UNFIX)
	{
		m_Log.LogMsg(_T("PCB UnFix Cylinder Start....."));

		m_pDigitalIOCtrl->SetOutPort(DO_PCBUnFixCYL, IO_SignalT_SetOn);

		if (IsReadyOk(DI_PCBUnFixCYLSensor) == TRUE)
		{
			m_Log.LogMsg(_T("PCB UnFix Cylinder End....."));
			return TRUE;
		}
		else
		{
			m_Log.LogMsg(_T("PCB UnFix Cylinder End Error....."));
			return FALSE;
		}
	}

	return TRUE;
}


BOOL CMotionSequence::DriverInOutCYLMotion(__in BOOL bFixUnFix)
{
	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
	{
		m_Log.LogMsg_Err(_T("Disconnect IoBoard....."));
		return FALSE;
	}
	
	m_pDigitalIOCtrl->SetOutPort(DO_DriverInCYL, IO_SignalT_SetOff);
	m_pDigitalIOCtrl->SetOutPort(DO_DriverOutCYL, IO_SignalT_SetOff);

	
// 	if (m_pDigitalIOCtrl->GetInStatus(DI_PCBFixCYLSensor) == TRUE)
// 	{
// 		return TRUE;
// 	}

	
	if (bFixUnFix == FIX)
	{
		m_Log.LogMsg(_T("Driver In Cylinder Start....."));

		m_pDigitalIOCtrl->SetOutPort(DO_DriverInCYL, IO_SignalT_SetOn);

		if (IsReadyOk(DI_DriverInCYLSensor) == TRUE)
		{
			m_Log.LogMsg(_T("Driver In Cylinder End....."));
			return TRUE;
		}
		else
		{
			m_Log.LogMsg(_T("Driver In Cylinder End Error....."));
			return FALSE;
		}
	}
	else if (bFixUnFix == UNFIX)
	{
		m_Log.LogMsg(_T("Driver Out Cylinder Start....."));

		m_pDigitalIOCtrl->SetOutPort(DO_DriverOutCYL, IO_SignalT_SetOn);

		if (IsReadyOk(DI_DriverOutCYLSensor) == TRUE)
		{
			m_Log.LogMsg(_T("Driver Out Cylinder End....."));
			return TRUE;
		}
		else
		{
			m_Log.LogMsg(_T("Driver Out Cylinder End Error....."));
			return FALSE;
		}
	}

	return TRUE;
}

BOOL CMotionSequence::ChartOnoff(__in BOOL bOnOff)
{
	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
	{
		m_Log.LogMsg_Err(_T("Disconnect IoBoard....."));
		return FALSE;
	}

	if (bOnOff == ON)
	{
		m_Log.LogMsg(_T("Chart On....."));

		m_pDigitalIOCtrl->SetOutPort(DO_ChartLeft, IO_SignalT_SetOn);
		m_pDigitalIOCtrl->SetOutPort(DO_ChartCenter, IO_SignalT_SetOn);
		m_pDigitalIOCtrl->SetOutPort(DO_ChartRight, IO_SignalT_SetOn);
	}
	else if (bOnOff == OFF)
	{
		m_Log.LogMsg(_T("Chart Off....."));

		m_pDigitalIOCtrl->SetOutPort(DO_ChartLeft, IO_SignalT_SetOff);
		m_pDigitalIOCtrl->SetOutPort(DO_ChartCenter, IO_SignalT_SetOff);
		m_pDigitalIOCtrl->SetOutPort(DO_ChartRight, IO_SignalT_SetOff);
	}

	return TRUE;
}

BOOL CMotionSequence::ReleaseCYLMotion()
{
	if (!m_pDigitalIOCtrl->AXTState())
	{
		m_Log.LogMsg_Err(_T("Disconnect IoBoard..."));
		return FALSE;
	}

	// PCB Un Fix
	PCBFixUnFixCYLMotion(UNFIX);

	// Driver Out
	DriverInOutCYLMotion(OFF);

	// Module Un Fix
	ModuleFixUnFixCYLMotion(UNFIX);


	return TRUE;
}

//=============================================================================
// Method		: TestMotionSequence
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/8/13 - 10:28
// Desc.		:
//=============================================================================
// BOOL CMotionSequence::TestMotionSequence(__in BOOL bOnOff)
// {
// 	// 보드 연결 상태 확인
// 	if (!m_pDigitalIOCtrl->AXTState())
// 	{
// 		m_Log.LogMsg_Err(_T("Disconnect IoBoard..."));
// 		return FALSE;
// 	}
// 
// 	// 안전 센서
// // 	if (m_pDigitalIOCtrl->GetInStatus(DI_Safety) == FALSE && m_pstOption->Inspector.bUseAreaSen_Err == TRUE)
// // 	{
// // 		MessageView(_T("Safety Sensor Detect, Check the Safety Sensor!!"));
// // 		m_Log.LogMsg_Err(_T("Area Sensor Detect !!"));
// // 		return FALSE;
// // 
// 
// 	return TRUE;
// }

//=============================================================================
// Method		: ButtonLampControl
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nPort
// Parameter	: __in enum_IO_SignalType enType
// Qualifier	:
// Last Update	: 2017/9/18 - 11:46
// Desc.		:
//=============================================================================
BOOL CMotionSequence::ButtonLampControl(__in UINT nPort, __in enum_IO_SignalType enType)
{
	CString szMassage;

	switch (nPort)
	{
	case DO_ModuleFixBtnLamp:
		szMassage.Format(_T("Module Fix Button Lamp"));
		break;

	case DO_ModuleUnFixBtnLamp:
		szMassage.Format(_T("Module UnFix Button Lamp"));
		break;

	case DO_PCBFixBtnLamp:
		szMassage.Format(_T("PCB Fix Button Lamp"));
		break;

	case DO_PCBUnFixBtnLamp:
		szMassage.Format(_T("PCB UnFix Button Lamp"));
		break;

	case DO_DriverInBtnLamp:
		szMassage.Format(_T("Driver In Button Lamp"));
		break;

	case DO_DriverOutBtnLamp:
		szMassage.Format(_T("Driver Out Button Lamp"));
		break;

	case DO_StartBtnL_Lamp:
		szMassage.Format(_T("Start Button Lamp"));
		break;

	case DO_StartBtnR_Lamp:
		szMassage.Format(_T("Stop Button Lamp"));
		break;

	default:
		break;
	}

	if (m_pDigitalIOCtrl->SetOutPort(nPort, enType) == FALSE)
	{
		szMassage = szMassage + _T("Error");
		m_Log.LogMsg(szMassage);
		return FALSE;
	}

	szMassage = szMassage + _T("Ok");
	m_Log.LogMsg(szMassage);
	return TRUE;
}

//=============================================================================
// Method		: TowerLamp
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nColor
// Parameter	: __in BOOL bMode
// Parameter	: long lTime
// Qualifier	:
// Last Update	: 2017/9/9 - 15:11
// Desc.		:
//=============================================================================
BOOL CMotionSequence::TowerLamp(UINT nColor, __in BOOL bMode, long lTime /*= 2000*/)
{
	enum_IO_SignalType enType;

	if (m_pDigitalIOCtrl == NULL)
		return FALSE;

	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
		return FALSE;

	// 점멸 기능으로 동작
	if (bMode == ON)
		enType = IO_SignalT_SetOn;

	if (bMode == OFF)
		enType = IO_SignalT_SetOff;

	for (UINT n = 0; n < 3; n++)
	{
		m_pDigitalIOCtrl->SetOutPort(DO_TowerLampRed + n, IO_SignalT_SetOff);
	}

	if (m_pDigitalIOCtrl->SetOutPort(DO_TowerLampRed + nColor, enType) == FALSE)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: TowerLampBuzzer
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nSound
// Parameter	: __in BOOL bMode
// Parameter	: long lTime
// Qualifier	:
// Last Update	: 2017/9/9 - 15:11
// Desc.		:
//=============================================================================
BOOL CMotionSequence::TowerLampBuzzer(UINT nSound, __in BOOL bMode, long lTime /*= 2000*/)
{
	enum_IO_SignalType enType;

	if (m_pDigitalIOCtrl == NULL)
		return FALSE;

	// 보드 연결 상태 확인
	if (!m_pDigitalIOCtrl->AXTState())
		return FALSE;

	// 점멸 기능으로 동작
	if (bMode == ON)
		enType = IO_SignalT_PulseOn;

	if (bMode == OFF)
		enType = IO_SignalT_PulseOff;

	//for (UINT n = 0; n < 5; n++)
	{
		m_pDigitalIOCtrl->SetOutPort(DO_TowerLampBuzzer, IO_SignalT_PulseOff);
	}

	if (m_pDigitalIOCtrl->SetOutPort(nSound, enType) == FALSE)
		return FALSE;

	return TRUE;
}


//=============================================================================
// Method		: WarringView
// Access		: public  
// Returns		: BOOL
// Parameter	: __in CString szText
// Qualifier	:
// Last Update	: 2017/7/12 - 10:56
// Desc.		:
//=============================================================================
BOOL CMotionSequence::MessageView(__in CString szText)
{
	BOOL bResult = FALSE;

	m_Dlg_Popup.PopupMessage(szText);
	if (m_Dlg_Popup.DoModal() == IDOK)
	{
		bResult = TRUE;
	}

	return bResult;
}