﻿#include "stdafx.h"
#include "TI_Collet.h"
#pragma warning( disable : 4244)

#define PHI 3.141592

CTI_Collet::CTI_Collet()
{

}

CTI_Collet::~CTI_Collet()
{
	
}



UINT CTI_Collet::TestColletAngle(unsigned char *Original_image, int nWidth, int nHeight, __in ST_VisionOptInfo* pstVisionOpt, __out double& dResultDegree)
{
	UINT nResult = 1;

	double	Error_Thershold = pstVisionOpt->dLensCTRatio;
	double	Error_Dist_Min = pstVisionOpt->dHoleLengthMin;
	double	Error_Dist_Max = pstVisionOpt->dHoleLengthMax;
	double	Error_Step = pstVisionOpt->dErrorStep;
	int		ColletType = pstVisionOpt->iColletType;
	int		NN = pstVisionOpt->iHoleCount;
	int		Threshold1 = pstVisionOpt->iSearchArea;
	int		Threshold2 = pstVisionOpt->iBinaryData;

	m_MODEL = ColletType;
	CvPoint Up_Collet, Down_Collet;

	double resultDegree = 0.0;
	double Degree_Collet = 999.0;

	CvPoint Temp_Cen;

	ORIImage_H = nHeight;
	ORIImage_W = nWidth;

	int sec, min, hour;
	CTime thetime = CTime::GetCurrentTime();
	hour = thetime.GetHour();
	min = thetime.GetMinute();
	sec = thetime.GetSecond();

	CStringA currDate, currDate2;

	currDate2.Format("D:\\비전결과\\OrgImage_%d_%d_%d.jpg", hour, min, sec);

	char fontBuf[200] = { 0, };

	current_sec = (double)clock() / CLOCKS_PER_SEC;
	end_sec = 0.0;

	Match_INFlag = FALSE; // 티칭모드로 선택시 자동으로 TRUE로 변함
	Match_F_IN = TRUE; //TRUE이면 티칭 모드 , FALSE이면 검사 모드. 

	char szName[MAX_PATH] = { 0, };

	//CvFont* font = new CvFont;
	//cvInitFont(font, CV_FONT_VECTOR0, 3.0f, 3.0f, 0, 3);

	CvFont* font2 = new CvFont;
	cvInitFont(font2, CV_FONT_VECTOR0, 1.0f, 1.0f, 0, 2);

	//기초 공사
	m_RGBResultImage = cvCreateImage(cvSize(ORIImage_W, ORIImage_H), IPL_DEPTH_8U, 1); // ROI 원본 저장용
	Convert_Image = cvCreateImage(cvSize(ORIImage_W, ORIImage_H), IPL_DEPTH_8U, 3);

	for (int y = 0; y < ORIImage_H; y++)
	{
		for (int x = 0; x < ORIImage_W; x++)
		{
			Convert_Image->imageData[y * Convert_Image->widthStep + (x * 3) + 0] = Original_image[y * (ORIImage_W * 4) + (x * 4) + 0];
			Convert_Image->imageData[y * Convert_Image->widthStep + (x * 3) + 1] = Original_image[y * (ORIImage_W * 4) + (x * 4) + 1];
			Convert_Image->imageData[y * Convert_Image->widthStep + (x * 3) + 2] = Original_image[y * (ORIImage_W * 4) + (x * 4) + 2];
		}
	}

	cvCvtColor(Convert_Image, m_RGBResultImage, CV_RGB2GRAY);
	srcImage = (IplImage*)cvClone(m_RGBResultImage);
	edgeImage = cvCreateImage(cvGetSize(srcImage), IPL_DEPTH_8U, CV_THRESH_BINARY);
	dstImage = cvCreateImage(cvGetSize(srcImage), IPL_DEPTH_8U, 3);

	SYSTEMTIME CurrTime;
	GetSystemTime(&CurrTime);
	int Year = CurrTime.wYear;
	int Month = CurrTime.wMonth;
	int Day = CurrTime.wDay;
	int Hour = CurrTime.wHour;
	int Minute = CurrTime.wMinute;
	int Second = CurrTime.wSecond;

	CStringA strPath;

	strPath.Format("D:\\Test\\%04d_%02d_%02d_%02d_%02d_%02d.bmp",
		Year, Month, Day, Hour, Minute, Second);

	cvSaveImage(strPath.GetBuffer(), Convert_Image);

	cvCopyImage(Convert_Image, dstImage);
	storage = cvCreateMemStorage(0);

	//if (ColletType == 0) 
	//{
	//	// 탐색선 반경
	//	Threshold1 = 290;
	//	Threshold2 = 120; // 이진화 한계치
	//	NN = 4;
	//	Error_Dist_Min = 48; // Collet 홈의 지름 길이 최소값 (양품 기준)
	//	Error_Dist_Max = 60; // Collet 홈의 지름 길이 최대값 (양품 기준)

	//}
	//else if (ColletType == 1)
	//{
	//	Threshold1 = 240;
	//	Threshold2 = 50;
	//	NN = 3;
	//	Error_Dist_Min = 25; // Collet 홈의 지름 길이 최소값 (양품 기준)
	//	Error_Dist_Max = 40; // Collet 홈의 지름 길이 최대값 (양품 기준)
	//}
	//else if (ColletType == 2) 
	//{
	//	Threshold1 = 292;
	//	Threshold2 = 100;
	//	NN = 4;
	//	Error_Dist_Min = 55; // Collet 홈의 지름 길이 최소값 (양품 기준)
	//	Error_Dist_Max = 73; // Collet 홈의 지름 길이 최대값 (양품 기준)
	//}

	if (Match_F_IN != 0)
	{
		matching_Image();
	}

	CvPoint MatchingCenter_Point;
	MatchingCenter_Point.x = 0;
	MatchingCenter_Point.y = 0;
	Temp_Cen.x = 0;
	Temp_Cen.y = 0;
	BOOL Match_Flag = FALSE;
	BOOL Fin_Center = FALSE;
	if (Match_INFlag)
	{
		// Teach한 이미지가 없을 경우 예외처리
		cvRectangle(dstImage, Matching_Point, cvPoint(Matching_Point.x + Teaching_Image->width - 2, Matching_Point.y + Teaching_Image->height), CV_RGB(0, 255, 0), 3);

		Temp_Cen.x = Matching_Point.x + Teaching_Image->width / 2;
		Temp_Cen.y = Matching_Point.y + Teaching_Image->height / 2;

		if (ColletType == 2){
			Temp_Cen.x += 3;
			Temp_Cen.y += 2;
		}

		cvThreshold(m_RGBResultImage, m_RGBResultImage, Threshold2, 255, CV_THRESH_BINARY);
		cvSaveImage("C:\\Test\\Test_Binary.bmp", m_RGBResultImage);

		cvCanny(m_RGBResultImage, edgeImage, 100, 200);
		cvSaveImage("C:\\Test\\Test_edge.bmp", edgeImage);
		storage_Circle = cvCreateMemStorage(0);

		cvSmooth(dstImage, dstImage);
		cvSmooth(dstImage, dstImage);
		cvSmooth(dstImage, dstImage);

		if (ColletType == 1){
// 			cvSmooth(dstImage, dstImage);
// 			cvSmooth(dstImage, dstImage);
// 			cvSmooth(dstImage, dstImage);
		}
		else if (ColletType == 2)
		{
			cvErode(dstImage, dstImage);
		}

		Image_Convert(dstImage);
		cvLine(dstImage, cvPoint(Temp_Cen.x - Threshold1, Temp_Cen.y), cvPoint(Temp_Cen.x + Threshold1, Temp_Cen.y), CV_RGB(0, 255, 0), 2);
		cvLine(dstImage, cvPoint(Temp_Cen.x, Temp_Cen.y - Threshold1), cvPoint(Temp_Cen.x, Temp_Cen.y + Threshold1), CV_RGB(0, 255, 0), 2);
		
		//if (ColletType != 1)
		{
			//if (1){

			///////////////////////////khk Insert/////////////////////////////////////////////////////////			
			double r = (double)Threshold1;
			int n = (int)(2.0 * 3.141592 * r  * 2.0);

			int *Data = new int[n];
			int *Diff_1 = new int[n];//1차 미분
			int n_diff = 2 * 2;				// 중심 [앞/뒤]로 빼기할 Data 의 개수

			//		int Data1[3649] = { 0 };
			double Step_Angle = 2.0 * 3.141592 / (double)n;

			CvPoint Now;
			for (int i = 0; i < n; i++){
				Now.x = Temp_Cen.x + r * cos(i*Step_Angle);
				Now.y = Temp_Cen.y + r * sin(i*Step_Angle);
				if (Now.x < 0 || Now.y < 0){// 메모리 Index가 음수가 되는 것을 방지 함.
					Now.x = 0;	Now.y = 0;
				}
				Data[i] = Data_Static[Now.x][Now.y];
// 				if (ColletType == 0)
// 				{
// 					if (Data_Static[Now.x][Now.x] > Threshold2) Data[i] = 255; // ColletType 1의 경우 Black 영역이 강해서 나머지는 White로 만듬.
// 				}
				//Data1[i] = Data[i];
			}

			int Max_Val = 0, Index_max = 0;
			int Min_Val = 9999, Index_min = 0;

			for (int i = 0; i < n; i++){ // 1차 미분하여 Diff_1에 저장함.
				Diff_1[i] = 0;
				for (int j = 0; j < n_diff; j++){
					Diff_1[i] += Data[(i + j + 1) % n];
					Diff_1[i] -= Data[(n + i - (j + 1)) % n];
				}
				Diff_1[i] = (int)((double)Diff_1[i] / (double)n_diff);

				//	if (ColletType == 1) Diff_1[i] *= (-1); // 모델이 1일때는 밝기를 반전해야 함. 

				if (Max_Val < Diff_1[i]){ // 1차 미분값 찾는 동안 미분값의 최대값을 동시에 찾음.
					Max_Val = Diff_1[i];
					Index_max = i;
				}
				// Data1[i] = Data[i];
			}

			int Start = 0, End = 0;
			int Max[4] = { 0 };
			int Min[4] = { 0 };
			int Sector = 0;
			for (Sector = 0; Sector < NN; Sector++){
				Start = Index_max + Sector*n / NN + n / (NN * 2);
				End = Index_max + Sector*n / NN + n * 3 / (NN * 2);

				Max_Val = 0; Min_Val = 9999;
				for (int i = Start; i < End; i++){ // 1차 미분하여 Diff_1에 저장함.
					int i_Current = i%n;
					if (Max_Val < Diff_1[i_Current]){ // 각 Sector의 최대값 찾음.
						Max_Val = Diff_1[i_Current];
						Max[Sector] = i_Current;
					}
					if (Min_Val > Diff_1[i_Current]){ // 각 Sector의 최소값을 찾음.
						Min_Val = Diff_1[i_Current];
						Min[Sector] = i_Current;
					}
				}
			}

			delete[]Data;
			delete[]Diff_1;

			CvPoint MaxP[4], MinP[4];// 찾은 인덱스 위치들을 x,y 좌표로 변환하여 저장하기 위한 위치 변수

			for (int i = 0; i < NN; i++){ // Collet 홈 위치를 좌표값으로 변화 & 화면에 표시
				MaxP[i].x = Temp_Cen.x + r * cos(Max[i] * Step_Angle);
				MaxP[i].y = Temp_Cen.y + r * sin(Max[i] * Step_Angle);

				MinP[i].x = Temp_Cen.x + r * cos(Min[i] * Step_Angle);
				MinP[i].y = Temp_Cen.y + r * sin(Min[i] * Step_Angle);

				cvLine(dstImage, MaxP[i], MinP[i], CV_RGB(0, 255, 255), 8);
			}

			/////////////////////오류 검출 1///////////////////////////////////////////
			int Step = 0;
			CvPoint LT, RB;
			LT.x = Temp_Cen.x - 16; LT.y = Temp_Cen.y - 16;
			RB.x = Temp_Cen.x + 16; RB.y = Temp_Cen.y + 16;

			for (int i = 0; i < NN; i++){  // 오류 검출 : 최대점이 균등하게 분포하고 있는 지 확인(전체 n개의 Point 중에서, 1/3 또는 1/4 위치마다 한개씩 찾아 졌는가? 확인
				if (Max[i + 1] > Max[i]){
					Step = Max[i + 1] - (Max[i] + n / NN);
				}
				else{
					Step = (Max[i + 1] + n) - (Max[i] + n / NN);
				}

				if (Step > Error_Step){ // Error Detect
					cvRectangle(dstImage, LT, RB, CV_RGB(255, 0, 0), 8, BS_SOLID, 0);
					nResult = 0;
				}

				if (Max[i + 1] > Max[i]){
					Step = Max[i + 1] - (Max[i] + n / NN);
				}
				else{
					Step = (Max[i + 1] + n) - (Max[i] + n / NN);
				}

				if (Step > Error_Step){ // Error Detect
					cvRectangle(dstImage, LT, RB, CV_RGB(255, 0, 0), 8, BS_SOLID, 0);
					nResult = 0;
				}
			}

			/////////////////////오류 검출 2///////////////////////////////////////////
			if (NN == 4){ // 오류 검출 : Collet 홈이 4개 일때 오류 검출
				CvPoint CheckP;
				double Distance = 0;
				double Delta_X = 0;
				double Delta_Y = 0;
				for (int i = 0; i < 2; i++){
					CheckP.x = (MaxP[i].x + MaxP[i + 2].x) / 2;
					CheckP.y = (MaxP[i].y + MaxP[i + 2].y) / 2;
					Delta_X = (double)(CheckP.x - Temp_Cen.x);
					Delta_Y = (double)(CheckP.y - Temp_Cen.y);
					Distance = sqrt(pow(Delta_X, 2) + pow(Delta_Y, 2));

					if (Distance>Error_Thershold){ // Error Detect
						cvLine(dstImage, CheckP, CheckP, CV_RGB(255, 0, 0), 8);
						cvCircle(dstImage, CheckP, 20, CV_RGB(255, 0, 0), 4, BS_SOLID, 0);
						nResult = 0;
					}
				}

				for (int i = 0; i < 2; i++){
					CheckP.x = (MinP[i].x + MinP[i + 2].x) / 2;
					CheckP.y = (MinP[i].y + MinP[i + 2].y) / 2;
					Delta_X = (double)(CheckP.x - Temp_Cen.x);
					Delta_Y = (double)(CheckP.y - Temp_Cen.y);
					Distance = sqrt(pow(Delta_X, 2) + pow(Delta_Y, 2));

					if (Distance>Error_Thershold){ // Error Detect
						cvLine(dstImage, CheckP, CheckP, CV_RGB(255, 0, 0), 8);
						cvCircle(dstImage, CheckP, 20, CV_RGB(255, 0, 0), 4, BS_SOLID, 0);
						nResult = 0;
					}
				}
			}

			int Index_Left = 0, Left_x = 9999;
			for (int i = 0; i < NN; i++){
				if (Left_x > MaxP[i].x){
					Left_x = MaxP[i].x;
					Index_Left = i;
				}
			}

			Up_Collet.x = Temp_Cen.x + r * cos(Max[Index_Left] * Step_Angle);
			Up_Collet.y = Temp_Cen.y + r * sin(Max[Index_Left] * Step_Angle);

			Down_Collet.x = Temp_Cen.x + r * cos(Min[Index_Left] * Step_Angle);
			Down_Collet.y = Temp_Cen.y + r * sin(Min[Index_Left] * Step_Angle);

			////////////////////////khk End/////////////////////////////////////////////////////////////////////////////////////////
		}
// 		else
// 		{
// 			int TURN_RIGHT = 0;
// 
// 			double Circle_G = 0.0;
// 			CvPoint MAX_X;
// 			if (TURN_RIGHT == 0) {
// 				MAX_X.x = 9999;
// 				MAX_X.y = 0;
// 			}
// 			else {
// 				MAX_X.x = 0;
// 				MAX_X.y = 0;
// 			}
// 			double Size_Circle_Till = 0.5;
// 
// 			int FLAG_Circle[1600][1200] = { 0, };
// 			for (int x = Temp_Cen.x - Threshold1; x <= Temp_Cen.x + Threshold1; x++) {
// 				for (int y = Temp_Cen.y - Threshold1; y <= Temp_Cen.y + Threshold1; y++) {
// 					Circle_G = sqrt(pow((double)Temp_Cen.x - x, 2) + pow((double)Temp_Cen.y - y, 2));
// 
// 					if (Circle_G >= (Threshold1 - Size_Circle_Till) && Circle_G <= (Threshold1 + Size_Circle_Till)) {
// 						cvLine(dstImage, cvPoint(x, y), cvPoint(x, y), CV_RGB(255, 0, 0), 2);
// 						if (ColletType == 0) {
// 							if (Data_Static[x][y] > Threshold2) {
// 								cvLine(dstImage, cvPoint(x, y), cvPoint(x, y), CV_RGB(0, 0, 255), 2);
// 								FLAG_Circle[x][y] = 1;
// 								if (TURN_RIGHT == 0) {
// 									if (MAX_X.x > x) {
// 										MAX_X.x = x;
// 										MAX_X.y = y;
// 									}
// 								}
// 								else {
// 									if (MAX_X.x < x) {
// 										MAX_X.x = x;
// 										MAX_X.y = y;
// 									}
// 								}
// 							}
// 						}
// 						else if (ColletType == 1){
// 							if (Data_Static[x][y] < Threshold2) {
// 								cvLine(dstImage, cvPoint(x, y), cvPoint(x, y), CV_RGB(0, 0, 255), 2);
// 								FLAG_Circle[x][y] = 1;
// 								if (TURN_RIGHT == 0) {
// 									if (MAX_X.x > x) {
// 										MAX_X.x = x;
// 										MAX_X.y = y;
// 									}
// 								}
// 								else {
// 									if (MAX_X.x < x) {
// 										MAX_X.x = x;
// 										MAX_X.y = y;
// 									}
// 								}
// 							}
// 						}
// 						else if (ColletType == 2){
// 							if (Data_Static[x][y] > Threshold2) {
// 								cvLine(dstImage, cvPoint(x, y), cvPoint(x, y), CV_RGB(0, 0, 255), 2);
// 								FLAG_Circle[x][y] = 1;
// 								if (TURN_RIGHT == 0) {
// 									if (MAX_X.x > x) {
// 										MAX_X.x = x;
// 										MAX_X.y = y;
// 									}
// 								}
// 								else {
// 									if (MAX_X.x < x) {
// 										MAX_X.x = x;
// 										MAX_X.y = y;
// 									}
// 								}
// 							}
// 						}
// 					}
// 				}
// 			}
// 			//cvLine(dstImage, MAX_X,MAX_X, CV_RGB(0, 0, 255), 20);
// 
// 			CvPoint Up_Collet, Down_Collet;
// 			Up_Collet.x = 9999; // Min으로 찾음
// 			Up_Collet.y = 9999;
// 			Down_Collet.x = 0; // Max로 찾음
// 			Down_Collet.y = 0;
// 			int _c = 0;
// 			int _f = 0;
// 
// 			for (int y = MAX_X.y - 100; y <= MAX_X.y + 100; y++) {
// 				if (y < 0)  y = 0;
// 				if (y > ORIImage_H) y = ORIImage_H;
// 				_c = 0;
// 				for (int x = MAX_X.x - 100; x <= MAX_X.x + 100; x++) {
// 					if (x < 0)  x = 0;
// 					if (x > ORIImage_W) x = ORIImage_W;
// 					if (FLAG_Circle[x][y] == 1) {
// 						if (Up_Collet.y > y) {
// 							Up_Collet.y = y;
// 							Up_Collet.x = x;
// 						}
// 						if (Down_Collet.y < y) {
// 							Down_Collet.y = y;
// 							Down_Collet.x = x;
// 
// 						}
// 					}
// 					_c++;
// 					if (_c > 200){
// 						break;
// 					}
// 				}
// 				_f++;
// 				if (_f > 200){
// 					break;
// 				}
// 			}
// 		}
		cvLine(dstImage, Up_Collet, Up_Collet, CV_RGB(0, 255, 255), 8);
		cvLine(dstImage, Down_Collet, Down_Collet, CV_RGB(0, 255, 255), 8);

		double Distance_Collet = sqrt(pow(Up_Collet.x - Down_Collet.x, 2) + pow(Up_Collet.y - Down_Collet.y, 2));
		sprintf(fontBuf, "( %d , %d  , %2.1f ) ", Up_Collet.x, Up_Collet.y, Distance_Collet);
		cvPutText(dstImage, fontBuf, cvPoint(Up_Collet.x + 10, Up_Collet.y + 30), font2, CV_RGB(255, 127, 0));
		///////////////////////////khk Insert/////////////////////////////////////////////////////////
		if (Distance_Collet<Error_Dist_Min || Distance_Collet > Error_Dist_Max)
		{
			// Error Detect
			cvLine(dstImage, Up_Collet, Down_Collet, CV_RGB(255, 0, 0), 8);
			nResult = 0;
		}
		////////////////////////khk End/////////////////////////////////////////////////////////////
		CvPoint Mid_Collet;
		Mid_Collet.x = (Down_Collet.x + Up_Collet.x) / 2.0;
		Mid_Collet.y = (Down_Collet.y + Up_Collet.y) / 2.0;

		cvLine(dstImage, Temp_Cen, Mid_Collet, CV_RGB(255, 0, 0), 8);

		Degree_Collet = atan((double)(Temp_Cen.y - Mid_Collet.y) / (double)(Temp_Cen.x - Mid_Collet.x)) * 180.0 / PHI;

		sprintf(fontBuf, "( %d , %d  , %2.1f` ) ", Temp_Cen.x, Temp_Cen.y, Degree_Collet);
		cvPutText(dstImage, fontBuf, cvPoint(Temp_Cen.x + 10, Temp_Cen.y + 30), font2, CV_RGB(255, 127, 0));


		cvReleaseImage(&Teaching_Image);
	}
	else {
		Percent_max = 0;
	}

	dResultDegree = Degree_Collet;
	//m_pstVisionTestData->Temp_CenX = Temp_Cen.x;
	//m_pstVisionTestData->Temp_CenY = Temp_Cen.y;

	end_sec = (double)clock() / CLOCKS_PER_SEC;
	double Time = end_sec - current_sec;
	sprintf(fontBuf, "Time : %1.3fs", Time);
	cvPutText(dstImage, fontBuf, cvPoint(10, 100), font2, CV_RGB(255, 127, 0));

	sprintf(fontBuf, "Matching Percent : %3.1f ", Percent_max*100.0);
	cvPutText(dstImage, fontBuf, cvPoint(10, 50), font2, CV_RGB(255, 127, 0));

// 	sprintf(fontBuf, Algo_Ver);
// 	cvPutText(dstImage, fontBuf, cvPoint(1100, 1100), font2, CV_RGB(0, 120, 255));
// 	sprintf(fontBuf, Ver_Date);
// 	cvPutText(dstImage, fontBuf, cvPoint(1100, 1140), font2, CV_RGB(0, 120, 255));

	cvLine(dstImage, cvPoint(0, ORIImage_H / 2.0), cvPoint(ORIImage_W, ORIImage_H / 2.0), CV_RGB(255, 0, 0), 1);
	cvLine(dstImage, cvPoint(ORIImage_W / 2.0, 0), cvPoint(ORIImage_W / 2.0, ORIImage_H), CV_RGB(255, 0, 0), 1);


	CStringA strPathResult;

	strPathResult.Format("C:\\Test\\%04d_%02d_%02d_%02d_%02d_%02d_Result.jpg",
		Year, Month, Day, Hour, Minute, Second);

	cvSaveImage(strPathResult.GetBuffer(), dstImage);
	//cvSaveImage(ValuePathResult, dstImage);

	resultDegree = 0;

	//currDate.Format("D:\\비전결과\\ResultImage_%d_%d_%d.jpg", hour, min, sec);

	//USES_CONVERSION;

	delete font2;

	//	cvCopyImage(result_Image, resultImage);


	cvReleaseMemStorage(&storage);
	cvReleaseMemStorage(&storage_Circle);

	cvReleaseImage(&m_RGBResultImage);
	cvReleaseImage(&srcImage);
	cvReleaseImage(&edgeImage);
	cvReleaseImage(&dstImage);
	cvReleaseImage(&Convert_Image);

	// 	if (Degree_Collet > 360 || Degree_Collet < -360)
	// 		return FALSE;
	// 	else
	// 		return TRUE;

	return nResult;
}

void CTI_Collet::Image_Convert(IplImage* orgImage)
{
	int x, y;
	int Max_x = ORIImage_W;
	int Max_y = ORIImage_H;

	BYTE B, G, R;
	double Y = 0;

	for (x = 0; x < Max_x; x++) {
		for (y = 0; y < Max_y; y++) {
			B = orgImage->imageData[y * orgImage->widthStep + x * 3 + 0];
			G = orgImage->imageData[y * orgImage->widthStep + x * 3 + 1];
			R = orgImage->imageData[y * orgImage->widthStep + x * 3 + 2];
			Y = ((0.29900*R) + (0.58700*G) + (0.11400*B));
			Data_Static[x][y] = Y;
		}
	}
}

void CTI_Collet::matching_Image()
{
	Percent_min = 0, 0;
	Percent_max = 0, 0;
	Match_INFlag = TRUE;

	if (m_MODEL == 1) {
		Teaching_Image = cvLoadImage("C:\\Collet_TEACH\\2.bmp", CV_LOAD_IMAGE_GRAYSCALE);
	}
	else if (m_MODEL == 0){
		Teaching_Image = cvLoadImage("C:\\Collet_TEACH\\1.bmp", CV_LOAD_IMAGE_GRAYSCALE);
	}
	else if (m_MODEL == 2){
		Teaching_Image = cvLoadImage("C:\\Collet_TEACH\\3.bmp", CV_LOAD_IMAGE_GRAYSCALE);
	}

	if (Teaching_Image == NULL) {
		AfxMessageBox(_T("ERR 01 :티칭 이미지가 없습니다. \n 경로 : C:\\Collet_TEACH"));
	}
	else {
		IplImage* Matching_Result = cvCreateImage(cvSize(srcImage->width - Teaching_Image->width + 1, srcImage->height - Teaching_Image->height + 1), IPL_DEPTH_32F, 1);

		cvMatchTemplate(srcImage, Teaching_Image, Matching_Result, CV_TM_CCOEFF_NORMED);
		cvMinMaxLoc(Matching_Result, &Percent_min, &Percent_max, NULL, &Matching_Point);
		/* 이미지 매칭이 현저히 떨어질 경우 90도 돌린 이미지로 검사 진행*/
		if (Percent_max < 0.3) {
			Teaching_Image = cvLoadImage("C:\\Collet_TEACH\\1.bmp", CV_LOAD_IMAGE_GRAYSCALE);
			if (Teaching_Image == NULL) {
				AfxMessageBox(_T("ERR 01 :티칭 이미지가 없습니다. \n 경로 : C:\\Collet_TEACH\\1.bmp"));
			}
			else {
				Matching_Result = cvCreateImage(cvSize(srcImage->width - Teaching_Image->width + 1, srcImage->height - Teaching_Image->height + 1), IPL_DEPTH_32F, 1);
				cvMatchTemplate(srcImage, Teaching_Image, Matching_Result, CV_TM_CCOEFF_NORMED);
				cvMinMaxLoc(Matching_Result, &Percent_min, &Percent_max, NULL, &Matching_Point);
			}
		}


		// 	cvNamedWindow("Matching_R",CV_WINDOW_NORMAL);
		// 	cvShowImage("Matching_R",Matching_Result);
		// 	cvWaitKey(100);

		cvReleaseImage(&Matching_Result);
	}
}

//
////=============================================================================
//// Method		: Image_Convert
//// Access		: public  
//// Returns		: void
//// Parameter	: __in IplImage * ipOrgImage
//// Parameter	: __out int * * iArrDataY
//// Qualifier	:
//// Last Update	: 2017/10/11 - 17:53
//// Desc.		:
////=============================================================================
//void CTI_Collet::Image_Convert(__in IplImage *ipOrgImage, __out int** iArrDataY)
//{
//	if (ipOrgImage != NULL)
//	{
//		int iWidth		= ipOrgImage->width;
//		int iHeight		= ipOrgImage->height;
//		int iWidthStep	= 0;
//		double dY		= 0;
//
//		BYTE B, G, R;
//
//		for (int x = 0; x < iWidth; x++) 
//		{
//			for (int y = 0; y < iHeight; y++) 
//			{
//				B = ipOrgImage->imageData[y * ipOrgImage->widthStep + x * 3 + 0];
//				G = ipOrgImage->imageData[y * ipOrgImage->widthStep + x * 3 + 1];
//				R = ipOrgImage->imageData[y * ipOrgImage->widthStep + x * 3 + 2];
//				dY = ((0.29900*R) + (0.58700*G) + (0.11400*B));
//
//				iArrDataY[x][y] = dY;
//			}
//			//iWidthStep += ipOrgImage->widthStep;
//		}
//	}
//}
//
////=============================================================================
//// Method		: Matching_ColletImage
//// Access		: public  
//// Returns		: BOOL
//// Parameter	: __in IplImage * ipSrcImage
//// Parameter	: __in int iColletType
//// Parameter	: __out CvPoint & MatchingPoint
//// Parameter	: __out double & dPercent_min
//// Parameter	: __out double & dPercent_max
//// Qualifier	:
//// Last Update	: 2017/10/11 - 17:53
//// Desc.		:
////=============================================================================
//BOOL CTI_Collet::Matching_ColletImage(__in IplImage* ipSrcImage, __in int iColletType, __out CvPoint& MatchingPoint, __out double& dPercent_min, __out double& dPercent_max, __out int& iMatchWidth, __out int& iMatchHeight)
//{
//	BOOL bResult = FALSE;
//
//	dPercent_min = 0;
//	dPercent_max = 0;
//
//	IplImage* ipTeachImage = NULL;
//
//	CString strFileName;
//	strFileName.Format(_T("C:\\Collet_TEACH\\ColletImage_%d.bmp"), iColletType);
//	ipTeachImage = cvLoadImage((CStringA)strFileName, CV_LOAD_IMAGE_GRAYSCALE);
//
//	if (ipTeachImage != NULL)
//	{
//		IplImage* ipMatching_Result = cvCreateImage(cvSize(ipSrcImage->width - ipTeachImage->width + 1, ipSrcImage->height - ipTeachImage->height + 1), IPL_DEPTH_32F, 1);
//
//		iMatchWidth = ipTeachImage->width;
//		iMatchHeight = ipTeachImage->height;
//
//		cvMatchTemplate(ipSrcImage, ipTeachImage, ipMatching_Result, CV_TM_CCOEFF_NORMED);
//		cvMinMaxLoc(ipMatching_Result, &dPercent_min, &dPercent_max, NULL, &MatchingPoint);
//
//		/* 이미지 매칭이 현저히 떨어질 경우 90도 돌린 이미지로 검사 진행*/
//		if (dPercent_max < 0.3)
//		{
//			ipTeachImage = cvLoadImage("C:\\Collet_TEACH\\ColletImage_%d.bmp", CV_LOAD_IMAGE_GRAYSCALE);
//			
//			if (ipTeachImage != NULL)
//			{
//				ipMatching_Result = cvCreateImage(cvSize(ipSrcImage->width - ipTeachImage->width + 1, ipSrcImage->height - ipTeachImage->height + 1), IPL_DEPTH_32F, 1);
//				cvMatchTemplate(ipSrcImage, ipTeachImage, ipMatching_Result, CV_TM_CCOEFF_NORMED);
//				cvMinMaxLoc(ipMatching_Result, &dPercent_min, &dPercent_max, NULL, &MatchingPoint);
//			}
//		}
//
//		cvReleaseImage(&ipMatching_Result);
//
//		bResult = TRUE;
//	}
//
//	return bResult;
//}
//
//
////=============================================================================
//// Method		: TestColletAngle
//// Access		: public  
//// Returns		: BOOL
//// Parameter	: __in LPBYTE Original_image
//// Parameter	: __in UINT nImageW
//// Parameter	: __in UINT nImageH
//// Parameter	: __in ST_VisionOptInfo * pstVisionOpt
//// Parameter	: __out double & dResultDegree
//// Qualifier	:
//// Last Update	: 2017/10/11 - 17:53
//// Desc.		:
////=============================================================================
//BOOL CTI_Collet::TestColletAngle(__in LPBYTE Original_image, __in UINT nImageW, __in UINT nImageH, __in ST_VisionOptInfo* pstVisionOpt, __out double& dResultDegree)
//{
//	if (pstVisionOpt == NULL)
//		return FALSE;
//
//	if (Original_image == NULL)
//		return FALSE;
//
//	BOOL bResult = TRUE;
//	
//	double	dLensCTRatio	= pstVisionOpt->dLensCTRatio;		// 렌즈 중심비
//	double	dHoleLengthMin	= pstVisionOpt->dHoleLengthMin;		// 홀 최소 길이
//	double	dHoleLengthMax	= pstVisionOpt->dHoleLengthMax;		// 홀 최대 길이
//	double	dErrorStep		= pstVisionOpt->dErrorStep;			// 에러 스텝
//	int		iColletType		= pstVisionOpt->iColletType;		// 콜렛 타입
//	int		iHoleCount		= pstVisionOpt->iHoleCount;			// 홀 개수
//	int		iSearchArea		= pstVisionOpt->iSearchArea;		// 탐색 범위
//	int		iBinaryData		= pstVisionOpt->iBinaryData;		// 이진화 수치
//
//	CvPoint Up_Collet;
//	CvPoint Down_Collet;
//	CvPoint Temp_Cen;
//	CvPoint Matching_Point;
//
//	int	iMatchWidth			= 0;
//	int	iMatchHeight		= 0;
//	double dPercent_min		= 0;
//	double dPercent_max		= 0;
//	double dresultDegree	= 0.0;
//	double dDegree_Collet	= 999.0;
//
//	char fontBuf[200]		= { 0, };
//
//
//	int** iArrDataY = new int*[2560];
//	for (int i = 0; i < 2560; i++)
//		iArrDataY[i] = new int[2048];
//
//
//	IplImage* ipBinaryImage		= cvCreateImage(cvSize(nImageW, nImageH), IPL_DEPTH_8U, 1);
//	IplImage* ipSrcImage		= cvCreateImage(cvSize(nImageW, nImageH), IPL_DEPTH_8U, 1);
//	IplImage* ipDstImage		= cvCreateImage(cvSize(nImageW, nImageH), IPL_DEPTH_8U, 3);
//	IplImage* ipEdgeImage		= cvCreateImage(cvSize(nImageW, nImageH), IPL_DEPTH_8U, CV_THRESH_BINARY);
//	IplImage* ipConvertImage	= cvCreateImage(cvSize(nImageW, nImageH), IPL_DEPTH_8U, 3);
//
//	CvFont* font = new CvFont;
//	cvInitFont(font, CV_FONT_VECTOR0, 1.0f, 1.0f, 0, 2);
//
//	for (int y = 0; y < nImageH; y++)
//	{
//		for (int x = 0; x < nImageW; x++)
//		{
//			ipConvertImage->imageData[y * ipConvertImage->widthStep + (x * 3) + 0] = Original_image[y * (nImageW * 4) + (x * 4) + 0];
//			ipConvertImage->imageData[y * ipConvertImage->widthStep + (x * 3) + 1] = Original_image[y * (nImageW * 4) + (x * 4) + 1];
//			ipConvertImage->imageData[y * ipConvertImage->widthStep + (x * 3) + 2] = Original_image[y * (nImageW * 4) + (x * 4) + 2];
//		}
//	}
//
//	cvCvtColor(ipConvertImage, ipBinaryImage, CV_RGB2GRAY);
//	cvCopy(ipBinaryImage, ipSrcImage);
//	cvCopy(ipConvertImage, ipDstImage);
//
//	CString strFileName;
//
//	strFileName.Format(_T("C:\\ColletImage_%d.bmp"), iColletType);
//	cvSaveImage((CStringA)strFileName, ipSrcImage);
//
//	if (Matching_ColletImage(ipSrcImage, iColletType, Matching_Point, dPercent_min, dPercent_max, iMatchWidth, iMatchHeight) == TRUE)
//	{
//		Temp_Cen.x = 0;
//		Temp_Cen.y = 0;
//
//		cvRectangle(ipDstImage, Matching_Point, cvPoint(Matching_Point.x + iMatchWidth - 2, Matching_Point.y + iMatchHeight), CV_RGB(0, 255, 0), 3);
//
//		Temp_Cen.x = Matching_Point.x + iMatchWidth / 2;
//		Temp_Cen.y = Matching_Point.y + iMatchHeight / 2;
//
//// 		if (iColletType == 1)
//// 		{
//// 			Temp_Cen.x += 3;
//// 			Temp_Cen.y += 2;
//// 		}
//		
//		cvThreshold(ipBinaryImage, ipBinaryImage, iBinaryData, 255, CV_THRESH_BINARY);
//		//cvSaveImage("C:\\Test_Binary.bmp", ipBinaryImage);
//
//		cvCanny(ipBinaryImage, ipEdgeImage, 100, 200);
//		//cvSaveImage("C:\\Test_edge.bmp", ipEdgeImage);
//
//		cvSmooth(ipDstImage, ipDstImage);
//		cvSmooth(ipDstImage, ipDstImage);
//		cvSmooth(ipDstImage, ipDstImage);
//
//		if (iColletType == 2)
//		{
//			cvSmooth(ipDstImage, ipDstImage);
//			cvSmooth(ipDstImage, ipDstImage);
//			cvSmooth(ipDstImage, ipDstImage);
//		} 
//		else if (iColletType == 1)
//		{
//			cvErode(ipDstImage, ipDstImage);
//		}
//
//
//		Image_Convert(ipDstImage, iArrDataY);
//		cvLine(ipDstImage, cvPoint(Temp_Cen.x - iSearchArea, Temp_Cen.y), cvPoint(Temp_Cen.x + iSearchArea, Temp_Cen.y), CV_RGB(0, 255, 0), 2);
//		cvLine(ipDstImage, cvPoint(Temp_Cen.x, Temp_Cen.y - iSearchArea), cvPoint(Temp_Cen.x, Temp_Cen.y + iSearchArea), CV_RGB(0, 255, 0), 2);
//		
//		if (iColletType == 0)
//		{
//			double r			= (double)iSearchArea;
//			int n				= (int)(2.0 * 3.141592 * r  * 2.0);
//
//			int *Data			= new int[n];
//			int *Diff_1			= new int[n];			//1차 미분
//			int n_diff			= 2 * 2;				// 중심 [앞/뒤]로 빼기할 Data 의 개수
//			int Max_Val			= 0; 
//			int Min_Val			= 9999; 
//			int Index_max		= 0;
//			int Index_min		= 0;
//			int Start			= 0;
//			int End				= 0;
//			int Max[4]			= { 0, };
//			int Min[4]			= { 0, };
//			int Sector			= 0;
//			int Step			= 0;
//			int Index_Left		= 0;
//			int Left_x			= 9999;
//			double Distance		= 0;
//			double Delta_X		= 0;
//			double Delta_Y		= 0;
//			double Step_Angle	= 2.0 * 3.141592 / (double)n;
//
//			CvPoint CheckP;
//			CvPoint cpNowPoint;
//			CvPoint MaxP[4];	// 찾은 인덱스 위치들을 x,y 좌표로 변환하여 저장하기 위한 위치 변수
//			CvPoint MinP[4];	// 찾은 인덱스 위치들을 x,y 좌표로 변환하여 저장하기 위한 위치 변수
//			CvPoint LT, RB;
//
//			for (int i = 0; i < n; i++)
//			{
//				cpNowPoint.x = Temp_Cen.x + r * cos(i*Step_Angle);
//				cpNowPoint.y = Temp_Cen.y + r * sin(i*Step_Angle);
//
//				if (cpNowPoint.x < 0 || cpNowPoint.y < 0)	// 메모리 Index가 음수가 되는 것을 방지 함.
//				{
//					cpNowPoint.x = 0;	cpNowPoint.y = 0;
//				}
//	
//				Data[i] = iArrDataY[cpNowPoint.x][cpNowPoint.y];
//				
//				if (iColletType == 2)		// ????? --> iColletType 1의 경우 Black 영역이 강해서 나머지는 White로 만듬.
//				{
//					if (iArrDataY[cpNowPoint.x][cpNowPoint.x] > iBinaryData) Data[i] = 255; 
//				}
//			}
//
//			for (int i = 0; i < n; i++)		// 1차 미분하여 Diff_1에 저장함.
//			{ 
//				Diff_1[i] = 0;
//				for (int j = 0; j < n_diff; j++)
//				{
//					Diff_1[i] += Data[(i + j + 1) % n];
//					Diff_1[i] -= Data[(n + i - (j + 1)) % n];
//				}
//
//				Diff_1[i] = (int)((double)Diff_1[i] / (double)n_diff);
//
//				if (Max_Val < Diff_1[i])	// 1차 미분값 찾는 동안 미분값의 최대값을 동시에 찾음.
//				{ 
//					Max_Val = Diff_1[i];
//					Index_max = i;
//				}
//			}
//
//			for (Sector = 0; Sector < iHoleCount; Sector++)
//			{
//				Start	= Index_max + Sector * n / iHoleCount + n / (iHoleCount * 2);
//				End		= Index_max + Sector * n / iHoleCount + n * 3 / (iHoleCount * 2);
//				Max_Val = 0; 
//				Min_Val = 9999;
//
//				for (int i = Start; i < End; i++)		// 1차 미분하여 Diff_1에 저장함.
//				{ 
//					int i_Current = i % n;
//
//					if (Max_Val < Diff_1[i_Current])	// 각 Sector의 최대값 찾음.
//					{ 
//						Max_Val = Diff_1[i_Current];
//						Max[Sector] = i_Current;
//					}
//
//					if (Min_Val > Diff_1[i_Current])	// 각 Sector의 최소값을 찾음.
//					{ 
//						Min_Val = Diff_1[i_Current];
//						Min[Sector] = i_Current;
//					}
//				}
//			}
//
//			delete[]Data;
//			delete[]Diff_1;
//
//			for (int i = 0; i < iHoleCount; i++)		// Collet 홈 위치를 좌표값으로 변화 & 화면에 표시
//			{ 
//				MaxP[i].x = Temp_Cen.x + r * cos(Max[i] * Step_Angle);
//				MaxP[i].y = Temp_Cen.y + r * sin(Max[i] * Step_Angle);
//
//				MinP[i].x = Temp_Cen.x + r * cos(Min[i] * Step_Angle);
//				MinP[i].y = Temp_Cen.y + r * sin(Min[i] * Step_Angle);
//
//				cvLine(ipDstImage, MaxP[i], MinP[i], CV_RGB(0, 255, 255), 8);
//			}
//
//			//----------------------------------------------------------------------- 오류 검출 1 Start -------//
//
//			LT.x = Temp_Cen.x - 16; LT.y = Temp_Cen.y - 16;
//			RB.x = Temp_Cen.x + 16; RB.y = Temp_Cen.y + 16;
//
//			// 오류 검출 : 최대점이 균등하게 분포하고 있는 지 확인(전체 n개의 Point 중에서, 1/3 또는 1/4 위치마다 한개씩 찾아 졌는가? 확인
//			for (int i = 0; i < iHoleCount; i++)	
//			{  
//				if (Max[i + 1] > Max[i])
//				{
//					Step = Max[i + 1] - (Max[i] + n / iHoleCount);
//				}
//				else
//				{
//					Step = (Max[i + 1] + n) - (Max[i] + n / iHoleCount);
//				}
//
//				if ((double)Step > dErrorStep)	// Error Detect
//				{ 
//					cvRectangle(ipDstImage, LT, RB, CV_RGB(255, 0, 0), 8, BS_SOLID, 0);
//					bResult = FALSE;
//				}
//
//				if (Max[i + 1] > Max[i])
//				{
//					Step = Max[i + 1] - (Max[i] + n / iHoleCount);
//				}
//				else
//				{
//					Step = (Max[i + 1] + n) - (Max[i] + n / iHoleCount);
//				}
//
//				if ((double)Step > dErrorStep)		// Error Detect
//				{ 
//					cvRectangle(ipDstImage, LT, RB, CV_RGB(255, 0, 0), 8, BS_SOLID, 0);
//					bResult = FALSE;
//				}
//			}
//
//			//----------------------------------------------------------------------- 오류 검출 1 End -------//
//
//
//			//----------------------------------------------------------------------- 오류 검출 2 Start -------//
//
//			if (iHoleCount == 4)	// 오류 검출 : Collet 홈이 4개 일때 오류 검출
//			{ 
//				for (int i = 0; i < 2; i++)
//				{
//					CheckP.x = (MaxP[i].x + MaxP[i + 2].x) / 2;
//					CheckP.y = (MaxP[i].y + MaxP[i + 2].y) / 2;
//					Delta_X = (double)(CheckP.x - Temp_Cen.x);
//					Delta_Y = (double)(CheckP.y - Temp_Cen.y);
//					Distance = sqrt(pow(Delta_X, 2) + pow(Delta_Y, 2));
//
//					if (Distance > dLensCTRatio)	// Error Detect
//					{ 
//						cvLine(ipDstImage, CheckP, CheckP, CV_RGB(255, 0, 0), 8);
//						cvCircle(ipDstImage, CheckP, 20, CV_RGB(255, 0, 0), 4, BS_SOLID, 0);
//						bResult = FALSE;
//					}
//				}
//
//				for (int i = 0; i < 2; i++)
//				{
//					CheckP.x = (MinP[i].x + MinP[i + 2].x) / 2;
//					CheckP.y = (MinP[i].y + MinP[i + 2].y) / 2;
//					Delta_X = (double)(CheckP.x - Temp_Cen.x);
//					Delta_Y = (double)(CheckP.y - Temp_Cen.y);
//					Distance = sqrt(pow(Delta_X, 2) + pow(Delta_Y, 2));
//
//					if (Distance > dLensCTRatio)	// Error Detect
//					{ 
//						cvLine(ipDstImage, CheckP, CheckP, CV_RGB(255, 0, 0), 8);
//						cvCircle(ipDstImage, CheckP, 20, CV_RGB(255, 0, 0), 4, BS_SOLID, 0);
//						bResult = FALSE;
//					}
//				}
//			}
//
//			for (int i = 0; i < iHoleCount; i++)
//			{
//				if (Left_x > MaxP[i].x)
//				{
//					Left_x = MaxP[i].x;
//					Index_Left = i;
//				}
//			}
//
//			Up_Collet.x		= Temp_Cen.x + r * cos(Max[Index_Left] * Step_Angle);
//			Up_Collet.y		= Temp_Cen.y + r * sin(Max[Index_Left] * Step_Angle);
//			Down_Collet.x	= Temp_Cen.x + r * cos(Min[Index_Left] * Step_Angle);
//			Down_Collet.y	= Temp_Cen.y + r * sin(Min[Index_Left] * Step_Angle);
//
//			//----------------------------------------------------------------------- 오류 검출 2 End -------//
//
//
//
//		}
//		else
//		{
//			int _c						= 0;
//			int _f						= 0;
//			int TURN_RIGHT				= 0;
//			int FLAG_Circle[1600][1200] = { 0, };
//
//			double Circle_G				= 0.0;
//			double Size_Circle_Till		= 0.5;
//
//			CvPoint Up_Collet;
//			CvPoint Down_Collet;
//			CvPoint MAX_X;
//
//			if (TURN_RIGHT == 0) 
//			{
//				MAX_X.x = 9999;
//				MAX_X.y = 0;
//			}
//			else 
//			{
//				MAX_X.x = 0;
//				MAX_X.y = 0;
//			}
//
//			for (int x = Temp_Cen.x - iSearchArea; x <= Temp_Cen.x + iSearchArea; x++)
//			{
//				for (int y = Temp_Cen.y - iSearchArea; y <= Temp_Cen.y + iSearchArea; y++)
//				{
//					Circle_G = sqrt(pow((double)Temp_Cen.x - x, 2) + pow((double)Temp_Cen.y - y, 2));
//
//					if (Circle_G >= (iSearchArea - Size_Circle_Till) && Circle_G <= (iSearchArea + Size_Circle_Till))
//					{
//						cvLine(ipDstImage, cvPoint(x, y), cvPoint(x, y), CV_RGB(255, 0, 0), 2);
//
//						if (iColletType == 0) 
//						{
//							if (iArrDataY[x][y] < iBinaryData) 
//							{
//								cvLine(ipDstImage, cvPoint(x, y), cvPoint(x, y), CV_RGB(0, 0, 255), 2);
//								FLAG_Circle[x][y] = 1;
//
//								if (TURN_RIGHT == 0) 
//								{
//									if (MAX_X.x > x) 
//									{
//										MAX_X.x = x;
//										MAX_X.y = y;
//									}
//								}
//								else 
//								{
//									if (MAX_X.x < x) 
//									{
//										MAX_X.x = x;
//										MAX_X.y = y;
//									}
//								}
//							}
//						}
//						else if (iColletType == 1)
//						{
//							if (iArrDataY[x][y] > iBinaryData)
//							{
//								cvLine(ipDstImage, cvPoint(x, y), cvPoint(x, y), CV_RGB(0, 0, 255), 2);
//								FLAG_Circle[x][y] = 1;
//
//								if (TURN_RIGHT == 0) 
//								{
//									if (MAX_X.x > x) 
//									{
//										MAX_X.x = x;
//										MAX_X.y = y;
//									}
//								}
//								else 
//								{
//									if (MAX_X.x < x) 
//									{
//										MAX_X.x = x;
//										MAX_X.y = y;
//									}
//								}
//							}
//						}
//						else if (iColletType == 2)
//						{
//							if (iArrDataY[x][y] > iBinaryData)
//							{
//								cvLine(ipDstImage, cvPoint(x, y), cvPoint(x, y), CV_RGB(0, 0, 255), 2);
//								FLAG_Circle[x][y] = 1;
//
//								if (TURN_RIGHT == 0) 
//								{
//									if (MAX_X.x > x) 
//									{
//										MAX_X.x = x;
//										MAX_X.y = y;
//									}
//								}
//								else 
//								{
//									if (MAX_X.x < x) 
//									{
//										MAX_X.x = x;
//										MAX_X.y = y;
//									}
//								}
//							}
//						}
//					}
//				}
//			}
//
//			Up_Collet.x		= 9999; // Min으로 찾음
//			Up_Collet.y		= 9999;
//			Down_Collet.x	= 0;	// Max로 찾음
//			Down_Collet.y	= 0;
//			
//			for (int y = MAX_X.y - 100; y <= MAX_X.y + 100; y++) 
//			{
//				if (y < 0) { y = 0; }
//				if (y > nImageH) { y = nImageH; }
//
//				_c = 0;
//
//				for (int x = MAX_X.x - 100; x <= MAX_X.x + 100; x++) 
//				{
//					if (x < 0) { x = 0; }
//					if (x > nImageW) { x = nImageW; }
//
//					if (FLAG_Circle[x][y] == 1) 
//					{
//						if (Up_Collet.y > y) 
//						{
//							Up_Collet.y = y;
//							Up_Collet.x = x;
//						}
//
//						if (Down_Collet.y < y) 
//						{
//							Down_Collet.y = y;
//							Down_Collet.x = x;
//						}
//					}
//
//					_c++;
//
//					if (_c > 200)
//						break;
//				}
//
//				_f++;
//
//				if (_f > 200)
//					break;
//			}
//		}
//
//		cvLine(ipDstImage, Up_Collet, Up_Collet, CV_RGB(0, 255, 255), 8);
//		cvLine(ipDstImage, Down_Collet, Down_Collet, CV_RGB(0, 255, 255), 8);
//
//		double Distance_Collet = sqrt(pow(Up_Collet.x - Down_Collet.x, 2) + pow(Up_Collet.y - Down_Collet.y, 2));
//
//		sprintf(fontBuf, "( %d , %d  , %2.1f ) ", Up_Collet.x, Up_Collet.y, Distance_Collet);
//		cvPutText(ipDstImage, fontBuf, cvPoint(Up_Collet.x + 10, Up_Collet.y + 30), font, CV_RGB(255, 127, 0));
//
//		if (Distance_Collet < dHoleLengthMin || Distance_Collet > dHoleLengthMax)
//		{
//			// Error Detect
//			cvLine(ipDstImage, Up_Collet, Down_Collet, CV_RGB(255, 0, 0), 8);
//			bResult = FALSE;
//		}
//
//		CvPoint Mid_Collet;
//		Mid_Collet.x = (Down_Collet.x + Up_Collet.x) / 2.0;
//		Mid_Collet.y = (Down_Collet.y + Up_Collet.y) / 2.0;
//
//		cvLine(ipDstImage, Temp_Cen, Mid_Collet, CV_RGB(255, 0, 0), 8);
//
//		dDegree_Collet = atan((double)(Temp_Cen.y - Mid_Collet.y) / (double)(Temp_Cen.x - Mid_Collet.x)) * 180.0 / PHI;
//
//		sprintf(fontBuf, "( %d , %d  , %2.1f` ) ", Temp_Cen.x, Temp_Cen.y, dDegree_Collet);
//		cvPutText(ipDstImage, fontBuf, cvPoint(Temp_Cen.x + 10, Temp_Cen.y + 30), font, CV_RGB(255, 127, 0));
//	}
//	else
//	{
//		dPercent_max = 0;
//	}
//
//	dResultDegree = dDegree_Collet;
//
//// 	end_sec = (double)clock() / CLOCKS_PER_SEC;
//// 	double Time = end_sec - current_sec;
//// 	sprintf(fontBuf, "Time : %1.3fs", Time);
//// 	cvPutText(ipDstImage, fontBuf, cvPoint(10, 100), font, CV_RGB(255, 127, 0));
//// 
//// 	sprintf(fontBuf, "Matching Percent : %3.1f ", dPercent_max * 100.0);
//// 	cvPutText(ipDstImage, fontBuf, cvPoint(10, 50), font2, CV_RGB(255, 127, 0));
//// 
//// 	sprintf(fontBuf, Algo_Ver);
//// 	cvPutText(ipDstImage, fontBuf, cvPoint(1100, 1100), font2, CV_RGB(0, 120, 255));
//// 	sprintf(fontBuf, Ver_Date);
//// 	cvPutText(ipDstImage, fontBuf, cvPoint(1100, 1140), font2, CV_RGB(0, 120, 255));
//
//	cvLine(ipDstImage, cvPoint(0, nImageH / 2.0), cvPoint(nImageW, nImageH / 2.0), CV_RGB(255, 0, 0), 1);
//	cvLine(ipDstImage, cvPoint(nImageW / 2.0, 0), cvPoint(nImageW / 2.0, nImageH), CV_RGB(255, 0, 0), 1);
//
//	cvSaveImage("C:\\ResultImage.bmp", ipDstImage);
//
//	cvReleaseImage(&ipBinaryImage);
//	cvReleaseImage(&ipSrcImage);
//	cvReleaseImage(&ipDstImage);
//	cvReleaseImage(&ipEdgeImage);
//	cvReleaseImage(&ipConvertImage);
//
//	for (int i = 0; i < 2560; i++)
//		delete[]iArrDataY[i];
//	delete[]iArrDataY;
//
//	delete font;
//
//
//	return bResult;
//}
