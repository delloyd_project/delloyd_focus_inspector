﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem.cpp
// Created	:	2017/1/2 - 15:40
// Modified	:	2017/1/2 - 15:40
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Wnd_Cfg_TestItem.h"
#include "Def_Test.h"
#include "Def_TestItem.h"

#define		IDC_LC_TESTITEM		1001
#define		IDC_LC_SELECTED		1002

IMPLEMENT_DYNAMIC(CWnd_Cfg_TestItem, CWnd_BaseView)

CWnd_Cfg_TestItem::CWnd_Cfg_TestItem()
{
	VERIFY(m_font_Data.CreateFont(
		19,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		FIXED_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}


CWnd_Cfg_TestItem::~CWnd_Cfg_TestItem()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_TestItem, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LC_TESTITEM,	OnLvnItemchanged)
END_MESSAGE_MAP()

// CWnd_Cfg_TestItem message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestItem::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_TestItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_TestItem.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_TestItem.SetFont_Gdip(L"Arial", 11.0F);

	m_st_SelectedItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_SelectedItem.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_SelectedItem.SetFont_Gdip(L"Arial", 11.0F);

	m_st_TestItem.Create (_T("Test Item"), dwStyle, rectDummy, this, IDC_STATIC);
	m_lc_TestItem.Create(WS_VISIBLE | WS_CHILD | LVS_REPORT | LVS_SHOWSELALWAYS | LVS_SINGLESEL | WS_BORDER, rectDummy, this, IDC_LC_TESTITEM);
	m_lc_TestItem.SetFont(&m_font_Data);

	m_st_SelectedItem.Create (_T("Selected Item"), dwStyle, rectDummy, this, IDC_STATIC);
	m_lc_SelectedItem.Create(WS_VISIBLE | WS_CHILD | LVS_REPORT | LVS_SHOWSELALWAYS | LVS_SINGLESEL | WS_BORDER, rectDummy, this, IDC_LC_SELECTED);
	m_lc_SelectedItem.SetFont(&m_font_Data);

	InitListCtrl();
	InitTestItem();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin		= 10;
	int iSpacing	= 5;
	int iCateSpacing= 10;

	int iLeft		= iMagrin;
	int iTop		= iMagrin;
	int iWidth		= cx - iMagrin - iMagrin;
	int iHeight		= cy - iMagrin - iMagrin;
	int iCtrlWidth	= 300;
	int iStHeight	= 30;
	int iCtrlHeight = iHeight - iStHeight - iSpacing;

	m_st_TestItem.MoveWindow(iLeft, iTop, iCtrlWidth, iStHeight);
	iTop += iStHeight + iSpacing;
	m_lc_TestItem.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft += iCtrlWidth + iCateSpacing + iCateSpacing;
	iTop = iMagrin;
	m_st_SelectedItem.MoveWindow(iLeft, iTop, iCtrlWidth, iStHeight);
	iTop += iStHeight + iSpacing;
	m_lc_SelectedItem.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_TestItem::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_TestItem::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class

	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnLvnItemchanged
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/3 - 13:01
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::OnLvnItemchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	if ((0 <= pNMLV->iItem) && (1 <= pNMLV->iSubItem))
	{
		int i = 0;
	}

	if (pNMLV->uChanged == LVHT_ONITEMSTATEICON)
	{
		// 0x1000 : Unchecked, 0x2000 : Checked
		if ((pNMLV->uNewState == 0x1000) || (pNMLV->uNewState == 0x2000))
		{
			UpdateSelectedItemz();
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: InitListCtrl
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/3 - 10:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::InitListCtrl()
{
	m_lc_TestItem.SetFont(&m_font_Data);
	m_lc_TestItem.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_CHECKBOXES | LVS_EX_DOUBLEBUFFER);

	m_lc_TestItem.InsertColumn(0, _T(""),		LVCFMT_LEFT,	22);
	m_lc_TestItem.InsertColumn(1, _T(""),		LVCFMT_CENTER,	38);
	m_lc_TestItem.InsertColumn(2, _T("Test"),	LVCFMT_CENTER,	240);

	m_lc_SelectedItem.SetFont(&m_font_Data);
	m_lc_SelectedItem.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	m_lc_SelectedItem.InsertColumn(0, _T(""),		LVCFMT_LEFT,	40);
	m_lc_SelectedItem.InsertColumn(1, _T("Test"),	LVCFMT_CENTER,	260);

}

//=============================================================================
// Method		: InitTestItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/3 - 11:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::InitTestItem()
{
	CString strText;

	for (UINT nIdx = 0; nIdx < USE_TEST_ITEM_CNT; nIdx++)
	{
		strText.Format(_T("%d"), nIdx + 1);
		m_lc_TestItem.InsertItem(nIdx, _T(""));
		m_lc_TestItem.SetItemText(nIdx, 1, strText);
		m_lc_TestItem.SetItemText(nIdx, 2, g_szLT_TestItem_Name[g_nEqp_TestItemIDList[nIdx]]);
	}
}

//=============================================================================
// Method		: GetTestItemID
// Access		: protected  
// Returns		: UINT
// Parameter	: __in UINT nListIndex
// Qualifier	:
// Last Update	: 2017/1/3 - 15:41
// Desc.		:
//=============================================================================
UINT CWnd_Cfg_TestItem::GetTestItemID(__in UINT nListIndex)
{
	switch (nListIndex)
	{
	case TI3Axis_Current:
		return TIID_Current;
		break;

// 	case TI3Axis_EIAJ:
// 		return TIID_EIAJ;
// 		break;

// 	case TI3Axis_SFR:
// 		return TIID_SFR;
// 		break;
// 
	case TI3Axis_ActiveAlgin:
		return TIID_ActiveAlgin;
		break;

// 	case TI3Axis_LEDTest:
// 		return TIID_LEDTest;
// 		break;

// 	case TI3Axis_Resolution:
// 		return TIID_Resolution;
// 		break;

// 	case TI3Axis_Rotation:
// 		return TIID_Rotation;
// 		break;
// 
// 	case TI3Axis_Particle:
// 		return TIID_Particle;
// 		break;

	default:
		break;
	}

	return 0;
}

//=============================================================================
// Method		: GetTestItemListIndex
// Access		: protected  
// Returns		: UINT
// Parameter	: __in UINT nItemID
// Qualifier	:
// Last Update	: 2017/1/3 - 16:29
// Desc.		:
//=============================================================================
UINT CWnd_Cfg_TestItem::GetTestItemListIndex(__in UINT nItemID)
{
	switch (nItemID)
	{
	case TIID_Current:
		return TI3Axis_Current;
		break;

// 	case TIID_EIAJ:
// 		return TI3Axis_EIAJ;
// 		break;

// 	case TIID_SFR:
// 		return TI3Axis_SFR;
// 		break;
// 
	case TIID_ActiveAlgin:
		return TI3Axis_ActiveAlgin;
		break;

// 	case TIID_LEDTest:
// 		return TI3Axis_LEDTest;
// 		break;
// 
// 	case TIID_CenterPointAdj:
// 		return TI3Axis_CenterAdjust;
// 		break;

// 	case TIID_Resolution:
// 		return TI3Axis_Resolution;
// 		break;

// 	case TIID_Rotation:
// 		return TI3Axis_Rotation;
// 		break;
// 
// 	case TIID_Particle:
// 		return TI3Axis_Particle;
// 		break;

	default:
		break;
	}

	return 0;
}

//=============================================================================
// Method		: UpdateSelectedItemz
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/3 - 15:30
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::UpdateSelectedItemz()
{
	int iCnt = m_lc_TestItem.GetItemCount();

	BOOL bCheck = TRUE;
	m_arSelectedItemz.RemoveAll();
	m_lc_SelectedItem.DeleteAllItems();

	UINT nID = 0;
	for (UINT nIdx = 0; nIdx < (UINT)iCnt; nIdx++)
	{
		if (m_lc_TestItem.GetCheck(nIdx))
		{
			nID = GetTestItemID(nIdx);
			m_arSelectedItemz.Add(nID);
		}
	}

	iCnt = (int)m_arSelectedItemz.GetCount();

	CString strText;
	for (UINT nIdx = 0; nIdx < (UINT)iCnt; nIdx++)
	{
		strText.Format(_T("%d"), nIdx + 1);
		m_lc_SelectedItem.InsertItem(nIdx, _T(""));
		m_lc_SelectedItem.SetItemText(nIdx, 0, strText);
		m_lc_SelectedItem.SetItemText(nIdx, 1, g_szLT_TestItem_Name[m_arSelectedItemz.GetAt(nIdx)]);
	}
}

//=============================================================================
// Method		: SetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_ModelInfo * pModelInfo
// Qualifier	:
// Last Update	: 2017/1/3 - 15:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::SetModelInfo(__in const ST_ModelInfo* pModelInfo)
{
	m_arSelectedItemz.RemoveAll();
	m_arSelectedItemz.Copy(pModelInfo->TestItemz);
	INT_PTR iCnt = m_arSelectedItemz.GetCount();

	m_lc_SelectedItem.DeleteAllItems();

 	INT_PTR iListIdx = 0;

	// 전체 Uncheck
	INT_PTR iItemCnt = m_lc_TestItem.GetItemCount();
	for (UINT nIdx = 0; nIdx < (UINT)iItemCnt; nIdx++)
	{
		m_lc_TestItem.SetCheck(nIdx, FALSE);
	}

	// 선택된 항목 Check
	for (UINT nIdx = 0; nIdx < (UINT)iCnt; nIdx++)
	{
		iListIdx = GetTestItemListIndex(pModelInfo->TestItemz.GetAt(nIdx));
		m_lc_TestItem.SetCheck((int)iListIdx, TRUE);
	}
}

//=============================================================================
// Method		: GetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/3 - 15:44
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::GetModelInfo(__out ST_ModelInfo& stModelInfo)
{
	stModelInfo.TestItemz.RemoveAll();
	stModelInfo.TestItemz.Copy(m_arSelectedItemz);
}
