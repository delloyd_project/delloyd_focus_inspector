﻿//*****************************************************************************
// Filename	: View_MainCtrl.h
// Created	: 2010/11/26
// Modified	: 2016/06/07
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef View_MainCtrl_h__
#define View_MainCtrl_h__

#pragma once

#include "TestManager_EQP.h"
// UI
#include "Wnd_MainView.h"
#include "Wnd_RecipeView.h"
#include "Wnd_DeviceView.h"
#include "Wnd_MotorView.h"
#include "Wnd_WorklistView.h"
#include "Wnd_LogView.h"
#include "Wnd_AccessMode.h"
#include "Wnd_ModelMode.h"
#include "Wnd_MessageView.h"
#include "Wnd_Origin.h"

#include "SplashScreenEx.h"
#include "Dlg_Barcode.h"
#include "Dlg_Popup.h"
#include "Dlg_FailConfirm.h"
//#include "Dlg_MasterMode.h"

#include "OverLay_Proc.h"

typedef enum _enumSubView
{
	SUBVIEW_MAIN	= 0,	
	SUBVIEW_RECIPE,
	SUBVIEW_DEVICE,
	SUBVIEW_WORKLIST,
	//SUBVIEW_ERROR,
	SUBVIEW_LOG,
	SUBVIEW_MAX,
}enumSubView;


//=============================================================================
// CView_MainCtrl 창
//=============================================================================
class CView_MainCtrl : public CWnd, public CTestManager_EQP
{
// 생성입니다.
public:
	CView_MainCtrl();
	virtual ~CView_MainCtrl();

protected:

	// Timer Queue
	HANDLE				m_hTimer_FailBox;
	HANDLE				m_hTimerQueue_FailBox;
	void				 CreateTimerQueue_FailBox();
	void				 DeleteTimerQueue_FailBox();
	static VOID CALLBACK TimerRoutine_FailBox(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnPaint					();
	afx_msg BOOL	OnEraseBkgnd			(CDC* pDC);
	afx_msg HBRUSH	OnCtlColor				(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void	OnTimer					(UINT_PTR nIDEvent);

	// 로그 메세지
	afx_msg	LRESULT	OnLogMsg				(WPARAM wParam, LPARAM lParam);

	// 검사 제어
	afx_msg	LRESULT	OnTestStart				(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnTestStop				(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnTestInit				(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnTestCompleted			(WPARAM wParam, LPARAM lParam);
	
	// MES 통신
	afx_msg LRESULT	OnCommStatus_MES		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnRecvMES				(WPARAM wParam, LPARAM lParam);

	// 그래버 보드 통신
	afx_msg LRESULT	OnCameraSelect			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnCameraChgStatus		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnCameraRecvVideo		(WPARAM wParam, LPARAM lParam);

	// 검사 제어
	afx_msg	LRESULT	OnSwitchPermissionMode	(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeModel			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeMode			(WPARAM wParam, LPARAM lParam);	
	afx_msg LRESULT	OnDeviceCtrl			(WPARAM wParam, LPARAM lParam);	
	
	// 통신 데이터
	afx_msg LRESULT	OnRecvBarcode			(WPARAM wParam, LPARAM lParam);

	// 시리얼 통신 데이터
	afx_msg LRESULT	OnRecvMainBrd			(WPARAM wParam, LPARAM lParam);

	// Pogo 카운트
	afx_msg LRESULT	OnPogoCnt_Increase		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnPogoCnt_Update		(WPARAM wParam, LPARAM lParam);

	// Driver Count
	afx_msg LRESULT	OnDriverCnt_Increase	(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnDriverCnt_Update		(WPARAM wParam, LPARAM lParam);

	// Manual 컨트롤
	afx_msg LRESULT	OnMasterSet				(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnModelSave				(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeMotor			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMasterMode(WPARAM wParam, LPARAM lParam);

	// 메뉴얼 조정
	//afx_msg LRESULT	OnManualFocusing		(WPARAM wParam, LPARAM lParam);


	DECLARE_MESSAGE_MAP()
	
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	// 배경색 처리용
	CBrush				m_brBkgr;
	CFile_Model			m_fileModel;

	//-----------------------------------------------------
	// 차일드 윈도우 구분용
	//-----------------------------------------------------	
	UINT				m_nWndIndex;
	CWnd*				m_pWndPtr[SUBVIEW_MAX];
	CWnd_MainView		m_wndMainView;

	CWnd_RecipeView		m_wndRecipeView;
	//CWnd_MotorView		m_wndMotorView;
	CWnd_DeviceView		m_wndDeviceView;
	CWnd_WorklistView	m_wndWorklistView;
	CWnd_LogView		m_wndLogView;
	CWnd_AccessMode		m_wndAccessMode;
	CWnd_ModelMode		m_wndModelMode;
	CWnd_Origin			m_wndOrigin;

	// 통신 상태 표시 Pane의 포인터
	CWnd*				m_pwndCommPane;

	// 바코드 입력 다이얼로그
	CDlg_Barcode*		m_pdlgBarcode;

	// 불량함 팝업창 다이얼로그
	CDlg_FailConfirm m_DlgFailBox;

	// 마스터 모드 팝업창 다이얼로그
	CDlg_MasterMode m_DlgMasterMode;

	//-----------------------------------------------------
	// 로그/파일 처리
	//-----------------------------------------------------
// 	CLogFile		m_Log_DevLog;
	virtual void	OnInitLogFolder					();
		
	//-----------------------------------------------------
	// 초기 설정 관련
	//-----------------------------------------------------
	// 생성자에서 초기화 할 세팅
	void			InitConstructionSetting			();
	// Window 생성 후 세팅
	void			InitUISetting					();
	// 주변장치들 기본 설정
	void			InitDeviceSetting				();
	// 모델, 모터 설정 파일 체크
	void			InitEVMS_EnvFile				(__in LPCTSTR szModelFile);
	void			InitEVMS_EnvFile_All			();
	
	//-----------------------------------------------------
	// 통신 연결 상태
	//-----------------------------------------------------

	// 그래버 보드
	virtual void	OnSetStatus_GrabberBrd_ComArt	(__in BOOL bConnect);

	// 바코드 리더기 통신 연결 상태
	virtual void	OnSetStatus_BCR					(__in UINT nConnect);

	// PCB 보드 통신 연결상태
	virtual void	OnSetStatus_PCBCamBrd			(__in UINT nConnect, __in UINT nIdxBrd);

	// 모션보드 연결 상태
	virtual void	OnSetStatus_IOBoard				(__in BOOL bConnect);
	virtual void	OnSetStatus_MotorBoard			(__in BOOL bConnect);

	// Digital Indicator 연결 상태
	virtual void	OnSetStatus_Indicator			(__in BOOL bConnect, __in UINT nChIdx = 0);

	// 컴아트 영상 신호 상태
	void			OnSetStatus_Signal_CA			(__in UINT nIndex, __in BOOL bOn);

	// MES 연결 상태
	void			OnSetStatus_MES					(__in UINT nConnect);

	// 비전 카메라 연결 상태
	//virtual void	OnSetStatus_VisionCam			(__in BOOL bConnect, __in UINT nCamIdx = 0);

	// 비전 조명 연결 상태
	//virtual void	OnSetStatus_VisionLight			(__in BOOL bConnect, __in UINT nChIdx = 0);


	// 바코드 입력
	virtual void	OnSet_Barcode					(__in LPCTSTR szBarcode);
	virtual void	OnAddErrorInfo					(__in enErrorCode lErrorCode);
	virtual void	OnSetCycleTime					();

	//-----------------------------------------------------
	// UI 업데이트
	//-----------------------------------------------------
	virtual void	OnUpdateElapsedTime_All			(__in DWORD dwTime);
	virtual void	OnTestProgress					(__in enTestProcess nProcess);
	virtual	void	OnTestResult					(__in enTestResult nResult);
	virtual	void	OnTestErrCode					(__in LPCTSTR szErrorCode);
	virtual void	OnTestFailResultCode_Unit		(__in UINT nUnitIdx, __in enResultCode_TestItem nFailCode);
	virtual void	OnSetInputTime					();
	virtual void	OnSetResetSiteInfo				();
	virtual void	OnSetInsertBarcode				(__in LPCTSTR szBarcode);
	virtual void	OnSetTestInitialize				(__in enTestEachResult EachResult, __in CString strText);	// 검사 초기화
	virtual void	OnSetTestFinalize				(__in enTestEachResult EachResult, __in CString strText);	// 검사 마무리

	virtual void	OnSetTestCurrent				(__in enTestEachResult EachResult, __in CString strText);	// 소비전류 검사
	virtual void	OnSetTestActiveAlgin			(__in enTestEachResult EachResult, __in CString strText);	// 광축조정 검사
	virtual void	OnSetTestFocus					(__in enTestEachResult EachResult, __in CString strText);	// 
	virtual void	OnSetTestCenterAdjust			(__in enTestEachResult EachResult, __in CString strText);	// 광축조정
	virtual void	OnSetTestResolution				(__in enTestEachResult EachResult, __in CString strText);	// 해상력 검사
	virtual void	OnSetTestRotation				(__in enTestEachResult EachResult, __in CString strText);	// Rotation 검사
	virtual void	OnSetTestParticle				(__in enTestEachResult EachResult, __in CString strText);	// 이물 검사
	virtual void	OnSetResult						(__in enTestResult Result							   );
	virtual void	OnSetResult						(__in enTestResult Result,		   __in CString strText);
	
	virtual void	OnBarcode_Input					();

	virtual void	OnSetLotInfo					();
	virtual void	OnSetMESInfo();

	virtual void	OnSetMaterSetView				(__in BOOL bEnable);

	//Indicator 데이터 
	virtual void	OnSetIndicatorData				(__in ST_ModelInfo &stModelInfo, __in float fAxisX,	__in float fAxisY);

	// 팝업된 윈도우 숨김
	virtual void	OnHidePopupUI					();
	
	virtual void	OnSetMasterOffSetData			(__in int iOffsetX, __in int iOffsetY);

	// TestInfo UI Button Change
	virtual void	OnChangeBtnState					(__in BOOL bMode);
	virtual void	OnChangeStartBtnState				(__in BOOL bMode);
	//-----------------------------------------------------
	// 테스트
	//-----------------------------------------------------
	virtual void	OnUpdateSiteCamInfo				();

	// 카메라 데이터 초기화
	virtual void	OnResetCamInfo					();
	
	// Worklist 처리
	virtual void	OnInsertWorklist				();
	virtual void	OnSaveWorklist					();
	virtual void	OnLoadWorklist					();

	virtual void	OnUpdateYield					();
	virtual void	OnSetStatus_PogoCount			();
	virtual void	OnSetStatus_DriverCount			();
	
	virtual BOOL	OnChangeLotInfo					(__in enModelStatus LotStatus);
	virtual BOOL	OnChangeModelInfo				();
	virtual void	OnResetYieldCycleTime			();

	//-------------------------------------------------------------------------
	// 모델 파일 불러오기 및 세팅
	inline BOOL		LoadModelInfo					(__in LPCTSTR szModel, __in BOOL bNotifyModelWnd = TRUE);
	void			InitLoadModelInfo				();
	
	virtual	BOOL	EVMSPathSearch					(__out CString &szEVNPath);

	//-----------------------------------------------------
	// 불량함 팝업 처리
	//-----------------------------------------------------
	virtual void	OnPopupFailBoxConfirm();
	virtual BOOL	OnPopupFailBoxConfirmVisible();

	//-----------------------------------------------------
	// 영상 작업 처리
	//-----------------------------------------------------
	void			DisplayVideo					(__in UINT nChIdx, __in LPBYTE lpbyRGB, __in DWORD dwRGBSize, __in UINT nWidth, __in UINT nHeight);
	void			DisplayVideo_Overlay			(__in UINT nChIdx, __in enPic_TestItem enItem, __inout IplImage *TestImage);
// 	virtual void	DisplayVideoPic					(__in CRect rcSize, __in CDC *pCDC);
// 	virtual void	DisplayVideoRecipePic			(__in CRect rcSize, __in CDC *pCDC);
	void			DisplayVideo_LastImage			(__in UINT nChIdx);
	virtual void	DisplayVideo_NoSignal			(__in UINT nChIdx);


//=============================================================================
public: 
//=============================================================================
	
	// 로그 메세지 처리용 함수
	virtual void	AddLog						(__in LPCTSTR lpszLog, __in BOOL bError = FALSE, __in UINT nLogType = LOGTYPE_NORMAL, __in BOOL bOnlyLogType = FALSE);
	// 윈도우 배경색 설정용 함수
	void			SetBackgroundColor			(__in COLORREF color, __in BOOL bRepaint = TRUE);
	// 차일드 윈도우 전환 시 사용
	UINT			SwitchWindow				(__in UINT nIndex);
	// 장치 통신 상태 표시 윈도우 포인터 설정
	void			SetCommPanePtr				(__in CWnd* pwndCommPane);
	// 옵션이 변경 되었을 경우 다시 UI나 데이터를 세팅하기 위한 함수
	void			ReloadOption				();
	// 프로그램 로딩 끝난 후 자동 처리를 위한 함수
	void			InitStartProgress			();	
	BOOL			InitStartBoardProgress		();	
	// 프로그램 종료시 처리해야 할 기능들을 처리하는 함수
	void			FinalExitProgress			();
	BOOL			FinalExitBoardProgress		();
	// 수동 바코드 입력
	void			ManualBarcode				();
	// 제어 권한 설정 창 
	void			PermissionView				();
	// 제어 권한 설정 창 
	virtual BOOL	MessageView					(__in CString szText, __in BOOL bMode = FALSE, __in BOOL bFixCheckMode = FALSE);
	// Indicator Zero
	void			IndicatorZero				(__in UINT nAxis);
	// 제어 권한 설정 창 
	virtual void	PermissionStatsView			();
	// 제어 권한 설정
	virtual void	SetPermissionMode			(__in enPermissionMode nAcessMode);
	//virtual void	ManualAutoMode				(__in BOOL bMode);
	virtual void	ManualBtnEnable				(__in BOOL bMode);

	BOOL			IsRecipeTest				();
	//--------------------- TEST --------------------------
	void			Test_Process				(__in UINT nTestNo);
		
	COverlay_Proc	m_OverlayProc;

	BOOL m_bFlag_StartBtn;
	BOOL m_bFlag_StopBtn;
	BOOL m_bFlag_FixBtn;
	BOOL m_bFlag_UnFixBtn;
	BOOL *m_pbFailBoxStatus;

};

#endif // View_MainCtrl_h__


