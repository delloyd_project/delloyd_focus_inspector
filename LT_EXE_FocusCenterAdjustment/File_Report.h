﻿//*****************************************************************************
// Filename	: 	File_Report.h
// Created	:	2016/8/5 - 15:50
// Modified	:	2016/8/5 - 15:50
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef File_Report_h__
#define File_Report_h__

#pragma once

#include "Def_DataStruct.h"

class CFile_Report
{
public:
	CFile_Report();
	~CFile_Report();


protected:

	CString Make_Worklist		(__in const ST_Worklist* pstWorklist);

public:

	BOOL	Save_Worklist		(__in LPCTSTR szPath, __in const ST_Worklist* pstWorklist);
	
	BOOL	Save_LotInfo		(__in LPCTSTR szPath, __in ST_LOTInfo* pstLotInfo);
	BOOL	Load_LotInfo		(__in LPCTSTR szPath, __out ST_LOTInfo& stLotInfo);

	// 수율 저장/불러오기
	BOOL	SaveYield_LOT		(__in LPCTSTR szFullPath, __in const ST_Yield* pYieldInfo);
	BOOL	LoadYield_LOT		(__in LPCTSTR szFullPath, __out ST_Yield& stYieldInfo);

	// ImageNext MES Function
	BOOL	SaveMES_Report		(__in LPCTSTR szFullPath, __in const ST_CamInfo* pstCamInfo);

};

#endif // File_Report_h__

