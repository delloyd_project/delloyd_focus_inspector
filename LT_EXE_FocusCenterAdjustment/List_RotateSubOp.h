﻿#ifndef List_RotateSubOp_h__
#define List_RotateSubOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_RtSOp
{
	RtSOp_Object,
	RtSOp_Degree,
	RtSOp_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_RtSOp[] =
{
	_T(""),
	_T("Degree"),
	NULL
};

typedef enum enListItemNum_RtSOp
{
	RtSOp_Standard,
	RtSOp_StandardOffset,
	RtSOp_TargetOffset,
	RtSOp_MaxOffset,
	RtSOp_ItemNum,
};

static const TCHAR*	g_lpszItem_RtSOp[] =
{
	_T("Standard"),
	_T("Standard Spc"),
	_T("Target Degree"),
	_T("Max Degree"),
	NULL 
};

const int	iListAglin_RtSOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_RtSOp[] =
{
	95,
	95,
};

class CList_RotateSubOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_RotateSubOp)

public:
	CList_RotateSubOp();
	virtual ~CList_RotateSubOp();

	ST_LT_TI_Rotate		*m_pstRotate;

	void SetPtr_Rotate(ST_LT_TI_Rotate *pstRotate)
	{
		if (pstRotate == NULL)
			return;

		m_pstRotate = pstRotate;
	};

	UINT m_nTestMode;
	void SetTestModeChange(UINT nTestMode)
	{
		m_nTestMode = nTestMode;
	}

	void InitHeader();
	void InsertFullData();
	void SetRectRow(UINT nRow);

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()

	afx_msg int	 OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize						(UINT nType, int cx, int cy);
	afx_msg void OnNMClick					(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclk					(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillFocusERtOpellEdit	();

	afx_msg BOOL OnMouseWheel				(UINT nFlags, short zDelta, CPoint pt);
	virtual BOOL PreCreateWindow			(CREATESTRUCT& cs);

	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;

	BOOL		UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL		UpdateCellData_double	(UINT nRow, UINT nCol, double dValue);

};

#endif // List_RotateSubOp_h__
