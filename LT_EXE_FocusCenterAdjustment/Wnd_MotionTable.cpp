﻿// Wnd_IOTable.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MotionTable.h"

// CWnd_MotionTable

IMPLEMENT_DYNAMIC(CWnd_MotionTable, CWnd)

CWnd_MotionTable::CWnd_MotionTable()
{
	m_pstDevice				= NULL;
	m_hTimerQueue			= NULL;
	m_hTimer_SensorCheck	= NULL;
	m_nAxisNum				= 0;

	CreateTimerQueue_Mon	();
	CreateTimerSensorCheck	();
}

CWnd_MotionTable::~CWnd_MotionTable()
{
	DeleteTimerQueue_Mon	();
}

BEGIN_MESSAGE_MAP(CWnd_MotionTable, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_ST_AXIS_NAME,	IDC_ST_SEN_AMP	 - 1, OnAxisSelect)
	ON_COMMAND_RANGE(IDC_ST_SEN_AMP,  	IDC_ST_SEN_POS	 - 1, OnAxisAmpCtr)
	ON_COMMAND_RANGE(IDC_ST_SEN_ALRAM,  IDC_ST_SEN_MAXNUM,	OnAxisAlramCtr)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/28 - 17:02
// Desc.		:
//=============================================================================
int CWnd_MotionTable::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();


	for (UINT nIdx = 0; nIdx < ST_MT_MAXNUM; nIdx++)
	{
		m_st_AxisName[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_AxisName[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_AxisName[nIdx].SetFont_Gdip(L"Arial", 10.0F);
		m_st_AxisName[nIdx].Create(g_szMotionStatusName[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < ST_MT_MAXNUM; nIdx++)
	{
		for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
		{
			m_st_AxisSen[nAxis][ST_MT_POS_NAME].SetFont_Gdip(L"Arial", 12.0F);
			m_st_AxisSen[nAxis][nIdx].SetStaticStyle(CVGStatic::StaticStyle_GroupHeader);

			if (nIdx == ST_MT_AXIS_NAME)
			{
				CString strName;
				strName = m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[nAxis].szAxisName;

				m_st_AxisSen[nAxis][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
				m_st_AxisSen[nAxis][nIdx].Create(strName, dwStyle | SS_NOTIFY | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_AXIS_NAME + nAxis + nIdx * MAX_AIXS);
			}
			else
			{
				m_st_AxisSen[nAxis][nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
				m_st_AxisSen[nAxis][nIdx].Create(_T(""), dwStyle | SS_NOTIFY | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_AXIS_NAME + nAxis + nIdx * MAX_AIXS);
			}
		}
	}

	m_st_AxisSen[0][ST_MT_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	m_Group_Nmae.SetTitle(L"MOTOR INFO MONITERING");

	if (!m_Group_Nmae.Create(_T("MOTOR INFO MONITERING"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MotionTable::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if (m_pstDevice == NULL)
		return;

	if ((0 == cx) || (0 == cy))
		return;

	if (m_pstDevice->MotionManager.m_pMotionCtr == NULL)
		return;

	cx = (int)(cx * MT_WIDTH_OFFSET);
	
	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = iMargin;
	int iWidth	 = cx;
	int iHeight  = cy - iMargin;
	int iStaticW  = ((iWidth - 15) - ((iSpacing * (ST_MT_MAXNUM - 1)))) / (ST_MT_MAXNUM + 3);
	
	int iStNameH = 28;
	int iEdNameH = 28;
	int iStSensorH = 0;
	int iHeightTemp = 0;
	int iStTitleNameH = 35;

	// 초기화
	for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		for (UINT nIdx = 0; nIdx < ST_MT_MAXNUM; nIdx++)
			m_st_AxisSen[nAxis][nIdx].MoveWindow(iLeft, iTop, 0, 0);
	}

	m_Group_Nmae.MoveWindow(iLeft, iTop, iWidth, iHeight);
	iTop += iSpacing * 6;

	iLeft = iMargin;
	m_st_AxisName[ST_MT_AXIS_NAME].MoveWindow(iLeft, iTop, iStaticW * 2, iStNameH);
	iLeft += iStaticW * 2 + iSpacing;

	m_st_AxisName[ST_MT_POWER_NAME].MoveWindow(iLeft, iTop, iStaticW * 1, iStNameH);
	iLeft += iStaticW * 1 + iSpacing;

	m_st_AxisName[ST_MT_POS_NAME].MoveWindow(iLeft, iTop, iStaticW * 3, iStNameH);
	iLeft += iStaticW * 3 + iSpacing;

	for (UINT nIdx = ST_MT_ORI_NAME; nIdx < ST_MT_MAXNUM; nIdx++)
	{
		m_st_AxisName[nIdx].MoveWindow(iLeft, iTop, iStaticW, iStNameH);
		iLeft += iStaticW + iSpacing;
	}

	iTop += iStNameH + iSpacing;

	iHeightTemp = iHeight - iTop;
	iStSensorH = (int)((iHeightTemp * HEIGHT_OFFSET) - (iSpacing * (m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt - 1))) / m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt;

	for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (m_pstDevice->MotionManager.GetAxisUseStatus(nAxis) == TRUE)
		{
			iLeft = iMargin;

			m_st_AxisSen[nAxis][ST_MT_AXIS_NAME].MoveWindow(iLeft, iTop, iStaticW * 2, iStSensorH);
			iLeft += iStaticW * 2 + iSpacing;

			m_st_AxisSen[nAxis][ST_MT_POWER_NAME].MoveWindow(iLeft, iTop, iStaticW * 1, iStSensorH);
			iLeft += iStaticW * 1 + iSpacing;

			m_st_AxisSen[nAxis][ST_MT_POS_NAME].MoveWindow(iLeft, iTop, iStaticW * 3, iStSensorH);
			iLeft += iStaticW * 3 + iSpacing;

			for (UINT nIdx = ST_MT_ORI_NAME; nIdx < ST_MT_MAXNUM; nIdx++)
			{
				m_st_AxisSen[nAxis][nIdx].MoveWindow(iLeft, iTop, iStaticW, iStSensorH);
				iLeft += iStaticW + iSpacing;
			}

			iTop += iStSensorH + iSpacing;
		}
	}
}

//=============================================================================
// Method		: OnSensorComds
// Access		: public  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/3/29 - 15:09
// Desc.		:
//=============================================================================
void CWnd_MotionTable::OnAxisSelect(UINT nID)
{
	if (m_pstDevice == NULL)
		return;

	m_nAxisNum = nID - IDC_ST_AXIS_NAME;

	GetOwner()->SendMessage(WM_SELECT_AXIS, (WPARAM)m_nAxisNum, 0);

	for (UINT nIdx = 0; nIdx < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nIdx++)
		m_st_AxisSen[nIdx][ST_MT_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_Default);

	m_st_AxisSen[m_nAxisNum][ST_MT_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
}

//=============================================================================
// Method		: OnAxisAmpCtr
// Access		: public  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/3/29 - 15:42
// Desc.		:
//=============================================================================
void CWnd_MotionTable::OnAxisAmpCtr(UINT nID)
{
	if (m_pstDevice == NULL)
		return;

	UINT nAxis = nID - IDC_ST_SEN_AMP;

	if (m_pstDevice->MotionManager.GetAmpStatus(nAxis))
		m_pstDevice->MotionManager.SetAmpCtr(nAxis, OFF);
	else
		m_pstDevice->MotionManager.SetAmpCtr(nAxis, ON);
}

//=============================================================================
// Method		: OnAxisAlramCtr
// Access		: public  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/3/29 - 15:42
// Desc.		:
//=============================================================================
void CWnd_MotionTable::OnAxisAlramCtr(UINT nID)
{
	if (m_pstDevice == NULL)
		return;

	UINT nAxis = nID - IDC_ST_SEN_ALRAM;

	m_pstDevice->MotionManager.SetAlarmClear(nAxis);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_MotionTable::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: TimerRoutine_SensorCheck
// Access		: protected static  
// Returns		: VOID CALLBACK
// Parameter	: __in PVOID lpParam
// Parameter	: __in BOOLEAN TimerOrWaitFired
// Qualifier	:
// Last Update	: 2017/3/29 - 9:01
// Desc.		:
//=============================================================================
VOID CALLBACK CWnd_MotionTable::TimerRoutine_SensorCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	CWnd_MotionTable* pThis = (CWnd_MotionTable*)lpParam;

	if (USE_TEST_MODE)
	pThis->OnMonitorSensorCheck();
}

//=============================================================================
// Method		: CreateTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:01
// Desc.		:
//=============================================================================
void CWnd_MotionTable::CreateTimerQueue_Mon()
{
	__try
	{
		// Create the timer queue.
		m_hTimerQueue = CreateTimerQueue();
		if (NULL == m_hTimerQueue)
		{
			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
			return;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		//	AddLog(_T("*** Exception Error : CreateTimerQueue_Mon ()"));
	}
}

//=============================================================================
// Method		: DeleteTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_MotionTable::DeleteTimerQueue_Mon()
{
	// 타이머가 종료될때까지 대기
	__try
	{
		if (!DeleteTimerQueue(m_hTimerQueue))
			TRACE(_T("DeleteTimerQueue failed (%d)\n"), GetLastError());
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_MotionTable::DeleteTimerQueue_Mon()\n"));
	}

	TRACE(_T("타이머 종료 : CWnd_MotionTable::DeleteTimerQueue_Mon()\n"));
}

//=============================================================================
// Method		: CreateTimerSensorCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_MotionTable::CreateTimerSensorCheck()
{
	__try
	{
		// Time Check Timer
		if (NULL == m_hTimer_SensorCheck)
		if (!CreateTimerQueueTimer(&m_hTimer_SensorCheck, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutine_SensorCheck, (PVOID)this, 2000, 150, WT_EXECUTEDEFAULT))
		{
			TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_IOTable::CreateTimer_InputCheck()\n"));
	}
}

//=============================================================================
// Method		: DeleteTimerSensorCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_MotionTable::DeleteTimerSensorCheck()
{
	__try
	{
		if (DeleteTimerQueueTimer(m_hTimerQueue, m_hTimer_SensorCheck, NULL))
			m_hTimer_SensorCheck = NULL;
		else
			TRACE(_T("DeleteTimerQueueTimer : m_hTimer_InputCheck failed (%d)\n"), GetLastError());
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_IOTable::DeleteTimer_InputCheck()\n"));
	}
}

//=============================================================================
// Method		: OnMonitorSensorCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_MotionTable::OnMonitorSensorCheck()
{
	// 모터 Amp 센서
	GetMotorAmpStatus();
	Sleep(10);

	// 모터 원점 센서
	GetMotorOriginStatus();
	Sleep(10);

	// 모터 동작 상태
	GetMotorMotionStatus();
	Sleep(10);

	// 모터 펄스 값
	GetMotorCurrentPosStatus();
	Sleep(10);

	// 모터 +/- 리미트 센서
	GetMotorLimitStatus();
	Sleep(10);

	// 모터 홈 센서
	GetMotorHomeStatus();
	Sleep(10);

	// 모터 알람 센서
	GetMotorAlarmStatus();
}

//=============================================================================
// Method		: GetMotorAmpStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:14
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorAmpStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if(m_pstDevice->MotionManager.GetAxisUseStatus(nAxis))
		{
			if (m_pstDevice->MotionManager.GetAmpStatus(nAxis))
				m_st_AxisSen[nAxis][ST_MT_POWER_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
			else
				m_st_AxisSen[nAxis][ST_MT_POWER_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
		}
	}  
}

//=============================================================================
// Method		: GetMotorCurrentPosStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:18
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorCurrentPosStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;

	CString strPos;

	for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (m_pstDevice->MotionManager.GetAxisUseStatus(nAxis))
		{
			strPos.Format(_T("%.0f"), m_pstDevice->MotionManager.GetCurrentPos(nAxis));
			m_st_AxisSen[nAxis][ST_MT_POS_NAME].SetText(strPos);
		}
	}
}

//=============================================================================
// Method		: GetMotorMotionStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 8:04
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorMotionStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (m_pstDevice->MotionManager.GetAxisUseStatus(nAxis))
		{
			if (m_pstDevice->MotionManager.GetMotionStatus(nAxis))
				m_st_AxisSen[nAxis][ST_MT_MOTION_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
			else
				m_st_AxisSen[nAxis][ST_MT_MOTION_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
		}
	}
}

//=============================================================================
// Method		: GetMotorOriginStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:25
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorOriginStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (m_pstDevice->MotionManager.GetAxisUseStatus(nAxis))
		{
			if (m_pstDevice->MotionManager.GetOriginStatus(nAxis))
				m_st_AxisSen[nAxis][ST_MT_ORI_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
			else
				m_st_AxisSen[nAxis][ST_MT_ORI_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
		}
	}
}

//=============================================================================
// Method		: GetMotorLimitStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:28
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorLimitStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;
	
	for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (m_pstDevice->MotionManager.GetAxisUseStatus(nAxis))
		{
			// + 센서 UNUSE 일 경우
			if (m_pstDevice->MotionManager.GetPosSensorLevel(nAxis) == 2)
			{
				m_st_AxisSen[nAxis][ST_MT_HLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->MotionManager.GetPosSensorStatus(nAxis))
					m_st_AxisSen[nAxis][ST_MT_HLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Red);
				else
					m_st_AxisSen[nAxis][ST_MT_HLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}
		
		if (m_pstDevice->MotionManager.GetAxisUseStatus(nAxis))
		{
			// - 센서 UNUSE 일 경우
			if (m_pstDevice->MotionManager.GetNegSensorLevel(nAxis) == 2)
			{
				m_st_AxisSen[nAxis][ST_MT_LLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->MotionManager.GetNegSensorStatus(nAxis))
					m_st_AxisSen[nAxis][ST_MT_LLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Red);
				else
					m_st_AxisSen[nAxis][ST_MT_LLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}
	}
}

//=============================================================================
// Method		: GetMotorHomeStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:41
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorHomeStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;
	
	for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (m_pstDevice->MotionManager.GetAxisUseStatus(nAxis))
		{
			// HOME 센서 UNUSE 일 경우
			if (m_pstDevice->MotionManager.GetHomeSensorLevel(nAxis) == 2)
			{
				m_st_AxisSen[nAxis][ST_MT_HOME_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->MotionManager.GetHomeSensorStatus(nAxis))
					m_st_AxisSen[nAxis][ST_MT_HOME_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
				else
					m_st_AxisSen[nAxis][ST_MT_HOME_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}
	}
}

//=============================================================================
// Method		: GetMotorAlarmStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:41
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorAlarmStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_AxisSen == NULL)
		return;
	
	for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (m_pstDevice->MotionManager.GetAxisUseStatus(nAxis))
		{
			// ALRAM 센서 UNUSE 일 경우
			if (m_pstDevice->MotionManager.GetAlramSensorLevel(nAxis) == 2)
			{
				m_st_AxisSen[nAxis][ST_MT_ALRAM_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->MotionManager.GetAlarmSenorStatus(nAxis))
					m_st_AxisSen[nAxis][ST_MT_ALRAM_NAME].SetColorStyle(CVGStatic::ColorStyle_Red);
				else
					m_st_AxisSen[nAxis][ST_MT_ALRAM_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}
	}
}

//=============================================================================
// Method		: SetUpdataAxisName
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/4/4 - 13:24
// Desc.		:
//=============================================================================
void CWnd_MotionTable::SetUpdataAxisName()
{
	CString strName;

	for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		strName = m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[nAxis].szAxisName;
		m_st_AxisSen[nAxis][ST_MT_AXIS_NAME].SetWindowText(strName);
	}

	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	}
}

//=============================================================================
// Method		: SetInspectionMode
// Access		: public  
// Returns		: void
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/4 - 17:18
// Desc.		:
//=============================================================================
void CWnd_MotionTable::SetUpdataDataReset(UINT nAxis)
{
	for (UINT nIdx = 0; nIdx < (UINT)m_pstDevice->MotionManager.m_AllMotorData.MotorInfo.lMaxAxisCnt; nIdx++)
		m_st_AxisSen[nIdx][ST_MT_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_Default);

	m_st_AxisSen[nAxis][ST_MT_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	// AXIS NAME
	SetUpdataAxisName();
}

//=============================================================================
// Method		: SetDeleteTimer
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/28 - 12:33
// Desc.		:
//=============================================================================
void CWnd_MotionTable::SetDeleteTimer()
{
	DeleteTimerSensorCheck();
}
