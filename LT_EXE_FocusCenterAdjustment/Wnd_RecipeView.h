﻿//*****************************************************************************
// Filename	: Wnd_RecipeView.h
// Created	: 2015/12/9 - 0:05
// Modified	: 2015/12/9 - 0:05
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Wnd_RecipeView_h__
#define Wnd_RecipeView_h__

enum enMouseEvent
{
	enMouseEvent_Empty = 0,
	enMouseEvent_LeftDown,
	enMouseEvent_RightDown,
};

#pragma once

#include "VGStatic.h"
#include "Def_Enum.h"
#include "File_Model.h"
#include "File_WatchList.h"
#include "Wnd_Cfg_Model.h"
#include "Wnd_Cfg_Base.h"
#include "Wnd_Cfg_TestItem.h"
#include "Wnd_Cfg_StdInfo.h"
#include "Wnd_Cfg_Pogo.h"
#include "Wnd_Cfg_DriverCount.h"
#include "Wnd_Cfg_AF.h"
#include "Wnd_Cfg_MotionTeach.h"
#include "Wnd_ManualCtrl.h"
#include "Wnd_MessageView.h"

#include "Dlg_LEDTest.h"

#include "Def_TestDevice.h"
#include "TI_PicControl.h"
#include "TI_Processing.h"

#include "Wnd_ImageView.h"

#include "Wnd_ImgProc.h"
#include "OverLay_Proc.h"

//=============================================================================
// CWnd_RecipeView
//=============================================================================
class CWnd_RecipeView : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_RecipeView)

public:
	CWnd_RecipeView();
	virtual ~CWnd_RecipeView();

	BOOL	m_bMessageBuf;
	CTI_PicControl		m_TIPicControl;
	CTI_Processing		m_TIProcessing;

	CMFCTabCtrl			m_tc_Option;
	UINT				m_nPicViewMode;

	void SetLTOption(stLT_Option* pstOption)
	{
		if (pstOption == NULL)
			return;

		m_pstOption = pstOption;
		m_TIProcessing.SetLTOption(m_pstOption);
	};

	double m_dDisplaceData;
	double m_dVColletData;

	COverlay_Proc	m_OverlayProc;

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	afx_msg void	OnBnClickedBnNew		();
	afx_msg void	OnBnClickedBnSave		();
	afx_msg void	OnBnClickedBnSaveAs		();
	afx_msg void	OnBnClickedBnLoad		();
	afx_msg void	OnBnClickedBnRefresh	();
	afx_msg void	OnLbnSelChangeModel		();	

	afx_msg LRESULT	OnFileModel				(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeModel			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnRefreshModelList		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnTabChangePic			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnAlignPic				(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnManualTestCmd			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnManualTestCmd_ImageSave(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnEdgeOnOff				(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnDistortionCorr		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnManualCtrlCmd			(WPARAM wParam, LPARAM lParam);
	
	CFont				m_font_Default;
	CFont				m_font_Data;

	// 비전 카메라
	//CVGStatic			m_st_VCam_T;
	//CVGStatic			m_st_VCam_V;

	// 모델 리스트	
	CVGStatic			m_st_File;
	CVGStatic			m_st_Location;
	CListBox			m_lst_ModelList;
	CButton				m_bn_Refresh;

	// New, Save, Save As, Load	
	CButton				m_bn_New;
	CButton				m_bn_Save;
	CButton				m_bn_SaveAs;
	CButton				m_bn_Load;

	// Tab Windows
	CWnd_Cfg_Model			m_wnd_ModelCfg;
	CWnd_Cfg_TestItem		m_wnd_TestItemCfg;
	CWnd_Cfg_StdInfo		m_wnd_StdInfoCfg;
	CWnd_Cfg_Pogo			m_wnd_PogoCfg;
	CWnd_Cfg_DriverCount	m_wnd_DriverCountCfg;
	//CWnd_Cfg_Vision			m_wnd_VisionCfg;
	//CWnd_Cfg_AF				m_wnd_AFCfg;
	//CWnd_Cfg_MotionTeach	m_wnd_MotionTeachCfg;
	
	CWnd_ManualCtrl		m_wnd_ManualCtrl;
	
	CVGStatic			m_st_BackColor;

	ST_ModelInfo		m_stModelInfo;
	CFile_Model			m_fileModel;
	CFile_WatchList		m_ModelWatch;

	ST_Device*			m_pDevice;
	stLT_Option*		m_pstOption;

	// New, Save, Save as, Load
	void		New_Model();
	void		SaveAs_Model();
	void		Load_Model();
	void		Save_Model();

	void		SetModelInfo();
	void		GetModelInfo();

	CString		m_strModelFullPath;
	CString		m_strModelPath;
	CString		m_strImageFilePath;
	CString		m_strPogoPath;
	CString		m_strDriverCountPath;

	void		SetFullPath		(__in LPCTSTR szModelName);
	void		RedrawWindow	();
	BOOL		MessageView		(__in CString szText, __in BOOL bMode = FALSE);

	ST_ImageMode* m_pstImageMode;

	IplImage	*m_LoadImage = NULL;

	HANDLE		m_hExternalExitEvent = NULL; // 외부의 통신 접속 쓰레드 종료 이벤트
	BOOL		m_bFlag_ImageViewMon = FALSE;
	HANDLE		m_hThr_ImageViewMon = NULL; // PLC 모니터링 쓰레드 핸들 
	DWORD		m_dwImageViewMonCycle = 50; // 모니터링 주기
	BOOL		m_bGrabImageStatus = FALSE;

	void		OnPopImageLoad				();
	void		OnImage_LoadDisplay			(CString strPath);
	BOOL		Start_ImageView_Mon			();
	BOOL		Stop_ImageView_Mon			();
	void		OnLoadImageDisplay			();
	void		ImageDisplayVideo			(__in UINT nChIdx, __in LPBYTE lpbyRGB, __in DWORD dwRGBSize, __in UINT nWidth, __in UINT nHeight);
	//void		DisplayVideoRecipePic		(__in CRect rcSize, __in CDC *pCDC);
	static UINT WINAPI Thread_ImageViewMon	(__in LPVOID lParam);
public:
	void		ChangeImageMode				(enImageMode eImageMode);
	void		DisplayVideo_Overlay		(__in UINT nChIdx, __in enPic_TestItem enItem, __inout IplImage *TestImage);
	void		ShowVideo					(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void		GetSaveImageFilePath		(__out CString& szOutPath);
	void		ImageSave					(UINT nTestItemID);

	void SetPtr_ImageMode(__in ST_ImageMode *stImageMode)
	{
		m_pstImageMode = stImageMode;
	};
	//CWnd_ImageView		m_wnd_ImageView;
	CWnd_ImgProc		m_wnd_ImgProc;
	// 비전 영상 화면 출력용
	CWnd_ImageView		m_wnd_VVideo;

	void SetPath(__in LPCTSTR szModelPath, __in LPCTSTR szImagePath, __in LPCTSTR szPogoPath, __in LPCTSTR szDriverCountPath)
	{
		if (NULL != szModelPath)
			m_strModelPath = szModelPath;

		if (NULL != szImagePath)
			m_strImageFilePath = szImagePath;
		
		if (NULL != szPogoPath)
			m_strPogoPath	 = szPogoPath;

		if (NULL != szDriverCountPath)
			m_strDriverCountPath = szDriverCountPath;

		m_wnd_PogoCfg.SetPogoPath(szPogoPath);
		m_wnd_DriverCountCfg.SetDriverCountPath(szDriverCountPath);
		m_ModelWatch.SetWatchOption(m_strModelPath, MODEL_FILE_EXT);
		m_ModelWatch.BeginWatchThrFunc();
	};

	void		ChangePath			(__in LPCTSTR lpszModelPath);
	
	void		SetModel			(__in LPCTSTR szModel);

	void		RefreshModelFileList(__in const CStringList* pFileList);

	void		SetPtr_Device		(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pDevice = pstDevice;
	};
	
	void		SetPtr_PogoCnt		(__in ST_PogoInfo* pstPogoCnt)
	{
		if (pstPogoCnt == NULL)
			return;

		m_wnd_PogoCfg.SetPtr_PogoCnt(pstPogoCnt);
	};

	void		SetPtr_DriverCnt		(__in ST_DriverCountInfo* pstDriverCnt)
	{
		if (pstDriverCnt == NULL)
			return;

		m_wnd_DriverCountCfg.SetPtr_DriverCount(pstDriverCnt);
	};

	void		InitOptionView			();

	void		SavePogoCount			();

	void		SaveDriverCount			();

	void		SaveMasterSet			();

	void		SetStatusEngineerMode	(__in enPermissionMode InspMode);

	UINT		GetVideoPicStatus		();

	BOOL		IsRecipeTest			();

	BOOL		m_bFlag_Test[Man_Cmd_Total];

};

#endif // Wnd_RecipeView_h__

