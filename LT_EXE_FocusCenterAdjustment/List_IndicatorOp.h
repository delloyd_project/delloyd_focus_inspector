﻿#ifndef List_IndicatorOp_h__
#define List_IndicatorOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_IndicatorOp
{
	IndicatorOp_Object,
	IndicatorOp_X,
	IndicatorOp_Y,
	IndicatorOp_MaxCol,
};

static const TCHAR*	g_lpszHeader_IndicatorOp[] =
{
	_T(""),
	_T("X"),
	_T("Y"),
	NULL
};

typedef enum enListItemNum_IndicatorOp
{
	IndicatorOp_Standard,
	IndicatorOp_Spec,
	IndicatorOp_ItemNum,
};

static const TCHAR*	g_lpszItem_IndicatorOp[] =
{
	_T("Standard"),
	_T("Spec"),
	NULL
};

const int	iListAglin_IndicatorOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_IndicatorOp[] =
{
	65,
	65,
	65,
};

class CList_IndicatorOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_IndicatorOp)

public:
	CList_IndicatorOp();
	virtual ~CList_IndicatorOp();

	ST_ModelInfo *m_pstModelInfo;

	/*기준*/
	FLOAT	m_fIndicator_Std_X;
	FLOAT	m_fIndicator_Std_Y;

	/*편차*/
	FLOAT	m_fIndicator_Spec_X;
	FLOAT	m_fIndicator_Spec_Y;

	void InitHeader();
	void InsertFullData();
	void SetRectRow(UINT nRow);
	void GetCellData();

	void SetPtr_Indicator(const ST_ModelInfo *pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = const_cast<ST_ModelInfo*>(pstModelInfo);
	};

protected:
	CFont	m_Font;

	DECLARE_MESSAGE_MAP()

	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;
	BOOL		UpdateCellData(UINT nRow, UINT nCol, FLOAT fValue);

	BOOL		UpdateCellData_double(UINT nRow, UINT nCol, double dValue);
	BOOL		CheckRectValue(__in const CRect* pRegionz);

	afx_msg void	OnEnKillFocusECpOpellEdit();

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
};


#endif // List_CurrentInfo_h__
