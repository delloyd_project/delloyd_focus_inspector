﻿//*****************************************************************************
// Filename	: 	Wnd_ManualCtrl.h
// Created	:	2017/1/4 - 13:33
// Modified	:	2017/1/4 - 13:33
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_ManualCtrl_h__
#define Wnd_ManualCtrl_h__

#pragma once

#include "Def_TestDevice.h"
#include "VGGroupWnd.h"
// CWnd_ManualCtrl

#define HEIGHT_SIZE_T				1

static LPCTSTR	g_szManual_Static[] =
{
	_T("Result Degree"),
	_T("Result Displace"),
	_T("Move Degree"),

	NULL
};


static LPCTSTR	g_szManual_Button[] =
{
	_T("POWER ON"),
	_T("POWER OFF"),
// 	_T("LED ON"),
// 	_T("LED OFF"),
	_T("MODULE FIX"),
	_T("MODULE UNFIX"),
	_T("PCB FIX"),
	_T("PCB UNFIX"),
	_T("DRIVE IN"),
	_T("DRIVE OUT"),
	_T("LAMP RED"),
	_T("LAMP YELLOW"),
	_T("LAMP GREEN"),
	_T("LAMP BUZZER"),
	_T("IMAGE SAVE"),
	_T("PIC IMAGE SAVE"),
	_T("IMAGE LOAD"),
	NULL
};


enum enManualCtrlStatic
{
	ST_MCTRL_RESULT_DEGREE = 0,
	ST_MCTRL_RESULT_DISPLACE,
	ST_MCTRL_MANUAL_MOVE,

	ST_MCTRL_MAXNUM,
};

enum enManualCtrlBtn
{
	BT_MCTRL_POWER_ON = 0,
	BT_MCTRL_POWER_OFF,
// 	BT_MCTRL_LED_ON,
// 	BT_MCTRL_LED_OFF,
	BT_MCTRL_MODULE_FIX,
	BT_MCTRL_MODULE_UNFIX,
	BT_MCTRL_PCB_FIX,
	BT_MCTRL_PCB_UNFIX,
	BT_MCTRL_DRIVE_IN,
	BT_MCTRL_DRIVE_OUT,
	BT_MCTRL_LAMP_RED,
	BT_MCTRL_LAMP_YELLOW,
	BT_MCTRL_LAMP_GREEN,
	BT_MCTRL_LAMP_BUZZER,
	BT_MCTRL_IMAGE_SAVE,
	BT_MCTRL_PIC_IMAGE_SAVE,
	BT_MCTRL_IMAGE_LOAD,
	BT_MCTRL_MAXNUM,
};

enum enManualCtrlEdit
{
	ED_MCTRL_VISION_DATA = 0,
	ED_MCTRL_DISPLACE_DATA,
	ED_MCTRL_DEGREE,

	ED_MCTRL_MAXNUM,
};

class CWnd_ManualCtrl : public CVGGroupWnd
{
	DECLARE_DYNAMIC(CWnd_ManualCtrl)

public:
	CWnd_ManualCtrl();
	virtual ~CWnd_ManualCtrl();

protected:
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnRangeCmds		(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()

	ST_Device*		m_pDevice;

	CFont			m_font_Default;
	CFont			m_font_Edit;
	// CVGStatic		m_st_Item[ST_MCTRL_MAXNUM];
	CMFCButton		m_Btn_Item[BT_MCTRL_MAXNUM];
	// CMFCMaskedEdit	m_ed_Item[ED_MCTRL_MAXNUM];

	void		ClickOut(UINT nID);

public:

	void		SetPtr_Device(__in ST_Device* pDevice)
	{
		m_pDevice = pDevice;
	};

	void OnGetManualDegree	(__out double& dDegree);

	void ImageLoadMode(bool bMode);
	void AllBtnEnableMode(bool bMode);
};
#endif // Wnd_ManualCtrl_h__


