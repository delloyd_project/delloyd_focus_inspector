﻿#ifndef List_Work_Rotate_h__
#define List_Work_Rotate_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_Rota_Worklist
{
	
	Rota_W_Time,
	Rota_W_Equipment,
	Rota_W_Model,
	Rota_W_SWVersion,
	Rota_W_LOTNum,
	Rota_W_Barcode,
	Rota_W_Operator,
	Rota_W_Result,
	Rota_W_Degree,
	Rota_W_MaxCol, 
};

// 헤더
static const TCHAR*	g_lpszHeader_Rota_Worklist[] =
{
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SWVersion"),
	_T("LOTNum"),
	_T("Barcode"),
	_T("Operator"),
	_T("Result"),
	_T("Degree"),
	NULL
};

const int	iListAglin_Rota_Worklist[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_Rota_Worklist[] =
{
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};
// CList_Work_Rotate

class CList_Work_Rotate : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_Rotate)

public:
	CList_Work_Rotate();
	virtual ~CList_Work_Rotate();

	CFont		m_Font;
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	UINT Header_MaxNum();

	void InitHeader();
	void InsertFullData(__in const ST_CamInfo* pstCamInfo);
	void SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo);

	void GetData(UINT nRow, UINT &DataNum, CString *Data);
};

#endif // List_Work_Rotate_h__


