﻿// List_Work_CenterPoint.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_CenterPoint.h"


// CList_Work_CenterPoint

IMPLEMENT_DYNAMIC(CList_Work_CenterPoint, CListCtrl)

CList_Work_CenterPoint::CList_Work_CenterPoint()
{

	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_CenterPoint::~CList_Work_CenterPoint()
{
	m_Font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CList_Work_CenterPoint, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CList_Work_CenterPoint 메시지 처리기입니다.
int CList_Work_CenterPoint::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_CenterPoint::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

// 	int iColWidth[CP_W_MaxCol] = { 0, };
// 	int iColDivide = 0;
// 	int iUnitWidth = 0;
// 	int iMisc = 0;
// 
// 	CRect rectClient;
// 	GetClientRect(rectClient);
// 
// 	
// 
// 	for (int nCol = 0; nCol <CP_W_MaxCol; nCol++)
// 		iColDivide += iHeaderWidth_CP_Worklist[nCol];
// 
// 	for (int nCol = 0; nCol < CP_W_MaxCol; nCol++)
// 		SetColumnWidth(nCol, iHeaderWidth_CP_Worklist[nCol]);

	CRect rc;
	GetClientRect(&rc);

	for (int nCol = CP_W_Time; nCol < CP_W_MaxCol; nCol++)
	{
		//SetColumnWidth(nCol, (rc.Width()) / (12 - CP_W_Time));
		SetColumnWidth(nCol, iHeaderWidth_CP_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
BOOL CList_Work_CenterPoint::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: Header_MaxNum
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
UINT CList_Work_CenterPoint::Header_MaxNum()
{
	return (UINT)CP_W_MaxCol;
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_CenterPoint::InitHeader()
{
	for (int nCol = 0; nCol < CP_W_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_CP_Worklist[nCol], iListAglin_CP_Worklist[nCol], iHeaderWidth_CP_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_CenterPoint::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;
	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));
	SetRectRow(iNewCount, pstCamInfo);
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:33
// Desc.		:
//=============================================================================
void CList_Work_CenterPoint::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

 	CString strText;

// 	strText.Format(_T("%s"), pstCamInfo->szIndex);
// 	SetItemText(nRow, CP_W_Recode, strText);

	strText.Format(_T("%s-%s"), pstCamInfo->szDay, pstCamInfo->szTime);
	SetItemText(nRow, CP_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, CP_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, CP_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, CP_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, CP_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, CP_W_Barcode, strText);

	strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	SetItemText(nRow, CP_W_Operator, strText);

	if (pstCamInfo->nJudgment == TR_UserStop)
	{
		strText.Format(_T("%s"), g_TestEachResult[TR_UserStop].szText);
		SetItemText(nRow, CP_W_Result, strText);

		strText.Format(_T(""));
		for (int i = CP_W_PosX; i < CP_W_MaxCol; i++)
		{
			SetItemText(nRow, i, strText);
		}
	}
	else
	{
		strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stCenterPointData.nResult].szText);
		SetItemText(nRow, CP_W_Result, strText);

		if (pstCamInfo->stCenterPointData.nResult == TER_Init)
		{
			for (int t = 0; t < CP_W_OffsetY - CP_W_PosX + 1; t++)
			{
				strText.Format(_T("X"));
				SetItemText(nRow, CP_W_PosX + t, strText);
			}
		}
		else
		{
			if (pstCamInfo->stCenterPointData.iResult_Pos_X == CP_No_Target)
			{
				for (int t = 0; t < CP_W_OffsetY - CP_W_PosX + 1; t++)
				{
					strText.Format(_T("No detect"));
					SetItemText(nRow, CP_W_PosX + t, strText);
				}
			}
			else
			{
				strText.Format(_T("%d"), pstCamInfo->stCenterPointData.iResult_Pos_X);
				SetItemText(nRow, CP_W_PosX, strText);

				strText.Format(_T("%d"), pstCamInfo->stCenterPointData.iResult_Pos_Y);
				SetItemText(nRow, CP_W_PosY, strText);

				strText.Format(_T("%d"), pstCamInfo->stCenterPointData.iResult_Offset_X);
				SetItemText(nRow, CP_W_OffsetX, strText);

				strText.Format(_T("%d"), pstCamInfo->stCenterPointData.iResult_Offset_Y);
				SetItemText(nRow, CP_W_OffsetY, strText);
			}
		}
	}
}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 12:43
// Desc.		:
//=============================================================================
void CList_Work_CenterPoint::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = CP_W_MaxCol;
	for (int t = 0; t < CP_W_MaxCol; t++)
		Data[t] = GetItemText(nRow, t);
}
