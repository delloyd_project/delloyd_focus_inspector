// Wnd_MotionTeach.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MotionTeach.h"


// CWnd_MotionTeach

IMPLEMENT_DYNAMIC(CWnd_MotionTeach, CWnd)

CWnd_MotionTeach::CWnd_MotionTeach()
{
	m_pstDevice		= NULL;
	m_pstMotionSeq	= NULL;
	m_pstSeqStep	= NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_MotionTeach::~CWnd_MotionTeach()
{
	m_font.DeleteObject();
}



BEGIN_MESSAGE_MAP(CWnd_MotionTeach, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_MS_CB_SEQUENCE_SELECT, &CWnd_MotionTeach::OnEnSelectSequenceCombo)
	ON_CBN_SELCHANGE(IDC_MS_CB_STEP_SELECT,		&CWnd_MotionTeach::OnEnSelectStepCombo)

	ON_BN_CLICKED(IDC_MS_BN_POSITION_SET,	OnBnClickedBnPositionSet)
	ON_BN_CLICKED(IDC_MS_BN_SEQUENCE_SAVE,	OnBnClickedBnSequenceSave)

	ON_BN_CLICKED(IDC_MS_BN_STEP_TEST,		OnBnClickedBnStepTest)
	ON_BN_CLICKED(IDC_MS_BN_SEQUENCE_TEST,	OnBnClickedBnSequenceTest)
	ON_BN_CLICKED(IDC_MS_BN_MOTION_STOP,	OnBnClickedBnMotionStop)
END_MESSAGE_MAP()



// CWnd_MotionTeach 메시지 처리기입니다.




int CWnd_MotionTeach::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER;
	DWORD dwStyle	= WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// Static
	for (UINT nIdx = ST_MS_SEQUENCE_SELECT; nIdx < ST_MS_MAXNUM; nIdx++)
	{
		if (nIdx == ST_MS_MOTION_TEST)
		{
			m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		} 
		else
		{
			m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		}

		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 10.0F);
		m_st_Item[nIdx].Create(g_szMotionTeachStatic[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	// Combo box
	for (UINT nIdx = CB_MS_SEQUENCE_SELECT; nIdx < CB_MS_MAXNUM; nIdx++)
	{
		m_cb_Item[nIdx].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_MS_CB_SEQUENCE_SELECT + nIdx);
		m_cb_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; NULL != g_szSequenceSelect[nIdx]; nIdx++)
		m_cb_Item[CB_MS_SEQUENCE_SELECT].AddString(g_szSequenceSelect[nIdx]);

	for (UINT nIdx = 0; NULL != g_szStepSelect[nIdx]; nIdx++)
		m_cb_Item[CB_MS_STEP_SELECT].AddString(g_szStepSelect[nIdx]);

	m_cb_Item[CB_MS_SEQUENCE_SELECT].SetCurSel(0);
	m_cb_Item[CB_MS_STEP_SELECT].SetCurSel(0);

	// Edit
	for (UINT nIdx = ED_MS_SEQUENCE_NAME; nIdx < ED_MS_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwEtStyle, rectDummy, this, IDC_MS_ED_SEQUENCE_NAME + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T(""));
	}

	// List Ctrl
	for (UINT nIdx = LC_MS_STEP_TABLE; nIdx < LC_MS_MAXNUM; nIdx++)
	{
		m_lc_Item[nIdx].Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_MS_LC_STEP_TABLE + nIdx);
	}

	// Button
	for (UINT nIdx = BN_MS_POSITION_SET; nIdx < BN_MS_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szMotionTeachButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_MS_BN_POSITION_SET + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	// Group Ctrl
	m_Group_Name.SetTitle(L"MOTION SEQUENCE");

	if (!m_Group_Name.Create(_T("MOTION SEQUENCE"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	return 0;
}


void CWnd_MotionTeach::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin = 7;
	int iSpacing = 5;

	int iLeft = 0;
	int iTop = iMargin;
	int iWidth = cx;
	int iHeight = cy - iMargin - iMargin;
	int iEdWidth = (iWidth - iMargin - iMargin - (iSpacing * 5)) / 6;
	int iLcWidth = (iWidth - iMargin - iMargin - (iSpacing * 2)) / 3;
	int iBnWidth = (iWidth - iMargin - iMargin - (iSpacing * 2)) / 3;

	int iStTitleNameH = 37;
	int iEditHeight = 24;
	int iListHeight = iHeight - iStTitleNameH - iEditHeight - (iSpacing * 2);

	m_Group_Name.MoveWindow(iLeft, iTop, iWidth, iHeight);

	iTop = 30 + iMargin;

	iLeft = iMargin;
	m_st_Item[ST_MS_SEQUENCE_SELECT].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

	iLeft += iEdWidth + iSpacing;
	m_cb_Item[CB_MS_SEQUENCE_SELECT].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

	iTop += iEditHeight + iMargin;

	iLeft = iMargin;
	m_st_Item[ST_MS_SEQUENCE_NAME].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

	iLeft += iEdWidth + iSpacing;
	m_ed_Item[ED_MS_SEQUENCE_NAME].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

	iTop += iEditHeight + iMargin;

	iLeft = iMargin;
	m_st_Item[ST_MS_STEP_COUNT].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

	iLeft += iEdWidth + iSpacing;
	m_ed_Item[ED_MS_STEP_COUNT].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

	iTop += iEditHeight + iMargin;

	iLeft = iMargin;
	m_bn_Item[BN_MS_POSITION_SET].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

	iLeft += iEdWidth + iSpacing;
	m_bn_Item[BN_MS_SEQUENCE_SAVE].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);


	iTop = 30 + iMargin;

	int iLeft_row2 = iLeft + iEdWidth + iSpacing;

	iLeft = iLeft_row2;
	m_st_Item[ST_MS_STEP_SELECT].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

	iLeft += iEdWidth + iSpacing;
	m_cb_Item[CB_MS_STEP_SELECT].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

	iTop += iEditHeight + iMargin;
	
	iLeft = iLeft_row2;
	m_lc_Item[LC_MS_STEP_TABLE].MoveWindow(iLeft, iTop, iLcWidth, iListHeight);


	iTop = 30 + iMargin;

	int iLeft_row3 = iLeft + iLcWidth + iSpacing;

	iLeft = iLeft_row3;
	m_st_Item[ST_MS_MOTION_TEST].MoveWindow(iLeft, iTop, iLcWidth, iEditHeight);

	iTop += iEditHeight + iMargin;

	m_bn_Item[BN_MS_STEP_TEST].MoveWindow(iLeft, iTop, iLcWidth, iEditHeight);

	iTop += iEditHeight + iMargin;

	m_bn_Item[BN_MS_SEQUENCE_TEST].MoveWindow(iLeft, iTop, iLcWidth, iEditHeight);

	iTop += iEditHeight + iMargin;

	m_bn_Item[BN_MS_MOTION_STOP].MoveWindow(iLeft, iTop, iLcWidth, iEditHeight);
}


BOOL CWnd_MotionTeach::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}


BOOL CWnd_MotionTeach::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if ((::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 86)) // Ctrl + V
		{
			CWnd* pWnd = CWnd::FromHandle(pMsg->hwnd);
			pWnd->SetWindowText(_T(""));
			pMsg->message = WM_PASTE;
		}

		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 67)  // Ctrl + C
			pMsg->message = WM_COPY;

		// 		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 90)  // Ctrl + Z
		// 			pMsg->message = WM_UNDO;

		break;
	}

	return CWnd::PreTranslateMessage(pMsg);
}


void CWnd_MotionTeach::OnEnSelectSequenceCombo()
{
	SetUpdataData();
}

void CWnd_MotionTeach::OnEnSelectStepCombo()
{
	SetUpdataData();
}

void CWnd_MotionTeach::OnBnClickedBnSequenceSave()
{
	if (m_pstDevice == NULL)
		return;

	GetUpdataData();

	m_pstDevice->MotionManager.SaveMotionParam();
}

void CWnd_MotionTeach::OnBnClickedBnPositionSet()
{
	if (m_pstDevice == NULL)
		return;

	SetUpdatePosition();
}

void CWnd_MotionTeach::OnBnClickedBnStepTest()
{
	if (m_pstDevice == NULL)
		return;

	int iSeq		= m_cb_Item[CB_MS_SEQUENCE_SELECT].GetCurSel();
	int iStep		= m_cb_Item[CB_MS_STEP_SELECT].GetCurSel();

	m_pstMotionSeq	= &m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];
	m_pstSeqStep	= &m_pstMotionSeq->stSeqStep[iStep];

	m_pstDevice->MotionManager.SequenceStepMove(m_pstSeqStep);
}

void CWnd_MotionTeach::OnBnClickedBnSequenceTest()
{
	if (m_pstDevice == NULL)
		return;

	int iSeq		= m_cb_Item[CB_MS_SEQUENCE_SELECT].GetCurSel();
	m_pstMotionSeq	= &m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];

	m_pstDevice->MotionManager.MotionSequenceMove(m_pstMotionSeq);
}

void CWnd_MotionTeach::OnBnClickedBnMotionStop()
{
	if (m_pstDevice == NULL)
		return;

	for (int iAxis = 0; iAxis < MotorAxisNum; iAxis++)
	{
		m_pstDevice->MotionManager.SetMotorSStop(iAxis);
	}
}

void CWnd_MotionTeach::SetUpdataData()
{
	if (m_pstDevice == NULL)
		return;

	CString strData;

	int iSeq		= m_cb_Item[CB_MS_SEQUENCE_SELECT].GetCurSel();
	int iStep		= m_cb_Item[CB_MS_STEP_SELECT].GetCurSel();

	m_pstMotionSeq	= &m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];
	m_pstSeqStep	= &m_pstMotionSeq->stSeqStep[iStep];

	strData = m_pstMotionSeq->szSequenceName;
	m_ed_Item[ED_MS_SEQUENCE_NAME].SetWindowTextW(strData);

	strData.Format(_T("%d"), m_pstMotionSeq->nStepCount);
	m_ed_Item[ED_MS_STEP_COUNT].SetWindowTextW(strData);

	m_lc_Item[LC_MS_STEP_TABLE].SetPtr_MotionStep(m_pstSeqStep);
	m_lc_Item[LC_MS_STEP_TABLE].InsertFullData();
}


void CWnd_MotionTeach::SetUpdatePosition()
{
	if (m_pstDevice == NULL)
		return;

	CString strData;

	int iSeq		= m_cb_Item[CB_MS_SEQUENCE_SELECT].GetCurSel();
	int iStep		= m_cb_Item[CB_MS_STEP_SELECT].GetCurSel();

	m_pstMotionSeq	= &m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];
	m_pstSeqStep	= &m_pstMotionSeq->stSeqStep[iStep];

	m_ed_Item[ED_MS_SEQUENCE_NAME].GetWindowTextW(strData);
	m_pstMotionSeq->szSequenceName = strData;

	m_ed_Item[ED_MS_STEP_COUNT].GetWindowTextW(strData);
	m_pstMotionSeq->nStepCount = _ttoi(strData);

	m_lc_Item[LC_MS_STEP_TABLE].SetPtr_MotionStep(m_pstSeqStep);

	for (int iAxis = 0; iAxis < MotorAxisNum; iAxis++)
	{
		m_pstSeqStep->stAxisParam[iAxis].dbPos = m_pstDevice->MotionManager.GetCurrentPos(iAxis);
		m_pstSeqStep->stAxisParam[iAxis].dbVel = m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[iAxis].dbVel;
		m_pstSeqStep->stAxisParam[iAxis].dbAcc = m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[iAxis].dbAcc;
		m_pstSeqStep->stAxisParam[iAxis].dbDec = m_pstDevice->MotionManager.m_AllMotorData.pMotionParam[iAxis].dbAcc;
	}

	m_lc_Item[LC_MS_STEP_TABLE].InsertFullData();
}

void CWnd_MotionTeach::GetUpdataData()
{
	if (m_pstDevice == NULL)
		return;

	CString strData;

	int iSeq		= m_cb_Item[CB_MS_SEQUENCE_SELECT].GetCurSel();
	int iStep		= m_cb_Item[CB_MS_STEP_SELECT].GetCurSel();

	m_pstMotionSeq	= &m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];
	m_pstSeqStep	= &m_pstMotionSeq->stSeqStep[iStep];

	m_ed_Item[ED_MS_SEQUENCE_NAME].GetWindowTextW(strData);
	m_pstMotionSeq->szSequenceName = strData;

	m_ed_Item[ED_MS_STEP_COUNT].GetWindowTextW(strData);
	m_pstMotionSeq->nStepCount = _ttoi(strData);

	m_lc_Item[LC_MS_STEP_TABLE].SetPtr_MotionStep(m_pstSeqStep);
	m_lc_Item[LC_MS_STEP_TABLE].GetFullData();
}
