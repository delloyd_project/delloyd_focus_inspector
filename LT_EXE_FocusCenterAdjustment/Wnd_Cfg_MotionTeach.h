﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_MotionTeach.h
// Created	:	2016/3/14 - 10:56
// Modified	:	2016/3/14 - 10:56
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_MotionTeach_h__
#define Wnd_Cfg_MotionTeach_h__

#pragma once
#include "Wnd_BaseView.h"
#include "Def_Enum.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"
#include "File_WatchList.h"
#include "Def_TestDevice.h"

enum enCMT_Static
{
	STI_CMT_UnLoading_Motion,	// Unloading
	STI_CMT_UL_OFFSET_Z,		// Axis Z
	STI_CMT_UL_OFFSET_Y,		// Axis Y
	STI_CMT_UL_OFFSET_X,		// Axis X

	STI_CMT_Vision_Motion,		// Vision Motion
	STI_CMT_VM_OFFSET_Z,		// Axis Z
	STI_CMT_VM_OFFSET_Y,		// Axis Y
	STI_CMT_VM_OFFSET_X,		// Axis X

	STI_CMT_Displace_Motion_A,	// Displace Motion - A
	STI_CMT_DM1_OFFSET_Z,		// Axis Z
	STI_CMT_DM1_OFFSET_Y,		// Axis Y
	STI_CMT_DM1_OFFSET_X,		// Axis X

	STI_CMT_Displace_Motion_B,	// Displace Motion - B
	STI_CMT_DM2_OFFSET_Z,		// Axis Z
	STI_CMT_DM2_OFFSET_Y,		// Axis Y
	STI_CMT_DM2_OFFSET_X,		// Axis X

	STI_CMT_AF_Position,		// AF Position
	STI_CMT_AP_OFFSET_Z,		// Axis Z
	STI_CMT_AP_OFFSET_Y,		// Axis Y
	STI_CMT_AP_OFFSET_X,		// Axis X

	STI_CMT_MAXNUM,				
};

static LPCTSTR	g_szCMT_Static[] =
{
	_T("Unloading Motion"),
	_T("Offset Z [mm]"),
	_T("Offset Y [mm]"),
	_T("Offset X [mm]"),

	_T("Vision Motion"),
	_T("Offset Z [mm]"),
	_T("Offset Y [mm]"),
	_T("Offset X [mm]"),

	_T("Displace Motion - A"),
	_T("Offset Z [mm]"),
	_T("Offset Y [mm]"),
	_T("Offset X [mm]"),

	_T("Displace Motion - B"),
	_T("Offset Z [mm]"),
	_T("Offset Y [mm]"),
	_T("Offset X [mm]"),

	_T("AF Position"),
	_T("Offset Z [mm]"),
	_T("Offset Y [mm]"),
	_T("Offset X [mm]"),

	NULL
};

enum enCMT_Edit
{
	Edit_CMT_UL_SEQ_NAME,		// SEQ_NAME
	Edit_CMT_UL_OFFSET_Z,		// Axis Z
	Edit_CMT_UL_OFFSET_Y,		// Axis Y
	Edit_CMT_UL_OFFSET_X,		// Axis X

	Edit_CMT_VM_SEQ_NAME,		// SEQ_NAME
	Edit_CMT_VM_OFFSET_Z,		// Axis Z
	Edit_CMT_VM_OFFSET_Y,		// Axis Y
	Edit_CMT_VM_OFFSET_X,		// Axis X

	Edit_CMT_DM1_SEQ_NAME,		// SEQ_NAME
	Edit_CMT_DM1_OFFSET_Z,		// Axis Z
	Edit_CMT_DM1_OFFSET_Y,		// Axis Y
	Edit_CMT_DM1_OFFSET_X,		// Axis X

	Edit_CMT_DM2_SEQ_NAME,		// SEQ_NAME
	Edit_CMT_DM2_OFFSET_Z,		// Axis Z
	Edit_CMT_DM2_OFFSET_Y,		// Axis Y
	Edit_CMT_DM2_OFFSET_X,		// Axis X

	Edit_CMT_AP_SEQ_NAME,		// SEQ_NAME
	Edit_CMT_AP_OFFSET_Z,		// Axis Z
	Edit_CMT_AP_OFFSET_Y,		// Axis Y
	Edit_CMT_AP_OFFSET_X,		// Axis X

	Edit_CMT_MAXNUM,
};


enum enCMT_Combo
{
	Combo_CMT_UnloadingMotion,
	Combo_CMT_VisionMotion,
	Combo_CMT_AFPosition,		
	Combo_CMT_DisplaceMotion_A,		
	Combo_CMT_DisplaceMotion_B,
	Combo_CMT_MAXNUM,
};

static LPCTSTR	g_szCMT_Combo[] =
{
	_T(" Sequence : 1"),
	_T(" Sequence : 2"),
	_T(" Sequence : 3"),
	_T(" Sequence : 4"),
	_T(" Sequence : 5"),
	_T(" Sequence : 6"),
	_T(" Sequence : 7"),
	_T(" Sequence : 8"),
	_T(" Sequence : 9"),
	_T(" Sequence : 10"),

	NULL
};

//-----------------------------------------------------------------------------
// CWnd_Cfg_VIsion
//-----------------------------------------------------------------------------
class CWnd_Cfg_MotionTeach : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_MotionTeach)

public:
	CWnd_Cfg_MotionTeach();
	virtual ~CWnd_Cfg_MotionTeach();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg void	OnEnSelectComboVM		();
	afx_msg void	OnEnSelectComboAF		();
	afx_msg void	OnEnSelectComboUL		();
	afx_msg void	OnEnSelectComboDMA		();
	afx_msg void	OnEnSelectComboDMB		();

	ST_Device*		m_pDevice;
	CFont			m_font_Data, m_font_Cb;

	CVGStatic		m_st_Item[STI_CMT_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[Edit_CMT_MAXNUM];
	CComboBox		m_cb_Item[Combo_CMT_MAXNUM];

	// UI에 세팅 된 데이터 -> 구조체
	void		GetUIData			(__out ST_ModelInfo& stModelInfo);

	// 구조체 -> UI에 세팅
	void		SetUIData			(__in const ST_ModelInfo* pModelInfo);

public:
	
	// 모델 데이터를 UI에 표시
	void		SetModelInfo		(__in const ST_ModelInfo* pModelInfo);
	// UI에 표시된 데이터의 구조체 반환	
	void		GetModelInfo		(__out ST_ModelInfo& stModelInfo);
	// 디바이스
	void		SetPtr_Device		(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pDevice = pstDevice;
	};
};

#endif // Wnd_Cfg_AF_h__


