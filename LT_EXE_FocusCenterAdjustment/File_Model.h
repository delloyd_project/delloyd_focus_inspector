﻿#ifndef File_Model_h__
#define File_Model_h__

#pragma once

#include "Def_DataStruct.h"

#define		Model_AppName		_T("Model")
#define		Common_AppName		_T("Common")
#define		ModelCode_KeyName	_T("ModelCode")

class CFile_Model
{
public:
	CFile_Model();
	~CFile_Model();

	BOOL	LoadModelFile			(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	SaveModelFile			(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	BOOL	Load_Common				(__in LPCTSTR szPath, __out ST_ModelInfo& stModelInfo);
	BOOL	Save_Common				(__in LPCTSTR szPath, __in const ST_ModelInfo* pstModelInfo);

	// 전류
	BOOL	LoadCurrentOpFile		(__in LPCTSTR szPath, __out ST_LT_TI_Current& stCurrentInfo);
	BOOL	SaveCurrentOpFile		(__in LPCTSTR szPath, __in const ST_LT_TI_Current* pstCurrentInfo);


	// 광축
	BOOL	LoadCenterPointOpFile	(__in LPCTSTR szPath, __out ST_LT_TI_CenterPoint& stCenterPointInfo);
	BOOL	SaveCenterPointOpFile	(__in LPCTSTR szPath, __in const ST_LT_TI_CenterPoint* pstCenterPointInfo);

	// 로테이션
	BOOL	SaveRotateFile			(__in LPCTSTR szPath, __in const ST_LT_TI_Rotate* pstRotateInfo);
	BOOL	LoadRotateFile			(__in LPCTSTR szPath, __out ST_LT_TI_Rotate& stRotateInfo);

	// EIAJ
	BOOL	LoadEIAJFile			(__in LPCTSTR szPath, __out ST_LT_TI_EIAJ& stResolutionInfo);
	BOOL	SaveEIAJFile			(__in LPCTSTR szPath, __in const ST_LT_TI_EIAJ* pstResolutionInfo);

	// SFR
	BOOL	LoadSFRFile				(__in LPCTSTR szPath, __out ST_LT_TI_SFR& stSFRInfo);
	BOOL	SaveSFRlFile			(__in LPCTSTR szPath, __in const ST_LT_TI_SFR* pstSFRInfo);

	// Pogo File
	BOOL	LoadPogoIniFile			(__in LPCTSTR szPath, __out ST_PogoInfo& stPogoInfo);
	BOOL	SavePogoIniFile			(__in LPCTSTR szPath, __in const ST_PogoInfo* pstPogoInfo);

	BOOL	LoadPogoCount			(__in LPCTSTR szPath, __in UINT nPogoIdx, __out DWORD& dwCount);
	BOOL	SavePogoCount			(__in LPCTSTR szPath, __in UINT nPogoIdx, __in DWORD dwCount);

	// DriverCount File
	BOOL	LoadDriverCountIniFile	(__in LPCTSTR szPath, __out ST_DriverCountInfo& stDriverCountInfo);
	BOOL	SaveDriverCountIniFile	(__in LPCTSTR szPath, __in const ST_DriverCountInfo* pstDriverCountInfo);

	BOOL	LoadDriverCount			(__in LPCTSTR szPath, __in UINT nDriverCountIdx, __out DWORD& dwCount);
	BOOL	SaveDriverCount			(__in LPCTSTR szPath, __in UINT nDriverCountIdx, __in DWORD dwCount);

	// AF 옵션
	BOOL	LoadAFInfoFile			(__in LPCTSTR szPath, __out ST_AFInfo& stAFInfo);
	BOOL	SaveAFInfoFile			(__in LPCTSTR szPath, __out const ST_AFInfo* pstAFInfo);

	// Custom Teach
	BOOL	LoadCustomTeachFile		(__in LPCTSTR szPath, __out ST_CustomTeach& stCustomTeach);
	BOOL	SaveCustomTeachFile		(__in LPCTSTR szPath, __out const ST_CustomTeach* pstCustomTeach);

	// 이물	
	BOOL	LoadParticleOpFile		(__in LPCTSTR szPath, __out ST_LT_TI_Particle& stParticleInfo);
	BOOL	SaveParticleOpFile		(__in LPCTSTR szPath, __in const ST_LT_TI_Particle* pstParticleInfo);

	// MES
	BOOL	SaveMESFile				(__in LPCTSTR szPath, __in const ST_InspectionInfo* pstInspectInfo, __in LPCTSTR szEqpId);
	BOOL	MakeMESCamData			(__out CString& szCamData, __in const ST_InspectionInfo* pstInspectInfo);


};

#endif // File_Model_h__
