﻿//*****************************************************************************
// Filename	: 	Grid_ModelInfo.h
// Created	:	2016/1/12 - 13:56
// Modified	:	2016/1/12 - 13:56
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Grid_ModelInfo_h__
#define Grid_ModelInfo_h__

#pragma once
#include "Grid_Base.h"
#include "Def_DataStruct.h"

class CGrid_ModelInfo : public CGrid_Base
{
public:
	CGrid_ModelInfo();
	virtual ~CGrid_ModelInfo();

protected:
	virtual void	OnSetup				();
	virtual int		OnHint				(int col, long row, int section, CString *string);
	virtual void	OnGetCell			(int col, long row, CUGCell *cell);
	virtual void	OnDrawFocusRect		(CDC *dc, RECT *rect);

	// 그리드 외형 및 내부 문자열을 채운다.
	virtual void	DrawGridOutline		();

	// 셀 갯수 가변에 따른 다시 그리기 위한 함수
	virtual void	CalGridOutline		();

	// 헤더를 초기화
	void			InitHeader			();

	CFont			m_font_Header;
	CFont			m_font_Data;

public:

	void		SetModelInfo			(__in const ST_ModelInfo* pstModelInfo);

	void		UpdatePogoCnt			(__in const ST_PogoInfo* pstPogoInfo);
	void		UpdateDriverCount		(__in const ST_DriverCountInfo* pstDriverCountInfo);
};

#endif // Grid_ModelInfo_h__

