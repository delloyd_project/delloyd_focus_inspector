﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Model.cpp
// Created	:	2016/3/14 - 10:57
// Modified	:	2016/3/14 - 10:57
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_Model.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_Model.h"
#include "resource.h"
#include "Def_WindowMessage.h"

typedef enum Model_ID
{
	IDC_ED_ModelName = 1001,
	IDC_ED_Voltage,
	IDC_ED_Cam_Delay,
	IDC_CB_Cam_Status,
	IDC_BTN_MODULE_FIX_CHECK,
	IDC_BTN_MOVE_STAGE_CHECK,
	IDC_LIST_INDICATORTOP
};

// CWnd_Cfg_Model
IMPLEMENT_DYNAMIC(CWnd_Cfg_Model, CWnd_BaseView)

CWnd_Cfg_Model::CWnd_Cfg_Model()
{
	VERIFY(m_font_Data.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_Model::~CWnd_Cfg_Model()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_Model, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CWnd_Cfg_Model message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
int CWnd_Cfg_Model::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < STI_ML_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szModel_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < Edit_ML_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_ModelName + nIdx);
		m_ed_Item[nIdx].SetFont(&m_font_Data);
		
		if (nIdx != Edit_ML_ModelName)
			m_ed_Item[nIdx].SetValidChars(_T("0123456789.-"));
	}

//  [1/2/2019 ysJang] Camera 방향 Combo Box 추가
	for (UINT nIdex = 0; nIdex < ComboI_ML_MAXNUM; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_Cam_Status + nIdex);
		m_cb_Item[nIdex].SetFont(&m_font_Data);
	}

	for (UINT nIdx = 0; NULL != g_szModel_Quad_Rect_Pos[nIdx]; nIdx++)
		m_cb_Item[ComboI_ML_Quad_Rect_Pos].AddString(g_szModel_Quad_Rect_Pos[nIdx]);

	m_cb_Item[ComboI_ML_Quad_Rect_Pos].SetCurSel(0);

//  [3/4/2019 ysjang] Indicator Op List
	m_ListIndicatorOp.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_INDICATORTOP);

//  [12/24/2018 ysJang] Check Box 추가
	for (UINT nIdx = BT_ML_MODULE_FIX_CHECK; nIdx < BT_ML_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szModel_Btn[nIdx], dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_MODULE_FIX_CHECK + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font_Data);

		m_bn_Item[nIdx].SetImage(IDB_UNCHECKED_16);
		m_bn_Item[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_bn_Item[nIdx].SizeToContent();
		m_bn_Item[nIdx].SetCheck(BST_UNCHECKED);
	}
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin			= 10;
	int iSpacing		= 5;
	int iCateSpacing	= 5;

	int iLeft	= iMagrin;
	int iTop	= iMagrin;
	int iWidth	= cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iCtrlWidth	= 0;
	int iCtrlHeight = 25;
	int iHalfWidth	= (iWidth - iCateSpacing- iCateSpacing) / 3;
	int iLeftSub  	= 0;
	int iTempWidth  = 0;

	int iCapWidth	= 140;

	iCtrlWidth = (iHalfWidth - iCateSpacing) / 2;

	for (UINT nIdx = 0; nIdx <= STI_ML_Rect_Position; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		
		iLeftSub = iLeft + iCapWidth + iCateSpacing;

		switch (nIdx)
		{
		case STI_ML_ModelName:
			m_ed_Item[Edit_ML_ModelName].MoveWindow(iLeftSub, iTop, iHalfWidth, iCtrlHeight);
			break;
		case STI_ML_Cam_Delay:
			m_ed_Item[Edit_ML_CameraDelay].MoveWindow(iLeftSub, iTop, iHalfWidth, iCtrlHeight);
			break;
		case STI_ML_Cam_Volt:
			m_ed_Item[Edit_ML_Voltage].MoveWindow(iLeftSub, iTop, iHalfWidth, iCtrlHeight);
			break;
		//  [1/2/2019 ysJang] Camera 방향 ComboBox 추가
 		case STI_ML_Rect_Position:
			m_cb_Item[ComboI_ML_Quad_Rect_Pos].MoveWindow(iLeftSub, iTop, iHalfWidth, iCtrlHeight);
 			break;
		default:
			break;
		}
		iTop += iCtrlHeight + iCateSpacing;
	}

	//  [3/4/2019 ysJang] 
	//iLeft = TempLeft;
	int List_W = iCapWidth + iHalfWidth + iSpacing;	//(Static_W + Combo_W + iSpacing);
	int List_H = iCtrlHeight * 3;

	m_ListIndicatorOp.MoveWindow(iLeft, iTop, List_W, List_H);

//  [12/24/2018 ysJang] Check Box 추가
	int iLeftBtn = iLeftSub + iHalfWidth + iCateSpacing *5;
	iTop = iMagrin;
	m_bn_Item[BT_ML_MODULE_FIX_CHECK].MoveWindow(iLeftBtn, iTop, iHalfWidth, iCtrlHeight);
	iTop += iCtrlHeight + iCateSpacing;
	m_bn_Item[BT_ML_MOVE_STAGE_CHECK].MoveWindow(iLeftBtn, iTop, iHalfWidth, iCtrlHeight);
}

//=============================================================================
// Method		: OnBnClickedBnApply
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/19 - 14:51
// Desc.		:
//=============================================================================
// void CWnd_Cfg_Model::OnBnClickedBnApply()
// {
// 	CString strValue;
// 
// 	m_ed_Item[Edit_ML_CameraAlign].GetWindowText(strValue);
// 	UINT nAlign = _ttoi(strValue.GetBuffer());
// 
// 	strValue.ReleaseBuffer();
// 
// 	GetOwner()->SendNotifyMessage(WM_ALIGN, (WPARAM)nAlign, 0);
// }

void CWnd_Cfg_Model::OnBnClickedBnDistortion()
{
// 	if (m_bn_Distortion.GetCheck() == BST_CHECKED)
// 	{
// 		GetOwner()->SendNotifyMessage(WM_DISTORTION_CORRECT, 1, 0);
// 	}
// 	else
// 	{
// 		GetOwner()->SendNotifyMessage(WM_DISTORTION_CORRECT, 0, 0);
// 	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Model::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Model::PreTranslateMessage(MSG* pMsg)
{
	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: GetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 13:13
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::GetUIData(__out ST_ModelInfo& stModelInfo)
{
	CString strValue;	
	UINT	nData = 0;

	// 모델명
	m_ed_Item[Edit_ML_ModelName].GetWindowText(strValue);
	stModelInfo.szModelCode = strValue;

	m_ed_Item[Edit_ML_Voltage].GetWindowText(strValue);
	stModelInfo.fVoltage = (FLOAT)_ttof(strValue.GetBuffer());
	strValue.ReleaseBuffer();


	m_ed_Item[Edit_ML_CameraDelay].GetWindowText(strValue);
	stModelInfo.nCameraDelay = _ttoi(strValue.GetBuffer());
	strValue.ReleaseBuffer();

	//  [1/2/2019 ysJang] Quad_Rect_Pos
	stModelInfo.nQuadRectPos = m_cb_Item[ComboI_ML_Quad_Rect_Pos].GetCurSel();

	//
	m_ListIndicatorOp.GetCellData();

	//  [12/24/2018 ysJang] bModuleFixCheck, bMoveStageCheck
	stModelInfo.bModuleFixCheck = m_bn_Item[BT_ML_MODULE_FIX_CHECK].GetCheck();
	stModelInfo.bMoveStageCheck = m_bn_Item[BT_ML_MOVE_STAGE_CHECK].GetCheck();

}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::SetUIData(__in const ST_ModelInfo* pModelInfo)
{
	CString strValue;
	UINT	nData = 0;

	// 모델명
	m_ed_Item[Edit_ML_ModelName].SetWindowText(pModelInfo->szModelCode);

	strValue.Format(_T("%.1f"), pModelInfo->fVoltage);
	m_ed_Item[Edit_ML_Voltage].SetWindowText(strValue);

	strValue.Format(_T("%d"), pModelInfo->nCameraDelay);
	m_ed_Item[Edit_ML_CameraDelay].SetWindowText(strValue);

	//  [1/2/2019 ysJang] Cam_Rect_Pos
	m_cb_Item[ComboI_ML_Quad_Rect_Pos].SetCurSel(pModelInfo->nQuadRectPos);

	//  [3/4/2019 ysJang] Indicator Option List 추가
	m_ListIndicatorOp.SetPtr_Indicator(pModelInfo);
	m_ListIndicatorOp.InsertFullData();

	//  [12/24/2018 ysJang] MODULE_FIX_CHECK, MOVE_STAGE_CHECK
	m_bn_Item[BT_ML_MODULE_FIX_CHECK].SetCheck(pModelInfo->bModuleFixCheck);
	m_bn_Item[BT_ML_MOVE_STAGE_CHECK].SetCheck(pModelInfo->bMoveStageCheck);
	
	//m_bn_Distortion.SetCheck(pModelInfo->nDistortion);
}

//=============================================================================
// Method		: RefreshFileList
// Access		: protected  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2017/7/6 - 9:47
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::RefreshFileList(__in const CStringList* pFileList)
{
// 	m_cb_Item[ComboI_ML_I2CFile].ResetContent();
// 
// 	INT_PTR iFileCnt = pFileList->GetCount();
// 
// 	POSITION pos;
// 	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
// 	{
// 		m_cb_Item[ComboI_ML_I2CFile].AddString(pFileList->GetNext(pos));
// 	}
// 
// 	// 이전에 선택되어있는 파일 다시 선택
// 	if (!m_szI2CPath.IsEmpty())
// 	{
// 		int iSel = m_cb_Item[ComboI_ML_I2CFile].FindStringExact(0, m_szI2CPath);
// 
// 		if (0 <= iSel)
// 		{
// 			m_cb_Item[ComboI_ML_I2CFile].SetCurSel(iSel);
// 		}
// 	}
}

//=============================================================================
// Method		: SetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_ModelInfo * pModelInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::SetModelInfo(__in const ST_ModelInfo* pModelInfo)
{
	SetUIData(pModelInfo);
}

//=============================================================================
// Method		: GetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/4 - 10:42
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::GetModelInfo(__out ST_ModelInfo& stModelInfo)
{
	GetUIData(stModelInfo);
}
