﻿//*****************************************************************************
// Filename	: 	MotionSequence.h
// Created	:	2017/01/09 - 13:54
// Modified	:	2017/01/09 - 13:54
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef MotionSequence_h__
#define MotionSequence_h__

#include "Def_Motion.h"
#include "MotionManager.h"
#include "WLog.h"
#include "Def_DataStruct.h"
#include "DigitalIOCtrl.h"
#include "LT_Option.h"
#include "Dlg_Popup.h"
#pragma once

#define MotorOriginWaitTime	20000
#define DIoWaitTime			5000

//-----------------------------------------------------------------------------
// CMotionSequence
//-----------------------------------------------------------------------------
class CMotionSequence
{
public:
	CMotionSequence(LPVOID p = NULL);
	~CMotionSequence(void);

protected:


	BOOL	IsReadyOk	(UINT nPort, BOOL bStatus = TRUE);

	// 모터 원점 루틴
	BOOL	OriginProcedure(UINT nStep);

	// 모니터링
	void	OnMonitorSensorCheck();

	BOOL	m_bAreaCheck;
	BOOL	m_bOriginStop;
	BOOL	m_bXmlStop;
	BOOL	m_bIoLoopStop;
	BOOL	m_bDigitalIn[DI_NotUseBit_Max];

public:
	CDlg_Popup	m_Dlg_Popup;
	BOOL	MessageView(__in CString szText);

	// LOG 메시지 
	CWLog			m_Log;
	HWND			m_hOwnerWnd;

	// LOG 메시지 
	void SetLogMsgID(HWND hOwnerWnd, UINT nWM_ID, UINT nLogType = 0)
	{
		m_hOwnerWnd = hOwnerWnd;
		m_Log.SetOwner(m_hOwnerWnd, nWM_ID);
		m_Log.SetLogType(nLogType);
	}

	// 안전센서, 도어센서 사용 유무 
	stLT_Option*		m_pstOption;
	void SetLTOption(stLT_Option* pstOption)
	{
		if (pstOption == NULL)
			return;

		m_pstOption = pstOption;
	};

	// 모터 티칭 테이블 데이터
	ST_ModelInfo*		m_pstModelInfo;
	void SetModelInfoData(ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	};

	// Digitial 제어
	CDigitalIOCtrl*		m_pDigitalIOCtrl;
	void SetPtrDigitalIOCtrl(CDigitalIOCtrl* pDigitalIOCtrl)
	{
		if (pDigitalIOCtrl == NULL)
			return;

		m_pDigitalIOCtrl = pDigitalIOCtrl;
	};

 	// Motion 제어
	CMotionManager*		m_pMotion;
	void SetPtrMotionManger(CMotionManager* pMotion)
	{
		if (pMotion == NULL)
			return;

		m_pMotion = pMotion;
	};

	// 모터 원점 시작
	BOOL	AllMotorOrigin				();

	// XML
	BOOL	XmlProcedure				();

	// 광축 조정 시퀀스
	BOOL	Stage3AxisInit				();
	BOOL	CenterPointAdjustment		(int iOffsetX, int iOffsetY);
	BOOL	RotateAdjustment			(double dbDegree);

	// 포커스 조정 시퀀스
	BOOL	FocusAdjustment	
	(	__in int	iDetectCnt,			// 측정 영역 개수
		__in double dbStepDegree,		// 해상력 단위 각도.	
		__in double dbLensConchoid,		// 렌즈 360도 회전 시 나사선 간격
		__in int	iStepData,			// 각도 단위 해상력
		__in int	iMDevData,			// 해상력 측정 편차
		__in int*	iCurrData,			// 해상력 측정 데이터
		__in int*	iThrMin,			// 해상력 측정 데이터 하한선
		__in int*	iThrMax				// 해상력 측정 데이터 상한선
	);

	// Collet 측정 각도 
	BOOL	ColletDegreeMove		(__in AXT_MOTION_ABSREL_MODE nMode, __in double dDegree);

	// Focus Step 각도 
	BOOL	FocusingDegreeMove		(__in double dDegree, __in double dConchoid);

	// Focus Init 위치
	BOOL	FocusingInitMove		(__in double dInitY, __in double dInitR);

	// Interlock
	//BOOL	InterlockMove			(__in BOOL bOnOff);

	//CYL CONTROL
	//BOOL	CYLJigControl			(__in BOOL bOnOff);

	// Test Motion 
	//BOOL	TestMotionSequence		(__in BOOL bOnOff);

	// Module Fix/UnFix
	BOOL ModuleFixUnFixCYLMotion(__in BOOL bFixUnFix);

	// PCB Fix/UnFix
	BOOL PCBFixUnFixCYLMotion(__in BOOL bFixUnFix);

	// Driver In/Out
	BOOL DriverInOutCYLMotion(__in BOOL bFixUnFix);

	//Chart On/Off
	BOOL ChartOnoff(__in BOOL bOnOff);

	// Release CYL Motion
	BOOL ReleaseCYLMotion();

	// BUTTON CONTROL
	BOOL	ButtonLampControl		(__in UINT nPort, __in enum_IO_SignalType enType);

	// 타워 램프 색상, 3종
	BOOL	TowerLamp				(UINT nColor, __in BOOL bMode, long lTime = 2000);

	// 타워 램프 소리, 5종
	BOOL	TowerLampBuzzer			(UINT nSound, __in BOOL bMode, long lTime = 2000);

};

#endif // MotionSequence_h__

