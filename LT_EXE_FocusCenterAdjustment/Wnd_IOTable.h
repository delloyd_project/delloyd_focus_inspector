﻿//*****************************************************************************
// Filename	: 	Wnd_IOTable.c
// Created	:	2017/02/05 - 13:47
// Modified	:	2017/02/05 - 13:47
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_IOTable_h__
#define Wnd_IOTable_h__

#pragma once
#include "VGGroupWnd.h"
#include "VGStatic.h"
#include "Def_TestDevice.h"

// CWnd_IOTable

class CWnd_IOTable : public CWnd
{
	DECLARE_DYNAMIC(CWnd_IOTable)

public:
	CWnd_IOTable();
	virtual ~CWnd_IOTable();

	ST_Device*	m_pstDevice;

	void SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
 		m_nInCnt	= m_pstDevice->DigitalIOCtrl.GetInputPortCnt();
 		m_nOutCnt	= m_pstDevice->DigitalIOCtrl.GetOutputPortCnt();

		if (m_nInCnt <= 0)
		{
			m_nInCnt = DI_NotUseBit_Max;
		}

		if (m_nOutCnt <= 0)
		{
			m_nOutCnt = DO_NotUseBit_Max;
		}

	};

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnRangeCmds			(UINT nID);
	virtual BOOL	PreTranslateMessage	(MSG* pMsg);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	void			SetDeleteTimer		();

	DECLARE_MESSAGE_MAP()

protected:
	CVGGroupWnd		m_InputGroup;
	CVGGroupWnd		m_OutputGroup;

	CFont			m_font_Data;

	BOOL			m_Inport_Flag[255];

	CVGStatic		m_st_InputName[255];
	CVGStatic		m_st_InputStatus[255];

	CVGStatic		m_st_OutportName[255];
	CVGStatic		m_st_OutportStatus[255];

	UINT			m_nInCnt;
	UINT			m_nOutCnt;

	//	타이머
	HANDLE			m_hTimer_InputCheck;
	HANDLE			m_hTimerQueue;

	static VOID CALLBACK TimerRoutine_InputCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);

	void	CreateTimerQueue_Mon		();
	void	DeleteTimerQueue_Mon		();
	void	CreateTimer_InputCheck		();
	void	DeleteTimer_InputCheck		();
	void	OnMonitor_InputCheck		();

	void	UpdateInputStatus			();
	void	UpdateOutputStatus			();
};

#endif // Wnd_IOTable_h__
