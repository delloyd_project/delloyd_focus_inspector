﻿//*****************************************************************************
// Filename	: 	Overlay_Proc.cpp
// Created	:	2017/3/23 - 08:14
// Modified	:	2017/3/23 - 08:14
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Overlay_Proc.h"


COverlay_Proc::COverlay_Proc()
{
}


COverlay_Proc::~COverlay_Proc()
{
}


//=============================================================================
// Method		: Overlay_OpticalCenter
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_OpticalCenter_Opt stOption
// Parameter	: __in ST_OpticalCenter_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 15:53
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_OpticalCenter(__inout IplImage* lpImage, __in ST_CenterPoint_Op stOption, __in ST_CenterPoint_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	iLineSize = 2;
	iMarkSize = 15;
	dbFontSize = 0.6;

	
 
 	CRect	rtRect;
 
 
 	CPoint	ptCenter;
 
 	ptCenter.x = stData.iResult_RealPos_X;
 	ptCenter.y = stData.iResult_RealPos_Y;
 
 	if (0 >= ptCenter.x || 0 >= ptCenter.y)
 		return;
 
 	rtRect.left		= ptCenter.x - iMarkSize;
 	rtRect.top		= ptCenter.y;
 	rtRect.right	= ptCenter.x + iMarkSize;
 	rtRect.bottom	= ptCenter.y;
 	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);
 
 	rtRect.left		= ptCenter.x;
 	rtRect.top		= ptCenter.y - iMarkSize;
 	rtRect.right	= ptCenter.x;
 	rtRect.bottom	= ptCenter.y + iMarkSize;
 	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

	if (stData.bQuadRectUse)
	{
		ptCenter.x = stData.iQuadRect_Pos_X;
		ptCenter.y = stData.iQuadRect_Pos_Y;

		rtRect.left = ptCenter.x - iMarkSize;
		rtRect.top = ptCenter.y;
		rtRect.right = ptCenter.x + iMarkSize;
		rtRect.bottom = ptCenter.y;
		Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(150, 255, 0), iLineSize);

		rtRect.left = ptCenter.x;
		rtRect.top = ptCenter.y - iMarkSize;
		rtRect.right = ptCenter.x;
		rtRect.bottom = ptCenter.y + iMarkSize;
		Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(150, 255, 0), iLineSize);
	}
 
 	CString szText;
	if (stData.iResult_Offset_X < -99999 || stData.iResult_Offset_Y < -99999)
		szText.Format(_T("Center Point No Detect"));
	else
		szText.Format(_T("OFFSET X: %d OFFSET Y: %d"), stData.iResult_Offset_X, stData.iResult_Offset_Y);

	if (stData.bQuadRectResult == FALSE && stData.bQuadRectUse == TRUE)
	{
		szText.Format(_T("Camera State Fail"));
	}
 
	rtRect.left = lpImage->width / 2 - 180;
	rtRect.top = lpImage->height - 150;
 
 	if (TR_Pass == stData.nResult)
 	{
 		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 30, 255), iLineSize, dbFontSize, szText);
 	}
 	else
 	{
 		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
 	}
}

//=============================================================================
// Method		: Overlay_Current
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_ECurrent_Opt stOption
// Parameter	: __in ST_ECurrent_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 15:56
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Current(__inout IplImage* lpImage, __in ST_Current_Op stOption, __in ST_Current_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	CRect rtRect;
	CString szText;
	rtRect.SetRectEmpty();

	iLineSize = 2;
	iMarkSize = 5;
	dbFontSize = 0.6;

	szText.Format(_T("Current : %.1f mA"), stData.nCurrent);

	rtRect.left = lpImage->width / 2 - 90;
	rtRect.top = lpImage->height - 20;


	if (TR_Pass == stData.nResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}


//=============================================================================
// Method		: Overlay_SFR
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_SFR_Opt stOption
// Parameter	: __in ST_SFR_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 16:02
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_SFR(__inout IplImage* lpImage, __in ST_SFR_Op stOption, __in ST_SFR_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	iLineSize = 2;
	iMarkSize = 5;
	dbFontSize = 0.5;


	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
	{

		if (TRUE == stOption.stSFR_Region[nIdx].bEnable)
		{
			/*if (m_bTestmode == TRUE)
				rtRect = stData.rtTestPicROI[nIdx];
				else
				rtRect = stOption.stRegion[nIdx].rtROI;*/

			int Width = stOption.stSFR_InitRegion[nIdx].nWidth;
			int Height = stOption.stSFR_InitRegion[nIdx].nHeight;
			rtRect.left = stOption.stSFR_InitRegion[nIdx].nPos_X - (Width / 2);
			rtRect.top = stOption.stSFR_InitRegion[nIdx].nPos_Y - (Height / 2);
			rtRect.right = rtRect.left + (Width);
			rtRect.bottom = rtRect.top + (Height);
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

			rtRectText.SetRectEmpty();

			switch (stOption.stSFR_Region[nIdx].nFont)
			{
			case 0:
				rtRectText.left = rtRect.left - 70;
				rtRectText.top  = (rtRect.top + rtRect.bottom) / 2;
				break;
			case 1:
				rtRectText.left = rtRect.right + 10;
				rtRectText.top  = (rtRect.top + rtRect.bottom) / 2;
				break;
			case 2:
				rtRectText.left = rtRect.left;
				rtRectText.top  = rtRect.top - 40;
				break;
			case 3:
				rtRectText.left = rtRect.left;
				rtRectText.top  = rtRect.bottom + 40;
				break;
			default:
				break;
			}

			szText.Format(_T("%d"), nIdx);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

			rtRectText.top += 30;
			szText.Format(_T("%.2f"), stData.dbResultValue[nIdx]);

			if (TR_Pass == stData.nEachResult[nIdx])
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			}
		}
	}
}


void COverlay_Proc::Overlay_EIAJ(__inout IplImage* lpImage, __in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData)
{
	if (NULL == lpImage)
		return;


	int SelR = 255, SelG = 255, SelB = 0;
	int		iLineSize = 2;
	double	dbFontSize = 0.5;

	CRect	rtROIRect;
	CString strText;
	CRect	rtRect;
	CString szText;
	CRect	rtRectText;
	/*rtRect.left = stData.cCenterPoint.x - 20;
	rtRect.right = stData.cCenterPoint.x + 20;
	rtRect.top = stData.cCenterPoint.y;
	rtRect.bottom = stData.cCenterPoint.y;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(255, 200, 0), iLineSize);

	rtRect.left = stData.cCenterPoint.x;
	rtRect.right = stData.cCenterPoint.x;
	rtRect.top = stData.cCenterPoint.y - 20;
	rtRect.bottom = stData.cCenterPoint.y + 20;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(255, 200, 0), iLineSize);



	rtRectText.left = lpImage->width - 400;
	rtRectText.top = lpImage->height - 100;
	szText.Format(_T("X: %d, Y: %d"), stData.cCenterPoint.x, stData.cCenterPoint.y);
	Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, 1.0, szText);

*/
	for (int nIdx = 0; nIdx < ReOp_ItemNum; nIdx++)
	{
		rtROIRect = stOption.rectData[nIdx].RegionList;

		if (nIdx == ReOp_CenterBlack || nIdx == ReOp_CenterWhite)
		{
			if (stOption.rectData[nIdx].bUse == TRUE)
			{
				Overlay_Process(lpImage, OvrMode_RECTANGLE, rtROIRect, RGB(255, 255, 255), iLineSize);
			}
		}
		else // index = 2
		{
			if (stOption.rectData[nIdx].bUse == TRUE)
			{
				if (m_bTestmode == FALSE)
				{
					Overlay_Process(lpImage, OvrMode_RECTANGLE, rtROIRect, RGB(255, 255, 0), iLineSize);

					strText.Format(_T("%s"), g_szResolutionRegion[nIdx]);

					if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Top_Bottom)
					{
						CRect rtRect;
						rtRect.left = rtROIRect.left + (rtROIRect.Width() * 2 / 3);
						rtRect.top = rtROIRect.top - 10;
						rtRect.right = 0;
						rtRect.bottom = 0;
						Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 255, 0), iLineSize, dbFontSize, strText);
					}
					else if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Bottom_Top)
					{
						CRect rtRect;
						rtRect.left = rtROIRect.left + (rtROIRect.Width() * 2 / 3);
						rtRect.top = rtROIRect.bottom + 20;
						rtRect.right = 0;
						rtRect.bottom = 0;
						Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 255, 0), iLineSize, dbFontSize, strText);
					}
					else if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Left_Right)
					{
						CRect rtRect;
						rtRect.left = rtROIRect.left + (rtROIRect.Width() * 1 / 5);
						rtRect.top = rtROIRect.top - 10;
						rtRect.right = 0;
						rtRect.bottom = 0;
						Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 255, 0), iLineSize, dbFontSize, strText);
					}
					else if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Right_Left)
					{
						CRect rtRect;
						rtRect.left = rtROIRect.right - (rtROIRect.Width() * 1 / 5);
						rtRect.top = rtROIRect.top - 10;
						rtRect.right = 0;
						rtRect.bottom = 0;
						Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 255, 0), iLineSize, dbFontSize, strText);
					}
				}
				else
				{
					if (stData.nEachResult[nIdx] == TER_Pass)
					{
						Overlay_Process(lpImage, OvrMode_RECTANGLE, rtROIRect, RGB(0, 0, 255), iLineSize);
					}
					else
					{
						Overlay_Process(lpImage, OvrMode_RECTANGLE, rtROIRect, RGB(255, 0, 0), iLineSize);
					}

					if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Bottom_Top
						|| stOption.rectData[nIdx].i_Mode == Resol_Mode_Top_Bottom)
					{
						int m_resultpeakvalue = Overlay_PeakData(stOption, stData, nIdx);
						int m_ConstValue = 0;
						int m_ResultValue = rtROIRect.top;


						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Top_Bottom)
						{
							m_ConstValue = rtROIRect.top + m_resultpeakvalue + stOption.rectData[nIdx].PatternStartPos.y;
						}
						else
						{
							m_ConstValue = rtROIRect.bottom - m_resultpeakvalue - (rtROIRect.Height() - stOption.rectData[nIdx].PatternStartPos.y);
						}

						int iOffset1 = rtROIRect.Height() - stOption.rectData[nIdx].PatternStartPos.y;
						int iOffset2 = stOption.rectData[nIdx].PatternEndPos.y;

						CRect rtRect1;
						rtRect1.left = rtROIRect.left;
						rtRect1.top = rtROIRect.bottom - iOffset1;
						rtRect1.right = rtROIRect.right;
						rtRect1.bottom = rtROIRect.bottom - iOffset1;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect1, RGB(0, 255, 0), iLineSize);

						CRect rtRect2;
						rtRect2.left = rtROIRect.left;
						rtRect2.top = rtROIRect.top + iOffset2;
						rtRect2.right = rtROIRect.right;
						rtRect2.bottom = rtROIRect.top + iOffset2;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect2, RGB(0, 255, 0), iLineSize);

						int iThresholdValue = 0;
						int iPeakThresholdValue = 0;
						int iThresholdPosition = Overlay_StandarData(stOption, stData, nIdx);

						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Bottom_Top)
						{
							iThresholdValue = rtROIRect.bottom - iOffset1 - iThresholdPosition;
							iPeakThresholdValue = rtROIRect.bottom - iOffset1 - Overlay_StandardPeakData(stOption, stData, nIdx);
						}
						else
						{
							iThresholdValue = rtROIRect.top + stOption.rectData[nIdx].PatternStartPos.y + iThresholdPosition;
							iPeakThresholdValue = rtROIRect.top + stOption.rectData[nIdx].PatternStartPos.y + Overlay_StandardPeakData(stOption, stData, nIdx);
						}

						if (abs(rtROIRect.left - rtROIRect.right) != 0)
						{
							CRect rtRect1;
							rtRect1.left = rtROIRect.left;
							rtRect1.top = iThresholdValue;
							rtRect1.right = rtROIRect.right;
							rtRect1.bottom = iThresholdValue;
							Overlay_Process(lpImage, OvrMode_LINE, rtRect1, RGB(255, 0, 255), iLineSize);

							CRect rtRect2;
							rtRect2.left = rtROIRect.left;
							rtRect2.top = iPeakThresholdValue;
							rtRect2.right = rtROIRect.right;
							rtRect2.bottom = iPeakThresholdValue;
							Overlay_Process(lpImage, OvrMode_LINE, rtRect2, RGB(255, 0, 255), iLineSize);
						}


						CRect rtRect3;
						rtRect3.left = rtROIRect.left;
						rtRect3.top = m_ConstValue;
						rtRect3.right = rtROIRect.right;
						rtRect3.bottom = m_ConstValue;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect3, RGB(255, 255, 0), iLineSize);

						strText.Format(_T("%s [%d]"), g_szResolutionRegion[nIdx], stData.nValueResult[nIdx]);

						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Top_Bottom)
						{
							CRect rtRect;
							rtRect.left = rtROIRect.left + 50;
							rtRect.top = rtROIRect.top + 15;
							rtRect.right = 0;
							rtRect.bottom = 0;
							if (stData.nEachResult[nIdx] == TER_Pass)
							{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 0, 255), iLineSize, dbFontSize, strText);
							}
							else{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, strText);
							}
						}
						else
						{
							int nGapX;
							if (strText.GetLength() > 7)
								nGapX = 85;
							else
								nGapX = 65;

							CRect rtRect;
							rtRect.left = rtROIRect.left - nGapX;
							rtRect.top = rtROIRect.bottom -5;
							rtRect.right = 0;
							rtRect.bottom = 0;
							if (stData.nEachResult[nIdx] == TER_Pass)
							{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 0, 255), iLineSize, dbFontSize, strText);
							}
							else{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, strText);
							}
						}
					}
					else if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Right_Left
						|| stOption.rectData[nIdx].i_Mode == Resol_Mode_Left_Right)
					{
						int m_resultpeakvalue = Overlay_PeakData(stOption, stData, nIdx);
						int m_ConstValue = 0;
						int m_ResultValue = rtROIRect.left;

						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Left_Right)
						{
							m_ConstValue = rtROIRect.left + stOption.rectData[nIdx].PatternStartPos.x + m_resultpeakvalue;
						}
						else
						{
							m_ConstValue = rtROIRect.right - (rtROIRect.Width() - stOption.rectData[nIdx].PatternStartPos.x) - m_resultpeakvalue;
						}

						int iOffset1 = rtROIRect.Width() - stOption.rectData[nIdx].PatternEndPos.x;
						int iOffset2 = stOption.rectData[nIdx].PatternStartPos.x;

						CRect rtRect1;
						rtRect1.left = rtROIRect.left + iOffset2;
						rtRect1.top = rtROIRect.top;
						rtRect1.right = rtROIRect.left + iOffset2;
						rtRect1.bottom = rtROIRect.bottom;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect1, RGB(0, 255, 0), iLineSize);

						CRect rtRect2;
						rtRect2.left = rtROIRect.right - iOffset1;
						rtRect2.top = rtROIRect.top;
						rtRect2.right = rtROIRect.right - iOffset1;
						rtRect2.bottom = rtROIRect.bottom;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect2, RGB(0, 255, 0), iLineSize);

						int iThresholdPosition = Overlay_StandarData(stOption, stData, nIdx);
						int iPeakThresholdValue = Overlay_StandardPeakData(stOption, stData, nIdx);
						int iThresholdValue = 0;
						int iThresholdValue_Peak = 0;

						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Left_Right)
						{
							iThresholdValue = rtROIRect.left + stOption.rectData[nIdx].PatternStartPos.x + iThresholdPosition;
							iThresholdValue_Peak = rtROIRect.left + stOption.rectData[nIdx].PatternStartPos.x + iPeakThresholdValue;

						}
						else
						{
							iThresholdValue = rtROIRect.right - (rtROIRect.Width() - stOption.rectData[nIdx].PatternStartPos.x) - iThresholdPosition;
							iThresholdValue_Peak = rtROIRect.right - (rtROIRect.Width() - stOption.rectData[nIdx].PatternStartPos.x) - iPeakThresholdValue;
						}

						if (abs(rtROIRect.top - rtROIRect.bottom) != 0)
						{
							CRect rtRect1;
							rtRect1.left = iThresholdValue;
							rtRect1.top = rtROIRect.top;
							rtRect1.right = iThresholdValue;
							rtRect1.bottom = rtROIRect.bottom;
							Overlay_Process(lpImage, OvrMode_LINE, rtRect1, RGB(255, 0, 255), iLineSize);

							CRect rtRect2;
							rtRect2.left = iThresholdValue_Peak;
							rtRect2.top = rtROIRect.top;
							rtRect2.right = iThresholdValue_Peak;
							rtRect2.bottom = rtROIRect.bottom;
							Overlay_Process(lpImage, OvrMode_LINE, rtRect2, RGB(255, 0, 255), iLineSize);

						}

						CRect rtRect3;
						rtRect3.left = m_ConstValue;
						rtRect3.top = rtROIRect.top;
						rtRect3.right = m_ConstValue;
						rtRect3.bottom = rtROIRect.bottom;
						Overlay_Process(lpImage, OvrMode_LINE, rtRect3, RGB(255, 255, 0), iLineSize);

						strText.Format(_T("%s [%d]"), g_szResolutionRegion[nIdx], stData.nValueResult[nIdx]);

						if (stOption.rectData[nIdx].i_Mode == Resol_Mode_Left_Right)
						{
							CRect rtRect;
							rtRect.left = rtROIRect.left;
							rtRect.top = rtROIRect.top - 10;
							rtRect.right = 0;
							rtRect.bottom = 0;
							if (stData.nEachResult[nIdx] == TER_Pass)
							{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 0, 255), iLineSize, dbFontSize, strText);
							}
							else{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, strText);

							}
						}
						else
						{
							CRect rtRect;
							rtRect.left = rtROIRect.left;
							rtRect.top = rtROIRect.top - 10;
							rtRect.right = 0;
							rtRect.bottom = 0;
							if (stData.nEachResult[nIdx] == TER_Pass)
							{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 0, 255), iLineSize, dbFontSize, strText);
							}
							else{
								Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, strText);
							}
						}
					}
				}
			}
		}
	}
}

int COverlay_Proc::Overlay_PeakData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI)
{
	if (stOption.rectData[nROI].i_Range_Max == stOption.rectData[nROI].i_Range_Min)
		return 0;

	double dbValue = (double)(stData.nValueResult[nROI] - stOption.rectData[nROI].i_Range_Min) / (double)(stOption.rectData[nROI].i_Range_Max - stOption.rectData[nROI].i_Range_Min);

	double sx1 = stOption.rectData[nROI].PatternStartPos.x;
	double sy1 = stOption.rectData[nROI].PatternStartPos.y;
	double sx2 = stOption.rectData[nROI].PatternEndPos.x;
	double sy2 = stOption.rectData[nROI].PatternEndPos.y;

	double dbDist = sqrt(((sx2 - sx1)*(sx2 - sx1)) + ((sy2 - sy1)*(sy2 - sy1)));
	int iOffset = (int)(dbDist * dbValue);

	return iOffset;
}

int COverlay_Proc::Overlay_StandarData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI)
{
	if (stData.nValueResult[nROI] == stOption.rectData[nROI].i_Range_Min)
		return 0;

	double dbValue = (double)(stOption.rectData[nROI].i_Threshold_Min - stOption.rectData[nROI].i_Range_Min)
		/ (double)(stOption.rectData[nROI].i_Threshold_Max - stOption.rectData[nROI].i_Range_Min);

	double sx1 = stOption.rectData[nROI].PatternStartPos.x;
	double sy1 = stOption.rectData[nROI].PatternStartPos.y;
	double sx2 = stOption.rectData[nROI].PatternEndPos.x;
	double sy2 = stOption.rectData[nROI].PatternEndPos.y;

	double dbDist = sqrt(((sx2 - sx1)*(sx2 - sx1)) + ((sy2 - sy1)*(sy2 - sy1)));
	int iOffset = (int)(dbDist * dbValue);

	return iOffset;
}

int COverlay_Proc::Overlay_StandardPeakData(__in ST_EIAJ_Op stOption, __in ST_EIAJ_Data stData, __in UINT nROI)
{
	if (stOption.rectData[nROI].i_Range_Max == stData.nValueResult[nROI])
		return 0;

	double dbValue = (double)(stOption.rectData[nROI].i_Threshold_Max - stOption.rectData[nROI].i_Range_Min)
		/ (double)(stOption.rectData[nROI].i_Range_Max - stOption.rectData[nROI].i_Range_Min);

	double sx1 = stOption.rectData[nROI].PatternStartPos.x;
	double sy1 = stOption.rectData[nROI].PatternStartPos.y;
	double sx2 = stOption.rectData[nROI].PatternEndPos.x;
	double sy2 = stOption.rectData[nROI].PatternEndPos.y;

	double dbDist = sqrt(((sx2 - sx1)*(sx2 - sx1)) + ((sy2 - sy1)*(sy2 - sy1)));
	int iOffset = (int)(dbDist * dbValue);

	return iOffset;
}

//=============================================================================
// Method		: Overlay_Process
// Access		: protected  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in enOverlayMode enMode
// Parameter	: __in CRect rtROI
// Parameter	: __in COLORREF clrLineColor
// Parameter	: __in int iLineSize
// Parameter	: __in double dbFontSize
// Parameter	: __in CString szText
// Qualifier	:
// Last Update	: 2018/3/2 - 10:56
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Process(__inout IplImage* lpImage, __in enOverlayMode enMode, __in CRect rtROI, __in COLORREF clrLineColor /*= RGB(255, 200, 0)*/, __in int iLineSize /*= 1*/, __in double dbFontSize /*= 1.0*/, __in CString szText /*= _T("")*/)
{
	CvPoint nStartPt;
	nStartPt.x = rtROI.left;
	nStartPt.y = rtROI.top;

	CvPoint nEndPt;
	nEndPt.x = rtROI.right;
	nEndPt.y = rtROI.bottom;

	CvPoint nCenterPt;
	nCenterPt.x = nStartPt.x + ((nEndPt.x - nStartPt.x) / 2);
	nCenterPt.y = nStartPt.y + ((nEndPt.y - nStartPt.y) / 2);

	CvSize Ellipse_Axis;
	Ellipse_Axis.width = (nEndPt.x - nStartPt.x)/2;
	Ellipse_Axis.height = (nEndPt.y - nStartPt.y)/2;


	switch (enMode)
	{
	case OvrMode_LINE:
		cvLine(lpImage, nStartPt, nEndPt, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_RECTANGLE:
		cvRectangle(lpImage, nStartPt, nEndPt, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_CIRCLE:
		//cvCircle(lpImage, nStartPt, abs(nEndPt.x - nStartPt.x) / 2, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		cvEllipse(lpImage, nCenterPt, Ellipse_Axis, 0, 0, 360, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_TXT:
	{
		CvFont* cvFont = new CvFont;
		cvInitFont(cvFont, CV_FONT_VECTOR0, dbFontSize, dbFontSize, 0, 1);
		
		//  [1/10/2019 ysJang] Center Point Pic 2줄로
		int nFind = szText.Find(_T("OFFSET"), 5);
		if (nFind > 0)
		{
			int nLength = szText.GetLength();
			CString szTmpText[2] = { _T(""), };
			szTmpText[0] = szText.Left(nFind);
			cvPutText(lpImage, CT2A(szTmpText[0]), nStartPt, cvFont, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)));

			nStartPt.y += 20;
			szTmpText[1] = szText.Right(nLength - nFind);
			cvPutText(lpImage, CT2A(szTmpText[1]), nStartPt, cvFont, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)));
		}
		else
			cvPutText(lpImage, CT2A(szText), nStartPt, cvFont, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor))); // cvPoint 위치에 설정 색상으로 글씨 넣기

		delete cvFont; // 해제
	}
		break;
	default:
		break;
	}
}
