﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Model.h
// Created	:	2016/3/14 - 10:56
// Modified	:	2016/3/14 - 10:56
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_Model_h__
#define Wnd_Cfg_Model_h__

#pragma once
#include "Wnd_BaseView.h"
#include "Def_Enum.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"
#include "File_WatchList.h"
#include "List_indicatorOp.h"

enum enModel_Static
{
	STI_ML_ModelName = 0,
	STI_ML_Cam_Volt,
	STI_ML_Cam_Delay,
	STI_ML_Rect_Position,
	STI_ML_MAXNUM,
};

static LPCTSTR	g_szModel_Static[] =
{
	_T("MODEL"),
	_T("Voltage Cam"),
	_T("Camera Delay [ms]"),
	_T("Quad Rect Position"),
	NULL
};

enum enModel_Btn
{
	BT_ML_APPLY,
	BT_ML_MODULE_FIX_CHECK,
	BT_ML_MOVE_STAGE_CHECK,
	BT_ML_MAXNUM,
};

static LPCTSTR	g_szModel_Btn[] =
{
	_T("APPLY"),
	_T("Use Module Fix Check"),
	_T("Use Move Stage Check"),
	NULL
};

enum enModel_Combobox{
	ComboI_ML_Quad_Rect_Pos,
	ComboI_ML_MAXNUM = 1,
};

static LPCTSTR	g_szModel_Quad_Rect_Pos[] =
{
	_T("Not Use"),
	_T("LeftTop"),
	_T("LeftBottom"),
	_T("RightTop"),
	_T("RightBottom"),

	NULL
};

enum enModel_Edit{
	Edit_ML_ModelName = 0,
	Edit_ML_Voltage,
	Edit_ML_CameraDelay,
	Edit_ML_MAXNUM,
};

//-----------------------------------------------------------------------------
// CWnd_Cfg_Model
//-----------------------------------------------------------------------------
class CWnd_Cfg_Model : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_Model)

public:
	CWnd_Cfg_Model();
	virtual ~CWnd_Cfg_Model();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
//	afx_msg void	OnBnClickedBnApply();
	afx_msg void	OnBnClickedBnDistortion();
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);

	CFont			m_font_Data;

	CVGStatic		m_st_Item[STI_ML_MAXNUM];
	CComboBox		m_cb_Item[ComboI_ML_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[Edit_ML_MAXNUM];
	CFile_WatchList	m_IniWatch;
	CMFCButton		m_bn_Item[BT_ML_MAXNUM];
	CMFCButton		m_bn_Distortion;

	CList_IndicatorOp m_ListIndicatorOp;

	// UI에 세팅 된 데이터 -> 구조체
	void		GetUIData			(__out ST_ModelInfo& stModelInfo);
	// 구조체 -> UI에 세팅
	void		SetUIData			(__in const ST_ModelInfo* pModelInfo);

	void		RefreshFileList		(__in const CStringList* pFileList);

public:

	// 모델 데이터를 UI에 표시
	void		SetModelInfo		(__in const ST_ModelInfo* pModelInfo);
	// UI에 표시된 데이터의 구조체 반환	
	void		GetModelInfo		(__out ST_ModelInfo& stModelInfo);
};

#endif // Wnd_Cfg_Model_h__


