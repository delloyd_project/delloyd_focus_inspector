﻿#include "stdafx.h"
#include "TI_Current.h"


CTI_Current::CTI_Current()
{
}


CTI_Current::~CTI_Current()
{
}

//=============================================================================
// Method		: Current_Test
// Access		: public  
// Returns		: UINT
// Parameter	: ST_LT_TI_Current * pstCurrent
// Parameter	: int iNum
// Qualifier	:
// Last Update	: 2017/1/12 - 16:59
// Desc.		:
//=============================================================================
UINT CTI_Current::Current_Test(ST_LT_TI_Current *pstCurrent, int iNum /*= 0*/)
{
	UINT nResult = TER_Fail;

	double iCurrent[2] = { 0, 0 };

	if (FALSE == m_pDevice->PCBCamBrd[iNum].Send_GetCurrent(iCurrent))
	{
		//통신 실패 시 Fail
		nResult = TER_Timeout;
		
		pstCurrent->stCurrentData.nResult = nResult;

		return nResult;
	}

//	iCurrent[0] *= 0.1;
	pstCurrent->stCurrentData.nCurrent = iCurrent[0];
	pstCurrent->stCurrentData.nResult = nResult;

	return pstCurrent->stCurrentData.nResult;
}
